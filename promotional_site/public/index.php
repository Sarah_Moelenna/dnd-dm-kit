<?php

    $index_file = file_get_contents("index.html");
    $doc = new DOMDocument();
    $doc->loadHTML($index_file);

    $head = $doc->getElementsByTagName('head')[0];
    $body = $doc->getElementsByTagName('body')[0];
    $raw_blog_data = file_get_contents("blog_data.json");
    $blog_data = json_decode($raw_blog_data, true);

    $requestURL = $_SERVER['REQUEST_URI'];
    $baseURL = $_SERVER['SERVER_NAME'];

    $title ="The Campaigner's Toolkit - Home";
    $description="The ultimate toolkit for any tabletop rpgs. Create monsters, spells, maps. Run Campaigns and play music in real time.";
    $meta_image='/notes-meta.jpeg';
    $author = null;

    if($requestURL == '/blogs/'){
        $title = "The Campaigner's Toolkit - Blogs";
    } else if (substr( $requestURL, 0, 7 )  == '/blogs/'){
        $blog_url = str_replace('/blogs/', '', $requestURL);
        for ($i = 0; $i <= sizeof($blog_data); $i++) {
            $blog = $blog_data[$i];
            if($blog['url'] == $blog_url){
                $title = $blog['title'];
                $description = $blog['excerpt'];
                $meta_image = $blog['meta_image'];
                $author = $blog['writer'];
                break;
            }
        }
    }

?>

<!doctype html>
<html lang="en" prefix="og: http://ogp.me/ns#">
    <head>
        <title><?php echo $title; ?></title>
        <meta name="description" content="<?php echo $description; ?>">
        <meta property="og:title" content="<?php echo $title; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://<?php echo $baseURL; ?><?php echo $requestURL; ?>" />
        <meta name="image" property="og:image" content="https://<?php echo $baseURL; ?><?php echo $meta_image; ?>" />
        <?php if($author != null)
            echo '<meta name="author" content="' . $author . '">';
        ?>
        <meta name="twitter:title" content="<?php echo $title; ?>">
        <meta name="twitter:description" content=" <?php echo $description; ?>">
        <meta name="twitter:image" content="https://<?php echo $baseURL; ?><?php echo $meta_image; ?>">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@TCT_RPG">
        <?php echo $doc->saveHtml($head); ?>
    </head>
    <body>
        <script>
            window.BLOG_DATA = <?php echo $raw_blog_data; ?>;
        </script>
        <?php echo $doc->saveHtml($body); ?>
    </body>
</html>