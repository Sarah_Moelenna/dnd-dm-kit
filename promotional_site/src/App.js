import React, {Component} from 'react';

import { BrowserRouter, Switch , Route  } from "react-router-dom";
import Carousel from "react-bootstrap/Carousel"
import { SignUp } from './sections/SignUp';
import { Sliders } from './Data';
import { MeetUs } from './sections/MeetUs';
import { Header } from './sections/Header';
import { About } from './sections/About';
import { Footer } from './sections/Footer';
import { BlogList } from './sections/BlogList';
import { Helmet } from "react-helmet";
import { BlogPage } from './sections/BlogPage';

class TCPCarousel extends Component {
  render() {
    return (
      <div className="carousel-container">
        <Carousel>
          {Sliders.map((slider) => (
            <Carousel.Item key={slider.title}>
              <div className="carousel-image-container">
                <img
                  className="d-block w-100"
                  src={slider.image_src}
                  alt={slider.alt}
                />
              </div>
              <Carousel.Caption>
                <h3>{slider.title}</h3>
                <p className='slider-caption'>{slider.caption}</p>
              </Carousel.Caption>
            </Carousel.Item>
          ))}
        </Carousel>
      </div>
    )
  }
}

class Home extends Component {
  render () {
    return (
      <div>
        <Helmet>
          <title>The Campaigner's Toolkit - Home</title>
          <meta name="description" content="The ultimate toolkit for any tabletop rpgs. Create monsters, spells, maps. Run Campaigns and play music in real time." />
        </Helmet>
        <TCPCarousel/>

        <div className="border-line"/>
        <div className="about-us" id="about">
          <About/>  
        </div>
        <div className="content-container">
          <div className="content">
            <div className="blog-section" id="blogs">
              <h2 className="section-header">Latest Blogs</h2>
              <BlogList basic_only={true} slice={-3}/>
              <div className="view-all-container">
                <a href="/blogs" className='view-all'>View All</a>
              </div>
            </div>
            <div className="team-members" id="meet-us">
              <MeetUs/>
            </div>
            <div className="contact" id="contact">
              <SignUp/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class Blogs extends Component {

  render () {
    return (
      <div>
        <Helmet>
          <title>The Campaigner's Toolkit - Blogs</title>
          <meta name="description" content="The ultimate toolkit for any tabletop rpgs. Create monsters, spells, maps. Run Campaigns and play music in real time." />
        </Helmet>
        <TCPCarousel/>

        <div className="border-line-blog"/>
        <div className="blogs" id="blogs">
          <BlogList basic_only={false} slice={0}/>
        </div>
      </div>
    )
  }
}

class App extends Component {

  render () {
    console.log(this.props)

    return (
      <BrowserRouter >
        <Header/>
        <div className="landing-page">
          <div className="landing-page-container">
            <Switch>
              <Route path="/blogs/:tag/:blogName" component={BlogPage} />
              <Route path="/blogs" component={Blogs} />
              <Route path="/" component={Home} />
            </Switch>
          </div>
        </div>
        <Footer/>
      </BrowserRouter >
    )
  }
}

export default App;
