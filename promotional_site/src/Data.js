export const Sliders = [
    {
        image_src: "/notes.webp",
        alt: "Easy to use notes creation and storage",
        title: "Campaign Notes",
        caption: "Easily create, store and access the notes for your campaign."
    },
    {
        image_src: "/dungeon.webp",
        alt: "Dungeon creation made easy",
        title: "Dungeon Maps",
        caption: "Share dungeon maps in real time with your players"
    },
    {
        image_src: "/encounter.webp",
        alt: "Simple encounter planning",
        title: "Encounters",
        caption: "Plan your encounters upfront, from monsters to music to maps."
    },
    {
        image_src: "/monster.webp",
        alt: "Easy to read Monster Stat Sheets",
        title: "Monsters",
        caption: "Access all your Monster Stat Sheets at the click of a button."
    },
    {
        image_src: "/race.webp",
        alt: "Customisable Homebrew Content",
        title: "Homebrew Support",
        caption: "Create your own homebrew content for races, monsters, spells and more."
    },
    {
        image_src: "/combat.webp",
        alt: "Real time combat tracker",
        title: "Combat Tracking",
        caption: "Track and Share combat in real time with your players."
    },
]

export const TeamMembers = [
    {
        name: "Sarah Anderson",
        image: "/sarah.webp",
        title: "Product Owner / Senior Fullstack Engineer",
        url: "https://www.linkedin.com/in/sarah-anderson-495537101/"
    },
    {
        name: "Gerik Peterson",
        image: "/gerik.webp",
        title: "Senior Fullstack Engineer",
        url: "https://www.linkedin.com/in/gerikpeterson/"
    },
    {
        name: "Ilja Belankins",
        image: "/ilja.webp",
        title: "Fullstack Engineer",
        url: "https://www.linkedin.com/in/ilja-belankins-3087b6145/"
    },
]

export const Menu = [
    {
        name: "Home",
        url: "/#home",
    },
    {
        name: "About",
        url: "/#about",
    },
    {
        name: "Blogs",
        url: "/blogs",
    },
    {
        name: "The Team",
        url: "/#meet-us",
    },
    {
        name: "Follow Us",
        url: "/#contact",
    },
]

export const AboutBlocks = [
    {
        name: "Homebrew Content",
        icon: "fas fa-spider",
        text: `With simple, easy to use editors for all the homebrew content you want. The Campaigners Toolkit allows you to create Monsters, Spells, Classes and more to share with your players.\n
        Ever found an interesting third party race or subclass you're dying to use but your tool of choice doesn't allow it, well now you can. Simply login to The Campaigners Toolkit and add it in yourself and your players can start building characters with it straight away.`
    },
    {
        name: "Audio Control",
        icon: "fas fa-music",
        text: `To really help your campaign feel magical, The Campainger's Toolkit built in audio service allows you to play and loop individual tracks or playlists and play one off sound effects.\n
        Never underestimate the power of a well thought out soundtrack to help set the mood of the adventure and wow your players. Playing sound at your table, or pushing it through to your player's devices has never been easier.`
    },
    {
        name: "Maps Galore",
        icon: "fas fa-compass",
        text: `A beautiful dungeon map or a hazardous battlemap can really bolster your players immersion in the game. The Campaigner's Toolkit provides powerful tools to help you build exquisite maps.\n
        Share your freshly made maps with your players in real-time digitally or use our observer mode to show them off at the tabletop.`
    },
    {
        name: "Encounter Tracking",
        icon: "fas fa-fist-raised",
        text: `Tracking who's hit whom, where everyone is and are they prone yet can be quite the pain for any dungeon master, new or experienced. Worry no more as The Campaigner's Toolkit makes this as easy as rolling a 1 on a stealth check.\n
        With built in initiative, HP and condition tracking and quick access monsters sheets, managing your monsters is a complex task no more. Add in a battlemap to your encounter to let your players move their characters around themselves for extra ease of use.`
    },
    {
        name: "Note Management",
        icon: "fas fa-book",
        text: `Notes in one place, tabletop in another and monster sheets in a third. With the fully integrated Campaigner's Toolkit, you can now keep your notes where you play. An easy to use interface lets you pre plan your campaign swiftly and edit it on the fly.\n
        Insert maps, items and more into your notes for super quick access to the information you need when you need. Add quick play buttons for both music and encounters and remove the faffing about of setting up a new combat.`
    }
]