import React, {Component} from 'react';
import { AboutBlocks } from '../Data';
import {Row, Col} from 'react-bootstrap'

export class About extends Component {

    render () {
        return (
            <div>
                {AboutBlocks.map((about_block, index) => (
                    <div key={about_block.name} className="about-section">
                        <div className="container">
                            {index%2 != 1 ?
                                <Row>
                                    <Col xs={{span: 12, order: 0}} sm={{span: 12, order: 0}} md={3} className="title-icon">
                                        <h3>{about_block.name}</h3>
                                        <i className={about_block.icon}></i>
                                    </Col>
                                    <Col xs={{span: 12, order: 1}} sm={{span: 12, order: 1}} md={9}>
                                        <p>{about_block.text}</p>
                                    </Col>
                                </Row>
                            :
                                <Row>
                                    <Col xs={{span: 12, order: 1}} sm={{span: 12, order: 1}} md={{span: 9, order: 0}}>
                                        <p>{about_block.text}</p>
                                    </Col>
                                    <Col  xs={{span: 12, order: 0}} sm={{span: 12, order: 0}} md={{span: 3, order: 1}} className="title-icon">
                                        <h3>{about_block.name}</h3>
                                        <i className={about_block.icon}></i>
                                    </Col>
                                </Row>
                            }
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}