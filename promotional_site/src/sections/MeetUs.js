import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap'
import { TeamMembers } from '../Data';

export class MeetUs extends Component {

    render () {
        return (
            <div>
                <h2 className="section-header">Meet the Team</h2>
                <Row>
                    {TeamMembers.map((team_member) => (
                        <Col sm={12} md={4} key={team_member.name} className="team-member">
                        <a href={team_member.url} target="_blank">
                            <div className="team-image-container">
                            <img src={team_member.image} alt={team_member.name + " - " + team_member.title}/>
                            </div>
                            <h3>{team_member.name}</h3>
                            <p>{team_member.title}</p>
                        </a>
                        </Col>
                    ))}
                </Row>
            </div>
        )
    }
}