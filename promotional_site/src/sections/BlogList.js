import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap'

export class BlogList extends Component {

    state = {
        blogs: []
      }
    
      componentWillMount() {
        if(window.BLOG_DATA != undefined){
          this.setState({blogs: window.BLOG_DATA})
        } else {
          fetch('/blog_data.json', {
                  method: "GET",
              }).then( response => {
              if (response.status >= 400) { throw response }
                  return response
              }).then(res => res.json())
              .then((response) => {
                  this.setState({blogs: response})
              }
          )
        }
      }

    render () {
        var blog_data = this.state.blogs
        if(this.props.slice != 0){
            blog_data = blog_data.reverse().slice(this.props.slice).reverse()
        }
        return (
            <div>
                <Row className='blog-row'>
                    {blog_data.map((blog, index) => (
                        <Col xs={12} sm={12} md={(index < 2 && this.props.basic_only == false) ? 6 : 4}>
                            <div className='blog-item'>
                                <a href={"/blogs/" + blog.url}>
                                    <div className='blog-thumbnail' style={{ 
                                        backgroundImage: `url(${blog.thumbnail})`
                                    }}>
                                        <div className='details'>{blog.date} by {blog.writer}</div>
                                    </div>
                                </a>
                                <div className='title'>{blog.title}</div>
                                { index < 2 && this.props.basic_only == false &&
                                    <div className='text'>
                                        <p className='excerpt'>{blog.excerpt}</p>
                                        <a href={"/blogs/" + blog.url} className='read-more'>Read More</a>
                                    </div>
                                }
                            </div>
                        </Col>
                    ))}
                </Row>
            </div>
        )
    }
}