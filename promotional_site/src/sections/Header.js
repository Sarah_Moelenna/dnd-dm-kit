import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap'
import { Menu } from '../Data';

export class Header extends Component {

    state = {
        toggle: false
    }

    render () {
        return (
            <div className="header-container" id="home">
                <div className="header">
                    <Row className='no-gutters'>
                        <Col xs={6} sm={6} md={4}>
                            <img src="/logo.webp" alt="The Campaigners Toolkit" className="logo"/>
                        </Col>
                        <Col xs={6} sm={0} md={0} className='mobile-menu-button-col'>
                            <div className='menu-button-container'>
                                <div className='menu-button' onClick={() =>{this.setState({toggle: !this.state.toggle})}}>
                                    <i className="fas fa-bars"></i>
                                </div>
                            </div>
                        </Col>
                        <Col xs={12} sm={12} md={8} className="menu-col">
                            <div className="menu desktop-menu">
                                {Menu.map((menu_item, index) => (
                                    <a key={index} className="menu-item" href={menu_item.url}>
                                        <p>{menu_item.name}</p>
                                    </a>
                                ))}
                            </div>
                        </Col>
                        {this.state.toggle == true &&
                            <Col xs={12} sm={12} md={12} className="menu-col-mobile">
                                <div className="menu mobile-menu">
                                    {Menu.map((menu_item, index) => (
                                        <a key={index} className="menu-item" href={menu_item.url} onClick={() =>{this.setState({toggle: false})}}>
                                            <p>{menu_item.name}</p>
                                        </a>
                                    ))}
                                </div>
                            </Col>
                        }
                    </Row>
                </div>
            </div>
        )
    }
}