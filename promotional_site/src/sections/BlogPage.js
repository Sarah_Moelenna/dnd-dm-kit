import React, {Component} from 'react';
import Markdown from 'react-markdown'
import { Helmet } from "react-helmet";
import { TeamMembers } from '../Data';
import { DNDMonsterInfo } from './DNDMonsterInfo';

export class BlogPage extends Component {
    state = {
      blog: null,
      resources: {monsters: []}
    }
  
    componentWillMount() {
        fetch('/blogs/' + this.props.match.params.blogName + '.json', {
                method: "GET",
            }).then( response => {
            if (response.status >= 400) { throw response }
                return response
            }).then(res => res.json())
            .then((response) => {
                this.setState({blog: response})
                this.fetch_resources(response)
            }
        )
    }

    getTeamMember = (name) => {
        for(let i = 0; i < TeamMembers.length; i++){
            let member = TeamMembers[i]
            if(member.name == name){
                return member
            }
        }
        return undefined
    }

    load_resource = (location, type) => {
        fetch(location, {
                method: "GET",
            }).then( response => {
            if (response.status >= 400) { throw response }
                return response
            }).then(res => res.json())
            .then((response) => {
                var resources = this.state.resources
                resources[type].push(response)
                this.setState({resources: resources})
            }
        )
    }

    fetch_resources = (blog) => {
        console.log(blog)
        if(blog.additional_resources.monsters){
            for(let i = 0; i < blog.additional_resources.monsters.length; i++){
                let monster_file = '/dnd-monsters/' + blog.additional_resources.monsters[i] + '.json'
                this.load_resource(monster_file, 'monsters')
            }
        }
    }
  
    render () {
        let author = undefined
        if(this.state.blog != null){
            author = this.getTeamMember(this.state.blog.author)
        }

        return (
            <div>
                {this.state.blog &&
                    <div>
                        <div className='banner-image-container' style={{ 
                            backgroundImage: `url(${this.state.blog.banner})`
                        }}/>
                        <div className="border-line-blog"/>
                            <div className='single-blog'>
                                <h1 className='blog-title'>{this.state.blog.title}</h1>
                                <div className='blog-content'>
                                    <div className='blog-meta'>
                                        <img className='author-image' src={author.image} alt={author.name}/>
                                        <p>by {author.name}<br/>{this.state.blog.date}</p>
                                        { (this.state.blog.meta.previous_blog != null || this.state.blog.meta.next_blog) &&
                                            <div className={'blog-navigation' + ((this.state.blog.meta.previous_blog == null && this.state.blog.meta.next_blog != null) ? " align-right": "")}>
                                                { this.state.blog.meta.previous_blog != null &&
                                                    <div className='previous-blog'>
                                                        <a href={this.state.blog.meta.previous_blog.url}><i className="fas fa-chevron-left"></i> {this.state.blog.meta.previous_blog.title}</a>
                                                    </div>
                                                }
                                                { this.state.blog.meta.next_blog != null &&
                                                    <div className='next-blog'>
                                                        <a href={this.state.blog.meta.next_blog.url}>{this.state.blog.meta.next_blog.title} <i className="fas fa-chevron-right"></i></a>
                                                    </div>
                                                } 
                                            </div>
                                        }
                                    </div>
                                    {this.state.blog.content.map((content_block, index) => (
                                        <div className='content-block' key={index}>
                                            { content_block.type == 'PARAGRAPH' &&
                                                <p className='paragraph'>{content_block.data}</p>
                                            }
                                            { content_block.type == 'SUBTITLE' &&
                                                <h2 className='subtitle'>{content_block.data}</h2>
                                            }
                                            { content_block.type == 'HEADING' &&
                                                <h3 className='heading'>{content_block.data}</h3>
                                            }
                                            { content_block.type == 'MARKDOWN' &&
                                                <div className='markdown'>
                                                    <Markdown children={content_block.data} />
                                                </div>
                                            }
                                            { content_block.type == 'LINE' &&
                                                <div className='line'/>
                                            }
                                            { content_block.type == 'IMAGES' &&
                                                <div className='images'>
                                                    {content_block.data.map((image, index) => (
                                                        <div className='image' key={index}>
                                                            <a target="_blank" href={image.url}><img src={image.url} alt={image.alt} /></a>
                                                            <p>{image.text}</p>
                                                        </div>
                                                    ))}
                                                </div>
                                            }
                                        </div>
                                    ))}
                                </div>
                                <div className='blog-resources'>
                                    {this.state.resources.monsters.length > 0 &&
                                        <div className='monsters'>
                                            <h3>Monster Stats</h3>
                                            {this.state.resources.monsters.map((monster, index) => (
                                                <div className='monster-resource' key={index}>
                                                    <DNDMonsterInfo monster={monster} />
                                                </div>
                                            ))}
                                        </div>
                                    }
                                </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
  }