import React, {Component} from 'react';

export class Footer extends Component {

    render () {
        return (
            <div className="footer-container">
                <div className="footer">
                    <p>© The Campaigner's Toolkit</p>
                </div>
            </div>
        )
    }
}