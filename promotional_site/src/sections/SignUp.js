import React, {Component} from 'react';
import Mailchimp from 'react-mailchimp-form'

export class SignUp extends Component {

    render () {
        return (
            <div>
                <h2 className="section-header">Subscribe to Updates</h2>
                <p>Join our mailing list for updates on new releases, free content and more!</p>
                <div className="form">
                    <Mailchimp
                        action="https://campaignerstoolkit.us20.list-manage.com/subscribe/post?u=2832de7531ffa331387f2b74e&id=71d26fdaaf"
                        fields={[
                            {
                                name: 'FNAME',
                                placeholder: 'Name',
                                type: 'text',
                                required: true
                            },
                            {
                                name: 'EMAIL',
                                placeholder: 'Email',
                                type: 'email',
                                required: true
                            }
                        ]}
                        messages = {
                            {
                            sending: "Sending...",
                            success: "Thank you for subscribing!",
                            error: "An unexpected internal error has occurred.",
                            empty: "You must write an e-mail.",
                            duplicate: "Too many subscribe attempts for this email address",
                            button: "Subscribe"
                            }
                        }
                    />
                </div>
                <p>Or follow us on social media for the latest updates.</p>
                <div className='social-media-links'>
                    <a className='linkedin-link' href="https://www.linkedin.com/company/campaigners-toolkit" target="_blank"><i className="fab fa-linkedin"></i></a>
                    <a className='facebook-link' href="https://www.facebook.com/Campaigners-Toolkit-104979802089859" target="_blank"><i className="fab fa-facebook"></i></a>
                    <a className='twitter-link' href="https://twitter.com/TCT_RPG" target="_blank"><i className="fab fa-twitter"></i></a>
                    <a className='discord-link' href="https://discord.gg/KdjWKZyP" target="_blank"><i className="fab fa-discord"></i></a>
                </div>
            </div>
        )
    }
}