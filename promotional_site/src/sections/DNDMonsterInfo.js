import React, { Component } from 'react';
import Markdown from 'react-markdown'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Collapse } from 'react-bootstrap'

export class DNDMonsterInfo extends Component {

    state = {
        toggle: false
    }

    render(){
        return(
            <div>
                <div className='monster-meta' onClick={() => {this.setState({toggle: !this.state.toggle})}}>
                    <h2>{this.props.monster.name}</h2>
                    {this.props.monster.types != null && <p>{this.props.monster.types}</p>}
                    {this.props.monster.environments != null && <p>{this.props.monster.environments}</p>}
                </div>
                <Collapse in={this.state.toggle}>
                    <div className="monster-data">
                        <Row>
                            <Col xs={12} md={6}>

                                {this.props.monster.armour_class != null && <p><b>Armor Class</b> {this.props.monster.armour_class}</p>}
                                {this.props.monster.hit_points != null && <p><b>Hit Points</b> {this.props.monster.hit_points}</p>}
                                {this.props.monster.speed != null && <p><b>Speed</b> {this.props.monster.speed}</p>}
                                <hr/>
                                <Row>
                                    <Col xs={4} md={2}>
                                        <p><b>STR</b><br/>{this.props.monster.strength}</p>
                                    </Col>
                                    <Col xs={4} md={2}>
                                        <p><b>DEX</b><br/>{this.props.monster.dexterity}</p>
                                    </Col>
                                    <Col xs={4} md={2}>
                                        <p><b>CON</b><br/>{this.props.monster.constitution}</p>
                                    </Col>
                                    <Col xs={4} md={2}>
                                        <p><b>INT</b><br/>{this.props.monster.intelligence}</p>
                                    </Col>
                                    <Col xs={4} md={2}>
                                        <p><b>WIS</b><br/>{this.props.monster.wisdom}</p>
                                    </Col>
                                    <Col xs={4} md={2}>
                                        <p><b>CHA</b><br/>{this.props.monster.charisma}</p>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={12} md={6}>
                                {this.props.monster.saving_throws != null && <p><b>Saving Throws</b> <Markdown children={this.props.monster.saving_throws}/></p>}
                                {this.props.monster.skills != null && <p><b>Skills</b> <Markdown children={this.props.monster.skills}/></p>}
                                {this.props.monster.damage_immunities != null && <p><b>Damage Immunities</b> {this.props.monster.damage_immunities}</p>}
                                {this.props.monster.damage_resistances != null && <p><b>Damage Resistances</b> {this.props.monster.damage_resistances}</p>}
                                {this.props.monster.damage_vulnerabilities != null && <p><b>Damage Vulnerability</b> {this.props.monster.damage_vulnerabilities}</p>}
                                {this.props.monster.condition_immunities != null && <p><b>Condition Immunities</b> {this.props.monster.condition_immunities}</p>}
                                {this.props.monster.senses != null && <p><b>Senses</b> {this.props.monster.senses}</p>}
                                {this.props.monster.languages != null && <p><b>Languages</b> {this.props.monster.languages}</p>}
                                {this.props.monster.challenge != null && <p><b>Challenge</b> {this.props.monster.challenge}</p>}
                                {this.props.monster.proficiency_bonus != null && <p><b>Proficiency Bonus</b> {this.props.monster.proficiency_bonus}</p>}
                            </Col>
                        </Row>
                        <hr/>
                        <Row>
                            <Col xs={12} md={6}>
                                {this.props.monster.actions != null &&
                                    <div>
                                        <h3>Actions</h3>
                                        <Markdown children={this.props.monster.actions} />
                                    </div>
                                }
                            </Col>
                            <Col xs={12} md={6}>
                                {this.props.monster.legendary_actions != null &&
                                    <div>
                                        <h3>Legendary Actions</h3>
                                        <Markdown children={this.props.monster.legendary_actions} />
                                    </div>
                                }
                                {this.props.monster.reactions != null &&
                                    <div>
                                        <h3>Reactions</h3>
                                        <Markdown children={this.props.monster.reactions} />
                                    </div>
                                }
                                {this.props.monster.description != null &&
                                    <div>
                                        <h3>Abilities</h3>
                                        <Markdown children={this.props.monster.description} />
                                    </div>
                                }
                                {this.props.monster.mythic != null &&
                                    <div>
                                        <h3>Mythic Actions</h3>
                                        <Markdown children={this.props.monster.mythic} />
                                    </div>
                                }
                                {this.props.monster.bonus != null &&
                                    <div>
                                        <h3>Bonus Actions</h3>
                                        <Markdown children={this.props.monster.bonus} />
                                    </div>
                                }
                                
                            </Col>
                        </Row>
                    </div>
                </Collapse>
            </div>
        )
    };
}
