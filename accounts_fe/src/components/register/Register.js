import React, {Component} from 'react';
import Cookies from 'universal-cookie';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { ACCOUNTS_POST } from '../AccountsRequests';
import { Link } from 'react-router-dom';

const cookies = new Cookies();

export class Register extends Component {

    state = {
        email: null,
        forename: null,
        surname: null,
        display_name: null,
        password: null,
    }

    register = (event) => {

        let data = {
            email: this.state.email,
            forename: this.state.forename,
            surname: this.state.surname,
            display_name: this.state.display_name,
            password: this.state.password
        }

        ACCOUNTS_POST(
            '/register',
            data,
            (jsondata) => {
                cookies.set('auth_token', jsondata.token, { path: '/' });
                this.props.history.push('/account');
            },
            (e) => { this.setState({error: true}) }
        )

        event.preventDefault();
    };

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    render () {
        return (
            <div className="login-box">
                <h1>REGISTER</h1>
                <Form onSubmit={this.register}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Email</Form.Label>
                        <Form.Control required value={this.state.email} name="email" type="text" placeholder="Email" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Password</Form.Label>
                        <Form.Control required value={this.state.password} name="password" type="password" placeholder="Password" onChange={this.handleChange} />
                    </Form.Group>
                    <hr/>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Forename</Form.Label>
                        <Form.Control required value={this.state.forename} name="forename" type="text" placeholder="Forename" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Surname</Form.Label>
                        <Form.Control required value={this.state.surname} name="surname" type="text" placeholder="Surname" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Display Name</Form.Label>
                        <Form.Control required value={this.state.display_name} name="display_name" type="text" placeholder="Display Name" onChange={this.handleChange} />
                    </Form.Group>
                    <br/>
                    <Button variant="primary" type="submit">
                            Register
                    </Button>
                    {
                        this.state.error == true &&
                        <p className="error">Registration Failed</p>
                    }
                </Form>
                <hr/>
                <p>Already have an account?</p>
                <Link className='btn btn-primary' to='/login'>Login Now</Link>
            </div>
        )
    }
}