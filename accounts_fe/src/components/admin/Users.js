import React, {Component} from 'react';
import { ACCOUNTS_GET } from '../AccountsRequests';
import Paginator from '../Paginator';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { stringify } from 'query-string';

export class Users extends Component {

    state = {
        users: [],
        total_pages: 0,
        page: 1,
        filters: {}
    }

    componentWillMount() {
        this.refresh_users(this.state.page, this.state.filters)
    }

    refresh_users = (page, filters) => {
        var params = filters
        params['page'] = page

        ACCOUNTS_GET(
            "/users?" + stringify(params),
            (jsondata) => {
                this.setState({users: jsondata.results, page: jsondata.current_page, total_pages: jsondata.total_pages})
            },
            (e) => { this.setState({users: [], page: 1, total_pages: 0}) }
        )

    };

    set_page = (page) => {
        this.refresh_users(page, this.state.filters)
    };

    render () {
        return (
            <div className="profile-box">
                <h1>Users</h1>
                <div className='list-items'>
                    {this.state.users.map((user) => (
                        <div key={user.id} className="list-item">
                            <Row>
                                <Col xs={3} md={3}>
                                    <p className="card-text">{user.display_name}</p>
                                </Col>
                                <Col xs={3} md={3}>
                                    <p className="card-text">{user.email}</p>
                                </Col>
                                <Col xs={3} md={3}>
                                    <p className="card-text">{user.user_type}</p>
                                </Col>
                            </Row>
                            <hr/>
                        </div>
                    ))}
                </div>
                <Paginator current_page={this.state.current_page} total_pages={this.state.total_pages} page_change_function={this.set_page} />
            </div>
        )
    }
}