const { REACT_APP_ACCOUNTS_API_URL, REACT_APP_BLOG_FE_URL } = process.env;

export const get_api_url = () => {
    if(process.env.NODE_ENV == 'development'){
        return REACT_APP_ACCOUNTS_API_URL;
    } else {
        return window.ACCOUNTS_API_URL;
    }
}

export const get_blog_fe_url = () => {
    if(process.env.NODE_ENV == 'development'){
        return REACT_APP_BLOG_FE_URL;
    } else {
        return window.BLOG_FE_URL;
    }
}