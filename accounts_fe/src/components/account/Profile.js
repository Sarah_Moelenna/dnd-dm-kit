import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { ACCOUNTS_PUT } from '../AccountsRequests';
import { UserContext } from '../UserContext';

export class Profile extends Component {

    state = {
        forename: null,
        surname: null,
        display_name: null,
        success: null
    }

    static contextType = UserContext

    componentWillMount() {
        const user_context = this.context
    
        this.setState({
            forename: user_context.user.forename,
            surname: user_context.user.surname,
            display_name: user_context.user.display_name
        })
      }

    update = (event) => {

        const user_context = this.context

        let data = {
            forename: this.state.forename,
            surname: this.state.surname,
            display_name: this.state.display_name
        }
        this.setState({success: null, error: null})

        ACCOUNTS_PUT(
            '/self',
            data,
            (jsondata) => {
                user_context.update_user(jsondata)
                this.setState({success: true})
            },
            (e) => { this.setState({error: true}) }
        )

        event.preventDefault();
    };

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    render () {
        return (
            <div className="profile-box">
                <h1>Profile</h1>
                <Form onSubmit={this.update}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control required value={this.state.forename} name="forename" type="text" placeholder="First Name" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control required value={this.state.surname} name="surname" type="text" placeholder="Last Name" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Display Name</Form.Label>
                        <Form.Control required value={this.state.display_name} name="display_name" type="text" placeholder="Display name" onChange={this.handleChange} />
                    </Form.Group>
                    <br/>
                    <Button variant="primary" type="submit">
                            Save
                    </Button>
                    {
                        this.state.error == true &&
                        <p className="error"><i className="fas fa-times"></i> Failed to Save</p>
                    }
                    {
                        this.state.success == true &&
                        <p className="success"><i className="fas fa-check"></i> Saved Successfully</p>
                    }
                </Form>
            </div>
        )
    }
}