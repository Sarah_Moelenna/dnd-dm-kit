import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import Spinner from '../Spinner';
import { UserContext } from '../UserContext';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Profile } from './Profile';
import { Home } from './Home';
import { UpdatePassword } from './UpdatePassword'; 
import { Switch , Route  } from "react-router-dom";
import { Users } from '../admin/Users';
import { get_blog_fe_url } from '../Config';

export class Dashboard extends Component {

  static contextType = UserContext


  render () {
    const user_context = this.context

    return (
        <div className="dashboard-container">
            {user_context.user == null ?
                <Spinner/>
                :
                <Switch>
                  <Route path={["/account/password"]} component={UpdatePassword} />
                  <Route path={["/account/profile"]} component={Profile} />
                  <Route path={["/account"]} component={Home} />
                  <Route path={["/admin/users"]} component={Users} />
                </Switch>
            }
        </div>
    )
  }
}

export class DashboardMenu extends Component {

    static contextType = UserContext
  
    render () {
      const user_context = this.context

      return (
          <div className="dashboard-menu">
                <Link className='menu-item' to="/account"><Row>
                    <Col xs={2} md={2} className='icon'><i className="fas fa-home"></i></Col>
                    <Col xs={10} md={10} className='name'>Home</Col>
                </Row></Link >
                <Link className='menu-item' to="/account/profile"><Row>
                    <Col xs={2} md={2} className='icon'><i className="fas fa-user"></i></Col>
                    <Col xs={10} md={10} className='name'>Profile</Col>
                </Row></Link >
                <Link className='menu-item' to="/account/password"><Row>
                    <Col xs={2} md={2} className='icon'><i className="fas fa-lock"></i></Col>
                    <Col xs={10} md={10} className='name'>Change Password</Col>
                </Row></Link >
                <div className='menu-item' onClick={() => this.props.logout()}><Row>
                    <Col xs={2} md={2} className='icon'><i className="fas fa-door-open"></i></Col>
                    <Col xs={10} md={10} className='name'>Logout</Col>
                </Row></div>
                { user_context.user != null && user_context.user.permissions.includes("ACCOUNTS.USER.MANAGEMENT") &&
                  <div>
                    <hr/>
                    <Link className='menu-item' to="/admin/users"><Row>
                      <Col xs={2} md={2} className='icon'><i className="fas fa-users"></i></Col>
                      <Col xs={10} md={10} className='name'>Users</Col>
                    </Row></Link >
                  </div>
                }
                { user_context.user != null && user_context.user.permissions.includes("BLOGS.MANAGEMENT") &&
                  <div>
                    <hr/>
                    <a className='menu-item' href={get_blog_fe_url()}><Row>
                      <Col xs={2} md={2} className='icon'><i className="fas fa-users"></i></Col>
                      <Col xs={10} md={10} className='name'>Blogs</Col>
                    </Row></a>
                  </div>
                }
          </div>
      )
    }
  }