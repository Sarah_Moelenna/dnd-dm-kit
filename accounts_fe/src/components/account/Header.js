import React, { Component } from 'react';

class Header extends Component {    
    
    render(){
        return(
            <div className="header">
                <p>The Campaigner's Toolkit</p>
            </div>
            );
 };
}

export default Header;