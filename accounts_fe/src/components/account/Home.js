import React, {Component} from 'react';
import { UserContext } from '../UserContext';


export class Home extends Component {

    static contextType = UserContext

    render () {
        const user_context = this.context

        return (
            <div className="home-box">
                <h1>Welcome Back {user_context.user.forename}</h1>

            </div>
        )
    }
}