import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { ACCOUNTS_PUT } from '../AccountsRequests';

export class UpdatePassword extends Component {

    state = {
        current_password: null,
        new_password: null,
        success: null
    }

    update_password = (event) => {

        let data = {
            current_password: this.state.current_password,
            new_password: this.state.new_password
        }
        this.setState({success: null, error: null})

        ACCOUNTS_PUT(
            '/self/password',
            data,
            (jsondata) => {
                this.setState({success: true, current_password: null, new_password: null})
            },
            (e) => { this.setState({error: true, current_password: null, new_password: null}) }
        )

        event.preventDefault();
    };

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    render () {
        return (
            <div className="profile-box">
                <h1>Change Password</h1>
                <Form onSubmit={this.update_password}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Current Password</Form.Label>
                        <Form.Control required value={this.state.current_password} name="current_password" type="password" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>New Password</Form.Label>
                        <Form.Control required value={this.state.new_password} name="new_password" type="password" onChange={this.handleChange} />
                    </Form.Group>
                    <br/>
                    <Button variant="primary" type="submit">
                            Save
                    </Button>
                    {
                        this.state.error == true &&
                        <p className="error"><i className="fas fa-times"></i> Failed to Update Password</p>
                    }
                    {
                        this.state.success == true &&
                        <p className="success"><i className="fas fa-check"></i> Successfully Changed Password</p>
                    }
                </Form>
            </div>
        )
    }
}