import React, {Component} from 'react';
import Cookies from 'universal-cookie';
import { ACCOUNTS_GET } from '../AccountsRequests';
import { UserContext } from '../UserContext';
import Header from './Header';
import Footer from './Footer';
import { Dashboard, DashboardMenu } from './Dashboard';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const cookies = new Cookies();

export class AccountHome extends Component {

  static contextType = UserContext

  state = {
    user: null
  }

  componentWillMount() {
    const user_context = this.context

    ACCOUNTS_GET(
        '/self',
        (jsondata) => {
          user_context.update_user(jsondata)
        },
        (e) => { 
            cookies.remove('auth_token', { path: '/' });
            this.props.history.push('/');
        }
    )
  }

  logout = () => {
    cookies.remove('auth_token', { path: '/' });
    this.props.history.push('/');
  }

  render () {

    return (
        <div className="account-container">
          <div className="account-area">
            <Header/>
              <div className="account-content">
                  <Row>
                    <Col xs={2} md={2} className='dashboard-menu'>
                        <DashboardMenu logout={this.logout}/>
                    </Col>
                    <Col xs={10} md={10} className='dashboard'>
                      <Dashboard/>
                    </Col>
                  </Row>
              </div>
            <Footer/>
          </div>
        </div>
    )
  }
}