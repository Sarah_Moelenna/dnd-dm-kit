import React, {Component} from 'react';

import { Switch , Route  } from "react-router-dom";
import { Login } from '../login/Login';
import { Register } from '../register/Register';

export class Home extends Component {

  render () {
    return (
        <div className="home-container">
            <div className="home-content">
                <Switch>
                    <Route path="/register" component={Register} />
                    <Route path={["/", "/login"]} component={Login} />
                </Switch>
            </div>
        </div>
    )
  }
}