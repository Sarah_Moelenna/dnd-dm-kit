const Spinner = () => {
    return (
        <div className="spinner">
            <i className="fas fa-spinner"></i>
        </div>
    );
}

export default Spinner;