import React, {Component} from 'react';
import Cookies from 'universal-cookie';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { ACCOUNTS_POST } from '../AccountsRequests';
import { Link } from 'react-router-dom';

const cookies = new Cookies();

export class Login extends Component {

    state = {
        email: null,
        password: null,
        error: false,
    }

    login = (event) => {

        let data = {
            email: this.state.email,
            password: this.state.password
        }

        ACCOUNTS_POST(
            '/login',
            data,
            (jsondata) => {
                if(process.env.NODE_ENV == 'development'){
                    cookies.set('auth_token', jsondata.token, { path: '/' });
                } else {
                    cookies.set('auth_token', jsondata.token, { path: '/', domain: 'campaignerstoolkit.com' });
                }
                this.props.history.push('/account');
            },
            (e) => { this.setState({error: true}) }
        )

        event.preventDefault();
    };

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    render () {
        return (
            <div className="login-box">
                <h1>LOGIN</h1>
                <Form onSubmit={this.login}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Email</Form.Label>
                        <Form.Control required value={this.state.email} name="email" type="text" placeholder="Email" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Password</Form.Label>
                        <Form.Control required value={this.state.password} name="password" type="password" placeholder="Password" onChange={this.handleChange} />
                    </Form.Group>
                    <br/>
                    <Button variant="primary" type="submit">
                            Login
                    </Button>
                    {
                        this.state.error == true &&
                        <p className="error">Login Failed</p>
                    }
                </Form>
                <hr/>
                <p>Forgotten your password?</p>
                <Link className='btn btn-primary' to='/password-reset'>Reset password</Link>
                <hr/>
                <p>Don't have an account?</p>
                <Link className='btn btn-primary' to='/register'>Register Now</Link>
            </div>
        )
    }
}