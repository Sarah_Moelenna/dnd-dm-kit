import React, {Component} from 'react';

import { BrowserRouter, Switch , Route  } from "react-router-dom";
import { Home } from './components/home/Home';
import { AccountHome } from './components/account/AccountHome';
import { UserContext } from './components/UserContext';

const get_random_image = () => {
  var images = [
      "/media/background_1.jpg",
      "/media/background_2.jpg",
      "/media/background_3.jpg",
      "/media/background_4.jpg",
      "/media/background_5.jpg"
  ]
  return images[Math.floor(Math.random() * images.length)];
};

class App extends Component {

  state = {
    user: null,
    background: get_random_image()
  }

  update_user = (user) => {
    this.setState({user: user})
  }

  get_user_context = () => {
    return {
      user: this.state.user,
      update_user: this.update_user
    }
  }

  render () {

    return (
      <div className="app-container" style={{ 
        backgroundImage: 'url("' + this.state.background + '")'
      }}>
        <UserContext.Provider value={this.get_user_context()}>
          <BrowserRouter >
            <Switch>
              <Route path={["/account", "/account/*", "/admin/*"]} component={AccountHome} />
              <Route path={["/", "/login", "/register"]} component={Home} />
            </Switch>
          </BrowserRouter >
        </UserContext.Provider>
      </div>
    )
  }
}

export default App;
