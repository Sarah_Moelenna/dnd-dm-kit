import os
from PIL import Image
from typing import List

def compress_images(directory, images: List[str], quality):
    for image in images:
        # 1. Open the image
        img = Image.open(directory + image)
        # 2. Compressing the image
        format = "webp"

        if 'meta' in image:
            format = 'jpeg'

        img.save(directory.replace('images-to-convert', 'public')+image.replace('.jpg', f'.{format}').replace('.png', f'.{format}'),
                optimize=True,
                quality=quality,
                format=format)

promo_build_root_images = [file for file in os.listdir('promotional_site/images-to-convert') if file.endswith(('jpg', 'png' ))]
print(f"Found Root Images: {promo_build_root_images}")
compress_images('promotional_site/images-to-convert/', promo_build_root_images, 50)

promo_build_media_images = [file for file in os.listdir('promotional_site/images-to-convert/media') if file.endswith(('jpg', 'png' ))]
print(f"Found Media Images: {promo_build_media_images}")
compress_images('promotional_site/images-to-convert/media/', promo_build_media_images, 70)
