import React from 'react';
import { get_api_url } from '../shared/Config';
import { DND_DELETE } from '.././shared/DNDRequests';
import Card from 'react-bootstrap/Card'
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListImages = ({ images, refresh_function}) => {

    function delete_image_function(id){
        DND_DELETE(
            '/user-images/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(images === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="images">
            {images.map((image) => (
                <div key={image.id} className="image-item">
                    <Card>
                        { image.file_location &&
                            <Card.Img variant="top" src={get_api_url() + "/" + image.file_location} />
                        }
                        <Card.Body>
                            <p><b>Uploaded On: </b>{image.created_at}</p>
                            <DeleteConfirmationButton id={image.id} name="Image" delete_function={delete_image_function} />
                        </Card.Body>
                    </Card>
                </div>
            ))}
        </div>
    );
}

export default ListImages;