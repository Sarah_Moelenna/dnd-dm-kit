import React, { Component } from 'react';
import { get_api_url } from '../shared/Config';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Card from 'react-bootstrap/Card'
import { DND_FORM, DND_GET } from '.././shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import Form from 'react-bootstrap/Form'

const SingleImageSelection = ({ image, callback_function, exclude_api_url}) => {

    return (
        <Card>
            { image.file_location &&
                <Card.Img variant="top" src={get_api_url() + "/" + image.file_location} />
            }
            <Card.Body>
                <p><b>Uploaded On: </b>{image.created_at}</p>
                <Button onClick={() => {callback_function(exclude_api_url == true ? image.file_location : get_api_url() + "/" + image.file_location)}}>Select Image</Button>
            </Card.Body>
        </Card>
    )
}

const ListImageSelection = ({ images, callback_function, upload_image, exclude_api_url}) => {

    return (
        <div className="images">
            
            <div key="upload" className="image-item">
                <Card>
                    <Card.Body>
                        <div>
                            <Form.Label>Upload New Image</Form.Label>
                            <Form.File onChange={upload_image} />
                        </div>
                    </Card.Body>
                </Card>
            </div>
            {images.map((image) => (
                <div key={image.id} className="image-item">
                    <SingleImageSelection image={image} callback_function={callback_function} exclude_api_url={exclude_api_url}/>
                </div>
            ))}
        </div>
    );
}

class UploadImage extends Component {

    state = {
        images: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false
    }

    componentDidMount() {
        this.refresh_images(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    upload_image = (e) => {
        if(e.target.files.length == 0){
            return
        }

        this.setState({uploading: true})

        const formData = new FormData()
        formData.append('file', e.target.files[0], e.target.files[0].name)

        DND_FORM(
            '/file-upload',
            formData,
            (data) => {
                this.setState({toggle: false})
                this.props.callback(this.props.exclude_api_url == true ? data['file'] : get_api_url() + "/" + data['file'])
            },
            null
        )
    }

    select_image = (image) => {

        this.setState({toggle: false})
        this.props.callback(image)
    }

    refresh_images = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/user-images?' + stringify(params),
          (jsondata) => {
            this.setState({ images: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_images(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_images(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_images(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_images(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    render(){
        return(
            <div>
                {this.state.toggle == false &&
                    <Button onClick={this.open}>Choose Image</Button>
                }
                <Modal show={this.state.toggle} size="md" className="image-upload-modal">
                        <Modal.Header>Images</Modal.Header>
                        <Modal.Body>
                            <ListImageSelection
                                images={this.state.images}
                                callback_function={this.select_image}
                                upload_image={this.upload_image}
                                exclude_api_url={this.props.exclude_api_url}
                                />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default UploadImage;