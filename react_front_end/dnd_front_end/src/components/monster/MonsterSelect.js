import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import FilterMonster from './FilterMonster';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const ListMonsters = ({ monsters, set_sort, current_sort_by, current_sort_value, select_function, disabled_ids}) => {

    function get_sort(sort_by){
        if(current_sort_by == sort_by){
            if(current_sort_value == "DESC"){
                return (
                    <div className="clickable-div" onClick={() => {set_sort(null, null)}}>
                        <i className="fas fa-sort-down"></i>
                    </div>
                ) 
            } else {
                return (
                    <div className="clickable-div" onClick={() => {set_sort(sort_by, "DESC")}}>
                        <i className="fas fa-sort-up"></i>
                    </div>
                ) 
            }
        } else {
            return (
                <div className="clickable-div" onClick={() => {set_sort(sort_by, "ASC")}}>
                    <i className="fas fa-sort"></i>
                </div>
            )
        }
    }

    return (
        <div className="monster-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={1} md={1}>
                        <p className="card-text">
                            <b>Challenge
                                {get_sort("challenge_number")}
                            </b>
                        </p>
                    </Col>
                    <Col xs={3} md={3}>
                        <p className="card-text">
                            <b>Name
                                {get_sort("name")}
                            </b>
                        </p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text">
                            <b>Types
                                {get_sort("types")}
                            </b>
                        </p>
                    </Col>
                    <Col xs={3} md={3}>
                        <p className="card-text">
                            <b>Environment
                                {get_sort("environments")}
                            </b>
                        </p>
                    </Col>
                    <Col xs={3} md={3}>
                    </Col>
                </Row>
            </div>
            {monsters.map((monster) => (
                <div key={monster.id} className={disabled_ids.includes(monster.id) ? "monster-item disabled" : "monster-item"}>
                    <Row>
                        <Col xs={1} md={1}>
                        <p className="card-text">{monster.challenge.split(" ")[0]}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <p className="card-text">{monster.name}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{monster.types}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <p className="card-text">{monster.environments}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            {!disabled_ids.includes(monster.id) &&
                                <Button onClick={() => {select_function(monster.id, monster.name, monster.dexterity, monster.xp, monster.image)}}>Select Monster</Button>
                            }
                            <a className="btn btn-primary" target="_blank" href={"/monster/" + monster.id}>View</a>
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

class MonsterSelect extends Component {

    state = {
        monsters: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false
    }

    componentDidMount() {
        this.refresh_monsters(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    select_monster = (monster) => {

        this.setState({toggle: false})
        this.props.callback(monster)
    }

    refresh_monsters = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        params['results_per_page'] = 10
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/monsters?' + stringify(params),
          (jsondata) => {
            this.setState({ monsters: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_monsters(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
        this.setState(
          {sort_by: sort_by, sort_value: sort_value},
          this.refresh_monsters(this.state.page, this.state.filters, sort_by, sort_value)
        )
      };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_monsters(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_monsters(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_monsters(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id, name, dexterity, xp, image) => {

        this.props.select_function(id, name, dexterity, xp, image)
        this.setState({toggle: false})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>{this.props.override_button != undefined ? this.props.override_button : "Choose Monster"}</Button>
                }
                <Modal show={this.state.toggle} size="md" className="monster-select-modal select-modal">
                        <Modal.Header>Monsters</Modal.Header>
                        <Modal.Body>
                            <FilterMonster filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                            <ListMonsters
                                monsters={this.state.monsters}
                                set_sort={this.set_sort}
                                current_sort_by={this.state.sort_by}
                                current_sort_value={this.state.sort_value}
                                select_function={this.select_function}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default MonsterSelect;