import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MonsterShortCodeGenerator from './MonsterShortCodeGenerator'
import { DND_DELETE } from '.././shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import Button from 'react-bootstrap/Button'
import {Link} from "react-router-dom";


const ListMonsterItem = ({ monsters, view_monster_function, refresh_function, edit_function, set_sort, current_sort_by, current_sort_value}) => {

    function delete_function(id){
        DND_DELETE(
            '/monster/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    function get_sort(sort_by){
        if(current_sort_by == sort_by){
            if(current_sort_value == "DESC"){
                return (
                    <div className="clickable-div" onClick={() => {set_sort(null, null)}}>
                        <i className="fas fa-sort-down"></i>
                    </div>
                ) 
            } else {
                return (
                    <div className="clickable-div" onClick={() => {set_sort(sort_by, "DESC")}}>
                        <i className="fas fa-sort-up"></i>
                    </div>
                ) 
            }
        } else {
            return (
                <div className="clickable-div" onClick={() => {set_sort(sort_by, "ASC")}}>
                    <i className="fas fa-sort"></i>
                </div>
            )
        }
    }

    if(monsters === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="monster-items">
            <div className="headers">
                <Row>
                    <Col xs={1} md={1}>
                        <p className="card-text">
                            <b>Challenge
                                {get_sort("challenge_number")}
                            </b>
                        </p>
                    </Col>
                    <Col xs={3} md={3}>
                        <p className="card-text">
                            <b>Name
                                {get_sort("name")}
                            </b>
                        </p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text">
                            <b>Types
                                {get_sort("types")}
                            </b>
                        </p>
                    </Col>
                    <Col xs={3} md={3}>
                        <p className="card-text">
                            <b>Environment
                                {get_sort("environments")}
                            </b>
                        </p>
                    </Col>
                    <Col xs={3} md={3}>
                    </Col>
                </Row>
                <hr/>
            </div>
            {monsters.map((monster) => (
                <div key={monster.id} className="monster-item">
                    <Row>
                        <Col xs={1} md={1}>
                        <p className="card-text">{monster.challenge.split(" ")[0]}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <p className="card-text">{monster.name}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{monster.types}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <p className="card-text">{monster.environments}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <Link className="btn btn-primary" to={"/monster/" + monster.id} onClick={() => view_monster_function(monster.id)}>
                                View
                            </Link >
                            { monster.can_edit == true &&
                                <Button className="btn btn-primary" onClick={() => {edit_function(monster.id)}}>Edit</Button>
                            }
                            { monster.can_edit == true &&
                                <DeleteConfirmationButton id={monster.id} override_button="Delete" name="Monster" delete_function={delete_function} />
                            }
                            <MonsterShortCodeGenerator monster={monster} />
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListMonsterItem;