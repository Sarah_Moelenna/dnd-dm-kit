import React, { Component } from 'react';
import MonsterSelect from './MonsterSelect';
import { DND_GET } from '.././shared/DNDRequests';
import MDEditor from '@uiw/react-md-editor';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Languages, Environments, Resistances, Atrributes, Conditions, Types, Sizes, Skills, Senses, Challenge_Ratings } from './Data';
import ListSelector from '../shared/ListSelector';
import ListWithNotes from '../shared/ListWithNotes';
import { DND_POST, DND_PUT } from '.././shared/DNDRequests';
import Button from 'react-bootstrap/Button';
import UploadImage from '../images/UploadImage';
import BackConfirmationButton from '../shared/BackConfirmationButton';

class CreateMonster extends Component {

    state = {
        name: null,
        type: null,
        size: null,
        challenge_rating: null,
        special_traits: null,
        actions: null,
        reactions: null,
        bonus: null,
        legendary: null,
        mythic: null,
        lair: null,
        ac: null,
        armour: null,
        perception: null,
        str: null,
        dex: null,
        con: null,
        wis: null,
        int: null,
        cha: null,
        hit_die_count: null,
        hit_die: null,
        hit_die_modifier: null,
        hit_points: null,
        walk: 0,
        fly: 0,
        swim: 0,
        climb: 0,
        burrow: 0,
        languages: "",
        environments: "",
        resistances: "",
        condition_immunities: "",
        saving_throws: "",
        image: null,
        senses: {},
        skills: {},

        has_changes: false,
    }

    componentWillMount() {
        if(this.props.edit_monster_id != undefined && this.props.edit_monster_id != null){
            this.copy_monster(this.props.edit_monster_id)
        }
    };

    copy_monster = (monster_id) => {

        DND_GET(
            '/monster/' + monster_id,
            (response) => {
                this.populate_with_monster_data(response)
            },
            null
        )
    };

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value, has_changes: true });
    }

    update_text = (text, type) => {
        if(type =='SpecialTraits'){
            this.setState({special_traits: text, has_changes: true})
        }
        if(type =='Actions'){
            this.setState({actions: text, has_changes: true})
        }
        if(type =='Reactions'){
            this.setState({reactions: text, has_changes: true})
        }
        if(type =='Bonus'){
            this.setState({bonus: text, has_changes: true})
        }
        if(type =='Legendary'){
            this.setState({legendary: text, has_changes: true})
        }
        if(type =='Mythic'){
            this.setState({mythic: text, has_changes: true})
        }
        if(type =='Lair'){
            this.setState({lair: text, has_changes: true})
        }
    }

    handleSubmit = (e) => {

        const getSpeed = () =>{
            var speeds = []
            speeds.push(this.state.walk + " ft")
            if(this.state.fly > 0){
                speeds.push("fly " + this.state.fly + " ft")
            }
            if(this.state.swim > 0){
                speeds.push("swim " + this.state.swim + " ft")
            }
            if(this.state.climb > 0){
                speeds.push("climb " + this.state.climb + " ft")
            }
            if(this.state.burrow > 0){
                speeds.push("burrow " + this.state.burrow + " ft")
            }
            return speeds.join(", ")
        }

        const getSkills = () =>{
            var skills = []
            var keys = Object.keys(this.state.skills)
            for(var i = 0; i < keys.length; i++){
                if(this.state.skills[keys[i]] != undefined){
                    skills.push(keys[i] + " +" + this.state.skills[keys[i]])
                }
            }
            var result = skills.join(", ")
            return result != "" ? result : null
        }

        const getSenses = () =>{
            var senses = []
            var keys = Object.keys(this.state.senses)
            for(var i = 0; i < keys.length; i++){
                if(this.state.senses[keys[i]] != undefined){
                    senses.push(keys[i] + " " + this.state.senses[keys[i]] + " ft")
                }
            }
            senses.push("Passive Perception " + this.state.perception)
            var result = senses.join(", ")
            return result != "" ? result : null
        }

        const getDamage = (suffix) =>{
            var adjustments = []

            for(var i = 0; i < Resistances.length; i++){
                if(Resistances[i].toUpperCase().includes(suffix.toUpperCase())){
                    if(this.state.resistances.includes(Resistances[i])){
                        adjustments.push(Resistances[i].replace(suffix, ""))
                    }
                }
            }
            var result = adjustments.join("; ")
            return result != "" ? result : null
        }

        const getSavingThrows = () =>{
            var saving_throws = []
            var values = this.state.saving_throws.split(",")
            

            for(var i = 0; i < values.length; i++){
                var modifier = 0
                if(values[i] == "STR"){
                    modifier = Math.floor(this.state.str/2) - 5
                } else if(values[i] == "DEX"){
                    modifier = Math.floor(this.state.dex/2) - 5
                } else if(values[i] == "CON"){
                    modifier = Math.floor(this.state.con/2) - 5
                } else if(values[i] == "WIS"){
                    modifier = Math.floor(this.state.wis/2) - 5
                } else if(values[i] == "CHA"){
                    modifier = Math.floor(this.state.cha/2) - 5
                } else if(values[i] == "INT"){
                    modifier = Math.floor(this.state.int/2) - 5
                }

                modifier = modifier + parseInt(Challenge_Ratings[this.state.challenge_rating]["proficiency"].replace("+",""))

                saving_throws.push(values[i] + " +" + modifier)
            }
            return saving_throws.join(", ")
        }

        var data = {
            name: this.state.name,
            image: this.state.image,
            actions: this.state.actions,
            legendary_actions: this.state.legendary,
            reactions: this.state.reactions,
            more_info: this.state.lair,
            description: this.state.special_traits,
            mythic: this.state.mythic,
            bonus: this.state.bonus,
            armour_class: this.state.ac + " " + this.state.armour,
            hit_points: this.state.hit_points + " (" + this.state.hit_die_count + this.state.hit_die + ((this.state.hit_die_modifier == undefined || this.state.hit_die_modifier == 0)? "" : " + " + this.state.hit_die_modifier) + ")",
            speed: getSpeed(),
            saving_throws: getSavingThrows(),
            skills: getSkills(),
            condition_immunities: this.state.condition_immunities != "" ? this.state.condition_immunities: null,
            damage_immunities: getDamage(" - Immunity"),
            damage_resistances: getDamage(" - Resistance"),
            damage_vulnerabilities: getDamage(" - Vulnerability"),
            senses: getSenses(),
            languages: this.state.languages != "" ? this.state.languages.replaceAll(",", ", ") : null,
            types: this.state.size + (this.state.size != null && this.state.size != '' ? " " : "") + this.state.type,
            environments: this.state.environments != "" ? this.state.environments.replaceAll(",", ", ") : null,
            strength: this.state.str,
            dexterity: this.state.dex,
            constitution: this.state.con,
            intelligence: this.state.int,
            wisdom: this.state.wis,
            charisma: this.state.cha,
            challenge: this.state.challenge_rating + " (" + Challenge_Ratings[this.state.challenge_rating]["xp"] + " XP)",
            challenge_number: parseInt(Challenge_Ratings[this.state.challenge_rating]["number"]),
            proficiency_bonus: Challenge_Ratings[this.state.challenge_rating]["proficiency"],
        }

        if(this.props.edit_monster_id != undefined && this.props.edit_monster_id != null){
            DND_PUT(
                '/monster/' + this.props.edit_monster_id,
                {"data": data},
                (response) => {
                    this.props.close_creating_fuction()
                },
                null
            )
        } else {
            DND_POST(
                '/monsters',
                {"data": data},
                (response) => {
                    this.props.close_creating_fuction()
                },
                null
            )
        }

        e.preventDefault();
    }

    get_list_items_from_string = (string, list, first_only) => {
        if(string == undefined || string == null){
            return "";
        }

        var uppercased_value = string.toUpperCase()
        var found_values = []
        for(var i = 0; i < list.length; i++){
            if(uppercased_value.includes(list[i].toUpperCase())){
                found_values.push(list[i])
            }
        }
        if (first_only != undefined && first_only == true){
            if(found_values.length > 0){
                return found_values[0]
            }
            return null
        }
        if(found_values.length > 0){
            return found_values.join(",")
        } else {
            return ""
        }
    }

    get_monster_hitpoints = (string) => {
        var average = null;
        var count = 1;
        var die = null;
        var modifier = 0;
        var values = string.replaceAll("(", "").replaceAll(")", "").replaceAll(" + ", " ").split(' ')
        average = values[0]
        if (values.length > 0){
            count = values[1].split("d")[0]
            die = "d" + values[1].split("d")[1]
        }
        if (values.length > 1){
            modifier = values[2]
        }
        return [average, count, die, modifier]
    }

    get_monster_speeds = (string) => {
        var walk = 0;
        var fly = 0;
        var climb = 0;
        var burrow = 0;
        var swim = 0;
        var values = string.split(',')
        for(var i = 0; i < values.length; i++){
            var items = values[i].trim().split(" ")
            if(values[i].includes("swim")){
                swim = parseInt(items[1])
            } else if(values[i].includes("fly")){
                fly = parseInt(items[1])
            } else if(values[i].includes("climb")){
                climb = parseInt(items[1])
            } else if(values[i].includes("burrow")){
                burrow = parseInt(items[1])
            } else {
                walk = parseInt(items[0])
            }
        }
        return [walk, fly, climb, burrow, swim]
    }

    get_monster_senses = (string) => {
        if(string == undefined || string == null){
            return "";
        }

        var senses = {}
        var perception = 0;

        var values = string.split(',')
        for(var i = 0; i < values.length; i++){
            var value = parseInt(values[i].trim().split(" ")[1])
            for(var j = 0; j < Senses.length; j++){
                if(values[i].includes(Senses[j])){
                    senses[Senses[j]] = value
                }
            }
            if(values[i].includes("Perception"[j])){
                perception = parseInt(values[i].trim().split(" ")[2])
            }
        }
        return [perception, senses]
    }

    get_monster_skils = (string) => {
        if(string == undefined || string == null){
            return "";
        }

        var skills = {}

        var values = string.split(',')
        for(var i = 0; i < values.length; i++){
            var value = parseInt(values[i].trim().split(" ")[1])
            for(var j = 0; j < Skills.length; j++){
                if(values[i].includes(Skills[j])){
                    skills[Skills[j]] = value
                }
            }
        }
        return skills
    }

    set_image = (image) => {
        this.setState({image: image, has_changes: true})
    }

    populate_with_monster_data = (monster) => {
        var hit_point_data = this.get_monster_hitpoints(monster.hit_points)
        var speeds = this.get_monster_speeds(monster.speed)
        var sense_data = this.get_monster_senses(monster.senses)
        var resistances = ""
        if(monster.damage_immunities != null){
            resistances = resistances + this.get_list_items_from_string(monster.damage_immunities.replaceAll(";", " - Immunity") + " - Immunity", Resistances, false);
        }
        if(monster.damage_resistances != null){
            resistances = resistances + this.get_list_items_from_string(monster.damage_resistances.replaceAll(";", " - Resistance") + " - Resistance", Resistances, false);
        }
        if(monster.damage_vulnerabilities != null){
            resistances = resistances + this.get_list_items_from_string(monster.damage_vulnerabilities.replaceAll(";", " - Vulnerability") + " - Vulnerability", Resistances, false);
        }

        var name = monster.name
        if(this.props.edit_monster_id == undefined || this.props.edit_monster_id == null){
            name = "Copy of " + name
        }
        this.setState({
            name: name,
            challenge_rating: monster.challenge.split(' ')[0],
            type: this.get_list_items_from_string(monster.types, Types, true),
            size: this.get_list_items_from_string(monster.types, Sizes, true),
            ac: monster.armour_class.split(' ')[0],
            armour: monster.armour_class.split(' ').slice(1).join(" "),
            cha: monster.charisma,
            condition_immunities: this.get_list_items_from_string(monster.condition_immunities, Conditions),
            con: monster.constitution,
            resistances: resistances,
            actions: monster.actions != null ? monster.actions.replaceAll("<br>", "\n") : null,
            special_traits: monster.description != null ? monster.description.replaceAll("<br>", "\n") : null,
            dex: monster.dexterity,
            environments: this.get_list_items_from_string(monster.environments, Environments),
            hit_points: hit_point_data[0],
            hit_die_count: hit_point_data[1],
            hit_die: hit_point_data[2],
            hit_die_modifier: hit_point_data[3],
            image: monster.image,
            int: monster.intelligence,
            languages: this.get_list_items_from_string(monster.languages, Languages),
            legendary: monster.legendary_actions != null ? monster.legendary_actions.replaceAll("<br>", "\n") : null,
            lair: monster.more_info != null ? monster.more_info.replaceAll("<br>", "\n") : null,
            reactions: monster.reactions != null ? monster.reactions.replaceAll("<br>", "\n") : null,
            mythic: monster.mythic != null ? monster.mythic.replaceAll("<br>", "\n") : null,
            bonus: monster.bonus != null ? monster.bonus.replaceAll("<br>", "\n") : null,
            saving_throws: this.get_list_items_from_string(monster.saving_throws, Atrributes),
            perception: sense_data[0],
            skills: this.get_monster_skils(monster.skills),
            senses: sense_data[1],
            walk: speeds[0],
            fly: speeds[1],
            climb: speeds[2],
            burrow: speeds[3],
            swim: speeds[4],
            str: monster.strength,
            wis: monster.wisdom
        })
    }
            
    render(){

        return(
            <div className="monster-create">
                {(this.props.edit_monster_id == undefined || this.props.edit_monster_id == null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Create Monster</h2>
                        <MonsterSelect select_function={this.copy_monster} override_button="Copy Existing Monster"/>
                    </div>
                }
                {(this.props.edit_monster_id != undefined && this.props.edit_monster_id != null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Edit Monster</h2>
                    </div>
                }
                <Form onSubmit={this.handleSubmit} className="create-campaign">
                    <Row>
                        <Col xs={12}>
                            <h3>Basic Information</h3>
                        </Col>
                        <Col xs={6}>
                            <Row>
                                <Col xs={6} md={6}>
                                    <Form.Group controlId="formBasicName">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control value={this.state.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                                    </Form.Group>
                                </Col>

                                <Col xs={6} md={6}>
                                    <Form.Group controlId="formBasicType">
                                        <Form.Label>Type</Form.Label>
                                        <Form.Control name="type" as="select" onChange={this.handleChange} custom>
                                                <option selected="true" value="">-</option>
                                                <option selected={this.state.type == "Aberration"} value="Aberration">Aberration</option>
                                                <option selected={this.state.type == "Beast"} value="Beast">Beast</option>
                                                <option selected={this.state.type == "Celestial"} value="Celestial">Celestial</option>
                                                <option selected={this.state.type == "Construct"} value="Construct">Construct</option>
                                                <option selected={this.state.type == "Dragon"} value="Dragon">Dragon</option>
                                                <option selected={this.state.type == "Elemental"} value="Elemental">Elemental</option>
                                                <option selected={this.state.type == "Fey"} value="Fey">Fey</option>
                                                <option selected={this.state.type == "Fiend"} value="Fiend">Fiend</option>
                                                <option selected={this.state.type == "Giant"} value="Giant">Giant</option>
                                                <option selected={this.state.type == "Humanoid"} value="Humanoid">Humanoid</option>
                                                <option selected={this.state.type == "Monstrosity"} value="Monstrosity">Monstrosity</option>
                                                <option selected={this.state.type == "Ooze"} value="Ooze">Ooze</option>
                                                <option selected={this.state.type == "Plant"} value="Plant">Plant</option>
                                                <option selected={this.state.type == "Undead"} value="Undead">Undead</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>

                                <Col xs={6} md={6}>
                                    <Form.Group controlId="formBasicSize">
                                        <Form.Label>Size</Form.Label>
                                        <Form.Control name="size" as="select" onChange={this.handleChange} custom>
                                                <option selected="true" value="">-</option>
                                                <option selected={this.state.size == "Tiny"} value="Tiny">Tiny</option>
                                                <option selected={this.state.size == "Small"} value="Small">Small</option>
                                                <option selected={this.state.size == "Medium"} value="Medium">Medium</option>
                                                <option selected={this.state.size == "Large"} value="Large">Large</option>
                                                <option selected={this.state.size == "Huge"} value="Huge">Huge</option>
                                                <option selected={this.state.size == "Gargantuan"} value="Gargantuan">Gargantuan</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>

                                <Col xs={6} md={6}>
                                    <Form.Group controlId="formBasicChallenge">
                                        <Form.Label>Challenge Rating</Form.Label>
                                        <Form.Control name="challenge_rating" as="select" onChange={this.handleChange} custom>
                                                    <option selected="true" value="">-</option>
                                                    <option selected={this.state.challenge_rating == "0"} value="0">0</option>
                                                    <option selected={this.state.challenge_rating == "1/8"} value="1/8">1/8</option>
                                                    <option selected={this.state.challenge_rating == "1/4"} value="1/4">1/4</option>
                                                    <option selected={this.state.challenge_rating == "1/2"} value="1/2">1/2</option>
                                                    <option selected={this.state.challenge_rating == "1"} value="1">1</option>
                                                    <option selected={this.state.challenge_rating == "2"} value="2">2</option>
                                                    <option selected={this.state.challenge_rating == "3"} value="3">3</option>
                                                    <option selected={this.state.challenge_rating == "4"} value="4">4</option>
                                                    <option selected={this.state.challenge_rating == "5"} value="5">5</option>
                                                    <option selected={this.state.challenge_rating == "6"} value="6">6</option>
                                                    <option selected={this.state.challenge_rating == "7"} value="7">7</option>
                                                    <option selected={this.state.challenge_rating == "8"} value="8">8</option>
                                                    <option selected={this.state.challenge_rating == "9"} value="9">9</option>
                                                    <option selected={this.state.challenge_rating == "10"} value="10">10</option>
                                                    <option selected={this.state.challenge_rating == "11"} value="11">11</option>
                                                    <option selected={this.state.challenge_rating == "12"} value="12">12</option>
                                                    <option selected={this.state.challenge_rating == "13"} value="13">13</option>
                                                    <option selected={this.state.challenge_rating == "14"} value="14">14</option>
                                                    <option selected={this.state.challenge_rating == "15"} value="15">15</option>
                                                    <option selected={this.state.challenge_rating == "16"} value="16">16</option>
                                                    <option selected={this.state.challenge_rating == "17"} value="17">17</option>
                                                    <option selected={this.state.challenge_rating == "18"} value="18">18</option>
                                                    <option selected={this.state.challenge_rating == "19"} value="19">19</option>
                                                    <option selected={this.state.challenge_rating == "20"} value="20">20</option>
                                                    <option selected={this.state.challenge_rating == "21"} value="21">21</option>
                                                    <option selected={this.state.challenge_rating == "22"} value="22">22</option>
                                                    <option selected={this.state.challenge_rating == "23"} value="23">23</option>
                                                    <option selected={this.state.challenge_rating == "24"} value="24">24</option>
                                                    <option selected={this.state.challenge_rating == "25"} value="25">25</option>
                                                    <option selected={this.state.challenge_rating == "26"} value="26">26</option>
                                                    <option selected={this.state.challenge_rating == "27"} value="27" >27</option>
                                                    <option selected={this.state.challenge_rating == "28"} value="28">28</option>
                                                    <option selected={this.state.challenge_rating == "29"} value="29">29</option>
                                                    <option selected={this.state.challenge_rating == "30"} value="30">30</option>
                                            </Form.Control>
                                    </Form.Group>
                                </Col>
                            </Row>
                        </Col>

                        <Col xs={6}>
                            <img src={this.state.image}/>
                            <UploadImage callback={this.set_image}/>
                        </Col>

                        <Col xs={12}>
                            <hr/>
                            <h3>Descriptions</h3>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicSpecialtraits">
                                <Form.Label>SPECIAL TRAITS DESCRIPTION</Form.Label>
                                <MDEditor
                                    value={this.state.special_traits}
                                    preview="edit"
                                    onChange={(text) => {this.update_text(text, "SpecialTraits")}}
                                />
                            </Form.Group>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicActions">
                                <Form.Label>ACTIONS DESCRIPTION</Form.Label>
                                <MDEditor
                                    value={this.state.actions}
                                    preview="edit"
                                    onChange={(text) => {this.update_text(text, "Actions")}}
                                />
                            </Form.Group>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicReactions">
                                <Form.Label>REACTIONS DESCRIPTION</Form.Label>
                                <MDEditor
                                    value={this.state.reactions}
                                    preview="edit"
                                    onChange={(text) => {this.update_text(text, "Reactions")}}
                                />
                            </Form.Group>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicBonus">
                                <Form.Label>BONUS ACTIONS DESCRIPTION</Form.Label>
                                <MDEditor
                                    value={this.state.bonus}
                                    preview="edit"
                                    onChange={(text) => {this.update_text(text, "Bonus")}}
                                />
                            </Form.Group>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicLegendary">
                                <Form.Label>LEGENDARY ACTIONS DESCRIPTION</Form.Label>
                                <MDEditor
                                    value={this.state.legendary}
                                    preview="edit"
                                    onChange={(text) => {this.update_text(text, "Legendary")}}
                                />
                            </Form.Group>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicMythic">
                                <Form.Label>MYTHIC ACTIONS DESCRIPTION</Form.Label>
                                <MDEditor
                                    value={this.state.mythic}
                                    preview="edit"
                                    onChange={(text) => {this.update_text(text, "Mythic")}}
                                />
                            </Form.Group>
                        </Col>

                        <Col xs={12} md={12}>
                            <Form.Group controlId="formBasicLair">
                                <Form.Label>More Info</Form.Label>
                                <MDEditor
                                    value={this.state.lair}
                                    preview="edit"
                                    onChange={(text) => {this.update_text(text, "Lair")}}
                                />
                            </Form.Group>
                        </Col>

                        <Col xs={12}>
                            <hr/>
                            <h3>Stats</h3>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicAC">
                                <Form.Label>Armour Class</Form.Label>
                                <Form.Control value={this.state.ac} required name="ac" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicAC">
                                <Form.Label>Armour Type</Form.Label>
                                <Form.Control value={this.state.armour} required name="armour" type="text" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicPerception">
                                <Form.Label>Passive Perception</Form.Label>
                                <Form.Control value={this.state.perception} required name="perception" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicHitDieCount">
                                <Form.Label>Hit Points Die Count</Form.Label>
                                <Form.Control value={this.state.hit_die_count} required name="hit_die_count" type="number" min={1} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicHitDie">
                                <Form.Label>Hit Points Die Value</Form.Label>
                                <Form.Control name="hit_die" as="select" onChange={this.handleChange} custom>
                                        <option selected="true" value="">-</option>
                                        <option selected={this.state.hit_die == "d4"} value="d4">d4</option>
                                        <option selected={this.state.hit_die == "d6"} value="d6">d6</option>
                                        <option selected={this.state.hit_die == "d8"} value="d8">d8</option>
                                        <option selected={this.state.hit_die == "d10"} value="d10">d10</option>
                                        <option selected={this.state.hit_die == "d12"} value="d12">d12</option>
                                        <option selected={this.state.hit_die == "d20"} value="d20">d20</option>
                                        <option selected={this.state.hit_die == "d100"} value="d100">d100</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>

                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicHitModifier">
                                <Form.Label>Hit Points Modifier</Form.Label>
                                <Form.Control value={this.state.hit_die_modifier} required name="hit_die_modifier" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicHitPoints">
                                <Form.Label>Average Hit Points</Form.Label>
                                <Form.Control value={this.state.hit_points} required name="hit_points" type="number" min={1} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicSTR">
                                <Form.Label>Str Score</Form.Label>
                                <Form.Control value={this.state.str} required name="str" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicDex">
                                <Form.Label>Dex Score</Form.Label>
                                <Form.Control value={this.state.dex} required name="dex" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicCon">
                                <Form.Label>Con Score</Form.Label>
                                <Form.Control value={this.state.con} required name="con" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicInt">
                                <Form.Label>Int Score</Form.Label>
                                <Form.Control value={this.state.int} required name="int" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicWis">
                                <Form.Label>Wis Score</Form.Label>
                                <Form.Control value={this.state.wis} required name="wis" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicCha">
                                <Form.Label>Cha Score</Form.Label>
                                <Form.Control value={this.state.cha} required name="cha" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={12}>
                            <hr/>
                            <h3>Movement</h3>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicWalk">
                                <Form.Label>Walk Speed</Form.Label>
                                <Form.Control value={this.state.walk} required name="walk" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicBurrow">
                                <Form.Label>Burrow Speed</Form.Label>
                                <Form.Control value={this.state.burrow} required name="burrow" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicClimb">
                                <Form.Label>Climb Speed</Form.Label>
                                <Form.Control value={this.state.climb} required name="climb" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicSwim">
                                <Form.Label>Swim Speed</Form.Label>
                                <Form.Control value={this.state.swim} required name="swim" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicFly">
                                <Form.Label>Fly Speed</Form.Label>
                                <Form.Control value={this.state.fly} required name="fly" type="number" min={0} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={12}>
                            <hr/>
                            <h3>Other</h3>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicFly">
                                <Form.Label>Languages</Form.Label>
                                <ListSelector input_value={this.state.languages} items={Languages} onChange={(data) => {this.setState({languages: data, has_changes: true})}} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicFly">
                                <Form.Label>Environments</Form.Label>
                                <ListSelector input_value={this.state.environments} items={Environments} onChange={(data) => {this.setState({environments: data, has_changes: true})}} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicFly">
                                <Form.Label>Saving Throw Proficiencies</Form.Label>
                                <ListSelector input_value={this.state.saving_throws} items={Atrributes} onChange={(data) => {this.setState({saving_throws: data, has_changes: true})}} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicFly">
                                <Form.Label>Condition Immunities</Form.Label>
                                <ListSelector input_value={this.state.condition_immunities} items={Conditions} onChange={(data) => {this.setState({condition_immunities: data, has_changes: true})}} />
                            </Form.Group>
                        </Col>

                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicFly">
                                <Form.Label>Damage Adjustments</Form.Label>
                                <ListSelector input_value={this.state.resistances} items={Resistances} onChange={(data) => {this.setState({resistances: data, has_changes: true})}} />
                            </Form.Group>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicFly">
                                <Form.Label>Skills</Form.Label>
                                <ListWithNotes input_value={this.state.skills} items={Skills} onChange={(data) => {this.setState({skills: data, has_changes: true})}} />
                            </Form.Group>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicFly">
                                <Form.Label>Senses</Form.Label>
                                <ListWithNotes input_value={this.state.senses} items={Senses} onChange={(data) => {this.setState({senses: data, has_changes: true})}} />
                            </Form.Group>
                        </Col>

                        <Col xs={12} md={12}>
                            <Button variant="primary" type="submit">
                                Save Monster
                            </Button>
                        </Col>

                    </Row>
                </Form>
            
            </div>
        )

    };
}

export default CreateMonster;