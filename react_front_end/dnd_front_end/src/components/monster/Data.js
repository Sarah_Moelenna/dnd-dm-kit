export const Levels = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20
]

export const LevelNames = {
    1: "1st",
    2: "2nd",
    3: "3rd",
    4: "4th",
    5: "5th",
    6: "6th",
    7: "7th",
    8: "8th",
    9: "9th",
    10: "10th",
    11: "11th",
    12: "12th",
    13: "13th",
    14: "14th",
    15: "15th",
    16: "16th",
    17: "17th",
    18: "18th",
    19: "19th",
    20: "20th"
}

export const LevelAdvancement = {
    1: {experience: 0, proficiency_bonus: 2},
    2: {experience: 300, proficiency_bonus: 2},
    3: {experience: 900, proficiency_bonus: 2},
    4: {experience: 2700, proficiency_bonus: 2},
    5: {experience: 6500, proficiency_bonus: 3},
    6: {experience: 14000, proficiency_bonus: 3},
    7: {experience: 23000, proficiency_bonus: 3},
    8: {experience: 34000, proficiency_bonus: 3},
    9: {experience: 48000, proficiency_bonus:4},
    10: {experience: 64000, proficiency_bonus: 4},
    11: {experience: 85000, proficiency_bonus: 4},
    12: {experience: 100000, proficiency_bonus: 4},
    13: {experience: 120000, proficiency_bonus: 5},
    14: {experience: 140000, proficiency_bonus: 5},
    15: {experience: 165000, proficiency_bonus: 5},
    16: {experience: 195000, proficiency_bonus: 5},
    17: {experience: 225000, proficiency_bonus: 6},
    18: {experience: 265000, proficiency_bonus: 6},
    19: {experience: 305000, proficiency_bonus: 6},
    20: {experience: 355000, proficiency_bonus: 6}
}

export const Environments = [
    "Arctic",
    "Coastal",
    "Desert",
    "Forest",
    "Grassland",
    "Hill",
    "Mountain",
    "Swamp",
    "Underdark",
    "Underwater",
    "Urban"
]

export const Types = [
    "Aberration",
    "Beast",
    "Celestial",
    "Construct",
    "Dragon",
    "Elemental",
    "Fey",
    "Fiend",
    "Giant",
    "Humanoid",
    "Monstrosity",
    "Ooze",
    "Plant",
    "Undead"
]

export const Sizes = [
    "Tiny",
    "Small",
    "Medium",
    "Large",
    "Huge",
    "Gargantuan"
]

export const Senses = [
    "Blindsight",
    "Darkvision",
    "Tremorsense",
    "Truesight",
]

export const Languages = [
    "Aarakocra",
    "Abyssal",
    "All",
    "Aquan",
    "Auran",
    "Blink Dog",
    "Bothii",
    "Bullywug",
    "Celestial",
    "Common",
    "Daelkyr",
    "Deep Speech",
    "Draconic",
    "Druidic",
    "Dwarvish",
    "Elvish",
    "Giant",
    "Giant Eagle",
    "Giant Elk",
    "Giant Owl",
    "Gith",
    "Gnoll",
    "Gnomish",
    "Goblin",
    "Grell",
    "Grippli",
    "Grung",
    "Halfling",
    "Hook Horror",
    "Ice Toad",
    "Ignan",
    "Infernal",
    "Ixitxachitl",
    "Kraul",
    "Leonin",
    "Loxodon",
    "Marquesian",
    "Minotaur",
    "Modron",
    "Naush",
    "Netherese",
    "Olman",
    "Orc",
    "Otyugh",
    "Primordial",
    "Quori",
    "Riedran",
    "Sahuagin",
    "Skitterwidget",
    "Slaad",
    "Sphinx",
    "Sylvan",
    "Telepathy",
    "Terran",
    "Thayan",
    "Thieves' Cant",
    "Thri-kreen",
    "Tlincalli",
    "Troglodyte",
    "Umber Hulk",
    "Undercommon",
    "Vedalken",
    "Vegepygmy",
    "Winter Wolf",
    "Worg",
    "Yeti",
    "Yikaria",
    "Zemnian"
]

export const Skills = [
    "Acrobatics",
    "Animal Handling",
    "Arcana",
    "Athletics",
    "Deception",
    "History",
    "Insight",
    "Intimidation",
    "Investigation",
    "Medicine",
    "Nature",
    "Perception",
    "Performance",
    "Persuasion",
    "Religion",
    "Sleight of Hand",
    "Stealth",
    "Survival"
]

export const SpellConditionType = {
    1: "Apply",
    2: "Remove",
    3: "Supress"
}

export const SpellLevels = {
    "Cantrip": 0,
    "1st": 1,
    "2nd": 2,
    "3rd": 3,
    "4th": 4,
    "5th": 5,
    "6th": 6,
    "7th": 7,
    "8th": 8,
    "9th": 9,
}

export const SpellComponents = {
    "V": 1,
    "S": 2,
    "M": 3,
}

export const SpellDurationType = [
    "Concentration",
    "Instantaneous",
    "Special",
    "Time",
    "Until Dispelled",
    "Until Dispelled or Triggered"
]

export const SpellActivationTypes = {
    "Action": 1,
    "Bonus Action": 3,
    "Hour": 7,
    "Minute": 6,
    "No Action": 2,
    "Reaction": 4,
    "Special": 8
}

export const SpellRangeTypes = [
    "Self",
    "Touch",
    "Ranged",
    "Sight",
    "Unlimited"
]

export const SpellAoETypes = [
    "Cone",
    "Cube",
    "Cylinder",
    "Line",
    "Sphere",
    "Square",
    "Square Feet"
]

export const SpellSchools = [
    "Abjuration",
    "Transmutation",
    "Conjuration",
    "Enchantment",
    "Evocation",
    "Divination",
    "Illusion",
    "Necromancy"
]

export const SpellTags = [
    "Banishment",
    "Buff",
    "Charmed",
    "Combat",
    "Communication",
    "Compulsion",
    "Control",
    "Creation",
    "Damage",
    "Debuff",
    "Deception",
    "Detection",
    "Dunamancy",
    "Environment",
    "Exploration",
    "Foreknowledge",
    "Healing",
    "Movement",
    "Negation",
    "Psionic",
    "Scrying",
    "Shapechanging",
    "Social",
    "Special",
    "Summoning",
    "Teleportation",
    "Utility",
    "Warding"
]

export const ItemTags = [
    "Artificer",
    "Bane",
    "Banishment",
    "Bard",
    "Belt",
    "Buff",
    "Combat",
    "Communication",
    "Consumable",
    "Container",
    "Control",
    "Creation",
    "Cursed",
    "Damage",
    "Debuff",
    "Deception",
    "Detection",
    "divination",
    "EldritchMachine",
    "enchantment",
    "evocation",
    "Exploration",
    "Eyewear",
    "Focus",
    "Footwear",
    "Handwear",
    "Headwear",
    "Healing",
    "Instrument",
    "Jewelry",
    "Movement",
    "Necklace",
    "NegatesDifficultTerrain",
    "Outerwear",
    "Scrying",
    "Sentient",
    "Shapechanging",
    "Social",
    "SubclassFeature",
    "Summoning",
    "Tag",
    "Tags",
    "Teleportation",
    "transmutation",
    "Utility",
    "Warding",
    "Warm",
    "Wristwear"
]

export const ItemRarity = [
    "Artifact",
    "Common",
    "Legendary",
    "Rare",
    "Uncommon",
    "Unknown Rarity",
    "Varies",
    "Very Rare"
]

export const ItemBaseType = [
    "Item",
    "Weapon",
    "Armour",
]

export const ItemType = [
    "Gear",
    "Wondrous item",
    "Rod",
    "Scroll",
    "Staff",
    "Wand",
    "Ring",
    "Potion",
]

export const ItemArmourDexBonus = [
    "Full Modifier",
    "Max 2",
    "None",
]

export const ItemResetTypes = [
    "Short Rest",
    "Long Rest",
    "Dawn",
    "Other",
    "None, Consumable"
]

export const ItemSubTypes = [
    "Adventuring Gear",
    "Ammunition",
    "Arcane Focus",
    "Arcane Focus",
    "Druidic Focus",
    "Holy Symbol",
    "Tool"
]

export const ItemArmourStealthCheck = {
    1: "None",
    2: "Disadvantage",
}

export const ItemArmourType = {
    1: "Light",
    2: "Medium",
    3: "Heavy",
    4: "Shield",
}

export const ScaleTypes = {
    "spellscale": "Spell Scale",
    "spelllevel": "Spell Level",
    "characterlevel": "Character Level",
}

export const AtHigherLevelTypes = {
    16:"Additional Count",
    11:"Additional Creatures",
    15:"Additional Points",
    1:"Additional Targets",
    9:"Extended Area",
    3:"Extended Duration",
    12:"Special",
}

export const ActionTypes = {
    3: "General",
    1: "Weapon",
    2: "Spell",
}

export const WeaponActionSubTypes = {
    2: "Natural",
    3: "Unarmed",
}

export const AttackRanges = {
    1: "Melee",
    2: "Ranged",
}

export const DamageTypes = {
    5:"Acid",
    1:"Bludgeoning",
    6:"Cold",
    7:"Fire",
    13:"Force",
    8:"Lightning",
    4:"Necrotic",
    2:"Piercing",
    10:"Poison",
    11:"Psychic",
    12:"Radiant",
    3:"Slashing",
    9:"Thunder",
}

export const Atrributes = [
    "STR",
    "DEX",
    "CON",
    "INT",
    "WIS",
    "CHA"
]

export const AttributeID = {
    1: "STR",
    2: "DEX",
    3: "CON",
    4: "INT",
    5: "WIS",
    6: "CHA"
}

export const ConditionID = {
    1:"Blinded",
    2:"Charmed",
    3:"Deafened",
    4:"Exhaustion",
    5:"Frightened",
    6:"Grappled",
    7:"Incapacitated",
    8:"Invisible",
    9:"Paralyzed",
    10:"Petrified",
    11:"Poisoned",
    12:"Prone",
    13:"Restrained",
    14:"Stunned",
    15:"Unconscious",
}

export const Conditions = [
    "Blinded",
    "Charmed",
    "Deafened",
    "Exhaustion",
    "Frightened",
    "Grappled",
    "Incapacitated",
    "Invisible",
    "Paralyzed",
    "Petrified",
    "Poisoned",
    "Prone",
    "Restrained",
    "Stunned",
    "Unconscious"
]

export const ItemProperties = [
    {
        "id": 1,
        "name": "Ammunition",
        "description": "You can use a weapon that has the ammunition property to make a ranged attack only if you have ammunition to fire from the weapon. Each time you attack with the weapon, you expend one piece of ammunition. Drawing the ammunition from a quiver, case, or other container is part of the attack (you need a free hand to load a one-handed weapon). At the end of the battle, you can recover half your expended ammunition by taking a minute to search the battlefield. \r\nIf you use a weapon that has the ammunition property to make a melee attack, you treat the weapon as an improvised weapon (see \"Improvised Weapons\" later in the section). A sling must be loaded to deal any damage when used in this way.",
    },
    {
        "id": 2,
        "name": "Finesse",
        "description": "When making an attack with a finesse weapon, you use your choice of your Strength or Dexterity modifier for the attack and damage rolls. You must use the same modifier for both rolls. ",
    },
    {
      "id": 3,
      "name": "Heavy",
      "description": "Creatures that are Small or Tiny have disadvantage on attack rolls with heavy weapons. A heavy weapon's size and bulk make it too large for a Small or Tiny creature to use effectively.",
    },
    {
      "id": 4,
      "name": "Light",
      "description": "A light weapon is small and easy to handle, making it ideal for use when fighting with two weapons.",
    },
    {
      "id": 5,
      "name": "Loading",
      "description": "Because of the time required to load this weapon, you can fire only one piece of ammunition from it when you use an action, bonus action, or reaction to fire it, regardless of the number of attacks you can normally make.",
    },
    {
      "id": 7,
      "name": "Range",
      "description": "A weapon that can be used to make a ranged attack has a range in parentheses after the ammunition or thrown property. The range lists two numbers. The first is the weapon's normal range in feet, and the second indicates the weapon's long range. When attacking a target beyond normal range, you have disadvantage on the attack roll. You can't attack a target beyond the weapon's long range.",
    },
    {
      "id": 8,
      "name": "Reach",
      "description": "This weapon adds 5 feet to your reach when you attack with it, as well as when determining your reach for opportunity attacks with it. ",
    },
    {
      "id": 9,
      "name": "Special",
      "description": "A weapon with the special property has unusual rules governing its use, explained in the weapon's description .",
    },
    {
      "id": 10,
      "name": "Thrown",
      "description": "If a weapon has the thrown property, you can throw the weapon to make a ranged attack. If the weapon is a melee weapon, you use the same ability modifier for that attack roll and damage roll that you would use for a melee attack with the weapon. For example, if you throw a handaxe, you use your Strength, but if you throw a dagger, you can use either your Strength or your Dexterity, since the dagger has the finesse property. ",
    },
    {
      "id": 11,
      "name": "Two-Handed",
      "description": "This weapon requires two hands when you attack with it. ",
    },
    {
      "id": 12,
      "name": "Versatile",
      "description": "This weapon can be used with one or two hands. A damage value in parentheses appears with the property--the damage when the weapon is used with two hands to make a melee attack.",
    },
    {
      "id": 13,
      "name": "Ammunition (Firearms)",
      "description": "The ammunition of a firearm is destroyed upon use. Renaissance and modern firearms use bullets. Futuristic firearms are powered by a special type of ammunition called energy cells. An energy cell contains enough power for all the shots its firearm can make.",
    },
    {
      "id": 14,
      "name": "Burst Fire",
      "description": "A weapon that has the burst fire property can make a normal single-target attack, or it can spray a 10-foot-cube area within normal range with shots. Each creature in the area must succeed on a DC 15 Dexterity saving throw or take the weapon’s normal damage. This action uses ten pieces of ammunition.",
    },
    {
      "id": 15,
      "name": "Reload",
      "description": "A limited number of shots can be made with a weapon that has the reload property. A character must then reload it using an action or a bonus action (the character’s choice).",
    },
    {
      "id": 16,
      "name": "Misfire",
      "description": "",
    },
    {
      "id": 17,
      "name": "Explosive",
      "description": "",
    },
]

export const Challenge_Ratings = {
    "0":{"number":"0","proficiency": "+2", "xp": "10"},
    "1/8":{"number":"0.125","proficiency": "+2", "xp": "25"},
    "1/4":{"number":"0.25","proficiency": "+2", "xp": "50"},
    "1/2":{"number":"0.5","proficiency": "+2", "xp": "100"},
    "1":{"number":"1","proficiency": "+2", "xp": "200"},
    "2":{"number":"2","proficiency": "+2", "xp": "450"},
    "3":{"number":"3","proficiency": "+2", "xp": "700"},
    "4":{"number":"4","proficiency": "+2", "xp": "1,100"},
    "5":{"number":"5","proficiency": "+3", "xp": "1,800"},
    "6":{"number":"6","proficiency": "+3", "xp": "2,300"},
    "7":{"number":"7","proficiency": "+3", "xp": "2,900"},
    "8":{"number":"8","proficiency": "+3", "xp": "3,900"},
    "9":{"number":"9","proficiency": "+4", "xp": "5,000"},
    "10":{"number": "10","proficiency": "+4", "xp": "5,900"},
    "11":{"number": "11","proficiency": "+4", "xp": "7,200"},
    "12":{"number": "12","proficiency": "+4", "xp": "8,400"},
    "13":{"number": "13","proficiency": "+5", "xp": "10,000"},
    "14":{"number": "14","proficiency": "+5", "xp": "11,500"},
    "15":{"number": "15","proficiency": "+5", "xp": "13,000"},
    "16":{"number": "16","proficiency": "+5", "xp": "15,000"},
    "17":{"number": "17","proficiency": "+6", "xp": "18,000"},
    "18":{"number": "18","proficiency": "+6", "xp": "20,000"},
    "19":{"number": "19","proficiency": "+6", "xp": "22,000"},
    "20":{"number": "20","proficiency": "+6", "xp": "25,000"},
    "21":{"number": "21","proficiency": "+7", "xp": "33,000"},
    "22":{"number": "22","proficiency": "+7", "xp": "41,000"},
    "23":{"number": "23","proficiency": "+7", "xp": "50,000"},
    "24":{"number": "24","proficiency": "+7", "xp": "62,000"},
    "25":{"number": "25","proficiency": "+8", "xp": "75,000"},
    "26":{"number": "26","proficiency": "+8", "xp": "90,000"},
    "27":{"number": "27","proficiency": "+8", "xp": "105,000"},
    "28":{"number": "28","proficiency": "+8", "xp": "120,000"},
    "29":{"number": "29","proficiency": "+9", "xp": "135,000"},
    "30":{"number": "30","proficiency": "+9", "xp": "155,000"},
}


export const Resistances = [
    "Acid - Resistance",
    "Acid - Immunity",
    "Acid - Vulnerability",
    "All - Resistance",
    "All damage but Force, Radiant, and Psychic - Resistance",
    "Bludgeoning - Vulnerability",
    "Bludgeoning - Immunity",
    "Bludgeoning - Resistance",
    "Bludgeoning from non magical attacks - Resistance",
    "Bludgeoning, Piercing, and Slashing from Magic Weapons - Resistance",
    "Bludgeoning, Piercing, and Slashing from Metal Weapons - Immunity",
    "Bludgeoning, Piercing, and Slashing from Nonmagical Attacks - Resistance",
    "Bludgeoning, Piercing, and Slashing from Nonmagical Attacks - Immunity",
    "Bludgeoning, Piercing, and Slashing from Nonmagical Attacks that aren't Adamantine - Immunity",
    "Bludgeoning, Piercing, and Slashing from Nonmagical Attacks that aren't Adamantine - Resistance",
    "Bludgeoning, Piercing, and Slashing from Nonmagical Attacks that aren't Adamantine or Silvered - Immunity",
    "Bludgeoning, Piercing, and Slashing from Nonmagical Attacks that aren't Silvered - Resistance",
    "Bludgeoning, Piercing, and Slashing from Nonmagical Attacks that aren't Silvered - Immunity",
    "Bludgeoning, Piercing, and Slashing from Nonmagical Attacks while in Dim Light or Darkness - Resistance",
    "Bludgeoning, Piercing, and Slashing while in Dim Light or Darkness - Resistance",
    "Cold - Vulnerability",
    "Cold - Immunity",
    "Cold - Resistance",
    "Damage Dealt By Traps - Resistance",
    "Damage from Spells - Resistance",
    "Fire - Vulnerability",
    "Fire - Resistance",
    "Fire - Immunity",
    "Force - Resistance",
    "Force - Immunity",
    "Force - Vulnerability",
    "Lightning - Vulnerability",
    "Lightning - Immunity",
    "Lightning - Resistance",
    "Necrotic - Resistance",
    "Necrotic - Immunity",
    "Necrotic - Vulnerability",
    "Nonmagical Bludgeoning, Piercing, and Slashing (from Stoneskin) - Resistance",
    "Petrified (Aberrant Armor Only) - Immunity",
    "Piercing - Vulnerability",
    "Piercing - Resistance",
    "Piercing - Immunity",
    "Piercing and Slashing from Nonmagical Attacks that aren't Adamantine - Resistance",
    "Piercing and Slashing from Nonmagical Attacks that aren't Adamantine - Immunity",
    "Piercing from Magic Weapons Wielded by Good Creatures - Vulnerability",
    "Poison - Vulnerability",
    "Poison - Resistance",
    "Poison - Immunity",
    "Psychic - Immunity",
    "Psychic - Resistance",
    "Psychic - Vulnerability",
    "Radiant - Vulnerability",
    "Radiant - Resistance",
    "Radiant - Immunity",
    "Ranged Attacks - Resistance",
    "Slashing - Vulnerability",
    "Slashing - Resistance",
    "Slashing - Immunity",
    "Thunder - Resistance",
    "Thunder - Immunity",
    "Thunder - Vulnerability"
]

export const DiceValues = [4,6,8,10,12,20,100]

export const ModifierDurationOptions = ["Round", "Minute", "Hour", "Day"]

export const ActionResetTypes = {
    1: {
        id: 1,
        name: "short-rest",
        display_name: "Short Rest"
    },
    2: {
        id: 2,
        name: "long-rest",
        display_name: "Long Rest"
    },
    3: {
        id: 3,
        name: "dawn",
        display_name: "Dawn"
    },
    4: {
        id: 3,
        name: "other",
        display_name: "Other"
    }
}

export const Proficiences = [{"id":419,"display_name":"Ability Checks","name":"ability-checks","group":"ABILITY_CHECK","subgroup":null},{"id":427,"display_name":"Acrobatics","name":"acrobatics","group":"SKILL","subgroup":null},{"id":501,"display_name":"Alchemist's Supplies","name":"alchemists-supplies","group":"TOOL","subgroup":"ARTISAN"},{"id":435,"display_name":"Animal Handling","name":"animal-handling","group":"SKILL","subgroup":null},{"id":430,"display_name":"Arcana","name":"arcana","group":"SKILL","subgroup":null},{"id":426,"display_name":"Athletics","name":"athletics","group":"SKILL","subgroup":null},{"id":526,"display_name":"Bagpipes","name":"bagpipes","group":"TOOL","subgroup":"MUSICAL"},{"id":467,"display_name":"Battleaxe","name":"battleaxe","group":"WEAPON","subgroup":"MARTIAL"},{"id":483,"display_name":"Blowgun","name":"blowgun","group":"WEAPON","subgroup":"MARTIAL"},{"id":495,"display_name":"Breastplate","name":"breastplate","group":"ARMOUR","subgroup":"MEDIUM"},{"id":502,"display_name":"Brewer's Supplies","name":"brewers-supplies","group":"TOOL","subgroup":"ARTISAN"},{"id":503,"display_name":"Calligrapher's Supplies","name":"calligraphers-supplies","group":"TOOL","subgroup":"ARTISAN"},{"id":504,"display_name":"Carpenter's Tools","name":"carpenters-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":505,"display_name":"Cartographer's Tools","name":"cartographers-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":498,"display_name":"Chain Mail","name":"chain-mail","group":"ARMOUR","subgroup":"HEAVY"},{"id":494,"display_name":"Chain Shirt","name":"chain-shirt","group":"ARMOUR","subgroup":"MEDIUM"},{"id":425,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks","group":"ABILITY_CHECK","subgroup":null},{"id":418,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws","group":"SAVING_THROW","subgroup":null},{"id":550,"display_name":"Choose a Gaming Set","name":"choose-a-gaming-set","group":"TOOL","subgroup":"CHOICE"},{"id":547,"display_name":"Choose a Heavy Armor Type","name":"choose-a-heavy-armor-type","group":"ARMOUR","subgroup":"CHOICE"},{"id":545,"display_name":"Choose a Light Armor Type","name":"choose-a-light-armor-type","group":"ARMOUR","subgroup":"CHOICE"},{"id":542,"display_name":"Choose a Martial Weapon","name":"choose-a-martial-weapon","group":"WEAPON","subgroup":"CHOICE"},{"id":546,"display_name":"Choose a Medium Armor Type","name":"choose-a-medium-armor-type","group":"ARMOUR","subgroup":"CHOICE"},{"id":551,"display_name":"Choose a Musical Instrument","name":"choose-a-musical-instrument","group":"TOOL","subgroup":"CHOICE"},{"id":543,"display_name":"Choose a Simple Weapon","name":"choose-a-simple-weapon","group":"WEAPON","subgroup":"CHOICE"},{"id":548,"display_name":"Choose a Skill","name":"choose-a-skill","group":"SKILL","subgroup":"CHOICE"},{"id":1794,"display_name":"Choose a Skill or Tool","name":"choose-a-skill-or-tool","group":"OTHER","subgroup":"CHOICE"},{"id":552,"display_name":"Choose a Tool","name":"choose-a-tool","group":"TOOL","subgroup":"CHOICE"},{"id":541,"display_name":"Choose a Weapon","name":"choose-a-weapon","group":"WEAPON","subgroup":"CHOICE"},{"id":544,"display_name":"Choose an Armor Type","name":"choose-an-armor-type","group":"ARMOUR","subgroup":"CHOICE"},{"id":549,"display_name":"Choose an Artisan's Tool","name":"choose-an-artisans-tool","group":"TOOL","subgroup":"CHOICE"},{"id":454,"display_name":"Club","name":"club","group":"WEAPON","subgroup":"SIMPLE"},{"id":506,"display_name":"Cobbler's Tools","name":"cobblers-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":422,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks","group":"ABILITY_CHECK","subgroup":null},{"id":415,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws","group":"SAVING_THROW","subgroup":null},{"id":507,"display_name":"Cook's Utensils","name":"cooks-utensils","group":"TOOL","subgroup":"ARTISAN"},{"id":450,"display_name":"Crossbow, Hand","name":"crossbow-hand","group":"WEAPON","subgroup":"MARTIAL"},{"id":484,"display_name":"Crossbow, Heavy","name":"crossbow-heavy","group":"WEAPON","subgroup":"MARTIAL"},{"id":463,"display_name":"Crossbow, Light","name":"crossbow-light","group":"WEAPON","subgroup":"SIMPLE"},{"id":452,"display_name":"Dagger","name":"dagger","group":"WEAPON","subgroup":"SIMPLE"},{"id":464,"display_name":"Dart","name":"dart","group":"WEAPON","subgroup":"SIMPLE"},{"id":440,"display_name":"Deception","name":"deception","group":"SKILL","subgroup":null},{"id":421,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks","group":"ABILITY_CHECK","subgroup":null},{"id":448,"display_name":"Dexterity Attacks","name":"dexterity-attacks","group":"ATTACK","subgroup":null},{"id":414,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws","group":"SAVING_THROW","subgroup":null},{"id":524,"display_name":"Dice Set","name":"dice-set","group":"TOOL","subgroup":"GAMING"},{"id":518,"display_name":"Disguise Kit","name":"disguise-kit","group":"TOOL","subgroup":"ARTISAN"},{"id":527,"display_name":"Drum","name":"drum","group":"TOOL","subgroup":"MUSICAL"},{"id":528,"display_name":"Dulcimer","name":"dulcimer","group":"TOOL","subgroup":"MUSICAL"},{"id":468,"display_name":"Flail","name":"flail","group":"WEAPON","subgroup":"MARTIAL"},{"id":529,"display_name":"Flute","name":"flute","group":"TOOL","subgroup":"MUSICAL"},{"id":519,"display_name":"Forgery Kit","name":"forgery-kit","group":"TOOL","subgroup":"ARTISAN"},{"id":451,"display_name":"Glaive","name":"glaive","group":"WEAPON","subgroup":"MARTIAL"},{"id":508,"display_name":"Glassblower's Tools","name":"glassblowers-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":469,"display_name":"Greataxe","name":"greataxe","group":"WEAPON","subgroup":"MARTIAL"},{"id":455,"display_name":"Greatclub","name":"greatclub","group":"WEAPON","subgroup":"SIMPLE"},{"id":470,"display_name":"Greatsword","name":"greatsword","group":"WEAPON","subgroup":"MARTIAL"},{"id":471,"display_name":"Halberd","name":"halberd","group":"WEAPON","subgroup":"MARTIAL"},{"id":496,"display_name":"Half Plate","name":"half-plate","group":"ARMOUR","subgroup":"MEDIUM"},{"id":456,"display_name":"Handaxe","name":"handaxe","group":"WEAPON","subgroup":"SIMPLE"},{"id":540,"display_name":"Heavy Armor","name":"heavy-armor","group":"ARMOUR","subgroup":"GROUP"},{"id":520,"display_name":"Herbalism Kit","name":"herbalism-kit","group":"TOOL","subgroup":"ARTISAN"},{"id":493,"display_name":"Hide","name":"hide","group":"ARMOUR","subgroup":"MEDIUM"},{"id":431,"display_name":"History","name":"history","group":"SKILL","subgroup":null},{"id":532,"display_name":"Horn","name":"horn","group":"TOOL","subgroup":"MUSICAL"},{"id":449,"display_name":"Initiative","name":"initiative","group":"OTHER","subgroup":null},{"id":436,"display_name":"Insight","name":"insight","group":"SKILL","subgroup":null},{"id":423,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks","group":"ABILITY_CHECK","subgroup":null},{"id":416,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws","group":"SAVING_THROW","subgroup":null},{"id":441,"display_name":"Intimidation","name":"intimidation","group":"SKILL","subgroup":null},{"id":432,"display_name":"Investigation","name":"investigation","group":"SKILL","subgroup":null},{"id":457,"display_name":"Javelin","name":"javelin","group":"WEAPON","subgroup":"SIMPLE"},{"id":509,"display_name":"Jeweler's Tools","name":"jewelers-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":472,"display_name":"Lance","name":"lance","group":"WEAPON","subgroup":"MARTIAL"},{"id":492,"display_name":"Leather","name":"leather","group":"ARMOUR","subgroup":"LIGHT"},{"id":510,"display_name":"Leatherworker's Tools","name":"leatherworkers-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":538,"display_name":"Light Armor","name":"light-armor","group":"ARMOUR","subgroup":"GROUP"},{"id":458,"display_name":"Light Hammer","name":"light-hammer","group":"WEAPON","subgroup":"SIMPLE"},{"id":485,"display_name":"Longbow","name":"longbow","group":"WEAPON","subgroup":"MARTIAL"},{"id":453,"display_name":"Longsword","name":"longsword","group":"WEAPON","subgroup":"MARTIAL"},{"id":530,"display_name":"Lute","name":"lute","group":"TOOL","subgroup":"MUSICAL"},{"id":531,"display_name":"Lyre","name":"lyre","group":"TOOL","subgroup":"MUSICAL"},{"id":459,"display_name":"Mace","name":"mace","group":"WEAPON","subgroup":"SIMPLE"},{"id":536,"display_name":"Martial Weapons","name":"martial-weapons","group":"WEAPON","subgroup":"GROUP"},{"id":511,"display_name":"Mason's Tools","name":"masons-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":473,"display_name":"Maul","name":"maul","group":"WEAPON","subgroup":"MARTIAL"},{"id":437,"display_name":"Medicine","name":"medicine","group":"SKILL","subgroup":null},{"id":539,"display_name":"Medium Armor","name":"medium-armor","group":"ARMOUR","subgroup":"GROUP"},{"id":444,"display_name":"Melee Attacks","name":"melee-attacks","group":"ATTACK","subgroup":null},{"id":489,"display_name":"Mithril Plate Armor","name":"mithril-plate-armor","group":"ARMOUR","subgroup":"HEAVY"},{"id":474,"display_name":"Morningstar","name":"morningstar","group":"WEAPON","subgroup":"MARTIAL"},{"id":433,"display_name":"Nature","name":"nature","group":"SKILL","subgroup":null},{"id":521,"display_name":"Navigator's Tools","name":"navigators-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":486,"display_name":"Net","name":"net","group":"WEAPON","subgroup":"MARTIAL"},{"id":491,"display_name":"Padded","name":"padded","group":"ARMOUR","subgroup":"LIGHT"},{"id":512,"display_name":"Painter's Supplies","name":"painters-supplies","group":"TOOL","subgroup":"ARTISAN"},{"id":533,"display_name":"Pan Flute","name":"pan-flute","group":"TOOL","subgroup":"MUSICAL"},{"id":438,"display_name":"Perception","name":"perception","group":"SKILL","subgroup":null},{"id":442,"display_name":"Performance","name":"performance","group":"SKILL","subgroup":null},{"id":443,"display_name":"Persuasion","name":"persuasion","group":"SKILL","subgroup":null},{"id":475,"display_name":"Pike","name":"pike","group":"WEAPON","subgroup":"MARTIAL"},{"id":500,"display_name":"Plate","name":"plate","group":"ARMOUR","subgroup":"HEAVY"},{"id":525,"display_name":"Playing Card Set","name":"playing-card-set","group":"TOOL","subgroup":"GAMING"},{"id":522,"display_name":"Poisoner's Kit","name":"poisoners-kit","group":"TOOL","subgroup":"OTHER"},{"id":513,"display_name":"Potter's Tools","name":"potters-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":460,"display_name":"Quarterstaff","name":"quarterstaff","group":"WEAPON","subgroup":"SIMPLE"},{"id":445,"display_name":"Ranged Attacks","name":"ranged-attacks","group":"ATTACK","subgroup":null},{"id":476,"display_name":"Rapier","name":"rapier","group":"WEAPON","subgroup":"MARTIAL"},{"id":434,"display_name":"Religion","name":"religion","group":"SKILL","subgroup":null},{"id":497,"display_name":"Ring Mail","name":"ring-mail","group":"ARMOUR","subgroup":"HEAVY"},{"id":412,"display_name":"Saving Throws","name":"saving-throws","group":"SAVING_THROW","subgroup":null},{"id":488,"display_name":"Scale Mail","name":"scale-mail","group":"ARMOUR","subgroup":"MEDIUM"},{"id":477,"display_name":"Scimitar","name":"scimitar","group":"WEAPON","subgroup":"MARTIAL"},{"id":534,"display_name":"Shawm","name":"shawm","group":"TOOL","subgroup":"MUSICAL"},{"id":465,"display_name":"Shortbow","name":"shortbow","group":"WEAPON","subgroup":"SIMPLE"},{"id":478,"display_name":"Shortsword","name":"shortsword","group":"WEAPON","subgroup":"MARTIAL"},{"id":461,"display_name":"Sickle","name":"sickle","group":"WEAPON","subgroup":"SIMPLE"},{"id":537,"display_name":"Simple Weapons","name":"simple-weapons","group":"WEAPON","subgroup":"GROUP"},{"id":428,"display_name":"Sleight of Hand","name":"sleight-of-hand","group":"SKILL","subgroup":null},{"id":466,"display_name":"Sling","name":"sling","group":"WEAPON","subgroup":"SIMPLE"},{"id":514,"display_name":"Smith's Tools","name":"smiths-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":462,"display_name":"Spear","name":"spear","group":"WEAPON","subgroup":"SIMPLE"},{"id":446,"display_name":"Spell Attacks","name":"spell-attacks","group":"ATTACK","subgroup":null},{"id":499,"display_name":"Splint","name":"splint","group":"ARMOUR","subgroup":"HEAVY"},{"id":429,"display_name":"Stealth","name":"stealth","group":"SKILL","subgroup":null},{"id":490,"display_name":"Steel Shield","name":"steel-shield","group":"ARMOUR","subgroup":"SHIELD"},{"id":420,"display_name":"Strength Ability Checks","name":"strength-ability-checks","group":"ABILITY_CHECK","subgroup":null},{"id":447,"display_name":"Strength Attacks","name":"strength-attacks","group":"ATTACK","subgroup":null},{"id":413,"display_name":"Strength Saving Throws","name":"strength-saving-throws","group":"SAVING_THROW","subgroup":null},{"id":487,"display_name":"Studded Leather","name":"studded-leather","group":"ARMOUR","subgroup":"LIGHT"},{"id":439,"display_name":"Survival","name":"survival","group":"SKILL","subgroup":null},{"id":523,"display_name":"Thieves' Tools","name":"thieves-tools","group":"TOOL","subgroup":"OTHER"},{"id":515,"display_name":"Tinker's Tools","name":"tinkers-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":479,"display_name":"Trident","name":"trident","group":"WEAPON","subgroup":"MARTIAL"},{"id":535,"display_name":"Viol","name":"viol","group":"TOOL","subgroup":"MUSICAL"},{"id":480,"display_name":"War pick","name":"war-pick","group":"WEAPON","subgroup":"MARTIAL"},{"id":481,"display_name":"Warhammer","name":"warhammer","group":"WEAPON","subgroup":"MARTIAL"},{"id":516,"display_name":"Weaver's Tools","name":"weavers-tools","group":"TOOL","subgroup":"ARTISAN"},{"id":482,"display_name":"Whip","name":"whip","group":"WEAPON","subgroup":"MARTIAL"},{"id":424,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks","group":"ABILITY_CHECK","subgroup":null},{"id":417,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws","group":"SAVING_THROW","subgroup":null},{"id":517,"display_name":"Woodcarver's Tools","name":"woodcarvers-tools","group":"TOOL","subgroup":"ARTISAN"}]

export const ModifierTypes = [{"display_name": "Bonus", "name": "bonus", "id": 1}, {"display_name": "Damage", "name": "damage", "id": 2}, {"display_name": "Advantage", "name": "advantage", "id": 3}, {"display_name": "Disadvantage", "name": "disadvantage", "id": 4}, {"display_name": "Resistance", "name": "resistance", "id": 5}, {"display_name": "Immunity", "name": "immunity", "id": 6}, {"display_name": "Vulnerability", "name": "vulnerability", "id": 7}, {"display_name": "Sense", "name": "sense", "id": 8}, {"display_name": "Set", "name": "set", "id": 9}, {"display_name": "Proficiency", "name": "proficiency", "id": 10}, {"display_name": "Language", "name": "language", "id": 11}, {"display_name": "Expertise", "name": "expertise", "id": 12}, {"display_name": "Half Proficiency", "name": "half-proficiency", "id": 13}, {"display_name": "Feat", "name": "feat", "id": 14}, {"display_name": "Carrying Capacity", "name": "carrying-capacity", "id": 17}, {"display_name": "Natural Weapon", "name": "natural-weapon", "id": 19}, {"display_name": "Stealth Disadvantage", "name": "stealth-disadvantage", "id": 23}, {"display_name": "Speed Reduction", "name": "speed-reduction", "id": 24}, {"display_name": "Melee Weapon Attack", "name": "melee-weapon-attack", "id": 25}, {"display_name": "Ranged Weapon Attack", "name": "ranged-weapon-attack", "id": 26}, {"display_name": "Weapon Property", "name": "weapon-property", "id": 28}, {"display_name": "Half Proficiency Round Up", "name": "half-proficiency-round-up", "id": 29}, {"display_name": "Favored Enemy", "name": "favored-enemy", "id": 30}, {"display_name": "Ignore", "name": "ignore", "id": 31}, {"display_name": "Eldritch Blast", "name": "eldritch-blast", "id": 32}, {"display_name": "Replace Damage Type", "name": "replace-damage-type", "id": 33}, {"display_name": "Twice Proficiency", "name": "twice-proficiency", "id": 34}, {"display_name": "Protection", "name": "protection", "id": 37}, {"display_name": "Stacking Bonus", "name": "stacking-bonus", "id": 38}, {"display_name": "Set Base", "name": "set-base", "id": 39}, {"display_name": "Ignore Weapon Property", "name": "ignore-weapon-property", "id": 40}, {"display_name": "Size", "name": "size", "id": 41}]

export const ModifierSubTypes = {"1":[{"id":15,"display_name":"Ability Checks","name":"ability-checks"},{"id":848,"display_name":"Ability Score Maximum","name":"ability-score-maximum"},{"id":770,"display_name":"AC Max Dex Modifier","name":"ac-max-dex-modifier"},{"id":23,"display_name":"Acrobatics","name":"acrobatics"},{"id":31,"display_name":"Animal Handling","name":"animal-handling"},{"id":26,"display_name":"Arcana","name":"arcana"},{"id":1,"display_name":"Armor Class","name":"armor-class"},{"id":806,"display_name":"Armored Armor Class","name":"armored-armor-class"},{"id":22,"display_name":"Athletics","name":"athletics"},{"id":21,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks"},{"id":14,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws"},{"id":7,"display_name":"Charisma Score","name":"charisma-score"},{"id":695,"display_name":"Choose an Ability Score","name":"choose-an-ability-score","choice_groups":["ABILITIES"]},{"id":1574,"display_name":"Choose Constitution or Intelligence","name":"choose-constitution-or-intelligence","choice_groups":["ABILITIES"],"only":["constitution-score","intelligence-score"]},{"id":1575,"display_name":"Choose Constitution or Wisdom","name":"choose-constitution-or-wisdom","choice_groups":["ABILITIES"],"only":["constitution-score","wisdom-score"]},{"id":1718,"display_name":"Choose Dexterity or Charisma","name":"choose-dexterity-or-charisma","choice_groups":["ABILITIES"],"only":["dexterity-score","charisma-score"]},{"id":1573,"display_name":"Choose Dexterity or Constitution","name":"choose-dexterity-or-constitution","choice_groups":["ABILITIES"],"only":["dexterity-score","constitution-score"]},{"id":1065,"display_name":"Choose Dexterity or Intelligence","name":"choose-dexterity-or-intelligence","choice_groups":["ABILITIES"],"only":["dexterity-score","intelligence-score"]},{"id":1678,"display_name":"Choose Dexterity or Wisdom","name":"choose-dexterity-or-wisdom","choice_groups":["ABILITIES"],"only":["dexterity-score","wisdom-score"]},{"id":1772,"display_name":"Choose Dexterity, Constitution, Intelligence, Wisdom, or Charisma","name":"choose-dexterity-constitution-intelligence-wisdom-or-charisma","choice_groups":["ABILITIES"],"exclude":["strength-score"]},{"id":1066,"display_name":"Choose Dexterity, Constitution, or Charisma","name":"choose-dexterity-constitution-or-charisma","choice_groups":["ABILITIES"],"only":["dexterity-score","constitution-score","charisma-score"]},{"id":1063,"display_name":"Choose Dexterity, Intelligence, Wisdom, or Charisma","name":"choose-dexterity-intelligence-wisdom-or-charisma","choice_groups":["ABILITIES"],"excludee":["strength-score","constitution-score"]},{"id":1064,"display_name":"Choose Intelligence or Charisma","name":"choose-intelligence-or-charisma","choice_groups":["ABILITIES"],"only":["intelligence-score","charisma-score"]},{"id":773,"display_name":"Choose Intelligence or Wisdom","name":"choose-intelligence-or-wisdom","choice_groups":["ABILITIES"],"only":["intelligence-score","wisdom-score"]},{"id":777,"display_name":"Choose Strength or Constitution","name":"choose-strength-or-constitution","choice_groups":["ABILITIES"],"only":["strength-score","constitution-score"]},{"id":764,"display_name":"Choose Strength or Dexterity","name":"choose-strength-or-dexterity","choice_groups":["ABILITIES"],"only":["strength-score","dexterity-score"]},{"id":1679,"display_name":"Choose Strength or Wisdom","name":"choose-strength-or-wisdom","choice_groups":["ABILITIES"],"only":["strength-score","WID"]},{"id":1773,"display_name":"Choose Strength, Constitution, Intelligence, Wisdom, or Charisma","name":"choose-strength-constitution-intelligence-wisdom-or-charisma","choice_groups":["ABILITIES"],"exclude":["dexterity-score"]},{"id":1684,"display_name":"Choose Strength, Constitution, or Charisma","name":"choose-strength-constitution-or-charisma","choice_groups":["ABILITIES"],"only":["strength-score","constitution-score","charisma-score"]},{"id":1061,"display_name":"Choose Strength, Constitution, or Dexterity","name":"choose-strength-constitution-or-dexterity","choice_groups":["ABILITIES"],"only":["strength-score","constitution-score","dexterity-score"]},{"id":1770,"display_name":"Choose Strength, Dexterity, Constitution, Intelligence, or Charisma","name":"choose-strength-dexterity-constitution-intelligence-or-charisma","choice_groups":["ABILITIES"],"exclude":["wisdom-score"]},{"id":1775,"display_name":"Choose Strength, Dexterity, Constitution, Intelligence, or Wisdom","name":"choose-strength-dexterity-constitution-intelligence-or-wisdom","choice_groups":["ABILITIES"],"exclude":["charisma-score"]},{"id":1771,"display_name":"Choose Strength, Dexterity, Constitution, Wisdom, or Charisma","name":"choose-strength-dexterity-constitution-wisdom-or-charisma","choice_groups":["ABILITIES"],"exclude":["intelligence-score"]},{"id":1774,"display_name":"Choose Strength, Dexterity, Intelligence, Wisdom, or Charisma","name":"choose-strength-dexterity-intelligence-wisdom-or-charisma","choice_groups":["ABILITIES"],"exclude":["constitution-score"]},{"id":1717,"display_name":"Choose Strength, Dexterity, or Wisdom ","name":"choose-strength-dexterity-or-wisdom","choice_groups":["ABILITIES"],"only":["strength-score","dexterity-score","wisdom-score"]},{"id":393,"display_name":"Cleric Cantrip Damage","name":"cleric-cantrip-damage"},{"id":18,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks"},{"id":11,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws"},{"id":4,"display_name":"Constitution Score","name":"constitution-score"},{"id":36,"display_name":"Deception","name":"deception"},{"id":17,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks"},{"id":189,"display_name":"Dexterity Attacks","name":"dexterity-attacks"},{"id":10,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws"},{"id":3,"display_name":"Dexterity Score","name":"dexterity-score"},{"id":844,"display_name":"Domain Spell","name":"domain-spell"},{"id":1039,"display_name":"Dual Wield Armor Class","name":"dual-wield-armor-class"},{"id":698,"display_name":"Eldritch Blast Damage","name":"eldritch-blast-damage"},{"id":355,"display_name":"Extra Attacks","name":"extra-attacks"},{"id":411,"display_name":"Half Proficiency","name":"half-proficiency"},{"id":27,"display_name":"History","name":"history"},{"id":192,"display_name":"Hit Points","name":"hit-points"},{"id":752,"display_name":"Hit Points per Level","name":"hit-points-per-level"},{"id":218,"display_name":"Initiative","name":"initiative"},{"id":32,"display_name":"Insight","name":"insight"},{"id":19,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks"},{"id":12,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws"},{"id":5,"display_name":"Intelligence Score","name":"intelligence-score"},{"id":37,"display_name":"Intimidation","name":"intimidation"},{"id":28,"display_name":"Investigation","name":"investigation"},{"id":312,"display_name":"Magic","name":"magic"},{"id":1768,"display_name":"Magic Item Attack With Charisma","name":"magic-item-attack-with-charisma"},{"id":1765,"display_name":"Magic Item Attack With Constitution","name":"magic-item-attack-with-constitution"},{"id":1764,"display_name":"Magic Item Attack With Dexterity","name":"magic-item-attack-with-dexterity"},{"id":1766,"display_name":"Magic Item Attack With Intelligence","name":"magic-item-attack-with-intelligence"},{"id":1763,"display_name":"Magic Item Attack With Strength","name":"magic-item-attack-with-strength"},{"id":1767,"display_name":"Magic Item Attack With Wisdom","name":"magic-item-attack-with-wisdom"},{"id":33,"display_name":"Medicine","name":"medicine"},{"id":802,"display_name":"Melee Attacks","name":"melee-attacks"},{"id":766,"display_name":"Melee Reach","name":"melee-reach"},{"id":804,"display_name":"Melee Spell Attacks","name":"melee-spell-attacks"},{"id":45,"display_name":"Melee Weapon Attacks","name":"melee-weapon-attacks"},{"id":1047,"display_name":"Natural Attacks","name":"natural-attacks"},{"id":29,"display_name":"Nature","name":"nature"},{"id":775,"display_name":"Passive Investigation","name":"passive-investigation"},{"id":774,"display_name":"Passive Perception","name":"passive-perception"},{"id":34,"display_name":"Perception","name":"perception"},{"id":38,"display_name":"Performance","name":"performance"},{"id":39,"display_name":"Persuasion","name":"persuasion"},{"id":313,"display_name":"Proficiency","name":"proficiency"},{"id":814,"display_name":"Proficiency Bonus","name":"proficiency-bonus"},{"id":803,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":805,"display_name":"Ranged Spell Attacks","name":"ranged-spell-attacks"},{"id":46,"display_name":"Ranged Weapon Attacks","name":"ranged-weapon-attacks"},{"id":30,"display_name":"Religion","name":"religion"},{"id":8,"display_name":"Saving Throws","name":"saving-throws"},{"id":1744,"display_name":"Shield AC on Dex Saves","name":"shield-ac-on-dex-saves"},{"id":24,"display_name":"Sleight of Hand","name":"sleight-of-hand"},{"id":40,"display_name":"Speed","name":"speed"},{"id":44,"display_name":"Speed (Burrowing)","name":"speed-burrowing"},{"id":42,"display_name":"Speed (Climbing)","name":"speed-climbing"},{"id":41,"display_name":"Speed (Flying)","name":"speed-flying"},{"id":43,"display_name":"Speed (Swimming)","name":"speed-swimming"},{"id":1697,"display_name":"Speed (Walking)","name":"speed-walking"},{"id":700,"display_name":"Spell Attack Range Multiplier","name":"spell-attack-range-multiplier"},{"id":47,"display_name":"Spell Attacks","name":"spell-attacks"},{"id":1829,"display_name":"Spell Group - Healing","name":"spell-group--healing"},{"id":316,"display_name":"Spell Save DC","name":"spell-save-dc"},{"id":25,"display_name":"Stealth","name":"stealth"},{"id":16,"display_name":"Strength Ability Checks","name":"strength-ability-checks"},{"id":186,"display_name":"Strength Attacks","name":"strength-attacks"},{"id":9,"display_name":"Strength Saving Throws","name":"strength-saving-throws"},{"id":2,"display_name":"Strength Score","name":"strength-score"},{"id":35,"display_name":"Survival","name":"survival"},{"id":193,"display_name":"Temporary Hit Points","name":"temporary-hit-points"},{"id":319,"display_name":"Twice Proficiency Bonus","name":"twice-proficiency-bonus"},{"id":1048,"display_name":"Unarmed Attacks","name":"unarmed-attacks"},{"id":801,"display_name":"Unarmored Armor Class","name":"unarmored-armor-class"},{"id":1685,"display_name":"Unarmored Movement","name":"unarmored-movement"},{"id":765,"display_name":"Weapon Attack Range Multiplier","name":"weapon-attack-range-multiplier"},{"id":20,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks"},{"id":13,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws"},{"id":6,"display_name":"Wisdom Score","name":"wisdom-score"}],"2":[{"id":48,"display_name":"Acid","name":"acid"},{"id":49,"display_name":"Bludgeoning","name":"bludgeoning"},{"id":50,"display_name":"Cold","name":"cold"},{"id":51,"display_name":"Fire","name":"fire"},{"id":52,"display_name":"Force","name":"force"},{"id":53,"display_name":"Lightning","name":"lightning"},{"id":809,"display_name":"Longbow","name":"longbow"},{"id":1687,"display_name":"Melee Weapon Attacks","name":"melee-weapon-attacks"},{"id":1050,"display_name":"Natural Attacks","name":"natural-attacks"},{"id":54,"display_name":"Necrotic","name":"necrotic"},{"id":810,"display_name":"One-Handed Melee Attacks","name":"onehanded-melee-attacks"},{"id":55,"display_name":"Piercing","name":"piercing"},{"id":56,"display_name":"Poison","name":"poison"},{"id":57,"display_name":"Psychic","name":"psychic"},{"id":58,"display_name":"Radiant","name":"radiant"},{"id":808,"display_name":"Shortbow","name":"shortbow"},{"id":59,"display_name":"Slashing","name":"slashing"},{"id":60,"display_name":"Thunder","name":"thunder"},{"id":1049,"display_name":"Unarmed Attacks","name":"unarmed-attacks"}],"3":[{"id":68,"display_name":"Ability Checks","name":"ability-checks"},{"id":76,"display_name":"Acrobatics","name":"acrobatics"},{"id":84,"display_name":"Animal Handling","name":"animal-handling"},{"id":79,"display_name":"Arcana","name":"arcana"},{"id":75,"display_name":"Athletics","name":"athletics"},{"id":74,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks"},{"id":67,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws"},{"id":71,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks"},{"id":64,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws"},{"id":1690,"display_name":"Death Saving Throws","name":"death-saving-throws"},{"id":89,"display_name":"Deception","name":"deception"},{"id":70,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks"},{"id":190,"display_name":"Dexterity Attacks","name":"dexterity-attacks"},{"id":63,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws"},{"id":80,"display_name":"History","name":"history"},{"id":217,"display_name":"Initiative","name":"initiative"},{"id":85,"display_name":"Insight","name":"insight"},{"id":72,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks"},{"id":65,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws"},{"id":90,"display_name":"Intimidation","name":"intimidation"},{"id":81,"display_name":"Investigation","name":"investigation"},{"id":86,"display_name":"Medicine","name":"medicine"},{"id":93,"display_name":"Melee Attacks","name":"melee-attacks"},{"id":82,"display_name":"Nature","name":"nature"},{"id":87,"display_name":"Perception","name":"perception"},{"id":91,"display_name":"Performance","name":"performance"},{"id":92,"display_name":"Persuasion","name":"persuasion"},{"id":94,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":83,"display_name":"Religion","name":"religion"},{"id":61,"display_name":"Saving Throws","name":"saving-throws"},{"id":77,"display_name":"Sleight of Hand","name":"sleight-of-hand"},{"id":95,"display_name":"Spell Attacks","name":"spell-attacks"},{"id":78,"display_name":"Stealth","name":"stealth"},{"id":69,"display_name":"Strength Ability Checks","name":"strength-ability-checks"},{"id":187,"display_name":"Strength Attacks","name":"strength-attacks"},{"id":62,"display_name":"Strength Saving Throws","name":"strength-saving-throws"},{"id":88,"display_name":"Survival","name":"survival"},{"id":73,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks"},{"id":66,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws"}],"4":[{"id":103,"display_name":"Ability Checks","name":"ability-checks"},{"id":111,"display_name":"Acrobatics","name":"acrobatics"},{"id":119,"display_name":"Animal Handling","name":"animal-handling"},{"id":114,"display_name":"Arcana","name":"arcana"},{"id":110,"display_name":"Athletics","name":"athletics"},{"id":858,"display_name":"Attack Rolls Against You","name":"attack-rolls-against-you"},{"id":109,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks"},{"id":102,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws"},{"id":106,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks"},{"id":99,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws"},{"id":1691,"display_name":"Death Saving Throws","name":"death-saving-throws"},{"id":124,"display_name":"Deception","name":"deception"},{"id":105,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks"},{"id":191,"display_name":"Dexterity Attacks","name":"dexterity-attacks"},{"id":98,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws"},{"id":115,"display_name":"History","name":"history"},{"id":120,"display_name":"Insight","name":"insight"},{"id":107,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks"},{"id":100,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws"},{"id":125,"display_name":"Intimidation","name":"intimidation"},{"id":116,"display_name":"Investigation","name":"investigation"},{"id":121,"display_name":"Medicine","name":"medicine"},{"id":128,"display_name":"Melee Attacks","name":"melee-attacks"},{"id":117,"display_name":"Nature","name":"nature"},{"id":122,"display_name":"Perception","name":"perception"},{"id":126,"display_name":"Performance","name":"performance"},{"id":127,"display_name":"Persuasion","name":"persuasion"},{"id":129,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":118,"display_name":"Religion","name":"religion"},{"id":96,"display_name":"Saving Throws","name":"saving-throws"},{"id":112,"display_name":"Sleight of Hand","name":"sleight-of-hand"},{"id":130,"display_name":"Spell Attacks","name":"spell-attacks"},{"id":113,"display_name":"Stealth","name":"stealth"},{"id":104,"display_name":"Strength Ability Checks","name":"strength-ability-checks"},{"id":188,"display_name":"Strength Attacks","name":"strength-attacks"},{"id":97,"display_name":"Strength Saving Throws","name":"strength-saving-throws"},{"id":123,"display_name":"Survival","name":"survival"},{"id":1699,"display_name":"Weapon Attacks","name":"weapon-attacks"},{"id":108,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks"},{"id":101,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws"}],"5":[{"id":131,"display_name":"Acid","name":"acid"},{"id":212,"display_name":"All","name":"all"},{"id":132,"display_name":"Bludgeoning","name":"bludgeoning"},{"id":215,"display_name":"Bludgeoning, Piercing, and Slashing from Nonmagical Weapons","name":"bludgeoning-piercing-and-slashing-from-nonmagical-weapons"},{"id":133,"display_name":"Cold","name":"cold"},{"id":769,"display_name":"Damage Dealt by Traps","name":"damage-dealt-by-traps"},{"id":134,"display_name":"Fire","name":"fire"},{"id":135,"display_name":"Force","name":"force"},{"id":136,"display_name":"Lightning","name":"lightning"},{"id":137,"display_name":"Necrotic","name":"necrotic"},{"id":138,"display_name":"Piercing","name":"piercing"},{"id":139,"display_name":"Poison","name":"poison"},{"id":140,"display_name":"Psychic","name":"psychic"},{"id":141,"display_name":"Radiant","name":"radiant"},{"id":318,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":142,"display_name":"Slashing","name":"slashing"},{"id":143,"display_name":"Thunder","name":"thunder"}],"6":[{"id":144,"display_name":"Acid","name":"acid"},{"id":213,"display_name":"All","name":"all"},{"id":194,"display_name":"Blinded","name":"blinded"},{"id":145,"display_name":"Bludgeoning","name":"bludgeoning"},{"id":216,"display_name":"Bludgeoning, Piercing, and Slashing from Nonmagical Weapons","name":"bludgeoning-piercing-and-slashing-from-nonmagical-weapons"},{"id":195,"display_name":"Charmed","name":"charmed"},{"id":146,"display_name":"Cold","name":"cold"},{"id":221,"display_name":"Critical Hits","name":"critical-hits"},{"id":196,"display_name":"Deafened","name":"deafened"},{"id":767,"display_name":"Disease","name":"disease"},{"id":197,"display_name":"Exhaustion","name":"exhaustion"},{"id":147,"display_name":"Fire","name":"fire"},{"id":148,"display_name":"Force","name":"force"},{"id":198,"display_name":"Frightened","name":"frightened"},{"id":199,"display_name":"Grappled","name":"grappled"},{"id":200,"display_name":"Incapacitated","name":"incapacitated"},{"id":201,"display_name":"Invisible","name":"invisible"},{"id":149,"display_name":"Lightning","name":"lightning"},{"id":150,"display_name":"Necrotic","name":"necrotic"},{"id":202,"display_name":"Paralyzed","name":"paralyzed"},{"id":203,"display_name":"Petrified","name":"petrified"},{"id":151,"display_name":"Piercing","name":"piercing"},{"id":152,"display_name":"Poison","name":"poison"},{"id":205,"display_name":"Poisoned","name":"poisoned"},{"id":206,"display_name":"Prone","name":"prone"},{"id":153,"display_name":"Psychic","name":"psychic"},{"id":154,"display_name":"Radiant","name":"radiant"},{"id":207,"display_name":"Restrained","name":"restrained"},{"id":155,"display_name":"Slashing","name":"slashing"},{"id":208,"display_name":"Stunned","name":"stunned"},{"id":156,"display_name":"Thunder","name":"thunder"},{"id":209,"display_name":"Unconscious","name":"unconscious"}],"7":[{"id":157,"display_name":"Acid","name":"acid"},{"id":214,"display_name":"All","name":"all"},{"id":158,"display_name":"Bludgeoning","name":"bludgeoning"},{"id":159,"display_name":"Cold","name":"cold"},{"id":160,"display_name":"Fire","name":"fire"},{"id":161,"display_name":"Force","name":"force"},{"id":162,"display_name":"Lightning","name":"lightning"},{"id":163,"display_name":"Necrotic","name":"necrotic"},{"id":164,"display_name":"Piercing","name":"piercing"},{"id":165,"display_name":"Poison","name":"poison"},{"id":166,"display_name":"Psychic","name":"psychic"},{"id":167,"display_name":"Radiant","name":"radiant"},{"id":168,"display_name":"Slashing","name":"slashing"},{"id":169,"display_name":"Thunder","name":"thunder"}],"8":[{"id":170,"display_name":"Blindsight","name":"blindsight"},{"id":171,"display_name":"Darkvision","name":"darkvision"},{"id":172,"display_name":"Tremorsense","name":"tremorsense"},{"id":173,"display_name":"Truesight","name":"truesight"}],"9":[{"id":1751,"display_name":"AC Max Dex Armored Modifier","name":"ac-max-dex-armored-modifier"},{"id":1045,"display_name":"AC Max Dex Modifier","name":"ac-max-dex-modifier"},{"id":1750,"display_name":"AC Max Dex Unarmored Modifier","name":"ac-max-dex-unarmored-modifier"},{"id":174,"display_name":"Armor Class","name":"armor-class"},{"id":1753,"display_name":"Attunement Slots","name":"attunement-slots"},{"id":180,"display_name":"Charisma Score","name":"charisma-score"},{"id":177,"display_name":"Constitution Score","name":"constitution-score"},{"id":176,"display_name":"Dexterity Score","name":"dexterity-score"},{"id":356,"display_name":"Extra Attacks","name":"extra-attacks"},{"id":185,"display_name":"Innate Speed (Burrowing)","name":"innate-speed-burrowing"},{"id":183,"display_name":"Innate Speed (Climbing)","name":"innate-speed-climbing"},{"id":182,"display_name":"Innate Speed (Flying)","name":"innate-speed-flying"},{"id":184,"display_name":"Innate Speed (Swimming)","name":"innate-speed-swimming"},{"id":181,"display_name":"Innate Speed (Walking)","name":"innate-speed-walking"},{"id":178,"display_name":"Intelligence Score","name":"intelligence-score"},{"id":1686,"display_name":"Minimum Base Armor","name":"minimum-base-armor"},{"id":1695,"display_name":"Speed (Burrowing)","name":"speed-burrowing"},{"id":1693,"display_name":"Speed (Climbing)","name":"speed-climbing"},{"id":1694,"display_name":"Speed (Flying)","name":"speed-flying"},{"id":1696,"display_name":"Speed (Swimming)","name":"speed-swimming"},{"id":1692,"display_name":"Speed (Walking)","name":"speed-walking"},{"id":175,"display_name":"Strength Score","name":"strength-score"},{"id":1005,"display_name":"Unarmed Damage Die","name":"unarmed-damage-die"},{"id":1006,"display_name":"Unarmored Armor Class","name":"unarmored-armor-class"},{"id":179,"display_name":"Wisdom Score","name":"wisdom-score"}],"10":[{"id":229,"display_name":"Ability Checks","name":"ability-checks"},{"id":237,"display_name":"Acrobatics","name":"acrobatics"},{"id":320,"display_name":"Alchemist's Supplies","name":"alchemists-supplies"},{"id":245,"display_name":"Animal Handling","name":"animal-handling"},{"id":240,"display_name":"Arcana","name":"arcana"},{"id":236,"display_name":"Athletics","name":"athletics"},{"id":345,"display_name":"Bagpipes","name":"bagpipes"},{"id":277,"display_name":"Battleaxe","name":"battleaxe"},{"id":1815,"display_name":"Birdpipes","name":"birdpipes"},{"id":293,"display_name":"Blowgun","name":"blowgun"},{"id":1804,"display_name":"Boomerang","name":"boomerang"},{"id":305,"display_name":"Breastplate","name":"breastplate"},{"id":321,"display_name":"Brewer's Supplies","name":"brewers-supplies"},{"id":322,"display_name":"Calligrapher's Supplies","name":"calligraphers-supplies"},{"id":323,"display_name":"Carpenter's Tools","name":"carpenters-tools"},{"id":324,"display_name":"Cartographer's Tools","name":"cartographers-tools"},{"id":308,"display_name":"Chain Mail","name":"chain-mail"},{"id":304,"display_name":"Chain Shirt","name":"chain-shirt"},{"id":235,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks"},{"id":228,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws"},{"id":1742,"display_name":"Choose a Centaur Skill","name":"choose-a-centaur-skill","choice_groups":["SKILLS"],"only":["animal-handling","medicine","nature","survival"]},{"id":811,"display_name":"Choose a Dwarf Artisan's Tool","name":"choose-a-dwarf-artisans-tool","choice_groups":["ARTISAN"],"only":["brewers-supplies","smiths-tools","masons-tools"]},{"id":408,"display_name":"Choose a Gaming Set","name":"choose-a-gaming-set","choice_groups":["GAMING"]},{"id":405,"display_name":"Choose a Heavy Armor Type","name":"choose-a-heavy-armor-type","choice_groups":["HEAVY"]},{"id":403,"display_name":"Choose a Light Armor Type","name":"choose-a-light-armor-type","choice_groups":["LIGHT"]},{"id":400,"display_name":"Choose a Martial Weapon","name":"choose-a-martial-weapon","choice_groups":["MARTIAL"]},{"id":404,"display_name":"Choose a Medium Armor Type","name":"choose-a-medium-armor-type","choice_groups":["MEDIUM"]},{"id":1740,"display_name":"Choose a Minotaur Skill","name":"choose-a-minotaur-skill","choice_groups":["SKILLS"],"only":["intimidation","persuasion"]},{"id":409,"display_name":"Choose a Musical Instrument","name":"choose-a-musical-instrument","choice_groups":["MUSICAL"]},{"id":800,"display_name":"Choose a Musical Instrument or Artisan's Tools","name":"choose-a-musical-instrument-or-artisans-tools","choice_groups":["intelligence-scoreRUMENT","ARTISAN"]},{"id":793,"display_name":"Choose a Musical Instrument or Gaming Set","name":"choose-a-musical-instrument-or-gaming-set","choice_groups":["GAMING","MUSICAL"]},{"id":846,"display_name":"Choose a One-Handed Melee Weapon","name":"choose-a-onehanded-melee-weapon","choice_groups":["ONE_HANDED_MELEE"]},{"id":1743,"display_name":"Choose a Order Domain Skill","name":"choose-a-order-domain-skill","choice_groups":["SKILLS"],"only":["intimidation","persuasion"]},{"id":768,"display_name":"Choose a Saving Throw","name":"choose-a-saving-throw","choice_groups":["ABILITIES"]},{"id":401,"display_name":"Choose a Simple Weapon","name":"choose-a-simple-weapon","choice_groups":["SIMPLE"]},{"id":1797,"display_name":"Choose a Simple Weapon or Tool","name":"choose-a-simple-weapon-or-tool","choice_groups":["SIMPLE","ARTISAN","MUSICAL","GAMING","TOOLS_OTHER"]},{"id":1798,"display_name":"Choose a Simple Weapon, Martial Weapon, or Tool","name":"choose-a-simple-weapon-martial-weapon-or-tool","choice_groups":["SIMPLE","MARTIAL","ARTISAN","MUSICAL","GAMING","TOOLS_OTHER"]},{"id":1801,"display_name":"Choose a Simple Weapon, Martial Weapon, Tool, or Heavy Armor","name":"choose-a-simple-weapon-martial-weapon-tool-or-heavy-armor","choice_groups":["SIMPLE","MARTIAL","ARTISAN","MUSICAL","GAMING","TOOLS_OTHER","HEAVY"]},{"id":1799,"display_name":"Choose a Simple Weapon, Martial Weapon, Tool, or Light Armor","name":"choose-a-simple-weapon-martial-weapon-tool-or-light-armor","choice_groups":["SIMPLE","MARTIAL","ARTISAN","MUSICAL","GAMING","TOOLS_OTHER","LIGHT"]},{"id":1800,"display_name":"Choose a Simple Weapon, Martial Weapon, Tool, or Medium Armor","name":"choose-a-simple-weapon-martial-weapon-tool-or-medium-armor","choice_groups":["SIMPLE","MARTIAL","ARTISAN","MUSICAL","GAMING","TOOLS_OTHER","MEDIUM"]},{"id":406,"display_name":"Choose a Skill","name":"choose-a-skill","choice_groups":["SKILLS"]},{"id":776,"display_name":"Choose a Skill or Tool","name":"choose-a-skill-or-tool","choice_groups":["SKILLS","ARTISAN","MUSICAL","GAMING","TOOLS_OTHER"]},{"id":1796,"display_name":"Choose a Skill, Simple Weapon, or Tool","name":"choose-a-skill-simple-weapon-or-tool","choice_groups":["SKILLS","SIMPLE","ARTISAN","MUSICAL","GAMING","TOOLS_OTHER"]},{"id":1778,"display_name":"Choose a Telepathic Skill","name":"choose-a-telepathic-skill","choice_groups":["SKILLS"],"only":["deception","insight","intimidation","persuasion"]},{"id":410,"display_name":"Choose a Tool","name":"choose-a-tool","choice_groups":["ARTISAN","MUSICAL","GAMING","TOOLS_OTHER"]},{"id":399,"display_name":"Choose a Weapon","name":"choose-a-weapon","choice_groups":["MARTIAL","SIMPLE"]},{"id":402,"display_name":"Choose an Armor Type","name":"choose-an-armor-type","choice_groups":["ARMOUR_TYPE"]},{"id":1749,"display_name":"Choose an Artificer Skill","name":"choose-an-artificer-skill","choice_groups":["SKILLS"],"only":["arcana","history","investigation","medicine","nature","perception","sleight-of-hand"]},{"id":407,"display_name":"Choose an Artisan's Tool","name":"choose-an-artisans-tool","choice_groups":["ARTISAN"]},{"id":792,"display_name":"Choose an Intelligence, Wisdom, or Charisma Skill","name":"choose-an-intelligence-wisdom-or-charisma-skill","choice_groups":["wisdom-scoreDOM_SKILLS","charisma-scoreRISMA_SKILLS","intelligence-scoreELLIGENCE_SKILLS"]},{"id":799,"display_name":"Choose an Urban Bounty Hunter Tool","name":"choose-an-urban-bounty-hunter-tool","choice_groups":["GAMING","MUSICAL"]},{"id":264,"display_name":"Club","name":"club"},{"id":325,"display_name":"Cobbler's Tools","name":"cobblers-tools"},{"id":232,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks"},{"id":225,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws"},{"id":326,"display_name":"Cook's Utensils","name":"cooks-utensils"},{"id":260,"display_name":"Crossbow, Hand","name":"crossbow-hand"},{"id":294,"display_name":"Crossbow, Heavy","name":"crossbow-heavy"},{"id":273,"display_name":"Crossbow, Light","name":"crossbow-light"},{"id":262,"display_name":"Dagger","name":"dagger"},{"id":274,"display_name":"Dart","name":"dart"},{"id":250,"display_name":"Deception","name":"deception"},{"id":231,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks"},{"id":258,"display_name":"Dexterity Attacks","name":"dexterity-attacks"},{"id":224,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws"},{"id":343,"display_name":"Dice Set","name":"dice-set"},{"id":337,"display_name":"Disguise Kit","name":"disguise-kit"},{"id":1721,"display_name":"Double-Bladed Scimitar","name":"doublebladed-scimitar"},{"id":1044,"display_name":"Dragonchess Set","name":"dragonchess-set"},{"id":346,"display_name":"Drum","name":"drum"},{"id":347,"display_name":"Dulcimer","name":"dulcimer"},{"id":1712,"display_name":"Firearms","name":"firearms"},{"id":278,"display_name":"Flail","name":"flail"},{"id":348,"display_name":"Flute","name":"flute"},{"id":338,"display_name":"Forgery Kit","name":"forgery-kit"},{"id":261,"display_name":"Glaive","name":"glaive"},{"id":327,"display_name":"Glassblower's Tools","name":"glassblowers-tools"},{"id":1816,"display_name":"Glaur","name":"glaur"},{"id":279,"display_name":"Greataxe","name":"greataxe"},{"id":265,"display_name":"Greatclub","name":"greatclub"},{"id":280,"display_name":"Greatsword","name":"greatsword"},{"id":281,"display_name":"Halberd","name":"halberd"},{"id":306,"display_name":"Half Plate","name":"half-plate"},{"id":1817,"display_name":"Hand Drum","name":"hand-drum"},{"id":266,"display_name":"Handaxe","name":"handaxe"},{"id":398,"display_name":"Heavy Armor","name":"heavy-armor"},{"id":339,"display_name":"Herbalism Kit","name":"herbalism-kit"},{"id":303,"display_name":"Hide","name":"hide"},{"id":241,"display_name":"History","name":"history"},{"id":351,"display_name":"Horn","name":"horn"},{"id":778,"display_name":"Improvised Weapons","name":"improvised-weapons"},{"id":259,"display_name":"Initiative","name":"initiative"},{"id":246,"display_name":"Insight","name":"insight"},{"id":233,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks"},{"id":226,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws"},{"id":251,"display_name":"Intimidation","name":"intimidation"},{"id":242,"display_name":"Investigation","name":"investigation"},{"id":267,"display_name":"Javelin","name":"javelin"},{"id":328,"display_name":"Jeweler's Tools","name":"jewelers-tools"},{"id":282,"display_name":"Lance","name":"lance"},{"id":302,"display_name":"Leather","name":"leather"},{"id":329,"display_name":"Leatherworker's Tools","name":"leatherworkers-tools"},{"id":396,"display_name":"Light Armor","name":"light-armor"},{"id":268,"display_name":"Light Hammer","name":"light-hammer"},{"id":295,"display_name":"Longbow","name":"longbow"},{"id":1818,"display_name":"Longhorn","name":"longhorn"},{"id":263,"display_name":"Longsword","name":"longsword"},{"id":349,"display_name":"Lute","name":"lute"},{"id":350,"display_name":"Lyre","name":"lyre"},{"id":269,"display_name":"Mace","name":"mace"},{"id":394,"display_name":"Martial Weapons","name":"martial-weapons"},{"id":330,"display_name":"Mason's Tools","name":"masons-tools"},{"id":283,"display_name":"Maul","name":"maul"},{"id":247,"display_name":"Medicine","name":"medicine"},{"id":397,"display_name":"Medium Armor","name":"medium-armor"},{"id":254,"display_name":"Melee Attacks","name":"melee-attacks"},{"id":299,"display_name":"Mithril Plate Armor","name":"mithril-plate-armor"},{"id":284,"display_name":"Morningstar","name":"morningstar"},{"id":243,"display_name":"Nature","name":"nature"},{"id":340,"display_name":"Navigator's Tools","name":"navigators-tools"},{"id":296,"display_name":"Net","name":"net"},{"id":301,"display_name":"Padded","name":"padded"},{"id":331,"display_name":"Painter's Supplies","name":"painters-supplies"},{"id":352,"display_name":"Pan Flute","name":"pan-flute"},{"id":248,"display_name":"Perception","name":"perception"},{"id":252,"display_name":"Performance","name":"performance"},{"id":253,"display_name":"Persuasion","name":"persuasion"},{"id":285,"display_name":"Pike","name":"pike"},{"id":310,"display_name":"Plate","name":"plate"},{"id":344,"display_name":"Playing Card Set","name":"playing-card-set"},{"id":341,"display_name":"Poisoner's Kit","name":"poisoners-kit"},{"id":332,"display_name":"Potter's Tools","name":"potters-tools"},{"id":270,"display_name":"Quarterstaff","name":"quarterstaff"},{"id":255,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":286,"display_name":"Rapier","name":"rapier"},{"id":244,"display_name":"Religion","name":"religion"},{"id":307,"display_name":"Ring Mail","name":"ring-mail"},{"id":222,"display_name":"Saving Throws","name":"saving-throws"},{"id":298,"display_name":"Scale Mail","name":"scale-mail"},{"id":287,"display_name":"Scimitar","name":"scimitar"},{"id":1042,"display_name":"Self","name":"self"},{"id":353,"display_name":"Shawm","name":"shawm"},{"id":694,"display_name":"Shields","name":"shields"},{"id":275,"display_name":"Shortbow","name":"shortbow"},{"id":288,"display_name":"Shortsword","name":"shortsword"},{"id":271,"display_name":"Sickle","name":"sickle"},{"id":395,"display_name":"Simple Weapons","name":"simple-weapons"},{"id":238,"display_name":"Sleight of Hand","name":"sleight-of-hand"},{"id":276,"display_name":"Sling","name":"sling"},{"id":333,"display_name":"Smith's Tools","name":"smiths-tools"},{"id":1819,"display_name":"Songhorn","name":"songhorn"},{"id":272,"display_name":"Spear","name":"spear"},{"id":256,"display_name":"Spell Attacks","name":"spell-attacks"},{"id":309,"display_name":"Splint","name":"splint"},{"id":239,"display_name":"Stealth","name":"stealth"},{"id":300,"display_name":"Steel Shield","name":"steel-shield"},{"id":230,"display_name":"Strength Ability Checks","name":"strength-ability-checks"},{"id":257,"display_name":"Strength Attacks","name":"strength-attacks"},{"id":223,"display_name":"Strength Saving Throws","name":"strength-saving-throws"},{"id":297,"display_name":"Studded Leather","name":"studded-leather"},{"id":249,"display_name":"Survival","name":"survival"},{"id":1820,"display_name":"Tantan","name":"tantan"},{"id":1821,"display_name":"Thelarr","name":"thelarr"},{"id":342,"display_name":"Thieves' Tools","name":"thieves-tools"},{"id":1043,"display_name":"Three-Dragon Ante Set","name":"threedragon-ante-set"},{"id":334,"display_name":"Tinker's Tools","name":"tinkers-tools"},{"id":1822,"display_name":"Tocken","name":"tocken"},{"id":289,"display_name":"Trident","name":"trident"},{"id":796,"display_name":"Vehicles (Land)","name":"vehicles-land"},{"id":797,"display_name":"Vehicles (Water)","name":"vehicles-water"},{"id":354,"display_name":"Viol","name":"viol"},{"id":290,"display_name":"War pick","name":"war-pick"},{"id":1823,"display_name":"Wargong","name":"wargong"},{"id":291,"display_name":"Warhammer","name":"warhammer"},{"id":335,"display_name":"Weaver's Tools","name":"weavers-tools"},{"id":292,"display_name":"Whip","name":"whip"},{"id":1824,"display_name":"Whistle-Stick","name":"whistlestick"},{"id":234,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks"},{"id":227,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws"},{"id":336,"display_name":"Woodcarver's Tools","name":"woodcarvers-tools"},{"id":1825,"display_name":"Yarting","name":"yarting"},{"id":1805,"display_name":"Yklwa","name":"yklwa"},{"id":1826,"display_name":"Zulkoon","name":"zulkoon"}],"11":[{"id":391,"display_name":"Aarakocra","name":"aarakocra"},{"id":365,"display_name":"Abyssal","name":"abyssal"},{"id":390,"display_name":"All","name":"all"},{"id":374,"display_name":"Aquan","name":"aquan"},{"id":375,"display_name":"Auran","name":"auran"},{"id":388,"display_name":"Blink Dog","name":"blink-dog"},{"id":366,"display_name":"Celestial","name":"celestial"},{"id":392,"display_name":"Choose a Language","name":"choose-a-language","choice_groups":["LANGUAGES","EXOTIC_LANGUAGES"]},{"id":1054,"display_name":"Choose an Exotic Language","name":"choose-an-exotic-language","choice_groups":["EXOTIC_LANGUAGES"]},{"id":1741,"display_name":"Choose Elvish or Vedalken","name":"choose-elvish-or-vedalken","choice_groups":["EXOTIC_LANGUAGES"],"only":["vedalken","elvish"]},{"id":357,"display_name":"Common","name":"common"},{"id":1776,"display_name":"Daelkyr","name":"daelkyr"},{"id":368,"display_name":"Deep Speech","name":"deep-speech"},{"id":367,"display_name":"Draconic","name":"draconic"},{"id":378,"display_name":"Druidic","name":"druidic"},{"id":358,"display_name":"Dwarvish","name":"dwarvish"},{"id":359,"display_name":"Elvish","name":"elvish"},{"id":360,"display_name":"Giant","name":"giant"},{"id":379,"display_name":"Giant Eagle","name":"giant-eagle"},{"id":380,"display_name":"Giant Elk","name":"giant-elk"},{"id":381,"display_name":"Giant Owl","name":"giant-owl"},{"id":1713,"display_name":"Gith","name":"gith"},{"id":382,"display_name":"Gnoll","name":"gnoll"},{"id":361,"display_name":"Gnomish","name":"gnomish"},{"id":362,"display_name":"Goblin","name":"goblin"},{"id":1827,"display_name":"Grippli","name":"grippli"},{"id":1781,"display_name":"Grung","name":"grung"},{"id":363,"display_name":"Halfling","name":"halfling"},{"id":376,"display_name":"Ignan","name":"ignan"},{"id":369,"display_name":"Infernal","name":"infernal"},{"id":1738,"display_name":"Kraul","name":"kraul"},{"id":1790,"display_name":"Leonin","name":"leonin"},{"id":1736,"display_name":"Loxodon","name":"loxodon"},{"id":1787,"display_name":"Marquesian","name":"marquesian"},{"id":1737,"display_name":"Minotaur","name":"minotaur"},{"id":1788,"display_name":"Naush","name":"naush"},{"id":364,"display_name":"Orc","name":"orc"},{"id":383,"display_name":"Otyugh","name":"otyugh"},{"id":370,"display_name":"Primordial","name":"primordial"},{"id":1722,"display_name":"Quori","name":"quori"},{"id":1777,"display_name":"Riedran","name":"riedran"},{"id":384,"display_name":"Sahuagin","name":"sahuagin"},{"id":385,"display_name":"Sphinx","name":"sphinx"},{"id":371,"display_name":"Sylvan","name":"sylvan"},{"id":373,"display_name":"Telepathy","name":"telepathy"},{"id":377,"display_name":"Terran","name":"terran"},{"id":1783,"display_name":"Thievesâ€™ Cant","name":"thievesâ€™-cant"},{"id":372,"display_name":"Undercommon","name":"undercommon"},{"id":1739,"display_name":"Vedalken","name":"vedalken"},{"id":386,"display_name":"Winter Wolf","name":"winter-wolf"},{"id":387,"display_name":"Worg","name":"worg"},{"id":389,"display_name":"Yeti","name":"yeti"},{"id":1786,"display_name":"Zemnian","name":"zemnian"}],"12":[{"id":419,"display_name":"Ability Checks","name":"ability-checks"},{"id":427,"display_name":"Acrobatics","name":"acrobatics"},{"id":501,"display_name":"Alchemist's Supplies","name":"alchemists-supplies"},{"id":435,"display_name":"Animal Handling","name":"animal-handling"},{"id":430,"display_name":"Arcana","name":"arcana"},{"id":426,"display_name":"Athletics","name":"athletics"},{"id":526,"display_name":"Bagpipes","name":"bagpipes"},{"id":467,"display_name":"Battleaxe","name":"battleaxe"},{"id":483,"display_name":"Blowgun","name":"blowgun"},{"id":495,"display_name":"Breastplate","name":"breastplate"},{"id":502,"display_name":"Brewer's Supplies","name":"brewers-supplies"},{"id":503,"display_name":"Calligrapher's Supplies","name":"calligraphers-supplies"},{"id":504,"display_name":"Carpenter's Tools","name":"carpenters-tools"},{"id":505,"display_name":"Cartographer's Tools","name":"cartographers-tools"},{"id":498,"display_name":"Chain Mail","name":"chain-mail"},{"id":494,"display_name":"Chain Shirt","name":"chain-shirt"},{"id":425,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks"},{"id":418,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws"},{"id":550,"display_name":"Choose a Gaming Set","name":"choose-a-gaming-set","choice_groups":["GAMING"]},{"id":547,"display_name":"Choose a Heavy Armor Type","name":"choose-a-heavy-armor-type","choice_groups":["HEAVY"]},{"id":545,"display_name":"Choose a Light Armor Type","name":"choose-a-light-armor-type","choice_groups":["LIGHT"]},{"id":542,"display_name":"Choose a Martial Weapon","name":"choose-a-martial-weapon","choice_groups":["MARTIAL"]},{"id":546,"display_name":"Choose a Medium Armor Type","name":"choose-a-medium-armor-type","choice_groups":["MEDIUM"]},{"id":551,"display_name":"Choose a Musical Instrument","name":"choose-a-musical-instrument","choice_groups":["MUSICAL"]},{"id":543,"display_name":"Choose a Simple Weapon","name":"choose-a-simple-weapon","choice_groups":["SIMPLE"]},{"id":548,"display_name":"Choose a Skill","name":"choose-a-skill","choice_groups":["SKILLS"]},{"id":1794,"display_name":"Choose a Skill or Tool","name":"choose-a-skill-or-tool","choice_groups":["SKILLS","ARTISAN","MUSICAL","GAMING","TOOLS_OTHER"]},{"id":552,"display_name":"Choose a Tool","name":"choose-a-tool","choice_groups":["ARTISAN","MUSICAL","GAMING","TOOLS_OTHER"]},{"id":541,"display_name":"Choose a Weapon","name":"choose-a-weapon","choice_groups":["SIMPLE","MARTIAL"]},{"id":544,"display_name":"Choose an Armor Type","name":"choose-an-armor-type","choice_groups":["ARMOUR_TYPE"]},{"id":549,"display_name":"Choose an Artisan's Tool","name":"choose-an-artisans-tool","choice_groups":["ARTISAN"]},{"id":807,"display_name":"Choose Rogue Expertise","name":"choose-rogue-expertise","choice_groups":["SKILLS","TOOLS_OTHER"],"only":["thieves-tools","acrobatics","stealth"]},{"id":454,"display_name":"Club","name":"club"},{"id":506,"display_name":"Cobbler's Tools","name":"cobblers-tools"},{"id":422,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks"},{"id":415,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws"},{"id":507,"display_name":"Cook's Utensils","name":"cooks-utensils"},{"id":450,"display_name":"Crossbow, Hand","name":"crossbow-hand"},{"id":484,"display_name":"Crossbow, Heavy","name":"crossbow-heavy"},{"id":463,"display_name":"Crossbow, Light","name":"crossbow-light"},{"id":452,"display_name":"Dagger","name":"dagger"},{"id":464,"display_name":"Dart","name":"dart"},{"id":440,"display_name":"Deception","name":"deception"},{"id":421,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks"},{"id":448,"display_name":"Dexterity Attacks","name":"dexterity-attacks"},{"id":414,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws"},{"id":524,"display_name":"Dice Set","name":"dice-set"},{"id":518,"display_name":"Disguise Kit","name":"disguise-kit"},{"id":527,"display_name":"Drum","name":"drum"},{"id":528,"display_name":"Dulcimer","name":"dulcimer"},{"id":468,"display_name":"Flail","name":"flail"},{"id":529,"display_name":"Flute","name":"flute"},{"id":519,"display_name":"Forgery Kit","name":"forgery-kit"},{"id":451,"display_name":"Glaive","name":"glaive"},{"id":508,"display_name":"Glassblower's Tools","name":"glassblowers-tools"},{"id":469,"display_name":"Greataxe","name":"greataxe"},{"id":455,"display_name":"Greatclub","name":"greatclub"},{"id":470,"display_name":"Greatsword","name":"greatsword"},{"id":471,"display_name":"Halberd","name":"halberd"},{"id":496,"display_name":"Half Plate","name":"half-plate"},{"id":456,"display_name":"Handaxe","name":"handaxe"},{"id":540,"display_name":"Heavy Armor","name":"heavy-armor"},{"id":520,"display_name":"Herbalism Kit","name":"herbalism-kit"},{"id":493,"display_name":"Hide","name":"hide"},{"id":431,"display_name":"History","name":"history"},{"id":532,"display_name":"Horn","name":"horn"},{"id":449,"display_name":"Initiative","name":"initiative"},{"id":436,"display_name":"Insight","name":"insight"},{"id":423,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks"},{"id":416,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws"},{"id":441,"display_name":"Intimidation","name":"intimidation"},{"id":432,"display_name":"Investigation","name":"investigation"},{"id":457,"display_name":"Javelin","name":"javelin"},{"id":509,"display_name":"Jeweler's Tools","name":"jewelers-tools"},{"id":472,"display_name":"Lance","name":"lance"},{"id":492,"display_name":"Leather","name":"leather"},{"id":510,"display_name":"Leatherworker's Tools","name":"leatherworkers-tools"},{"id":538,"display_name":"Light Armor","name":"light-armor"},{"id":458,"display_name":"Light Hammer","name":"light-hammer"},{"id":485,"display_name":"Longbow","name":"longbow"},{"id":453,"display_name":"Longsword","name":"longsword"},{"id":530,"display_name":"Lute","name":"lute"},{"id":531,"display_name":"Lyre","name":"lyre"},{"id":459,"display_name":"Mace","name":"mace"},{"id":536,"display_name":"Martial Weapons","name":"martial-weapons"},{"id":511,"display_name":"Mason's Tools","name":"masons-tools"},{"id":473,"display_name":"Maul","name":"maul"},{"id":437,"display_name":"Medicine","name":"medicine"},{"id":539,"display_name":"Medium Armor","name":"medium-armor"},{"id":444,"display_name":"Melee Attacks","name":"melee-attacks"},{"id":489,"display_name":"Mithril Plate Armor","name":"mithril-plate-armor"},{"id":474,"display_name":"Morningstar","name":"morningstar"},{"id":433,"display_name":"Nature","name":"nature"},{"id":521,"display_name":"Navigator's Tools","name":"navigators-tools"},{"id":486,"display_name":"Net","name":"net"},{"id":491,"display_name":"Padded","name":"padded"},{"id":512,"display_name":"Painter's Supplies","name":"painters-supplies"},{"id":533,"display_name":"Pan Flute","name":"pan-flute"},{"id":438,"display_name":"Perception","name":"perception"},{"id":442,"display_name":"Performance","name":"performance"},{"id":443,"display_name":"Persuasion","name":"persuasion"},{"id":475,"display_name":"Pike","name":"pike"},{"id":500,"display_name":"Plate","name":"plate"},{"id":525,"display_name":"Playing Card Set","name":"playing-card-set"},{"id":522,"display_name":"Poisoner's Kit","name":"poisoners-kit"},{"id":513,"display_name":"Potter's Tools","name":"potters-tools"},{"id":460,"display_name":"Quarterstaff","name":"quarterstaff"},{"id":445,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":476,"display_name":"Rapier","name":"rapier"},{"id":434,"display_name":"Religion","name":"religion"},{"id":497,"display_name":"Ring Mail","name":"ring-mail"},{"id":412,"display_name":"Saving Throws","name":"saving-throws"},{"id":488,"display_name":"Scale Mail","name":"scale-mail"},{"id":477,"display_name":"Scimitar","name":"scimitar"},{"id":534,"display_name":"Shawm","name":"shawm"},{"id":465,"display_name":"Shortbow","name":"shortbow"},{"id":478,"display_name":"Shortsword","name":"shortsword"},{"id":461,"display_name":"Sickle","name":"sickle"},{"id":537,"display_name":"Simple Weapons","name":"simple-weapons"},{"id":428,"display_name":"Sleight of Hand","name":"sleight-of-hand"},{"id":466,"display_name":"Sling","name":"sling"},{"id":514,"display_name":"Smith's Tools","name":"smiths-tools"},{"id":462,"display_name":"Spear","name":"spear"},{"id":446,"display_name":"Spell Attacks","name":"spell-attacks"},{"id":499,"display_name":"Splint","name":"splint"},{"id":429,"display_name":"Stealth","name":"stealth"},{"id":490,"display_name":"Steel Shield","name":"steel-shield"},{"id":420,"display_name":"Strength Ability Checks","name":"strength-ability-checks"},{"id":447,"display_name":"Strength Attacks","name":"strength-attacks"},{"id":413,"display_name":"Strength Saving Throws","name":"strength-saving-throws"},{"id":487,"display_name":"Studded Leather","name":"studded-leather"},{"id":439,"display_name":"Survival","name":"survival"},{"id":523,"display_name":"Thieves' Tools","name":"thieves-tools"},{"id":515,"display_name":"Tinker's Tools","name":"tinkers-tools"},{"id":479,"display_name":"Trident","name":"trident"},{"id":535,"display_name":"Viol","name":"viol"},{"id":480,"display_name":"War pick","name":"war-pick"},{"id":481,"display_name":"Warhammer","name":"warhammer"},{"id":516,"display_name":"Weaver's Tools","name":"weavers-tools"},{"id":482,"display_name":"Whip","name":"whip"},{"id":424,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks"},{"id":417,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws"},{"id":517,"display_name":"Woodcarver's Tools","name":"woodcarvers-tools"}],"13":[{"id":560,"display_name":"Ability Checks","name":"ability-checks"},{"id":568,"display_name":"Acrobatics","name":"acrobatics"},{"id":642,"display_name":"Alchemist's Supplies","name":"alchemists-supplies"},{"id":576,"display_name":"Animal Handling","name":"animal-handling"},{"id":571,"display_name":"Arcana","name":"arcana"},{"id":567,"display_name":"Athletics","name":"athletics"},{"id":667,"display_name":"Bagpipes","name":"bagpipes"},{"id":608,"display_name":"Battleaxe","name":"battleaxe"},{"id":624,"display_name":"Blowgun","name":"blowgun"},{"id":636,"display_name":"Breastplate","name":"breastplate"},{"id":643,"display_name":"Brewer's Supplies","name":"brewers-supplies"},{"id":644,"display_name":"Calligrapher's Supplies","name":"calligraphers-supplies"},{"id":645,"display_name":"Carpenter's Tools","name":"carpenters-tools"},{"id":646,"display_name":"Cartographer's Tools","name":"cartographers-tools"},{"id":639,"display_name":"Chain Mail","name":"chain-mail"},{"id":635,"display_name":"Chain Shirt","name":"chain-shirt"},{"id":566,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks"},{"id":559,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws"},{"id":595,"display_name":"Club","name":"club"},{"id":647,"display_name":"Cobbler's Tools","name":"cobblers-tools"},{"id":563,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks"},{"id":556,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws"},{"id":648,"display_name":"Cook's Utensils","name":"cooks-utensils"},{"id":591,"display_name":"Crossbow, Hand","name":"crossbow-hand"},{"id":625,"display_name":"Crossbow, Heavy","name":"crossbow-heavy"},{"id":604,"display_name":"Crossbow, Light","name":"crossbow-light"},{"id":593,"display_name":"Dagger","name":"dagger"},{"id":605,"display_name":"Dart","name":"dart"},{"id":581,"display_name":"Deception","name":"deception"},{"id":562,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks"},{"id":589,"display_name":"Dexterity Attacks","name":"dexterity-attacks"},{"id":555,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws"},{"id":665,"display_name":"Dice Set","name":"dice-set"},{"id":659,"display_name":"Disguise Kit","name":"disguise-kit"},{"id":668,"display_name":"Drum","name":"drum"},{"id":669,"display_name":"Dulcimer","name":"dulcimer"},{"id":609,"display_name":"Flail","name":"flail"},{"id":670,"display_name":"Flute","name":"flute"},{"id":660,"display_name":"Forgery Kit","name":"forgery-kit"},{"id":592,"display_name":"Glaive","name":"glaive"},{"id":649,"display_name":"Glassblower's Tools","name":"glassblowers-tools"},{"id":610,"display_name":"Greataxe","name":"greataxe"},{"id":596,"display_name":"Greatclub","name":"greatclub"},{"id":611,"display_name":"Greatsword","name":"greatsword"},{"id":612,"display_name":"Halberd","name":"halberd"},{"id":637,"display_name":"Half Plate","name":"half-plate"},{"id":597,"display_name":"Handaxe","name":"handaxe"},{"id":681,"display_name":"Heavy Armor","name":"heavy-armor"},{"id":661,"display_name":"Herbalism Kit","name":"herbalism-kit"},{"id":634,"display_name":"Hide","name":"hide"},{"id":572,"display_name":"History","name":"history"},{"id":673,"display_name":"Horn","name":"horn"},{"id":590,"display_name":"Initiative","name":"initiative"},{"id":577,"display_name":"Insight","name":"insight"},{"id":564,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks"},{"id":557,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws"},{"id":582,"display_name":"Intimidation","name":"intimidation"},{"id":573,"display_name":"Investigation","name":"investigation"},{"id":598,"display_name":"Javelin","name":"javelin"},{"id":650,"display_name":"Jeweler's Tools","name":"jewelers-tools"},{"id":613,"display_name":"Lance","name":"lance"},{"id":633,"display_name":"Leather","name":"leather"},{"id":651,"display_name":"Leatherworker's Tools","name":"leatherworkers-tools"},{"id":679,"display_name":"Light Armor","name":"light-armor"},{"id":599,"display_name":"Light Hammer","name":"light-hammer"},{"id":626,"display_name":"Longbow","name":"longbow"},{"id":594,"display_name":"Longsword","name":"longsword"},{"id":671,"display_name":"Lute","name":"lute"},{"id":672,"display_name":"Lyre","name":"lyre"},{"id":600,"display_name":"Mace","name":"mace"},{"id":677,"display_name":"Martial Weapons","name":"martial-weapons"},{"id":652,"display_name":"Mason's Tools","name":"masons-tools"},{"id":614,"display_name":"Maul","name":"maul"},{"id":578,"display_name":"Medicine","name":"medicine"},{"id":680,"display_name":"Medium Armor","name":"medium-armor"},{"id":585,"display_name":"Melee Attacks","name":"melee-attacks"},{"id":630,"display_name":"Mithril Plate Armor","name":"mithril-plate-armor"},{"id":615,"display_name":"Morningstar","name":"morningstar"},{"id":574,"display_name":"Nature","name":"nature"},{"id":662,"display_name":"Navigator's Tools","name":"navigators-tools"},{"id":627,"display_name":"Net","name":"net"},{"id":632,"display_name":"Padded","name":"padded"},{"id":653,"display_name":"Painter's Supplies","name":"painters-supplies"},{"id":674,"display_name":"Pan Flute","name":"pan-flute"},{"id":579,"display_name":"Perception","name":"perception"},{"id":583,"display_name":"Performance","name":"performance"},{"id":584,"display_name":"Persuasion","name":"persuasion"},{"id":616,"display_name":"Pike","name":"pike"},{"id":641,"display_name":"Plate","name":"plate"},{"id":666,"display_name":"Playing Card Set","name":"playing-card-set"},{"id":663,"display_name":"Poisoner's Kit","name":"poisoners-kit"},{"id":654,"display_name":"Potter's Tools","name":"potters-tools"},{"id":601,"display_name":"Quarterstaff","name":"quarterstaff"},{"id":586,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":617,"display_name":"Rapier","name":"rapier"},{"id":575,"display_name":"Religion","name":"religion"},{"id":638,"display_name":"Ring Mail","name":"ring-mail"},{"id":553,"display_name":"Saving Throws","name":"saving-throws"},{"id":629,"display_name":"Scale Mail","name":"scale-mail"},{"id":618,"display_name":"Scimitar","name":"scimitar"},{"id":675,"display_name":"Shawm","name":"shawm"},{"id":606,"display_name":"Shortbow","name":"shortbow"},{"id":619,"display_name":"Shortsword","name":"shortsword"},{"id":602,"display_name":"Sickle","name":"sickle"},{"id":678,"display_name":"Simple Weapons","name":"simple-weapons"},{"id":569,"display_name":"Sleight of Hand","name":"sleight-of-hand"},{"id":607,"display_name":"Sling","name":"sling"},{"id":655,"display_name":"Smith's Tools","name":"smiths-tools"},{"id":603,"display_name":"Spear","name":"spear"},{"id":587,"display_name":"Spell Attacks","name":"spell-attacks"},{"id":640,"display_name":"Splint","name":"splint"},{"id":570,"display_name":"Stealth","name":"stealth"},{"id":631,"display_name":"Steel Shield","name":"steel-shield"},{"id":561,"display_name":"Strength Ability Checks","name":"strength-ability-checks"},{"id":588,"display_name":"Strength Attacks","name":"strength-attacks"},{"id":554,"display_name":"Strength Saving Throws","name":"strength-saving-throws"},{"id":628,"display_name":"Studded Leather","name":"studded-leather"},{"id":580,"display_name":"Survival","name":"survival"},{"id":664,"display_name":"Thieves' Tools","name":"thieves-tools"},{"id":656,"display_name":"Tinker's Tools","name":"tinkers-tools"},{"id":620,"display_name":"Trident","name":"trident"},{"id":676,"display_name":"Viol","name":"viol"},{"id":621,"display_name":"War pick","name":"war-pick"},{"id":622,"display_name":"Warhammer","name":"warhammer"},{"id":657,"display_name":"Weaver's Tools","name":"weavers-tools"},{"id":623,"display_name":"Whip","name":"whip"},{"id":565,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks"},{"id":558,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws"},{"id":658,"display_name":"Woodcarver's Tools","name":"woodcarvers-tools"}],"14":[{"id":702,"display_name":"Actor","name":"actor"},{"id":703,"display_name":"Alert","name":"alert"},{"id":704,"display_name":"Athlete","name":"athlete"},{"id":705,"display_name":"Charger","name":"charger"},{"id":696,"display_name":"Choose a Feat","name":"choose-a-feat","choice_groups":["SPECIAL_FEATS"]},{"id":706,"display_name":"Crossbow Expert","name":"crossbow-expert"},{"id":707,"display_name":"Defensive Duelist","name":"defensive-duelist"},{"id":708,"display_name":"Dual Wielder","name":"dual-wielder"},{"id":709,"display_name":"Dungeon Delver","name":"dungeon-delver"},{"id":710,"display_name":"Durable","name":"durable"},{"id":711,"display_name":"Elemental Adept","name":"elemental-adept"},{"id":701,"display_name":"Grappler","name":"grappler"},{"id":712,"display_name":"Great Weapon Master","name":"great-weapon-master"},{"id":713,"display_name":"Healer","name":"healer"},{"id":714,"display_name":"Heavily Armored","name":"heavily-armored"},{"id":715,"display_name":"Heavy Armor Master","name":"heavy-armor-master"},{"id":716,"display_name":"Inspiring Leader","name":"inspiring-leader"},{"id":717,"display_name":"Keen Mind","name":"keen-mind"},{"id":718,"display_name":"Lightly Armored","name":"lightly-armored"},{"id":719,"display_name":"Linguist","name":"linguist"},{"id":720,"display_name":"Lucky","name":"lucky"},{"id":721,"display_name":"Mage Slayer","name":"mage-slayer"},{"id":722,"display_name":"Magic Initiate","name":"magic-initiate"},{"id":723,"display_name":"Martial Adept","name":"martial-adept"},{"id":724,"display_name":"Medium Armor Master","name":"medium-armor-master"},{"id":725,"display_name":"Mobile","name":"mobile"},{"id":726,"display_name":"Moderately Armored","name":"moderately-armored"},{"id":727,"display_name":"Mounted Combatant","name":"mounted-combatant"},{"id":728,"display_name":"Observant","name":"observant"},{"id":729,"display_name":"Polearm Master","name":"polearm-master"},{"id":730,"display_name":"Resilient","name":"resilient"},{"id":731,"display_name":"Ritual Caster","name":"ritual-caster"},{"id":743,"display_name":"Savage Attacker","name":"savage-attacker"},{"id":732,"display_name":"Sentinel","name":"sentinel"},{"id":733,"display_name":"Sharpshooter","name":"sharpshooter"},{"id":734,"display_name":"Shield Master","name":"shield-master"},{"id":735,"display_name":"Skilled","name":"skilled"},{"id":736,"display_name":"Skulker","name":"skulker"},{"id":737,"display_name":"Spell Sniper","name":"spell-sniper"},{"id":738,"display_name":"Svirfneblin Magic","name":"svirfneblin-magic"},{"id":739,"display_name":"Tavern Brawler","name":"tavern-brawler"},{"id":740,"display_name":"Tough","name":"tough"},{"id":741,"display_name":"War Caster","name":"war-caster"},{"id":742,"display_name":"Weapon Master","name":"weapon-master"}],"17":[{"id":750,"display_name":"Gargantuan","name":"gargantuan"},{"id":749,"display_name":"Huge","name":"huge"},{"id":748,"display_name":"Large","name":"large"},{"id":747,"display_name":"Medium","name":"medium"},{"id":842,"display_name":"Multiplier","name":"multiplier"},{"id":746,"display_name":"Small","name":"small"},{"id":745,"display_name":"Tiny","name":"tiny"}],"19":[{"id":755,"display_name":"Bludgeoning","name":"bludgeoning"},{"id":756,"display_name":"Piercing","name":"piercing"},{"id":754,"display_name":"Slashing","name":"slashing"}],"23":[{"id":772,"display_name":"Impose","name":"impose"},{"id":771,"display_name":"Remove","name":"remove"}],"24":[{"id":813,"display_name":"Impose","name":"impose"},{"id":812,"display_name":"Remove","name":"remove"}],"25":[{"id":815,"display_name":"Acid","name":"acid"},{"id":816,"display_name":"Bludgeoning","name":"bludgeoning"},{"id":817,"display_name":"Cold","name":"cold"},{"id":818,"display_name":"Fire","name":"fire"},{"id":819,"display_name":"Force","name":"force"},{"id":820,"display_name":"Lightning","name":"lightning"},{"id":821,"display_name":"Necrotic","name":"necrotic"},{"id":822,"display_name":"Piercing","name":"piercing"},{"id":823,"display_name":"Poison","name":"poison"},{"id":824,"display_name":"Psychic","name":"psychic"},{"id":825,"display_name":"Radiant","name":"radiant"},{"id":826,"display_name":"Slashing","name":"slashing"},{"id":827,"display_name":"Thunder","name":"thunder"}],"26":[{"id":828,"display_name":"Acid","name":"acid"},{"id":829,"display_name":"Bludgeoning","name":"bludgeoning"},{"id":830,"display_name":"Cold","name":"cold"},{"id":831,"display_name":"Fire","name":"fire"},{"id":832,"display_name":"Force","name":"force"},{"id":833,"display_name":"Lightning","name":"lightning"},{"id":834,"display_name":"Necrotic","name":"necrotic"},{"id":835,"display_name":"Piercing","name":"piercing"},{"id":836,"display_name":"Poison","name":"poison"},{"id":837,"display_name":"Psychic","name":"psychic"},{"id":838,"display_name":"Radiant","name":"radiant"},{"id":839,"display_name":"Slashing","name":"slashing"},{"id":840,"display_name":"Thunder","name":"thunder"}],"28":[{"id":861,"display_name":"Finesse","name":"finesse"},{"id":865,"display_name":"Heavy","name":"heavy"},{"id":866,"display_name":"Light","name":"light"},{"id":867,"display_name":"Loading","name":"loading"},{"id":868,"display_name":"Range","name":"range"},{"id":869,"display_name":"Reach","name":"reach"},{"id":862,"display_name":"Thrown","name":"thrown"},{"id":864,"display_name":"Two-Handed","name":"twohanded"},{"id":863,"display_name":"Versatile","name":"versatile"}],"29":[{"id":877,"display_name":"Ability Checks","name":"ability-checks"},{"id":885,"display_name":"Acrobatics","name":"acrobatics"},{"id":959,"display_name":"Alchemist's Supplies","name":"alchemists-supplies"},{"id":893,"display_name":"Animal Handling","name":"animal-handling"},{"id":888,"display_name":"Arcana","name":"arcana"},{"id":884,"display_name":"Athletics","name":"athletics"},{"id":984,"display_name":"Bagpipes","name":"bagpipes"},{"id":925,"display_name":"Battleaxe","name":"battleaxe"},{"id":941,"display_name":"Blowgun","name":"blowgun"},{"id":953,"display_name":"Breastplate","name":"breastplate"},{"id":960,"display_name":"Brewer's Supplies","name":"brewers-supplies"},{"id":961,"display_name":"Calligrapher's Supplies","name":"calligraphers-supplies"},{"id":962,"display_name":"Carpenter's Tools","name":"carpenters-tools"},{"id":963,"display_name":"Cartographer's Tools","name":"cartographers-tools"},{"id":956,"display_name":"Chain Mail","name":"chain-mail"},{"id":952,"display_name":"Chain Shirt","name":"chain-shirt"},{"id":883,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks"},{"id":876,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws"},{"id":912,"display_name":"Club","name":"club"},{"id":964,"display_name":"Cobbler's Tools","name":"cobblers-tools"},{"id":880,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks"},{"id":873,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws"},{"id":965,"display_name":"Cook's Utensils","name":"cooks-utensils"},{"id":908,"display_name":"Crossbow, Hand","name":"crossbow-hand"},{"id":942,"display_name":"Crossbow, Heavy","name":"crossbow-heavy"},{"id":921,"display_name":"Crossbow, Light","name":"crossbow-light"},{"id":910,"display_name":"Dagger","name":"dagger"},{"id":922,"display_name":"Dart","name":"dart"},{"id":898,"display_name":"Deception","name":"deception"},{"id":879,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks"},{"id":906,"display_name":"Dexterity Attacks","name":"dexterity-attacks"},{"id":872,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws"},{"id":982,"display_name":"Dice Set","name":"dice-set"},{"id":976,"display_name":"Disguise Kit","name":"disguise-kit"},{"id":985,"display_name":"Drum","name":"drum"},{"id":986,"display_name":"Dulcimer","name":"dulcimer"},{"id":926,"display_name":"Flail","name":"flail"},{"id":987,"display_name":"Flute","name":"flute"},{"id":977,"display_name":"Forgery Kit","name":"forgery-kit"},{"id":909,"display_name":"Glaive","name":"glaive"},{"id":966,"display_name":"Glassblower's Tools","name":"glassblowers-tools"},{"id":927,"display_name":"Greataxe","name":"greataxe"},{"id":913,"display_name":"Greatclub","name":"greatclub"},{"id":928,"display_name":"Greatsword","name":"greatsword"},{"id":929,"display_name":"Halberd","name":"halberd"},{"id":954,"display_name":"Half Plate","name":"half-plate"},{"id":914,"display_name":"Handaxe","name":"handaxe"},{"id":998,"display_name":"Heavy Armor","name":"heavy-armor"},{"id":978,"display_name":"Herbalism Kit","name":"herbalism-kit"},{"id":951,"display_name":"Hide","name":"hide"},{"id":889,"display_name":"History","name":"history"},{"id":990,"display_name":"Horn","name":"horn"},{"id":1000,"display_name":"Improvised Weapons","name":"improvised-weapons"},{"id":907,"display_name":"Initiative","name":"initiative"},{"id":894,"display_name":"Insight","name":"insight"},{"id":881,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks"},{"id":874,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws"},{"id":899,"display_name":"Intimidation","name":"intimidation"},{"id":890,"display_name":"Investigation","name":"investigation"},{"id":915,"display_name":"Javelin","name":"javelin"},{"id":967,"display_name":"Jeweler's Tools","name":"jewelers-tools"},{"id":930,"display_name":"Lance","name":"lance"},{"id":950,"display_name":"Leather","name":"leather"},{"id":968,"display_name":"Leatherworker's Tools","name":"leatherworkers-tools"},{"id":996,"display_name":"Light Armor","name":"light-armor"},{"id":916,"display_name":"Light Hammer","name":"light-hammer"},{"id":943,"display_name":"Longbow","name":"longbow"},{"id":911,"display_name":"Longsword","name":"longsword"},{"id":988,"display_name":"Lute","name":"lute"},{"id":989,"display_name":"Lyre","name":"lyre"},{"id":917,"display_name":"Mace","name":"mace"},{"id":994,"display_name":"Martial Weapons","name":"martial-weapons"},{"id":969,"display_name":"Mason's Tools","name":"masons-tools"},{"id":931,"display_name":"Maul","name":"maul"},{"id":895,"display_name":"Medicine","name":"medicine"},{"id":997,"display_name":"Medium Armor","name":"medium-armor"},{"id":902,"display_name":"Melee Attacks","name":"melee-attacks"},{"id":947,"display_name":"Mithril Plate Armor","name":"mithril-plate-armor"},{"id":932,"display_name":"Morningstar","name":"morningstar"},{"id":891,"display_name":"Nature","name":"nature"},{"id":979,"display_name":"Navigator's Tools","name":"navigators-tools"},{"id":944,"display_name":"Net","name":"net"},{"id":949,"display_name":"Padded","name":"padded"},{"id":970,"display_name":"Painter's Supplies","name":"painters-supplies"},{"id":991,"display_name":"Pan Flute","name":"pan-flute"},{"id":896,"display_name":"Perception","name":"perception"},{"id":900,"display_name":"Performance","name":"performance"},{"id":901,"display_name":"Persuasion","name":"persuasion"},{"id":933,"display_name":"Pike","name":"pike"},{"id":958,"display_name":"Plate","name":"plate"},{"id":983,"display_name":"Playing Card Set","name":"playing-card-set"},{"id":980,"display_name":"Poisoner's Kit","name":"poisoners-kit"},{"id":971,"display_name":"Potter's Tools","name":"potters-tools"},{"id":918,"display_name":"Quarterstaff","name":"quarterstaff"},{"id":903,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":934,"display_name":"Rapier","name":"rapier"},{"id":892,"display_name":"Religion","name":"religion"},{"id":955,"display_name":"Ring Mail","name":"ring-mail"},{"id":870,"display_name":"Saving Throws","name":"saving-throws"},{"id":946,"display_name":"Scale Mail","name":"scale-mail"},{"id":935,"display_name":"Scimitar","name":"scimitar"},{"id":992,"display_name":"Shawm","name":"shawm"},{"id":999,"display_name":"Shields","name":"shields"},{"id":923,"display_name":"Shortbow","name":"shortbow"},{"id":936,"display_name":"Shortsword","name":"shortsword"},{"id":919,"display_name":"Sickle","name":"sickle"},{"id":995,"display_name":"Simple Weapons","name":"simple-weapons"},{"id":886,"display_name":"Sleight of Hand","name":"sleight-of-hand"},{"id":924,"display_name":"Sling","name":"sling"},{"id":972,"display_name":"Smith's Tools","name":"smiths-tools"},{"id":920,"display_name":"Spear","name":"spear"},{"id":904,"display_name":"Spell Attacks","name":"spell-attacks"},{"id":957,"display_name":"Splint","name":"splint"},{"id":887,"display_name":"Stealth","name":"stealth"},{"id":948,"display_name":"Steel Shield","name":"steel-shield"},{"id":878,"display_name":"Strength Ability Checks","name":"strength-ability-checks"},{"id":905,"display_name":"Strength Attacks","name":"strength-attacks"},{"id":871,"display_name":"Strength Saving Throws","name":"strength-saving-throws"},{"id":945,"display_name":"Studded Leather","name":"studded-leather"},{"id":897,"display_name":"Survival","name":"survival"},{"id":981,"display_name":"Thieves' Tools","name":"thieves-tools"},{"id":973,"display_name":"Tinker's Tools","name":"tinkers-tools"},{"id":937,"display_name":"Trident","name":"trident"},{"id":1001,"display_name":"Vehicles (Land)","name":"vehicles-land"},{"id":1002,"display_name":"Vehicles (Water)","name":"vehicles-water"},{"id":993,"display_name":"Viol","name":"viol"},{"id":938,"display_name":"War pick","name":"war-pick"},{"id":939,"display_name":"Warhammer","name":"warhammer"},{"id":974,"display_name":"Weaver's Tools","name":"weavers-tools"},{"id":940,"display_name":"Whip","name":"whip"},{"id":882,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks"},{"id":875,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws"},{"id":975,"display_name":"Woodcarver's Tools","name":"woodcarvers-tools"}],"30":[{"id":1007,"display_name":"Aberrations","name":"aberrations"},{"id":1008,"display_name":"Beasts","name":"beasts"},{"id":1009,"display_name":"Celestials","name":"celestials"},{"id":1010,"display_name":"Constructs","name":"constructs"},{"id":1025,"display_name":"Dragonborn","name":"dragonborn"},{"id":1011,"display_name":"Dragons","name":"dragons"},{"id":1021,"display_name":"Dwarves","name":"dwarves"},{"id":1012,"display_name":"Elementals","name":"elementals"},{"id":1022,"display_name":"Elves","name":"elves"},{"id":1013,"display_name":"Fey","name":"fey"},{"id":1014,"display_name":"Fiends","name":"fiends"},{"id":1015,"display_name":"Giants","name":"giants"},{"id":1030,"display_name":"Gnolls","name":"gnolls"},{"id":1024,"display_name":"Gnomes","name":"gnomes"},{"id":1028,"display_name":"Goblinoids","name":"goblinoids"},{"id":1026,"display_name":"Grimlock","name":"grimlock"},{"id":1023,"display_name":"Halflings","name":"halflings"},{"id":1020,"display_name":"Humans","name":"humans"},{"id":1032,"display_name":"Kobolds","name":"kobolds"},{"id":1031,"display_name":"Lizardfolk","name":"lizardfolk"},{"id":1027,"display_name":"Merfolk","name":"merfolk"},{"id":1016,"display_name":"Monstrosities","name":"monstrosities"},{"id":1017,"display_name":"Oozes","name":"oozes"},{"id":1029,"display_name":"Orcs","name":"orcs"},{"id":1018,"display_name":"Plants","name":"plants"},{"id":1033,"display_name":"Sahuagin","name":"sahuagin"},{"id":1019,"display_name":"Undead","name":"undead"},{"id":1034,"display_name":"Werebears","name":"werebears"},{"id":1035,"display_name":"Wereboars","name":"wereboars"},{"id":1036,"display_name":"Wererats","name":"wererats"},{"id":1037,"display_name":"Werewolves","name":"werewolves"}],"31":[{"id":1040,"display_name":"Dual Wield Light Restrictions","name":"dual-wield-light-restrictions"},{"id":1041,"display_name":"Dual Wield Modifier Restrictions","name":"dual-wield-modifier-restrictions"},{"id":1789,"display_name":"Heavy Armor Speed Reduction","name":"heavy-armor-speed-reduction"},{"id":1055,"display_name":"Unarmored Dex AC Bonus","name":"unarmored-dex-ac-bonus"},{"id":1728,"display_name":"Unarmored Dex Natural AC Bonus","name":"unarmored-dex-natural-ac-bonus"}],"32":[{"id":1052,"display_name":"Bonus Damage","name":"bonus-damage"},{"id":1053,"display_name":"Bonus Range","name":"bonus-range"}],"33":[{"id":1700,"display_name":"Acid","name":"acid"},{"id":1702,"display_name":"Bludgeoning","name":"bludgeoning"},{"id":1701,"display_name":"Cold","name":"cold"},{"id":1703,"display_name":"Fire","name":"fire"},{"id":1704,"display_name":"Force","name":"force"},{"id":1705,"display_name":"Lightning","name":"lightning"},{"id":1706,"display_name":"Necrotic","name":"necrotic"},{"id":1707,"display_name":"Piercing","name":"piercing"},{"id":1708,"display_name":"Poison","name":"poison"},{"id":1709,"display_name":"Psychic","name":"psychic"},{"id":1056,"display_name":"Radiant","name":"radiant"},{"id":1710,"display_name":"Slashing","name":"slashing"},{"id":1711,"display_name":"Thunder","name":"thunder"}],"34":[{"id":1398,"display_name":"Ability Checks","name":"ability-checks"},{"id":1406,"display_name":"Acrobatics","name":"acrobatics"},{"id":1480,"display_name":"Alchemist's Supplies","name":"alchemists-supplies"},{"id":1414,"display_name":"Animal Handling","name":"animal-handling"},{"id":1409,"display_name":"Arcana","name":"arcana"},{"id":1405,"display_name":"Athletics","name":"athletics"},{"id":1505,"display_name":"Bagpipes","name":"bagpipes"},{"id":1446,"display_name":"Battleaxe","name":"battleaxe"},{"id":1462,"display_name":"Blowgun","name":"blowgun"},{"id":1474,"display_name":"Breastplate","name":"breastplate"},{"id":1481,"display_name":"Brewer's Supplies","name":"brewers-supplies"},{"id":1482,"display_name":"Calligrapher's Supplies","name":"calligraphers-supplies"},{"id":1483,"display_name":"Carpenter's Tools","name":"carpenters-tools"},{"id":1484,"display_name":"Cartographer's Tools","name":"cartographers-tools"},{"id":1477,"display_name":"Chain Mail","name":"chain-mail"},{"id":1473,"display_name":"Chain Shirt","name":"chain-shirt"},{"id":1404,"display_name":"Charisma Ability Checks","name":"charisma-ability-checks"},{"id":1397,"display_name":"Charisma Saving Throws","name":"charisma-saving-throws"},{"id":1560,"display_name":"Choose a Dwarf Artisan's Tool","name":"choose-a-dwarf-artisans-tool","choice_groups":["ARTISAN"],"only":["brewers-supplies","smiths-tools","masons-tools"]},{"id":1433,"display_name":"Club","name":"club"},{"id":1485,"display_name":"Cobbler's Tools","name":"cobblers-tools"},{"id":1401,"display_name":"Constitution Ability Checks","name":"constitution-ability-checks"},{"id":1394,"display_name":"Constitution Saving Throws","name":"constitution-saving-throws"},{"id":1486,"display_name":"Cook's Utensils","name":"cooks-utensils"},{"id":1429,"display_name":"Crossbow, Hand","name":"crossbow-hand"},{"id":1463,"display_name":"Crossbow, Heavy","name":"crossbow-heavy"},{"id":1442,"display_name":"Crossbow, Light","name":"crossbow-light"},{"id":1431,"display_name":"Dagger","name":"dagger"},{"id":1443,"display_name":"Dart","name":"dart"},{"id":1419,"display_name":"Deception","name":"deception"},{"id":1400,"display_name":"Dexterity Ability Checks","name":"dexterity-ability-checks"},{"id":1427,"display_name":"Dexterity Attacks","name":"dexterity-attacks"},{"id":1393,"display_name":"Dexterity Saving Throws","name":"dexterity-saving-throws"},{"id":1503,"display_name":"Dice Set","name":"dice-set"},{"id":1497,"display_name":"Disguise Kit","name":"disguise-kit"},{"id":1566,"display_name":"Dragonchess Set","name":"dragonchess-set"},{"id":1506,"display_name":"Drum","name":"drum"},{"id":1507,"display_name":"Dulcimer","name":"dulcimer"},{"id":1447,"display_name":"Flail","name":"flail"},{"id":1508,"display_name":"Flute","name":"flute"},{"id":1498,"display_name":"Forgery Kit","name":"forgery-kit"},{"id":1430,"display_name":"Glaive","name":"glaive"},{"id":1487,"display_name":"Glassblower's Tools","name":"glassblowers-tools"},{"id":1448,"display_name":"Greataxe","name":"greataxe"},{"id":1434,"display_name":"Greatclub","name":"greatclub"},{"id":1449,"display_name":"Greatsword","name":"greatsword"},{"id":1450,"display_name":"Halberd","name":"halberd"},{"id":1475,"display_name":"Half Plate","name":"half-plate"},{"id":1435,"display_name":"Handaxe","name":"handaxe"},{"id":1519,"display_name":"Heavy Armor","name":"heavy-armor"},{"id":1499,"display_name":"Herbalism Kit","name":"herbalism-kit"},{"id":1472,"display_name":"Hide","name":"hide"},{"id":1410,"display_name":"History","name":"history"},{"id":1511,"display_name":"Horn","name":"horn"},{"id":1538,"display_name":"Improvised Weapons","name":"improvised-weapons"},{"id":1428,"display_name":"Initiative","name":"initiative"},{"id":1415,"display_name":"Insight","name":"insight"},{"id":1402,"display_name":"Intelligence Ability Checks","name":"intelligence-ability-checks"},{"id":1395,"display_name":"Intelligence Saving Throws","name":"intelligence-saving-throws"},{"id":1420,"display_name":"Intimidation","name":"intimidation"},{"id":1411,"display_name":"Investigation","name":"investigation"},{"id":1436,"display_name":"Javelin","name":"javelin"},{"id":1488,"display_name":"Jeweler's Tools","name":"jewelers-tools"},{"id":1451,"display_name":"Lance","name":"lance"},{"id":1471,"display_name":"Leather","name":"leather"},{"id":1489,"display_name":"Leatherworker's Tools","name":"leatherworkers-tools"},{"id":1517,"display_name":"Light Armor","name":"light-armor"},{"id":1437,"display_name":"Light Hammer","name":"light-hammer"},{"id":1464,"display_name":"Longbow","name":"longbow"},{"id":1432,"display_name":"Longsword","name":"longsword"},{"id":1509,"display_name":"Lute","name":"lute"},{"id":1510,"display_name":"Lyre","name":"lyre"},{"id":1438,"display_name":"Mace","name":"mace"},{"id":1515,"display_name":"Martial Weapons","name":"martial-weapons"},{"id":1490,"display_name":"Mason's Tools","name":"masons-tools"},{"id":1452,"display_name":"Maul","name":"maul"},{"id":1416,"display_name":"Medicine","name":"medicine"},{"id":1518,"display_name":"Medium Armor","name":"medium-armor"},{"id":1423,"display_name":"Melee Attacks","name":"melee-attacks"},{"id":1468,"display_name":"Mithril Plate Armor","name":"mithril-plate-armor"},{"id":1453,"display_name":"Morningstar","name":"morningstar"},{"id":1412,"display_name":"Nature","name":"nature"},{"id":1500,"display_name":"Navigator's Tools","name":"navigators-tools"},{"id":1465,"display_name":"Net","name":"net"},{"id":1470,"display_name":"Padded","name":"padded"},{"id":1491,"display_name":"Painter's Supplies","name":"painters-supplies"},{"id":1512,"display_name":"Pan Flute","name":"pan-flute"},{"id":1417,"display_name":"Perception","name":"perception"},{"id":1421,"display_name":"Performance","name":"performance"},{"id":1422,"display_name":"Persuasion","name":"persuasion"},{"id":1454,"display_name":"Pike","name":"pike"},{"id":1479,"display_name":"Plate","name":"plate"},{"id":1504,"display_name":"Playing Card Set","name":"playing-card-set"},{"id":1501,"display_name":"Poisoner's Kit","name":"poisoners-kit"},{"id":1492,"display_name":"Potter's Tools","name":"potters-tools"},{"id":1439,"display_name":"Quarterstaff","name":"quarterstaff"},{"id":1424,"display_name":"Ranged Attacks","name":"ranged-attacks"},{"id":1455,"display_name":"Rapier","name":"rapier"},{"id":1413,"display_name":"Religion","name":"religion"},{"id":1476,"display_name":"Ring Mail","name":"ring-mail"},{"id":1391,"display_name":"Saving Throws","name":"saving-throws"},{"id":1467,"display_name":"Scale Mail","name":"scale-mail"},{"id":1456,"display_name":"Scimitar","name":"scimitar"},{"id":1513,"display_name":"Shawm","name":"shawm"},{"id":1532,"display_name":"Shields","name":"shields"},{"id":1444,"display_name":"Shortbow","name":"shortbow"},{"id":1457,"display_name":"Shortsword","name":"shortsword"},{"id":1440,"display_name":"Sickle","name":"sickle"},{"id":1516,"display_name":"Simple Weapons","name":"simple-weapons"},{"id":1407,"display_name":"Sleight of Hand","name":"sleight-of-hand"},{"id":1445,"display_name":"Sling","name":"sling"},{"id":1493,"display_name":"Smith's Tools","name":"smiths-tools"},{"id":1441,"display_name":"Spear","name":"spear"},{"id":1425,"display_name":"Spell Attacks","name":"spell-attacks"},{"id":1478,"display_name":"Splint","name":"splint"},{"id":1408,"display_name":"Stealth","name":"stealth"},{"id":1469,"display_name":"Steel Shield","name":"steel-shield"},{"id":1399,"display_name":"Strength Ability Checks","name":"strength-ability-checks"},{"id":1426,"display_name":"Strength Attacks","name":"strength-attacks"},{"id":1392,"display_name":"Strength Saving Throws","name":"strength-saving-throws"},{"id":1466,"display_name":"Studded Leather","name":"studded-leather"},{"id":1418,"display_name":"Survival","name":"survival"},{"id":1502,"display_name":"Thieves' Tools","name":"thieves-tools"},{"id":1565,"display_name":"Three-Dragon Ante Set","name":"threedragon-ante-set"},{"id":1494,"display_name":"Tinker's Tools","name":"tinkers-tools"},{"id":1458,"display_name":"Trident","name":"trident"},{"id":1555,"display_name":"Vehicles (Land)","name":"vehicles-land"},{"id":1556,"display_name":"Vehicles (Water)","name":"vehicles-water"},{"id":1514,"display_name":"Viol","name":"viol"},{"id":1459,"display_name":"War pick","name":"war-pick"},{"id":1460,"display_name":"Warhammer","name":"warhammer"},{"id":1495,"display_name":"Weaver's Tools","name":"weavers-tools"},{"id":1461,"display_name":"Whip","name":"whip"},{"id":1403,"display_name":"Wisdom Ability Checks","name":"wisdom-ability-checks"},{"id":1396,"display_name":"Wisdom Saving Throws","name":"wisdom-saving-throws"},{"id":1496,"display_name":"Woodcarver's Tools","name":"woodcarvers-tools"}],"37":[{"id":1689,"display_name":"0 HP","name":"0-hp"}],"38":[{"id":1734,"display_name":"Charisma Score","name":"charisma-score"},{"id":1731,"display_name":"Constitution Score","name":"constitution-score"},{"id":1730,"display_name":"Dexterity Score","name":"dexterity-score"},{"id":1732,"display_name":"Intelligence Score","name":"intelligence-score"},{"id":1729,"display_name":"Strength Score","name":"strength-score"},{"id":1733,"display_name":"Wisdom Score","name":"wisdom-score"}],"39":[{"id":1745,"display_name":"Blindsight","name":"blindsight"},{"id":1746,"display_name":"Darkvision","name":"darkvision"},{"id":1747,"display_name":"Tremorsense","name":"tremorsense"},{"id":1748,"display_name":"Truesight","name":"truesight"}],"40":[{"id":1754,"display_name":"Finesse","name":"finesse"},{"id":1758,"display_name":"Heavy","name":"heavy"},{"id":1759,"display_name":"Light","name":"light"},{"id":1760,"display_name":"Loading","name":"loading"},{"id":1761,"display_name":"Range","name":"range"},{"id":1762,"display_name":"Reach","name":"reach"},{"id":1755,"display_name":"Thrown","name":"thrown"},{"id":1757,"display_name":"Two-Handed","name":"twohanded"},{"id":1756,"display_name":"Versatile","name":"versatile"}],"41":[{"id":1811,"display_name":"Gargantuan","name":"gargantuan"},{"id":1810,"display_name":"Huge","name":"huge"},{"id":1809,"display_name":"Large","name":"large"},{"id":1808,"display_name":"Medium","name":"medium"},{"id":1807,"display_name":"Small","name":"small"},{"id":1806,"display_name":"Tiny","name":"tiny"}]}