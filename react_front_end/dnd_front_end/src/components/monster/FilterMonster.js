import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class FilterMonster extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="monster_filters">
                <Row>
                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Monster Name</Form.Label>
                            <Form.Control value={this.props.filters["name"] ? this.props.filters["name"] : ''} name="name" type="text" placeholder="Search Monster Names" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicChallenge">
                            <Form.Label>Challenge Range</Form.Label>
                            <Row>
                                <Col xs={5} md={5}>
                                    <Form.Control name="challenge_min" as="select" onChange={this.handleChange} custom>
                                            <option selected="true" value="">-</option>
                                            <option value="0" selected={this.props.filters["challenge_min"] == "0"}>0</option>
                                            <option value="1/8" selected={this.props.filters["challenge_min"] == "1/8"}>1/8</option>
                                            <option value="1/4" selected={this.props.filters["challenge_min"] == "1/4"}>1/4</option>
                                            <option value="1/2" selected={this.props.filters["challenge_min"] == "1/2"}>1/2</option>
                                            <option value="1" selected={this.props.filters["challenge_min"] == "1"}>1</option>
                                            <option value="2" selected={this.props.filters["challenge_min"] == "2"}>2</option>
                                            <option value="3" selected={this.props.filters["challenge_min"] == "3"}>3</option>
                                            <option value="4" selected={this.props.filters["challenge_min"] == "4"}>4</option>
                                            <option value="5" selected={this.props.filters["challenge_min"] == "5"}>5</option>
                                            <option value="6" selected={this.props.filters["challenge_min"] == "6"}>6</option>
                                            <option value="7" selected={this.props.filters["challenge_min"] == "7"}>7</option>
                                            <option value="8" selected={this.props.filters["challenge_min"] == "8"}>8</option>
                                            <option value="9" selected={this.props.filters["challenge_min"] == "9"}>9</option>
                                            <option value="10" selected={this.props.filters["challenge_min"] == "10"}>10</option>
                                            <option value="11" selected={this.props.filters["challenge_min"] == "11"}>11</option>
                                            <option value="12" selected={this.props.filters["challenge_min"] == "12"}>12</option>
                                            <option value="13" selected={this.props.filters["challenge_min"] == "13"}>13</option>
                                            <option value="14" selected={this.props.filters["challenge_min"] == "14"}>14</option>
                                            <option value="15" selected={this.props.filters["challenge_min"] == "15"}>15</option>
                                            <option value="16" selected={this.props.filters["challenge_min"] == "16"}>16</option>
                                            <option value="17" selected={this.props.filters["challenge_min"] == "17"}>17</option>
                                            <option value="18" selected={this.props.filters["challenge_min"] == "18"}>18</option>
                                            <option value="19" selected={this.props.filters["challenge_min"] == "19"}>19</option>
                                            <option value="20" selected={this.props.filters["challenge_min"] == "20"}>20</option>
                                            <option value="21" selected={this.props.filters["challenge_min"] == "21"}>21</option>
                                            <option value="22" selected={this.props.filters["challenge_min"] == "22"}>22</option>
                                            <option value="23" selected={this.props.filters["challenge_min"] == "23"}>23</option>
                                            <option value="24" selected={this.props.filters["challenge_min"] == "24"}>24</option>
                                            <option value="25" selected={this.props.filters["challenge_min"] == "25"}>25</option>
                                            <option value="26" selected={this.props.filters["challenge_min"] == "26"}>26</option>
                                            <option value="27" selected={this.props.filters["challenge_min"] == "27"}>27</option>
                                            <option value="28" selected={this.props.filters["challenge_min"] == "28"}>28</option>
                                            <option value="29" selected={this.props.filters["challenge_min"] == "29"}>29</option>
                                            <option value="30" selected={this.props.filters["challenge_min"] == "30"}>30</option>
                                    </Form.Control>
                                </Col>
                                <Col xs={1} md={1}>-</Col>
                                <Col xs={5} md={5}>
                                    <Form.Control name="challenge_max" as="select" onChange={this.handleChange} custom>
                                            <option selected="true" value="">-</option>
                                            <option value="0" selected={this.props.filters["challenge_max"] == "0"}>0</option>
                                            <option value="1/8" selected={this.props.filters["challenge_max"] == "1/8"}>1/8</option>
                                            <option value="1/4" selected={this.props.filters["challenge_max"] == "1/4"}>1/4</option>
                                            <option value="1/2" selected={this.props.filters["challenge_max"] == "1/2"}>1/2</option>
                                            <option value="1" selected={this.props.filters["challenge_max"] == "1"}>1</option>
                                            <option value="2" selected={this.props.filters["challenge_max"] == "2"}>2</option>
                                            <option value="3" selected={this.props.filters["challenge_max"] == "3"}>3</option>
                                            <option value="4" selected={this.props.filters["challenge_max"] == "4"}>4</option>
                                            <option value="5" selected={this.props.filters["challenge_max"] == "5"}>5</option>
                                            <option value="6" selected={this.props.filters["challenge_max"] == "6"}>6</option>
                                            <option value="7" selected={this.props.filters["challenge_max"] == "7"}>7</option>
                                            <option value="8" selected={this.props.filters["challenge_max"] == "8"}>8</option>
                                            <option value="9" selected={this.props.filters["challenge_max"] == "9"}>9</option>
                                            <option value="10" selected={this.props.filters["challenge_max"] == "10"}>10</option>
                                            <option value="11" selected={this.props.filters["challenge_max"] == "11"}>11</option>
                                            <option value="12" selected={this.props.filters["challenge_max"] == "12"}>12</option>
                                            <option value="13" selected={this.props.filters["challenge_max"] == "13"}>13</option>
                                            <option value="14" selected={this.props.filters["challenge_max"] == "14"}>14</option>
                                            <option value="15" selected={this.props.filters["challenge_max"] == "15"}>15</option>
                                            <option value="16" selected={this.props.filters["challenge_max"] == "16"}>16</option>
                                            <option value="17" selected={this.props.filters["challenge_max"] == "17"}>17</option>
                                            <option value="18" selected={this.props.filters["challenge_max"] == "18"}>18</option>
                                            <option value="19" selected={this.props.filters["challenge_max"] == "19"}>19</option>
                                            <option value="20" selected={this.props.filters["challenge_max"] == "20"}>20</option>
                                            <option value="21" selected={this.props.filters["challenge_max"] == "21"}>21</option>
                                            <option value="22" selected={this.props.filters["challenge_max"] == "22"}>22</option>
                                            <option value="23" selected={this.props.filters["challenge_max"] == "23"}>23</option>
                                            <option value="24" selected={this.props.filters["challenge_max"] == "24"}>24</option>
                                            <option value="25" selected={this.props.filters["challenge_max"] == "25"}>25</option>
                                            <option value="26" selected={this.props.filters["challenge_max"] == "26"}>26</option>
                                            <option value="27" selected={this.props.filters["challenge_max"] == "27"}>27</option>
                                            <option value="28" selected={this.props.filters["challenge_max"] == "28"}>28</option>
                                            <option value="29" selected={this.props.filters["challenge_max"] == "29"}>29</option>
                                            <option value="30" selected={this.props.filters["challenge_max"] == "30"}>30</option>
                                    </Form.Control>
                                </Col>
                            </Row>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Size</Form.Label>
                            <Form.Control name="size" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    <option value="Tiny" selected={this.props.filters["size"] == "Tiny"}>Tiny</option>
                                    <option value="Small" selected={this.props.filters["size"] == "Small"}>Small</option>
                                    <option value="Medium" selected={this.props.filters["size"] == "Medium"}>Medium</option>
                                    <option value="Large" selected={this.props.filters["size"] == "Large"}>Large</option>
                                    <option value="Huge" selected={this.props.filters["size"] == "Huge"}>Huge</option>
                                    <option value="Gargantuan" selected={this.props.filters["size"] == "Gargantuan"}>Gargantuan</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicEnvironment">
                            <Form.Label>Environment</Form.Label>
                            <Form.Control name="environment" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    <option value="Arctic" selected={this.props.filters["environment"] == "Arctic"}>Arctic</option>
                                    <option value="Coastal" selected={this.props.filters["environment"] == "Coastal"}>Coastal</option>
                                    <option value="Desert" selected={this.props.filters["environment"] == "Desert"}>Desert</option>
                                    <option value="Forest" selected={this.props.filters["environment"] == "Forest"}>Forest</option>
                                    <option value="Grassland" selected={this.props.filters["environment"] == "Grassland"}>Grassland</option>
                                    <option value="Hill" selected={this.props.filters["environment"] == "Hill"}>Hill</option>
                                    <option value="Mountain" selected={this.props.filters["environment"] == "Mountain"}>Mountain</option>
                                    <option value="Swamp" selected={this.props.filters["environment"] == "Swamp"}>Swamp</option>
                                    <option value="Underdark" selected={this.props.filters["environment"] == "Underdark"}>Underdark</option>
                                    <option value="Underwater" selected={this.props.filters["environment"] == "Underwater"}>Underwater</option>
                                    <option value="Urban" selected={this.props.filters["environment"] == "Urban"}>Urban</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicEnvironment">
                            <Form.Label>Type</Form.Label>
                            <Form.Control name="type" as="select" onChange={this.handleChange} custom>
                            <option selected="true" value="">-</option>
                                    <option value="Aberration" selected={this.props.filters["type"] == "Aberration"}>Aberration</option>
                                    <option value="Beast" selected={this.props.filters["type"] == "Beast"}>Beast</option>
                                    <option value="Celestial" selected={this.props.filters["type"] == "Celestial"}>Celestial</option>
                                    <option value="Construct" selected={this.props.filters["type"] == "Construct"}>Construct</option>
                                    <option value="Dragon" selected={this.props.filters["type"] == "Dragon"}>Dragon</option>
                                    <option value="Elemental" selected={this.props.filters["type"] == "Elemental"}>Elemental</option>
                                    <option value="Fey" selected={this.props.filters["type"] == "Fey"}>Fey</option>
                                    <option value="Fiend" selected={this.props.filters["type"] == "Fiend"}>Fiend</option>
                                    <option value="Giant" selected={this.props.filters["type"] == "Giant"}>Giant</option>
                                    <option value="Humanoid" selected={this.props.filters["type"] == "Humanoid"}>Humanoid</option>
                                    <option value="Monstrosity" selected={this.props.filters["type"] == "Monstrosity"}>Monstrosity</option>
                                    <option value="Ooze" selected={this.props.filters["type"] == "Ooze"}>Ooze</option>
                                    <option value="Plant" selected={this.props.filters["type"] == "Plant"}>Plant</option>
                                    <option value="Undead" selected={this.props.filters["type"] == "Undead"}>Undead</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicEnvironment">
                            <Form.Label>Homebrew</Form.Label>
                            <Form.Control name="homebrew" as="select" onChange={this.handleChange} custom>
                                    <option selected="" value="">-</option>
                                    <option value={true} selected={this.props.filters["homebrew"] == true}>True</option>
                                    <option value={false} selected={this.props.filters["homebrew"] == false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col xs={3} md={3}>
                        <a className="btn btn-primary" onClick={() => this.props.clear_filter_function()}>Clear Filters</a>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default FilterMonster;