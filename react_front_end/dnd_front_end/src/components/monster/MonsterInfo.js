import React, { Component } from 'react';
import Markdown from 'react-markdown'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import RollingButton from '../shared/RollingButton';


function process_monster_markdown(markdown, show_roll_buttons){
    const roll_with_plus_regex = "([^><])([0-9]+d[0-9]+([ \\t]+)*\\+([ \\t]+)*[0-9]+)([^><])";
    const general_roll_regex = "([^><])([0-9]+d[0-9]+)([^><])"
    const roll_with_no_preceeding = "([^><0-9])(d[0-9]+)([^><])"
    const roll_with_only_plus = "([ \\t])(\\+[0-9]+)([ \\t]|[,]|$)"

    markdown = markdown.replaceAll("<br>", "\n")

    if (show_roll_buttons == true){
        var completed_matches = []
        var matches = [...markdown.matchAll(roll_with_plus_regex)]
        for(var i = 0; i < matches.length; i++){
            var match = matches[i][2]
            if(!completed_matches.includes(match)){
                markdown = markdown.replaceAll(match, "<a>" + match.replaceAll(" ", "") + "</a>")
                completed_matches.push(match)
            }
        }

        var matches = [...markdown.matchAll(general_roll_regex)]
        for(var i = 0; i < matches.length; i++){
            var match = matches[i][2]
            if(!completed_matches.includes(match)){
                markdown = markdown.replaceAll(matches[i][0], "<a>" + match.replaceAll(" ", "") + "</a>")
                completed_matches.push(match)
            }
        }

        var matches = [...markdown.matchAll(roll_with_no_preceeding)]
        for(var i = 0; i < matches.length; i++){
            var match = matches[i][2]
            if(!completed_matches.includes(match)){
                markdown = markdown.replaceAll(matches[i][0], "<a>" + match.replaceAll(" ", "") + "</a>")
                completed_matches.push(match)
            }
        }

        var matches = [...markdown.matchAll(roll_with_only_plus)]
        for(var i = 0; i < matches.length; i++){
            var match = matches[i][2]
            if(!completed_matches.includes(match)){
                markdown = markdown.replaceAll(matches[i][0], " <a>" + match.replaceAll(" ", "") + "</a> ")
                completed_matches.push(match)
            }
        }
        markdown = markdown.replaceAll("<a>", "[rollable](").replaceAll("</a>", ")")
    }

    return markdown
}



class MonsterInfo extends Component {

    get_rolling_identity = () => {
        if (this.props.override_name != undefined){
            return this.props.override_name;
        }
        return this.props.monster.name
    }

    get_rolling_type = () => {
        if (this.props.override_roll_type != undefined && this.props.override_roll_type != null){
            return this.props.override_roll_type;
        }
        return "MONSTER_ROLL"
    }

    components = () => {
        return {
            a: props => {return (props.children[0] == "rollable" ? <RollingButton roll_dice_function={this.props.roll_dice_function} roll={props.href} type={this.get_rolling_type()} identity={this.get_rolling_identity()}/> : props.children[0])}
        }
    };

    get_roll_from_attribute = (value) => {
        var modifier = Math.floor(value/2) - 5
        if (modifier < 0){
            return "d20-" + (modifier*-1)
        }
        if (modifier > 0){
            return "d20+" + modifier
        }
        if (modifier == 0){
            return "d20"
        }
    }
      
    render(){
        var components = this.components()

                return(
                    <div>
                        <div className="data">
                            <Row>
                                <Col xs={12} md={6}>

                                    {this.props.monster.armour_class != null && <p><b>Armor Class</b> {this.props.monster.armour_class}</p>}
                                    {this.props.monster.hit_points != null && <p><b>Hit Points</b> {this.props.monster.hit_points}</p>}
                                    {this.props.monster.speed != null && <p><b>Speed</b> {this.props.monster.speed}</p>}
                                    <hr/>
                                    <Row>
                                        <Col xs={2} md={2}>
                                            <p><b>STR</b><br/>
                                            { this.props.show_roll_buttons != undefined && this.props.show_roll_buttons == false ?
                                                this.props.monster.strength
                                                :
                                                <RollingButton roll_dice_function={this.props.roll_dice_function} display_override={this.props.monster.strength} roll={this.get_roll_from_attribute(this.props.monster.strength)} type={this.get_rolling_type()} identity={this.get_rolling_identity()}/>
                                            }
                                            </p>
                                        </Col>
                                        <Col xs={2} md={2}>
                                            <p><b>DEX</b><br/>
                                            { this.props.show_roll_buttons != undefined && this.props.show_roll_buttons == false ?
                                                this.props.monster.dexterity
                                                :
                                                <RollingButton roll_dice_function={this.props.roll_dice_function} display_override={this.props.monster.dexterity} roll={this.get_roll_from_attribute(this.props.monster.dexterity)} type={this.get_rolling_type()} identity={this.get_rolling_identity()}/>
                                            }
                                            </p>
                                        </Col>
                                        <Col xs={2} md={2}>
                                            <p><b>CON</b><br/>
                                            { this.props.show_roll_buttons != undefined && this.props.show_roll_buttons == false ?
                                                this.props.monster.constitution
                                                :
                                            <RollingButton roll_dice_function={this.props.roll_dice_function} display_override={this.props.monster.constitution} roll={this.get_roll_from_attribute(this.props.monster.constitution)} type={this.get_rolling_type()} identity={this.get_rolling_identity()}/>
                                        }
                                        </p>
                                        </Col>
                                        <Col xs={2} md={2}>
                                            <p><b>INT</b><br/>
                                            { this.props.show_roll_buttons != undefined && this.props.show_roll_buttons == false ?
                                                this.props.monster.intelligence
                                                :
                                            <RollingButton roll_dice_function={this.props.roll_dice_function} display_override={this.props.monster.intelligence} roll={this.get_roll_from_attribute(this.props.monster.intelligence)} type={this.get_rolling_type()} identity={this.get_rolling_identity()}/>
                                        }
                                        </p>
                                        </Col>
                                        <Col xs={2} md={2}>
                                            <p><b>WIS</b><br/>
                                            { this.props.show_roll_buttons != undefined && this.props.show_roll_buttons == false ?
                                                this.props.monster.wisdom
                                                :
                                            <RollingButton roll_dice_function={this.props.roll_dice_function} display_override={this.props.monster.wisdom} roll={this.get_roll_from_attribute(this.props.monster.wisdom)} type={this.get_rolling_type()} identity={this.get_rolling_identity()}/>
                                        }
                                        </p>
                                        </Col>
                                        <Col xs={2} md={2}>
                                            <p><b>CHA</b><br/>
                                            { this.props.show_roll_buttons != undefined && this.props.show_roll_buttons == false ?
                                                this.props.monster.charisma
                                                :
                                            <RollingButton roll_dice_function={this.props.roll_dice_function} display_override={this.props.monster.charisma} roll={this.get_roll_from_attribute(this.props.monster.charisma)} type={this.get_rolling_type()} identity={this.get_rolling_identity()}/>
                                        }
                                        </p>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col xs={12} md={6}>
                                    {this.props.monster.saving_throws != null && <p><b>Saving Throws</b> <Markdown components={components} children={process_monster_markdown(this.props.monster.saving_throws, this.props.show_roll_buttons)}/></p>}
                                    {this.props.monster.skills != null && <p><b>Skills</b> <Markdown components={components} children={process_monster_markdown(this.props.monster.skills, this.props.show_roll_buttons)}/></p>}
                                    {this.props.monster.damage_immunities != null && <p><b>Damage Immunities</b> {this.props.monster.damage_immunities}</p>}
                                    {this.props.monster.damage_resistances != null && <p><b>Damage Resistances</b> {this.props.monster.damage_resistances}</p>}
                                    {this.props.monster.damage_vulnerabilities != null && <p><b>Damage Vulnerability</b> {this.props.monster.damage_vulnerabilities}</p>}
                                    {this.props.monster.condition_immunities != null && <p><b>Condition Immunities</b> {this.props.monster.condition_immunities}</p>}
                                    {this.props.monster.senses != null && <p><b>Senses</b> {this.props.monster.senses}</p>}
                                    {this.props.monster.languages != null && <p><b>Languages</b> {this.props.monster.languages}</p>}
                                    {this.props.monster.challenge != null && <p><b>Challenge</b> {this.props.monster.challenge}</p>}
                                    {this.props.monster.proficiency_bonus != null && <p><b>Proficiency Bonus</b> {this.props.monster.proficiency_bonus}</p>}
                                </Col>
                            </Row>
                            <hr/>
                            <Row>
                                <Col xs={12} md={6}>
                                    {this.props.monster.actions != null &&
                                        <div>
                                            <h3>Actions</h3>
                                            <Markdown components={components} children={process_monster_markdown(this.props.monster.actions, this.props.show_roll_buttons)} />
                                        </div>
                                    }
                                </Col>
                                <Col xs={12} md={6}>
                                    {this.props.monster.legendary_actions != null &&
                                        <div>
                                            <h3>Legendary Actions</h3>
                                            <Markdown components={components} children={process_monster_markdown(this.props.monster.legendary_actions, this.props.show_roll_buttons)} />
                                        </div>
                                    }
                                    {this.props.monster.reactions != null &&
                                        <div>
                                            <h3>Reactions</h3>
                                            <Markdown components={components} children={process_monster_markdown(this.props.monster.reactions, this.props.show_roll_buttons)} />
                                        </div>
                                    }
                                    {this.props.monster.description != null &&
                                        <div>
                                            <h3>Abilities</h3>
                                            <Markdown components={components} children={process_monster_markdown(this.props.monster.description, this.props.show_roll_buttons)} />
                                        </div>
                                    }
                                    {this.props.monster.mythic != null &&
                                        <div>
                                            <h3>Mythic Actions</h3>
                                            <Markdown components={components} children={process_monster_markdown(this.props.monster.mythic, this.props.show_roll_buttons)} />
                                        </div>
                                    }
                                    {this.props.monster.bonus != null &&
                                        <div>
                                            <h3>Bonus Actions</h3>
                                            <Markdown components={components} children={process_monster_markdown(this.props.monster.bonus, this.props.show_roll_buttons)} />
                                        </div>
                                    }
                                    
                                </Col>
                            </Row>
                        </div>

                        {this.props.show_more_info == true && this.props.monster.more_info != null &&
                            <div>
                                <Markdown components={components} children={process_monster_markdown(this.props.monster.more_info, this.props.show_roll_buttons)} />
                            </div>
                        }
                        
                    </div>
                )

    };
}

export default MonsterInfo;