import React, { Component } from 'react';
import Markdown from 'react-markdown'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import APIButton from '.././shared/APIButton';
import MonsterInfo from './MonsterInfo'
import DifficultyCalculator from '.././shared/DifficultyCalculator';
import Collapsible from '.././shared/Collapsible';
import MonsterShortCodeGenerator from './MonsterShortCodeGenerator';
import Button from 'react-bootstrap/Button'
import { SocketContext } from '../socket/Socket';
import {Link} from "react-router-dom";
import ContentCollectionResource from '../shared/ContentCollectionResource';

class SingleMonster extends Component {

    static contextType = SocketContext

    /*create_image_share_command = (url) => {
        var data = {
            "url": url,
            "location": "ROLLING"
        }
        return {
            "command_code": "CMD_SHARE_IMG",
            "data": data
        }
    }*/

    emit_image_message = (url) => {
        const socket = this.context
        var data = {
            type: "URL",
            name: "",
            url: url
          }
          socket.emit("send_message", data);
    }
            
    render(){

                return(
                    <div className="monster">
                        <Collapsible contents={<DifficultyCalculator xp={this.props.monster.xp}/>} title="Difficulty Calculator"/>
                        <h2>
                            <Link to="/monster" onClick={this.props.close_monster_fuction}>
                                <i className="fas fa-chevron-left clickable-icon"></i>
                            </Link >
                             {this.props.monster.name} <MonsterShortCodeGenerator monster={this.props.monster} />
                        </h2>
                        {this.props.monster.types != null && <p>{this.props.monster.types}</p>}
                        {this.props.monster.environments != null && <p>{this.props.monster.environments}</p>}
                        <Row>
                            <Col xs={12} md={9}>
                                <MonsterInfo monster={this.props.monster} show_roll_buttons={false} show_more_info={false}/>
                            </Col>
                            <Col xs={12} md={3}>
                                {this.props.monster.image != null &&
                                    <div className="image">
                                        <img src={this.props.monster.image}></img>
                                        <br/>
                                        {this.props.monster.image.startsWith("http") &&
                                            <Button className="btn btn-primary" onClick={() => {this.emit_image_message(this.props.monster.image)}} >Share to Chat</Button>
                                        }
                                    </div>
                                }
                            </Col>
                        </Row>
                        {this.props.monster.more_info != null &&
                            <div>
                                <Markdown children={this.props.monster.more_info.replaceAll("<br>", "\n")} />
                            </div>
                        }
                        <ContentCollectionResource resource='monster' resource_id={this.props.monster.id}/>
                        
                    </div>
                )

    };
}

export default SingleMonster;