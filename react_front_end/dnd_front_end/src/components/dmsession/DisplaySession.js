import React, {useContext} from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '../shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import {SocketContext} from '.././socket/Socket';

const DisplaySession = ({ session, refresh_function}) => {

    const socket = useContext(SocketContext);

    const end_session = (id) => {
        DND_DELETE(
            "/session/" + id,
            null,
            (response) => {
                socket.emit("session_end");
                refresh_function();
            },
            null
        )
    }

    if(session==null){
        return null;
    }

    return (
        <div>
            <div className="session-container">
                <div>
                    <div className="session-description">
                        <Row>
                            <Col xs={6}>
                                <p>Click session key below to copy to clipboard.</p>
                                </Col>
                            <Col xs={6}>
                                <DeleteConfirmationButton id={session.id} name="Session" delete_function={end_session} override_button="End Session" override_title="End Session"/>
                            </Col>
                            <Col xs={12}>
                                <div className="session-key" onClick={() => {navigator.clipboard.writeText(session.key);}}>
                                    {session.key}
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DisplaySession;