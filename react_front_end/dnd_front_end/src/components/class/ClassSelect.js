import React, { Component, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '../shared/DNDRequests';
import { stringify } from 'query-string';
import FilterClass from './FilterClass';
import Paginator from '../shared/Paginator';
import Markdown from 'react-markdown'

const ClassItem = ({dnd_class, select_dnd_class_function, exclude}) => {
    return (
        <div key={dnd_class.name}>
            <div className="dnd-class-item">
                <Row>
                    <Col xs={2} md={2}>
                        <h3 className="card-text">{dnd_class.name}</h3>
                        <div className="dnd-class-image"
                            style={{  
                                backgroundImage: "url(" + dnd_class.portrait_url + ")",
                            }}
                        />
                    </Col>
                    <Col xs={8} md={8}>
                        <Markdown children={dnd_class.description}/>
                    </Col>
                    <Col xs={2} md={2}>
                        {exclude.includes(dnd_class.id) == false &&
                            <div>
                                <Button className="btn btn-primary" onClick={() => select_dnd_class_function(dnd_class.id, dnd_class.name)}>Select Class</Button>
                            </div>
                        }
                    </Col>
                </Row>
                <hr/>
            </div>
        </div>
    );
}

const ListClasses = ({ dnd_classes, select_dnd_class_function, exclude}) => {

    if(dnd_classes === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="dnd-class-items">
            {dnd_classes.map((dnd_class) => (
                <ClassItem key={dnd_class.id} dnd_class={dnd_class} select_dnd_class_function={select_dnd_class_function} exclude={exclude} />
            ))}
        </div>
    );
}

class ClassSelect extends Component {

    state = {
        dnd_classes: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false
    }

    componentDidMount() {
        this.refresh_dnd_classes(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    select_dnd_class = (dnd_class, name) => {

        this.setState({toggle: false})
        this.props.callback(dnd_class, name)
    }

    refresh_dnd_classes = () => {
        DND_GET(
          '/class?' + stringify(this.state.filters),
          (jsondata) => {
            this.setState({ dnd_classes: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_dnd_classes(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_dnd_classes(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_dnd_classes(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_page = (page) => {
        this.refresh_spells(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id, name) => {
        this.props.select_function(id, name)
        this.setState({toggle: false})
    }

    render(){

        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>{this.props.override_button != undefined ? this.props.override_button : "Choose Class"}</Button>
                }
                <Modal show={this.state.toggle} size="md" className="dnd-class-select-modal select-modal dnd-class-page">
                        <Modal.Header>Classes</Modal.Header>
                        <Modal.Body>
                            <FilterClass filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                            <ListClasses
                                dnd_classes={this.state.dnd_classes}
                                select_dnd_class_function={this.select_function}
                                exclude={this.props.exclude}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default ClassSelect;