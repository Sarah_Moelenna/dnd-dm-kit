import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ContentCollectionResource from '../shared/ContentCollectionResource';
import Markdown from 'react-markdown';
import { LevelNames } from '../monster/Data';

class SingleSubClass extends Component {

    render(){

                return(
                    <div className="class-data view">
                        <h2>
                            <Link to="/class" onClick={this.props.close_class_fuction}>
                                <i className="fas fa-chevron-left clickable-icon"></i>
                            </Link >
                            {this.props.subclass.class_name} - {this.props.subclass.name}
                        </h2>

                        <Row>
                            <Col xs={12} md={12}>
                                <hr/>
                                <Markdown children={this.props.subclass.description}/>
                            </Col>

                            <Col xs={12} md={12}>
                                <div className="class-feature-items">
                                    {this.props.subclass.class_features.map((class_feature) => (
                                        <div key={class_feature.id} className="class-feature-item">
                                            <Row>
                                                <Col xs={12} md={12} className="class-feature-name">
                                                    { class_feature.level &&
                                                        <span><p>{LevelNames[class_feature.level]}</p></span>
                                                    }
                                                    <span><p>{class_feature.name}</p></span>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <Markdown children={class_feature.description}/>
                                                </Col>
                                            </Row>
                                            <hr/>
                                        </div>
                                    ))}
                                </div>
                            </Col>
                        </Row>
                        <ContentCollectionResource resource='subclass' resource_id={this.props.subclass.id}/>
                    </div>
                )

    };
}

export default SingleSubClass;