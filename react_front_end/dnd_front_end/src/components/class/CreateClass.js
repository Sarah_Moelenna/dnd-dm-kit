import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { DND_GET, DND_PUT, DND_POST } from '../shared/DNDRequests';
import Button from 'react-bootstrap/Button'
import MDEditor from '@uiw/react-md-editor';
import { DiceValues, AttributeID, LevelNames, ActionResetTypes, Atrributes, SpellActivationTypes } from '../monster/Data';
import ListSelector from '../shared/ListSelector';
import { v4 as uuidv4 } from 'uuid';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton'
import { createModifier, ListModifiers } from '../shared/Modifier';
import { createAtHigherLevel, ListAtHigherLevel } from '../shared/AtHigherLevel';
import { createAction, ListActions } from '../shared/Action';
import { ListOptionSets, createOptionSet } from '../shared/OptionSet';
import { Collapse } from 'reactstrap';
import SpellSelection from '../spell/SpellSelection';
import { EditEquipmentSet, createEquipmentSet } from '../shared/EquipmentSet';
import { ListPrerequisites, createPrerequisite } from '../shared/Prerequisites';
import UploadImage from '../images/UploadImage';
import BackConfirmationButton from '../shared/BackConfirmationButton';
import { FormulaInput } from '../shared/FormulaInput';

export const createResource = () => {
    return {
        "id":  uuidv4(),
        "name": "Unnamed Resource",
        "snippet": null,
        "description": null,
        "reset_type": null,
        "activation_type": null,
        "modifiers": [],
        "at_higher_levels": [],
        "resource_count": {
            "1": null,
            "2": null,
            "3": null,
            "4": null,
            "5": null,
            "6": null,
            "7": null,
            "8": null,
            "9": null,
            "10": null,
            "11": null,
            "12": null,
            "13": null,
            "14": null,
            "15": null,
            "16": null,
            "17": null,
            "18": null,
            "19": null,
            "20": null
        },
    }
}

export const createClassFeature = () => {
    return {
        "id":  uuidv4(),
        "name": "Unnamed Class Feature",
        "description": null,
        "level": null,
        "modifiers": [],
        "actions": [],
        "option_sets": [],
        "spell_selection": null,
        "gives_spells": false,
        "hide_in_sheet": false
    }
}

class ClassFeatureItem extends Component {

    state = {
        toggle: false,
    }

    update_attribute = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.props.update_class_feature(this.props.class_feature.id, e.target.name, new_value)
    }

    update_attribute_by_name = (name, value) => {
        this.props.update_class_feature(this.props.class_feature.id, name, value)
    }

    update_text = (text) => {
        this.props.update_class_feature(this.props.class_feature.id, "description", text)
    }

    add_modifier = () => {
        var new_modifier = createModifier()
        var modifiers = this.props.class_feature.modifiers
        modifiers.push(new_modifier)
        this.props.update_class_feature(this.props.class_feature.id, "modifiers", modifiers)
    }

    add_option_set = () => {
        var new_option_set = createOptionSet()
        var option_sets = this.props.class_feature.option_sets
        option_sets.push(new_option_set)
        this.props.update_class_feature(this.props.class_feature.id, "option_sets", option_sets)
    }

    add_action = () => {
        var new_action = createAction()
        var actions = this.props.class_feature.actions
        actions.push(new_action)
        this.props.update_class_feature(this.props.class_feature.id, "actions", actions)
    }

    update_modifier = (id, new_object) => {
        var modifiers = []
        for(var i = 0; i < this.props.class_feature.modifiers.length; i++){
            var modifier = this.props.class_feature.modifiers[i]
            if(modifier.id == id){
                modifiers.push(new_object)
            } else {
                modifiers.push(modifier)
            }
        }
        this.props.update_class_feature(this.props.class_feature.id, "modifiers", modifiers)
    }

    update_option_set = (id, new_object) => {
        var option_sets = []
        for(var i = 0; i < this.props.class_feature.option_sets.length; i++){
            var option_set = this.props.class_feature.option_sets[i]
            if(option_set.id == id){
                option_sets.push(new_object)
            } else {
                option_sets.push(option_set)
            }
        }
        this.props.update_class_feature(this.props.class_feature.id, "option_sets", option_sets)
    }

    update_action = (id, new_object) => {
        var actions = []
        for(var i = 0; i < this.props.class_feature.actions.length; i++){
            var action = this.props.class_feature.actions[i]
            if(action.id == id){
                actions.push(new_object)
            } else {
                actions.push(action)
            }
        }
        this.props.update_class_feature(this.props.class_feature.id, "actions", actions)
    }

    delete_modifier = (id) => {
        var modifiers = []
        for(var i = 0; i < this.props.class_feature.modifiers.length; i++){
            var modifier = this.props.class_feature.modifiers[i]
            if(modifier.id != id){
                modifiers.push(modifier)
            }
        }
        this.props.update_class_feature(this.props.class_feature.id, "modifiers", modifiers)
    }

    delete_option_set = (id) => {
        var option_sets = []
        for(var i = 0; i < this.props.class_feature.option_sets.length; i++){
            var option_set = this.props.class_feature.option_sets[i]
            if(option_set.id != id){
                option_sets.push(option_set)
            }
        }
        this.props.update_class_feature(this.props.class_feature.id, "option_sets", option_sets)
    }

    delete_action = (id) => {
        var actions = []
        for(var i = 0; i < this.props.class_feature.actions.length; i++){
            var action = this.props.class_feature.actions[i]
            if(action.id != id){
                actions.push(action)
            }
        }
        this.props.update_class_feature(this.props.class_feature.id, "actions", actions)
    }

    render(){

        return(
            <Row key={this.props.class_feature.id} className="class-feature-item">
                <Col xs={6} md={6} className="class-feature-name">
                    <p>{this.props.class_feature.name}</p>
                </Col>
                <Col xs={2} md={2} className="class-feature-level">
                    { this.props.class_feature.level &&
                        <p>{LevelNames[this.props.class_feature.level]}</p>
                    }
                </Col>
                <Col xs={2} md={2}>
                    <DeleteConfirmationButton id={this.props.class_feature.id} name="Class Feature" delete_function={this.props.delete_class_feature} override_button="Delete"/>
                </Col>
                <Col xs={2} md={2} className="class-feature-dropper">
                    <div className="clickable-div" onClick={() => {this.setState({toggle: !this.state.toggle})}}><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                </Col>

                <Col xs={12} md={12}>
                    <Collapse isOpen={this.state.toggle}>
                        { this.state.toggle &&
                            <Row>
                                <Col xs={3} md={3}>
                                    <Form.Group>
                                        <Form.Label>NAME</Form.Label>
                                        <Form.Control value={this.props.class_feature.name} required name="name" type="text" placeholder="Enter Name" onChange={this.update_attribute} />
                                    </Form.Group>
                                </Col>

                                <Col xs={3} md={3}>
                                    <Form.Group>
                                        <Form.Label>Required Level</Form.Label>
                                        <Form.Control
                                            name="level"
                                            as="select"
                                            onChange={this.update_attribute}
                                            custom
                                        >
                                                <option selected="true" value="">-</option>
                                                {Object.keys(LevelNames).map((level_id) => (
                                                    <option
                                                        key={level_id}
                                                        selected={this.props.class_feature.level == level_id}
                                                        value={level_id}
                                                    >
                                                        {LevelNames[level_id]}
                                                    </option>
                                                ))}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>

                                <Col xs={3} md={3}>
                                    { this.props.has_spells && 
                                        <Form.Group>
                                            <Form.Label>Gives Spells?</Form.Label>
                                            <Form.Control name="gives_spells" as="select" onChange={this.update_attribute} custom>
                                                    <option selected={this.props.class_feature.gives_spells == true} value={true}>True</option>
                                                    <option selected={this.props.class_feature.gives_spells == false} value={false}>False</option>
                                            </Form.Control>
                                        </Form.Group>
                                    }
                                </Col>

                                <Col xs={3} md={3}>
                                    <Form.Group>
                                        <Form.Label>Hide in Sheet</Form.Label>
                                        <Form.Control name="hide_in_sheet" as="select" onChange={this.update_attribute} custom>
                                                <option selected={this.props.class_feature.hide_in_sheet == true} value={true}>True</option>
                                                <option selected={this.props.class_feature.hide_in_sheet == false} value={false}>False</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>

                                <Col xs={12} md={12}>
                                    <Form.Label>DESCRIPTION</Form.Label>
                                    <MDEditor
                                        value={this.props.class_feature.description}
                                        preview="edit"
                                        onChange={this.update_text}
                                    />
                                </Col>

                                <Col xs={12} md={12}>
                                    {this.props.class_feature.modifiers.length > 0 ?
                                        <div>
                                            <hr/>
                                            <h4>
                                                {this.props.class_feature.modifiers.length} Modifiers
                                                <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                            <ListModifiers modifiers={this.props.class_feature.modifiers} update_modifier={this.update_modifier} delete_modifier={this.delete_modifier} display_type="RACE"/>
                                        </div>
                                        :
                                        <div>
                                            <hr/>
                                            <h4>
                                                {this.props.class_feature.modifiers.length} Modifiers
                                                <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                        </div>
                                    }
                                </Col>

                                <Col xs={12} md={12}>
                                    {this.props.class_feature.actions.length > 0 ?
                                        <div>
                                            <hr/>
                                            <h4>
                                                {this.props.class_feature.actions.length} Actions
                                                <span className="clickable-div add-button" onClick={() => {this.add_action()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                            <ListActions actions={this.props.class_feature.actions} update_action={this.update_action} delete_action={this.delete_action} display_type="RACE"/>
                                        </div>
                                        :
                                        <div>
                                            <hr/>
                                            <h4>
                                                {this.props.class_feature.actions.length} Actions
                                                <span className="clickable-div add-button" onClick={() => {this.add_action()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                        </div>
                                    }
                                </Col>

                                {this.props.has_spells == true && this.props.class_feature.gives_spells == true &&
                                    <Col xs={12} md={12}>
                                        <h4>Spells Selection</h4>
                                        <SpellSelection
                                            spell_selection={this.props.class_feature.spell_selection}
                                            update_function={(spell_selection_data) => {this.update_attribute_by_name("spell_selection", spell_selection_data)}}
                                            disallow_groups={true}    
                                        />
                                    </Col>
                                }

                                <Col xs={12} md={12}>
                                    {this.props.class_feature.option_sets.length > 0 ?
                                        <div>
                                            <hr/>
                                            <h4>
                                                {this.props.class_feature.option_sets.length} Option Sets
                                                <span className="clickable-div add-button" onClick={() => {this.add_option_set()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                            <ListOptionSets option_sets={this.props.class_feature.option_sets} update_option_set ={this.update_option_set} delete_option_set={this.delete_option_set}/>
                                        </div>
                                        :
                                        <div>
                                            <hr/>
                                            <h4>
                                                {this.props.class_feature.option_sets.length} Option Sets
                                                <span className="clickable-div add-button" onClick={() => {this.add_option_set()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                        </div>
                                    }
                                </Col>

                            </Row>
                        }
                    </Collapse>
                </Col>
            </Row>
        )
    }
}

export const ListClassFeature = ({ class_features, update_class_feature, delete_class_feature, has_spells}) => {

    return (
        <div className="class-feature-items">
            {class_features.map((class_feature) => (
                <ClassFeatureItem class_feature={class_feature} update_class_feature={update_class_feature} delete_class_feature={delete_class_feature} id={class_feature.id}  key={class_feature.id} has_spells={has_spells}/>
            ))}
        </div>
    );
}

class ResourceItem extends Component {

    state = {
        toggle: false,
    }

    update_resource_count = (e) => {
        var new_resource_count = this.props.resource.resource_count
        var value = e.target.value
        var id = e.target.name
        new_resource_count[id] = value
        this.props.update_resource(this.props.resource.id, "resource_count", new_resource_count)
    }

    update_attribute = (e) => {
        this.props.update_resource(this.props.resource.id, e.target.name, e.target.value)
    }

    update_attribute_by_name = (name, value) => {
        this.props.update_resource(this.props.resource.id, name, value)
    }

    update_text = (text) => {
        this.props.update_resource(this.props.resource.id, "description", text)
    }

    add_modifier = () => {
        var new_modifier = createModifier()
        var modifiers = this.props.resource.modifiers
        modifiers.push(new_modifier)
        this.props.update_resource(this.props.resource.id, "modifiers", modifiers)
    }

    update_modifier = (id, new_object) => {
        var modifiers = []
        for(var i = 0; i < this.props.resource.modifiers.length; i++){
            var modifier = this.props.resource.modifiers[i]
            if(modifier.id == id){
                modifiers.push(new_object)
            } else {
                modifiers.push(modifier)
            }
        }
        this.props.update_resource(this.props.resource.id, "modifiers", modifiers)
    }

    delete_modifier = (id) => {
        var modifiers = []
        for(var i = 0; i < this.props.resource.modifiers.length; i++){
            var modifier = this.props.resource.modifiers[i]
            if(modifier.id != id){
                modifiers.push(modifier)
            }
        }
        this.props.update_resource(this.props.resource.id, "modifiers", modifiers)
    }


    add_at_higher_level = () => {
        var new_at_higher_level = createAtHigherLevel()
        var at_higher_levels = this.props.resource.at_higher_levels
        at_higher_levels.push(new_at_higher_level)
        this.props.update_resource(this.props.resource.id, "at_higher_levels", at_higher_levels)
    }

    update_at_higher_level = (index, new_object) => {
        var at_higher_levels = this.props.resource.at_higher_levels
        at_higher_levels[index] = new_object
        this.props.update_resource(this.props.resource.id, "at_higher_levels", at_higher_levels)
    }

    delete_at_higher_level = (index) => {
        var at_higher_levels = this.props.resource.at_higher_levels
        at_higher_levels.splice(index, 1)
        this.props.update_resource(this.props.resource.id, "at_higher_levels", at_higher_levels)
    }


    render(){

        return(
            <Row key={this.props.id} className="resource-item">
                <Col xs={4} md={4} className="resource-name">
                    <p>{this.props.resource.name}</p>
                </Col>
                <Col xs={4} md={4} className="resource-reset-type">
                    <p>{this.props.resource.reset_type && ActionResetTypes[this.props.resource.reset_type].display_name}</p>
                </Col>
                <Col xs={2} md={2}>
                    <DeleteConfirmationButton id={this.props.resource.id} name="Resource" delete_function={this.props.delete_resource} override_button="Delete"/>
                </Col>
                <Col xs={2} md={2} className="resource-dropper">
                    <div className="clickable-div" onClick={() => {this.setState({toggle: !this.state.toggle})}}><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                </Col>

                <Col xs={12} md={12}>
                    <Collapse isOpen={this.state.toggle}>
                        { this.state.toggle &&
                            <Row>
                                <Col xs={3} md={3}>
                                    <table className="table-data">
                                        <tbody>
                                            <tr className="headers">
                                                <td>Level</td>
                                                <td>Resource Count</td>
                                            </tr>
                                            {Object.keys(this.props.resource.resource_count).map((level_id) => (
                                                <tr key={level_id}>
                                                    <td>
                                                        {LevelNames[level_id]}
                                                    </td>
                                                    <td>
                                                        <Form.Control value={this.props.resource.resource_count[level_id]} name={level_id} type="number" min={0} onChange={this.update_resource_count} placeholder="-" />
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </Col>
                                <Col xs={9} md={9}>
                                    <Row>
                                        <Col xs={4} md={4}>
                                            <Form.Group>
                                                <Form.Label>NAME</Form.Label>
                                                <Form.Control value={this.props.resource.name} required name="name" type="text" placeholder="Enter Name" onChange={this.update_attribute} />
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            <Form.Group controlId="formBasicType">
                                            <Form.Label>ACTIVATION TYPE</Form.Label>
                                                <Form.Control name="activation_type" as="select" onChange={this.update_attribute} custom value={this.props.resource.activation_type}>
                                                        <option selected="true" value="">-</option>
                                                        {Object.keys(SpellActivationTypes).map((activation_type_display) => (
                                                            <option 
                                                                key={SpellActivationTypes[activation_type_display]}
                                                                value={SpellActivationTypes[activation_type_display]}
                                                                >
                                                                    {activation_type_display}
                                                                </option>
                                                        ))}
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            <Form.Group>
                                                <Form.Label>RESET TYPE</Form.Label>
                                                <Form.Control
                                                    name="reset_type"
                                                    as="select"
                                                    onChange={this.update_attribute}
                                                    custom
                                                >
                                                        <option selected="true" value="">-</option>
                                                        {Object.keys(ActionResetTypes).map((reset_type_id) => (
                                                            <option
                                                                key={reset_type_id}
                                                                selected={this.props.resource.reset_type == reset_type_id}
                                                                value={reset_type_id}
                                                            >
                                                                {ActionResetTypes[reset_type_id].display_name}
                                                            </option>
                                                        ))}
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        

                                        <Col xs={12} md={12}>
                                            <Form.Group>
                                                <Form.Label>SNIPPET</Form.Label>
                                                <Form.Control value={this.props.resource.snippet} required name="snippet" type="text" placeholder="Enter Snippet" onChange={this.update_attribute} />
                                            </Form.Group>
                                        </Col>

                                        <Col xs={12} md={12}>
                                            <Form.Label>DESCRIPTION</Form.Label>
                                            <MDEditor
                                                value={this.props.resource.description}
                                                preview="edit"
                                                onChange={this.update_text}
                                            />
                                        </Col>

                                        <Col xs={12} md={12}>
                                            {this.props.resource.modifiers.length > 0 ?
                                                <div>
                                                    <hr/>
                                                    <h4>
                                                        {this.props.resource.modifiers.length} Modifiers
                                                        <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                                            <i className="fas fa-plus"></i>
                                                        </span>
                                                    </h4>
                                                    <ListModifiers modifiers={this.props.resource.modifiers} update_modifier={this.update_modifier} delete_modifier={this.delete_modifier} display_type="RACE"/>
                                                </div>
                                                :
                                                <div>
                                                    <hr/>
                                                    <h4>
                                                        {this.props.resource.modifiers.length} Modifiers
                                                        <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                                            <i className="fas fa-plus"></i>
                                                        </span>
                                                    </h4>
                                                </div>
                                            }
                                        </Col>


                                        <Col xs={12} md={12}>
                                            {this.props.resource.at_higher_levels.length > 0 ?
                                                <div>
                                                    <hr/>
                                                    <h4>
                                                        {this.props.resource.at_higher_levels.length} At Higher Levels
                                                        <span className="clickable-div add-button" onClick={() => {this.add_at_higher_level()}}>
                                                            <i className="fas fa-plus"></i>
                                                        </span>
                                                    </h4>
                                                    <ListAtHigherLevel
                                                        items={this.props.resource.at_higher_levels}
                                                        update_function={this.update_at_higher_level}
                                                        delete_function={this.delete_at_higher_level}
                                                        scale_type="characterlevel"
                                                    />
                                                </div>
                                                :
                                                <div>
                                                    <hr/>
                                                    <h4>
                                                        {this.props.resource.at_higher_levels.length} At Higher Levels
                                                        <span className="clickable-div add-button" onClick={() => {this.add_at_higher_level()}}>
                                                            <i className="fas fa-plus"></i>
                                                        </span>
                                                    </h4>
                                                </div>
                                            }
                                        </Col>

                                    </Row>
                                </Col>
                            </Row>
                        }
                    </Collapse>
                </Col>
            </Row>
        )
    }
}

export const ListResources = ({ resources, update_resource, delete_resource}) => {

    return (
        <div className="resource-items">
            {resources.map((resource, index) => (
                <ResourceItem resource={resource} update_resource={update_resource} delete_resource={delete_resource} id={index}/>
            ))}
        </div>
    );
}

export class CreateClass extends Component {

    state = {
        name: null,
        description: null,
        has_spells: false,
        subclass_level: 1,
        subclass_title: null,
        subclass_description: null,
        hit_dice: null,
        base_hit_points: null,
        hit_point_attribute_id: null,
        primary_attribute_ids: [],
        spell_casting_attribute_id: null,
        is_ritual_spell_caster: false,
        knows_all_spells: false,
        are_spells_prepared: false,
        has_static_spell_count: true,
        custom_spell_count: null,
        spell_container_name: null,
        avatar_url: null,
        portrait_url: null,
        spell_data: {
            "1": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "2": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "3": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "4": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "5": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "6": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "7": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "8": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "9": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "10": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "11": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "12": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "13": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "14": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "15": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "16": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "17": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "18": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "19": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "20": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null}
        },
        resources: [],
        class_features: [],
        prerequisites: [],
        equipment_set: createEquipmentSet(),

        proficieny_toggle: false,
        spell_toggle: false,
        resource_toggle: false,
        class_feature_toggle: false,
        equipment_toggle: false,
        prerequisite_toggle: false,
        has_changes: false,
        multiclass_value: 1,
    }

    componentWillMount() {
        if(this.props.edit_class_id != undefined && this.props.edit_class_id != null){
            this.copy_class(this.props.edit_class_id)
        }
    };

    copy_class = (class_id) => {

        DND_GET(
            '/class/' + class_id,
            (response) => {
                console.log(response)
                this.populate_with_class_data(response)
            },
            null
        )
    };

    populate_with_class_data = (class_data) => {
        this.setState(
            {
                name: class_data.name,
                description: class_data.description,
                has_spells: class_data.has_spells,
                subclass_level: class_data.subclass_level,
                subclass_title: class_data.subclass_title,
                subclass_description: class_data.subclass_description,
                hit_dice: class_data.hit_dice,
                base_hit_points: class_data.base_hit_points,
                hit_point_attribute_id: class_data.hit_point_attribute_id,
                primary_attribute_ids: class_data.primary_attribute_ids,
                spell_casting_attribute_id: class_data.spell_casting_attribute_id,
                is_ritual_spell_caster: class_data.is_ritual_spell_caster,
                knows_all_spells: class_data.knows_all_spells,
                are_spells_prepared: class_data.are_spells_prepared,
                spell_container_name: class_data.spell_container_name,
                avatar_url: class_data.avatar_url,
                portrait_url: class_data.portrait_url,
                spell_data: class_data.spell_data,
                resources: class_data.resources,
                class_features: class_data.class_features,
                prerequisites: class_data.prerequisites,
                equipment_set: class_data.equipment_set,
                has_static_spell_count: class_data.has_static_spell_count,
                custom_spell_count: class_data.custom_spell_count,
                multiclass_value: class_data.multiclass_value
            }
        )
    }

    save_class = () => {

        var final_class = {
            name: this.state.name,
            description: this.state.description,
            has_spells: this.state.has_spells,
            subclass_level: this.state.subclass_level,
            subclass_title: this.state.subclass_title,
            subclass_description: this.state.subclass_description,
            hit_dice: this.state.hit_dice,
            base_hit_points: this.state.base_hit_points,
            hit_point_attribute_id: this.state.hit_point_attribute_id,
            primary_attribute_ids: this.state.primary_attribute_ids,
            spell_casting_attribute_id: this.state.spell_casting_attribute_id,
            is_ritual_spell_caster: this.state.is_ritual_spell_caster,
            knows_all_spells: this.state.knows_all_spells,
            are_spells_prepared: this.state.are_spells_prepared,
            spell_container_name: this.state.spell_container_name,
            avatar_url: this.state.avatar_url,
            portrait_url: this.state.portrait_url,
            spell_data: this.state.spell_data,
            resources: this.state.resources,
            class_features: this.state.class_features,
            prerequisites: this.state.prerequisites,
            equipment_set: this.state.equipment_set,
            has_static_spell_count: this.state.has_static_spell_count,
            custom_spell_count: this.state.custom_spell_count,
            multiclass_value: this.state.multiclass_value
        }
        console.log(final_class)

        if(this.props.edit_class_id != undefined && this.props.edit_class_id != null){
            DND_PUT(
                '/class/' + this.props.edit_class_id,
                {data: final_class},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        } else {
            DND_POST(
                '/class',
                {data: final_class},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        }
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.setState({ [e.target.name]: new_value, has_changes: true });
    }

    update_text = (text) => {
        this.setState({description: text, has_changes: true})
    }

    update_subclass_description = (text) => {
        this.setState({subclass_description: text, has_changes: true})
    }

    update_spell_data = (e) => {
        var new_spell_data = this.state.spell_data
        var value = e.target.value
        var level_id = e.target.dataset.levelId
        var id = e.target.name
        new_spell_data[level_id][id] = value
        this.setState({spell_data: new_spell_data, has_changes: true})
    }

    add_resource = () => {
        var new_resource = createResource()
        var resources = this.state.resources
        resources.push(new_resource)
        this.setState({resources: resources, has_changes: true})
    }

    delete_resource = (id) => {
        var resources = []
        for(var i = 0; i < this.state.resources.length; i++){
            var resource = this.state.resources[i]
            if(resource.id != id){
                resources.push(resource)
            }
        }
        this.setState({resources: resources, has_changes: true})
    }

    update_resource = (id, attribute, value) => {
        var resources = []
        for(var i = 0; i < this.state.resources.length; i++){
            var resource = this.state.resources[i]
            if(resource.id == id){
                resource[attribute] = value
            }
            resources.push(resource)
        }
        this.setState({resources: resources, has_changes: true})
    }


    add_class_feature = () => {
        var new_class_feature = createClassFeature()
        var class_features = this.state.class_features
        class_features.push(new_class_feature)
        this.setState({class_features: class_features, has_changes: true})
    }

    delete_class_feature = (id) => {
        var class_features = []
        for(var i = 0; i < this.state.class_features.length; i++){
            var class_feature = this.state.class_features[i]
            if(class_feature.id != id){
                class_features.push(class_feature)
            }
        }
        this.setState({class_features: class_features, has_changes: true})
    }

    update_class_feature = (id, attribute, value) => {
        var class_features = []
        for(var i = 0; i < this.state.class_features.length; i++){
            var class_feature = this.state.class_features[i]
            if(class_feature.id == id){
                class_feature[attribute] = value
            }
            class_features.push(class_feature)
        }
        this.setState({class_features: class_features, has_changes: true})
    }

    add_prerequisite = () => {
        var new_prerequisite = createPrerequisite()
        var prerequisites = this.state.prerequisites
        prerequisites.push(new_prerequisite)
        this.setState({prerequisites: prerequisites, has_changes: true})
    }

    update_prerequisite = (id, new_object) => {
        var prerequisites = []
        for(var i = 0; i < this.state.prerequisites.length; i++){
            var prerequisite = this.state.prerequisites[i]
            if(prerequisite.id == id){
                prerequisites.push(new_object)
            } else {
                prerequisites.push(prerequisite)
            }
        }
        this.setState({prerequisites: prerequisites, has_changes: true})
    }

    delete_prerequisite = (id) => {
        var prerequisites = []
        for(var i = 0; i < this.state.prerequisites.length; i++){
            var prerequisite = this.state.prerequisites[i]
            if(prerequisite.id != id){
                prerequisites.push(prerequisite)
            }
        }
        this.setState({prerequisites: prerequisites, has_changes: true})
    }

    get_sorted_class_features = () => {
        var final_members = []
        var levels = []
        var sorting_object = {}
        for (var i = 0; i < this.state.class_features.length; i++){
            var member = this.state.class_features[i]
            var key = 0
            if (member.level != null){
                key = parseInt(member.level)
            }
            if (levels.includes(key) == false){
                levels.push(key)
            }
            if (sorting_object[key] == undefined){
                sorting_object[key] = []
            }
            sorting_object[key].push(member)
        }
        
        levels.sort((a, b) => b - a);
        levels.reverse()

        for(var i = 0; i < levels.length; i++){
            var members = sorting_object[levels[i]]
            members.sort((a, b) => (a.name > b.name) ? 1 : -1)
            final_members = final_members.concat(members)
        }
        return final_members
    }

    get_primary_attributes = () => {
        var attributes  = []
        for(var i = 0; i < this.state.primary_attribute_ids.length; i++){
            attributes.push(AttributeID[this.state.primary_attribute_ids[i]])
        }
        return attributes.join(",")
    }

    update_primary_attributes = (data) => {
        var ids = []
        var att_names = data.replaceAll(' ', '').split(',')
        for(var i = 0; i < att_names.length; i++){
            var keys = Object.keys(AttributeID)
            for(var j = 0; j < keys.length; j++){
                if(AttributeID[keys[j]] == att_names[i]){
                    ids.push(keys[j])
                }
            }
        }
        this.setState({primary_attribute_ids: ids, has_changes: true})
    }

    update_image = (image, attribute) =>{
        this.setState({[attribute]: image, has_changes: true})
    }
            
    render(){

        var class_features = this.get_sorted_class_features()

        return(
            <div className="class">
                {(this.props.edit_class_id == undefined || this.props.edit_class_id == null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Create Class</h2>
                    </div>
                }
                {(this.props.edit_class_id != undefined && this.props.edit_class_id != null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Edit Class</h2>
                    </div>
                }
                <Row className="class-settings">
                    <Col xs={12} md={12}>
                        <h4>Basic Information</h4>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control value={this.state.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group>
                            <Form.Label>Primary Attributes</Form.Label>
                            <ListSelector input_value={this.get_primary_attributes()} items={Atrributes} onChange={this.update_primary_attributes} />
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group>
                            <Form.Label>Has Spells?</Form.Label>
                            <Form.Control name="has_spells" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.has_spells == true} value={true}>True</option>
                                    <option selected={this.state.has_spells == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group>
                            <Form.Label>Multiclass Value</Form.Label>
                            <Form.Control name="multiclass_value" as="select" onChange={this.handleChange} custom value={this.state.multiclass_value}>
                                    <option value={0}>0</option>
                                    <option value={0.5}>0.5</option>
                                    <option value={1}>1</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Label>Description</Form.Label>
                        <MDEditor
                            value={this.state.description}
                            preview="edit"
                            onChange={this.update_text}
                        />
                        <br/>
                    </Col>

                    <Col xs={6} md={6}>
                        <Form.Group>
                            <Form.Label>Subclass Title</Form.Label>
                            <Form.Control value={this.state.subclass_title} required name="subclass_title" type="text" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={6} md={6}>
                        <Form.Group>
                            <Form.Label>Subclass Level</Form.Label>
                            <Form.Control value={this.state.subclass_level} required name="subclass_level" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Label>Subclass Description</Form.Label>
                        <MDEditor
                            value={this.state.subclass_description}
                            preview="edit"
                            onChange={this.update_subclass_description}
                        />
                        <br/>
                    </Col>

                    <Col xs={6} md={6} className="item-avatar">
                        <Form.Label>Portrait</Form.Label>
                        <UploadImage callback={(image) => this.update_image(image, "portrait_url")} />
                        <img src={this.state.portrait_url}/>
                    </Col>

                    <Col xs={6} md={6} className="item-avatar">
                        <Form.Label>Avatar</Form.Label>
                        <UploadImage callback={(image) => this.update_image(image, "avatar_url")} />
                        <img src={this.state.avatar_url}/>
                    </Col>

                    <Col xs={12} md={12}>
                        <h4>Hit Points</h4>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group>
                            <Form.Label>Hit Point Dice</Form.Label>
                            <Form.Control name="hit_dice" as="select" onChange={(e) => {this.setState({hit_dice: e.target.value, has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {DiceValues.map((dice_value) => (
                                        <option key={dice_value} selected={this.state.hit_dice == dice_value} value={dice_value}>d{dice_value}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group>
                            <Form.Label>Base Hit Points</Form.Label>
                            <Form.Control value={this.state.base_hit_points} required name="base_hit_points" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group>
                            <Form.Label>Hit Point Attribute</Form.Label>
                            <Form.Control name="hit_point_attribute_id" as="select" onChange={(e) => {this.setState({hit_point_attribute_id: e.target.value, has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {Object.keys(AttributeID).map((attribute_id) => (
                                        <option key={attribute_id} selected={this.state.hit_point_attribute_id == attribute_id} value={attribute_id}>{AttributeID[attribute_id]}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    {this.state.has_spells &&
                        <Col xs={12} md={12}>
                            <h4 className="class-section-title">
                            Spells and Spell Slots
                            <span className="clickable-div dropper" onClick={() => {this.setState({spell_toggle: !this.state.spell_toggle})}}>
                                <i className={this.state.spell_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i>
                            </span>
                            </h4>
                        </Col>
                    }

                    {this.state.has_spells &&
                        <Col xs={12} md={12}>
                            <Collapse isOpen={this.state.spell_toggle}>
                                { this.state.spell_toggle &&
                                    <Row>

                                        <Col xs={6} md={6}>
                                            <Form.Group>
                                                <Form.Label>Spell Casting Attribute</Form.Label>
                                                <Form.Control name="spell_casting_attribute_id" as="select" onChange={(e) => {this.setState({spell_casting_attribute_id: e.target.value, has_changes: true})}} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(AttributeID).map((attribute_id) => (
                                                        <option key={attribute_id} selected={this.state.spell_casting_attribute_id == attribute_id} value={attribute_id}>{AttributeID[attribute_id]}</option>
                                                    ))}
                                            </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={6} md={6}>
                                            <Form.Group>
                                                <Form.Label>Is Ritual Spell Caster?</Form.Label>
                                                <Form.Control name="is_ritual_spell_caster" as="select" onChange={this.handleChange} custom>
                                                        <option selected={this.state.is_ritual_spell_caster == true} value={true}>True</option>
                                                        <option selected={this.state.is_ritual_spell_caster == false} value={false}>False</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            <Form.Group>
                                                <Form.Label>Knows All Spells?</Form.Label>
                                                <Form.Control name="knows_all_spells" as="select" onChange={this.handleChange} custom>
                                                        <option selected={this.state.knows_all_spells == true} value={true}>True</option>
                                                        <option selected={this.state.knows_all_spells == false} value={false}>False</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            <Form.Group>
                                                <Form.Label>Are Spells Prepared?</Form.Label>
                                                <Form.Control name="are_spells_prepared" as="select" onChange={this.handleChange} custom>
                                                        <option selected={this.state.are_spells_prepared == true} value={true}>True</option>
                                                        <option selected={this.state.are_spells_prepared == false} value={false}>False</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            <Form.Group>
                                                <Form.Label>Spell Container Name</Form.Label>
                                                <Form.Control value={this.state.spell_container_name} required name="spell_container_name" type="text" onChange={this.handleChange} />
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            { (this.state.knows_all_spells == false || (this.state.knows_all_spells == true && this.state.are_spells_prepared == true)) &&
                                                <Form.Group>
                                                    <Form.Label>Has Static Spell Count?</Form.Label>
                                                    <Form.Control name="has_static_spell_count" as="select" onChange={this.handleChange} custom>
                                                            <option selected={this.state.has_static_spell_count == true} value={true}>True</option>
                                                            <option selected={this.state.has_static_spell_count == false} value={false}>False</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            }
                                        </Col>

                                        <Col xs={4} md={4}>
                                            { this.state.has_static_spell_count == false && 
                                                <FormulaInput
                                                    name='Spell Count Formula'
                                                    value={this.state.custom_spell_count}
                                                    handle_change={(i) => {this.setState({custom_spell_count: i})}}
                                                />
                                            }
                                        </Col>
                                        
                                        <Col xs={12} md={12}>
                                            <table className="table-data">
                                                <tbody>
                                                    <tr className="headers">
                                                        <td>Level</td>
                                                        <td>Cantrips Known</td>
                                                        <td>Spells Known</td>
                                                        <td>1st</td>
                                                        <td>2nd</td>
                                                        <td>3rd</td>
                                                        <td>4th</td>
                                                        <td>5th</td>
                                                        <td>6th</td>
                                                        <td>7th</td>
                                                        <td>8th</td>
                                                        <td>9th</td>
                                                    </tr>
                                                    {Object.keys(this.state.spell_data).map((level_id) => (
                                                        <tr key={level_id}>
                                                            <td>
                                                                {LevelNames[level_id]}
                                                            </td>
                                                            <td>
                                                                <Form.Control value={this.state.spell_data[level_id]['cantrip']} data-level-id={level_id} name="cantrip" type="number" min={0} onChange={this.update_spell_data} placeholder="-" />
                                                            </td>
                                                            <td>
                                                                <Form.Control value={this.state.spell_data[level_id]['spells']} data-level-id={level_id} name="spells" type="number" min={0} onChange={this.update_spell_data} placeholder="-" />
                                                            </td>
                                                            {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((column_id) => (
                                                                <td>
                                                                    <Form.Control value={this.state.spell_data[level_id][column_id]} data-level-id={level_id} name={column_id} type="number" min={0} onChange={this.update_spell_data} placeholder="-" />
                                                                </td>
                                                            ))}
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </Col>
                                    </Row>
                                }
                            </Collapse>
                        </Col>
                    }

                    <Col xs={12} md={12}>
                        <h4 className="class-section-title">
                            Resources
                            <span className="clickable-div dropper" onClick={() => {this.setState({resource_toggle: !this.state.resource_toggle})}}>
                                <i className={this.state.resource_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i>
                            </span>
                        </h4>
                    </Col>

                    <Col xs={12} md={12}>
                        <Collapse isOpen={this.state.resource_toggle}>
                            { this.state.resource_toggle &&
                                <Row>
                                    <Col xs={12} md={12}>
                                        <div className="resource-items">
                                            <ListResources resources={this.state.resources} update_resource={this.update_resource} delete_resource={this.delete_resource}/>
                                        </div>
                                        
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <Button variant="primary" type="submit" onClick={this.add_resource}>
                                            Add New Resource
                                        </Button>
                                    </Col>
                                </Row>
                            }
                        </Collapse>
                    </Col>


                    <Col xs={12} md={12}>
                        <h4 className="class-section-title">
                            Class Features
                            <span className="clickable-div dropper" onClick={() => {this.setState({class_feature_toggle: !this.state.class_feature_toggle})}}>
                                <i className={this.state.class_feature_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i>
                            </span>
                        </h4>
                    </Col>

                    <Col xs={12} md={12}>
                        <Collapse isOpen={this.state.class_feature_toggle}>
                            { this.state.class_feature_toggle &&
                                <Row>
                                    <Col xs={12} md={12}>
                                        <div className="class-feature-items">
                                            <ListClassFeature class_features={class_features} update_class_feature={this.update_class_feature} delete_class_feature={this.delete_class_feature} has_spells={this.state.has_spells}/>
                                        </div>
                                        
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <Button variant="primary" type="submit" onClick={this.add_class_feature}>
                                            Add New Class Feature
                                        </Button>
                                    </Col>
                                </Row>
                            }
                        </Collapse>
                    </Col>

                    <Col xs={12} md={12}>
                        <h4 className="class-section-title">
                            Equipment
                            <span className="clickable-div dropper" onClick={() => {this.setState({equipment_toggle: !this.state.equipment_toggle})}}>
                                <i className={this.state.equipment_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i>
                            </span>
                        </h4>
                    </Col>

                    <Col xs={12} md={12}>
                        <Collapse isOpen={this.state.equipment_toggle}>
                            {this.state.equipment_toggle &&
                                <Row>
                                    <Col xs={12} md={12}>
                                        <EditEquipmentSet
                                            equipment_set={this.state.equipment_set}
                                            update_equipment_set={(id, data) => {this.setState({equipment_set: {...data}, has_changes: true})}}
                                        />
                                    </Col>
                                </Row>
                            }
                        </Collapse>
                    </Col>

                    <Col xs={12} md={12}>
                        <h4 className="class-section-title">
                            Prerequisites
                            <span className="clickable-div dropper" onClick={() => {this.setState({prerequisite_toggle: !this.state.prerequisite_toggle})}}>
                                <i className={this.state.prerequisite_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i>
                            </span>
                        </h4>
                    </Col>

                    <Col xs={12} md={12}>
                        <Collapse isOpen={this.state.prerequisite_toggle}>
                            {this.state.prerequisite_toggle &&
                                <Row>
                                    <Col xs={12} md={12}>
                                        <div className="prerequisite-items">
                                            <ListPrerequisites prerequisites={this.state.prerequisites} update_prerequisite={this.update_prerequisite} delete_prerequisite={this.delete_prerequisite}/>
                                        </div>
                                        
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <Button variant="primary" type="submit" onClick={this.add_prerequisite}>
                                            Add Prerequisite
                                        </Button>
                                    </Col>
                                </Row>
                            }
                        </Collapse>
                    </Col>
                
                </Row>
                <br/>
                <Button variant="primary" type="submit" onClick={this.save_class}>
                    Save Class
                </Button>
            </div>
        )

    };
}