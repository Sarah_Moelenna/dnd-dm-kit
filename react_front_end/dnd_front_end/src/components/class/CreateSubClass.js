import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { DND_GET, DND_PUT, DND_POST } from '../shared/DNDRequests';
import Button from 'react-bootstrap/Button'
import MDEditor from '@uiw/react-md-editor';
import { AttributeID, LevelNames, Atrributes } from '../monster/Data';
import { Collapse } from 'reactstrap';
import BackConfirmationButton from '../shared/BackConfirmationButton';
import { FormulaInput } from '../shared/FormulaInput';
import { ListResources, ListClassFeature, createResource, createClassFeature } from './CreateClass';


class CreateSubClass extends Component {

    state = {
        name: null,
        description: null,
        override_spells: false,
        override_resources: false,
        has_spells: false,
        spell_casting_attribute_id: null,
        is_ritual_spell_caster: false,
        knows_all_spells: false,
        are_spells_prepared: false,
        has_static_spell_count: true,
        custom_spell_count: null,
        spell_container_name: null,
        spell_data: {
            "1": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "2": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "3": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "4": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "5": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "6": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "7": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "8": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "9": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "10": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "11": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "12": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "13": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "14": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "15": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "16": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "17": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "18": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "19": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
            "20": {"cantrip": null, "spells": null, "1": null, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null}
        },
        resources: [],
        class_features: [],

        spell_toggle: false,
        resource_toggle: false,
        class_feature_toggle: false,
        has_changes: false,
    }

    componentWillMount() {
        if(this.props.edit_sub_class_id != undefined && this.props.edit_sub_class_id != null){
            this.copy_sub_class(this.props.edit_sub_class_id)
        } else {
            this.copy_class(this.props.base_class_id)
        }
    };

    copy_class = (class_id) => {

        DND_GET(
            '/class/' + class_id,
            (response) => {
                console.log(response)
                this.populate_with_class_data(response)
            },
            null
        )
    };

    copy_sub_class = (class_id) => {

        DND_GET(
            '/sub-class/' + class_id,
            (response) => {
                console.log(response)
                this.populate_with_sub_class_data(response)
            },
            null
        )
    };

    populate_with_sub_class_data = (class_data) => {
        this.setState(
            {
                name: class_data.name,
                description: class_data.description,
                has_spells: class_data.has_spells,
                override_spells: class_data.override_spells,
                override_resources: class_data.override_resources,
                spell_casting_attribute_id: class_data.spell_casting_attribute_id,
                is_ritual_spell_caster: class_data.is_ritual_spell_caster,
                knows_all_spells: class_data.knows_all_spells,
                are_spells_prepared: class_data.are_spells_prepared,
                spell_container_name: class_data.spell_container_name,
                spell_data: class_data.spell_data,
                resources: class_data.resources,
                class_features: class_data.class_features,
                has_static_spell_count: class_data.has_static_spell_count,
                custom_spell_count: class_data.custom_spell_count,
            }
        )
    }

    populate_with_class_data = (class_data) => {
        this.setState(
            {
                name: 'Unamed ' + class_data.name + ' Subclass',
                has_spells: class_data.has_spells,
                spell_casting_attribute_id: class_data.spell_casting_attribute_id,
                is_ritual_spell_caster: class_data.is_ritual_spell_caster,
                knows_all_spells: class_data.knows_all_spells,
                are_spells_prepared: class_data.are_spells_prepared,
                spell_container_name: class_data.spell_container_name,
                spell_data: class_data.spell_data,
                resources: class_data.resources,
                has_static_spell_count: class_data.has_static_spell_count,
                custom_spell_count: class_data.custom_spell_count,
            }
        )
    }

    save_class = () => {

        var final_class = {
            name: this.state.name,
            description: this.state.description,
            has_spells: this.state.has_spells,
            override_spells: this.state.override_spells,
            override_resources: this.state.override_resources,
            spell_casting_attribute_id: this.state.spell_casting_attribute_id,
            is_ritual_spell_caster: this.state.is_ritual_spell_caster,
            knows_all_spells: this.state.knows_all_spells,
            are_spells_prepared: this.state.are_spells_prepared,
            spell_container_name: this.state.spell_container_name,
            spell_data: this.state.spell_data,
            resources: this.state.resources,
            class_features: this.state.class_features,
            has_static_spell_count: this.state.has_static_spell_count,
            custom_spell_count: this.state.custom_spell_count,
        }
        console.log(final_class)

        if(this.props.edit_sub_class_id != undefined && this.props.edit_sub_class_id != null){
            DND_PUT(
                '/sub-class/' + this.props.edit_sub_class_id,
                {data: final_class},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        } else {
            DND_POST(
                '/class/' + this.props.base_class_id + '/sub-class',
                {data: final_class},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        }
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.setState({ [e.target.name]: new_value, has_changes: true });
    }

    update_text = (text) => {
        this.setState({description: text, has_changes: true})
    }

    update_subclass_description = (text) => {
        this.setState({subclass_description: text, has_changes: true})
    }

    update_spell_data = (e) => {
        var new_spell_data = this.state.spell_data
        var value = e.target.value
        var level_id = e.target.dataset.levelId
        var id = e.target.name
        new_spell_data[level_id][id] = value
        this.setState({spell_data: new_spell_data, has_changes: true})
    }

    add_resource = () => {
        var new_resource = createResource()
        var resources = this.state.resources
        resources.push(new_resource)
        this.setState({resources: resources, has_changes: true})
    }

    delete_resource = (id) => {
        var resources = []
        for(var i = 0; i < this.state.resources.length; i++){
            var resource = this.state.resources[i]
            if(resource.id != id){
                resources.push(resource)
            }
        }
        this.setState({resources: resources, has_changes: true})
    }

    update_resource = (id, attribute, value) => {
        var resources = []
        for(var i = 0; i < this.state.resources.length; i++){
            var resource = this.state.resources[i]
            if(resource.id == id){
                resource[attribute] = value
            }
            resources.push(resource)
        }
        this.setState({resources: resources, has_changes: true})
    }


    add_class_feature = () => {
        var new_class_feature = createClassFeature()
        var class_features = this.state.class_features
        class_features.push(new_class_feature)
        this.setState({class_features: class_features, has_changes: true})
    }

    delete_class_feature = (id) => {
        var class_features = []
        for(var i = 0; i < this.state.class_features.length; i++){
            var class_feature = this.state.class_features[i]
            if(class_feature.id != id){
                class_features.push(class_feature)
            }
        }
        this.setState({class_features: class_features, has_changes: true})
    }

    update_class_feature = (id, attribute, value) => {
        var class_features = []
        for(var i = 0; i < this.state.class_features.length; i++){
            var class_feature = this.state.class_features[i]
            if(class_feature.id == id){
                class_feature[attribute] = value
            }
            class_features.push(class_feature)
        }
        this.setState({class_features: class_features, has_changes: true})
    }

    get_sorted_class_features = () => {
        var final_members = []
        var levels = []
        var sorting_object = {}
        for (var i = 0; i < this.state.class_features.length; i++){
            var member = this.state.class_features[i]
            var key = 0
            if (member.level != null){
                key = parseInt(member.level)
            }
            if (levels.includes(key) == false){
                levels.push(key)
            }
            if (sorting_object[key] == undefined){
                sorting_object[key] = []
            }
            sorting_object[key].push(member)
        }
        
        levels.sort((a, b) => b - a);
        levels.reverse()

        for(var i = 0; i < levels.length; i++){
            var members = sorting_object[levels[i]]
            members.sort((a, b) => (a.name > b.name) ? 1 : -1)
            final_members = final_members.concat(members)
        }
        return final_members
    }

    update_image = (image, attribute) =>{
        this.setState({[attribute]: image, has_changes: true})
    }
            
    render(){

        var class_features = this.get_sorted_class_features()

        return(
            <div className="class">
                {(this.props.edit_sub_class_id == undefined || this.props.edit_sub_class_id == null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Create Subclass</h2>
                    </div>
                }
                {(this.props.edit_sub_class_id != undefined && this.props.edit_sub_class_id != null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Edit Subclass</h2>
                    </div>
                }
                <Row className="class-settings">
                    <Col xs={12} md={12}>
                        <h4>Basic Information</h4>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control value={this.state.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        {this.state.override_spells &&
                            <Form.Group>
                                <Form.Label>Has Spells?</Form.Label>
                                <Form.Control name="has_spells" as="select" onChange={this.handleChange} custom>
                                        <option selected={this.state.has_spells == true} value={true}>True</option>
                                        <option selected={this.state.has_spells == false} value={false}>False</option>
                                </Form.Control>
                            </Form.Group>
                        }
                    </Col>

                    <Col xs={4} md={4}></Col>

                    <Col xs={12} md={12}>
                        <Form.Label>Description</Form.Label>
                        <MDEditor
                            value={this.state.description}
                            preview="edit"
                            onChange={this.update_text}
                        />
                        <br/>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group>
                            <Form.Label>Override Spell Data?</Form.Label>
                            <Form.Control name="override_spells" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.override_spells == true} value={true}>True</option>
                                    <option selected={this.state.override_spells == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group>
                            <Form.Label>Override Resource Data?</Form.Label>
                            <Form.Control name="override_resources" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.override_resources == true} value={true}>True</option>
                                    <option selected={this.state.override_resources == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    {this.state.has_spells && this.state.override_spells &&
                        <Col xs={12} md={12}>
                            <h4 className="class-section-title">
                            Spells and Spell Slots
                            <span className="clickable-div dropper" onClick={() => {this.setState({spell_toggle: !this.state.spell_toggle})}}>
                                <i className={this.state.spell_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i>
                            </span>
                            </h4>
                        </Col>
                    }

                    {this.state.has_spells && this.state.override_spells &&
                        <Col xs={12} md={12}>
                            <Collapse isOpen={this.state.spell_toggle}>
                                { this.state.spell_toggle &&
                                    <Row>

                                        <Col xs={6} md={6}>
                                            <Form.Group>
                                                <Form.Label>Spell Casting Attribute</Form.Label>
                                                <Form.Control name="spell_casting_attribute_id" as="select" onChange={(e) => {this.setState({spell_casting_attribute_id: e.target.value, has_changes: true})}} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(AttributeID).map((attribute_id) => (
                                                        <option key={attribute_id} selected={this.state.spell_casting_attribute_id == attribute_id} value={attribute_id}>{AttributeID[attribute_id]}</option>
                                                    ))}
                                            </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={6} md={6}>
                                            <Form.Group>
                                                <Form.Label>Is Ritual Spell Caster?</Form.Label>
                                                <Form.Control name="is_ritual_spell_caster" as="select" onChange={this.handleChange} custom>
                                                        <option selected={this.state.is_ritual_spell_caster == true} value={true}>True</option>
                                                        <option selected={this.state.is_ritual_spell_caster == false} value={false}>False</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            <Form.Group>
                                                <Form.Label>Knows All Spells?</Form.Label>
                                                <Form.Control name="knows_all_spells" as="select" onChange={this.handleChange} custom>
                                                        <option selected={this.state.knows_all_spells == true} value={true}>True</option>
                                                        <option selected={this.state.knows_all_spells == false} value={false}>False</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            <Form.Group>
                                                <Form.Label>Are Spells Prepared?</Form.Label>
                                                <Form.Control name="are_spells_prepared" as="select" onChange={this.handleChange} custom>
                                                        <option selected={this.state.are_spells_prepared == true} value={true}>True</option>
                                                        <option selected={this.state.are_spells_prepared == false} value={false}>False</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            <Form.Group>
                                                <Form.Label>Spell Container Name</Form.Label>
                                                <Form.Control value={this.state.spell_container_name} required name="spell_container_name" type="text" onChange={this.handleChange} />
                                            </Form.Group>
                                        </Col>

                                        <Col xs={4} md={4}>
                                            { (this.state.knows_all_spells == false || (this.state.knows_all_spells == true && this.state.are_spells_prepared == true)) &&
                                                <Form.Group>
                                                    <Form.Label>Has Static Spell Count?</Form.Label>
                                                    <Form.Control name="has_static_spell_count" as="select" onChange={this.handleChange} custom>
                                                            <option selected={this.state.has_static_spell_count == true} value={true}>True</option>
                                                            <option selected={this.state.has_static_spell_count == false} value={false}>False</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            }
                                        </Col>

                                        <Col xs={4} md={4}>
                                            { this.state.has_static_spell_count == false && 
                                                <FormulaInput
                                                    name='Spell Count Formula'
                                                    value={this.state.custom_spell_count}
                                                    handle_change={(i) => {this.setState({custom_spell_count: i})}}
                                                />
                                            }
                                        </Col>
                                        
                                        <Col xs={12} md={12}>
                                            <table className="table-data">
                                                <tbody>
                                                    <tr className="headers">
                                                        <td>Level</td>
                                                        <td>Cantrips Known</td>
                                                        <td>Spells Known</td>
                                                        <td>1st</td>
                                                        <td>2nd</td>
                                                        <td>3rd</td>
                                                        <td>4th</td>
                                                        <td>5th</td>
                                                        <td>6th</td>
                                                        <td>7th</td>
                                                        <td>8th</td>
                                                        <td>9th</td>
                                                    </tr>
                                                    {Object.keys(this.state.spell_data).map((level_id) => (
                                                        <tr key={level_id}>
                                                            <td>
                                                                {LevelNames[level_id]}
                                                            </td>
                                                            <td>
                                                                <Form.Control value={this.state.spell_data[level_id]['cantrip']} data-level-id={level_id} name="cantrip" type="number" min={0} onChange={this.update_spell_data} placeholder="-" />
                                                            </td>
                                                            <td>
                                                                <Form.Control value={this.state.spell_data[level_id]['spells']} data-level-id={level_id} name="spells" type="number" min={0} onChange={this.update_spell_data} placeholder="-" />
                                                            </td>
                                                            {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((column_id) => (
                                                                <td>
                                                                    <Form.Control value={this.state.spell_data[level_id][column_id]} data-level-id={level_id} name={column_id} type="number" min={0} onChange={this.update_spell_data} placeholder="-" />
                                                                </td>
                                                            ))}
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </Col>
                                    </Row>
                                }
                            </Collapse>
                        </Col>
                    }

                    { this.state.override_resources &&
                        <Col xs={12} md={12}>
                            <h4 className="class-section-title">
                                Resources
                                <span className="clickable-div dropper" onClick={() => {this.setState({resource_toggle: !this.state.resource_toggle})}}>
                                    <i className={this.state.resource_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i>
                                </span>
                            </h4>
                        </Col>
                    }

                    { this.state.override_resources &&
                        <Col xs={12} md={12}>
                            <Collapse isOpen={this.state.resource_toggle}>
                                { this.state.resource_toggle &&
                                    <Row>
                                        <Col xs={12} md={12}>
                                            <div className="resource-items">
                                                <ListResources resources={this.state.resources} update_resource={this.update_resource} delete_resource={this.delete_resource}/>
                                            </div>
                                            
                                        </Col>

                                        <Col xs={12} md={12}>
                                            <Button variant="primary" type="submit" onClick={this.add_resource}>
                                                Add New Resource
                                            </Button>
                                        </Col>
                                    </Row>
                                }
                            </Collapse>
                        </Col>
                    }

                    <Col xs={12} md={12}>
                        <h4 className="class-section-title">
                            Subclass Features
                            <span className="clickable-div dropper" onClick={() => {this.setState({class_feature_toggle: !this.state.class_feature_toggle})}}>
                                <i className={this.state.class_feature_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i>
                            </span>
                        </h4>
                    </Col>

                    <Col xs={12} md={12}>
                        <Collapse isOpen={this.state.class_feature_toggle}>
                            { this.state.class_feature_toggle &&
                                <Row>
                                    <Col xs={12} md={12}>
                                        <div className="class-feature-items">
                                            <ListClassFeature class_features={class_features} update_class_feature={this.update_class_feature} delete_class_feature={this.delete_class_feature} has_spells={this.state.has_spells}/>
                                        </div>
                                        
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <Button variant="primary" type="submit" onClick={this.add_class_feature}>
                                            Add New Subclass Feature
                                        </Button>
                                    </Col>
                                </Row>
                            }
                        </Collapse>
                    </Col>
                
                </Row>
                <br/>
                <Button variant="primary" type="submit" onClick={this.save_class}>
                    Save Subclass
                </Button>
            </div>
        )

    };
}

export default CreateSubClass;