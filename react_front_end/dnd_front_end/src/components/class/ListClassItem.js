import React, { useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Link } from 'react-router-dom';
import { DND_DELETE } from '../shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import { Collapse } from 'reactstrap';

const ClassItem = ({ dnd_class, refresh_function, view_class_function, edit_class_function, view_sub_class_function, edit_sub_class_function, setOpen, isOpen}) => {

    function delete_function(id){
        DND_DELETE(
            '/class/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    function delete_sub_function(id){
        DND_DELETE(
            '/sub-class/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    return (
        <div key={dnd_class.id} className="class-item">
            <Row>
                <Col xs={7} md={7}>
                    <p className="card-text">{dnd_class.name}</p>
                </Col>
                <Col xs={4} md={4}>
                    <Link className="btn btn-primary" to={"/class/" + dnd_class.id} onClick={() => view_class_function(dnd_class.id)}>
                        View
                    </Link >
                    { dnd_class.can_edit == true &&
                        <a className="btn btn-primary" onClick={() => edit_class_function(dnd_class.id)}>Edit Class</a>
                    }
                    { dnd_class.can_edit == true &&
                        <DeleteConfirmationButton id={dnd_class.id} override_button="Delete" name="Class" delete_function={delete_function} />
                    }
                </Col>
                <Col xs={1} md={1}>
                    <div className="clickable-div" onClick={() => {setOpen(isOpen == false ? dnd_class.id : null)}}><i className={isOpen == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                </Col>
                <Col xs={12} md={12}>
                    <Collapse isOpen={isOpen}>
                        { isOpen &&
                            <div>
                                <Row>
                                    <Col xs={2} md={2}>
                                        <p><b>Subclasses</b></p>
                                    </Col>
                                    <Col xs={2} md={2}>
                                    <a className="btn btn-primary" onClick={() => edit_sub_class_function(null, dnd_class.id)}>Create Subclass</a>
                                    </Col>
                                </Row>
                                {dnd_class.subclasses.map((sub_class) => (
                                    <div key={sub_class.id} className="class-item">
                                        <Row>
                                            <Col xs={8} md={8}>
                                                <p className="card-text">{sub_class.name}</p>
                                            </Col>
                                            <Col xs={4} md={4}>
                                                <Link className="btn btn-primary" to={"/sub-class/" + sub_class.id} onClick={() => view_sub_class_function(sub_class.id)}>
                                                    View
                                                </Link >
                                                { sub_class.can_edit == true &&
                                                    <a className="btn btn-primary" onClick={() => edit_sub_class_function(sub_class.id, dnd_class.id)}>Edit Subclass</a>
                                                }
                                                { sub_class.can_edit == true &&
                                                    <DeleteConfirmationButton id={sub_class.id} override_button="Delete" name="Subclass" delete_function={delete_sub_function} />
                                                }
                                            </Col>
                                        </Row>
                                        <hr/>
                                    </div>
                                ))}
                            </div>
                        }
                    </Collapse>
                </Col>
            </Row>
            <hr/>
        </div>
    )

}

const ListClassItem = ({ classes, refresh_function, view_class_function, edit_class_function, view_sub_class_function, edit_sub_class_function, set_sort, current_sort_by, current_sort_value}) => {

    const [isOpen, setIsOpen] = useState(null);

    const setOpen = (value) => setIsOpen(value);

    function get_sort(sort_by){
        if(current_sort_by == sort_by){
            if(current_sort_value == "DESC"){
                return (
                    <div className="clickable-div" onClick={() => {set_sort(null, null)}}>
                        <i className="fas fa-sort-down"></i>
                    </div>
                ) 
            } else {
                return (
                    <div className="clickable-div" onClick={() => {set_sort(sort_by, "DESC")}}>
                        <i className="fas fa-sort-up"></i>
                    </div>
                ) 
            }
        } else {
            return (
                <div className="clickable-div" onClick={() => {set_sort(sort_by, "ASC")}}>
                    <i className="fas fa-sort"></i>
                </div>
            )
        }
    }

    if(classes === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="class-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={8} md={8}>
                        <p className="card-text"><b>Name{get_sort("name")}</b></p>
                    </Col>
                    <Col xs={4} md={4}>

                    </Col>
                </Row>
                <hr/>
            </div>
            {classes.map((dnd_class) => (
                <ClassItem 
                    dnd_class={dnd_class}
                    refresh_function={refresh_function}
                    view_class_function={view_class_function}
                    edit_class_function={edit_class_function}
                    view_sub_class_function={view_sub_class_function}
                    edit_sub_class_function={edit_sub_class_function}
                    setOpen={setOpen}
                    isOpen={dnd_class.id==isOpen}
                />
            ))}
        </div>
    );
}

export default ListClassItem;