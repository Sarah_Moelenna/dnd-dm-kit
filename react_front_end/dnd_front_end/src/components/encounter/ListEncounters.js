import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import EncounterShortCodeGenerator from './EncounterShortCodeGenerator'
import { DND_DELETE } from '.././shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListEncounters = ({ encounters, refresh_function, view_encounter_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/encounter/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(encounters === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="encounter-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={3} md={3} className="cenetered-column">
                        <p className="card-text"><b>Campaign</b></p>
                    </Col>
                    <Col xs={3} md={3} className="cenetered-column">
                        <p className="card-text"><b>Summary</b></p>
                    </Col>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Controls</b></p>
                    </Col>
                </Row>
                <hr/>
            </div>
            {encounters.map((encounter) => (
                <div key={encounter.id} className="encounter-item">
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{encounter.name}</p>
                        </Col>
                        <Col xs={3} md={3} className="cenetered-column">
                            {encounter.campaign != null &&
                                <p className="card-text">{encounter.campaign.name}</p>
                            }
                        </Col>
                        <Col xs={3} md={3} className="cenetered-column">
                            <p className="card-text">{encounter.summary}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <a className="btn btn-primary" onClick={() => view_encounter_function(encounter.id)}>Edit</a>
                            <DeleteConfirmationButton id={encounter.id} name="Encounter" delete_function={delete_page_function} override_button="Delete"/>
                            <EncounterShortCodeGenerator encounter={encounter}/>
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListEncounters;