import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

class SingleEncounterCustomParticipant extends Component {

    handleSubmit = (event) => {
        
        event.preventDefault();
      
    }

    handleChange = e => {
        if (e.target.name == 'display_name'){
            this.props.edit_custom_function(this.props.participant.id, e.target.value, this.props.participant.delayed)
        }
    };

    update_delayed = (value) => {
        this.props.edit_custom_function(this.props.participant.id, this.props.participant.display_name, value)
    }
            
    render(){

        return(
            <div className="monster">
                <Row>
                    <Col xs={1} md={1}>

                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text">{this.props.participant.name}</p>
                    </Col>
                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicName">
                            <Form.Control value={this.props.participant.display_name} name="display_name" type="text" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>
                    <Col xs={2} md={2}>
          
                    </Col>
                    <Col xs={2} md={2}>
 
                    </Col>
                    <Col xs={2} md={2}>
                        <p className='delayed-status'>
                            {this.props.participant.delayed == 1 ? 
                                <i className="fas fa-toggle-on"  onClick={() => this.update_delayed(0)}></i>
                                :
                                <i className="fas fa-toggle-off"  onClick={() => this.update_delayed(1)}></i>
                            }
                        </p>
                    </Col>
                    <Col xs={1} md={1}>
                        <i className="fas fa-trash clickable-icon" onClick={() => {this.props.remove_custom_function(this.props.participant.id)}}></i>
                    </Col>
                </Row>

            </div>
        )

    };
}

export default SingleEncounterCustomParticipant;