import React, { Component } from 'react';
import ListEncounterMonsters from './ListEncounterMonsters'
import Card from 'react-bootstrap/Card'
import APIButton from '../shared/APIButton';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import DifficultyCalculator from '../shared/DifficultyCalculator';
import EncounterShortCodeGenerator from './EncounterShortCodeGenerator'
import AddCustom from './AddCustom';
import { v4 as uuidv4 } from 'uuid';
import { DND_PUT, DND_POST } from '.././shared/DNDRequests';
import { get_api_url } from '../shared/Config';
import MonsterSelect from '../monster/MonsterSelect';
import BattlemapSelect from '../battlemap/BattlemapSelect';
import MusicSelect from '../music/MusicSelect';
import PlaylistSelect from '../playlist/PlaylistSelect';
import CampaignSelect from '../campaign/CampaignSelect';
import { Collapse } from 'reactstrap';
import Button from 'react-bootstrap/Button'

class SingleEncounter extends Component {

    state = {
        difficulty_toggle: false
    }

    get_battlemap_id = () => {
        if(this.props.encounter.battlemap == undefined){
            return null;
        }
        return this.props.encounter.battlemap.id
    }

    get_song_id = () => {
        if(this.props.encounter.song == undefined){
            return null;
        }
        return this.props.encounter.song.id
    }

    get_playlist_id = () => {
        if(this.props.encounter.playlist == undefined){
            return null;
        }
        return this.props.encounter.playlist.id
    }

    get_campaign_id = () => {
        if(this.props.encounter.campaign == undefined){
            return null;
        }
        return this.props.encounter.campaign.id
    }


    add_monster_to_encounter = (monster_id) => {

        var data = {
            monster_id: monster_id,
            count: 1,
            grouping_type: "SINGLE"
        }

        DND_POST(
            '/encounter/' + this.props.encounter.id + '/encounter_monster',
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    };

    add_music_to_encounter = (audio_id) => {

        var data = {
            audio_id: audio_id,
            battlemap_id: this.get_battlemap_id(),
            campaign_id: this.get_campaign_id(),
        }

        DND_PUT(
            '/encounter/' + this.props.encounter.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )

    };

    add_playlist_to_encounter = (playlist_id) => {

        var data = {
            battlemap_id: this.get_battlemap_id(),
            campaign_id: this.get_campaign_id(),
            playlist_id: playlist_id,
        }

        DND_PUT(
            '/encounter/' + this.props.encounter.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )

    };

    add_campaign_to_encounter = (campaign_id) => {

        var data = {
            audio_id: this.get_song_id(),
            battlemap_id: this.get_battlemap_id(),
            playlist_id: this.get_playlist_id(),
            campaign_id: campaign_id,
        }

        DND_PUT(
            '/encounter/' + this.props.encounter.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )

    };

    add_battlemap_to_encounter = (battlemap_id) => {

        var data = {
            battlemap_id: battlemap_id,
            audio_id: this.get_song_id(),
            campaign_id: this.get_campaign_id(),
            playlist_id: this.get_playlist_id(),
        }

        DND_PUT(
            '/encounter/' + this.props.encounter.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )

    };

    add_custom = (name) => {
        var new_custom = this.props.encounter.custom

        var new_data = {
            "type": "CUSTOM",
            "id": uuidv4(),
            "name": name,
            "display_name": null,
            "conditions": [],
            "state": "ALIVE",
            "initiative": null,
            "damage_taken": 0,
            "delayed": false,
            "x": null,
            "y": null
        }

        new_custom.push(new_data)

        var data = {
            audio_id: this.get_song_id(),
            custom: new_custom,
            battlemap_id: this.get_battlemap_id(),
            campaign_id: this.get_campaign_id(),
        }

        DND_PUT(
            '/encounter/' + this.props.encounter.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )

    }

    edit_custom = (id, display_name, delayed) => {
        var new_custom = this.props.encounter.custom

        for(var i = 0; i <  new_custom.length; i++){
            if(new_custom[i].id == id){
                new_custom[i].display_name = display_name
                new_custom[i].delayed = delayed
            }
        }
        var data = {
            audio_id: this.get_song_id(),
            custom: new_custom,
            battlemap_id: this.get_battlemap_id(),
            campaign_id: this.get_campaign_id(),
        }

        DND_PUT(
            '/encounter/' + this.props.encounter.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )

    }

    remove_custom = (id) => {
        var new_custom = []

        for(var i = 0; i <  this.props.encounter.custom.length; i++){
           
            if(this.props.encounter.custom[i].id != id){
                new_custom.push(this.props.encounter.custom[i])
            }
        }
        var data = {
            audio_id: this.get_song_id(),
            custom: new_custom,
            battlemap_id: this.get_battlemap_id(),
            campaign_id: this.get_campaign_id(),
        }

        DND_PUT(
            '/encounter/' + this.props.encounter.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )

    }

    edit_encounter_monster = (monster_id, count, grouping_type, display_name, delayed_count) => {

        var data = {
            monster_id: monster_id,
            count: count,
            grouping_type: grouping_type,
            display_name: display_name,
            delayed_count: delayed_count,
        }

        DND_POST(
            '/encounter/' + this.props.encounter.id + '/encounter_monster',
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    };

    create_combat = () => {

        var data = {
            encounter_id: this.props.encounter.id ,
        }

        DND_POST(
            '/combat/',
            data,
            (jsondata) => {
                this.props.set_combat_function(jsondata)
            },
            null
        )
    };

    create_audio_command = (id, play_type) => {
        var data = {
            "music_play_type": play_type,
            "audio_id": id
        }
        return {
            "command_code": "CMD_PLAY",
            "data": data
        }
    }

    create_playlist_command = (id, play_type, playlist_type) => {
        var data = {
            "music_play_type": play_type,
            "playlist_id": id,
            "type": playlist_type
        }
        return {
            "command_code": "CMD_PLAYLIST",
            "data": data
        }
    }

    get_monster_ids = () => {
        var ids = []
        for ( var i = 0; i < this.props.encounter.encounter_monsters.length; i++){
            ids.push(
                this.props.encounter.encounter_monsters[i].monster.id
            )
        }
        return ids
    }
            
    render(){

        return(
            <div className="encounter">
                <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_encounter_fuction}></i> {this.props.encounter.name} <EncounterShortCodeGenerator encounter={this.props.encounter}/></h2>
                <Row>
                    <Col xs={3} md={3}>
                        <p><b>Monster Count -</b> {this.props.encounter.monster_count}</p>
                        <p><b>Total XP -</b> {this.props.encounter.total_xp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                        <p><b>Monster Count Modifier -</b> x{this.props.encounter.modifier}</p>
                        <p><b>Modified Total XP -</b> {this.props.encounter.modified_xp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>

                        <MonsterSelect select_function={this.add_monster_to_encounter} disabled_ids={this.get_monster_ids()} override_button="Add Monster to Encounter"/>
                        <AddCustom add_custom_function={this.add_custom}/>
                        <Button 
                            onClick={() => {this.setState({difficulty_toggle: !this.state.difficulty_toggle})}}
                            className="difficulty-button"
                        >
                            {this.state.difficulty_toggle ? "Hide Difficulty Calculator" : "Show Difficulty Calculator"}
                        </Button>
                    </Col>
                    <Col xs={3} md={3}>
                        <div className="resource">
                            <h2>Encounter Battlemap</h2>
                            <BattlemapSelect select_function={this.add_battlemap_to_encounter} disabled_ids={[this.get_battlemap_id()]}/>
                            {this.props.encounter.battlemap != null &&
                                <Card>
                                    <Card.Img variant="top" src={get_api_url() + "/" + this.props.encounter.battlemap.image} />
                                    <Card.Body>
                                        <Card.Title>{this.props.encounter.battlemap.name}</Card.Title>
                                    </Card.Body>
                                </Card>
                            }
                        </div>
                    </Col>
                    <Col xs={3} md={3}>
                        <div className="resource">
                            <h2>Encounter Music</h2>
                            <MusicSelect select_function={this.add_music_to_encounter} disabled_ids={[this.get_song_id()]}/>
                            <PlaylistSelect select_function={this.add_playlist_to_encounter} disabled_ids={[this.get_playlist_id()]}/>
                            {this.props.encounter.song != null &&
                                <Card>
                                    <Card.Body>
                                        <Card.Title>{this.props.encounter.song.name}</Card.Title>
                                        <APIButton data={this.create_audio_command(this.props.encounter.song.id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton>
                                    </Card.Body>
                                </Card>
                            }
                            {this.props.encounter.playlist != null &&
                                <Card>
                                    <Card.Body>
                                        <Card.Title>{this.props.encounter.playlist.name}</Card.Title>
                                        <APIButton data={this.create_playlist_command(this.props.encounter.playlist.id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton>
                                    </Card.Body>
                                </Card>
                            }
                        </div>
                    </Col>
                    <Col xs={3} md={3}>
                        <div className="resource">
                            <h2>Encounter Campaign</h2>
                            <CampaignSelect select_function={this.add_campaign_to_encounter} disabled_ids={[this.get_campaign_id()]}/>
                            {this.props.encounter.campaign != null &&
                                <Card>
                                    <Card.Body>
                                        <Card.Title>{this.props.encounter.campaign.name}</Card.Title>
                                    </Card.Body>
                                </Card>
                            }
                        </div>
                    </Col>
                </Row>
                
                <Collapse isOpen={this.state.difficulty_toggle}>
                    <hr/>
                        <DifficultyCalculator xp={this.props.encounter.modified_xp}/>
                    <hr/>
                </Collapse>
                <div className="monsters">
                    <br/>
                    <ListEncounterMonsters 
                        encounter_id={this.props.encounter.id}
                        encounter_monsters={this.props.encounter.encounter_monsters}
                        custom_participants={this.props.encounter.custom}
                        refresh_function={this.props.refresh_function}
                        view_encounter_function={this.display_encounter}
                        edit_encounter_monster_function={this.edit_encounter_monster}
                        edit_custom_function={this.edit_custom}
                        remove_custom_function={this.remove_custom}
                    />
                </div>
                <hr/>
                <p>Create and start a new combat from this encounter, using the currently active campaign and its currently tracked players.</p>
                <a className="btn btn-primary" onClick={() => this.create_combat()}>Start a New Combat</a>
            </div>
        )

    };
}

export default SingleEncounter;