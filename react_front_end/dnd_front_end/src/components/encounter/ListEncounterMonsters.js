import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SingleEncounterMonster from './SingleEncounterMonster'
import SingleEncounterCustomParticipant from './SingleEncounterCustomParticipant';


const ListEncounterMonsters = ({ encounter_monsters, refresh_function, edit_encounter_monster_function, custom_participants, edit_custom_function, remove_custom_function}) => {
    if(encounter_monsters === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="encounter-monster-items">
            <div className="headers">
                <Row>
                    <Col xs={1} md={1}>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Monster Name</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Display Name</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Grouping</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Count</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Delayed Entry</b></p>
                    </Col>
                    <Col xs={1} md={1}>
                    </Col>
                </Row>
            </div>
            {encounter_monsters.map((encounter_monster) => (
                <div key={encounter_monster.id} className="encounter-monster-item">
                    <hr/>
                    <SingleEncounterMonster encounter_monster={encounter_monster} refresh_function={refresh_function} edit_encounter_monster_function={edit_encounter_monster_function}/>
                </div>
            ))}
            {custom_participants.map((custom_participant) => (
                <div key={custom_participant.id} className="encounter-monster-item">
                    <hr/>
                    <SingleEncounterCustomParticipant participant={custom_participant} edit_custom_function={edit_custom_function} remove_custom_function={remove_custom_function}/>
                </div>
            ))}
        </div>
    );
}

export default ListEncounterMonsters;