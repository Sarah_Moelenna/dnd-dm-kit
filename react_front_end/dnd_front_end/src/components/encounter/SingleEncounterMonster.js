import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import APIButton from '.././shared/APIButton';
import Form from 'react-bootstrap/Form';
import MonsterInfo from '../monster/MonsterInfo';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

class SingleEncounterMonster extends Component {

    state = {
        toggle: false
    }

    handleSubmit = (event) => {
        
        event.preventDefault();
      
    }

    handleChange = e => {
        if (e.target.name == 'count'){
            this.props.edit_encounter_monster_function(this.props.encounter_monster.monster.id, e.target.value, this.props.encounter_monster.grouping_type, this.props.encounter_monster.display_name, this.props.encounter_monster.delayed)
        } else if (e.target.name == 'grouping_type'){
            this.props.edit_encounter_monster_function(this.props.encounter_monster.monster.id, this.props.encounter_monster.count, e.target.value, this.props.encounter_monster.display_name, this.props.encounter_monster.delayed)
        } else if (e.target.name == 'display_name'){
            this.props.edit_encounter_monster_function(this.props.encounter_monster.monster.id, this.props.encounter_monster.count, this.props.encounter_monster.grouping_type, e.target.value, this.props.encounter_monster.delayed)
        } else if (e.target.name == 'delayed_count'){
            this.props.edit_encounter_monster_function(this.props.encounter_monster.monster.id, this.props.encounter_monster.count, this.props.encounter_monster.grouping_type, this.props.encounter_monster.display_name, e.target.value)
        }
    };

    update_delayed = (value) => {
        this.props.edit_encounter_monster_function(this.props.encounter_monster.monster.id, this.props.encounter_monster.count, this.props.encounter_monster.grouping_type, this.props.encounter_monster.display_name, value)
    }
            
    render(){

        return(
            <div className="monster">
                <Row>
                    <Col xs={1} md={1}>
                        <div className="image">
                            <img src={this.props.encounter_monster.monster.image}></img>
                        </div>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text">{this.props.encounter_monster.monster.name}</p>
                        <Button onClick={() => {this.setState({toggle: true})}}>More Info</Button>
                        <Modal show={this.state.toggle} size="md" className="select-modal monster">
                            <Modal.Header>{this.props.encounter_monster.monster.name}
                                <Button onClick={()=>{this.setState({toggle: false})}}>Close</Button>
                            </Modal.Header>
                            <Modal.Body>
                                <MonsterInfo monster={this.props.encounter_monster.monster} show_roll_buttons={false} show_more_info={true}/>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={()=>{this.setState({toggle: false})}}>Close</Button>
                            </Modal.Footer>
                        </Modal>
                    </Col>
                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicName">
                            <Form.Control value={this.props.encounter_monster.display_name} name="display_name" type="text" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>
                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicGroupind">
                            <Form.Control name="grouping_type" as="select" size="m" onChange={this.handleChange} custom>
                                    <option value="SINGLE" selected={this.props.encounter_monster.grouping_type == "SINGLE"}>Single</option>
                                    <option value="MULTIPLE" selected={this.props.encounter_monster.grouping_type == "MULTIPLE"}>Multiple</option>
                                    <option value="SWARM" selected={this.props.encounter_monster.grouping_type == "SWARM"}>Swarm</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col xs={2} md={2}>
                        {this.props.encounter_monster.grouping_type != 'SINGLE' &&
                            <Form.Group controlId="formBasicName">
                                <Form.Control value={this.props.encounter_monster.count} name="count" type="number"onChange={this.handleChange}/>
                            </Form.Group>
                        }
                    </Col>
                    <Col xs={2} md={2}>
                        {this.props.encounter_monster.grouping_type == 'MULTIPLE' ?
                            <Form.Group controlId="formBasicName">
                                <Form.Control value={this.props.encounter_monster.delayed_count} name="delayed_count" type="number"onChange={this.handleChange}/>
                            </Form.Group>
                            :
                            <p className='delayed-status'>
                                {this.props.encounter_monster.delayed_count == 1 ? 
                                    <i className="fas fa-toggle-on"  onClick={() => this.update_delayed(0)}></i>
                                    :
                                    <i className="fas fa-toggle-off"  onClick={() => this.update_delayed(1)}></i>
                                }
                            </p>
                        }
                    </Col>
                    <Col xs={1} md={1}>
                        <APIButton api_endpoint={"/encounter_monster/" + this.props.encounter_monster.id} method="DELETE" icon="fas fa-trash" post_response_function={this.props.refresh_function}></APIButton>
                    </Col>
                </Row>
            </div>
        )

    };
}

export default SingleEncounterMonster;