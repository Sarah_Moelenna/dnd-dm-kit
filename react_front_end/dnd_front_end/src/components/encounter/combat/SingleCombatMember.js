import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_PUT } from '../.././shared/DNDRequests';

class SingleCombatMember extends Component {

    state = {
        deal_damage: 0,
        toggle_rename: false
    }

    has_condition = (condition) => {
        return this.props.member.conditions.includes(condition)
    }

    update_condition = (condition) => {
        var conditions = this.props.member.conditions
        if(this.has_condition(condition) == true){
            conditions = []
            for(var i = 0; i < this.props.member.conditions.length; i++){
                if(this.props.member.conditions[i] != condition){
                    conditions.push(this.props.member.conditions[i])
                }
            }
        } else {
            conditions.push(condition)
        }
        this.props.edit_member_function(this.props.member.id, {conditions: conditions})
    }

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    update_damage = (multiplier) => {
        var new_damage = this.props.member.damage_taken + (this.state.deal_damage * multiplier)

        if(this.props.member.type == 'ALLY'){
            var data={
                "damage_taken": new_damage
            }

            DND_PUT(
                '/ally/' + this.props.member.ally_id,
                data,
                null,
                null
            )
        }
        
        this.setState({ deal_damage: 0 }, this.props.edit_member_function(this.props.member.id, {damage_taken: new_damage}));
    }

    update_name = (e) => {
        var new_name = e.target.value
        this.setState({ deal_damage: 0 }, this.props.edit_member_function(this.props.member.id, {display_name: new_name}));
    }

    update_state = (new_status) => {
        var data = {
            state: new_status
        }
        if(new_status == 'DEAD'){
            data["x"] = null
            data["y"] = null
        }
        this.props.edit_member_function(this.props.member.id, data)
    }

    blinded_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Blinded</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A blinded creature can’t see and automatically fails any ability check that requires sight.</li>
                    <li>Attack Rolls against the creature have advantage, and the creature’s Attack Rolls have disadvantage.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    charmed_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Charmed</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A charmed creature can’t Attack the charmer or target the charmer with harmful Abilities or magical Effects.</li>
                    <li>The charmer has advantage on any ability check to interact socially with the creature.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    frightened_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Frightened</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A frightened creature has disadvantage on Ability Checks and Attack Rolls while the source of its fear is within Line of Sight.</li>
                    <li>The creature can’t willingly move closer to the source of its fear.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    deafened_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Defeaned</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A deafened creature can’t hear and automatically fails any ability check that requires hearing.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    grappled_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Grappled</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A grappled creature’s speed becomes 0, and it can’t benefit from any bonus to its speed.</li>
                    <li>The condition ends if the Grappler is incapacitated (see the condition).</li>
                    <li>The condition also ends if an Effect removes the grappled creature from the reach of the Grappler or Grappling Effect, such as when a creature is hurled away by the Thunderwave spell.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    invisible_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Invisible</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>An invisible creature is impossible to see without the aid of magic or a Special sense. For the Purpose of Hiding, the creature is heavily obscured. The creature’s location can be detected by any noise it makes or any tracks it leaves.</li>
                    <li>Attack Rolls against the creature have disadvantage, and the creature’s Attack Rolls have advantage.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    incapacitated_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Incapacitated</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>An incapacitated creature can’t take Actions or Reactions.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    paralyzed_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Paralyzed</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A paralyzed creature is incapacitated (see the condition) and can’t move or speak.</li>
                    <li>The creature automatically fails Strength and Dexterity Saving Throws.</li>
                    <li>Attack Rolls against the creature have advantage.</li>
                    <li>Any Attack that hits the creature is a critical hit if the attacker is within 5 feet of the creature.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    petrified_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Petrified</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A petrified creature is transformed, along with any nonmagical object it is wearing or carrying, into a solid inanimate substance (usually stone). Its weight increases by a factor of ten, and it ceases aging.</li>
                    <li>The creature is incapacitated (see the condition), can’t move or speak, and is unaware of its surroundings.</li>
                    <li>Attack Rolls against the creature have advantage.</li>
                    <li>The creature automatically fails Strength and Dexterity Saving Throws.</li>
                    <li>The creature has Resistance to all damage.</li>
                    <li>The creature is immune to poison and disease, although a poison or disease already in its system is suspended, not neutralized.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    poisoned_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Poisoned</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A poisoned creature has disadvantage on Attack Rolls and Ability Checks.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    prone_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Prone</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A prone creature’s only Movement option is to crawl, unless it stands up and thereby ends the condition.</li>
                    <li>The creature has disadvantage on Attack Rolls.</li>
                    <li>An Attack roll against the creature has advantage if the attacker is within 5 feet of the creature. Otherwise, the Attack roll has disadvantage.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    restrained_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Restrained</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A restrained creature’s speed becomes 0, and it can’t benefit from any bonus to its speed.</li>
                    <li>Attack Rolls against the creature have advantage, and the creature’s Attack Rolls have disadvantage.</li>
                    <li>The creature has disadvantage on Dexterity Saving Throws.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    stunned_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Stunned</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>A stunned creature is incapacitated (see the condition), can’t move, and can speak only falteringly.</li>
                    <li>The creature automatically fails Strength and Dexterity Saving Throws.</li>
                    <li>Attack Rolls against the creature have advantage.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    unconscious_popover = (
        <Popover id="popover-basic">
            <Popover.Title as="h3">Unconscious</Popover.Title>
            <Popover.Content>
                <ul>
                    <li>An unconscious creature is incapacitated (see the condition), can’t move or speak, and is unaware of its surroundings</li>
                    <li>The creature drops whatever it’s holding and falls prone.</li>
                    <li>The creature automatically fails Strength and Dexterity Saving Throws.</li>
                    <li>Attack Rolls against the creature have advantage.</li>
                    <li>Any Attack that hits the creature is a critical hit if the attacker is within 5 feet of the creature.</li>
                </ul>
            </Popover.Content>
        </Popover>
    );

    render(){
        return(
            <div className="focus-combat-member">
                {this.state.toggle_rename == false || this.props.member.display_name == undefined ?
                    <div>
                        {this.props.player_read == true ?
                            <h1>{this.props.member.display_name != undefined ? this.props.member.display_name : this.props.member.name}</h1>
                            :
                            <h1>
                                {this.props.member.display_name != undefined ? this.props.member.display_name : this.props.member.name} {this.props.member.display_name != undefined && "(" + this.props.member.name +")"} {this.props.member.display_name != undefined && <i className="fas fa-edit clickable-icon" onClick={() => {this.setState({toggle_rename: !this.state.toggle_rename})}}></i>}
                            </h1>
                        }
                    </div>
                    :
                    <div className="rename-combat-member">
                        <Form.Group controlId="formBasicName">
                            <span><Form.Control value={this.props.member.display_name} name="display_name" type="text" onChange={this.update_name}/></span>
                            <span><i className="fas fa-edit clickable-icon" onClick={() => {this.setState({toggle_rename: !this.state.toggle_rename})}}></i></span>
                        </Form.Group>
                    </div>
                }
                { this.props.member.delayed != true &&
                    <div>
                        <Row>
                            <Col xs={6} md={6}>
                                {this.props.player_read == true ?
                                    <p className='living-status'><b>Status</b>: {this.props.member.state == 'ALIVE'? 'Alive' : 'Dead'}</p>
                                    :
                                    <p className='living-status'><b>Status</b>: {this.props.member.state == 'ALIVE'? 'Alive' : 'Dead'}
                                        {this.props.member.state == 'ALIVE' ? 
                                        <i className="fas fa-toggle-on"  onClick={() => this.update_state('DEAD')}></i>
                                        :
                                        <i className="fas fa-toggle-off"  onClick={() => this.update_state('ALIVE')}></i>
                                        }
                                    </p>
                                }
                                <p><b>Initative</b>: {this.props.member.initiative}</p>
                                {this.props.member.type == 'ALLY' && 
                                    <p><b>Max Health</b>: {this.props.member.max_health}</p>
                                }
                                {(this.props.member.type == 'MONSTER' || this.props.member.type == 'CUSTOM' || this.props.member.type == 'ALLY') &&
                                    <div className='damage'>
                                        <p><b>Damage Taken</b>: {this.props.member.damage_taken}</p>
                                        {this.props.player_read != true &&
                                            <div className='controls'>
                                                <i className="fas fa-minus" onClick={() => this.update_damage(-1)}></i>
                                                <Form onSubmit={this.handleSubmit}>
                                                    <Form.Group controlId="formBasicDamage">
                                                        <Form.Control value={this.state.deal_damage} name="deal_damage" type="number" onChange={this.handleChange}/>
                                                    </Form.Group>
                                                </Form>
                                                <i className="fas fa-plus"  onClick={() => this.update_damage(1)}></i>
                                            </div>
                                        }
                                    </div>
                                }
                            </Col>
                            <Col xs={6} md={6}>
                                <div className="conditions">
                                    {/* https://roll20.net/compendium/dnd5e/Conditions#content */}
                                    <h2>Conditions:</h2>
                                    <OverlayTrigger placement="bottom" overlay={this.blinded_popover}>
                                        <span className={this.has_condition('BLINDED') ? 'active' : ''} onClick={() => this.update_condition("BLINDED")}>Blinded</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.charmed_popover}>
                                        <span className={this.has_condition('CHARMED') ? 'active' : ''} onClick={() => this.update_condition("CHARMED")}>Charmed</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.deafened_popover}>
                                        <span className={this.has_condition('DEAFENED') ? 'active' : ''} onClick={() => this.update_condition("DEAFENED")}>Deafened</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.frightened_popover}>
                                        <span className={this.has_condition('FRIGHTENED') ? 'active' : ''} onClick={() => this.update_condition("FRIGHTENED")}>Frightened</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.grappled_popover}>
                                        <span className={this.has_condition('GRAPPLED') ? 'active' : ''} onClick={() => this.update_condition("GRAPPLED")}>Grappled</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.incapacitated_popover}>
                                    <span className={this.has_condition('INCAPACITATED') ? 'active' : ''} onClick={() => this.update_condition("INCAPACITATED")}>Incapacitated</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.invisible_popover}>
                                        <span className={this.has_condition('INVISIBLE') ? 'active' : ''} onClick={() => this.update_condition("INVISIBLE")}>Invisible</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.paralyzed_popover}>
                                        <span className={this.has_condition('PARALYZED') ? 'active' : ''} onClick={() => this.update_condition("PARALYZED")}>Paralyzed</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.petrified_popover}>
                                        <span className={this.has_condition('PETRIFIED') ? 'active' : ''} onClick={() => this.update_condition("PETRIFIED")}>Petrified</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.poisoned_popover}>
                                        <span className={this.has_condition('POISONED') ? 'active' : ''} onClick={() => this.update_condition("POISONED")}>Poisoned</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.prone_popover}>
                                        <span className={this.has_condition('PRONE') ? 'active' : ''} onClick={() => this.update_condition("PRONE")}>Prone</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.restrained_popover}>
                                        <span className={this.has_condition('RESTRAINED') ? 'active' : ''} onClick={() => this.update_condition("RESTRAINED")}>Restrained</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.stunned_popover}>
                                        <span className={this.has_condition('STUNNED') ? 'active' : ''} onClick={() => this.update_condition("STUNNED")}>Stunned</span>
                                    </OverlayTrigger>
                                    <OverlayTrigger placement="bottom" overlay={this.unconscious_popover}>
                                        <span className={this.has_condition('UNCONSCIOUS') ? 'active' : ''} onClick={() => this.update_condition("UNCONSCIOUS")}>Unconscious</span>
                                    </OverlayTrigger>
                                </div>
                            </Col>
                        </Row>
                    </div>
                }
                { this.props.member.delayed == true &&
                    <div>
                        <p>This member has yet to enter the fray.</p>
                        <a onClick={() => {this.props.enter_combat(this.props.member.id)}} className="btn btn-primary">Enter Combat</a>
                    </div>
                }
            </div>

        )

    };
}

export default SingleCombatMember;