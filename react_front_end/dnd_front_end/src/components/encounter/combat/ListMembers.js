import React from 'react';
import SingleParticipant from './SingleParticipant'


const ListMembers = ({ members, editable_initiative, edit_member_function, set_focus_function, current_member, current_focus_member, monsters, player_read}) => {
    if(members === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }

    return (
        <div className={editable_initiative == true ? "combat-members-items preparing" : "combat-members-items running"}>
            {members.map((member) => (
                <div key={member.id} 
                    className={
                        (member.id == current_member ? "combat-members-item active" : (member.state == "ALIVE" ? (member.delayed == true ? "combat-members-item alive delayed" :"combat-members-item alive" ) : "combat-members-item dead"))
                        + (member.id == current_focus_member ? ' viewing' : '')
                    }
                >
                    <SingleParticipant member={member} editable_initiative={editable_initiative} edit_member_function={edit_member_function} set_focus_function={set_focus_function} monsters={monsters} player_read={player_read}/>
                </div>
            ))}
        </div>
    );
}

export default ListMembers;