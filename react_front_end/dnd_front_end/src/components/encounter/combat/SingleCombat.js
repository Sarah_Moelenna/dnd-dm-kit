import React, { Component } from 'react';
import ListMembers from './ListMembers'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SingleCombatMember from './SingleCombatMember'
import MonsterInfo from '../../monster/MonsterInfo'
import APIButton from '../../shared/APIButton';
import CustomRoller from '../../shared/CustomRoller';
import Collapsible from '../../shared/Collapsible';
import { DND_GET, DND_POST } from '../.././shared/DNDRequests';
import SingleBattlemap from '../.././battlemap/SingleBattlemap';
import { SocketContext } from '../../socket/Socket';
import { get_auth_token } from '../../shared/AuthToken';
import MonsterSelect from '../../monster/MonsterSelect';
import Form from 'react-bootstrap/Form'
import { v4 as uuidv4 } from 'uuid';

class SingleCombat extends Component {

    static contextType = SocketContext

    state = {
        monsters: {},
        focus_member: null,
        focus_monster: null,
        tab: "TRACKER",
        share: false,
        add_data: {}
    }

    constructor(props) {
        super(props);
        this.state = {
            monsters: {},
            focus_member: null,
            focus_monster: null,
            tab: "TRACKER",
            share: this.props.share,
            add_data: {}
        };
    }

    componentWillMount() {
        const socket = this.context
        socket.emit("get_current_combat_id");
    }

    componentDidMount() {
        this.refresh_monsters()
    }

    toggle_share_combat = () => {
        var new_share = !this.state.share
        const socket = this.context
        if(new_share == true){
            socket.emit("combat_update", this.props.combat.id, get_auth_token());
        } else {
            socket.emit("combat_clear");
        }
        this.setState({share: new_share})
        this.props.clear_share()
    }

    set_tab = (tab) => {
        this.setState({tab: tab})
    }
  
    refresh_monsters = () => {
        DND_GET(
            '/combat/' + this.props.combat.id + '/monsters',
            (jsondata) => {
                this.setState({ monsters: jsondata})
            },
            null
        )
    };

    generate_monster_initative = () => {
        var new_members = []
        for (var i = 0; i < this.props.combat.state.members.length; i++){
            var member = this.props.combat.state.members[i]
            if (member.type == 'CHARACTER' || member.type == 'CUSTOM'){
                new_members.push(member)
            }
            else if (member.type == 'MONSTER'){
                if (member.initiative == null){
                    var monster = this.state.monsters[member.object_id]
                    var dex_mod = Math.floor(monster.dexterity / 2) - 5
                    var d20_roll = Math.floor((Math.random() * 20) + 1);
                    member.initiative = d20_roll + dex_mod
                }
                new_members.push(member)
            }
            else if (member.type == 'ALLY'){
                if (member.initiative == null){
                    var monster = this.state.monsters[member.object_id]
                    var dex_mod = Math.floor(monster.dexterity / 2) - 5
                    var d20_roll = Math.floor((Math.random() * 20) + 1);
                    member.initiative = d20_roll + dex_mod
                }
                new_members.push(member)
            }
        }
        var new_combat = this.props.combat
        new_combat.state.members = new_members
        this.props.update_combat_function(new_combat, this.state.share)
    }

    edit_member = (id, attribute, value) => {
        var new_members = []
        for (var i = 0; i < this.props.combat.state.members.length; i++){
            var member = this.props.combat.state.members[i]
            if (member.id != id){
                new_members.push(member)
            } else{
                member[attribute] = value
                if(this.state.focus_member != null && this.state.focus_member.id == id){
                    this.setState({ focus_member: member})
                }
                new_members.push(member)
            }
        }
        var new_combat = this.props.combat
        new_combat.state.members = new_members
        this.props.update_combat_function(new_combat, this.state.share)
    }

    edit_member_attributes = (id, attribute_dict) => {
        var new_members = []
        for (var i = 0; i < this.props.combat.state.members.length; i++){
            var member = this.props.combat.state.members[i]
            if (member.id != id){
                new_members.push(member)
            } else{
                var keys = Object.keys(attribute_dict)
                for(var j = 0; j < keys.length; j++){
                    member[keys[j]] = attribute_dict[keys[j]]
                }
                if(this.state.focus_member != null && this.state.focus_member.id == id){
                    this.setState({ focus_member: member})
                }
                new_members.push(member)
            }
        }
        var new_combat = this.props.combat
        new_combat.state.members = new_members
        this.props.update_combat_function(new_combat, this.state.share)
    }

    enter_combat = (id) => {
        var new_members = []
        var member_display_name = ''
        var member_initiative = 0
        for (var i = 0; i < this.props.combat.state.members.length; i++){
            var member = this.props.combat.state.members[i]
            if (member.id != id){
                new_members.push(member)
            } else{
                member["delayed"] = 0
                member_display_name = member.display_name
                member_initiative = member.initiative
                if(this.state.focus_member != null && this.state.focus_member.id == id){
                    this.setState({ focus_member: member})
                }
                new_members.push(member)
            }
        }
        var new_combat = this.props.combat
        new_combat.state.members = new_members
        this.props.update_combat_function(new_combat, this.state.share)

        /*var data = {
            "text": 'COMBAT UPDATE\n\n' + member_display_name + " has entered combat with initative " + member_initiative,
            "location": "ROLLING"
        }
        var body = {
            "command_code": "CMD_SHARE_TXT",
            "data": data
        }

        DND_POST(
            '/commands',
            body,
            null,
            null
        )*/
    }

    set_member_focus = (id) => {
        var focus_member = null
        var focus_monster = null
        for (var i = 0; i < this.props.combat.state.members.length; i++){
            var member = this.props.combat.state.members[i]
            if (member.id == id){
                focus_member = member
            }
        }
        if (focus_member != null){
            if (focus_member.type == 'MONSTER' || focus_member.type == 'ALLY'){
                focus_monster = this.state.monsters[focus_member.object_id]
            }
        }
        this.setState({ focus_member: focus_member, focus_monster: focus_monster})
    }

    is_ready_to_start = () => {
        for (var i = 0; i < this.props.combat.state.members.length; i++){
            var member = this.props.combat.state.members[i]
            if (member.initiative == null){
                return false
            }
        }
        if (this.props.combat.state.combat_state != "ROLLING_INITIATIVE"){
            return false
        }
        return true
    }

    start_combat = () => {
        var new_combat = this.props.combat
        new_combat.state.combat_state = "IN_PROGRESS"
        new_combat.state.current_turn = 0
        this.props.update_combat_function(new_combat, this.state.share)
    }

    get_sorted_members = (player_read) => {
        var final_members = []
        var intiatives = []
        var sorting_object = {}
        for (var i = 0; i < this.props.combat.state.members.length; i++){
            var member = this.props.combat.state.members[i]
            if(player_read == true && (member.state == "DEAD" || member.delayed == true || member.delayed == 1)){
                continue;
            }
            var key = 0
            if (member.initiative != null){
                key = parseInt(member.initiative)
            }
            if (intiatives.includes(key) == false){
                intiatives.push(key)
            }
            if (sorting_object[key] == undefined){
                sorting_object[key] = []
            }
            sorting_object[key].push(member)
        }
        
        intiatives.sort((a, b) => b - a);

        for(var i = 0; i < intiatives.length; i++){
            var members = sorting_object[intiatives[i]]
            members.sort((a, b) => (a.name > b.name) ? 1 : -1)
            final_members = final_members.concat(members)
        }
        return final_members
    }

    get_filtered_members = (player_read) => {
        var final_members = []
        for (var i = 0; i < this.props.combat.state.members.length; i++){
            var member = this.props.combat.state.members[i]
            if(player_read == true && (member.state == "DEAD" || member.delayed == true || member.delayed == 1)){
                continue;
            }
            final_members.push(member)
        }
        return final_members
    }

    allow_initative_editing = () => {
        return this.props.combat.state.combat_state == "ROLLING_INITIATIVE"
    }

    is_running = () => {
        return this.props.combat.state.combat_state != "ROLLING_INITIATIVE"
    }

    get_current_turn_member = () => {
        if(this.props.combat.state.current_turn != null){
            var members = this.get_sorted_members()
            return members[this.props.combat.state.current_turn].id
        }
        return null;
    }

    focus_current_member = () => {
        if(this.props.combat.state.current_turn != null){
            var members = this.get_sorted_members()
            
            this.set_member_focus(members[this.props.combat.state.current_turn].id)
        }
    }

    add_monster = (id, name, dex, xp, image) => {
        var new_add_data = this.state.add_data
        new_add_data.monster = {
            id: id,
            name: name,
            dex: dex,
            xp: xp,
            image: image
        }
        this.setState({add_data: new_add_data})
    }

    set_add_type = (type) => {
        var new_add_data = this.state.add_data
        new_add_data.type = type
        this.setState({add_data: new_add_data})
    }

    set_add_display_name = (e) => {
        var name = e.target.value
        var new_add_data = this.state.add_data
        new_add_data.display_name = name
        this.setState({add_data: new_add_data})
    }

    set_add_initiative = (e) => {
        var new_add_data = this.state.add_data
        new_add_data.initiative = e.target.value
        this.setState({add_data: new_add_data})
    }

    add_roll_initiative = () => {
        var new_add_data = this.state.add_data
        var dex_mod = Math.floor(new_add_data.monster.dex / 2) - 5
        var d20_roll = Math.floor((Math.random() * 20) + 1);
        new_add_data.initiative = d20_roll + dex_mod
        this.setState({add_data: new_add_data})
    }

    add_participant = () => {
        var participant = null
        var new_combat = this.props.combat
        if(this.state.add_data.type == "ALLY"){
            participant = {
                "type": "ALLY",
                "id": uuidv4(),
                "object_id": this.state.add_data.monster.id,
                "name": this.state.add_data.monster.name,
                "display_name": this.state.add_data.display_name,
                "conditions": [],
                "state": "ALIVE",
                "image": this.state.add_data.monster.image,
                "initiative": this.state.add_data.initiative,
                "damage_taken": 0,
                "x": null,
                "y": null
            }
        } else if(this.state.add_data.type == "MONSTER"){
            participant = {
                "type": "MONSTER",
                "id": uuidv4(),
                "object_id": this.state.add_data.monster.id,
                "name": this.state.add_data.monster.name,
                "display_name": this.state.add_data.display_name,
                "conditions": [],
                "state": "ALIVE",
                "image": this.state.add_data.monster.image,
                "initiative": this.state.add_data.initiative,
                "damage_taken": 0,
                "grouping_type": "SINGLE",
                "delayed": 0,
                "x": null,
                "y": null
            }
            new_combat.state.xp.total_xp = new_combat.state.xp.total_xp + this.state.add_data.monster.xp
            new_combat.state.xp.modified_xp = new_combat.state.xp.modified_xp + (this.state.add_data.monster.xp * new_combat.state.xp.modifier)
            new_combat.state.xp.monster_count = new_combat.state.xp.monster_count + 1
        } else if(this.state.add_data.type == "CUSTOM"){
            participant = {
                "type": "CUSTOM",
                "id": uuidv4(),
                "name": this.state.add_data.display_name,
                "display_name": this.state.add_data.display_name,
                "conditions": [],
                "state": "ALIVE",
                "initiative": this.state.add_data.initiative,
                "damage_taken": 0,
                "delayed": 0,
                "x": null,
                "y": null
            }
        }

        if(participant){
            new_combat.state.members.push(participant)
            this.props.update_combat_function(new_combat, this.state.share, this.refresh_monsters)
            this.setState({add_data: {}, tab: "TRACKER"})
        }
    }

    clear_add = () => {
        this.setState({add_data: {}})
    }

    next_turn = () => {
        if(this.props.combat.state.current_turn != null){
            var members = this.get_sorted_members()
            
            var next_turn = null
            for(var i = this.props.combat.state.current_turn + 1; i < members.length && next_turn == null; i++){
                if(members[i].state == 'ALIVE'){
                    next_turn = i
                }
            }

            if (next_turn == null){
                for(var i = 0; i < members.length && next_turn == null; i++){
                    if(members[i].state == 'ALIVE'){
                        next_turn = i
                    }
                }
            }

            if (next_turn != null){
                var new_combat = this.props.combat
                new_combat.state.current_turn = next_turn
                this.props.update_combat_function(new_combat, this.state.share)
                this.focus_current_member()
                return
            }
        }

        var new_combat = this.props.combat
        new_combat.state.current_turn = 0
        this.props.update_combat_function(new_combat, this.state.share)
    }

    create_audio_command = (id, play_type) => {
        var data = {
            "music_play_type": play_type,
            "audio_id": id
        }
        return {
            "command_code": "CMD_PLAY",
            "data": data
        }
    }

    create_playlist_command = (id, play_type, playlist_type) => {
        var data = {
            "music_play_type": play_type,
            "playlist_id": id,
            "type": playlist_type
        }
        return {
            "command_code": "CMD_PLAYLIST",
            "data": data
        }
    }

    /*post_combat_to_discord = () => {
        var members = this.get_sorted_members()
        var text = 'COMBAT UPDATE\n\n'
        for(var i = 0; i < members.length; i++){
            var member = members[i]
            var new_text = member.display_name != undefined ? member.display_name.replace(' - ', ' ') : member.name.replace(' - ', ' ')
            if(member.state == "ALIVE" && member.delayed != true){
                if (member.type == 'MONSTER' || member.type == 'CUSTOM'){
                    if (member.damage_taken > 0){
                        new_text = new_text + '  :  -' + member.damage_taken + ' HP'
                    }
                }
                if (member.type == 'ALLY'){
                    if (member.damage_taken > 0){
                        new_text = new_text + '  :  ' + member.damage_taken + '/' + member.max_health + ' HP'
                    }
                }
                if (member.conditions.length > 0){
                    new_text = new_text + '  :  ' + member.conditions.join(' ').toLowerCase()
                }
                new_text = new_text + "\n"
                    text = text + new_text
            }
        }

        var data = {
            "text": text,
            "location": "ROLLING"
        }
        var body = {
            "command_code": "CMD_SHARE_TXT",
            "data": data
        }

        DND_POST(
            '/commands',
            body,
            null,
            null
        )
    }*/
  
    render(){

        var ready_to_start = this.is_ready_to_start()
        this.get_sorted_members(this.props.player_read)

        if(this.props.player_read == true){
            return (
                <Row>
                    <Col xs={3} md={3}>
                        <ListMembers 
                            members={this.is_running() == false ? this.get_filtered_members(this.props.player_read) : this.get_sorted_members(this.props.player_read)}
                            refresh_function={() => {}}
                            editable_initiative={this.allow_initative_editing()}
                            edit_member_function={() => {}}
                            set_focus_function={this.set_member_focus}
                            current_member={this.get_current_turn_member()}
                            current_focus_member={this.state.focus_member != null ? this.state.focus_member.id : null}
                            monsters={this.state.monsters}
                            player_read={true}
                        />
                    </Col>
                    <Col xs={9} md={9}>
                        { this.props.combat.state.battlemap != undefined &&
                            <div className="battlemap">
                                <SingleBattlemap battlemap={this.props.combat.state.battlemap} objects={this.props.combat.state.members} update_battlemap_object={this.props.set_member_location_function} player_read={true}/>
                            </div>
                        }
                        <div className='single-combat-member-focus'>
                            <Row>
                                <Col xs={10} md={10}>
                                    {this.state.focus_member != null &&
                                        <SingleCombatMember member={this.state.focus_member} edit_member_function={() => {}} enter_combat={() => {}} player_read={true}/>
                                    }
                                </Col>
                                <Col xs={2} md={2}>
                                    {this.state.focus_member != null &&
                                        <img src={this.state.focus_member.image} />
                                    }
                                </Col>
                            </Row>
                            {this.state.focus_monster != null && this.state.focus_member.type == "ALLY" &&
                                <div className="monster">
                                    <hr/>
                                    <h2>Monster Information</h2>
                                    <MonsterInfo
                                        monster={this.state.focus_monster}
                                        show_roll_buttons={false}
                                        show_more_info={true}
                                        roll_dice_function={this.props.roll_dice_function}
                                        override_name={this.state.focus_member.display_name}
                                        override_roll_type={this.state.focus_member.type == "ALLY" ? "ALLY_ROLL" : null}
                                    />
                                </div>
                            }
                        </div>
                    </Col>
                </Row>
            )
        }

        return(
            <div>
                <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_combat_fuction}></i> {this.props.combat.name}
                    { this.props.combat.state.song_id != undefined && this.props.combat.state.song_id != null &&
                    <APIButton data={this.create_audio_command(this.props.combat.state.song_id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton>
                    }
                    { this.props.combat.state.playlist_id != undefined && this.props.combat.state.playlist_id != null &&
                    <APIButton data={this.create_playlist_command(this.props.combat.state.playlist_id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton>
                    }
                </h2>
                <div className="tabs">
                    <div className={"tab first-tab" + (this.state.tab == "TRACKER" ? " active" : "")} onClick={() => this.set_tab("TRACKER")}>
                        <p>Combat Tracker</p>
                    </div>
                    { this.props.combat.state.battlemap != undefined &&
                        <div className={"tab" + (this.state.tab == "BATTLEMAP" ? " active" : "")} onClick={() => this.set_tab("BATTLEMAP")}>
                            <p>Battlemap</p>
                        </div>
                    }
                    <div className={"tab last-tab" + (this.state.tab == "MODIFY" ? " active" : "")} onClick={() => this.set_tab("MODIFY")}>
                        <p>Modify Combat</p>
                    </div>
                </div>
                { this.state.tab == "TRACKER" &&
                    <Row>
                        <Col xs={5} md={5}>
                            <div className="combat-controls">
                                {ready_to_start == true &&
                                    <div className="combat-button">
                                        <div><a className="btn btn-primary" onClick={() => this.start_combat()}>Begin Combat</a></div>
                                    </div>
                                }
                                {this.allow_initative_editing() == true && ready_to_start == false &&
                                    <div className="combat-button">
                                        <div><a className="btn btn-primary" onClick={() => this.generate_monster_initative()}>Roll Initative for Monsters</a></div>
                                    </div>
                                }
                                {this.is_running() == true &&
                                    <div className="combat-button">
                                        <div><a className="btn btn-primary" onClick={() => this.focus_current_member()}>Focus Current Member</a></div>
                                    </div>
                                }
                                {this.is_running() == true &&
                                    <div className="combat-button">
                                        <div><a className="btn btn-primary" onClick={() => this.next_turn()}>Next Turn</a></div>
                                    </div>
                                }
                            </div>
                            <ListMembers 
                                members={this.is_running() == false ? this.props.combat.state.members : this.get_sorted_members()}
                                refresh_function={this.props.refresh_function}
                                editable_initiative={this.allow_initative_editing()}
                                edit_member_function={this.edit_member}
                                set_focus_function={this.set_member_focus}
                                current_member={this.get_current_turn_member()}
                                current_focus_member={this.state.focus_member != null ? this.state.focus_member.id : null}
                                monsters={this.state.monsters}
                            />
                            <div className={"combat-share" + (this.state.share == true ? " sharing" : "")} onClick={() => this.toggle_share_combat()}>
                                {this.state.share == true ?
                                    "Sharing"
                                    :
                                    "Click to Share"
                                }
                            </div>
                            <p><b>Total XP -</b> {this.props.combat.state.xp.total_xp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                            <p><b>Monster Count Modifier -</b> x{this.props.combat.state.xp.modifier}</p>
                            <p><b>Modified Total XP -</b> {this.props.combat.state.xp.modified_xp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                        </Col>
                        <Col xs={7} md={7}>
                            <div className='single-combat-member-focus'>
                                <Row>
                                    <Col xs={10} md={10}>
                                        {this.state.focus_member != null &&
                                            <SingleCombatMember member={this.state.focus_member} edit_member_function={this.edit_member_attributes} enter_combat={this.enter_combat}/>
                                        }
                                    </Col>
                                    <Col xs={2} md={2}>
                                        {this.state.focus_member != null &&
                                            <img src={this.state.focus_member.image} />
                                        }
                                    </Col>
                                </Row>
                                {this.state.focus_member != null &&
                                    <Collapsible contents={<CustomRoller roll_dice_function={this.props.roll_dice_function} type="MONSTER_ROLL" identity={this.state.focus_member.display_name} />} title="Roll Dice"/>
                                }
                                {this.state.focus_monster != null &&
                                    <div className="monster">
                                        <hr/>
                                        <h2>Monster Information</h2>
                                        <MonsterInfo
                                            monster={this.state.focus_monster}
                                            show_roll_buttons={true}
                                            show_more_info={true}
                                            roll_dice_function={this.props.roll_dice_function}
                                            override_name={this.state.focus_member.display_name}
                                            override_roll_type={this.state.focus_member.type == "ALLY" ? "ALLY_ROLL" : null}
                                        />
                                    </div>
                                }
                            </div>
                        </Col>
                    </Row>
                }
                { this.state.tab == "BATTLEMAP" &&
                    <div className="battlemap">
                        <SingleBattlemap battlemap={this.props.combat.state.battlemap} objects={this.props.combat.state.members} update_battlemap_object={this.edit_member_attributes} />
                    </div>
                }
                { this.state.tab == "MODIFY" &&
                    <div className="modify">
                        {!this.state.add_data.type &&
                            <div>
                                <p>Select a participant type to add.</p>
                                <button className="btn btn-primary" onClick={() => this.set_add_type("MONSTER")}>Add Enemy</button>
                                <button className="btn btn-primary" onClick={() => this.set_add_type("ALLY")}>Add Ally</button>
                                <button className="btn btn-primary" onClick={() => this.set_add_type("CUSTOM")}>Add Custom</button>
                            </div>
                        }
                        {this.state.add_data.type == "MONSTER" &&
                            <div>
                                <h2>Add Enemy</h2>
                                <MonsterSelect
                                    select_function={this.add_monster}
                                    override_button="Select Enemy Monster"
                                    disabled_ids={this.state.add_data.monster ? [this.state.add_data.monster.id] : []}
                                />
                                {this.state.add_data.monster &&
                                    <Row>
                                        <Col xs={3}>
                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Display Name</Form.Label>
                                                <Form.Control value={this.state.add_data.monster.name} disabled="disabled" name="name" type="text" placeholder="Name"/>
                                            </Form.Group>
                                        </Col>
                                        <Col xs={3}>
                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Display Name</Form.Label>
                                                <Form.Control value={this.state.add_data.display_name} name="name" type="text" placeholder="Name" onChange={this.set_add_display_name}/>
                                            </Form.Group>
                                        </Col>
                                        <Col xs={3}>
                                            <Form.Group controlId="formInitiative">
                                                <Form.Label>Initiative</Form.Label>
                                                <Form.Control value={this.state.add_data.initiative} name="name" type="number" onChange={this.set_add_initiative}/>
                                            </Form.Group>
                                        </Col>
                                        <Col xs={3}>
                                            <button className="btn btn-primary" onClick={() => this.add_roll_initiative()}>Roll Initiative</button>
                                        </Col>
                                        <Col xs={12}>
                                            {(this.state.add_data.initiative && this.state.add_data.display_name && this.state.add_data.monster) ?
                                                <button className="btn btn-primary" onClick={() => this.add_participant()}>Add</button>
                                                :
                                                <button className="btn btn-primary" disabled="disabled">Add</button>
                                            }
                                        </Col>
                                    </Row>
                                }
                            </div>
                        }
                        {this.state.add_data.type == "ALLY" &&
                            <div>
                                <h2>Add Ally</h2>
                                <MonsterSelect
                                    select_function={this.add_monster}
                                    override_button="Select Ally Monster"
                                    disabled_ids={this.state.add_data.monster ? [this.state.add_data.monster.id] : []}
                                />
                                {this.state.add_data.monster &&
                                    <Row>
                                        <Col xs={3}>
                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Display Name</Form.Label>
                                                <Form.Control value={this.state.add_data.monster.name} disabled="disabled" name="name" type="text" placeholder="Name"/>
                                            </Form.Group>
                                        </Col>
                                        <Col xs={3}>
                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Display Name</Form.Label>
                                                <Form.Control value={this.state.add_data.display_name} name="name" type="text" placeholder="Name" onChange={this.set_add_display_name}/>
                                            </Form.Group>
                                        </Col>
                                        <Col xs={3}>
                                            <Form.Group controlId="formInitiative">
                                                <Form.Label>Initiative</Form.Label>
                                                <Form.Control value={this.state.add_data.initiative} name="name" type="number" onChange={this.set_add_initiative}/>
                                            </Form.Group>
                                        </Col>
                                        <Col xs={3}>
                                            <button className="btn btn-primary" onClick={() => this.add_roll_initiative()}>Roll Initiative</button>
                                        </Col>
                                        <Col xs={12}>
                                            {(this.state.add_data.initiative && this.state.add_data.display_name && this.state.add_data.monster) ?
                                                <button className="btn btn-primary" onClick={() => this.add_participant()}>Add</button>
                                                :
                                                <button className="btn btn-primary" disabled="disabled">Add</button>
                                            }
                                        </Col>
                                    </Row>
                                }
                            </div>
                        }
                        {this.state.add_data.type == "CUSTOM" &&
                            <div>
                                <h2>Add Custom</h2>
                                    <Row>
                                        <Col xs={6}>
                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Display Name</Form.Label>
                                                <Form.Control value={this.state.add_data.display_name} name="name" type="text" placeholder="Name" onChange={this.set_add_display_name}/>
                                            </Form.Group>
                                        </Col>
                                        <Col xs={6}>
                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Initiative</Form.Label>
                                                <Form.Control value={this.state.add_data.initiative} name="name" type="number" onChange={this.set_add_initiative}/>
                                            </Form.Group>
                                        </Col>
                                        <Col xs={12}>
                                            {(this.state.add_data.initiative && this.state.add_data.display_name) ?
                                                <button className="btn btn-primary" onClick={() => this.add_participant()}>Add</button>
                                                :
                                                <button className="btn btn-primary" disabled="disabled">Add</button>
                                            }
                                        </Col>
                                    </Row>
                            </div>
                        }
                        <hr/>
                        <button className="btn btn-primary" onClick={() => this.clear_add()}>Clear</button>
                    </div>
                }
            </div>
        )

    };
}

export default SingleCombat;