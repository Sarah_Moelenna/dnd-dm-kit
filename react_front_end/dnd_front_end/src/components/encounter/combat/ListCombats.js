import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '../.././shared/DNDRequests';
import DeleteConfirmationButton from '../../shared/DeleteConfirmationButton';

const ListCombats = ({ combats, refresh_function, view_combat_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/combat/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(combats === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="combat-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={4} md={4} className="cenetered-column">
                        <p className="card-text"><b>Last Played</b></p>
                    </Col>
                    <Col xs={5} md={5}>
                        <p className="card-text"><b>Controls</b></p>
                    </Col>
                </Row>
                <hr/>
            </div>
            {combats.map((combat) => (
                <div key={combat.id} className="combat-item">
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{combat.name}</p>
                        </Col>
                        <Col xs={4} md={4} className="cenetered-column">
                        <p className="card-text">{combat.updated_at}</p>
                        </Col>
                        <Col xs={5} md={5}>
                            <a className="btn btn-primary" onClick={() => view_combat_function(combat.id)}>Resume Combat</a>
                            <DeleteConfirmationButton id={combat.id} name="Combat" delete_function={delete_page_function} />
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListCombats;