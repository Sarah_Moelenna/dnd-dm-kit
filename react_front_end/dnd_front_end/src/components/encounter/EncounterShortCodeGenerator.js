import React, { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Form from 'react-bootstrap/Form'

const EncounterShortCodeGenerator = ({encounter}) => {

    const [isVisible, setIsVisible] = useState(false);

    const toggle = () => setIsVisible(!isVisible);

    function get_shortcode(){
        return "<ENCOUNTER name=" + encounter.name.replaceAll(" ", "%20") + " summary=" + encounter.summary.replaceAll(" ", "%20") + " id=" + encounter.id + ">"
    }

    var popover = <Popover className="encounter-shortcode-popover">
        <Popover.Title>Encounter Shortcode</Popover.Title>
        <Popover.Content className="content">
            <Form.Control value={get_shortcode()} name="name" type="text" disabled="disabled"/>
            <p className="copy" onClick={() => {navigator.clipboard.writeText(get_shortcode()); toggle();}}>Copy <i className="fas fa-copy"></i></p>
        </Popover.Content>
    </Popover>

    return (
        <div className="encounter-shortcode">
            <div>
                <OverlayTrigger show={isVisible} trigger={['click']} placement="right" overlay={popover} onToggle={toggle}>
                    <i className="fas fa-code"></i>
                </OverlayTrigger>
            </div>
        </div>
    );
}

export default EncounterShortCodeGenerator;