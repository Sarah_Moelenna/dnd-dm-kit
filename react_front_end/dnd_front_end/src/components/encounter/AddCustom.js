import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

class AddCustom extends Component {

    state = {
        name: null,
        toggle: false
    }

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    add_participant = () => {
        if(this.state.name == null){
            return;
        }
        this.props.add_custom_function(this.state.name)
        this.setState({ name:null, toggle: false });
    }

    render(){

        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>{this.props.override_button != undefined ? this.props.override_button : "Add Custom Particpant"}</Button>
                }
                <Modal show={this.state.toggle} size="md" className="monster-select-modal select-modal">
                        <Modal.Header>Custom Particpant</Modal.Header>  
                        <Modal.Body>
                            <Form onSubmit={this.handleSubmit} className="add-participant">
                                <Form.Group controlId="formBasicName">
                                    <Form.Label>Participant Name</Form.Label>
                                    <Form.Control value={this.state.name} name="name" type="text" placeholder="Name" onChange={this.handleChange}/>
                                </Form.Group>
                            </Form>
                            <a className="btn btn-primary" onClick={() => this.add_participant()}>Add Participant</a>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>

                
                
            </div>
        );
    };
}

export default AddCustom;