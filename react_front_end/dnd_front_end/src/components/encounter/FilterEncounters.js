import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PlayerContext } from '../shared/PlayerContext';

class FilterEncounters extends Component {

    static contextType = PlayerContext

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        const player = this.context

        return(
            <Form onSubmit={this.handleSubmit} className="encounter_filters">
                <Row>
                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Encounter Name</Form.Label>
                            <Form.Control value={this.props.filters["name"]} name="name" type="text" placeholder="Search Encounter Names" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicLevel">
                            <Form.Label>Campaign</Form.Label>
                            <Form.Control name="campaign_id" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    {player.campaigns.map((campaign) => (
                                        <option key={campaign.id} selected={this.props.filters["campaign_id"] == campaign.id} value={campaign.id}>{campaign.name}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <a className="btn btn-primary" onClick={() => this.props.clear_filter_function()}>Clear Filters</a>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default FilterEncounters;