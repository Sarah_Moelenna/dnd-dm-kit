import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from '.././shared/Spinner'
import { DND_POST } from '.././shared/DNDRequests';

class DownloadForm extends Component {

    state = {
        url: null,
        name: null,
        processing: false,
        audio_type: null
    }

    handleSubmit = (event) => {
        
        if( this.state.url == null || this.state.name == null){
            event.preventDefault();
            return
        }

        var data = {
            url: this.state.url,
            display_name: this.state.name,
            audio_type: this.state.audio_type
        }
        this.setState({ processing: true })

        DND_POST(
            '/audio',
            data,
            (response) => {
                this.props.refresh_function()
                this.setState({ url: null, name: null, audio_type: null, processing: false })
            },
            null
        )
        
        event.preventDefault();
      
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };


    render(){
        if(this.state.processing === true) {
            return (
                <Spinner/>
            )
        }

        return(
            <Form onSubmit={this.handleSubmit} className="yt-downloader">
                <Row>
                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicURL">
                            <Form.Label>Youtube URL</Form.Label>
                            <Form.Control required value={this.state.url} name="url" type="text" placeholder="Enter Url" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Display Name</Form.Label>
                            <Form.Control required value={this.state.name} name="name" type="text" placeholder="Enter Display Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Audio Type</Form.Label>
                            <Form.Control required name="audio_type" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    <option value="AMBIENCE" selected={this.state.audio_type == "AMBIENCE"}>Ambient Sound</option>
                                    <option value="MUSIC" selected={this.state.audio_type == "MUSIC"}>Music</option>
                                    <option value="EFFECT" selected={this.state.audio_type == "EFFECT"}>Sound Effect</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default DownloadForm;