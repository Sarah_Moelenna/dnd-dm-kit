import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class FilterMusic extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Row className="music_filters">
                <Col xs={2} md={2}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control value={this.props.filters["name"]  ? this.props.filters["name"] : ''} name="name" type="text" placeholder="Search Music Names" onChange={this.handleChange}/>
                    </Form.Group>
                 </Col>

                {this.props.hide_type_filter != true &&
                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Audio Type</Form.Label>
                            <Form.Control name="audio_type" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    <option value="AMBIENCE" selected={this.props.filters["audio_type"] == "AMBIENCE"}>Ambient Sound</option>
                                    <option value="MUSIC" selected={this.props.filters["audio_type"] == "MUSIC"}>Music</option>
                                    <option value="EFFECT" selected={this.props.filters["audio_type"] == "EFFECT"}>Sound Effect</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                }

                <Col xs={2} md={2}>
                    <a className="btn btn-primary" onClick={() => this.props.clear_filter_function()}>Clear Filters</a>
                </Col>
            </Row>
        );
    };
}

export default FilterMusic;