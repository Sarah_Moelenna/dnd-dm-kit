import React, { Component } from 'react';
import APIButton from '.././shared/APIButton';
import { DND_DELETE } from '.././shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import Card from 'react-bootstrap/esm/Card';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import { DND_PUT } from '.././shared/DNDRequests';
import { SoundEffectButton } from './SoundEffectButton';
import MusicShortCodeGenerator from './MusicShortCodeGenerator'

const create_audio_command = (id, play_type) => {
    var data = {
        "music_play_type": play_type,
        "audio_id": id
    }
    return {
        "command_code": "CMD_PLAY",
        "data": data
    }
}

class AudioItem extends Component {

    state = {
        edit: false,
        name: null,
        type: null
    }

    delete_audio_function = () => {
        DND_DELETE(
            '/audio/' + this.props.audio_item.id,
            null,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    }

    edit_audio = () => {
        var data = {
            "display_name": this.state.name ? this.state.name : this.props.audio_item.name,
            "audio_type": this.state.type ? this.state.type : this.props.audio_item.audio_type
        }

        DND_PUT(
            '/audio/' + this.props.audio_item.id,
            data,
            (response) => {
                this.setState({edit: false, name: null, type: null})
                this.props.refresh_function()
            },
            null
        )
          
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    render(){
        if (this.state.edit == false){
            return (
                <Card>
                    {this.props.audio_item.audio_type != "EFFECT" &&
                    <Card.Title>{this.props.audio_item.name} <APIButton data={create_audio_command(this.props.audio_item.id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton></Card.Title>
                    }
                    {this.props.audio_item.audio_type == "EFFECT" &&
                    <Card.Title>{this.props.audio_item.name} <SoundEffectButton file_name={this.props.audio_item.file_name} icon="fas fa-play"/></Card.Title>
                    }
                    {this.props.audio_item.audio_type != "EFFECT" &&
                    <Card.Title className='audio-status'>{this.props.audio_item.status_display} <MusicShortCodeGenerator audio_item={this.props.audio_item}/></Card.Title>
                    }
                    {this.props.audio_item.audio_type == "EFFECT" &&
                    <Card.Title className='audio-status'>{this.props.audio_item.status_display}</Card.Title>
                    }
                    <Card.Body>
                        <div className='icon'>
                            { this.props.audio_item.audio_type == 'MUSIC' &&
                                <i class="fas fa-music"></i>
                            }
                            { this.props.audio_item.audio_type == 'AMBIENCE' &&
                                <i class="fas fa-cloud-rain"></i>
                            }
                            { this.props.audio_item.audio_type == 'EFFECT' &&
                                <i class="fas fa-volume-up"></i>
                            }
                        </div>
                        <Button variant="primary" type="submit" onClick={() => {this.setState({edit: true})}}>Edit</Button>
                        <DeleteConfirmationButton id={this.props.audio_item.id} override_button="Delete" name="Audio Item" delete_function={this.delete_audio_function} />
                    </Card.Body>
                </Card>
            );
        } else {
            let edit_audio_type = this.state.type ? this.state.type : this.props.audio_item.audio_type
            return (
                <Card>
                    <Card.Title>{this.props.audio_item.name} <APIButton data={create_audio_command(this.props.audio_item.id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton></Card.Title>
                    
                    <Card.Body>

                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control required value={this.state.name ? this.state.name : this.props.audio_item.name} name="name" type="text" onChange={this.handleChange}/>
                        </Form.Group>

                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Audio Type</Form.Label>
                            <Form.Control required name="type" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    <option value="AMBIENCE" selected={edit_audio_type == "AMBIENCE"}>Ambient Sound</option>
                                    <option value="MUSIC" selected={edit_audio_type == "MUSIC"}>Music</option>
                                    <option value="EFFECT" selected={edit_audio_type == "EFFECT"}>Sound Effect</option>
                            </Form.Control>
                        </Form.Group>

                        <Button variant="primary" type="submit" onClick={() => {this.setState({edit: false, name: null, type: null})}}>Cancel</Button>
                        <Button variant="primary" type="submit" onClick={() => {this.edit_audio()}}>Save</Button>
                    </Card.Body>
                </Card>
            );
        }
    }
}

const ListAudioItem = ({ audio_items, refresh_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/audio/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(audio_items === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }

    return (
        <div className="audio-items">
            {audio_items.map((audio_item) => (
                <div key={audio_item.id} className="audio-item">
                    <AudioItem audio_item={audio_item} refresh_function={refresh_function} />
                </div>
            ))}
        </div>
    );
}

export default ListAudioItem;