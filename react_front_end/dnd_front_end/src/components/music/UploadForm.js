import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from '.././shared/Spinner'
import { DND_FORM } from '.././shared/DNDRequests';

class UploadForm extends Component {

    state = {
        file: null,
        name: null,
        audio_type: null,
        uploading: false,
    }

    constructor(props) {
        super(props);
        this.form_ref = React.createRef();
    }

    handleSubmit = (event) => {
        if( this.state.file == null || this.state.name == null || this.state.audio_type == null){
            event.preventDefault();
            return
        }

        event.preventDefault();

        this.setState({uploading: true})

        const formData = new FormData()
        formData.append('file', this.state.file, this.state.file.name)
        formData.append('display_name', this.state.name)
        formData.append('audio_type', this.state.audio_type)

        DND_FORM(
            '/audio/upload',
            formData,
            (data) => {
                this.setState({uploading: false, file: null, display_name: null, audio_type: null})
                this.form_ref.current.reset()
                this.props.refresh_function()
            },
            null
        )
      
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleFileChange = e => {
        this.setState({ file: e.target.files[0] });
    };


    render(){
        if(this.state.uploading === true) {
            return (
                <Spinner/>
            )
        }

        return(
            <Form ref={this.form_ref}>
                <Row>
                    <Col xs={3} md={3}>
                        <Form.Label>Upload New File</Form.Label>
                        <Form.File name="files" onChange={this.handleFileChange} />
                    </Col>

                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Display Name</Form.Label>
                            <Form.Control required value={this.state.name} name="name" type="text" placeholder="Enter Display Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Audio Type</Form.Label>
                            <Form.Control required name="audio_type" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    <option value="AMBIENCE" selected={this.state.audio_type == "AMBIENCE"}>Ambient Sound</option>
                                    <option value="MUSIC" selected={this.state.audio_type == "MUSIC"}>Music</option>
                                    <option value="EFFECT" selected={this.state.audio_type == "EFFECT"}>Sound Effect</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Button variant="primary" type="submit" onClick={this.handleSubmit}>
                            Submit
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default UploadForm;