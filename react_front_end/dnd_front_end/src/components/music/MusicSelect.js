import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import FilterMusic from './FilterMusic';
import Card from 'react-bootstrap/esm/Card';
import APIButton from '.././shared/APIButton';
import { SoundEffectButton } from './SoundEffectButton';

const create_audio_command = (id, play_type) => {
    var data = {
        "music_play_type": play_type,
        "audio_id": id
    }
    return {
        "command_code": "CMD_PLAY",
        "data": data
    }
}

export const SingleMusicListItem = ({audio_item, select_function, disabled_ids}) => {
    return (
        <Card>
            {audio_item.audio_type != "EFFECT" &&
                <Card.Title>{audio_item.name} <APIButton data={create_audio_command(audio_item.id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton></Card.Title>
            }
            {audio_item.audio_type == "EFFECT" &&
                <Card.Title>{audio_item.name} <SoundEffectButton file_name={audio_item.file_name} icon="fas fa-play"/></Card.Title>
            }
            <Card.Body>
                <div className='icon'>
                    { audio_item.audio_type == 'MUSIC' &&
                        <i class="fas fa-music"></i>
                    }
                    { audio_item.audio_type == 'AMBIENCE' &&
                        <i class="fas fa-cloud-rain"></i>
                    }
                    { audio_item.audio_type == 'EFFECT' &&
                        <i class="fas fa-volume-up"></i>
                    }
                </div>
                {disabled_ids.includes(audio_item.id) == false &&
                    <Button onClick={() => {select_function(audio_item.id, audio_item)}}>Select Audio</Button>
                }
            </Card.Body>
        </Card>
    )
}

const ListMusic = ({ music_items, select_function, disabled_ids}) => {

    return (
        <div className="audio-items">
            {music_items.map((audio_item) => (
                <div key={audio_item.id} className={disabled_ids.includes(audio_item.id) ? "audio-item disabled" : "audio-item"}>
                    <SingleMusicListItem audio_item={audio_item} select_function={select_function} disabled_ids={disabled_ids}/>
                </div>
            ))}
        </div>
    );
}

class MusicSelect extends Component {

    state = {
        music_items: [],
        page: 1,
        total_pages: 1,
        filters: {},
        toggle: false
    }

    componentDidMount() {
        this.refresh_music(this.state.page, this.state.filters)
    };

    select_music = (music, object) => {

        this.setState({toggle: false})
        this.props.callback(music, object)
    }

    refresh_music = (page, filters) => {
        var params = filters
        
        params['page'] = page
        params['results_per_page'] = 20
        if(this.props.audio_type_filter != undefined && this.props.audio_type_filter != null){
            params['audio_type'] = this.props.audio_type_filter
        }

        DND_GET(
          '/audio?' + stringify(params),
          (jsondata) => {
            this.setState({ music_items: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_music(page, this.state.filters)
    };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_music(1, new_filters))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_music(1, []))
    };

    quick_refresh = () => {
      this.refresh_music(this.state.page, this.state.filters)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id, name, dexterity, xp, image) => {

        this.props.select_function(id, name, dexterity, xp, image)
        this.setState({toggle: false})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>Choose Audio</Button>
                }
                <Modal show={this.state.toggle} size="md" className="music-select-modal select-modal">
                        <Modal.Header>Music</Modal.Header>
                        <Modal.Body>
                            <FilterMusic filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters} hide_type_filter={true}/>
                            <ListMusic
                                music_items={this.state.music_items}
                                select_function={this.select_function}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default MusicSelect;