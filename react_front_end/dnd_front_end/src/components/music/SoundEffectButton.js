import React, { useContext } from 'react';
import {SocketContext} from '.././socket/Socket';

export const SoundEffectButton = ({ file_name, icon, border}) => {

    const socket = useContext(SocketContext);

    function on_play(){
        socket.emit("sound_effect_play", file_name)
    }
    
    return (
        <i class={border == true ? icon + " clickable-icon sound-effect border": icon + " sound-effect clickable-icon"} onClick={() => {on_play()}}></i>
    );
}