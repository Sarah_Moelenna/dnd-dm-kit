import React, { Component } from 'react';
import ListPages from '.././notes/ListPages';

class RelatedNotesSidebar extends Component {

    goto_page = (page_id) => {
        var override = {
            "page": "NOTES",
            "page_focus": page_id,
            "collection_focus": this.get_collection_id_for_page_id(page_id)
        }
        this.props.override_function(override)
    }

    get_name_for_page_id = (page_id, collection) => {
        for(var i = 0; i < collection.pages.length; i++){
            if (collection.pages[i].id == page_id){
                return collection.pages[i].name
            }
        }
    }

    get_collection_id_for_page_id = (page_id) => {
        for(var j = 0; j < this.props.current_campaign.collections.length; j++){
            var collection = this.props.current_campaign.collections[j]
            for(var i = 0; i < collection.pages.length; i++){
                if (collection.pages[i].id == page_id){
                    return collection.id
                }
            }
        }
    }

    get_page_order = (collection) => {

        var pages = []
        var ids = []
        var heirachy = collection.heirachy

        if (heirachy == null){
            heirachy = this.create_initial_heirachy(collection)
        }

        for(var i = 0; i < Object.keys(heirachy).length; i++){
            pages.push(
                heirachy[i]
            )
            ids.push(
                heirachy[i].id
            )
        }

        for(var i = 0; i < collection.pages.length; i++){
            if(!ids.includes(collection.pages[i].id)){
                var page = collection.pages[i]
                page["indentation"] = 0
                pages.push(
                    page
                )
            }
        }

        for(var i = 0; i < pages.length; i++){
            pages[i].name = this.get_name_for_page_id(pages[i].id, collection)
        }

        return pages
    }

    create_initial_heirachy = (collection) => {
        var heirachy = {}

        for(var i = 0; i < collection.pages.length; i++){
            heirachy[i] = collection.pages[i]
            heirachy[i]["indentation"] = 0
        }

        return heirachy
    }

    render(){
            return(
                <div className="related-campaign-notes">
                    {
                        this.props.current_campaign != undefined && this.props.current_campaign != null ?
                        <div><h1>Related Collections - {this.props.current_campaign.name}</h1><i className="fas fa-sync clickable-icon" onClick={this.props.refresh_campaign_function}></i></div>
                        :
                        <div><h1>Related Collections</h1><i className="fas fa-sync clickable-icon" onClick={this.props.refresh_campaign_function}></i></div>
                    }
                    <hr/>
                    {
                        this.props.current_campaign != undefined && this.props.current_campaign != null && this.props.current_campaign.collections.length > 0 != null ?
                        <div>
                            {this.props.current_campaign.collections.map((collection) => (
                                <div key={collection.id}>
                                    <h2>{collection.name}</h2>
                                    <ListPages pages={this.get_page_order(collection)} read={true} view_page_function={this.goto_page}/>
                                </div>
                            ))}
                        </div>
                    :
                        <p>No Currently Active Campaign with Related Collections</p>
                    }
                    
                </div>
            );
    };
}

export default RelatedNotesSidebar;