import React, { Component } from 'react';
import ListMessages from '.././discord/ListMessages';

class DiscordTracker extends Component {

    render(){
            return(
                <div className="discord-tracker">
                    <h1>Discord Messages</h1>
                    <hr/>
                    <ListMessages messages={this.props.bot_state.messages}/>
                </div>
            );
    };
}

export default DiscordTracker;