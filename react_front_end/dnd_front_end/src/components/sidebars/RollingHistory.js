import React, { Component } from 'react';
import ListRolls from '../rolling_history/ListRolls';
import Collapsible from '../shared/Collapsible'
import CustomRoller from '../shared/CustomRoller';

class RollingHistory extends Component {

    render(){
            return(
                <div className="rolling-history">
                    <h1>Rolling History</h1>
                    <Collapsible contents={<CustomRoller roll_dice_function={this.props.roll_dice_function} type="DUNGEON_MASTER" identity={null} />} title="Roll Dice"/>
                    <hr/>
                    <ListRolls rolls={this.props.rolling_history}/>
                </div>
            );
    };
}

export default RollingHistory;