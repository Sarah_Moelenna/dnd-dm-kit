import React, { Component } from 'react';
import ListCharacters from '.././character/ListCharacters';

class CharacterTracker extends Component {

    render(){
            return(
                <div className="character-tracker">
                    <h2 className="title">Character Status</h2>
                    {
                        this.props.characters == null ? <i className="fas fa-sync clickable-icon" onClick={this.props.refresh_characters_function}></i> : (this.props.characters.length > 0 ? <i className="fas fa-sync clickable-icon" onClick={this.props.refresh_characters_function}></i> : null)
                    }
                    <br/>
                    <ListCharacters characters={this.props.characters}/>

                </div>
            );
    };
}

export default CharacterTracker;