import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'

class CampaignNotesSidebar extends Component {

    state = {
        notes: "",
    }

    constructor(props) {
        super(props);
        if(this.props.current_campaign != undefined){
            this.state = {
                notes: this.props.current_campaign.notes
            };
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.current_campaign == null){
            return true;
        }
        if (nextProps.current_campaign.notes != this.state.notes){
            this.setState({notes: nextProps.current_campaign.notes})
        }
        return true
    }

    handleChange = e => {
        
        this.props.update_campaign_notes_function(e.target.value)
        this.setState({notes: e.target.value})

        e.preventDefault();
    };

    render(){
            return(
                <div className="active-campaign-notes">
                    {
                        this.props.current_campaign != undefined && this.props.current_campaign != null ?
                        <div><h1>Campaign Notes - {this.props.current_campaign.name}</h1><i className="fas fa-sync clickable-icon" onClick={this.props.refresh_campaign_function}></i></div>
                        :
                        <div><h1>Campaign Notes</h1><i className="fas fa-sync clickable-icon" onClick={this.props.refresh_campaign_function}></i></div>
                    }
                    <hr/>
                    {
                        this.props.current_campaign != undefined && this.props.current_campaign != null ?
                        <Form.Group controlId="Campaign Notes">
                            <Form.Label>Campaign Notes</Form.Label>
                            <Form.Control name="notes" as="textarea" rows={7} value={this.state.notes ? this.state.notes : ''} onChange={this.handleChange}/>
                        </Form.Group>
                    :
                        <p>No Currently Active Campaign</p>
                    }
                    
                </div>
            );
    };
}

export default CampaignNotesSidebar;