import React from 'react';

const create_uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

const ListEmbeds = ({ embeds }) => {
    if(embeds === null || embeds === undefined || embeds.length == 0) {
        return null
    }
    return (
        <div className="embeds">
            {embeds.map((embed) => (
                <div key={create_uuidv4}>
                    <p>{embed.title}<br/>{embed.description}</p>
                </div>
            ))}
        </div>
    );
}

export default ListEmbeds;