import React from 'react';
import SingleMessage from './SingleMessage'

const ListMessages = ({ messages}) => {
    if(messages === null || messages === undefined || messages.length == 0) {
        return (<p>No Messages</p>)
    }
    return (
        <div className="messages">
            {messages.map((message) => (
                <div key={message.id}>
                    <SingleMessage message={message} />
                </div>
            ))}
        </div>
    );
}

export default ListMessages;