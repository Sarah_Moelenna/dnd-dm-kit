import React from 'react';
import Markdown from 'react-markdown'
import ListEmbeds from './ListEmbeds'

const SingleMessage = ({ message }) => {

    var message_content = message.content

    for(var i = 0; i < message.mentioned_users.length; i++){
        var mention = message.mentioned_users[i]
        message_content = message_content.replaceAll("<@!" + mention.id + ">", '*@' + mention.name + '*').replaceAll('<:beyond:783780183559372890>', '*D&DBeyond*')
    }

    return (
        <div className='message'>
            <h2 className='title'>{message.author}</h2>
            <Markdown children={message_content} />
            <ListEmbeds embeds={message.embeds} />
            <span className='time'><i>{message.minutes_since_created} Minutes Ago</i></span>
            <hr/>
        </div>
    );
}

export default SingleMessage;