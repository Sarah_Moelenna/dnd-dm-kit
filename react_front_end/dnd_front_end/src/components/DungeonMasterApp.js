import React, {Component} from 'react';
import {DMNavigation, get_page_from_path} from './layout/Navigation';
import Header from './layout/Header';
import Footer from './layout/Footer';
import SideBar from './layout/SideBar';
import SessionPage from './pages/SessionPage';
import MusicPage from './pages/MusicPage';
import EncounterPage from './pages/EncounterPage';
import MonsterPage from './pages/MonsterPage';
import CampaignPage from './pages/CampaignPage';
import MappingPage from './pages/MappingPage';
import BattleMapPage from './pages/BattleMapPage';
import NotesPage from './pages/NotesPage';
import SettingsPage from './pages/SettingsPage';
import ItemPage from './pages/ItemPage';
import RacePage from './pages/RacePage';
import SpellPage from './pages/SpellPage';
import ClassPage from './pages/ClassPage';
import CharacterPage from './pages/CharacterPage';
import MyContentCollectionPage from './pages/MyContentCollectionPage';
import ContentCollectionPage from './pages/ContentCollectionPage';
import ImageUploadsPage from './pages/ImageUploadsPage';
import BackgroundPage from './pages/BackgroundPage';
import { DND_GET, DND_PUT } from './shared/DNDRequests';
import { SocketContext, socket } from './socket/Socket';
import {withRouter} from 'react-router-dom';
import { PlayerContext } from './shared/PlayerContext';
import SpellListPage from './pages/SpellListPage';
import FeatPage from './pages/FeatPage';
import MusicPlayerPage  from './pages/MusicPlayerPage';
import PlaylistPage from './pages/PlaylistPage';
import EffectDeckPage from './pages/EffectDeckPage';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class DungeonMasterApp extends Component {

  state = {
    //bot_state: {},
    current_page: "CAMPAIGN",
    roll_history: [],
    characters: null,
    current_campaign: null,
    campaigns: null,
    override: {},
    current_user: null,
    name: "Dungeon Master",

    loaded: false,

    left_collapsed: false,
    right_collapsed: false,
  }

  get_player_context = ()=> {
    return {
      'user': this.state.current_user != null ? this.state.current_user : {},
      'characters': this.state.characters != null ? this.state.characters : {},
      'campaigns': this.state.campaigns != null ? this.state.campaigns : [],
      'current_campaign': this.state.current_campaign != null ? this.state.current_campaign : {}
    }
  }

  componentDidMount() {
    this.refresh_characters()
    this.refresh_user()
    //this.interval = setInterval(this.refresh_bot_state, 1000);


    var page = get_page_from_path(this.props.location.pathname)
    if(page != undefined && page != null){
      this.set_page(page)
    }

    socket.on("session_end", data => {
      this.refresh_characters()
      this.refresh_user()
    });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  //refresh_bot_state = () => {
  //  DND_GET(
  //    '/bot-state',
  //    (json) => {this.setState({ bot_state: json})},
  //    null
  //  )
  //};

  set_override = (new_value) => {
    if(new_value.page != undefined){
      this.setState({ override: new_value, current_page: new_value.page})
    } else {
      this.setState({ override: new_value})
    }
  };

  toggle_sidebar_collapse = (side) => {
    if(side == "LEFT"){
      this.setState({left_collapsed: !this.state.left_collapsed})
    }
    if(side == "RIGHT"){
      this.setState({right_collapsed: !this.state.right_collapsed})
    }
  }

  refresh_user = () => {
    DND_GET(
      '/user',
      (json) => {
        if(json['active_campaign'] != null && json['active_campaign']['session'] != null){
          socket.emit('join', json['active_campaign']['session']['id']);
        } else {
          socket.emit('join', "user-" + cookies.get('auth_token'));
        }
        this.setState({ 
          current_campaign: json['active_campaign'],
          current_user: json['user'], 
          campaigns: json['campaigns'], 
          name: json['dm_name'],
          loaded: true
        }
      )},
      null
    )
  };

  update_campaign_notes = (new_notes) => {
    var data = {
      notes: new_notes,
    }

    DND_PUT(
      '/campaign/' + this.state.current_campaign.id,
      data,
      null,
      null
    )

    var new_campaign = this.state.current_campaign
    new_campaign.notes = new_notes
    this.setState({ current_campaign: new_campaign})
  }

  set_page = (page) => {
    this.setState({ current_page: page })
  };

  refresh_characters = () => {
    this.setState({ characters: []})

    DND_GET(
      '/character-tracker',
      (json) => {
        if(json.length>0){
            this.setState({ characters: json})
        } else {
            this.setState({ characters: null})
        }
      },
      null
    )
  };

  roll_dice = (input, type, modifier, identity) => {
    var url = '/dice-roll/?roll=' + encodeURIComponent(input) + "&roll_type=" + type + "&roll_modifier=" + modifier + "&roll_identity=" + encodeURIComponent(identity)

    DND_GET(
      url,
      (json) => {
        var new_roll_history = this.state.roll_history
        new_roll_history.unshift(json)
        var data = {
          type: "ROLL",
          name: "",
          data: json
        }
        socket.emit("send_message", data);
        this.setState({ roll_history: new_roll_history})
      },
      null
    )
    
  }

  get_page_view_class = () => {
    if (this.state.left_collapsed == true && this.state.right_collapsed == true){
      return "page-view-section two-collapsed"
    } else if (this.state.left_collapsed == true || this.state.right_collapsed == true){
      return "page-view-section one-collapsed"
    }

    return "page-view-section"
  }

  render () {
    return(
        <div className="dm-app">
          <PlayerContext.Provider value={this.get_player_context()}>
            <Header/>
            <DMNavigation page_switch={this.set_page} current_page={this.state.current_page} logout_function={this.props.logout}/>
              <SocketContext.Provider value={socket}>
                {this.state.loaded == true && 
                  <div>
                    <div className="sidebar-buttons">
                      {this.state.left_collapsed == true ?
                          <div className="left-sidebar-button" onClick={() => {this.toggle_sidebar_collapse("LEFT")}}><i className="fas fa-angle-double-right"></i></div>
                        :
                          <div className="left-sidebar-button" onClick={() => {this.toggle_sidebar_collapse("LEFT")}}><i className="fas fa-angle-double-left"></i></div>
                      }
                      {this.state.right_collapsed == true ?
                          <div className="right-sidebar-button" onClick={() => {this.toggle_sidebar_collapse("RIGHT")}}><i className="fas fa-angle-double-left"></i></div>
                        :
                          <div className="right-sidebar-button" onClick={() => {this.toggle_sidebar_collapse("RIGHT")}}><i className="fas fa-angle-double-right"></i></div>
                      }
                    </div>
                    <div className="page-container">
                        <div className={this.state.left_collapsed == false ? "sidebar-section" : "sidebar-section collapsed"}>
                            <SideBar
                              bot_state={{}}
                              refresh_characters_function={this.refresh_characters}
                              characters={this.state.characters}
                              rolling_history={this.state.roll_history}
                              refresh_campaign_function={this.refresh_user}
                              current_campaign={this.state.current_campaign}
                              update_campaign_notes_function={this.update_campaign_notes}
                              override_function={this.set_override}
                              roll_dice_function={this.roll_dice}
                              user_type="DM"
                              name={this.state.name}
                            />
                        </div>
                        <div className={this.get_page_view_class()}>
                            <div className="page-bg">
                                <SessionPage active={this.state.current_page=="DMSESSION"} user={this.state.user} active_campaign={this.state.current_campaign} campaigns={this.state.campaigns} refresh_function={this.refresh_user} is_player={false}/>
                                <MusicPage active={this.state.current_page=="MUSIC"}/>
                                <CampaignPage active={this.state.current_page=="CAMPAIGN"} bot_state={{}} override_function={this.set_override} roll_dice_function={this.roll_dice}/>
                                <MonsterPage pathname={this.props.location.pathname} active={this.state.current_page=="MONSTER"} override={this.state.override} override_function={this.set_override}/>
                                <EncounterPage active={this.state.current_page=="ENCOUNTER"} roll_dice_function={this.roll_dice} override={this.state.override} override_function={this.set_override}/>
                                <SettingsPage active={this.state.current_page=="SETTINGS"} user={this.state.current_user}/>
                                <NotesPage active={this.state.current_page=="NOTES"} override={this.state.override} override_function={this.set_override}/>
                                <MappingPage active={this.state.current_page=="MAPPING"} override={this.state.override} override_function={this.set_override}/>
                                <BattleMapPage active={this.state.current_page=="BATTLEMAP"} />
                                <ItemPage pathname={this.props.location.pathname} active={this.state.current_page=="ITEM"} />
                                <BackgroundPage pathname={this.props.location.pathname} active={this.state.current_page=="BACKGROUND"} />
                                <RacePage active={this.state.current_page=="RACE"} />
                                <MyContentCollectionPage active={this.state.current_page=="MYCONTENTCOLLECTIONS"} />
                                <ContentCollectionPage active={this.state.current_page=="CONTENTCOLLECTIONS"} />
                                <ImageUploadsPage active={this.state.current_page=="MYIMAGES"} />
                                <SpellPage pathname={this.props.location.pathname} active={this.state.current_page=="SPELL"} override={this.state.override}/>
                                <ClassPage pathname={this.props.location.pathname} active={this.state.current_page=="CLASS"} override={this.state.override}/>
                                <CharacterPage pathname={this.props.location.pathname} active={this.state.current_page=="CHARACTER"}/>
                                <SpellListPage pathname={this.props.location.pathname} active={this.state.current_page=="SPELLLIST"}/>
                                <FeatPage pathname={this.props.location.pathname} active={this.state.current_page=="FEAT"} />
                                <MusicPlayerPage active={this.state.current_page=="MUSICPLAYER"} />
                                <PlaylistPage active={this.state.current_page=="PLAYLIST"} />
                                <EffectDeckPage active={this.state.current_page=="EFFECTDECK"} />
                            </div>
                        </div>
                        <div className={this.state.right_collapsed == false ? "sidebar-section" : "sidebar-section collapsed"}>
                            <SideBar
                              bot_state={{}}
                              refresh_characters_function={this.refresh_characters}
                              characters={this.state.characters}
                              rolling_history={this.state.roll_history}
                              refresh_campaign_function={this.refresh_user}
                              current_campaign={this.state.current_campaign}
                              update_campaign_notes_function={this.update_campaign_notes}
                              override_function={this.set_override}
                              roll_dice_function={this.roll_dice}
                              user_type="DM"
                              name={this.state.name}
                            />
                        </div>
                    </div>
                    <div className="fake-footer"></div>
                    <Footer/>
                  </div>
                  }
              </SocketContext.Provider>
            </PlayerContext.Provider>
        </div>
    )

  }
}

export default withRouter(DungeonMasterApp);
