import React, { useRef } from 'react';
import ListSettings from './ListSettings'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import ProgressBar from 'react-bootstrap/ProgressBar'
import { DND_PUT } from '.././shared/DNDRequests';


const ListSettingsGroups = ({ settings_groups, refresh_function, user}) => {
    const formRef = useRef(null);

    const handleSubmit = (event) => {
        
        var old_password = event.target[0].value
        var new_password = event.target[1].value
        var confirm_password = event.target[2].value
    
        if(new_password != confirm_password){
            event.preventDefault();
            return;
        }
    
        var data = {
            old_password: old_password,
            password: new_password
        }
    
        DND_PUT(
            '/account',
            data,
            null,
            null
        )

        formRef.current.reset()
    
        event.preventDefault();
    }

    if(settings_groups === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="settings-groups">
            <div className="settings-groups">
                <Card className="settings-group">
                    <Card.Body>
                    <Card.Title>My Account</Card.Title>
                    {user != undefined && user != null &&
                        <div className="settings">
                            <div className="setting">
                                <p className="setting-display-name">File Storage</p>
                                <div className="setting-progress-bar">
                                    <ProgressBar animated now={user.storage_used} label={(user.storage_used/1073741274).toFixed(2) + "/" + (user.max_storage/1073741274).toFixed(2) + "GB Used"}  min={0} max={user.max_storage}/>
                                </div>
                            </div>
                            <hr/>
                        </div>
                    }
                    <div className="settings">
                        <div className="setting">
                            <p className="setting-display-name">Update Password</p>
                            <Form ref={formRef} onSubmit={handleSubmit}>
                                <Form.Group>
                                    <Form.Label>Current Password</Form.Label>
                                    <Form.Control type="password" name="current_password"/>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>New Password</Form.Label>
                                    <Form.Control type="password" name="new_password"/>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Confirm new Password</Form.Label>
                                    <Form.Control type="password" name="confirm_password"/>
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Submit
                                </Button>
                            </Form>
                            <hr/>
                        </div>
                    </div>
                    </Card.Body>
                </Card>
            </div>
            {Object.keys(settings_groups).map((settings_group) => (
                <Card key={settings_group} className="settings-group">
                    <Card.Body>
                    <Card.Title>{settings_group}</Card.Title>
                    <ListSettings settings={settings_groups[settings_group]} refresh_function={refresh_function}/>
                    </Card.Body>
                </Card>
            ))}
        </div>
    );
}

export default ListSettingsGroups;