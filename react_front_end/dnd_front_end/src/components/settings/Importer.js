import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Spinner from '.././shared/Spinner';
import { DND_FORM } from '.././shared/DNDRequests';

class Importer extends Component {

    state = {
        uploading: false,
    }

    upload_file = (e) => {
        if(e.target.files.length == 0){
            return
        }

        this.setState({uploading: true})

        const formData = new FormData()
        formData.append('file', e.target.files[0], e.target.files[0].name)
        
        DND_FORM(
            '/import',
            formData,
            (data) => {
                this.setState({uploading: false})
            },
            null
        )
    }
    render(){
            return(
                <div className="importer">
                    { this.state.uploading == false ?
                    <Form.File onChange={this.upload_file} /> : <Spinner/>
                    }
                </div>
            );

    };
}

export default Importer;