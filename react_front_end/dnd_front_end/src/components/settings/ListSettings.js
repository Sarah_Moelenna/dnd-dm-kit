import React from 'react';
import Form from 'react-bootstrap/Form'
import { DND_POST } from '.././shared/DNDRequests';

const ListSettings = ({ settings, refresh_function}) => {

    const handleChange = e => {
        var value = e.target.value
        if(e.target.dataset.type=="BOOL"){
            value = (value === 'true');
        }

        var data = {
            setting_name: e.target.name,
            setting_value: value,
        }

        DND_POST(
            '/user-settings',
            data,
            (response) => {
                refresh_function()
            },
            null
        )
    };

    if(settings === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="settings">
            {settings.map((setting) => (
                <div key={setting.name} className="setting">
                    <p className="setting-display-name">{setting.display_name}</p>
                    {
                        setting.type == "BOOLEAN" &&
                        <Form.Control name={setting.name} data-type="BOOL" as="select" onChange={handleChange} custom>
                                    <option value={true} selected={setting.value == true}>True</option>
                                    <option value={false} selected={setting.value == false}>False</option>
                        </Form.Control>
                    }
                    {
                        setting.type == "STRING" &&
                        <Form.Control data-type="STRING" value={setting.value} name={setting.name} type="text" onChange={handleChange}/>
                    }
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListSettings;