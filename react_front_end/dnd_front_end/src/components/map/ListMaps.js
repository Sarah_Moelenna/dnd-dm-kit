import React from 'react';
import { get_api_url } from '../shared/Config';
import { DND_DELETE } from '.././shared/DNDRequests';
import Card from 'react-bootstrap/Card'
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListMaps = ({ maps, refresh_function, view_map_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/map/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(maps === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="map-items">
             {maps.map((map) => (
                <div key={map.id} className="map-item">
                    <Card>
                        { map.url &&
                            <Card.Img variant="top" src={get_api_url() + "/" + map.url} />
                        }
                        <Card.Body>
                            <Card.Title>{map.name}</Card.Title>
                            <a className="btn btn-primary" onClick={() => view_map_function(map.id)}>View Map</a>
                            <DeleteConfirmationButton id={map.id} name="Map" delete_function={delete_page_function} />
                        </Card.Body>
                    </Card>
                </div>
            ))}
        </div>
    );
}

export default ListMaps;