import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'

class FilterMap extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="mao-filters">
                <Form.Group controlId="formBasicName">
                    <Form.Label>Map Name</Form.Label>
                    <Form.Control value={this.props.filters["name"]} name="name" type="text" placeholder="Search Map Names" onChange={this.handleChange}/>
                </Form.Group>
            </Form>
        );
    };
}

export default FilterMap;