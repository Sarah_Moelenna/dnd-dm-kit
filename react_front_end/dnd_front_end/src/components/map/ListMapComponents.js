import React, { useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import MDEditor from '@uiw/react-md-editor';
import Markdown from 'react-markdown'
import {process_markdown, create_link} from '.././notes/MarkdownHandlers';
import Button from 'react-bootstrap/Button'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Collapsible from '.././shared/Collapsible';


const DeleteNotes = ({callback}) => {

    const [isVisible, setIsVisible] = useState(false);

    const toggle = () => setIsVisible(!isVisible);

    function process_selection(){
        toggle()
        callback()
    }

    var popover = <Popover className="delete-text-popover">
        <Popover.Title>Delete Notes</Popover.Title>
        <Popover.Content className="content">
            <p>Are You Sure?</p>
            <Row>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => process_selection()}>
                        <p>Yes</p>
                    </div>
                </Col>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => toggle()}>
                        <p>No</p>
                    </div>
                </Col>
            </Row>
        </Popover.Content>
    </Popover>

    return (
        <div className="delete-text">
            <div>
                <OverlayTrigger show={isVisible} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                    <p>DELETE NOTES</p>
                </OverlayTrigger>
            </div>
        </div>
    );
}

const ListMapComponents = ({ read, items, alter_component_state, delete_component, override_function}) => {

    function change_attribute(e){
        var type = e.target.dataset.type
        var id = e.target.dataset.id
        var value = e.target.value
        var attribute = e.target.name
        alter_component_state(type, id, attribute, value)
    };

    const [filter, setFilter] = useState("ROOM");

    const change_filter = (value) => setFilter(value);

    if(items === undefined) {
        return "";
    }
    return (
        <div className="components-container">
            <h2>Map Components</h2>
            <div className="filters">
                <div className={filter=='ROOM' ? 'clickable-div active' : 'clickable-div'} onClick={() => change_filter("ROOM")}>
                    <i className="far fa-square"></i>
                    <p>Room</p>
                </div>
                <div className={filter=='DOOR' ? 'clickable-div active' : 'clickable-div'} onClick={() => change_filter("DOOR")}>
                    <i className="fas fa-dungeon"></i>
                    <p>Door</p>
                </div>
                <div className={filter=='POI' ? 'clickable-div active' : 'clickable-div'} onClick={() => change_filter("POI")}>
                    <i className="fas fa-map-marker-alt"></i>
                    <p>POI</p>
                </div>
                <div className={filter=='TRAP' ? 'clickable-div active' : 'clickable-div'} onClick={() => change_filter("TRAP")}>
                    <i className="fas fa-skull-crossbones"></i>
                    <p>Traps</p>
                </div>
            </div>
            <div className="headers">
                <Row>
                    {read == false &&
                        <Col xs={1} md={1}>
                            <p className="card-text"><b>Type</b></p>
                        </Col>
                    }
                    <Col xs={read == false ? 2 : 7} md={read == false ? 2 : 7}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    {read == false &&
                        <Col xs={5} md={5}>
                            <p className="card-text"><b>Notes</b></p>
                        </Col>
                    }
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Visibility</b></p>
                    </Col>
                    {read == false &&
                        <Col xs={1} md={1}>
                        </Col>
                    }
                </Row>
                <hr/>
            </div>
            {items.map((item) => (
                <div key={item.id} className={ (filter==item.type ? "item show" : "item hide") + (item.highlighted != undefined && item.highlighted == true ? " highlighted" : "")}>
                    <Row>
                        {read == false &&
                            <Col xs={1} md={1}>
                                <p className="card-text">{item.type}</p>
                            </Col>
                        }
                        <Col xs={read == false ? 2 : 7} md={read == false ? 2 : 7}>
                            {item.description != undefined && read == false &&
                                <Form.Control name="description" data-type={item.type} data-id={item.id} type="text" value={item.description} onChange={change_attribute}/>
                            }
                            {read == true &&
                                <p>{item.description}</p>
                            }
                        </Col>
                        {read == false &&
                            <Col xs={5} md={5}>
                                {item.type != "DOOR" && item.type != "POI" && read == false &&
                                    <div>
                                        {(item.notes != undefined && item.notes != null) ?
                                            <div>
                                                <MDEditor
                                                    value={item.notes}
                                                    preview="edit"
                                                    onChange={(text) => {alter_component_state(item.type, item.id, "notes", text)}}
                                                />
                                                <DeleteNotes callback={() => {alter_component_state(item.type, item.id, "notes", null)}}/>
                                            </div>
                                            :
                                            <Button className="btn btn-primary"
                                            onClick={() => {alter_component_state(item.type, item.id, "notes", "")}}
                                            >Add Notes</Button>
                                        }
                                    </div>
                                }
                            </Col>
                        }
                        <Col xs={2} md={2}>
                            {item.visibility == 1 ?
                                <i className={item.type == "ROOM" ? "far fa-eye-slash clickable-icon" :  "far fa-eye clickable-icon"}  onClick={() => alter_component_state(item.type, item.id, "visibility", 0)}></i>
                            :
                                <i className={item.type == "ROOM" ? "far fa-eye clickable-icon" :  "far fa-eye-slash clickable-icon"}  onClick={() => alter_component_state(item.type, item.id, "visibility", 1)}></i>
                            }
                        </Col>
                        {read == false &&
                            <Col xs={1} md={1}>
                                <i className="fa fa-trash clickable-icon"  onClick={() => delete_component(item.type, item.id)}></i>
                            </Col>
                        }
                        {read == true && item.notes != undefined && item.notes != null && item.notes != "" &&
                            <Col xs={12} md={12}>
                                <div className="notes">
                                    <Collapsible 
                                        contents={
                                            <Markdown
                                                components={{a: props => {return create_link(props, override_function, false, true)}}}
                                                children={process_markdown(item.notes)}
                                                />
                                        } 
                                        title="Notes"
                                    />
                                </div>
                            </Col>
                        }
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListMapComponents;