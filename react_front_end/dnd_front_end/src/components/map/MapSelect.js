import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import FilterMap from './FilterMap';
import { get_api_url } from '../shared/Config';
import Card from 'react-bootstrap/Card'


const ListMaps = ({ maps, set_sort, current_sort_by, current_sort_value, select_function, disabled_ids}) => {

    function get_sort(sort_by){
        if(current_sort_by == sort_by){
            if(current_sort_value == "DESC"){
                return (
                    <div className="clickable-div" onClick={() => {set_sort(null, null)}}>
                        <i className="fas fa-sort-down"></i>
                    </div>
                ) 
            } else {
                return (
                    <div className="clickable-div" onClick={() => {set_sort(sort_by, "DESC")}}>
                        <i className="fas fa-sort-up"></i>
                    </div>
                ) 
            }
        } else {
            return (
                <div className="clickable-div" onClick={() => {set_sort(sort_by, "ASC")}}>
                    <i className="fas fa-sort"></i>
                </div>
            )
        }
    }

    return (
        <div className="map-items">
            {maps.map((map) => (
                <div key={map.id} className={disabled_ids.includes(map.id) ? "map-item selected" : "map-item"}>
                    <Card>
                        { map.url &&
                            <Card.Img variant="top" src={get_api_url() + "/" + map.url} />
                        }
                        <Card.Body>
                            <Card.Title>{map.name}</Card.Title>
                            {!disabled_ids.includes(map.id) &&
                                <Button onClick={() => {select_function(map.id)}}>Select Map</Button>
                            }
                        </Card.Body>
                    </Card>
                </div>
            ))}
        </div>
    );
}

class MapSelect extends Component {

    state = {
        maps: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false
    }

    componentDidMount() {
        this.refresh_maps(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    select_map = (map) => {

        this.setState({toggle: false})
        this.props.callback(map)
    }

    refresh_maps = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        params['results_per_page'] = 10
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/map?' + stringify(params),
          (jsondata) => {
            this.setState({ maps: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_maps(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
        this.setState(
          {sort_by: sort_by, sort_value: sort_value},
          this.refresh_maps(this.state.page, this.state.filters, sort_by, sort_value)
        )
      };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_maps(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_maps(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_maps(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id, name, dexterity, xp, image) => {

        this.props.select_function(id, name, dexterity, xp, image)
        this.setState({toggle: false})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>Choose Map</Button>
                }
                <Modal show={this.state.toggle} size="md" className="map-select-modal select-modal">
                        <Modal.Header>Maps</Modal.Header>
                        <Modal.Body>
                            <FilterMap filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                            <ListMaps
                                maps={this.state.maps}
                                set_sort={this.set_sort}
                                current_sort_by={this.state.sort_by}
                                current_sort_value={this.state.sort_value}
                                select_function={this.select_function}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default MapSelect;