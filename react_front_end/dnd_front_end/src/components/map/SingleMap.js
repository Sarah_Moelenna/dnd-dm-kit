import React, { Component, useRef, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Spinner from '.././shared/Spinner';
import { v4 as uuidv4 } from 'uuid';
import Canvas from './Canvas';
import ListMapComponents from './ListMapComponents';
import MapCreationTools from './MapCreationTools';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_PUT } from '.././shared/DNDRequests';
import { get_api_url } from '../shared/Config';
import { SocketContext } from '../socket/Socket';
import { get_auth_token } from '../shared/AuthToken';
import UploadImage from '../images/UploadImage';

const REACT_APP_API_URL = get_api_url()

const PhaseTabs = ({phases, current_phase, show_add, add_phase_function, set_phase_function}) => {
    return (
        <div className="map-phase-tabs">
            {phases.map((phase, index) => (
                <div
                    key={index}
                    className={current_phase == index ? "phase-tab object current" : "phase-tab object"}
                    onClick={() => {index != current_phase && set_phase_function(index)}}
                >
                    <p>Phase {index + 1}</p>
                </div>
            ))}
            {show_add && 
                <div className="phase-tab">
                    <p><i className="fas fa-plus clickable-icon" onClick={() => {add_phase_function()}}></i></p>
                </div>
            }
        </div>
    )
}

class SingleMap extends Component {

    static contextType = SocketContext

    state = {
        map_data: {},
        creating: null,
        creation_data: {},
        image_load: false,
        pings: {},
        share: false,
        timers: []
    }

    constructor(props) {
        super(props);
        this.imageRef = React.createRef();
        this.state = {
            map_data: this.props.map.data,
            creating: null,
            creation_data: {},
            image_load: false,
            post_map: false,
            pings: {},
            share: false,
            timers: []
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot){
        if(this.props.player_read == true){
            if ((prevProps.map_data != this.props.map_data)) {
                this.setState({ 
                    map_data: this.props.map.data,
                });
            }
        }
    }

    componentDidMount(){
        const socket = this.context
        socket.on("map_id", map_id => {
            if(this.props.map.id == map_id){
                this.setState({share: true})
            } else {
                if(this.state.share == true){
                    this.setState({share: false})
                }
            }
        });

        socket.on("map_ping", coordinates => {
            var new_pings = this.state.pings
            new_pings[coordinates.id] = coordinates
            this.setState({pings: new_pings})
            setTimeout(
                () => {
                    var new_pings = {}
                    for(var i = 0; i < Object.keys(this.state.pings).length; i++){
                        var key = Object.keys(this.state.pings)[0]
                        if(key != coordinates.id){
                            new_pings[key] = this.state.pings[key]
                        }
                    }
                    this.setState({pings: new_pings})
                },
                1000
            );

        });
        socket.emit("get_current_map_id");
    }

    confirm_post_map = () => {
        this.setState({post_map: false})
    }

    post_map = () => {
        this.setState({post_map: true})
    }


    toggle_share_map = () => {
        var new_share = !this.state.share
        const socket = this.context
        if(new_share == true){
            socket.emit("map_update", this.props.map.id, get_auth_token());
        } else {
            socket.emit("map_clear");
        }
        this.setState({share: new_share})
    }

    update_image = (url, phase) => {
        var new_map_data = this.state.map_data

        if(new_map_data.phases == undefined){
            new_map_data.phases = []
        }
        new_map_data.phases[phase] = url

        if (phase == 0){
            var data = {
                "url": url,
                "data": new_map_data,
            }
        } else {
            var data = {
                "data": new_map_data,
            }
        }

        DND_PUT(
            '/map/' + this.props.map.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
          
    }

    update_data = (map_data) => {

        var data = {
            "data": map_data,
        }

        DND_PUT(
            '/map/' + this.props.map.id,
            data,
            (response) => {
                if(this.state.share == true){
                    const socket = this.context
                    socket.emit("map_update", this.props.map.id, get_auth_token());
                }
            },
            null
        )
          
    }

    intersects = (a,b,c,d,p,q,r,s) => {
        var det, gamma, lambda;
        det = (c - a) * (s - q) - (r - p) * (d - b);
        if (det === 0) {
            return false;
        } else {
            lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
            gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
            return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
        }
    };

    distance = (a,b,c,d) => {
        return Math.sqrt( Math.pow((a-c), 2) + Math.pow((b-d), 2) );
    }

    update_scale = (e) => {
        var scale = e.target.value/10

        var new_map_state = this.state.map_data
        new_map_state.scale = scale
        this.setState({map_data: new_map_state})
        this.update_data(new_map_state)
    }

    handle_canvas_move = (x, y) => {
        
        var change = false;
        var new_map_data = this.state.map_data

        if(this.state.map_data.rooms != undefined){
            var new_rooms = []
            for(var i = 0; i < this.state.map_data.rooms.length; i++){
                var room = this.state.map_data.rooms[i]
                var intersections = 0
                var highlighted = false
                for(var j = 0; j < room.points.length; j++){
                    var current_point = room.points[j]
                    var next_point = (j < room.points.length - 1 ? room.points[j+1] : room.points[0])
                    var intersects = this.intersects(current_point.x, current_point.y, next_point.x, next_point.y, x, y, 0, 0)
                    if(intersects == true){
                        intersections +=1;
                    }
                }
                if((intersections  % 2) == 1){
                    highlighted = true
                }
                if(room.highlighted == undefined){
                    room.highlighted = highlighted
                    change = true
                } else {
                    if(room.highlighted != highlighted){
                        change = true
                    }
                    room.highlighted = highlighted
                }
                new_rooms.push(room)
            }
            
            new_map_data.rooms = new_rooms
        }
        if(this.state.map_data.doors != undefined){
            var new_doors = []
            for(var i = 0; i < this.state.map_data.doors.length; i++){
                var highlighted = false
                var door = this.state.map_data.doors[i]
                var distance = this.distance(door.x, door.y, x, y)
                if (distance < 1.5){
                    highlighted = true
                }
                if(door.highlighted == undefined){
                    door.highlighted = highlighted
                    change = true
                } else {
                    if(door.highlighted != highlighted){
                        change = true
                    }
                    door.highlighted = highlighted
                }
                new_doors.push(door)
            }
            new_map_data.doors = new_doors
        }

        if(this.state.map_data.traps != undefined){
            var new_traps = []
            for(var i = 0; i < this.state.map_data.traps.length; i++){
                var highlighted = false
                var trap = this.state.map_data.traps[i]
                var distance = this.distance(trap.x, trap.y, x, y)
                if (distance < 1.5){
                    highlighted = true
                }
                if(trap.highlighted == undefined){
                    trap.highlighted = highlighted
                    change = true
                } else {
                    if(trap.highlighted != highlighted){
                        change = true
                    }
                    trap.highlighted = highlighted
                }
                new_traps.push(trap)
            }
            new_map_data.traps = new_traps
        }

        if(this.state.map_data.pois != undefined){
            var new_pois = []
            for(var i = 0; i < this.state.map_data.pois.length; i++){
                var highlighted = false
                var poi = this.state.map_data.pois[i]
                var distance = this.distance(poi.x, poi.y, x, y)
                if (distance < 1.5){
                    highlighted = true
                }
                if(poi.highlighted == undefined){
                    poi.highlighted = highlighted
                    change = true
                } else {
                    if(poi.highlighted != highlighted){
                        change = true
                    }
                    poi.highlighted = highlighted
                }
                new_pois.push(poi)
            }
            new_map_data.pois = new_pois
        }

        if(change == true){
            this.setState({map_data: new_map_data})
        }
    }

    handle_canvas_click = (x, y) => {
        if(this.props.player_read == true){
            const socket = this.context
            socket.emit("map_ping", {x: x, y: y});
        }

        if(this.state.creating == "ROOM" && this.state.creation_data.points != undefined){
            var new_creation_data = this.state.creation_data
            new_creation_data.points.push({
                "x": x,
                "y": y
            })
            this.setState({creation_data: new_creation_data})
        }
        if(this.state.creating == "DOOR"){
            var new_creation_data = this.state.creation_data
            if(new_creation_data.doors == undefined){
                new_creation_data.doors = []
            }
            new_creation_data.doors.push({
                "x": x,
                "y": y
            })
            this.setState({creation_data: new_creation_data})
        }
        if(this.state.creating == "POI"){
            var new_creation_data = this.state.creation_data
            if(new_creation_data.pois == undefined){
                new_creation_data.pois = []
            }
            new_creation_data.pois.push({
                "x": x,
                "y": y
            })
            this.setState({creation_data: new_creation_data})
        }
        if(this.state.creating == "TRAP"){
            var new_creation_data = this.state.creation_data
            if(new_creation_data.traps == undefined){
                new_creation_data.traps = []
            }
            new_creation_data.traps.push({
                "x": x,
                "y": y
            })
            this.setState({creation_data: new_creation_data})
        }

        if(this.props.read != undefined || this.props.read == true){
            var new_map_state = this.state.map_data
            new_map_state.party_location = {"x": x, "y": y}
            this.setState({map_data: new_map_state})
            this.update_data(new_map_state)
        }

    }

    start_creating = (type) => {
        this.setState({creating: type})
    }

    return_to_options = () => {
        this.setState({creating: null, creation_data: {}})
    }

    start_room = () => {
        var new_creation_data = {
            "points": []
        }
        this.setState({creation_data: new_creation_data})
    }

    finish_room = () => {
        var room_points = this.state.creation_data.points
        var room_data = {
            "id": uuidv4(),
            "description": "",
            "visibility": 1,
            "type": "ROOM",
            "points": room_points,
        }
        var new_map_state = this.state.map_data
        if(new_map_state.rooms == undefined){
            new_map_state.rooms = []
        }
        new_map_state.rooms.push(room_data)
        this.setState({map_data: new_map_state, creation_data: {}})
        this.update_data(new_map_state)
    }

    finish_doors = () => {
        if(this.state.creation_data.doors == undefined){
            return;
        }

        var new_map_state = this.state.map_data
        if(new_map_state.doors == undefined){
            new_map_state.doors = []
        }

        var doors = this.state.creation_data.doors
        for(var i = 0; i < doors.length; i++){
            var door_data = {
                "id": uuidv4(),
                "type": "DOOR",
                "visibility": 1,
                "x": doors[i].x,
                "y": doors[i].y,
            }
            new_map_state.doors.push(door_data)
        }
        
        this.setState({map_data: new_map_state, creation_data: {}})
        this.update_data(new_map_state)
    }

    finish_poi = () => {
        if(this.state.creation_data.pois == undefined){
            return;
        }

        var new_map_state = this.state.map_data
        if(new_map_state.pois == undefined){
            new_map_state.pois = []
        }

        var pois = this.state.creation_data.pois
        for(var i = 0; i < pois.length; i++){
            var poi_data = {
                "id": uuidv4(),
                "type": "POI",
                "visibility": 1,
                "description": "",
                "x": pois[i].x,
                "y": pois[i].y,
            }
            new_map_state.pois.push(poi_data)
        }
        
        this.setState({map_data: new_map_state, creation_data: {}})
        this.update_data(new_map_state)
    }

    finish_traps = () => {
        if(this.state.creation_data.traps == undefined){
            return;
        }

        var new_map_state = this.state.map_data
        if(new_map_state.traps == undefined){
            new_map_state.traps = []
        }

        var traps = this.state.creation_data.traps
        for(var i = 0; i < traps.length; i++){
            var trap_data = {
                "id": uuidv4(),
                "visibility": 1,
                "type": "TRAP",
                "description": "",
                "x": traps[i].x,
                "y": traps[i].y,
            }
            new_map_state.traps.push(trap_data)
        }
        
        this.setState({map_data: new_map_state, creation_data: {}})
        this.update_data(new_map_state)
    }

    alter_component_state = (type, id, attribute, value) => {
        if(type == "ROOM"){
            var new_rooms = []
            for(var i = 0; i < this.state.map_data.rooms.length; i++){
                var room = this.state.map_data.rooms[i]
                if(room.id == id){
                    room[attribute] = value
                }
                new_rooms.push(room)
            }
            var new_map_data = this.state.map_data
            new_map_data.rooms = new_rooms
            this.setState({map_data: new_map_data})
            this.update_data(new_map_data)
        }
        if(type == "DOOR"){
            var new_doors = []
            for(var i = 0; i < this.state.map_data.doors.length; i++){
                var door = this.state.map_data.doors[i]
                if(door.id == id){
                    door[attribute] = value
                }
                new_doors.push(door)
            }
            var new_map_data = this.state.map_data
            new_map_data.doors = new_doors
            this.setState({map_data: new_map_data})
            this.update_data(new_map_data)
        }
        if(type == "TRAP"){
            var new_traps = []
            for(var i = 0; i < this.state.map_data.traps.length; i++){
                var trap = this.state.map_data.traps[i]
                if(trap.id == id){
                    trap[attribute] = value
                }
                new_traps.push(trap)
            }
            var new_map_data = this.state.map_data
            new_map_data.traps = new_traps
            this.setState({map_data: new_map_data})
            this.update_data(new_map_data)
        }
        if(type == "POI"){
            var new_pois = []
            for(var i = 0; i < this.state.map_data.pois.length; i++){
                var poi = this.state.map_data.pois[i]
                if(poi.id == id){
                    poi[attribute] = value
                }
                new_pois.push(poi)
            }
            var new_map_data = this.state.map_data
            new_map_data.pois = new_pois
            this.setState({map_data: new_map_data})
            this.update_data(new_map_data)
        }
    }

    delete_component = (type, id) => {
        if(type == "ROOM"){
            var new_rooms = []
            for(var i = 0; i < this.state.map_data.rooms.length; i++){
                var room = this.state.map_data.rooms[i]
                if(room.id != id){
                    new_rooms.push(room)
                }
            }
            var new_map_data = this.state.map_data
            new_map_data.rooms = new_rooms
            this.setState({map_data: new_map_data})
            this.update_data(new_map_data)
        }
        if(type == "DOOR"){
            var new_doors = []
            for(var i = 0; i < this.state.map_data.doors.length; i++){
                var door = this.state.map_data.doors[i]
                if(door.id != id){
                    new_doors.push(door)
                }
            }
            var new_map_data = this.state.map_data
            new_map_data.doors = new_doors
            this.setState({map_data: new_map_data})
            this.update_data(new_map_data)
        }
        if(type == "TRAP"){
            var new_traps = []
            for(var i = 0; i < this.state.map_data.traps.length; i++){
                var trap = this.state.map_data.traps[i]
                if(trap.id != id){
                    new_traps.push(trap)
                }
            }
            var new_map_data = this.state.map_data
            new_map_data.traps = new_traps
            this.setState({map_data: new_map_data})
            this.update_data(new_map_data)
        }
        if(type == "POI"){
            var new_pois = []
            for(var i = 0; i < this.state.map_data.pois.length; i++){
                var poi = this.state.map_data.pois[i]
                if(poi.id != id){
                    new_pois.push(poi)
                }
            }
            var new_map_data = this.state.map_data
            new_map_data.pois = new_pois
            this.setState({map_data: new_map_data})
            this.update_data(new_map_data)
        }
    }

    get_image = (phases, current_phase) => {
        return phases[current_phase]
    }

    get_current_phase = (map_data) => {
        if (map_data.current_phase){
           return map_data.current_phase
        } else {
            return 0
        }
    }

    get_phases = (map_data, base_url) => {
        if (map_data.phases){
           return map_data.phases
        } else {
            return [base_url]
        }
    }

    add_phase = () => {
        var new_map_data = this.state.map_data
        
        if(new_map_data.phases == undefined){
            new_map_data.phases = [this.props.map.url, null]
            new_map_data.current_phase = 1
        } else {
            new_map_data.phases.push(null)
        }

        this.setState({map_data: new_map_data})
        this.update_data(new_map_data)
    }

    set_phase = (value) => {
        var new_map_data = this.state.map_data
        new_map_data.current_phase = value
        this.setState({map_data: new_map_data, image_load: false})
        this.update_data(new_map_data)
    }
    
            
    render(){

        var map_data = this.props.map_data != undefined ? this.props.map_data : this.state.map_data
        var phases = this.get_phases(map_data, this.props.map.url)
        var current_phase = this.get_current_phase(map_data)
        var image_url = this.get_image(phases, current_phase)

        var rooms = (map_data.rooms != undefined ? map_data.rooms : [])
        var doors = (map_data.doors != undefined ? map_data.doors : [])
        var pois = (map_data.pois != undefined ? map_data.pois : [])
        var traps = (map_data.traps != undefined ? map_data.traps : [])
        var party_location = (map_data.party_location != undefined ? map_data.party_location : null)
        var scale = (map_data.scale != undefined ? map_data.scale : 1)
        var pings = []
        for (const [key, value] of Object.entries(this.state.pings)) {
            pings.push(value)
        }
        var items = []
        items.push(...rooms)
        items.push(...doors)
        items.push(...pois)
        items.push(...traps)

        if(this.props.player_read == true){
            return (
                <div className="map player-read">
                    <div className="map-instructions">
                        <i>Click the map to ping it  <i className="fas fa-certificate"></i> | Hover over a POI <i className="fas fa-map-marker-alt"></i> to see it's name</i>
                    </div>
                    <div className="map-container">
                        {pois.map((poi) => {
                            if (poi.visibility == 1 && poi.highlighted == true){
                                return (
                                    <div className="poi-player-title">
                                        <p>{poi.description}</p>
                                    </div>
                                );
                            }
                        })}
                        <div className="image-container">
                            <img 
                                ref={this.imageRef}
                                className="mapping-image"
                                src={REACT_APP_API_URL + '/' + image_url}
                                onLoad={() => this.setState({image_load: true})}
                                crossorigin="anonymous"
                                style={this.state.image_load == false ? {display: 'none' } : {}}
                                />
                            {this.state.image_load == true && 
                                <Canvas 
                                    handle_canvas_click={this.handle_canvas_click}
                                    handle_canvas_move={this.handle_canvas_move}
                                    rooms = {rooms}
                                    doors = {doors}
                                    pois = {pois}
                                    traps = {traps}
                                    party_location = {party_location}
                                    scale = {scale}
                                    creation_data={null}
                                    background_image_ref={this.imageRef.current}
                                    post_map={null}
                                    confirm_post_map={null}
                                    player_read={true}
                                    pings={pings}
                                />
                            }
                        </div>
                    </div>
                </div>
            )
        }
        if(this.props.read == undefined || this.props.read == false){
            return(
                <div className="map">
                        <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_map_fuction}></i> {this.props.map.name}</h2>
                        {image_url != null ?
                            <div className="map-container">
                                <MapCreationTools 
                                    creating={this.state.creating}
                                    creation_data={this.state.creation_data}
                                    start_creating={this.start_creating}
                                    return_to_options={this.return_to_options}
                                    start_room={this.start_room}
                                    finish_room={this.finish_room}
                                    finish_doors={this.finish_doors}
                                    finish_poi={this.finish_poi}
                                    finish_traps={this.finish_traps}
                                />
                                <Form.Control type="range" value={scale*10} min={0} max={100} onChange={this.update_scale}/>
                                <div className="image-container">
                                    <img 
                                        ref={this.imageRef}
                                        className="mapping-image"
                                        src={REACT_APP_API_URL + '/' + image_url}
                                        onLoad={() => this.setState({image_load: true})}
                                        crossorigin="anonymous"
                                        />
                                    {this.state.image_load == true && 
                                        <Canvas 
                                            handle_canvas_click={this.handle_canvas_click}
                                            handle_canvas_move={this.handle_canvas_move}
                                            rooms = {rooms}
                                            doors = {doors}
                                            pois = {pois}
                                            traps = {traps}
                                            party_location = {party_location}
                                            scale = {scale}
                                            creation_data={this.state.creation_data}
                                            background_image_ref={this.imageRef.current}
                                            post_map={this.state.post_map}
                                            confirm_post_map={this.confirm_post_map}
                                            pings={[]}
                                        />
                                    }
                                </div>
                                {this.props.map.url != null &&
                                    <PhaseTabs 
                                        phases={phases}
                                        current_phase={current_phase}
                                        show_add={true}
                                        add_phase_function={this.add_phase}
                                        set_phase_function={this.set_phase}
                                    />
                                }
                                {items.length > 0 &&
                                    <ListMapComponents read={false} items={items} alter_component_state={this.alter_component_state} delete_component={this.delete_component} />
                                }
                            </div>
                            :
                            <div>
                                <p>Upload Map</p>
                                <UploadImage callback={(url) => {this.update_image(url, current_phase)}} exclude_api_url={true} />
                                <hr/>
                                {this.props.map.url != null &&
                                    <PhaseTabs 
                                        phases={phases}
                                        current_phase={current_phase}
                                        show_add={true}
                                        add_phase_function={this.add_phase}
                                        set_phase_function={this.set_phase}
                                    />
                                }
                            </div>
                        }
                        
                </div>
            )
        } else {
            return (
                <div className="map">
                        {image_url != null ?
                            <div className="map-container">
                                <Row>
                                    <Col xs={4} md={4}>
                                        {this.props.map.url != null && phases.length > 1 &&
                                            <PhaseTabs 
                                                phases={phases}
                                                current_phase={current_phase}
                                                show_add={false}
                                                set_phase_function={this.set_phase}
                                            />
                                        }
                                        {items.length > 0 &&
                                            <ListMapComponents read={true} items={items} alter_component_state={this.alter_component_state} override_function={this.props.override_function}/>
                                        }
                                    </Col>
                                    <Col xs={8} md={8}>
                                        <Form.Control type="range" value={scale*10} min={0} max={100} onChange={this.update_scale}/>
                                        <div className="image-container">
                                            <img 
                                                ref={this.imageRef}
                                                className="mapping-image"
                                                src={REACT_APP_API_URL + '/' + image_url}
                                                onLoad={() => this.setState({image_load: true})}
                                                crossorigin="anonymous"
                                                />
                                            {this.state.image_load == true && 
                                            
                                                <Canvas 
                                                    handle_canvas_click={this.handle_canvas_click}
                                                    handle_canvas_move={this.handle_canvas_move}
                                                    rooms = {rooms}
                                                    doors = {doors}
                                                    pois = {pois}
                                                    traps = {traps}
                                                    party_location = {party_location}
                                                    scale = {scale}
                                                    creation_data={this.state.creation_data}
                                                    background_image_ref={this.imageRef.current}
                                                    post_map={this.state.post_map}
                                                    confirm_post_map={this.confirm_post_map}
                                                    pings={pings}
                                                />
                                            }
                                        </div>
                                        {this.state.image_load == true && 
                                            <div className={"map-share" + (this.state.share == true ? " sharing" : "")} onClick={() => this.toggle_share_map()}>
                                                {this.state.share == true ?
                                                    "Sharing"
                                                    :
                                                    "Click to Share"
                                                }
                                            </div>
                                        }
                                    </Col>
                                </Row>
                            </div>
                            :
                            <div>
                                <p>No Map Uploaded</p>
                            </div>
                        }
                        
                </div>
            )
        }

    };
}

export default SingleMap;