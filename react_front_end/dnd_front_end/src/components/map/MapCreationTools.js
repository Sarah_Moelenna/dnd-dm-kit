import React, { useState } from 'react';

const MapCreationTools = ({creating, creation_data, start_creating, return_to_options, start_room, finish_room, finish_doors, finish_poi, finish_traps}) => {

    if (creating == null){
        return (<div className="creation-options">
            <h2>Creation Tools</h2>
            <div className="clickable-div" onClick={() => start_creating("ROOM")}>
                <i className="far fa-square"></i>
                <p>Room</p>
            </div>
            <div className="clickable-div" onClick={() => start_creating("DOOR")}>
                <i className="fas fa-dungeon"></i>
                <p>Door</p>
            </div>
            <div className="clickable-div" onClick={() => start_creating("POI")}>
                <i className="fas fa-map-marker-alt"></i>
                <p>POI</p>
            </div>
            <div className="clickable-div" onClick={() => start_creating("TRAP")}>
                <i className="fas fa-skull-crossbones"></i>
                <p>Traps</p>
            </div>
        </div>)
    }
    if(creating == "ROOM"){
        return(<div className="creation-options">
            <h2>Room Creation</h2>
            {creation_data.points == undefined ?
                <button className="btn btn-primary" onClick={() => start_room()}>Start Room</button>
                :
                <button className="btn btn-primary" onClick={() => finish_room()}>Commit Room</button>
            }
            <button className="btn btn-primary" onClick={() => return_to_options()}>Return to Creation Options</button>
        </div>)
    }
    if(creating == "DOOR"){
        return(<div className="creation-options">
            <h2>Door Creation</h2>
            <button className="btn btn-primary" onClick={() => finish_doors()}>Commit Doors</button>
            <button className="btn btn-primary" onClick={() => return_to_options()}>Return to Creation Options</button>
        </div>)
    }
    if(creating == "POI"){
        return(<div className="creation-options">
            <h2>POI Creation</h2>
            <button className="btn btn-primary" onClick={() => finish_poi()}>Commit POIs</button>
            <button className="btn btn-primary" onClick={() => return_to_options()}>Return to Creation Options</button>
        </div>)
    }
    if(creating == "TRAP"){
        return(<div className="creation-options">
            <h2>Trap Creation</h2>
            <button className="btn btn-primary" onClick={() => finish_traps()}>Commit Traps</button>
            <button className="btn btn-primary" onClick={() => return_to_options()}>Return to Creation Options</button>
        </div>)
    }
}

export default MapCreationTools;