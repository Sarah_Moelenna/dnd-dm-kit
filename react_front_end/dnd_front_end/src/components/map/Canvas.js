import React, { Component } from 'react';
import { DND_POST, DND_FORM } from '.././shared/DNDRequests';

class Canvas extends Component {

    constructor(props) {
        super(props);
        this.canvasRef = React.createRef();
        this.doorRef = React.createRef();
        this.trapRef = React.createRef();
        this.poiRef = React.createRef();
    }
    
    componentDidMount() {
        window.addEventListener("mousemove", this.logMousePosition);
        this.draw()
    }

    componentDidUpdate() {
        this.draw()
    }

    logMousePosition = e => {
        if(this.canvasRef.current == null){
            return
        }

        var canvas_bounding_data = this.canvasRef.current.getBoundingClientRect()
        var mouse_x = e.clientX
        var mouse_y = e.clientY
        var canvas_x = canvas_bounding_data.x
        var canvas_y = canvas_bounding_data.y
        var canvas_width = canvas_bounding_data.width
        var canvas_height = canvas_bounding_data.height
        var canvas_click_x = mouse_x - canvas_x
        var canvas_click_Y = mouse_y - canvas_y
        var canvas_click_x_percentage = (canvas_click_x/canvas_width) * 100
        var canvas_click_y_percentage = (canvas_click_Y/canvas_height) * 100
        if(canvas_click_x_percentage >= 0 && canvas_click_x_percentage <= 100 && canvas_click_y_percentage >= 0 && canvas_click_y_percentage <= 100){
            this.props.handle_canvas_move(canvas_click_x_percentage, canvas_click_y_percentage)
        }
        e.preventDefault();
    };


    draw = () => {
        const canvas = this.canvasRef.current
        const context = canvas.getContext('2d')
        context.clearRect(0, 0, canvas.width, canvas.height);

        context.drawImage(this.props.background_image_ref, 0, 0, canvas.width, canvas.height);
        
        //commited rooms
        this.draw_rooms(canvas, context, this.props.rooms)
        //uncommited rooms
        if(this.props.creation_data != undefined && this.props.creation_data.points != undefined){
            this.draw_points(canvas, context, this.props.creation_data.points, '#00BFFF', true)
        }
        //uncommited doors
        if(this.props.creation_data != undefined && this.props.creation_data.doors != undefined){
            for(var i = 0; i < this.props.creation_data.doors.length; i++){
                this.draw_door(context, this.props.creation_data.doors[i].x, this.props.creation_data.doors[i].y, '#00BFFF')
            }
        }
        //commited doors
        for(var i = 0; i < this.props.doors.length; i++){
            if(this.props.doors[i].visibility == 1){
                this.draw_door(context, this.props.doors[i].x, this.props.doors[i].y, '#FFFFFF', '#B22222')
            }
        }

        //uncommited pois
        if( this.props.creation_data != undefined && this.props.creation_data.pois != undefined){
            for(var i = 0; i < this.props.creation_data.pois.length; i++){
                this.draw_poi(context, this.props.creation_data.pois[i].x, this.props.creation_data.pois[i].y, '#00BFFF')
            }
        }
        //commited pois
        for(var i = 0; i < this.props.pois.length; i++){
            if(this.props.pois[i].visibility == 1){
                this.draw_poi(context, this.props.pois[i].x, this.props.pois[i].y, '#FFFFFF', '#B22222')
            }
        }

        //uncommited traps
        if(this.props.creation_data != undefined &&  this.props.creation_data.traps != undefined){
            for(var i = 0; i < this.props.creation_data.traps.length; i++){
                this.draw_trap(context, this.props.creation_data.traps[i].x, this.props.creation_data.traps[i].y, '#00BFFF')
            }
        }
        //commited traps
        for(var i = 0; i < this.props.traps.length; i++){
            if(this.props.traps[i].visibility == 1){
                this.draw_trap(context, this.props.traps[i].x, this.props.traps[i].y, '#FFFFFF', '#B22222')
            }
        }

        if(this.props.party_location != null){
            this.draw_party(context, this.props.party_location.x, this.props.party_location.y, '#FFFFFF', '#B22222')
        }

        // pings
        for(var i = 0; i < this.props.pings.length; i++){
            this.draw_ping(context, this.props.pings[i].x, this.props.pings[i].y, '#FF6347', '#000000')
        }

        if(this.props.post_map == true){

            function upload_image(url){
                var data = {
                    "filepath": url,
                    "location": "ROLLING",
                    "auto_delete": true
                }
                var body = {
                    "command_code": "CMD_UPLOAD_IMG",
                    "data": data
                }

                DND_POST(
                    '/commands',
                    body,
                    null,
                    null
                )

            }
            
            function post_blob(blob){
                const formData = new FormData()
                formData.append('file', blob, "canvas-share.jpg")

                DND_FORM(
                    '/file-upload',
                    formData,
                    (data) => {
                        upload_image(data['file'])
                    },
                    null
                )
            }

            canvas.toBlob(post_blob, "image/jpeg");
            this.props.confirm_post_map()
        } else {
            if (this.props.player_read != true){
                //highlighted rooms
                this.highlight_rooms(canvas, context, this.props.rooms)
                //highlighted doors
                for(var i = 0; i < this.props.doors.length; i++){
                    if(this.props.doors[i].highlighted == true){
                        this.draw_door(context, this.props.doors[i].x, this.props.doors[i].y, '#FF6347')
                    }
                }
                //highlighted traps
                for(var i = 0; i < this.props.traps.length; i++){
                    if(this.props.traps[i].highlighted == 1){
                        this.draw_trap(context, this.props.traps[i].x, this.props.traps[i].y, '#FF6347')
                    }
                }
                //highlighted pois
                for(var i = 0; i < this.props.pois.length; i++){
                    if(this.props.pois[i].highlighted == 1){
                        this.draw_poi(context, this.props.pois[i].x, this.props.pois[i].y, '#FF6347')
                    }
                }
            } else {
                //highlighted pois
                for(var i = 0; i < this.props.pois.length; i++){
                    if(this.props.pois[i].highlighted == 1 && this.props.pois[i].visibility == 1){
                        this.draw_poi(context, this.props.pois[i].x, this.props.pois[i].y, '#FF6347')
                    }
                }
            }
        }

        
    }

    draw_rooms = (canvas, context, rooms) => {
        for(var j = 0; j < rooms.length; j++){
            if(rooms[j].visibility == 1){
                this.draw_points(canvas, context, rooms[j].points, '#000000', true)
            }
        }
    }

    highlight_rooms = (canvas, context, rooms) => {
        for(var j = 0; j < rooms.length; j++){
            if(rooms[j].highlighted == true){
                this.draw_points(canvas, context, rooms[j].points, '#FF6347', false)
            }
        }
    }

    draw_door = (context, x, y, color, stroke_color) => {
        var center_x = Math.round(context.canvas.width * x / 100)
        var center_y = Math.round(context.canvas.height * y / 100)
        var font_size = (this.props.background_image_ref.width * 0.02 * this.props.scale)
        context.fillStyle = color
        context.font = '900 ' + font_size + 'px "Font Awesome 5 Free"'
        context.fillText('\uf6d9', center_x-(font_size*0.4), center_y+(font_size*0.4));
        if (stroke_color != undefined){
            context.strokeStyle = stroke_color
            context.lineWidth = 1;
            context.strokeText('\uf6d9', center_x-(font_size*0.4), center_y+(font_size*0.4));
        }
    }

    draw_party = (context, x, y, color, stroke_color) => {
        var center_x = Math.round(context.canvas.width * x / 100)
        var center_y = Math.round(context.canvas.height * y / 100)
        var font_size = (this.props.background_image_ref.width * 0.02 * this.props.scale)
        context.fillStyle = color
        context.font = '900 ' + font_size + 'px "Font Awesome 5 Free"'
        context.fillText('\uf0c0', center_x-(font_size*0.5), center_y+(font_size*0.5));
        if (stroke_color != undefined){
            context.strokeStyle = stroke_color
            context.lineWidth = 2;
            context.strokeText('\uf0c0', center_x-(font_size*0.5), center_y+(font_size*0.5));
        }
    }

    draw_ping = (context, x, y, color, stroke_color) => {
        var center_x = Math.round(context.canvas.width * x / 100)
        var center_y = Math.round(context.canvas.height * y / 100)
        var font_size = (this.props.background_image_ref.width * 0.02 * this.props.scale)
        context.fillStyle = color
        context.font = '900 ' + font_size + 'px "Font Awesome 5 Free"'
        context.fillText('\uf0a3', center_x-(font_size*0.5), center_y+(font_size*0.5));
        if (stroke_color != undefined){
            context.strokeStyle = stroke_color
            context.lineWidth = 2;
            context.strokeText('\uf0a3', center_x-(font_size*0.5), center_y+(font_size*0.5));
        }
    }

    draw_poi = (context, x, y, color, stroke_color) => {
        var center_x = Math.round(context.canvas.width * x / 100)
        var center_y = Math.round(context.canvas.height * y / 100)
        var font_size = (this.props.background_image_ref.width * 0.02 * this.props.scale)
        context.fillStyle = color
        context.font = '900 ' + font_size + 'px "Font Awesome 5 Free"'
        context.fillText('\uf3c5', center_x-(font_size*0.4), center_y+(font_size*0.4));
        if (stroke_color != undefined){
            context.strokeStyle = stroke_color
            context.lineWidth = 2;
            context.strokeText('\uf3c5', center_x-(font_size*0.4), center_y+(font_size*0.4));
        }
    }

    draw_trap = (context, x, y, color, stroke_color) => {
        var center_x = Math.round(context.canvas.width * x / 100)
        var center_y = Math.round(context.canvas.height * y / 100)
        var font_size = (this.props.background_image_ref.width * 0.02 * this.props.scale)
        context.fillStyle = color
        context.font = '900 ' + font_size + 'px "Font Awesome 5 Free"'
        context.fillText('\uf714', center_x-(font_size*0.4), center_y+(font_size*0.4));
        if (stroke_color != undefined){
            context.strokeStyle = stroke_color
            context.lineWidth = 2;
            context.strokeText('\uf714', center_x-(font_size*0.4), center_y+(font_size*0.4));
        }
    }

    draw_points = (canvas, context, points, colour, fill) => {
        
        context.fillStyle = colour
        context.strokeStyle = colour
        //context.fillRect(0, 0, context.canvas.width/2, context.canvas.height/2)
        
        context.beginPath()
        for(var i = 0; i < points.length; i++){
            var center_x = Math.round(context.canvas.width * points[i].x / 100)
            var center_y = Math.round(context.canvas.height * points[i].y / 100)
            if(i == 0){
                context.moveTo(center_x, center_y);
            } else {
                context.lineTo(center_x, center_y);
            }
        }
        
        if(fill == true){
            context.lineWidth = 1;
            context.stroke()
            context.fill()
        } else {
            context.lineTo( Math.round(context.canvas.width * points[0].x / 100), Math.round(context.canvas.height * points[0].y / 100));
            context.lineTo( Math.round(context.canvas.width * points[1].x / 100), Math.round(context.canvas.height * points[1].y / 100));
            context.lineWidth = 5;
            context.stroke()
        }
        context.closePath()

    }

    handle_click = (e) => {
        var canvas_bounding_data = this.canvasRef.current.getBoundingClientRect()
        var mouse_x = e.clientX
        var mouse_y = e.clientY
        var canvas_x = canvas_bounding_data.x
        var canvas_y = canvas_bounding_data.y
        var canvas_width = canvas_bounding_data.width
        var canvas_height = canvas_bounding_data.height
        var canvas_click_x = mouse_x - canvas_x
        var canvas_click_Y = mouse_y - canvas_y
        var canvas_click_x_percentage = (canvas_click_x/canvas_width) * 100
        var canvas_click_y_percentage = (canvas_click_Y/canvas_height) * 100
        this.props.handle_canvas_click(canvas_click_x_percentage, canvas_click_y_percentage)
        e.preventDefault();
    }
    
    render(){
        return (
            <canvas ref={this.canvasRef} onClick={this.handle_click} width={this.props.background_image_ref.width} height={this.props.background_image_ref.height}/>
        )
    }
  }

export default Canvas;