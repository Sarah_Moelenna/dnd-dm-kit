import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from '.././shared/Spinner'
import { DND_POST } from '.././shared/DNDRequests';

const test_regex_one = new RegExp('^https:\\/\\/www\\.dndbeyond\\.com\\/profile\\/(\\w*)\\/characters\\/(\\d*)');
const test_regex_two = new RegExp('https:\\/\\/www\\.dndbeyond\\.com\\/characters\\/(\\d*)');

class CreateCharacter extends Component {

    

    state = {
        url: null,
        error: null,
        processing: false,
    }

    handleSubmit = (event) => {
        
        if( this.state.url == null){
            event.preventDefault();
            return
        }

        var test_one = test_regex_one.test(this.state.url)
        var test_two = test_regex_two.test(this.state.url)
        if( test_one == false && test_two == false){
            event.preventDefault();
            return
        }

        var data = {
            url: this.state.url,
            campaign_id: this.props.campaign_id
        }

        this.setState({ processing: true })

        DND_POST(
            '/character',
            data,
            (response) => {
                this.props.refresh_function()
                this.setState({ url: null, processing: false })
            },
            null
        )
        
        event.preventDefault();
      
    }

    handleChange = e => {
        var error = null

        var test_one = test_regex_one.test(e.target.value)
        var test_two = test_regex_two.test(e.target.value)
        if( test_one == false && test_two == false){
            error = 'Invalid URL Format'
        }

        this.setState({ [e.target.name]: e.target.value , error: error});
    };


    render(){
        if(this.state.processing === true) {
            return (
                <Spinner/>
            )
        }
        return(
            <Form onSubmit={this.handleSubmit} className="create-campaign">
                <Row>

                    <Col xs={6} md={6}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Character Sheet URL</Form.Label>
                            <Form.Control required value={this.state.url} name="url" type="text" placeholder="Enter URL" onChange={this.handleChange} />
                            <Form.Text className="text-muted">
                                {this.state.error}
                            </Form.Text>
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default CreateCharacter;