import React, { Component } from 'react';
import APIButton from '.././shared/APIButton';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Collapsible from '../shared/Collapsible';
import MonsterInfo from '../monster/MonsterInfo';
import { DND_PUT } from '.././shared/DNDRequests';

class SingleAlly extends Component {

    state = {
        max_health: 0,
        damage_taken: 0,
    }

    constructor(props) {
        super(props);
        this.state = {
            max_health: this.props.ally.max_health,
            damage_taken: this.props.ally.damage_taken,
        };
    }

    goto_monster = (monster_id) => {
        var override = {
            "page": "MONSTER",
            "monster_focus": monster_id
        }
        this.props.override_function(override)
    }

    handleChange = (e) => {

        if (e.target.name == "max_health"){
            this.update_ally(this.props.ally.id, null, null, e.target.value, null)
        } else if (e.target.name == "damage_taken"){
            this.update_ally(this.props.ally.id, null, null, null, e.target.value)
        }
        this.setState({ [e.target.name]: e.target.value });
    };

    update_ally = (ally_id, display_name, tracking, max_health, damage_taken) => {
        var data={
            "tracking": tracking,
            "display_name": display_name,
            "max_health": max_health,
            "damage_taken": damage_taken
        }

        DND_PUT(
            '/ally/' + ally_id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )

    }

    render(){
        return (
            <div key={this.props.ally.id} className="ally-item">
                <Row>
                    <Col xs={1} md={1}>
                        <img src={this.props.ally.monster.image}/>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text">{this.props.ally.display_name}</p>
                    </Col>
                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicName">
                            <Form.Control value={this.state.damage_taken} name="damage_taken" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>
                    <Col xs={2} md={2}>
                        <Form.Control value={this.state.max_health} name="max_health" type="number" min={0} onChange={this.handleChange} />
                    </Col>
                    <Col xs={2} md={2} className="cenetered-column">
                        {
                            this.props.ally.tracking == true ?
                            <i className="fas fa-check-circle clickable-icon" onClick={() => {this.update_ally(this.props.ally.id, this.props.ally.display_name, false, this.props.ally.max_health, this.props.ally.damage_taken)}}></i>
                            :
                            <i className="fas fa-times-circle clickable-icon" onClick={() => {this.update_ally(this.props.ally.id, this.props.ally.display_name, true, this.props.ally.max_health, this.props.ally.damage_taken)}}></i>
                        }
                    </Col>
                    <Col xs={1} md={1}>
                        <APIButton api_endpoint={"/ally/" + this.props.ally.id} method="DELETE" icon="fas fa-trash" post_response_function={this.props.refresh_function}></APIButton>
                    </Col>
                </Row>
                <div className="monster">
                    <Collapsible contents={
                        <MonsterInfo
                            monster={this.props.ally.monster}
                            show_roll_buttons={true}
                            show_more_info={true}
                            override_name={this.props.ally.display_name}
                            override_roll_type="ALLY_ROLL"
                            roll_dice_function={this.props.roll_dice_function}
                        />}
                        title="More Info"
                    />
                </div>
                <hr/>
            </div>
        );
    }
}

export default SingleAlly;