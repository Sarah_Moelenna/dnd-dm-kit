import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import FilterCampaign from './FilterCampign';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const ListCampaigns = ({ campaigns, set_sort, current_sort_by, current_sort_value, select_function, disabled_ids}) => {

    function get_sort(sort_by){
        if(current_sort_by == sort_by){
            if(current_sort_value == "DESC"){
                return (
                    <div className="clickable-div" onClick={() => {set_sort(null, null)}}>
                        <i className="fas fa-sort-down"></i>
                    </div>
                ) 
            } else {
                return (
                    <div className="clickable-div" onClick={() => {set_sort(sort_by, "DESC")}}>
                        <i className="fas fa-sort-up"></i>
                    </div>
                ) 
            }
        } else {
            return (
                <div className="clickable-div" onClick={() => {set_sort(sort_by, "ASC")}}>
                    <i className="fas fa-sort"></i>
                </div>
            )
        }
    }

    return (
        <div className="campaign-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={3} md={3}>
                    </Col>
                </Row>
            </div>
            {campaigns.map((campaign) => (
                <div key={campaign.id} className={disabled_ids.includes(campaign.id) ? "campaign-item disabled" : "campaign-item"}>
                    <Row>
                    <Col xs={3} md={3}>
                            <p className="card-text">{campaign.name}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            {!disabled_ids.includes(campaign.id) &&
                                <Button onClick={() => {select_function(campaign.id, campaign.name, campaign.dexterity, campaign.xp, campaign.image)}}>Select Campaign</Button>
                            }
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

class CampaignSelect extends Component {

    state = {
        campaigns: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false
    }

    componentDidMount() {
        this.refresh_campaigns(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    select_campaign = (campaign) => {

        this.setState({toggle: false})
        this.props.callback(campaign)
    }

    refresh_campaigns = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        params['results_per_page'] = 10
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/campaign?' + stringify(params),
          (jsondata) => {
            this.setState({ campaigns: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_campaigns(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
        this.setState(
          {sort_by: sort_by, sort_value: sort_value},
          this.refresh_campaigns(this.state.page, this.state.filters, sort_by, sort_value)
        )
      };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_campaigns(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_campaigns(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_campaigns(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id, name, dexterity, xp, image) => {

        this.props.select_function(id, name, dexterity, xp, image)
        this.setState({toggle: false})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>{this.props.override_button != undefined ? this.props.override_button : "Choose Campaign"}</Button>
                }
                <Modal show={this.state.toggle} size="md" className="campaign-select-modal select-modal">
                        <Modal.Header>Campaigns</Modal.Header>
                        <Modal.Body>
                            <FilterCampaign filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                            <ListCampaigns
                                campaigns={this.state.campaigns}
                                set_sort={this.set_sort}
                                current_sort_by={this.state.sort_by}
                                current_sort_value={this.state.sort_value}
                                select_function={this.select_function}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default CampaignSelect;