import React from 'react';
import APIButton from '.././shared/APIButton';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { DND_PUT } from '.././shared/DNDRequests';

const ListCollections = ({ collections, campaign_id, refresh_function, override_function}) => {

    function goto_collection(collection_id){
        var override = {
            "page": "NOTES",
            "collection_focus": collection_id
        }
        override_function(override)
    }

    function handleOnDragEnd(result){
        if (!result.destination) return;

        const items = Array.from(collections);
        const [reorderedItem] = items.splice(result.source.index, 1);
        items.splice(result.destination.index, 0, reorderedItem);

        var collection_orders = {}
        for (var i = 0; i < items.length; i++){
            collection_orders[i] = items[i].id
        }

        var data = {
            "order": collection_orders
        }

        DND_PUT(
            '/campaign/' + campaign_id + '/collection',
            data,
            (response) => {
                refresh_function()
            },
            null
        )
        
    }

    function shareCollection(id, value){
        var data = {
            "share": value,
            "collection_id": id
        }
        DND_PUT(
            '/campaign/' + campaign_id + '/collection',
            data,
            (response) => {
                refresh_function()
            },
            null
        )
        
    }

    if(collections === undefined || collections.length == 0) {
        return (
            <p>No Collections related to this campaign.</p>
        )
    }
    return (
        <div className="collections-items">
            <hr></hr>
            <DragDropContext onDragEnd={handleOnDragEnd}>
                <Droppable droppableId="collections">
                    {(provided) => (
                        <ul className="collections-list" {...provided.droppableProps} ref={provided.innerRef}>
                            {collections.map((collection, index) => (
                                <Draggable key={collection.id} draggableId={collection.id} index={index}>
                                    {(provided) => (
                                        <li ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                            <Row>
                                                <Col xs={3} md={3}>
                                                    <p className="card-text">{collection.name}</p>
                                                </Col><Col xs={5} md={5} className="cenetered-column">
                                                    {
                                                        collection.sharing == false ?
                                                        <div className="collection-share" onClick={() => {shareCollection(collection.id, true)}}>Share Collection</div>
                                                        :
                                                        <div className="collection-share sharing" onClick={() => {shareCollection(collection.id, false)}}>Stop Sharing Collection</div>
                                                    }
                                                </Col>
                                                <Col xs={3} md={3} className="cenetered-column">
                                                    <a className="btn btn-primary" onClick={() => {goto_collection(collection.id)}}>Read Collection</a>
                                                </Col>
                                                <Col xs={1} md={1}>
                                                    <APIButton api_endpoint={"/campaign/" + campaign_id + "/collection"} data={{"collection_id": collection.id}} method="DELETE" icon="fas fa-trash" post_response_function={refresh_function}></APIButton>
                                                </Col>
                                            </Row>
                                            <hr/>
                                        </li>
                                    )}
                                </Draggable>
                            ))}
                        {provided.placeholder}
                        </ul>
                    )}
                </Droppable>
            </DragDropContext>
        </div>
    );
}

export default ListCollections;