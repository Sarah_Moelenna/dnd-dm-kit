import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ToggleButton from 'react-bootstrap/ToggleButton';
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup';
import { DND_PUT } from '.././shared/DNDRequests';

class CampaignEdit extends Component {

    state = {
        notes: "",
    }

    constructor(props) {
        super(props);
        this.state = {
            notes: this.props.campaign.notes
        };
    }

    handleChange = e => {

        var data = {
            [e.target.name]: e.target.value,
        }
        
        this.update_campaign(data)

    };

    handleChangeNoRefresh = e => {

        var data = {
            [e.target.name]: e.target.value,
        }
        
        this.update_campaign(data, false)
        this.setState({[e.target.name]: e.target.value})

        e.preventDefault();
    };

    update_campaign = (data, with_refresh) => {

        DND_PUT(
            '/campaign/' + this.props.campaign.id,
            data,
            (response) => {
                if(with_refresh == undefined || with_refresh == true){
                    this.props.refresh_function()
                }
            },
            null
        )

    }


    render(){

        return(
            <div className="campaign-edit">
                <Row>
                    <Col xs={12} md={12}>
                    <Form.Group controlId="Campaign Notes">
                        <Form.Label>Campaign Notes</Form.Label>
                        <Form.Control name="notes" as="textarea" rows={7} value={this.state.notes} onChange={this.handleChangeNoRefresh}/>
                    </Form.Group>
                    </Col>
                </Row>
                <hr/>
            </div>
        );
    };
}

export default CampaignEdit;