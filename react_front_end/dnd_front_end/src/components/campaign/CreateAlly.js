import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MonsterSelect from '../monster/MonsterSelect'

class CreateAlly extends Component {

    state = {
        display_name: "",
        max_health: 0,
        monster_id: null,
        monster_name: null
    }

    handleSubmit = (event) => {
        
        if( this.state.display_name == '' || this.state.max_health < 1 || this.state.monster_id == null){
            event.preventDefault();
            return
        }

        this.props.add_function(this.state.monster_id, this.state.display_name, this.state.max_health)
        this.setState({ display_name: "",max_health: 0,monster_id: null,monster_name: null});
        
        event.preventDefault();
      
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value});
    };

    select_monster = (id, name) => {
        this.setState({monster_id: id, monster_name: name})
    }


    render(){
        return(
            <Form onSubmit={this.handleSubmit} className="create-ally">
                <Row>
                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Monster</Form.Label>
                            { this.state.monster_id != null &&
                                <p>{this.state.monster_name}</p>
                            }
                            <MonsterSelect select_function={this.select_monster} disabled_ids={[this.state.monster_id]}/>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Display Name</Form.Label>
                            <Form.Control required value={this.state.display_name} name="display_name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Max Health</Form.Label>
                            <Form.Control required value={this.state.max_health} name="max_health" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default CreateAlly;