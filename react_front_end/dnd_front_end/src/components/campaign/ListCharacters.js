import React from 'react';
import APIButton from '.././shared/APIButton';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';;

const ListCharacters = ({ characters, refresh_function}) => {
    if(characters === undefined) {
        return
    }
    return (
        <div className="character-items">
            <hr></hr>
            <Row>
                <Col xs={1} md={1}>

                </Col>
                <Col xs={2} md={2}>
                    <p className="card-text"><b>Name</b></p>
                </Col>
                <Col xs={2} md={2} className="cenetered-column">
                    <p className="card-text"><b>Is Tracking</b></p>
                </Col>
                <Col xs={2} md={2}>
                    
                </Col>
                <Col xs={1} md={1}>
                    <p className="card-text"><b>Controls</b></p>
                </Col>
            </Row>
            {characters.map((character) => (
                <div key={character.id} className="character-item">
                    <Row>
                        <Col xs={1} md={1}>
                            <img src={character.avatar}/>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{character.name}</p>
                        </Col>
                        <Col xs={2} md={2} className="cenetered-column">
                            <APIButton data={{"tracking": !character.tracking}} api_endpoint={"/character/" + character.id + "/tracking"} method="PUT" icon={character.tracking ? "fas fa-check-circle" : "fas fa-times-circle"} post_response_function={refresh_function}></APIButton>
                        </Col>
                        <Col xs={2} md={2}>
                            <a className="btn btn-primary" href={character.url}>View Character Sheet</a>
                        </Col>
                        <Col xs={1} md={1}>
                            <APIButton api_endpoint={"/character/" + character.id} method="DELETE" icon="fas fa-trash" post_response_function={refresh_function}></APIButton>
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListCharacters;