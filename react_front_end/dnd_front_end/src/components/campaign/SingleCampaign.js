import React, { Component } from 'react';
import ListCharacters from './ListCharacters';
import Collapsible from '../shared/Collapsible';
import CreateCharacter from './CreateCharacter';
import CampaignEdit from './CampaignEdit';
import ListCollections from './ListCollections';
import ListAllies from './ListAllies';
import { DND_POST } from '.././shared/DNDRequests';
import SingleSession from './SingleSession';
import CreateAlly from './CreateAlly'
import CollectionSelect from '../notes/CollectionSelect';

class SingleCampaign extends Component {

    add_collection = (collection_id) => {

        var data={
            "collection_id": collection_id
        }

        DND_POST(
            '/campaign/' + this.props.campaign.id + '/collection',
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
        
    }

    create_session = () => {
        DND_POST(
            '/campaign/' + this.props.campaign.id + '/session',
            null,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    }

    add_ally = (monster_id, display_name, max_health) => {

        var data={
            "monster_id": monster_id,
            "display_name": display_name,
            "max_health": max_health,
            "campaign_id": this.props.campaign.id
        }

        DND_POST(
            '/ally',
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
        
    }

    get_collection_ids = () => {
        var ids = []
        for(var i = 0; i < this.props.campaign.collections.length; i++){
            ids.push(this.props.campaign.collections[i].id)
        }
        return ids
    }
            

    render(){

        return(
            <div>
                <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_campaign_fuction}></i> {this.props.campaign.name}</h2>
                <CampaignEdit campaign={this.props.campaign} refresh_function={this.props.refresh_function}/>
                <h3>Session</h3>
                <SingleSession can_create_session={this.props.campaign.can_create_session} session={this.props.campaign.session} create_session={this.create_session} refresh_function={this.props.refresh_function}/>
                <hr/>
                <h3>Characters</h3>
                <Collapsible contents={<CreateCharacter refresh_function={this.props.refresh_function} campaign_id={this.props.campaign.id}/>} title="Create New Character"/>
                <ListCharacters characters={this.props.campaign.characters} refresh_function={this.props.refresh_function}/>
                <h3>Allies</h3>
                <Collapsible contents={<CreateAlly add_function={this.add_ally}/>} title="Add an Ally"/>
                <ListAllies allies={this.props.campaign.allies} refresh_function={this.props.refresh_function} override_function={this.props.override_function} roll_dice_function={this.props.roll_dice_function}/>
                <h3>Related Collections</h3>
                <CollectionSelect select_function={this.add_collection} override_button="Add Collection to Campaign" disabled_ids={this.get_collection_ids()}/>
                <ListCollections refresh_function={this.props.refresh_function} campaign_id={this.props.campaign.id} override_function={this.props.override_function} collections={this.props.campaign.collections}/>
            </div>
        )

    };
}

export default SingleCampaign;