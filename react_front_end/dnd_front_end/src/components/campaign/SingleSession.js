import React, {useContext} from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Button } from 'reactstrap';
import { DND_DELETE } from '../shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import {SocketContext} from '.././socket/Socket';

const SingleSession = ({ can_create_session, session, create_session, refresh_function}) => {

    const socket = useContext(SocketContext);

    const end_session = (id) => {
        DND_DELETE(
            "/session/" + id,
            null,
            (response) => {
                socket.emit("session_end");
                refresh_function();
            },
            null
        )
    }

    return (
        <div>
            {
                session == null ?
                <div>
                    <p>No session for this campaign.</p>
                    {can_create_session == true ?
                        <Button className="btn btn-primary" onClick={() => {create_session()}}>Start a Session</Button>
                        :
                        <div>
                            <p><b>End other current session to start a new one!</b></p>
                            <Button disabled="disabled" className="btn btn-primary">Start a Session</Button>
                        </div>
                    }
                </div>
                :
                <div className="session-container">
                    <div>
                        <div className="session-description">
                            <h3>Ongoing Session</h3>
                            <Row>
                                <Col xs={6}>
                                    <p>Click session key below to copy to clipboard.</p>
                                    </Col>
                                <Col xs={6}>
                                    <DeleteConfirmationButton id={session.id} name="Session" delete_function={end_session} override_button="End Session" override_title="End Session"/>
                                </Col>
                                <Col xs={12}>
                                    <div className="session-key" onClick={() => {navigator.clipboard.writeText(session.key);}}>
                                        {session.key}
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>
            }
        </div>
    );
}

export default SingleSession;