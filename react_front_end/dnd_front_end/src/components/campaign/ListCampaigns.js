import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '.././shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListCampaigns = ({ campaigns, refresh_function, view_campaign_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/campaign/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(campaigns === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="campaign-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={3} md={3} className="cenetered-column">
                        <p className="card-text"><b>Is Running Session</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        
                    </Col>
                    <Col xs={1} md={1}>
                        <p className="card-text"><b>Controls</b></p>
                    </Col>
                </Row>
                <hr/>
            </div>
            {campaigns.map((campaign) => (
                <div key={campaign.id} className="campaign-item">
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{campaign.name}</p>
                        </Col>
                        <Col xs={3} md={3} className="cenetered-column">
                            {campaign.session != null && <i className="fas fa-check-circle"></i>}
                            {campaign.session == null && <i className="fas fa-times-circle"></i>}
                        </Col>
                        <Col xs={2} md={2}>
                            <a className="btn btn-primary" onClick={() => view_campaign_function(campaign.id)}>View Campaign</a>
                        </Col>
                        <Col xs={1} md={1}>
                            <DeleteConfirmationButton id={campaign.id} name="Campaign" delete_function={delete_page_function} />
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListCampaigns;