import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SingleAlly from './SingleAlly';

const ListAllies = ({ allies, refresh_function, override_function, roll_dice_function}) => {

    if(allies === undefined || allies.length == 0) {
        return (
            <div className="allies-items">
                <p>No Allies in this campaign.</p>
                <hr/>
            </div>
        )
    }
    return (
        <div className="allies-items">
            <hr></hr>
            <Row>
                <Col xs={1} md={1}>
                </Col>
                <Col xs={2} md={2}>
                    <p className="card-text"><b>Name</b></p>
                </Col>
                <Col xs={2} md={2}>
                    <p className="card-text"><b>Damage Taken</b></p>
                </Col>
                <Col xs={2} md={2}>
                    <p className="card-text"><b>Max Health</b></p>
                </Col>
                <Col xs={2} md={2}>
                    
                </Col>
                <Col xs={1} md={1}>

                </Col>
            </Row>
            {allies.map((ally) => (
                <SingleAlly ally={ally} refresh_function={refresh_function} override_function={override_function} roll_dice_function={roll_dice_function}/>
            ))}
        </div>
    );
}

export default ListAllies;