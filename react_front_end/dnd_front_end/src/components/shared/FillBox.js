import React from 'react';


const FillBox = ({value, on_click, use_small_fill_box}) => {
  return (
    <div className={"fill-box" + (use_small_fill_box ? ' small' : '')} onClick={(e) => {e.stopPropagation(); on_click(!value);}}>
        {value == true ?
            <div className="fill-box-filling"></div>
            :
            <div className="fill-box-no-filling"></div>
        }
    </div>
  );
}

export default FillBox;