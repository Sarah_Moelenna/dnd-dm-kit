import React, { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'


const RollingButton = ({roll_dice_function, roll, type, identity, display_override}) => {

    var popover = <Popover id="popover-basic">
        <Popover.Content>
            <a className="dice-roll-button" onClick={() => roll_dice_function(roll, type, "ADVANTAGE", identity)}>Advantage</a>
            <a className="dice-roll-button" onClick={() => roll_dice_function(roll, type, "DISADVANTAGE", identity)}>Disadvantage</a>
        </Popover.Content>
    </Popover>

    return (
        <OverlayTrigger trigger={['hover', 'focus']} delay={{ show: 0, hide: 1000 }} placement="bottom" overlay={popover}>
            <a className="dice-roll-button" onClick={() => roll_dice_function(roll, type, "DEFAULT", identity)}>{display_override == undefined ? roll : display_override}</a>
        </OverlayTrigger>
    );
}

export default RollingButton;