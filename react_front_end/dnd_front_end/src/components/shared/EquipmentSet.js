import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'
import { v4 as uuidv4 } from 'uuid';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton'
import Card from 'react-bootstrap/Card'
import ListSelector from './ListSelector';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import ItemSelect from '../items/ItemSelect'

export const createEquipmentSet = () => {
    return {
        "id": uuidv4(),
        "description": "Unnamed Equipment Set",
        "groups": [],
    }
}

export const createEquipmentSetGroup = () => {
    return {
        "id": uuidv4(),
        "name": "Unnamed Group",
        "options": [],
    }
}

export const createEquipmentSetGroupOption = () => {
    return {
        "id": uuidv4(),
        "name": "Unnamed Option",
        "custom_items": [],
        "gold": 0,
        "select_type": null,
        "item_quantity": 1,
        "items": [],
        "subsets": []
    }
}

export const createEquipmentSetGroupOptionSubset = () => {
    return {
        "id": uuidv4(),
        "select_type": null,
        "items": []
    }
}

class EquipmentSetGroupOptionSubsetItem extends Component {

    update_attribute = (attribute, value) => {
        var new_value = value
        if (new_value == 'true'){
            new_value = true
        } else if (new_value == 'false'){
            new_value = false
        }

        var new_subset = this.props.subset
        new_subset[attribute] = new_value

        this.props.update_subset(this.props.subset.id, new_subset)
    }

    add_item = (id, name) => {
        var new_item = {
            id: id,
            name: name
        }
        var new_subset = this.props.subset
        new_subset.items.push(new_item)

        this.props.update_subset(this.props.subset.id, new_subset)
    }

    remove_item = (new_data) => {
        var new_ids = new_data.replaceAll(" ", "").split(",")
        var new_items = []
        for(var i = 0; i < this.props.subset.items.length; i++){
            var item = this.props.subset.items[i]
            if(new_ids.includes(item.id)){
                new_items.push(item)
            }
        }
        this.update_attribute("items", new_items)
    }

    render(){
        return(
            <div className="equipment-set-group-subset-item">
                <Card>
                    <Card.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <Form.Group>
                                    <Form.Label>Item Provision Type</Form.Label>
                                    <Form.Control name="select_type" as="select" onChange={(e) => {this.update_attribute("select_type", e.target.value)}} custom>
                                            <option selected="" value="">-</option>
                                            <option value="PROVIDE ALL" selected={this.props.subset.select_type == "PROVIDE ALL"}>Provide All</option>
                                            <option value="SELECT_ONE" selected={this.props.subset.select_type == "SELECT_ONE"}>Select One</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>

                            <Col xs={12} md={12}>
                                <Form.Group>
                                    <Form.Label className="item-label">
                                        Items
                                        <ItemSelect
                                            select_function={this.add_item}
                                            disabled_ids={this.props.subset.items.map(function (el) { return el.id; })}
                                            use_button={false}
                                        />
                                    </Form.Label>
                                    <ListSelector
                                        input_value={this.props.subset.items.map(function (el) { return el.id; })}
                                        items={this.props.subset.items}
                                        onChange={this.remove_item} use_keys={["id", "name"]}
                                        no_popover={true}
                                    />
                                </Form.Group>
                            </Col>
                            
                            <Col xs={12} md={12}>
                                <DeleteConfirmationButton id={this.props.subset.id} name="Option" delete_function={this.props.delete_subset} override_button="Delete Option"/>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}

const ListEquipmentSetGroupOptionSubsets = ({ subsets, update_subset, delete_subset}) => {

    return (
        <div className="equipment-set-group-option-items">
            {subsets.map((subset) => (
                <EquipmentSetGroupOptionSubsetItem subset={subset} update_subset={update_subset} delete_subset={delete_subset}/>
            ))}
        </div>
    );
}

class EquipmentSetGroupOptionItem extends Component {
    state = {
        toggle: false,
        new_custom_item: null
    }

    update_attribute = (attribute, value) => {
        var new_value = value
        if (new_value == 'true'){
            new_value = true
        } else if (new_value == 'false'){
            new_value = false
        }

        var new_option = this.props.option
        new_option[attribute] = new_value

        this.props.update_option(this.props.option.id, new_option)
    }

    add_subset = () => {
        var new_subset = createEquipmentSetGroupOptionSubset()
        var subsets = this.props.option.subsets
        subsets.push(new_subset)
        this.update_attribute('subsets', subsets)
    }

    update_subset = (id, new_object) => {
        var subsets = []
        for(var i = 0; i < this.props.option.subsets.length; i++){
            var subset = this.props.option.subsets[i]
            if(subset.id == id){
                subsets.push(new_object)
            } else {
                subsets.push(subset)
            }
        }
        this.update_attribute('subsets', subsets)
    }

    delete_subset = (id) => {
        var subsets = []
        for(var i = 0; i < this.props.option.subsets.length; i++){
            var subset = this.props.option.subsets[i]
            if(subset.id != id){
                subsets.push(subset)
            }
        }
        this.update_attribute('subsets', subsets)
    }

    add_custom_item = () => {
        if(this.props.option.custom_items != null && this.props.option.custom_items != ""){
            var new_custom_items = this.props.option.custom_items
            new_custom_items = new_custom_items.split("|")
        } else {
            new_custom_items=[]
        }
        new_custom_items.push(this.state.new_custom_item)
        new_custom_items = new_custom_items.join('|')

        var new_option = this.props.option
        new_option.custom_items = new_custom_items

        this.setState(
            {toggle: false, new_custom_item: null},
            this.props.update_option(this.props.option.id, new_option)
        )
    }

    add_item = (id, name) => {
        var new_item = {
            id: id,
            name: name
        }
        var new_option = this.props.option
        new_option.items.push(new_item)

        this.props.update_option(this.props.option.id, new_option)
    }

    remove_item = (new_data) => {
        var new_ids = new_data.replaceAll(" ", "").split(",")
        var new_items = []
        for(var i = 0; i < this.props.option.items.length; i++){
            var item = this.props.option.items[i]
            if(new_ids.includes(item.id)){
                new_items.push(item)
            }
        }
        this.update_attribute("items", new_items)
    }

    render(){
        return(
            <div className="equipment-set-group-option-item">
                <Card>
                    <Card.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <Form.Group>
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control 
                                        value={this.props.option.name}
                                        name="name"
                                        type="text"
                                        onChange={(e) => {this.update_attribute("name", e.target.value)}}
                                    />
                                </Form.Group>
                            </Col>

                            <Col xs={12} md={12}>
                                <Form.Group>
                                    <Form.Label>Gold</Form.Label>
                                    <Form.Control 
                                        value={this.props.option.gold}
                                        name="gold"
                                        type="number"
                                        min={0}
                                        onChange={(e) => {this.update_attribute("gold", e.target.value)}}
                                    />
                                </Form.Group>
                            </Col>

                            <Col xs={12} md={12}>
                                <Form.Group>
                                    <Form.Label>
                                        Custom Items
                                        <span className="clickable-div add-button" onClick={() => {this.setState({toggle: true})}}>
                                            <i className="fas fa-plus"></i>
                                        </span>
                                    </Form.Label>
                                    <Modal show={this.state.toggle} size="md" className="custom-item-add-modal">
                                        <Modal.Header>Add Custom item</Modal.Header>
                                        <Modal.Body>
                                            <Form.Group>
                                                <Form.Control 
                                                    value={this.state.new_custom_item}
                                                    name="name"
                                                    type="text"
                                                    onChange={(e) => {this.setState({new_custom_item: e.target.value})}}
                                                />
                                            </Form.Group>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button onClick={this.add_custom_item}>Add</Button>
                                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                                        </Modal.Footer>
                                    </Modal>
                                    <ListSelector
                                        input_value={this.props.option.custom_items != "" ? this.props.option.custom_items.split("|") : []}
                                        items={this.props.option.custom_items != "" ? this.props.option.custom_items.split("|") : []}
                                        onChange={(data) => {this.update_attribute("custom_items", data)}}
                                        no_popover={true}
                                    />
                                </Form.Group>
                            </Col>

                            { this.props.option.items.length > 0 &&
                                <Col xs={12} md={12}>
                                    <Form.Group>
                                        <Form.Label>Item Provision Type</Form.Label>
                                        <Form.Control name="select_type" as="select" onChange={(e) => {this.update_attribute("select_type", e.target.value)}} custom>
                                                <option selected="" value="">-</option>
                                                <option value="PROVIDE ALL" selected={this.props.option.select_type == "PROVIDE ALL"}>Provide All</option>
                                                <option value="SELECT_ONE" selected={this.props.option.select_type == "SELECT_ONE"}>Select One</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                            }

                            { this.props.option.items.length > 0 &&
                                <Col xs={12} md={12}>
                                    <Form.Group>
                                        <Form.Label>Item Quantity</Form.Label>
                                        <Form.Control 
                                            value={this.props.option.item_quantity}
                                            name="item_quantity"
                                            type="number"
                                            min={1}
                                            onChange={(e) => {this.update_attribute("item_quantity", e.target.value)}}
                                        />
                                    </Form.Group>
                                </Col>
                            }

                            <Col xs={12} md={12}>
                                <Form.Group>
                                    <Form.Label className="item-label">
                                        Items
                                        <ItemSelect
                                            select_function={this.add_item}
                                            disabled_ids={this.props.option.items.map(function (el) { return el.id; })}
                                            use_button={false}
                                        />
                                    </Form.Label>
                                    <ListSelector
                                        input_value={this.props.option.items.map(function (el) { return el.id; })}
                                        items={this.props.option.items}
                                        onChange={this.remove_item} use_keys={["id", "name"]}
                                        no_popover={true}
                                    />
                                </Form.Group>
                            </Col>

                            <Col xs={12} md={12}>
                                {this.props.option.subsets.length > 0 ?
                                    <div>
                                        <h4>
                                            {this.props.option.subsets.length} Subsets
                                            <span className="clickable-div add-button" onClick={() => {this.add_subset()}}>
                                                <i className="fas fa-plus"></i>
                                            </span>
                                        </h4>
                                        <ListEquipmentSetGroupOptionSubsets
                                            subsets={this.props.option.subsets}
                                            update_subset={this.update_subset}
                                            delete_subset={this.delete_subset}
                                        />
                                        <br/>
                                    </div>
                                    :
                                    <div>
                                        <hr/>
                                        <h4>
                                            {this.props.option.subsets.length} Subsets
                                            <span className="clickable-div add-button" onClick={() => {this.add_subset()}}>
                                                <i className="fas fa-plus"></i>
                                            </span>
                                        </h4>
                                    </div>
                                }
                            </Col>
                            
                            <Col xs={12} md={12}>
                                <DeleteConfirmationButton id={this.props.option.id} name="Option" delete_function={this.props.delete_option} override_button="Delete Option"/>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}

const ListEquipmentSetGroupOptions = ({ options, update_option, delete_option}) => {

    return (
        <div className="equipment-set-group-option-items">
            {options.map((option) => (
                <EquipmentSetGroupOptionItem option={option} update_option={update_option} delete_option={delete_option}/>
            ))}
        </div>
    );
}

class EquipmentSetGroupItem extends Component {
    update_attribute = (attribute, value) => {
        var new_value = value
        if (new_value == 'true'){
            new_value = true
        } else if (new_value == 'false'){
            new_value = false
        }

        var new_group = this.props.group
        new_group[attribute] = new_value

        this.props.update_group(this.props.group.id, new_group)
    }

    add_option = () => {
        var new_option = createEquipmentSetGroupOption()
        var options = this.props.group.options
        options.push(new_option)
        this.update_attribute('options', options)
    }

    update_option = (id, new_object) => {
        var options = []
        for(var i = 0; i < this.props.group.options.length; i++){
            var option = this.props.group.options[i]
            if(option.id == id){
                options.push(new_object)
            } else {
                options.push(option)
            }
        }
        this.update_attribute('options', options)
    }

    delete_option = (id) => {
        var options = []
        for(var i = 0; i < this.props.group.options.length; i++){
            var option = this.props.group.options[i]
            if(option.id != id){
                options.push(option)
            }
        }
        this.update_attribute('options', options)
    }

    render(){
        return(
            <div className="equipment-set-group-item">
                <Row>
                    <Col xs={12} md={12}>
                        <Form.Group>
                            <Form.Label>Name <DeleteConfirmationButton id={this.props.group.id} name="Group" delete_function={this.props.delete_group} override_button="Delete Group"/></Form.Label>
                            <Form.Control 
                                value={this.props.group.name}
                                name="name"
                                type="text"
                                onChange={(e) => {this.update_attribute("name", e.target.value)}}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        {this.props.group.options.length > 0 ?
                            <div>
                                <h4>
                                    {this.props.group.options.length} Options
                                    <span className="clickable-div add-button" onClick={() => {this.add_option()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListEquipmentSetGroupOptions
                                    options={this.props.group.options}
                                    update_option={this.update_option}
                                    delete_option={this.delete_option}
                                />
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.props.group.options.length} Options
                                    <span className="clickable-div add-button" onClick={() => {this.add_option()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>
                </Row>
            </div>
        )
    }
}

const ListEquipmentSetGroups = ({ groups, update_group, delete_group}) => {

    return (
        <div className="equipment-set-group-items">
            {groups.map((group) => (
                <EquipmentSetGroupItem group={group} update_group={update_group} delete_group={delete_group}/>
            ))}
        </div>
    );
}

export class EditEquipmentSet extends Component {

    update_attribute = (attribute, value) => {
        var new_value = value
        if (new_value == 'true'){
            new_value = true
        } else if (new_value == 'false'){
            new_value = false
        }

        var new_equipment_set = this.props.equipment_set
        new_equipment_set[attribute] = new_value

        this.props.update_equipment_set(this.props.equipment_set.id, new_equipment_set)
    }

    add_group = () => {
        var new_group = createEquipmentSetGroup()
        var groups = this.props.equipment_set.groups
        groups.push(new_group)
        this.update_attribute('groups', groups)
    }

    update_group = (id, new_object) => {
        var groups = []
        for(var i = 0; i < this.props.equipment_set.groups.length; i++){
            var group = this.props.equipment_set.groups[i]
            if(group.id == id){
                groups.push(new_object)
            } else {
                groups.push(group)
            }
        }
        this.update_attribute('groups', groups)
    }

    delete_group = (id) => {
        var groups = []
        for(var i = 0; i < this.props.equipment_set.groups.length; i++){
            var group = this.props.equipment_set.groups[i]
            if(group.id != id){
                groups.push(group)
            }
        }
        this.update_attribute('groups', groups)
    }

    render(){
        return(
            <div className="equipment-set">
                <Row>
                    <Col xs={12} md={12}>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control 
                                value={this.props.equipment_set.description}
                                name="description"
                                type="text"
                                onChange={(e) => {this.update_attribute("description", e.target.value)}}
                            />
                        </Form.Group>
                    </Col>
                    <Col xs={12} md={12}>
                        {this.props.equipment_set.groups.length > 0 ?
                            <div>
                                <h4>
                                    {this.props.equipment_set.groups.length} Groups
                                    <span className="clickable-div add-button" onClick={() => {this.add_group()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListEquipmentSetGroups
                                    groups={this.props.equipment_set.groups}
                                    update_group={this.update_group}
                                    delete_group={this.delete_group}
                                />
                            </div>
                            :
                            <div>
                                <h4>
                                    {this.props.equipment_set.groups.length} Groups
                                    <span className="clickable-div add-button" onClick={() => {this.add_group()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>
                </Row>
            </div>
        )
    }
}