import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { get_api_url } from './Config';

const REACT_APP_API_URL = get_api_url()

class CustomRoller extends Component {

    state = {
        amount: 1,
        dice: "d20",
        symbol: "+",
        modifier: 0
    }

    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    };

    set_dice = (value) => {
        this.setState({dice: value})
    }

    roll = () => {
        var roll = ""
        if(this.state.amount == null || this.state.amount < 1){
            return
        }
        roll = roll + this.state.amount + this.state.dice
        if(this.state.modifier > 0){
            roll = roll + this.state.symbol + this.state.modifier
        }

        this.props.roll_dice_function(roll, this.props.type, "DEFAULT", this.props.identity)
    }

    render(){
        return (
            <div className="custom-dice-roller">
                <Row>
                    <Col xs={2} md={2}>
                        <div className="die clickable-div" onClick={() => {this.set_dice("d4")}}>
                            <img src={REACT_APP_API_URL + "/static/d4.png"}/>
                            <p>D4</p>
                        </div>
                    </Col>
                    <Col xs={2} md={2}>
                        <div className="die clickable-div" onClick={() => {this.set_dice("d6")}}>
                            <img src={REACT_APP_API_URL + "/static/d6.png"}/>
                            <p>D6</p>
                        </div>
                    </Col>
                    <Col xs={2} md={2}>
                        <div className="die clickable-div" onClick={() => {this.set_dice("d8")}}>
                            <img src={REACT_APP_API_URL + "/static/d8.png"}/>
                            <p>D8</p>
                        </div>
                    </Col>
                    <Col xs={2} md={2}>
                        <div className="die clickable-div" onClick={() => {this.set_dice("d10")}}>
                            <img src={REACT_APP_API_URL + "/static/d10.png"}/>
                            <p>D10</p>
                        </div>
                    </Col>
                    <Col xs={2} md={2}>
                        <div className="die clickable-div" onClick={() => {this.set_dice("d20")}}>
                            <img src={REACT_APP_API_URL + "/static/d20.png"}/>
                            <p>D20</p>
                        </div>
                    </Col>
                    <Col xs={2} md={2}>
                        <div className="die clickable-div" onClick={() => {this.set_dice("d100")}}>
                            <img src={REACT_APP_API_URL + "/static/d100.png"}/>
                            <p>D100</p>
                        </div>
                    </Col>
                </Row>
                <hr/>
                <div className="options">
                    <Form.Control className="option" value={this.state.amount} name="amount" type="number" onChange={this.handleChange} min={1}/>
                    <p className="option">{this.state.dice}</p>
                    <Form.Control className="option" name="symbol" as="select" onChange={this.handleChange} value={this.state.symbol} custom>
                        <option value="+">+</option>
                        <option value="-">-</option>
                    </Form.Control>
                    <Form.Control className="option" value={this.state.modifier} name="modifier" type="number" onChange={this.handleChange} min={0}/>
                    <button className="option btn btn-primary" onClick={this.roll}>Roll</button>
                </div>
            </div>
        );
    }
}

export default CustomRoller;