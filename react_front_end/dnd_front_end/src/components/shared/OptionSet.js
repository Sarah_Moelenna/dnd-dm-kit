import React, { Component } from 'react';
import { v4 as uuidv4 } from 'uuid';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton'
import { ListOptions, createOption } from '../shared/Option';
import Card from 'react-bootstrap/Card'
import { Collapse } from 'reactstrap';


export const createOptionSet = () => {
    return {
        "id": uuidv4(),
        "name": "Unnamed Option Set",
        "level": 0,
        "options": [],
    }
}

class OptionSet extends Component {

    state = {
        toggle: false
    }

    handleChange = (e) => {
        var new_option = this.props.option_set
        new_option[e.target.name] = e.target.value
        this.props.update_option_set(this.props.option_set.id, new_option)
    }

    add_option = () => {
        var new_option = createOption()
        var options = this.props.option_set.options
        options.push(new_option)

        
        var new_option = this.props.option_set
        new_option.options = options
        this.props.update_option_set(this.props.option_set.id, new_option)
    }

    delete_option = (id) => {
        var options = []
        for(var i = 0; i < this.props.option_set.options.length; i++){
            var option = this.props.option_set.options[i]
            if(option.id != id){
                options.push(option)
            }
        }

        var new_option = this.props.option_set
        new_option.options = options
        this.props.update_option_set(this.props.option_set.id, new_option)
    }


    update_option = (id, new_object) => {
        var options = []
        for(var i = 0; i < this.props.option_set.options.length; i++){
            var option = this.props.option_set.options[i]
            if(option.id == id){
                options.push(new_object)
            } else {
                options.push(option)
            }
        }

        var new_option = this.props.option_set
        new_option.options = options
        this.props.update_option_set(this.props.option_set.id, new_option)
    }

    render(){

        return(
            <Card key={this.props.option_set.id} className="option-set-item">
                <Card.Body>
                    <Row>
                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control value={this.props.option_set.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Level</Form.Label>
                                <Form.Control value={this.props.option_set.level} required name="level" type="number" min={0} placeholder="" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>
                        
                        <Col xs={12} md={12}>
                            {this.props.option_set.options.length > 0 ?
                                <div>
                                    <hr/>
                                    <h4 className="list-title">
                                        {this.props.option_set.options.length} Options
                                        <span className="clickable-div add-button" onClick={() => {this.add_option()}}>
                                            <i className="fas fa-plus"></i>
                                        </span>
                                        <span className="option-set-dropper">
                                            <div className="clickable-div" onClick={() => {this.setState({toggle: !this.state.toggle})}}><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                                        </span>
                                    </h4>
                                    <Collapse isOpen={this.state.toggle}>
                                        {this.state.toggle &&
                                            <ListOptions options={this.props.option_set.options} update_option={this.update_option} delete_option={this.delete_option} display_type="RACE"/>
                                        }
                                    </Collapse>
                                </div>
                                :
                                <div>
                                    <hr/>
                                    <h4 className="list-title">
                                        {this.props.option_set.options.length} Options
                                        <span className="clickable-div add-button" onClick={() => {this.add_option()}}>
                                            <i className="fas fa-plus"></i>
                                        </span>
                                    </h4>
                                </div>
                            }
                        </Col>
                        
                        <Col xs={2} md={2} className="option-snippet">
                            <DeleteConfirmationButton id={this.props.option_set.id} name="Option Set" delete_function={this.props.delete_option_set} override_button="Delete"/>
                        </Col>
                        
                    </Row>
                </Card.Body>
            </Card>
        )

    };
}

export const ListOptionSets = ({option_sets, update_option_set, delete_option_set}) => {
    return (
        <div className="option-set-items">
            {option_sets.map((option_set) => (
                <OptionSet option_set={option_set} update_option_set={update_option_set} delete_option_set={delete_option_set}/>
            ))}
        </div>
    )
}