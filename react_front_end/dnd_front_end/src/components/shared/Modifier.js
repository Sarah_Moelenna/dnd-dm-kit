import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { AttributeID, ModifierTypes, ModifierSubTypes, DiceValues, ModifierDurationOptions } from '../monster/Data';
import DeleteConfirmationButton from './DeleteConfirmationButton'
import { v4 as uuidv4 } from 'uuid';
import { ListAtHigherLevel, createAtHigherLevel } from './AtHigherLevel'
import ListSelector from './ListSelector';
import { get_choices } from './Choices';



export const createModifier = () => {
    return {
        "id": uuidv4(),
        "fixed_value": null,
        "type": null,
        "sub_type": null,
        "dice": {"diceCount": null, "diceValue": null, "diceMultiplier": null, "fixedValue": null, "diceString": null},
        "restriction": null,
        "stat_id": null,
        "requires_attunement": null,
        "friendly_type_name": null,
        "friendly_sub_type_name": null,
        "duration": null,
        "is_granted": null,
        "bonus_types": null,
        "value": null,
        "available_to_multiclass": null,
        "modifier_type_id": null,
        "modifier_sub_type_id": null,
        "use_primary_stat": null,
        "count": null,
        "at_higher_levels": [],
        "limits": null,
    }
}

export class ModifierItem extends Component {

    state = {
        toggle: false,
        subtypes: [],
        selected_limit: null
    }

    componentWillMount() {
        var subtypes = ModifierSubTypes[this.props.modifier.modifier_type_id]
        if(subtypes != undefined && subtypes != null){
            this.setState({subtypes: subtypes}
            )
        }
    };

    get_subtype_choice_data = () => {
        let sub_type = null
        let choices = []
        if(!this.props.modifier.modifier_type_id || !this.props.modifier.modifier_sub_type_id){
            return choices
        }

        for(var i = 0; i < ModifierSubTypes[this.props.modifier.modifier_type_id.toString()].length; i++){
            if(ModifierSubTypes[this.props.modifier.modifier_type_id][i].id == this.props.modifier.modifier_sub_type_id.toString()){
                sub_type = ModifierSubTypes[this.props.modifier.modifier_type_id][i]
            }
        }
        if(sub_type){
            if(sub_type.choice_groups && sub_type.choice_groups.length > 0){
                choices = get_choices(sub_type.choice_groups, sub_type.only, sub_type.exclude, [])
            }
            
        }

        return choices
    }
    
    add_choice_to_limit = () => {
        let choices = this.get_subtype_choice_data()
        if(choices.length > 0){
            let selected = null
            choices.forEach((choice) => {
                if(this.state.selected_limit == choice.id){
                    selected = this.state.selected_limit
                }
            })

            if(selected){
                var new_modifier = this.props.modifier
                if(!new_modifier.limits){
                    new_modifier.limits = []
                }
                new_modifier.limits.push(selected)
                this.props.update_modifier(this.props.modifier.id, new_modifier)
                this.setState({selected_limit: null})
            }
        }

        
    }

    update_type = (e) => {
        var subtypes = ModifierSubTypes[e.target.value]
        if(subtypes != undefined && subtypes != null){
            this.setState({subtypes: subtypes})
        }

        var new_modifier = this.props.modifier
        new_modifier.modifier_type_id = e.target.value

        for(var i = 0; i < ModifierTypes.length; i++){
            if(ModifierTypes[i].id == e.target.value){
                new_modifier.type = ModifierTypes[i].name
                new_modifier.friendly_type_name = ModifierTypes[i].display_name
                new_modifier.modifier_sub_type_id = null
                new_modifier.sub_type = null
                new_modifier.friendly_sub_type_name = null
                new_modifier.limits = null
            }
        }

        this.props.update_modifier(this.props.modifier.id, new_modifier)
        
    }

    update_subtype = (e) => {
        var new_modifier = this.props.modifier
        new_modifier.modifier_sub_type_id = e.target.value

        for(var i = 0; i < ModifierSubTypes[this.props.modifier.modifier_type_id.toString()].length; i++){
            if(ModifierSubTypes[this.props.modifier.modifier_type_id][i].id == e.target.value){
                new_modifier.modifier_sub_type_id = e.target.value
                new_modifier.sub_type = ModifierSubTypes[this.props.modifier.modifier_type_id][i].name
                new_modifier.friendly_sub_type_name = ModifierSubTypes[this.props.modifier.modifier_type_id][i].display_name
                new_modifier.limits = null
            }
        }

        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value = true
        } else if(new_value == "false"){
            new_value = false
        }
        var new_modifier = this.props.modifier
        new_modifier[e.target.name] = new_value
        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    update_limits = (limits) => {
        var new_modifier = this.props.modifier
        new_modifier.limits = limits
        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    handleChangeIntOrNull = (e) => {
        var new_value = e.target.value
        if(new_value == ""){
            new_value = null
        } else {
            new_value = parseInt(new_value)
        }
        var new_modifier = this.props.modifier
        new_modifier[e.target.name] = new_value
        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    update_dice = (e) => {
        var new_value = e.target.value
        var new_modifier = this.props.modifier
        if(new_modifier.dice == null){
            new_modifier.dice = {}
        }

        if(e.target.name == 'diceCount' || e.target.name == 'diceValue' || e.target.name == 'fixedValue'){
            if(new_value == ''){
                new_value = null
            } else {
                new_value = parseInt(new_value)
            }
        }

        new_modifier.dice[e.target.name] = new_value
        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    update_duration = (e) => {
        var new_value = e.target.value
        var new_modifier = this.props.modifier
        if(new_modifier.duration == null){
            new_modifier.duration = {}
        }

        if(e.target.name == 'durationInterval'){
            if(new_value == ''){
                new_value = null
            } else {
                new_value = parseInt(new_value)
            }
        }

        new_modifier.duration[e.target.name] = new_value
        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    add_at_higher_level = () => {
        var new_modifier = this.props.modifier
        new_modifier.at_higher_levels.push(createAtHigherLevel())
        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    update_at_higher_level = (index, new_object) => {
        var new_modifier = this.props.modifier
        new_modifier.at_higher_levels[index] = new_object
        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    delete_at_higher_level = (index) => {
        var new_modifier = this.props.modifier
        var new_higher_levels = new_modifier.at_higher_levels
        new_higher_levels.splice(index, 1)
        new_modifier.at_higher_levels = new_higher_levels
        this.props.update_modifier(this.props.modifier.id, new_modifier)
    }

    render(){

        let subtype_choices = this.get_subtype_choice_data()
        let limits = this.props.modifier.limits ? this.props.modifier.limits.join(',') : ''

        return(
            <Row key={this.props.modifier.id} className="modifier-item">
                <Col xs={2} md={2} className="modifier-name">
                    <p>{this.props.modifier.friendly_type_name} - {this.props.modifier.friendly_sub_type_name}</p>
                </Col>
                <Col xs={2} md={2}>
                    {this.props.modifier.dice != null && (this.props.modifier.dice.diceCount != null || this.props.modifier.dice.diceValue != null) &&
                        <p>{this.props.modifier.dice.diceCount}d{this.props.modifier.dice.diceValue}</p>
                    }
                </Col>
                <Col xs={1} md={1}>
                    {this.props.modifier.stat_id != null &&
                        <p>{AttributeID[this.props.modifier.stat_id]}</p>
                    }
                </Col>
                {this.props.display_type == "SPELL" &&
                    <Col xs={1} md={1}>
                        {this.props.modifier.count != null &&
                            <p>{this.props.modifier.count}</p>
                        }
                    </Col>
                }
                <Col xs={1} md={1}>
                    {this.props.modifier.dice.fixedValue != null &&
                        <p>{this.props.modifier.dice.fixedValue}</p>
                    }
                </Col>
                <Col xs={2} md={2}>
                    {this.props.modifier.restriction != null &&
                        <p>{this.props.modifier.restriction}</p>
                    }
                </Col>
                <Col xs={1} md={1}>
                    {this.props.modifier.duration != null &&
                        <p>{this.props.modifier.duration.durationInterval} {this.props.modifier.duration.durationUnit}</p>
                    }
                </Col>
                <Col xs={1} md={1}>
                    <Button onClick={() => {this.setState({toggle: true})}}>Edit</Button>
                </Col>
                <Col xs={this.props.display_type == "SPELL" ? 1 : 2} md={this.props.display_type == "SPELL" ? 1 : 2}>
                    <DeleteConfirmationButton id={this.props.modifier.id} name="Modifier" delete_function={this.props.delete_modifier} override_button="Delete"/>
                </Col>
                <Col xs={12} md={12}>
                    <Modal show={this.state.toggle} size="md" className="select-modal modifier-edit">
                        <Modal.Header>Edit Modifier</Modal.Header>
                        {this.state.toggle &&
                            <Modal.Body>
                                <Row>
                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>MODIFIER TYPE</Form.Label>
                                            <Form.Control name="size" as="select" onChange={this.update_type} custom>
                                                    <option selected="true" value="">-</option>
                                                    {ModifierTypes.map((modifier_type) => (
                                                        <option key={modifier_type.id} selected={this.props.modifier.modifier_type_id == modifier_type.id} value={modifier_type.id}>{modifier_type.display_name}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>MODIFIER SUBTYPE</Form.Label>
                                            <Form.Control name="subtype" as="select" onChange={this.update_subtype} custom>
                                                    <option selected="true" value="">-</option>
                                                    {this.state.subtypes.map((subtype) => (
                                                        <option key={subtype.id} selected={this.props.modifier.modifier_sub_type_id == subtype.id} value={subtype.id}>{subtype.display_name}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>FIXED VALUE</Form.Label>
                                            <Form.Control value={this.props.modifier.dice != null && this.props.modifier.dice.fixedValue} required name="fixedValue" type="number" min={0} onChange={this.update_dice} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        {this.props.display_type == "RACE" &&
                                            <Form.Group controlId="formBasicSize">
                                                <Form.Label>ABILITY SCORE</Form.Label>
                                                <Form.Control name="stat_id" as="select" onChange={this.handleChangeIntOrNull} custom>
                                                        <option selected="true" value="">-</option>
                                                        {Object.keys(AttributeID).map((attribute_id) => (
                                                            <option key={attribute_id} selected={this.props.modifier.stat_id == attribute_id} value={attribute_id}>{AttributeID[attribute_id]}</option>
                                                        ))}
                                                </Form.Control>
                                            </Form.Group>
                                        }
                                        {this.props.display_type == "SPELL" &&
                                            <Form.Group controlId="formBasicSize">
                                                <Form.Label>USE PRIMARY STAT</Form.Label>
                                                <Form.Control name="use_primary_stat" as="select" onChange={this.handleChange} custom>
                                                        <option selected={this.props.modifier.use_primary_stat == true} value={true}>True</option>
                                                        <option selected={this.props.modifier.use_primary_stat == false} value={false}>False</option>
                                                </Form.Control>
                                            </Form.Group>
                                        }
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>DICE COUNT</Form.Label>
                                            <Form.Control value={this.props.modifier.dice != null && this.props.modifier.dice.diceCount} required name="diceCount" type="number" min={0} onChange={this.update_dice} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>DIE TYPE</Form.Label>
                                            <Form.Control name="diceValue" as="select" onChange={this.update_dice} custom>
                                                    <option selected="true" value="">-</option>
                                                    {DiceValues.map((dice_value) => (
                                                        <option key={dice_value} selected={this.props.modifier.dice != null && this.props.modifier.dice.diceValue == dice_value} value={dice_value}>d{dice_value}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    {this.props.display_type == "SPELL" &&
                                        <Col xs={4} md={4}>
                                            <Form.Group controlId="formBasicWis">
                                                <Form.Label>COUNT</Form.Label>
                                                <Form.Control value={this.props.modifier.count != null && this.props.modifier.count} required name="count" type="number" min={0} onChange={this.handleChange} />
                                            </Form.Group>
                                        </Col>
                                    }

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>DURATION INTERVAL</Form.Label>
                                            <Form.Control value={this.props.modifier.duration != null && this.props.modifier.duration.durationInterval} required name="durationInterval" type="number" min={0} onChange={this.update_duration} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>{'\u00A0'}</Form.Label>
                                            <Form.Control name="durationUnit" as="select" onChange={this.update_duration} custom>
                                                    <option selected="true" value="">-</option>
                                                    {ModifierDurationOptions.map((duration_unit) => (
                                                        <option key={duration_unit} selected={this.props.modifier.duration != null && this.props.modifier.duration.durationUnit == duration_unit} value={duration_unit}>{duration_unit}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <Form.Group controlId="formBasicName">
                                            <Form.Label>RESTRICTION</Form.Label>
                                            <Form.Control value={this.props.modifier.restriction} required name="restriction" type="text" placeholder="" onChange={this.handleChange} />
                                        </Form.Group>
                                    </Col>

                                    { subtype_choices.length > 0 &&
                                        <React.Fragment>
                                            <Col xs={3} md={3}>
                                                <Form.Label>Avaliable Choices</Form.Label>
                                                <Form.Control name="durationUnit" as="select" onChange={(e) => {this.setState({selected_limit: e.target.value})}} value={this.state.selected_limit} custom>
                                                        <option selected="true" value="">-</option>
                                                        {subtype_choices.map((subtype_choice) => (
                                                            <option key={subtype_choice.id} value={subtype_choice.id}>{subtype_choice.name}</option>
                                                        ))}
                                                </Form.Control>
                                            </Col>
                                            <Col xs={3} md={3}>
                                                <br/>
                                                <Button className='btn btn-primary' onClick={this.add_choice_to_limit}>Add Choice To Limit</Button>
                                            </Col>
                                            <Col xs={6} md={6}>
                                                <Form.Label>LIMIT SELECTION</Form.Label>
                                                <ListSelector input_value={limits} items={subtype_choices} use_keys={["id", "name"]} no_popover={true} onChange={(data) => {this.update_limits(data.split(","))}} />
                                            </Col>
                                        </React.Fragment>
                                    }

                                    {this.props.display_type == "SPELL" && this.props.modifier.at_higher_levels != undefined && this.props.modifier.at_higher_levels != null && this.props.modifier.at_higher_levels != undefined &&
                                        <Col xs={12} md={12}>
                                            {this.props.modifier.at_higher_levels.length > 0 ?
                                                <div>
                                                    <hr/>
                                                    <h4>
                                                        {this.props.modifier.at_higher_levels.length} At Higher Level
                                                        <span className="clickable-div add-button" onClick={() => {this.add_at_higher_level()}}>
                                                            <i className="fas fa-plus"></i>
                                                        </span>
                                                    </h4>
                                                    <ListAtHigherLevel
                                                        items={this.props.modifier.at_higher_levels}
                                                        update_function={this.update_at_higher_level}
                                                        delete_function={this.delete_at_higher_level}
                                                        scale_type={this.props.scale_type}
                                                        as_collapse={true}
                                                    />
                                                </div>
                                                :
                                                <div>
                                                    <hr/>
                                                    <h4>
                                                        {this.props.modifier.at_higher_levels.length} At Higher Level
                                                        <span className="clickable-div add-button" onClick={() => {this.add_at_higher_level()}}>
                                                            <i className="fas fa-plus"></i>
                                                        </span>
                                                    </h4>
                                                </div>
                                            }
                                        </Col>
                                    }

                                </Row>
                            </Modal.Body>
                        }
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: false})}}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </Col>
            </Row>
        )
    }
}

export const ListModifiers = ({ modifiers, update_modifier, delete_modifier, display_type, scale_type}) => {

    return (
        <div className="modifier-items">
            <Row>
                <Col xs={2} md={2}>
                    <p>MODIFIER</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>DICE ROLL</p>
                </Col>
                <Col xs={1} md={1}>
                    <p>PRIMARY STAT</p>
                </Col>
                {display_type == "SPELL" &&
                    <Col xs={1} md={1}>
                        <p>COUNT</p>
                    </Col>
                }
                <Col xs={1} md={1}>
                    <p>FIXED VALUE</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>RESTRICTION</p>
                </Col>
                <Col xs={1} md={1}>
                    <p>DURATION</p>
                </Col>
                <Col xs={display_type == "SPELL" ? 1 : 2} md={display_type == "SPELL" ? 1 : 2}>
                    
                </Col>
                <Col xs={1} md={1}>
                </Col>
            </Row>
            {modifiers.map((modifier) => (
                <ModifierItem modifier={modifier} update_modifier={update_modifier} delete_modifier={delete_modifier} display_type={display_type} scale_type={scale_type}/>
            ))}
        </div>
    );
}