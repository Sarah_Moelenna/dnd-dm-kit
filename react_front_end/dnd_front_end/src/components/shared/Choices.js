import { DND_GET } from './DNDRequests';

export const get_choices = (groups, only, exclude, always_include, store_function, stored_choices) => {
    var all_choices = []
    var final_choices = []
    for(var i = 0; i < groups.length; i++){
      if(store_function && groups[i] == 'SPECIAL_FEATS' && (!stored_choices || !Object.keys(stored_choices).includes('SPECIAL_FEATS'))){
        get_feats(groups[i], store_function)
      }
      if(CHOICES[groups[i]]){
        all_choices.push(...CHOICES[groups[i]])
      }
      if(stored_choices && stored_choices[groups[i]]){
        all_choices.push(...stored_choices[groups[i]])
      }
    }
    if(only != undefined || exclude != null){
      
      for(var i = 0; i < all_choices.length; i++){
        var choice = all_choices[i]
        let include = true
        if(only != undefined && !only.includes(choice.id)){
          include = false
        }
        if(exclude != undefined && exclude.includes(choice.id)){
          include = false
        }
        if(always_include != undefined && always_include.includes(choice.id)){
          include = true
        }
        if(include == true){
          final_choices.push(choice)
        }
      }
      
    } else {
      final_choices = all_choices;
    }

    final_choices = final_choices.sort(function(a, b) {
      const nameA = a.name.toUpperCase()
      const nameB = b.name.toUpperCase()
      if (nameA < nameB) {
        return -1
      }
      if (nameA > nameB) {
        return 1
      }
      return 0
    });

    return final_choices
}

const get_feats = (group, store_function) => {
  DND_GET(
    '/feat?page=1&results_per_page=999&is_selectable=true',
    (jsondata) => {
      let data = []
      data[group] = jsondata.results
      store_function(data)
    },
    null
  )
}

export const CHOICES = {
    "ABILITIES":[
        {
            "name": "Strength",
            "id": "strength-score"
        },
        {
            "name": "Dexterity",
            "id": "dexterity-score"
        },
        {
            "name": "Constitution",
            "id": "constitution-score"
        },
        {
            "name": "Wisdom",
            "id": "wisdom-score"
        },
        {
            "name": "Intelligence",
            "id": "intelligence-score"
        },
        {
            "name": "Charisma",
            "id": "charisma-score"
        }
    ],
    "ARTISAN": [
        {
            "name": "Alchemist's Supplies",
            "id": "alchemists-supplies"
        },
        {
            "name": "Brewer's Supplies",
            "id": "brewers-supplies"
        },
        {
          "name": "Calligrapher's Supplies",
          "id": "calligraphers-supplies"
        },
        {
          "name": "Carpenter's Tools",
          "id": "carpenters-tools"
        },
        {
          "name": "Cartographer's Tools",
          "id": "cartographers-tools"
        },
        {
          "name": "Cobbler's Tools",
          "id": "cobblers-tools"
        },
        {
          "name": "Cook's Utensils",
          "id": "cooks-utensils"
        },
        {
          "name": "Disguise Kit",
          "id": "disguise-kit"
        },
        {
          "name": "Forgery Kit",
          "id": "forgery-kit"
        },
        {
          "name": "Glassblower's Tools",
          "id": "glassblowers-tools"
        },
        {
          "name": "Herbalism Kit",
          "id": "herbalism-kit"
        },
        {
          "name": "Jeweler's Tools",
          "id": "jewelers-tools"    
        },
        {
          "name": "Leatherworker's Tools",
          "id": "leatherworkers-tools"
        },
        {
          "name": "Mason's Tools",
          "id": "masons-tools"
        },
        {
          "name": "Navigator's Tools",
          "id": "navigators-tools"
        },
        {
          "name": "Painter's Supplies",
          "id": "painters-supplies"
        },
        {
          "name": "Potter's Tools",
          "id": "potters-tools"
        },
        {
          "name": "Smith's Tools",
          "id": "smiths-tools"
        },
        {
          "name": "Tinker's Tools",
          "id": "tinkers-tools"
        },
        {
          "name": "Weaver's Tools",
          "id": "weavers-tools"
        },
        {
          "name": "Woodcarver's Tools",
          "id": "woodcarvers-tools"
        }
    ],
    "MUSICAL": [
        {
            "name": "Bagpipes",
            "id": "bagpipes"
        },
        {
          "name": "Drum",
          "id": "drum"
        },
        {
          "name": "Dulcimer",
          "id": "dulcimer"
        },
        {
          "name": "Flute",
          "id": "flute"
        },
        {
          "name": "Horn",
          "id": "horn"
        },
        {
          "name": "Lute",
          "id": "lute"
        },
        {
          "name": "Lyre",
          "id": "lyre"
        },
        {
          "name": "Pan Flute",
          "id": "pan-flute"
        },
        {
          "name": "Shawm",
          "id": "shawm"
        },
        {
          "name": "Viol",
          "id": "viol"
        }
    ],
    "GAMING": [
        {
            "name": "Dice Set",
            "id": "dice-set"
        },
        {
          "name": "Playing Card Set",
          "id": "playing-card-set"
        }
    ],
    "TOOLS_OTHER": [
        {
            "name": "Poisoner's Kit",
            "id": "poisoners-kit"
        },
        {
          "name": "Thieves' Tools",
          "id": "thieves-tools"
        }
    ],
    "MARTIAL": [
        {
            "name": "Battleaxe",
            "id": "battleaxe"
        },
        {
            "name": "Blowgun",
            "id": "blowgun"
        },
        {
          "name": "Crossbow, Hand",
          "id": "crossbow-hand"
        },
        {
          "name": "Crossbow, Heavy",
          "id": "crossbow-heavy"
        },
        {
          "name": "Flail",
          "id": "flail"
        },
        {
          "name": "Glaive",
          "id": "glaive"
        },
        {
          "name": "Greataxe",
          "id": "greataxe"
        },
        {
          "name": "Greatsword",
          "id": "greatsword"
        },
        {
          "name": "Halberd",
          "id": "halberd"
        },
        {
          "name": "Lance",
          "id": "lance"
        },
        {
          "name": "Longbow",
          "id": "longbow"
        },
        {
          "name": "Longsword",
          "id": "longsword"
        },
        {
          "name": "Maul",
          "id": "maul"
        },
        {
          "name": "Morningstar",
          "id": "morningstar"
        },
        {
          "name": "Net",
          "id": "net"
        },
        {
          "name": "Pike",
          "id": "pike"
        },
        {
          "name": "Rapier",
          "id": "rapier"
        },
        {
          "name": "Scimitar",
          "id": "scimitar"
        },
        {
          "name": "Shortsword",
          "id": "shortsword"
        },
        {
          "name": "Trident",
          "id": "trident"
        },
        {
          "name": "War pick",
          "id": "war-pick"
        },
        {
          "name": "Warhammer",
          "id": "warhammer"
        },
        {
          "name": "Whip",
          "id": "whip"
        }
    ],
    "SIMPLE": [
        {
            "name": "Club",
            "id": "club"
        },
        {
          "name": "Crossbow, Light",
          "id": "crossbow-light"
        },
        {
          "name": "Dagger",
          "id": "dagger"
        },
        {
          "name": "Dart",
          "id": "dart"
        },
        {
          "name": "Greatclub",
          "id": "greatclub"
        },
        {
          "name": "Handaxe",
          "id": "handaxe"
        },
        {
          "name": "Javelin",
          "id": "javelin"
        },
        {
          "name": "Light Hammer",
          "id": "light-hammer"
        },
        {
          "name": "Mace",
          "id": "mace"
        },
        {
          "name": "Quarterstaff",
          "id": "quarterstaff"
        },
        {
          "name": "Shortbow",
          "id": "shortbow"
        },
        {
          "name": "Sickle",
          "id": "sickle"
        },
        {
          "name": "Sling",
          "id": "sling"
        },
        {
          "name": "Spear",
          "id": "spear"
        }
    ],
    "ONE_HANDED_MELEE": [
        {
            "name": "Club",
            "id": "club"
        },
        {
          "name": "Dagger",
          "id": "dagger"
        },
        {
          "name": "Handaxe",
          "id": "handaxe"
        },
        {
          "name": "Javelin",
          "id": "javelin"
        },
        {
          "name": "Light Hammer",
          "id": "light-hammer"
        },
        {
          "name": "Mace",
          "id": "mace"
        },
        {
          "name": "Quarterstaff",
          "id": "quarterstaff"
        },
        {
          "name": "Sickle",
          "id": "sickle"
        },
        {
          "name": "Sling",
          "id": "sling"
        },
        {
          "name": "Spear",
          "id": "spear"
        },
        {
            "name": "Battleaxe",
            "id": "battleaxe"
        },
        {
            "name": "Blowgun",
            "id": "blowgun"
        },
        {
          "name": "Flail",
          "id": "flail"
        },
        {
          "name": "Lance",
          "id": "lance"
        },
        {
          "name": "Longsword",
          "id": "longsword"
        },
        {
          "name": "Morningstar",
          "id": "morningstar"
        },
        {
          "name": "Rapier",
          "id": "rapier"
        },
        {
          "name": "Scimitar",
          "id": "scimitar"
        },
        {
          "name": "Shortsword",
          "id": "shortsword"
        },
        {
          "name": "Trident",
          "id": "trident"
        },
        {
          "name": "War pick",
          "id": "war-pick"
        },
        {
          "name": "Warhammer",
          "id": "warhammer"
        },
        {
          "name": "Whip",
          "id": "whip"
        }
    ],
    "SKILLS": [
        {
            "name": "Acrobatics",
            "id": "acrobatics"
        },
        {
          "name": "Animal Handling",
          "id": "animal-handling"
        },
        {
          "name": "Arcana",
          "id": "arcana"
        },
        {
          "name": "Athletics",
          "id": "athletics"
        },
        {
          "name": "Deception",
          "id": "deception"
        },
        {
          "name": "History",
          "id": "history"
        },
        {
          "name": "Insight",
          "id": "insight"
        },
        {
          "name": "Intimidation",
          "id": "intimidation"
        },
        {
          "name": "Investigation",
          "id": "investigation"
        },
        {
          "name": "Medicine",
          "id": "medicine"
        },
        {
          "name": "Nature",
          "id": "nature"
        },
        {
          "name": "Perception",
          "id": "perception"
        },
        {
          "name": "Performance",
          "id": "performance"
        },
        {
          "name": "Persuasion",
          "id": "persuasion"
        },
        {
          "name": "Religion",
          "id": "religion"
        },
        {
          "name": "Sleight of Hand",
          "id": "sleight-of-hand"
        },
        {
          "name": "Stealth",
          "id": "stealth"
        },
        {
          "name": "Survival",
          "id": "survival"
        }
    ],
    "LANGUAGES": [
        {
          "name": "Daelkyr",
          "id": "daelkyr"
        },
        {
          "name": "Dwarvish",
          "id": "dwarvish"
        },
        {
          "name": "Elvish",
          "id": "elvish"
        },
        {
          "name": "Giant",
          "id": "giant"
        },
        {
          "name": "Gith",
          "id": "gith"
        },
        {
          "name": "Gnomish",
          "id": "gnomish"
        },
        {
          "name": "Halfling",
          "id": "halfling"
        },
        {
          "name": "Kraul",
          "id": "kraul"
        },
        {
          "name": "Leonin",
          "id": "leonin"
        },
        {
          "name": "Loxodon",
          "id": "loxodon"
        },
        {
          "name": "Marquesian",
          "id": "marquesian"
        },
        {
          "name": "Minotaur",
          "id": "minotaur"
        },
        {
          "name": "Naush",
          "id": "naush"
        },
        {
          "name": "Orc",
          "id": "orc"
        },
        {
          "name": "Quori",
          "id": "quori"
        },
        {
          "name": "Riedran",
          "id": "riedran"
        },
        {
          "name": "Vedalken",
          "id": "vedalken"
        },
        {
          "name": "Zemnian",
          "id": "zemnian"
        }
    ],
    "EXOTIC_LANGUAGES": [
        {
          "name": "Abyssal",
          "id": "abyssal"
        },
        {
          "name": "Celestial",
          "id": "celestial"
        },
        {
          "name": "Deep Speech",
          "id": "deep-speech"
        },
        {
          "name": "Draconic",
          "id": "draconic"
        },
        {
          "name": "Infernal",
          "id": "infernal"
        },
        {
          "name": "Primordial",
          "id": "primordial"
        },
        {
          "name": "Sylvan",
          "id": "sylvan"
        },
        {
          "name": "Undercommon",
          "id": "undercommon"
        },
    ],
    "HEAVY": [
        {
            "name": "Chain Mail",
            "id": "chain-mail",
        },
        {
          "name": "Mithril Plate Armor",
          "id": "mithril-plate-armor",
        },
        {
          "name": "Plate",
          "id": "plate",
        },
        {
          "name": "Ring Mail",
          "id": "ring-mail",
        },
        {
          "name": "Splint",
          "id": "splint"
        }
    ],
    "LIGHT": [
        {
            "name": "Studded Leather",
            "id": "studded-leather"
        },
        {
          "name": "Leather",
          "id": "leather",
        },
        {
          "name": "Padded",
          "id": "padded",
        },
        {
          "name": "Studded Leather",
          "id": "studded-leather",
        },
    ],
    "MEDIUM": [
        {
            "name": "Breastplate",
            "id": "breastplate"
        },
        {
          "name": "Chain Shirt",
          "id": "chain-shirt"
        },
        {
          "name": "Half Plate",
          "id": "half-plate"
        },
        {
          "name": "Hide",
          "id": "hide"
        },
        {
          "name": "Scale Mail",
          "id": "scale-mail"
        }
    ],
    "ARMOUR_TYPE": [
        {
            "name": "Heavy",
            "id": "heavy-armor"
        },
        {
            "name": "Medium",
            "id": "medium-armor"
        },
        {
            "name": "Light",
            "id": "light-armor"
        }
    ],
    "STRENGTH_SKILLS": [
        {
          "name": "Athletics",
          "id": "athletics"
        }
    ],
    "DEXTERITY_SKILLS": [
        {
            "name": "Acrobatics",
            "id": "acrobatics"
        },
        {
          "name": "Sleight of Hand",
          "id": "sleight-of-hand"
        },
        {
          "name": "Stealth",
          "id": "stealth"
        }
    ],
    "INTELLIGENCE_SKILLS": [
        {
          "name": "Arcana",
          "id": "arcana"
        },
        {
          "name": "Athletics",
          "id": "athletics"
        },
        {
          "name": "History",
          "id": "history"
        },
        {
          "name": "Investigation",
          "id": "investigation"
        },
        {
          "name": "Nature",
          "id": "nature"
        },
        {
          "name": "Religion",
          "id": "religion"
        }
    ],
    "WISDOM_SKILLS": [
        {
          "name": "Animal Handling",
          "id": "animal-handling"
        },
        {
          "name": "Insight",
          "id": "insight"
        },
        {
          "name": "Medicine",
          "id": "medicine"
        },
        {
          "name": "Perception",
          "id": "perception"
        },
        {
          "name": "Survival",
          "id": "survival"
        }
    ],
    "CHARISMA_SKILLS": [
        {
          "name": "Deception",
          "id": "deception"
        },
        {
          "name": "Intimidation",
          "id": "intimidation"
        },
        {
          "name": "Performance",
          "id": "performance"
        },
        {
          "name": "Persuasion",
          "id": "persuasion"
        }
    ]
}

