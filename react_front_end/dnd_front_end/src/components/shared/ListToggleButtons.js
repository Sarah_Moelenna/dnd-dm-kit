import React from 'react';

const ListToggleButtons = ({values, item_dict, on_change}) => {

    const toggle_value = (value) => {
        var new_values = []
        if(values.includes(value) == true){
            for(var i = 0; i < values.length; i++){
                if(values[i] != value){
                    new_values.push(values[i])
                }
            }
        } else {
            new_values = values
            new_values.push(value)
        }
        on_change(new_values)
    }

    return (
        <div className="list-toggle">
            {Object.keys(item_dict).map((item_display) => (
                <div key={item_dict[item_display]} className={"list-toggle-item" + (values.includes(item_dict[item_display]) ? " active" : " inactive")} onClick={() => {toggle_value(item_dict[item_display])}}>
                    <p>{item_display}</p>
                </div>
            ))}
        </div>
    );
}

export default ListToggleButtons;