import React, { Component } from 'react';
import { v4 as uuidv4 } from 'uuid';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

const prerequisiteTypes = {
    "ability-score": {
        name: "Ability Score",
        subTypes: {
            "strength": "Strength",
            "dexterity": "Dexterity",
            "constituition": "Constitution",
            "intelligence": "Intelligence",
            "wisdom": "Wisdom",
            "charisma": "Charisma",
        },
        value_type: "number"
    }
}

export const createPrerequisite = () => {
    return {
        "id": uuidv4(),
        "description": "Unnamed Prerequisite",
        "mappings": [],
    }
}

const createMapping = () => {
    return {
        "id": uuidv4(),
        "type": null,
        "subtype": null,
        "value": null
    }
}

class MappingItem extends Component {

    state = {
        toggle: false,
    }

    update_type = (e) => {
        var new_mapping = this.props.mapping
        if (e.target.value == ''){
            new_mapping.type = null
        } else {
            new_mapping.type = e.target.value
        }
        new_mapping.subtype = null
        new_mapping.value = null
        this.props.update_mapping(this.props.mapping.id, new_mapping)
    }

    update_subtype = (e) => {
        var new_mapping = this.props.mapping
        if (e.target.value == ''){
            new_mapping.subtype = null
        } else {
            new_mapping.subtype = e.target.value
        }
        new_mapping.value = null
        this.props.update_mapping(this.props.mapping.id, new_mapping)
    }

    update_value = (e) => {
        var new_mapping = this.props.mapping
        new_mapping.value = e.target.value
        this.props.update_mapping(this.props.mapping.id, new_mapping)
    }

    has_subtypes = () => {
        if (this.props.mapping.type == null){
            return false
        } else {
            return prerequisiteTypes[this.props.mapping.type].subTypes != null
        }
    }

    has_value = () => {
        if (this.props.mapping.subtype == null){
            return false
        } else {
            return prerequisiteTypes[this.props.mapping.type].value_type != null
        }
    }


    render(){

        return(
            <Row key={this.props.mapping.id} className="mapping-item">
                <Col xs={3} md={3}>
                    {this.props.mapping.type != null ? prerequisiteTypes[this.props.mapping.type].name : "-"}
                </Col>
                <Col xs={3} md={3}>
                    {this.props.mapping.subtype != null ? prerequisiteTypes[this.props.mapping.type].subTypes[this.props.mapping.subtype] : "-"}
                </Col>
                <Col xs={2} md={2}>
                    {this.props.mapping.value}
                </Col>
                <Col xs={2} md={2}>
                    <Button onClick={() => {this.setState({toggle: true})}}>Edit</Button>
                </Col>
                <Col xs={2} md={2}>
                    <DeleteConfirmationButton id={this.props.mapping.id} name="Mapping" delete_function={this.props.delete_mapping} override_button="Delete"/>
                </Col>
                <Col xs={12} md={12}>
                    <Modal show={this.state.toggle} size="md" className="select-modal mapping-edit">
                        <Modal.Header>Edit Mapping</Modal.Header>
                        <Modal.Body>
                            <Row>
                                <Col xs={4} md={4}>
                                    <Form.Group>
                                        <Form.Label>MAPPING TYPE</Form.Label>
                                        <Form.Control name="size" as="select" onChange={this.update_type} custom>
                                                <option selected="true" value="">-</option>
                                                {Object.keys(prerequisiteTypes).map((prerequisite_type) => (
                                                    <option key={prerequisite_type} selected={this.props.mapping.type == prerequisite_type} value={prerequisite_type}>{prerequisiteTypes[prerequisite_type].name}</option>
                                                ))}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>

                                { this.has_subtypes() &&
                                    <Col xs={4} md={4}>
                                        <Form.Group>
                                            <Form.Label>MAPPING SUBTYPE</Form.Label>
                                            <Form.Control name="size" as="select" onChange={this.update_subtype} custom>
                                                    <option selected="true" value={null}>-</option>
                                                    {Object.keys(prerequisiteTypes[this.props.mapping.type].subTypes).map((prerequisite_subtype) => (
                                                        <option key={prerequisite_subtype} selected={this.props.mapping.subType == prerequisite_subtype} value={prerequisite_subtype}>{prerequisiteTypes[this.props.mapping.type].subTypes[prerequisite_subtype]}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>
                                }

                                { this.has_value() &&
                                    <Col xs={4} md={4}>
                                        { prerequisiteTypes[this.props.mapping.type].value_type == 'number' &&
                                            <Form.Group>
                                                <Form.Label>VALUE</Form.Label>
                                                <Form.Control key={this.props.mapping.value} value={this.props.mapping.value} required name="value" type="number" min={0} onChange={this.update_value} />
                                            </Form.Group>
                                        }
                                    </Col>
                                }

                            </Row>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: false})}}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </Col>
            </Row>
        )
    }
}

const ListMappings = ({mappings, update_mapping, delete_mapping}) => {
    return (
        <div className="mapping-items">
            {mappings.map((mapping) => (
                <MappingItem mapping={mapping} update_mapping={update_mapping} delete_mapping={delete_mapping}/>
            ))}
        </div>
    )
}

class Prerequisite extends Component {

    handleChange = (e) => {
        var new_mapping = this.props.prerequisite
        new_mapping[e.target.name] = e.target.value
        this.props.update_prerequisite(this.props.prerequisite.id, new_mapping)
    }

    add_mapping = () => {
        var new_mapping = createMapping()
        var mappings = this.props.prerequisite.mappings
        mappings.push(new_mapping)

        
        var new_prerequisite = this.props.prerequisite
        new_prerequisite.mappings = mappings
        this.props.update_prerequisite(this.props.prerequisite.id, new_prerequisite)
    }

    delete_mapping = (id) => {
        var mappings = []
        for(var i = 0; i < this.props.prerequisite.mappings.length; i++){
            var mapping = this.props.prerequisite.mappings[i]
            if(mapping.id != id){
                mappings.push(mapping)
            }
        }

        var new_prerequisite = this.props.prerequisite
        new_prerequisite.mappings = mappings
        this.props.update_prerequisite(this.props.prerequisite.id, new_prerequisite)
    }


    update_mapping = (id, new_object) => {
        var mappings = []
        for(var i = 0; i < this.props.prerequisite.mappings.length; i++){
            var mapping = this.props.prerequisite.mappings[i]
            if(mapping.id == id){
                mappings.push(new_object)
            } else {
                mappings.push(mapping)
            }
        }

        var new_prerequisite = this.props.prerequisite
        new_prerequisite.mappings = mappings
        this.props.update_prerequisite(this.props.prerequisite.id, new_prerequisite)
    }

    render(){

        return(
            <Card key={this.props.prerequisite.id} className="prerequisite-item">
                <Card.Body>
                    <Row>
                        <Col xs={12} md={12}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control value={this.props.prerequisite.description} required name="description" type="text" placeholder="Enter Description" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>
                        
                        <Col xs={12} md={12}>
                            {this.props.prerequisite.mappings.length > 0 ?
                                <div>
                                    <hr/>
                                    <h4 className="list-title">
                                        {this.props.prerequisite.mappings.length} Mappings
                                        <span className="clickable-div add-button" onClick={() => {this.add_mapping()}}>
                                            <i className="fas fa-plus"></i>
                                        </span>
                                    </h4>
                                    <ListMappings mappings={this.props.prerequisite.mappings} update_mapping={this.update_mapping} delete_mapping={this.delete_mapping}/>
                                </div>
                                :
                                <div>
                                    <hr/>
                                    <h4 className="list-title">
                                        {this.props.prerequisite.mappings.length} Mappings
                                        <span className="clickable-div add-button" onClick={() => {this.add_mapping()}}>
                                            <i className="fas fa-plus"></i>
                                        </span>
                                    </h4>
                                </div>
                            }
                        </Col>
                        
                        <Col xs={2} md={2} className="mapping-snippet">
                            <br/>
                            <DeleteConfirmationButton id={this.props.prerequisite.id} name="Prerequisite" delete_function={this.props.delete_prerequisite} override_button="Delete"/>
                        </Col>
                        
                    </Row>
                </Card.Body>
            </Card>
        )

    };
}

export const ListPrerequisites = ({prerequisites, update_prerequisite, delete_prerequisite}) => {
    return (
        <div className="prerequisite-items">
            {prerequisites.map((prerequisite) => (
                <Prerequisite prerequisite={prerequisite} update_prerequisite={update_prerequisite} delete_prerequisite={delete_prerequisite}/>
            ))}
            <br/>
        </div>
    )
}