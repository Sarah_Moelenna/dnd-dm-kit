import React, { useState, useEffect } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Form from 'react-bootstrap/Form'

const ListItem = ({id, display, remove_function}) => {
    return (
        <div className="list-selector-item">
            <p><i className="fas fa-times clickable-icon" onClick={() => remove_function(id)}></i>{display}</p>
        </div>
    )
}

const ListSelector = ({input_value, items, onChange, use_keys, no_popover}) => {

    const [isVisible, setIsVisible] = useState(false);

    const [selected, setSelected] = useState(get_initial_selected());

    const toggle = () => setIsVisible(!isVisible);

    useEffect(() => {
        setSelected(get_initial_selected())
      }, [input_value]);

    function get_initial_selected(){
        var values = []
        for(var i = 0; i < items.length;i++){
            var id = use_keys ? items[i][use_keys[0]] : items[i]
            if(input_value.includes(id)){
                values.push(id)
            }
        }

        return values
    }

    const process_selection = (e) => {
        var new_selected = selected
        new_selected.push(e.target.value)
        setSelected(new_selected)
        onChange(new_selected.join(', '))
        toggle()
    }

    const remove_selection = (item) => {
        var new_selected = []
        for(var i = 0; i < selected.length;i++){
            if(selected[i] != item){
                new_selected.push(selected[i])
            }
        }
        setSelected(new_selected)
        onChange(new_selected.join(', '))
    }

    var popover = <Popover className="list-selector-popover">
        <Form.Control name="list" as="select" onChange={process_selection} custom>
            <option selected="selected" disabled="disabled" value="">Click to Select</option>
            {items.map((item, index) => 
                { if (!selected.includes(use_keys ? item[use_keys[0]] : item))
                    return (<option key={index} value={use_keys ? item[use_keys[0]] : item}>{use_keys ? item[use_keys[1]] : item}</option>)
                }
            )}
        </Form.Control>
    </Popover>

    var display_values = {}
    if(use_keys){
        for(var i = 0; i < items.length; i++){
            display_values[items[i][use_keys[0]]] = items[i][use_keys[1]]
        }
    }

    return (
        <div className="list-selector">
            <OverlayTrigger show={isVisible && no_popover != true} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                <div className="list-selector-items">
                    {selected.map((item, index) => ( 
                        <ListItem key={index} id={item} display={use_keys ? display_values[item] : item} remove_function={remove_selection} use_keys={use_keys}/>
                    ))}
                </div>
            </OverlayTrigger>
        </div>
    );
}

export default ListSelector;