import React, { Component } from 'react';
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_POST, DND_GET, DND_DELETE } from '.././shared/DNDRequests';
import Collapsible from '.././shared/Collapsible';
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form';



class AddToContentCollection extends Component {
    state = {
        collection_id: null
    }

    add_to_collection = (e) => {
        this.setState({collection_id: e.target.value})
    }

    handleSubmit = (event) => {
        
        if( this.state.collection_id == null){
            event.preventDefault();
            return
        }

        var data = {
            content_collection_id: this.state.collection_id
        }

        DND_POST(
            '/content-collection/' + this.props.resource + '/' + this.props.resource_id,
            data,
            (response) => {
                this.props.refresh_function()
                this.setState({ collection_id: null })
            },
            null
        ) 
        
        event.preventDefault();
      
    }

    render(){
        return(
            <div className="content-collection-add">
                
                <Form onSubmit={this.handleSubmit}>
                    <Row>
                        <Col xs={6} md={6}>
                            <Form.Group controlId="formBasicLevel">
                                <Form.Label>Content Collection</Form.Label>
                                <Form.Control name="collection" as="select" onChange={this.add_to_collection} custom>
                                        <option selected="true" value="">-</option>
                                        {this.props.collections.map((collection) => (
                                            <option key={collection.id} value={collection.id}>{collection.name}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>

                        <Col xs={2} md={2}>
                            { this.state.collection_id &&
                                <Button variant="primary" type="submit">
                                    Add to Collection
                                </Button>
                            }
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    };
}

class ContentCollectionResource extends Component {

    state = {
        existing: [],
        avaliable: []
    }

    componentDidMount() {
        this.get_collections()
    };

    get_collections = () => {
        DND_GET(
          '/content-collection/' + this.props.resource + '/' + this.props.resource_id,
          (jsondata) => {
            this.setState({ existing: jsondata.existing, avaliable: jsondata.avaliable })
          },
          null
        )
    };

    remove_from_collection = (collection_id) => {
        
        if( collection_id == null){
            return
        }

        var data = {
            content_collection_id: collection_id
        }

        DND_DELETE(
            '/content-collection/' + this.props.resource + '/' + this.props.resource_id,
            data,
            (response) => {
                this.get_collections()
            },
            null
        )
      
    }


    render(){
            if (this.state.existing.length == 0 && this.state.avaliable.length == 0){
                return null
            }

            return(
                <div className="content-collection-resources">
                    <Card>
                        <h2>Content Collections</h2>
                        <Card.Body>
                            { this.state.existing.length > 0 &&
                                <div className="content-collection-existing">
                                    <Card.Title>Shared In</Card.Title>
                                    <Row>
                                        <Col xs={4} md={4} className="cenetered-column">
                                            <p><b>Name</b></p>
                                        </Col>
                                        <Col xs={4} md={4} className="cenetered-column">
                                            <p><b>Is Shared</b></p>
                                        </Col>
                                        <Col xs={4} md={4} className="cenetered-column">
                                        </Col>
                                    </Row>
                                    <hr/>
                                    {this.state.existing.map((content_collection) => (
                                        <div key={content_collection.id} className="content-collection-item">
                                                <Row>
                                                    <Col xs={4} md={4} className="cenetered-column content-collection-name">
                                                        <p>{content_collection.name}</p>
                                                    </Col>
                                                    <Col xs={4} md={4} className="cenetered-column">
                                                        {content_collection.is_shareable ?
                                                            <p>Yes</p>
                                                            :
                                                            <p>No</p>
                                                        }
                                                    </Col>
                                                    <Col xs={4} md={4} className="cenetered-column">
                                                        {content_collection.is_shareable ?
                                                            <Button disabled>Remove From Collection</Button>
                                                            :
                                                            <Button onClick={() => {this.remove_from_collection(content_collection.id)}}>Remove From Collection</Button>
                                                        }
                                                    </Col>
                                                </Row>
                                            <hr/>
                                        </div>
                                    ))}
                                </div>
                            }
                            { this.state.avaliable.length > 0 &&
                                <Collapsible contents={<AddToContentCollection resource={this.props.resource} resource_id={this.props.resource_id} collections={this.state.avaliable} refresh_function={this.get_collections}/>} title="Add to Content Collection"/>
                            }
                        </Card.Body>
                    </Card>
                </div>
            );
    };
}

export default ContentCollectionResource;