import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MDEditor from '@uiw/react-md-editor';
import { Collapse } from 'reactstrap';
import Form from 'react-bootstrap/Form'
import { v4 as uuidv4 } from 'uuid';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton'
import { ListModifiers, createModifier } from '../shared/Modifier';
import { ListActions, createAction } from '../shared/Action';
import SpellSelection from '../spell/SpellSelection';


export const createOption = () => {
    return {
        "id": uuidv4(),
        "name": "Unnamed Option",
        "description": null,
        "spell_selection": [],
        "actions": [],
        "modifiers": [],
    }
}

class Option extends Component {

    state = {
        toggle: false,
    }

    update_text = (text) => {
        var new_option = this.props.option
        new_option.description = text
        this.props.update_option(this.props.option.id, new_option)
    }

    handleChange = (e) => {
        var new_option = this.props.option
        new_option[e.target.name] = e.target.value
        this.props.update_option(this.props.option.id, new_option)
    }

    add_modifier = () => {
        var new_modifier = createModifier()
        var modifiers = this.props.option.modifiers
        modifiers.push(new_modifier)

        
        var new_option = this.props.option
        new_option.modifiers = modifiers
        this.props.update_option(this.props.option.id, new_option)
    }

    add_action = () => {
        var new_action = createAction()
        var actions = this.props.option.actions
        actions.push(new_action)

        var new_option = this.props.option
        new_option.actions = actions
        this.props.update_option(this.props.option.id, new_option)
    }

    delete_modifier = (id) => {
        var modifiers = []
        for(var i = 0; i < this.props.option.modifiers.length; i++){
            var modifier = this.props.option.modifiers[i]
            if(modifier.id != id){
                modifiers.push(modifier)
            }
        }

        var new_option = this.props.option
        new_option.modifiers = modifiers
        this.props.update_option(this.props.option.id, new_option)
    }

    delete_action = (id) => {
        var actions = []
        for(var i = 0; i < this.props.option.actions.length; i++){
            var action = this.props.option.actions[i]
            if(action.id != id){
                actions.push(action)
            }
        }

        var new_option = this.props.option
        new_option.actions = actions
        this.props.update_option(this.props.option.id, new_option)
    }


    update_modifier = (id, new_object) => {
        var modifiers = []
        for(var i = 0; i < this.props.option.modifiers.length; i++){
            var modifier = this.props.option.modifiers[i]
            if(modifier.id == id){
                modifiers.push(new_object)
            } else {
                modifiers.push(modifier)
            }
        }

        var new_option = this.props.option
        new_option.modifiers = modifiers
        this.props.update_option(this.props.option.id, new_option)
    }

    update_action = (id, new_object) => {
        var actions = []
        for(var i = 0; i < this.props.option.actions.length; i++){
            var action = this.props.option.actions[i]
            if(action.id == id){
                actions.push(new_object)
            } else {
                actions.push(action)
            }
        }
        
        var new_option = this.props.option
        new_option.actions = actions
        this.props.update_option(this.props.option.id, new_option)
    }

    update_spell_selection = (spell_selection) => {
        var new_option = this.props.option
        new_option.spell_selection = {...spell_selection}
        this.props.update_option(this.props.option.id, new_option)
    }

    render(){

        return(
            <Row key={this.props.option.id} className="option-item">
                <Col xs={8} md={8} className="option-name">
                    <p>{this.props.option.name}</p>
                </Col>
                <Col xs={2} md={2} className="option-snippet">
                    <DeleteConfirmationButton id={this.props.option.id} name="Option" delete_function={this.props.delete_option} override_button="Delete"/>
                </Col>
                <Col xs={2} md={2} className="option-dropper">
                    <div className="clickable-div" onClick={() => {this.setState({toggle: !this.state.toggle})}}><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                </Col>
                <Col xs={12} md={12}>
                    <Collapse isOpen={this.state.toggle}>
                        {this.state.toggle &&
                            <Row>
                                <Col xs={6} md={6}>
                                    <Form.Group controlId="formBasicName">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control value={this.props.option.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                                    </Form.Group>
                                </Col>

                                <Col xs={6} md={6}>
                                </Col>

                                <Col xs={12} md={12}>
                                    <Form.Label>Description</Form.Label>
                                    <MDEditor
                                        value={this.props.option.description}
                                        preview="edit"
                                        onChange={this.update_text}
                                    />
                                </Col>
                                
                                <Col xs={12} md={12}>
                                    {this.props.option.modifiers.length > 0 ?
                                        <div>
                                            <hr/>
                                            <h4 className="list-title">
                                                {this.props.option.modifiers.length} Modifiers
                                                <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                            <ListModifiers modifiers={this.props.option.modifiers} update_modifier={this.update_modifier} delete_modifier={this.delete_modifier} display_type="RACE"/>
                                        </div>
                                        :
                                        <div>
                                            <hr/>
                                            <h4 className="list-title">
                                                {this.props.option.modifiers.length} Modifiers
                                                <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                        </div>
                                    }
                                </Col>

                                <Col xs={12} md={12}>
                                    {this.props.option.actions.length > 0 ?
                                        <div>
                                            <hr/>
                                            <h4 className="list-title">
                                                {this.props.option.actions.length} Actions
                                                <span className="clickable-div add-button" onClick={() => {this.add_action()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                            <ListActions actions={this.props.option.actions} update_action={this.update_action} delete_action={this.delete_action}/>
                                        </div>
                                        :
                                        <div>
                                            <hr/>
                                            <h4 className="list-title">
                                                {this.props.option.actions.length} Actions
                                                <span className="clickable-div add-button" onClick={() => {this.add_action()}}>
                                                    <i className="fas fa-plus"></i>
                                                </span>
                                            </h4>
                                        </div>
                                    }
                                </Col>

                                <Col xs={12} md={12}>
                                    <hr/>
                                    <h4 className="list-title">Spells Selection</h4>
                                    <SpellSelection
                                        spell_selection={this.props.option.spell_selection ? this.props.option.spell_selection : null}
                                        update_function={this.update_spell_selection}
                                    />
                                </Col>
                                
                            </Row>
                        }
                    </Collapse>
                </Col>
                
            </Row>
        )

    };
}

export const ListOptions = ({options, update_option, delete_option}) => {
    return (
        <div className="option-items">
            {options.map((option) => (
                <Option option={option} update_option={update_option} delete_option={delete_option}/>
            ))}
        </div>
    )
}