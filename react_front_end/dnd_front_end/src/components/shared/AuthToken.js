import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const get_auth_token = () => {
    return cookies.get('auth_token')
}