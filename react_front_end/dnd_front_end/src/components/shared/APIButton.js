import React, { Component } from 'react';
import { DND_POST, DND_PUT, DND_GET, DND_DELETE } from '.././shared/DNDRequests';
class APIButton extends Component {


    handleClick = (event) => {

        if(this.props.method == "POST"){
          DND_POST(
            this.props.api_endpoint,
            this.props.data,
            (response) => {
              if (this.props.post_response_function !== undefined){
                this.props.post_response_function()
              }
            },
            null
          )
        } else if(this.props.method == "GET"){
          DND_GET(
            this.props.api_endpoint,
            (response) => {
              if (this.props.post_response_function !== undefined){
                this.props.post_response_function()
              }
            },
            null
          )
        } else if(this.props.method == "PUT"){
          DND_PUT(
            this.props.api_endpoint,
            this.props.data,
            (response) => {
              if (this.props.post_response_function !== undefined){
                this.props.post_response_function()
              }
            },
            null
          )
        } else if(this.props.method == "DELETE"){
          DND_DELETE(
            this.props.api_endpoint,
            this.props.data,
            (response) => {
              if (this.props.post_response_function !== undefined){
                this.props.post_response_function()
              }
            },
            null
          )
        }

        event.preventDefault();
      
    }


    render(){
            var className = this.props.icon + ' clickable-icon' + (this.props.alt_style ? "-alt": "")
            return(
                <i className={className} onClick={this.handleClick}></i>
            );
    };
}

export default APIButton;