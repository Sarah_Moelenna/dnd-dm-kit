import React, { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const BackConfirmationButton = ({back_function, has_changes}) => {

    const [isVisible, setIsVisible] = useState(false);

    const toggle = () => setIsVisible(!isVisible);

    function process_selection(){
        toggle()
        back_function()
    }

    var popover = <Popover className="back-button-popover">
        <Popover.Title>Discard Unsaved Changes?</Popover.Title>
        <Popover.Content className="content">
            <Row>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => process_selection()}>
                        <p>Yes</p>
                    </div>
                </Col>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => toggle()}>
                        <p>No</p>
                    </div>
                </Col>
            </Row>
        </Popover.Content>
    </Popover>

    return (
        <span className="back-confirm-button">
            { has_changes == true ?
                <OverlayTrigger show={isVisible} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                    <i className="fas fa-chevron-left clickable-icon"></i>
                </OverlayTrigger>
            :
                <i className="fas fa-chevron-left clickable-icon" onClick={back_function}></i>
            }
        </span>
    );
}

export default BackConfirmationButton;