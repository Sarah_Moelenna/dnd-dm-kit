const { REACT_APP_API_URL, REACT_APP_SOCKET_URL } = process.env;

export const get_api_url = () => {
    if(process.env.NODE_ENV == 'development'){
        return REACT_APP_API_URL;
    } else {
        return window.API_URL;
    }
}

export const get_socket_url = () => {
    if(process.env.NODE_ENV == 'development'){
        return REACT_APP_SOCKET_URL;
    } else {
        return window.SOCKET_URL;
    }
}