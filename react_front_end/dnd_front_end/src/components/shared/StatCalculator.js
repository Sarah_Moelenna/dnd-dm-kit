import { get_choices } from '../shared/Choices';
import { ModifierSubTypes} from '../monster/Data';
import { v4 as uuidv4 } from 'uuid';

export const calculate_stats = (
        base_stats,
        class_data,
        subclass_data,
        character_class_data,
        race_data,
        race_choices,
        background_data,
        background_choices,
        feat_data,
        feat_choices,
        inventory,
        class_spell_choices,
        spell_data
    ) => {
        let modifiers = []

        create_base_stats(base_stats)

        if(class_data && character_class_data){
            modifiers.push(...get_class_modifiers(class_data, character_class_data, base_stats))
        }
        if(subclass_data && character_class_data){
            modifiers.push(...get_subclass_modifiers(subclass_data, character_class_data, base_stats))
        }
        if(race_data && race_choices){
            modifiers.push(...get_race_modifiers(race_data, race_choices, base_stats))
        }
        if(background_data && background_choices){
            modifiers.push(...process_modifier_list(background_data.modifiers, background_data.id, background_choices, base_stats))
        }
        if(feat_data && feat_choices){
            modifiers.push(...get_feat_modifiers(feat_data, feat_choices, base_stats))
        }
        if(inventory){
            modifiers.push(...get_inventory_modifiers(inventory, base_stats))
        }

        add_race_data_to_base_stats(base_stats, race_data)
        add_spell_choices(base_stats, class_spell_choices)

        update_base_stats_with_modifiers(base_stats, modifiers)
        calculate_ability_modifiers(base_stats)
        adjust_stats_by_ability_modifiers(base_stats)
        apply_proficiency_bonuses(base_stats)
        calculate_hit_points(character_class_data, class_data, base_stats)
        apply_spell_and_resources_data(subclass_data, class_data, character_class_data, base_stats)
        build_spell_list(base_stats, class_data, subclass_data, spell_data, class_spell_choices, character_class_data)
        console.log(base_stats)
        return base_stats
}

const proficiency_bonus = {
    "1": 2,
    "2": 2,
    "3": 2,
    "4": 2,
    "5": 3,
    "6": 3,
    "7": 3,
    "8": 3,
    "9": 4,
    "10": 4,
    "11": 4,
    "12": 4,
    "13": 5,
    "14": 5,
    "15": 5,
    "16": 5,
    "17": 6,
    "18": 6,
    "19": 6,
    "20": 6
}

const add_race_data_to_base_stats = (base_stats, race_data) => {
    let has_speed_data = race_data && race_data['weight_speeds'] && race_data['weight_speeds']['normal']
    let speed_data = has_speed_data ? race_data['weight_speeds']['normal'] : {}
    base_stats['innate-speed-burrowing'] = has_speed_data && speed_data['burrow'] ? speed_data['burrow'] : 0
    base_stats['innate-speed-climbing'] = has_speed_data && speed_data['climb'] ? speed_data['climb'] : 0
    base_stats['innate-speed-flying'] = has_speed_data && speed_data['fly'] ? speed_data['fly'] : 0
    base_stats['innate-speed-swimming'] = has_speed_data && speed_data['swim'] ? speed_data['swim'] : 0
    base_stats['innate-speed-walking'] = has_speed_data && speed_data['walk'] ? speed_data['walk'] : 0
    base_stats['speed-burrowing'] = 0
    base_stats['speed-climbing'] = 0
    base_stats['speed-flying'] = 0
    base_stats['speed-swimming'] = 0
    base_stats['speed-walking'] = 0
    base_stats['size'] = race_data && race_data['size'] ? race_data['size'] : 'Medium'
}

const add_spell_choices = (base_stats, class_spell_choices) => {
    Object.values(class_spell_choices).forEach((spell_ids) => {
        base_stats['spell_ids'].push(...spell_ids)
    })
}

const calculate_hit_points = (character_class_data, class_data, base_stats) => {
    // add hit points per level
    base_stats['hit-points'] = base_stats['hit-points'] + base_stats['hit-points-per-level'] * base_stats['character-level']
    // add rolled hp
    base_stats['hit-points'] = base_stats['hit-points'] + base_stats['rolled-hit-points']
    // class hp
    Object.values(character_class_data).forEach((character_class) => {
        let single_class_data = class_data[character_class.id]
        if(single_class_data){
            // add base hitpoint from class
            base_stats['hit-points'] = base_stats['hit-points'] + single_class_data.base_hit_points
            // add hp ability modifier from class
            if(single_class_data.hit_point_attribute_id == 1){
                base_stats['hit-points'] = base_stats['hit-points'] + base_stats['strength-modifier'] * (character_class.level)
            } else if(single_class_data.hit_point_attribute_id == 2){
                base_stats['hit-points'] = base_stats['hit-points'] + base_stats['dexterity-modifier'] * (character_class.level)
            } else if(single_class_data.hit_point_attribute_id == 3){
                base_stats['hit-points'] = base_stats['hit-points'] + base_stats['constitution-modifier'] * (character_class.level)
            } else if(single_class_data.hit_point_attribute_id == 4){
                base_stats['hit-points'] = base_stats['hit-points'] + base_stats['intelligence-modifier'] * (character_class.level)
            } else if(single_class_data.hit_point_attribute_id == 5){
                base_stats['hit-points'] = base_stats['hit-points'] + base_stats['wisdom-modifier'] * (character_class.level)
            } else if(single_class_data.hit_point_attribute_id == 6){
                base_stats['hit-points'] = base_stats['hit-points'] + base_stats['charisma-modifier'] * (character_class.level)
            }
            // add hit die
            if(character_class.level > 1){
                if(!base_stats['hit-die'][single_class_data.hit_dice]){
                    base_stats['hit-die'][single_class_data.hit_dice] = character_class.level - 1
                } else {
                    base_stats['hit-die'][single_class_data.hit_dice] = base_stats['hit-die'][single_class_data.hit_dice] + character_class.level - 1
                }
            }
        }
    })

    if(!base_stats['current-hit-points']){
        base_stats['current-hit-points'] = base_stats['hit-points']
    }
}

const multiclass_spell_slots = {
    "1": {"1": 2, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
    "2": {"1": 3, "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
    "3": {"1": 4, "2": 2, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
    "4": {"1": 4, "2": 3, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
    "5": {"1": 4, "2": 3, "3": 2, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
    "6": {"1": 4, "2": 3, "3": 3, "4": null, "5": null, "6": null, "7": null, "8": null, "9": null},
    "7": {"1": 4, "2": 3, "3": 3, "4": 1, "5": null, "6": null, "7": null, "8": null, "9": null},
    "8": {"1": 4, "2": 3, "3": 3, "4": 2, "5": null, "6": null, "7": null, "8": null, "9": null},
    "9": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 1, "6": null, "7": null, "8": null, "9": null},
    "10": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": null, "7": null, "8": null, "9": null},
    "11": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": 1, "7": null, "8": null, "9": null},
    "12": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": 1, "7": null, "8": null, "9": null},
    "13": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": 1, "7": 1, "8": null, "9": null},
    "14": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": 1, "7": 1, "8": null, "9": null},
    "15": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": 1, "7": 1, "8": 1, "9": null},
    "16": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": 1, "7": 1, "8": 1, "9": null},
    "17": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": 1, "7": 1, "8": 1, "9": 1},
    "18": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 2, "6": 1, "7": 1, "8": 1, "9": 1},
    "19": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 3, "6": 2, "7": 1, "8": 1, "9": 1},
    "20": {"1": 4, "2": 3, "3": 3, "4": 3, "5": 3, "6": 2, "7": 2, "8": 1, "9": 1},
}

const apply_spell_and_resources_data = (subclass_data, class_data, character_class_data, base_stats,) => {

    if(!(Object.keys(class_data).length > 0) || !(Object.keys(character_class_data).length > 0)){
        return
    }
    let spell_casting_classses = 0
    let spell_casting_levels = 0

    Object.keys(character_class_data).forEach((class_id) => {
        let character_class_info = character_class_data[class_id]
        let dnd_class = class_data[class_id]
        let subclass = null
        if(character_class_info.subclass_id){
            subclass = subclass_data[character_class_info.subclass_id]
        }
        //spells
        if(subclass && subclass.override_spells == true){
            if(subclass.has_spells == true){
                base_stats['spell_data']["cantrip"] = base_stats['spell_data']["cantrip"] + subclass.spell_data[character_class_info.level]["cantrip"]
                base_stats['spell_data']["1"] = base_stats['spell_data']["1"] + subclass.spell_data[character_class_info.level]["1"]
                base_stats['spell_data']["2"] = base_stats['spell_data']["2"] + subclass.spell_data[character_class_info.level]["2"]
                base_stats['spell_data']["3"] = base_stats['spell_data']["3"] + subclass.spell_data[character_class_info.level]["3"]
                base_stats['spell_data']["4"] = base_stats['spell_data']["4"] + subclass.spell_data[character_class_info.level]["4"]
                base_stats['spell_data']["5"] = base_stats['spell_data']["5"] + subclass.spell_data[character_class_info.level]["5"]
                base_stats['spell_data']["6"] = base_stats['spell_data']["6"] + subclass.spell_data[character_class_info.level]["6"]
                base_stats['spell_data']["7"] = base_stats['spell_data']["7"] + subclass.spell_data[character_class_info.level]["7"]
                base_stats['spell_data']["8"] = base_stats['spell_data']["8"] + subclass.spell_data[character_class_info.level]["8"]
                base_stats['spell_data']["9"] = base_stats['spell_data']["9"] + subclass.spell_data[character_class_info.level]["9"]
                spell_casting_classses += 1
                spell_casting_levels += Math.floor(character_class_info.level * dnd_class.multiclass_value)
            }
        } else {
            if(dnd_class.has_spells == true){
                base_stats['spell_data']["cantrip"] = base_stats['spell_data']["cantrip"] + dnd_class.spell_data[character_class_info.level]["cantrip"]
                base_stats['spell_data']["1"] = base_stats['spell_data']["1"] + dnd_class.spell_data[character_class_info.level]["1"]
                base_stats['spell_data']["2"] = base_stats['spell_data']["2"] + dnd_class.spell_data[character_class_info.level]["2"]
                base_stats['spell_data']["3"] = base_stats['spell_data']["3"] + dnd_class.spell_data[character_class_info.level]["3"]
                base_stats['spell_data']["4"] = base_stats['spell_data']["4"] + dnd_class.spell_data[character_class_info.level]["4"]
                base_stats['spell_data']["5"] = base_stats['spell_data']["5"] + dnd_class.spell_data[character_class_info.level]["5"]
                base_stats['spell_data']["6"] = base_stats['spell_data']["6"] + dnd_class.spell_data[character_class_info.level]["6"]
                base_stats['spell_data']["7"] = base_stats['spell_data']["7"] + dnd_class.spell_data[character_class_info.level]["7"]
                base_stats['spell_data']["8"] = base_stats['spell_data']["8"] + dnd_class.spell_data[character_class_info.level]["8"]
                base_stats['spell_data']["9"] = base_stats['spell_data']["9"] + dnd_class.spell_data[character_class_info.level]["9"]
                spell_casting_classses += 1
                spell_casting_levels += Math.floor(character_class_info.level * dnd_class.multiclass_value)
            }
        }
        
        //resources
        if(subclass && subclass.override_resources == true){
            subclass.resources.forEach((resource) => {
                if(resource.resource_count[character_class_info.level] > 0){
                    base_stats['resources'].push(
                        {
                            'id': resource.id,
                            'name': resource.name,
                            'snippet': resource.snippet,
                            'action_type': 3,
                            'activation_type': resource.activation_type,
                            'limited_use': {
                                'resetType': parseInt(resource.reset_type),
                                'maxUses': parseInt(resource.resource_count[character_class_info.level])
                            }
                        }
                    )
                }
            })
        } else {
            dnd_class.resources.forEach((resource) => {
                if(resource.resource_count[character_class_info.level] > 0){
                    base_stats['resources'].push(
                        {
                            'id': resource.id,
                            'name': resource.name,
                            'snippet': resource.snippet,
                            'action_type': 3,
                            'activation_type': resource.activation_type,
                            'limited_use': {
                                'resetType': parseInt(resource.reset_type),
                                'maxUses': parseInt(resource.resource_count[character_class_info.level])
                            }
                        }
                    )
                }
            })
        }
    })

    if(spell_casting_classses > 1){
        base_stats['spell_data']["1"] = multiclass_spell_slots[spell_casting_levels]["1"]
        base_stats['spell_data']["2"] = multiclass_spell_slots[spell_casting_levels]["2"]
        base_stats['spell_data']["3"] = multiclass_spell_slots[spell_casting_levels]["3"]
        base_stats['spell_data']["4"] = multiclass_spell_slots[spell_casting_levels]["4"]
        base_stats['spell_data']["5"] = multiclass_spell_slots[spell_casting_levels]["5"]
        base_stats['spell_data']["6"] = multiclass_spell_slots[spell_casting_levels]["6"]
        base_stats['spell_data']["7"] = multiclass_spell_slots[spell_casting_levels]["7"]
        base_stats['spell_data']["8"] = multiclass_spell_slots[spell_casting_levels]["8"]
        base_stats['spell_data']["9"] = multiclass_spell_slots[spell_casting_levels]["9"]
    }
}

const build_spell_list = (base_stats, class_data, subclass_data, spell_data, class_spell_choices, character_class_data) => {
    base_stats['spells'] = {
        "cantrip": [],
        "1": [],
        "2": [],
        "3": [],
        "4": [],
        "5": [],
        "6": [],
        "7": [],
        "8": [],
        "9": []
    }
    Object.keys(character_class_data).forEach((class_id) => {
        let character_class_info = character_class_data[class_id]
        let dnd_class = class_data[class_id]
        let subclass = null
        let spell_casting_attribute = dnd_class.spell_casting_attribute_id
        if(character_class_info.subclass_id){
            subclass = subclass_data[character_class_info.subclass_id]
        }
        if(subclass && subclass.override_spells == true){
            spell_casting_attribute = subclass.spell_casting_attribute_id
        }
        if(class_spell_choices[class_id]){
            class_spell_choices[class_id].forEach((spell_id) => {
                if(spell_data[spell_id]){
                    if(spell_data[spell_id].scale_type == "spellscale" || spell_data[spell_id].scale_type == "spelllevel"){
                        for(let i = parseInt(spell_data[spell_id].level); i <= 9; i++){
                            base_stats['spells'][i].push(
                                {
                                    "spell_casting_attribute": spell_casting_attribute,
                                    "name": spell_data[spell_id].name,
                                    "id": spell_data[spell_id].id
                                }
                            )
                        }
                    } else if(spell_data[spell_id].scale_type == "characterlevel"){
                        if(spell_data[spell_id].level == 0){
                            base_stats['spells']['cantrip'].push(
                                {
                                    "spell_casting_attribute": spell_casting_attribute,
                                    "name": spell_data[spell_id].name,
                                    "id": spell_data[spell_id].id
                                }
                            )
                        } else {
                            base_stats['spells'][spell_data[spell_id].level].push(
                                {
                                    "spell_casting_attribute": spell_casting_attribute,
                                    "name": spell_data[spell_id].name,
                                    "id": spell_data[spell_id].id
                                }
                            )
                        }
                    }
                }
            })
        }
    })
}

const create_base_stats = (base_stats) => {

    base_stats['spell_ids'] = []
    base_stats['actions'] = []
    base_stats['feat_ids'] = []
    base_stats['spell_data'] = {"cantrip": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0, "9": 0}
    //senses
    base_stats['senses'] = {}
    //features
    base_stats['features'] = {'class': [], 'race': []}
    //hp
    base_stats['hit-points'] = 0
    base_stats['hit-points-per-level'] = 0
    base_stats['hit-die'] = {}
    // lists
    base_stats['language'] = new Set()
    base_stats['resistance'] = new Set()
    base_stats['immunity'] = new Set()
    base_stats['vulnerability'] = new Set()
    base_stats['ignore-rules'] = new Set()
    base_stats['advantage'] = []
    base_stats['disadvantage'] = []
    base_stats['resources'] = []
    //saving throws and ability checks
    base_stats["charisma-ability-checks"] = 0
    base_stats["charisma-saving-throws"] = 0
    base_stats["constitution-ability-checks"] = 0
    base_stats["constitution-saving-throws"] = 0
    base_stats["dexterity-ability-checks"] = 0
    base_stats["dexterity-attacks"] = 0
    base_stats["dexterity-saving-throws"] = 0
    base_stats["intelligence-ability-checks"] = 0
    base_stats["intelligence-saving-throws"] = 0
    base_stats["strength-ability-checks"] = 0
    base_stats["strength-attacks"] = 0
    base_stats["strength-saving-throws"] = 0
    base_stats["wisdom-ability-checks"] = 0
    base_stats["wisdom-saving-throws"] = 0
    //skills
    base_stats["acrobatics"] = 0
    base_stats["animal-handling"] = 0
    base_stats["arcana"] = 0
    base_stats["athletics"] = 0
    base_stats["deception"] = 0
    base_stats["history"] = 0
    base_stats["insight"] = 0
    base_stats["intimidation"] = 0
    base_stats["investigation"] = 0
    base_stats["medicine"] = 0
    base_stats["nature"] = 0
    base_stats["perception"] = 0
    base_stats["performance"] = 0
    base_stats["persuasion"] = 0
    base_stats["religion"] = 0
    base_stats["sleight-of-hand"] = 0
    base_stats["stealth"] = 0
    base_stats["survival"] = 0
    // passive skills
    base_stats["passive-investigation"] = 10
    base_stats["passive-perception"] = 10
    base_stats["passive-insight"] = 10
    //proficieny
    base_stats["proficiency-bonus"] = proficiency_bonus[String(base_stats['character-level'])]
    //armour class
    base_stats['armor-class'] = 10
    //initiative
    base_stats['initiative'] = 0
    //item proficies
    base_stats['misc_proficiencies'] = {}
    //proficiencies [amount, round up?]
    base_stats['proficiencies'] = {
        //skills
        "acrobatics": [0, false],
        "animal-handling": [0, false],
        "arcana": [0, false],
        "athletics": [0, false],
        "deception": [0, false],
        "history": [0, false],
        "insight": [0, false],
        "intimidation": [0, false],
        "investigation": [0, false],
        "medicine": [0, false],
        "nature": [0, false],
        "perception": [0, false],
        "performance": [0, false],
        "persuasion": [0, false],
        "religion": [0, false],
        "sleight-of-hand": [0, false],
        "stealth": [0, false],
        "survival": [0, false],
        //iniative
        "initiative": [0, false],
        //other stuff?
        "charisma-ability-checks": [0, false],
        "charisma-saving-throws": [0, false],
        "constitution-ability-checks": [0, false],
        "constitution-saving-throws": [0, false],
        "dexterity-ability-checks": [0, false],
        "dexterity-attacks": [0, false],
        "dexterity-saving-throws": [0, false],
        "intelligence-ability-checks": [0, false],
        "intelligence-saving-throws": [0, false],
        "saving-throws": [0, false],
        "strength-ability-checks": [0, false],
        "strength-attacks": [0, false],
        "strength-saving-throws": [0, false],
        "wisdom-ability-checks": [0, false],
        "wisdom-saving-throws": [0, false],
        //items + attacks
        "alchemists-supplies": [0, false],
        "bagpipes": [0, false],
        "battleaxe": [0, false],
        "birdpipes": [0, false],
        "blowgun": [0, false],
        "boomerang": [0, false],
        "breastplate": [0, false],
        "brewers-supplies": [0, false],
        "calligraphers-supplies": [0, false],
        "carpenters-tools": [0, false],
        "cartographers-tools": [0, false],
        "chain-mail": [0, false],
        "chain-shirt": [0, false],
        "club": [0, false],
        "cobblers-tools": [0, false],
        "cooks-utensils": [0, false],
        "crossbow-hand": [0, false],
        "crossbow-heavy": [0, false],
        "crossbow-light": [0, false],
        "dagger": [0, false],
        "dart": [0, false],
        "dice-set": [0, false],
        "disguise-kit": [0, false],
        "doublebladed-scimitar": [0, false],
        "dragonchess-set": [0, false],
        "drum": [0, false],
        "dulcimer": [0, false],
        "firearms": [0, false],
        "flail": [0, false],
        "flute": [0, false],
        "forgery-kit": [0, false],
        "glaive": [0, false],
        "glassblowers-tools": [0, false],
        "glaur": [0, false],
        "greataxe": [0, false],
        "greatclub": [0, false],
        "greatsword": [0, false],
        "halberd": [0, false],
        "half-plate": [0, false],
        "hand-drum": [0, false],
        "handaxe": [0, false],
        "heavy-armor": [0, false],
        "herbalism-kit": [0, false],
        "hide": [0, false],
        "horn": [0, false],
        "improvised-weapons": [0, false],
        "javelin": [0, false],
        "jewelers-tools": [0, false],
        "lance": [0, false],
        "leather": [0, false],
        "leatherworkers-tools": [0, false],
        "light-armor": [0, false],
        "light-hammer": [0, false],
        "longbow": [0, false],
        "longhorn": [0, false],
        "longsword": [0, false],
        "lute": [0, false],
        "lyre": [0, false],
        "mace": [0, false],
        "martial-weapons": [0, false],
        "masons-tools": [0, false],
        "maul": [0, false],
        "medium-armor": [0, false],
        "melee-attacks": [0, false],
        "mithril-plate-armor": [0, false],
        "morningstar": [0, false],
        "nature": [0, false],
        "navigators-tools": [0, false],
        "net": [0, false],
        "padded": [0, false],
        "painters-supplies": [0, false],
        "pan-flute": [0, false],
        "pike": [0, false],
        "plate": [0, false],
        "playing-card-set": [0, false],
        "poisoners-kit": [0, false],
        "potters-tools": [0, false],
        "quarterstaff": [0, false],
        "ranged-attacks": [0, false],
        "rapier": [0, false],
        "ring-mail": [0, false],
        "scale-mail": [0, false],
        "scimitar": [0, false],
        "self": [0, false],
        "shawm": [0, false],
        "shields": [0, false],
        "shortbow": [0, false],
        "shortsword": [0, false],
        "sickle": [0, false],
        "simple-weapons": [0, false],
        "sling": [0, false],
        "smiths-tools": [0, false],
        "songhorn": [0, false],
        "spear": [0, false],
        "spell-attacks": [0, false],
        "splint": [0, false],
        "steel-shield": [0, false],
        "studded-leather": [0, false],
        "tantan": [0, false],
        "thelarr": [0, false],
        "thieves-tools": [0, false],
        "threedragon-ante-set": [0, false],
        "tinkers-tools": [0, false],
        "tocken": [0, false],
        "trident": [0, false],
        "vehicles-land": [0, false],
        "vehicles-water": [0, false],
        "viol": [0, false],
        "war-pick": [0, false],
        "wargong": [0, false],
        "warhammer": [0, false],
        "weavers-tools": [0, false],
        "whip": [0, false],
        "whistlestick": [0, false],
        "woodcarvers-tools": [0, false],
        "yarting": [0, false],
        "yklwa": [0, false],
        "zulkoon": [0, false],
    }

    // add default actions
    base_stats['actions'].push(
        {
            'id': uuidv4(),
            'name': "Actions in Combat",
            'snippet': "Attack, Cast a Spell, Dash, Disengage, Dodge, Grapple, Help, Hide, Improvise, Ready, Search, Shove, Use an Object",
            'action_type': 4,
            'activation': {'activationType': 1}
        }
    )
    base_stats['actions'].push(
        {
            'id': uuidv4(),
            'name': "Actions in Combat",
            'snippet': "Two-Weapon Fighting",
            'action_type': 4,
            'activation': {'activationType': 3}
        }
    )
    base_stats['actions'].push(
        {
            'id': uuidv4(),
            'name': "Actions in Combat",
            'snippet': "Opportunity Attack",
            'action_type': 4,
            'activation': {'activationType': 4}
        }
    )
    base_stats['actions'].push(
        {
            'id': uuidv4(),
            'name': "Actions in Combat",
            'snippet': "Interact with an Object",
            'action_type': 4,
            'activation': {'activationType': 8}
        }
    )
    base_stats['actions'].push(
        {
            'id': uuidv4(),
            'name': "Unarmed Strike",
            'action_type': 1,
            'activation': {'activationType': 1},
            "attack_type_range": 1,
            "damage_type_id": 1,
            "dice": {'fixedValue': 1},
            "range": {'range': 5},
            "ability_modifier_stat_id": 1
        }
    )

}

const update_base_stats_with_modifiers = (base_stats, modifiers) => {

    /*
    bonus++
    damage
    advantage++
    disadvantage++
    resistance++
    immunity++
    vulnerability++
    sense++
    set++
    proficiency++
    language++
    expertise++
    half-proficiency++
    feat
    carrying-capacity
    natural-weapon
    stealth-disadvantage
    speed-reduction
    melee-weapon-attack
    ranged-weapon-attack
    weapon-property
    half-proficiency-round-up++
    favored-enemy
    ignore++
    eldritch-blast
    replace-damage-type
    twice-proficiency++
    protection
    stacking-bonus
    set-base++
    ignore-weapon-property
    size++
    */

    //apply set first (changes the base value of stats)
    modifiers.forEach((modifier) => {
        if(modifier.type == 'set'){
            if(!Object.keys(base_stats).includes(modifier.sub_type)){
                base_stats[modifier.sub_type] = 0
            }
            base_stats[modifier.sub_type] = modifier.dice.fixedValue
        }
        else if(modifier.type == 'set-base'){
            base_stats['senses'][modifier.sub_type] = {
                value: modifier.dice.fixedValue,
                display: modifier.friendly_sub_type_name
            }
        }
        
    })

    modifiers.forEach((modifier) => {
        if(modifier.type == 'bonus'){
            if(!Object.keys(base_stats).includes(modifier.sub_type)){
                base_stats[modifier.sub_type] = 0
            }
            base_stats[modifier.sub_type] = base_stats[modifier.sub_type] + modifier.dice.fixedValue
        }
        else if(modifier.type == 'language'){
            base_stats['language'].add(modifier.friendly_sub_type_name)
        }
        else if(modifier.type == 'resistance'){
            base_stats['resistance'].add(modifier.friendly_sub_type_name)
        }
        else if(modifier.type == 'immunity'){
            base_stats['immunity'].add(modifier.friendly_sub_type_name)
        }
        else if(modifier.type == 'vulnerability'){
            base_stats['vulnerability'].add(modifier.friendly_sub_type_name)
        }
        else if(modifier.type == 'ignore'){
            base_stats['ingore-rules'].add(modifier.sub_type)
        }
        else if(modifier.type == 'advantage'){
            base_stats['advantage'].push(
                {
                    display: modifier.friendly_sub_type_name,
                    name: modifier.sub_type,
                    restriction: modifier.restriction,
                }
            )
        }
        else if(modifier.type == 'disadvantage'){
            base_stats['disadvantage'].push(
                {
                    display: modifier.friendly_sub_type_name,
                    name: modifier.sub_type,
                    restriction: modifier.restriction,
                }
            )
        }
        else if(modifier.type == 'proficiency'){
            if(Object.keys(base_stats['proficiencies']).includes(modifier.sub_type)){
                base_stats['proficiencies'][modifier.sub_type] = [Math.max(base_stats['proficiencies'][modifier.sub_type][0], 1), false]
            }
        }
        else if(modifier.type == 'expertise'){
            if(Object.keys(base_stats['proficiencies']).includes(modifier.sub_type)){
                base_stats['proficiencies'][modifier.sub_type] = [Math.max(base_stats['proficiencies'][modifier.sub_type][0], 2), false]
            }
        }
        else if(modifier.type == 'half-proficiency'){
            if(Object.keys(base_stats['proficiencies']).includes(modifier.sub_type)){
                base_stats['proficiencies'][modifier.sub_type] = [Math.max(base_stats['proficiencies'][modifier.sub_type][0], 0.5), false]
            }
        }
        else if(modifier.type == 'half-proficiency-round-up'){
            if(Object.keys(base_stats['proficiencies']).includes(modifier.sub_type)){
                base_stats['proficiencies'][modifier.sub_type] = [Math.max(base_stats['proficiencies'][modifier.sub_type][0], 0.5), true]
            }
        }
        else if(modifier.type == 'twice-proficiency'){
            if(Object.keys(base_stats['proficiencies']).includes(modifier.sub_type)){
                base_stats['proficiencies'][modifier.sub_type] = [Math.max(base_stats['proficiencies'][modifier.sub_type][0], 2), false]
            }
        }
        else if(modifier.type == 'size'){
            base_stats['size'] = modifier.friendly_sub_type_name
        }else if(modifier.type == 'sense'){
            if(!Object.keys(base_stats['senses']).includes(modifier.sub_type)){
                base_stats['senses'][modifier.sub_type] = {
                    value: modifier.dice.fixedValue,
                    display: modifier.friendly_sub_type_name
                }
            }

            base_stats['senses'][modifier.sub_type].value = base_stats['senses'][modifier.sub_type].value + modifier.dice.fixedValue
        }
        
    })
    return base_stats
}

const apply_proficiency_bonuses = (base_stats) => {
    Object.keys(base_stats['proficiencies']).forEach((proficiency_name) => {
        let proficiency_data = base_stats['proficiencies'][proficiency_name]
        if(Object.keys(base_stats).includes(proficiency_name)){
            let adjust_amount = base_stats["proficiency-bonus"] * proficiency_data[0]
            if(proficiency_data[0] == true){
                adjust_amount = Math.floor(adjust_amount)
            } else {
                adjust_amount = Math.ceil(adjust_amount)
            }
            base_stats[proficiency_name] = base_stats[proficiency_name] + adjust_amount
        } else if(proficiency_data[0] > 0){
            base_stats['misc_proficiencies'][proficiency_name] = proficiency_data[0]
        }
    })
    
    base_stats["passive-investigation"] = base_stats["passive-investigation"] + base_stats["proficiency-bonus"] * base_stats['proficiencies']['investigation'][0]
    base_stats["passive-perception"] = base_stats["passive-perception"] + base_stats["proficiency-bonus"] * base_stats['proficiencies']['perception'][0]
    base_stats["passive-insight"] = base_stats["passive-insight"] + base_stats["proficiency-bonus"] * base_stats['proficiencies']['insight'][0]
}

const adjust_stats_by_ability_modifiers = (base_stats) => {
    //skills
    base_stats["acrobatics"] = base_stats["acrobatics"] + base_stats['dexterity-modifier']
    base_stats["animal-handling"] = base_stats["animal-handling"] + base_stats['wisdom-modifier']
    base_stats["arcana"] = base_stats["arcana"] + base_stats['intelligence-modifier']
    base_stats["athletics"] = base_stats["athletics"] + base_stats['strength-modifier']
    base_stats["deception"] = base_stats["deception"] + base_stats['charisma-modifier']
    base_stats["history"] = base_stats["history"] + base_stats['intelligence-modifier']
    base_stats["insight"] = base_stats["insight"] + base_stats['wisdom-modifier']
    base_stats["intimidation"] = base_stats["intimidation"] + base_stats['charisma-modifier']
    base_stats["investigation"] = base_stats["investigation"] + base_stats['intelligence-modifier']
    base_stats["medicine"] = base_stats["medicine"] + base_stats['wisdom-modifier']
    base_stats["nature"] = base_stats["nature"] + base_stats['intelligence-modifier']
    base_stats["perception"] = base_stats["perception"] + base_stats['wisdom-modifier']
    base_stats["performance"] = base_stats["performance"] + base_stats['charisma-modifier']
    base_stats["persuasion"] = base_stats["persuasion"] + base_stats['charisma-modifier']
    base_stats["religion"] = base_stats["religion"] + base_stats['intelligence-modifier']
    base_stats["sleight-of-hand"] = base_stats["sleight-of-hand"] + base_stats['dexterity-modifier']
    base_stats["stealth"] = base_stats["stealth"] + base_stats['dexterity-modifier']
    base_stats["survival"] = base_stats["survival"] + base_stats['wisdom-modifier']
    //passive_skills
    base_stats["passive-investigation"] = base_stats["passive-investigation"] + base_stats['intelligence-modifier']
    base_stats["passive-perception"] = base_stats["passive-perception"] + base_stats['wisdom-modifier']
    base_stats["passive-insight"] = base_stats["passive-insight"] + base_stats['wisdom-modifier']
    //initiative
    base_stats['initiative'] = base_stats['initiative'] + base_stats['dexterity-modifier']
    //saving throws and checks
    base_stats["charisma-ability-checks"] = base_stats["charisma-ability-checks"] + base_stats['charisma-modifier']
    base_stats["charisma-saving-throws"] = base_stats["charisma-saving-throws"] + base_stats['charisma-modifier']
    base_stats["constitution-ability-checks"] = base_stats["constitution-ability-checks"] + base_stats['constitution-modifier']
    base_stats["constitution-saving-throws"] = base_stats["constitution-saving-throws"] + base_stats['constitution-modifier']
    base_stats["dexterity-ability-checks"] = base_stats["dexterity-ability-checks"] + base_stats['dexterity-modifier']
    base_stats["dexterity-saving-throws"] = base_stats["dexterity-saving-throws"] + base_stats['dexterity-modifier']
    base_stats["intelligence-ability-checks"] = base_stats["intelligence-ability-checks"] + base_stats['intelligence-modifier']
    base_stats["intelligence-saving-throws"] = base_stats["intelligence-saving-throws"] + base_stats['intelligence-modifier']
    base_stats["strength-ability-checks"] = base_stats["strength-ability-checks"] + base_stats['strength-modifier']
    base_stats["strength-saving-throws"] = base_stats["strength-saving-throws"] + base_stats['strength-modifier']
    base_stats["wisdom-ability-checks"] = base_stats["wisdom-ability-checks"] + base_stats['wisdom-modifier']
    base_stats["wisdom-saving-throws"] = base_stats["wisdom-saving-throws"] + base_stats['wisdom-modifier']
}

const get_class_modifiers = (class_data, character_class_data, base_stats) => {
    let modifiers = []
    for(let j = 0; j < Object.keys(character_class_data).length; j++){
        let single_class_data = class_data[Object.keys(character_class_data)[j]]
        let single_character_class_data = character_class_data[Object.keys(character_class_data)[j]]
        let single_character_class_choices = single_character_class_data.class_choice

        // class features
        if(single_class_data && single_class_data.class_features){
            for(let i = 0; i < single_class_data.class_features.length; i++){
                let feature = single_class_data.class_features[i]
                if(feature.hide_in_sheet == false){
                    base_stats['features']['class'].push(
                        {
                            'name': feature.name,
                            'description': feature.description,
                            'id': feature.id
                        }
                    )
                }

                if(feature.level == undefined || feature.level == null || feature.level <= single_character_class_data.level){
                    //Modifiers on class feature
                    let new_modifiers = process_modifier_list(feature.modifiers, feature.id, single_character_class_choices, base_stats)
                    modifiers.push(...new_modifiers)
                    new_modifiers = process_option_set_list(feature.option_sets, feature.id, single_character_class_choices, base_stats)
                    modifiers.push(...new_modifiers)

                    if(feature.actions){
                        base_stats.actions.push(...feature.actions)
                    }
                }
            }
        }
    }
    return modifiers
}

const get_subclass_modifiers = (subclass_data, character_class_data, base_stats) => {
    let modifiers = []
    for(let j = 0; j < Object.keys(character_class_data).length; j++){
        let single_character_class_data = character_class_data[Object.keys(character_class_data)[j]]
        if(single_character_class_data.subclass_id && Object.keys(subclass_data).includes(single_character_class_data.subclass_id)){
            let single_character_subclass_data = subclass_data[single_character_class_data.subclass_id]
            let single_character_class_choices = single_character_class_data.class_choice

            if(single_character_subclass_data && single_character_subclass_data.class_features){
                for(let i = 0; i < single_character_subclass_data.class_features.length; i++){
                    let feature = single_character_subclass_data.class_features[i]
                    if(feature.hide_in_sheet == false){
                        base_stats['features']['class'].push(
                            {
                                'name': feature.name,
                                'description': feature.description,
                                'id': feature.id
                            }
                        )
                    }
                    if(feature.level == undefined || feature.level == null || feature.level <= single_character_class_data.level){
                        //Modifiers on class feature
                        let new_modifiers = process_modifier_list(feature.modifiers, feature.id, single_character_class_choices, base_stats)
                        modifiers.push(...new_modifiers)
                        new_modifiers = process_option_set_list(feature.option_sets, feature.id, single_character_class_choices, base_stats)
                        modifiers.push(...new_modifiers)

                        if(feature.actions){
                            base_stats.actions.push(...feature.actions)
                        }
                    }
                }
            }
        }
    }
    return modifiers
}

const get_inventory_modifiers = (inventory, base_stats) => {
    let modifiers = []

    inventory.forEach((inventory_item) => {
        if(inventory_item.item && inventory_item.item.base_type=='Weapon' && inventory_item.is_equipped == true){
            create_action_from_weapon(base_stats, inventory_item.item)
        }

        //Modifiers on feat_choices
        if(inventory_item.item && inventory_item.is_equipped == true){
            // armours set AC
            if(inventory_item.item.base_type == 'Armour' && inventory_item.item.armor_class){
                base_stats['armor-class'] = inventory_item.item.armor_class
            }

            let new_modifiers = process_modifier_list(inventory_item.item.modifiers, null, null, base_stats)
            modifiers.push(...new_modifiers)

            if(inventory_item.actions){
                base_stats.actions.push(...inventory_item.item.actions)
            }
        }
    })

    return modifiers
}

const get_properties = (props) => {
    let results = []
    props.forEach((prop) => {
        results.push(prop.name)
    })
    return results.join(', ')
};

const create_action_from_weapon = (base_stats, item) => {

    if(item.magic == true){
        base_stats['actions'].push(
            {
                'id': uuidv4(),
                'name': item.name,
                'action_type': 1,
                'activation': {'activationType': 1},
                "attack_type_range": 1,
                "damage_type_id": 1,
                "dice": item.parent.damage,
                "range": {'range': item.parent.range, 'longRange': item.range != item.parent.long_range ? item.parent.long_range : null},
                "ability_modifier_stat_id": item.parent.attack_type == 1 ? 1 : 2,
                "bonus_to_hit": item.bonus_to_hit,
                "bonus_to_damage": item.bonus_to_damage,
                "notes": get_properties(item.parent.properties)
            }
        )
    } else {
        base_stats['actions'].push(
            {
                'id': uuidv4(),
                'name': item.name,
                'action_type': 1,
                'activation': {'activationType': 1},
                "attack_type_range": 1,
                "damage_type_id": 1,
                "dice": item.damage,
                "range": {'range': item.range, 'longRange': item.range != item.long_range ? item.long_range : null},
                "ability_modifier_stat_id": item.attack_type == 1 ? 1 : 2,
                "notes": get_properties(item.properties)
            }
        )
    }
}

const get_feat_modifiers = (feat_data, feat_choices, base_stats) => {
    let modifiers = []

    Object.values(feat_data).forEach((feat) => {

        //Modifiers on feat_choices
        let new_modifiers = process_modifier_list(feat.modifiers, feat.id, feat_choices, base_stats)
        modifiers.push(...new_modifiers)
        new_modifiers = process_option_set_list(feat.option_sets, feat.id, feat_choices, base_stats)
        modifiers.push(...new_modifiers)

        if(feat.actions){
            base_stats.actions.push(...feat.actions)
        }
    })

    return modifiers
}

const get_race_modifiers = (race_data, race_choices, base_stats) => {
    let modifiers = []


    if(race_data && race_data.racial_traits){
        for(let i = 0; i < race_data.racial_traits.length; i++){
            let racial_trait = race_data.racial_traits[i]
            if(racial_trait.hide_in_sheet == false){
                base_stats['features']['race'].push(
                    {
                        'name': racial_trait.name,
                        'description': racial_trait.description,
                        'id': racial_trait.id
                    }
                )
            }

            //Modifiers on class feature
            let new_modifiers = process_modifier_list(racial_trait.modifiers, racial_trait.id, race_choices, base_stats)
            modifiers.push(...new_modifiers)
            new_modifiers = process_option_set_list(racial_trait.option_sets, racial_trait.id, race_choices, base_stats)
            modifiers.push(...new_modifiers)

            if(racial_trait.actions){
                base_stats.actions.push(...racial_trait.actions)
            }
        }
    }

    return modifiers
}

const process_option_set_list = (option_sets, object_id, character_choices, base_stats) => {
    let new_modifiers = []

    option_sets.forEach((option_set) => {
        let selected_option = null
        let option_choice_id = object_id + option_set.id
        let choice_value = character_choices[option_choice_id]
        for(let i = 0; i <  option_set.options.length; i++){
            let option = option_set.options[i]
            if(option.id == choice_value){
                selected_option=option
            }
        }
        if(selected_option){
            let option_modifiers = process_modifier_list(selected_option.modifiers, object_id, character_choices, base_stats)
            new_modifiers.push(...option_modifiers)
        }
    })


    return new_modifiers
}

const process_modifier_list = (modifiers, object_id, character_choices, base_stats) => {
    let new_modifiers = []

    modifiers.forEach((modifier, index) => {
        let subtype = find_modifier_subtype(modifier.modifier_type_id, modifier.modifier_sub_type_id)
        let choice_id = object_id + '-' + modifier.modifier_type_id + '-' + modifier.modifier_sub_type_id + '-' + index
        if(subtype != null){
            if(subtype != null && subtype.choice_groups != undefined && subtype.choice_groups.includes('SPECIAL_FEATS')){
                let choice_value = character_choices[choice_id]
                if(choice_value){
                    base_stats['feat_ids'] .push(choice_value)
                }
            }
            else if(subtype != null && subtype.choice_groups != undefined){
                let choices = get_choices(subtype.choice_groups, subtype.only, subtype.exclude, [])
                //let choice_id = object_id + '-' + modifier.modifier_type_id + '-' + modifier.modifier_sub_type_id + '-' + index
                let choice_value = character_choices[choice_id]
                if(choice_value != undefined && choice_value != null){
                    let choice_object = null
                    for(let i = 0; i < choices.length; i++){
                        let choice = choices[i]
                        if(choice.id == choice_value){
                            choice_object = choice
                        }
                    }
                    if(choice_object){
                        let alternative_subtype = find_modifier_subtype_by_name(modifier.modifier_type_id, choice_object.id)
                        if(!alternative_subtype){
                            console.log("HELP NO ALTERNATIVE FOUND! FIX YOUR DATA")
                        }
                        let new_modifier = {...modifier}
                        new_modifier.sub_type = alternative_subtype.name
                        new_modifier.modifier_sub_type_id = alternative_subtype.id
                        new_modifier.friendly_sub_type_name = alternative_subtype.display_name
                        new_modifiers.push(new_modifier)
                    }
                }
            } else {
                new_modifiers.push(modifier)
            }
        } else {
            new_modifiers.push(modifier)
        }
    })

    return new_modifiers
}

const find_modifier_subtype_by_name = (type_id, subtype_name) => {
    let modifer_subtypes = ModifierSubTypes[type_id]
    for(let i = 0; i < modifer_subtypes.length; i++){
        let subtype=modifer_subtypes[i]
        if(subtype.name == subtype_name){
            return subtype
        }
    }
    return null
}

const find_modifier_subtype = (type_id, subtype_id) => {
    let modifer_subtypes = ModifierSubTypes[type_id]
    for(let i = 0; i < modifer_subtypes.length; i++){
        let subtype=modifer_subtypes[i]
        if(subtype.id == parseInt(subtype_id)){
            return subtype
        }
    }
    return null
}

const calculate_ability_modifiers = (base_stats) => {
    base_stats['strength-modifier'] = Math.floor((base_stats['strength-score'])/2)-5
    base_stats['constitution-modifier'] = Math.floor((base_stats['constitution-score'])/2)-5
    base_stats['dexterity-modifier'] = Math.floor((base_stats['dexterity-score'])/2)-5
    base_stats['intelligence-modifier'] = Math.floor((base_stats['intelligence-score'])/2)-5
    base_stats['wisdom-modifier'] = Math.floor((base_stats['wisdom-score'])/2)-5
    base_stats['charisma-modifier'] = Math.floor((base_stats['charisma-score'])/2)-5
}