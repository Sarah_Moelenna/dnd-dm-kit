import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card'

var character_thresholds = {
    "1": {
        "easy": 25,
        "medium": 50,
        "hard": 75,
        "deadly": 100
    },
    "2": {
        "easy": 50,
        "medium": 100,
        "hard": 150,
        "deadly": 200
    },
    "3": {
        "easy": 75,
        "medium": 150,
        "hard": 225,
        "deadly": 400
    },
    "4": {
        "easy": 125,
        "medium": 250,
        "hard": 375,
        "deadly": 500
    },
    "5": {
        "easy": 250,
        "medium": 500,
        "hard": 750,
        "deadly": 1100
    },
    "6": {
        "easy": 300,
        "medium": 600,
        "hard": 900,
        "deadly": 1400
    },
    "7": {
        "easy": 350,
        "medium": 750,
        "hard": 1100,
        "deadly": 1700
    },
    "8": {
        "easy": 450,
        "medium": 900,
        "hard": 1400,
        "deadly": 2100
    },
    "9": {
        "easy": 550,
        "medium": 1100,
        "hard": 1600,
        "deadly": 2400
    },
    "10": {
        "easy": 600,
        "medium": 1200,
        "hard": 1900,
        "deadly": 2800
    },
    "11": {
        "easy": 800,
        "medium": 1600,
        "hard": 2400,
        "deadly": 3600
    },
    "12": {
        "easy": 1000,
        "medium": 2000,
        "hard": 3000,
        "deadly": 4500
    },
    "13": {
        "easy": 1100,
        "medium": 2200,
        "hard": 3400,
        "deadly": 5100
    },
    "14": {
        "easy": 1250,
        "medium": 2500,
        "hard": 3800,
        "deadly": 5700
    },
    "15": {
        "easy": 1400,
        "medium": 2800,
        "hard": 4300,
        "deadly": 6400
    },
    "16": {
        "easy": 1600,
        "medium": 3200,
        "hard": 4800,
        "deadly": 7200
    },
    "17": {
        "easy": 2000,
        "medium": 3900,
        "hard": 5900,
        "deadly": 8800
    },
    "18": {
        "easy": 2100,
        "medium": 4200,
        "hard": 6300,
        "deadly": 9500
    },
    "19": {
        "easy": 2400,
        "medium": 4900,
        "hard": 7300,
        "deadly": 10900
    },
    "20": {
        "easy": 2800,
        "medium": 5700,
        "hard": 8500,
        "deadly": 12700
    },
}


class DifficultyCalculator extends Component {

    state = {
        player_count: 1,
        players: {"1": "1"}
    }

    update_player_count = e => {
        var new_players = {}

        for(var i = 1; i <= e.target.value; i++){
            if(this.state.players[i] != undefined){
                new_players[i] = this.state.players[i]
            } else {
                new_players[i] = 1
            }
        }

        this.setState({ player_count: e.target.value, players: new_players });
    };

    update_player_level = e => {
        var new_players = this.state.players
        var player_number = e.target.name.replace("player-", "")
        new_players[player_number] = e.target.value
        this.setState({ players: new_players });
    };

    get_party_xp_thresholds = () => {
        var party_thresholds = {
            "easy": 0,
            "medium": 0,
            "hard": 0,
            "deadly": 0
        }

        for(var i = 1; i <= this.state.player_count; i++){
            var player_level = this.state.players[i]
            party_thresholds["easy"] = party_thresholds["easy"] + character_thresholds[player_level]["easy"]
            party_thresholds["medium"] = party_thresholds["medium"] + character_thresholds[player_level]["medium"]
            party_thresholds["hard"] = party_thresholds["hard"] + character_thresholds[player_level]["hard"]
            party_thresholds["deadly"] = party_thresholds["deadly"] + character_thresholds[player_level]["deadly"]
        }

        return party_thresholds
    };

    get_difficulty = () => {
        var party_thresholds = this.get_party_xp_thresholds()

        if(this.props.xp < party_thresholds["easy"]){
            return "Negligible"
        }
        else if(this.props.xp >= party_thresholds["easy"] && this.props.xp < party_thresholds["medium"]){
            return "Easy"
        }
        else if(this.props.xp >= party_thresholds["medium"] && this.props.xp < party_thresholds["hard"]){
            return "Medium"
        }
        else if(this.props.xp >= party_thresholds["hard"] && this.props.xp < party_thresholds["deadly"]){
            return "Hard"
        }
        else if(this.props.xp >= party_thresholds["deadly"]){
            return "Deadly"
        }
        return "ERRR?"

    }

    render(){
        var party_thresholds = this.get_party_xp_thresholds()

        return(
            <div className="difficulty-calculator">
                <Row>
                    <Col xs={4} md={4}>
                        <Row>
                            <Col xs={12} md={12}>
                                <Form.Group controlId="formBasicPlayerCount">
                                <h2>Party Count</h2>
                                    <Form.Control name="size" as="select" onChange={this.update_player_count} custom>
                                            <option value="1" selected={this.state.player_count == "1"}>1</option>
                                            <option value="2" selected={this.state.player_count == "2"}>2</option>
                                            <option value="3" selected={this.state.player_count == "3"}>3</option>
                                            <option value="4" selected={this.state.player_count == "4"}>4</option>
                                            <option value="5" selected={this.state.player_count == "5"}>5</option>
                                            <option value="6" selected={this.state.player_count == "6"}>6</option>
                                            <option value="7" selected={this.state.player_count == "7"}>7</option>
                                            <option value="8" selected={this.state.player_count == "8"}>8</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col xs={6} md={6}>
                                <h2>Party XP Thresholds</h2>
                                <p>Easy - {party_thresholds["easy"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " XP"}<br/>
                                Medium - {party_thresholds["medium"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " XP"}<br/>
                                Hard - {party_thresholds["hard"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " XP"}<br/>
                                Deadly - {party_thresholds["deadly"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " XP"}</p>
                            </Col>
                            <Col xs={6} md={6}>
                                <h2>Estimated Difficulty:</h2>
                                <p>{this.get_difficulty()}</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={8} md={8}>
                        <div className="difficulty-calculator-players">
                            {Object.keys(this.state.players).map((player) => (
                                <Card key={player} className="difficulty-calculator-player">
                                    <Card.Body>
                                        <Card.Title>Player {player}</Card.Title>
                                        <Form.Group controlId="formBasicPlayerCount">
                                            <Form.Label>Level</Form.Label>
                                            <Form.Control name={"player-"+ player} as="select" onChange={this.update_player_level} custom>
                                                    <option value="1" selected={this.state.players[player] == "1"}>1</option>
                                                    <option value="2" selected={this.state.players[player] == "2"}>2</option>
                                                    <option value="3" selected={this.state.players[player] == "3"}>3</option>
                                                    <option value="4" selected={this.state.players[player] == "4"}>4</option>
                                                    <option value="5" selected={this.state.players[player] == "5"}>5</option>
                                                    <option value="6" selected={this.state.players[player] == "6"}>6</option>
                                                    <option value="7" selected={this.state.players[player] == "7"}>7</option>
                                                    <option value="8" selected={this.state.players[player] == "8"}>8</option>
                                                    <option value="9" selected={this.state.players[player] == "9"}>9</option>
                                                    <option value="10" selected={this.state.players[player] == "10"}>10</option>
                                                    <option value="11" selected={this.state.players[player] == "11"}>11</option>
                                                    <option value="12" selected={this.state.players[player] == "12"}>12</option>
                                                    <option value="13" selected={this.state.players[player] == "13"}>13</option>
                                                    <option value="14" selected={this.state.players[player] == "14"}>14</option>
                                                    <option value="15" selected={this.state.players[player] == "15"}>15</option>
                                                    <option value="16" selected={this.state.players[player] == "16"}>16</option>
                                                    <option value="17" selected={this.state.players[player] == "17"}>17</option>
                                                    <option value="18" selected={this.state.players[player] == "18"}>18</option>
                                                    <option value="19" selected={this.state.players[player] == "19"}>19</option>
                                                    <option value="20" selected={this.state.players[player] == "20"}>20</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Card.Body>
                                </Card>
                            ))}
                        </div>
                    </Col>
                </Row>
            </div>
        )


};
}

export default DifficultyCalculator;