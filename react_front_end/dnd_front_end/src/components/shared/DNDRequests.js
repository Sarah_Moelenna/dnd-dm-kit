import Cookies from 'universal-cookie';
import { get_api_url } from './Config';

const cookies = new Cookies();
const REACT_APP_API_URL = get_api_url()

export const DND_FORM = (path, data, callback, error_callback) => {

    var headers = {
        "token": cookies.get('auth_token'),
        "session-id": cookies.get('session_id')
    }
    
    fetch(REACT_APP_API_URL + path, {
        method: "POST",
        headers: headers,
        body: data
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const DND_POST = (path, data, callback, error_callback) => {

    var body = JSON.stringify(data)

    var headers = {
        'Content-Type': 'application/json',
        "token": cookies.get('auth_token'),
        "session-id": cookies.get('session_id')
    }
    
    fetch(REACT_APP_API_URL + path, {
        method: "POST",
        headers: headers,
        body: body
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const DND_PATCH = (path, data, callback, error_callback) => {

    var body = JSON.stringify(data)

    var headers = {
        'Content-Type': 'application/json',
        "token": cookies.get('auth_token'),
        "session-id": cookies.get('session_id')
    }
    
    fetch(REACT_APP_API_URL + path, {
        method: "PATCH",
        headers: headers,
        body: body
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const DND_PUT = (path, data, callback, error_callback) => {

    var body = JSON.stringify(data)

    var headers = {
        'Content-Type': 'application/json',
        "token": cookies.get('auth_token'),
        "session-id": cookies.get('session_id')
    }
    
    fetch(REACT_APP_API_URL + path, {
        method: "PUT",
        headers: headers,
        body: body
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const DND_DELETE = (path, data, callback, error_callback) => {

    var body = JSON.stringify(data)

    var headers = {
        'Content-Type': 'application/json',
        "token": cookies.get('auth_token'),
        "session-id": cookies.get('session_id')
    }
    
    fetch(REACT_APP_API_URL + path, {
        method: "DELETE",
        headers: headers,
        body: body
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const DND_GET = (path, callback, error_callback) => {

    var headers = {
        "token": cookies.get('auth_token'),
        "session-id": cookies.get('session_id')
    }

    fetch(REACT_APP_API_URL + path, {
        method: "GET",
        headers: headers,
        }).then( response => {
            if (response.status >= 400) { throw response }
            return response
        }).then(res => res.json())
        .then((response) => {
            if (callback != undefined){
                callback(response)
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}
