import React, { useState } from 'react';
import { Collapse, Button } from 'reactstrap';

const Collapsible = ({title, contents, initial_state}) => {
  const [isOpen, setIsOpen] = useState(initial_state == undefined ? false : initial_state);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="collapsible">
        <p className="title" onClick={toggle}><i className="fas fa-chevron-right"></i> {title}</p> 
        <Collapse isOpen={isOpen}>
            {contents}
        </Collapse>
    </div>
  );
}

export default Collapsible;