import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { Collapse } from 'reactstrap';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton'
import { AtHigherLevelTypes, DiceValues, ScaleTypes } from '../monster/Data';

export const createAtHigherLevel = () => {
    return {
        "level": null,
        "typeId": null,
        "dice": {"diceCount": null, "diceValue": null, "diceMultiplier": null, "fixedValue": null, "diceString": null},
        "details": null
    }
}

export const AtHigherLevelForm = ({update_attribute, update_attribute_by_name , update_dice, item, scale_type}) => {
    return (
        <Row>
            <Col xs={2} md={2}>
                <Form.Group controlId="formBasicWis">
                    <Form.Label>SCALING LEVEL VALUE</Form.Label>
                    <Form.Control value={item.level} required name="level" type="number" min={0} onChange={update_attribute} />
                </Form.Group>
            </Col>

            <Col xs={2} md={2}>
                <Form.Group controlId="formBasicType">
                <Form.Label>SCALE TYPE</Form.Label>
                    <Form.Control name="typeId" as="select" disabled={true} custom>
                        <option selected="true" value="">-</option>
                        {Object.keys(ScaleTypes).map((type_value) => (
                            <option key={type_value} selected={scale_type == type_value} value={type_value}>{ScaleTypes[type_value]}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
            </Col>

            <Col xs={2} md={2}>
                <Form.Group controlId="formBasicType">
                <Form.Label>SCALE EFFECT</Form.Label>
                    <Form.Control name="typeId" as="select" onChange={(e) => {update_attribute_by_name("typeId", parseInt(e.target.value))}} custom>
                        <option selected="true" value="">-</option>
                        {Object.keys(AtHigherLevelTypes).map((type_id) => (
                            <option key={type_id} selected={item.typeId == type_id} value={type_id}>{AtHigherLevelTypes[type_id]}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
            </Col>

            <Col xs={2} md={2}>
                <Form.Group controlId="formBasicWis">
                    <Form.Label>DICE COUNT</Form.Label>
                    <Form.Control value={item.dice != null && item.dice.diceCount} required name="diceCount" type="number" min={0} onChange={update_dice} />
                </Form.Group>
            </Col>

            <Col xs={2} md={2}>
                <Form.Group controlId="formBasicSize">
                    <Form.Label>DIE TYPE</Form.Label>
                    <Form.Control name="diceValue" as="select" onChange={update_dice} custom>
                            <option selected="true" value="">-</option>
                            {DiceValues.map((dice_value) => (
                                <option key={dice_value} selected={item.dice != null && item.dice.diceValue == dice_value} value={dice_value}>d{dice_value}</option>
                            ))}
                    </Form.Control>
                </Form.Group>
            </Col>

            <Col xs={2} md={2}>
                <Form.Group controlId="formBasicWis">
                    <Form.Label>FIXED VALUE</Form.Label>
                    <Form.Control value={item.dice != null && item.dice.fixedValue} name="fixedValue" type="number" min={0} onChange={update_dice} />
                </Form.Group>
            </Col>

            <Col xs={12} md={12}>
                <Form.Group controlId="formBasicName">
                    <Form.Label>DETAILS</Form.Label>
                    <Form.Control value={item.details} required name="details" type="text" placeholder="" onChange={update_attribute} />
                </Form.Group>
            </Col>

        </Row>
    )
}

class AtHigherLevelItem extends Component {

    state = {
        toggle: false,
    }

    update_attribute = (e) => {
        var new_item = this.props.item
        new_item[e.target.name] = e.target.value
        this.props.update_function(this.props.id, new_item)
    }

    update_attribute_by_name = (name, value) => {
        var new_item = this.props.item
        new_item[name] = value
        this.props.update_function(this.props.id, new_item)
    }

    update_dice = (e) => {
        var new_item = this.props.item
        if(new_item.dice == null){
            new_item.dice = {}
        }
        new_item.dice[e.target.name] = e.target.value
        this.props.update_function(this.props.id, new_item)
    }


    render(){

        return(
            <Row key={this.props.id} className="at-higher-level-item">
                <Col xs={2} md={2}>
                    <p>{this.props.item.level}</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>{AtHigherLevelTypes[this.props.item.typeId]}</p>
                </Col>
                <Col xs={2} md={2}>
                    {this.props.item.dice != null && (this.props.item.dice.diceCount != null || this.props.item.dice.diceValue != null) &&
                        <p>{this.props.item.dice.diceCount}d{this.props.item.dice.diceValue}</p>
                    }
                </Col>
                <Col xs={2} md={2}>
                    {this.props.item.dice != null && this.props.item.dice.fixedValue != undefined &&
                        <p>{this.props.item.dice.fixedValue}</p>
                    }
                </Col>
                <Col xs={2} md={2}>
                    <p>{this.props.item.details}</p>
                </Col>
                { !this.props.as_collapse &&
                    <Col xs={1} md={1}>
                        <Button onClick={() => {this.setState({toggle: true})}}>Edit</Button>
                    </Col>
                }
                <Col xs={1} md={1}>
                    <DeleteConfirmationButton id={this.props.id} name="At Higher Level" delete_function={this.props.delete_function} override_button="Delete"/>
                </Col>
                { this.props.as_collapse &&
                    <Col xs={1} md={1}>
                        <div className="clickable-div" onClick={() => {this.setState({toggle: !this.state.toggle})}}><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                    </Col>
                }
                <Col xs={12} md={12}>
                    {
                        this.props.as_collapse ?
                            <Collapse isOpen={this.state.toggle}>
                                <hr/>
                                <AtHigherLevelForm
                                    update_attribute={this.update_attribute}
                                    update_attribute_by_name={this.update_attribute_by_name}
                                    update_dice={this.update_dice}
                                    item={this.props.item}
                                    scale_type={this.props.scale_type}
                                />
                            </Collapse>
                        :
                            <Modal show={this.state.toggle} size="md" className="select-modal">
                                <Modal.Header>Edit At Higher Level</Modal.Header>
                                <Modal.Body>
                                    <AtHigherLevelForm
                                        update_attribute={this.update_attribute}
                                        update_attribute_by_name={this.update_attribute_by_name}
                                        update_dice={this.update_dice}
                                        item={this.props.item}
                                        scale_type={this.props.scale_type}
                                    />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button onClick={()=>{this.setState({toggle: false})}}>Close</Button>
                                </Modal.Footer>
                            </Modal>
                    }
                </Col>
            </Row>
        )
    }
}

export const ListAtHigherLevel = ({ items, update_function, delete_function, scale_type, as_collapse}) => {

    return (
        <div className="at-higher-level-items">
            <Row>
                <Col xs={2} md={2}>
                    <p>LEVEL</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>TYPE</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>DICE ROLL</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>FIXED VALUE</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>DETAILS</p>
                </Col>
                <Col xs={1} md={1}>
                </Col>
                <Col xs={1} md={1}>
                </Col>
            </Row>
            {items.map((item, index) => (
                <AtHigherLevelItem item={item} update_function={update_function} delete_function={delete_function} id={index} scale_type={scale_type} as_collapse={as_collapse}/>
            ))}
        </div>
    );
}