import React, { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const DeleteConfirmationButton = ({id, delete_function, name, override_button, override_title}) => {

    const [isVisible, setIsVisible] = useState(false);

    const toggle = () => setIsVisible(!isVisible);

    function process_selection(){
        toggle()
        delete_function(id)
    }

    var popover = <Popover className="delete-button-popover">
        <Popover.Title>{override_title != undefined ? override_title : "Delete " + name}</Popover.Title>
        <Popover.Content className="content">
            <p>Are You Sure?</p>
            <Row>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => process_selection()}>
                        <p>Yes</p>
                    </div>
                </Col>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => toggle()}>
                        <p>No</p>
                    </div>
                </Col>
            </Row>
        </Popover.Content>
    </Popover>

    return (
        <div className="delete-button btn btn-primary">
            <div>
                <OverlayTrigger show={isVisible} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                    <p>{override_button != undefined ? override_button : "Delete " + name}</p>
                </OverlayTrigger>
            </div>
        </div>
    );
}

export default DeleteConfirmationButton;