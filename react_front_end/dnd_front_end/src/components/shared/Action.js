import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MDEditor from '@uiw/react-md-editor';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form'
import { ActionResetTypes, ActionTypes, WeaponActionSubTypes, Levels, DamageTypes, AttackRanges, AttributeID, DiceValues, SpellRangeTypes, SpellAoETypes, SpellActivationTypes } from '../monster/Data';
import { v4 as uuidv4 } from 'uuid';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton'


export const createAction = () => {
    return {
        "id": uuidv4(),
        "limited_use": {},
        "name": "Unnamed Action",
        "description": null,
        "snippet": null,
        "ability_modifier_stat_id": null,
        "on_miss_description": null,
        "save_fail_description": null,
        "save_success_description": null,
        "save_stat_id": null,
        "fixed_save_dc": null,
        "attack_type_range": null,
        "action_type": null,
        "attack_sub_type": null,
        "dice": {},
        "value": null,
        "damage_type_id": null,
        "is_martial_arts": null,
        "is_proficient": null,
        "spell_range_type": null,
        "display_as_attack": null,
        "range": {},
        "activation": {},
        "number_of_targets": null,
        "fixed_to_hit": null,
        "ammunition": null,
        "level": null
    }
}

class ActionItem extends Component {

    state = {
        toggle: false,
    }
    
    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value = true
        } else if(new_value == "false"){
            new_value = false
        }
        var new_action = this.props.action
        new_action[e.target.name] = new_value
        this.props.update_action(this.props.action.id, new_action)
    }

    handleChangeIntOrNull = (e) => {
        var new_value = e.target.value
        if(new_value == ""){
            new_value = null
        } else {
            new_value = parseInt(new_value)
        }
        var new_action = this.props.action
        new_action[e.target.name] = new_value
        this.props.update_action(this.props.action.id, new_action)
    }

    update_dice = (e) => {
        var new_value = e.target.value
        var new_action = this.props.action
        if(new_action.dice == null){
            new_action.dice = {}
        }

        if(e.target.name == 'diceCount' || e.target.name == 'diceValue' || e.target.name == 'fixedValue'){
            if(new_value == ''){
                new_value = null
            } else {
                new_value = parseInt(new_value)
            }
        }

        new_action.dice[e.target.name] = new_value
        
        this.props.update_action(this.props.action.id, new_action)
    }

    update_range = (e) => {
        var new_value = e.target.value
        var new_action = this.props.action
        if(new_action.range == null){
            new_action.range = {}
        }

        if(e.target.name == 'range' || e.target.name == 'longRange' || e.target.name == 'aoeSize'){
            if(new_value == ''){
                new_value = null
            } else {
                new_value = parseInt(new_value)
            }
        }

        new_action.range[e.target.name] = new_value
        this.props.update_action(this.props.action.id, new_action)
    }

    update_activation = (e) => {
        var new_value = e.target.value
        var new_action = this.props.action
        if(new_action.activation == null){
            new_action.activation = {}
        }

        if(e.target.name == 'activationTime' || e.target.name == 'activationType'){
            if(new_value == ''){
                new_value = null
            } else {
                new_value = parseInt(new_value)
            }
        }

        new_action.activation[e.target.name] = new_value
        this.props.update_action(this.props.action.id, new_action)
    }

    update_limit = (e) => {
        var new_value = e.target.value
        var new_action = this.props.action
        if(new_action.limited_use == null){
            new_action.limited_use = {}
        }

        if(e.target.name == 'resetType'){
            if(new_value == ''){
                new_value = null
            } else {
                new_value = parseInt(new_value)
            }
        }

        new_action.limited_use[e.target.name] = new_value
        this.props.update_action(this.props.action.id, new_action)
    }

    update_text = (text) => {
        var new_action = this.props.action
        new_action.description = text
        this.props.update_action(this.props.action.id, new_action)
    }

    render(){

        return(
            <Row key={this.props.action.id} className="action-item">
                <Col xs={4} md={4}>
                    <p>{this.props.action.name}</p>
                </Col>
                <Col xs={4} md={4}>
                    {this.props.action.limited_use != null && this.props.action.limited_use.resetType != undefined && this.props.action.limited_use.resetType != null &&
                        <p>{ActionResetTypes[this.props.action.limited_use.resetType].display_name}</p>
                    }
                </Col>
                <Col xs={1} md={1}>
                    <Button onClick={() => {this.setState({toggle: true})}}>Edit</Button>
                </Col>
                <Col xs={3} md={3}>
                    <DeleteConfirmationButton id={this.props.action.id} name="Action" delete_function={this.props.delete_action} override_button="Delete"/>
                </Col>
                <Col xs={12} md={12}>
                    <Modal show={this.state.toggle} size="md" className="select-modal">
                        <Modal.Header>Edit Action</Modal.Header>
                        { this.state.toggle &&
                            <Modal.Body>
                            {(this.props.action.action_type != null && this.props.action.action_type != '') ?
                                <Row>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>ACTION TYPE</Form.Label>
                                            <Form.Control name="action_type" as="select" onChange={this.handleChangeIntOrNull} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(ActionTypes).map((action_type_id) => (
                                                        <option key={action_type_id} selected={this.props.action.action_type == action_type_id} value={action_type_id}>{ActionTypes[action_type_id]}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>NAME</Form.Label>
                                            <Form.Control value={this.props.action.name} required name="name" type="text" onChange={this.handleChange} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>ABILITY SCORE TYPE</Form.Label>
                                            <Form.Control name="ability_modifier_stat_id" as="select" onChange={this.handleChangeIntOrNull} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(AttributeID).map((attribute_id) => (
                                                        <option key={attribute_id} selected={this.props.action.ability_modifier_stat_id == attribute_id} value={attribute_id}>{AttributeID[attribute_id]}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>LEVEL</Form.Label>
                                            <Form.Control name="level" as="select" onChange={this.handleChangeIntOrNull} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Levels.map((level) => (
                                                        <option key={level} selected={this.props.action.level == level} value={level}>{level}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>IS PROFICIENT</Form.Label>
                                            <Form.Control name="is_proficient" as="select" onChange={this.handleChange} custom>
                                                    <option selected={this.props.action.is_proficient == true} value={true}>True</option>
                                                    <option selected={this.props.action.is_proficient == false} value={false}>False</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>ATTACK RANGE</Form.Label>
                                            <Form.Control name="attack_type_range" as="select" onChange={this.handleChangeIntOrNull} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(AttackRanges).map((range_id) => (
                                                        <option key={range_id} selected={this.props.action.attack_type_range == range_id} value={range_id}>{AttackRanges[range_id]}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>SAVE TYPE</Form.Label>
                                            <Form.Control name="save_stat_id" as="select" onChange={this.handleChangeIntOrNull} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(AttributeID).map((attribute_id) => (
                                                        <option key={attribute_id} selected={this.props.action.save_stat_id == attribute_id} value={attribute_id}>{AttributeID[attribute_id]}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>FIXED SAVE DC</Form.Label>
                                            <Form.Control
                                                disabled={this.props.action.save_stat_id == null || this.props.action.save_stat_id == ''}
                                                value={this.props.action.fixed_save_dc}
                                                required
                                                name="fixed_save_dc"
                                                min={0}
                                                type="number"
                                                onChange={this.handleChangeIntOrNull}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>AFFECTED BY MARTIAL ARTS</Form.Label>
                                            <Form.Control name="is_martial_arts" as="select" onChange={this.handleChange} custom>
                                                    <option selected={this.props.action.is_martial_arts == true} value={true}>True</option>
                                                    <option selected={this.props.action.is_martial_arts == false} value={false}>False</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>DICE COUNT</Form.Label>
                                            <Form.Control value={this.props.action.dice != null && this.props.action.dice.diceCount} required name="diceCount" type="number" min={0} onChange={this.update_dice} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>DIE TYPE</Form.Label>
                                            <Form.Control name="diceValue" as="select" onChange={this.update_dice} custom>
                                                    <option selected="true" value="">-</option>
                                                    {DiceValues.map((dice_value) => (
                                                        <option key={dice_value} selected={this.props.action.dice != null && this.props.action.dice.diceValue == dice_value} value={dice_value}>d{dice_value}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>FIXED VALUE</Form.Label>
                                            <Form.Control value={this.props.action.dice != null && this.props.action.dice.fixedValue} required name="fixedValue" type="number" onChange={this.update_dice} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="Campaign Notes">
                                            <Form.Label>EFFECT ON MISS</Form.Label>
                                            <Form.Control key={"miss" + this.props.action.id} name="on_miss_description" as="textarea" rows={7} value={this.props.action.on_miss_description} onChange={this.handleChange}/>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="Campaign Notes">
                                            <Form.Label>EFFECT ON SAVE SUCCESS</Form.Label>
                                            <Form.Control
                                                key={"sucess" + this.props.action.id}
                                                name="save_success_description"
                                                as="textarea"
                                                disabled={this.props.action.save_stat_id == null || this.props.action.save_stat_id == ''}
                                                rows={7}
                                                value={this.props.action.save_success_description}
                                                onChange={this.handleChange}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="Campaign Notes">
                                            <Form.Label>EFFECT ON SAVE FAIL</Form.Label>
                                            <Form.Control
                                                key={"fail" + this.props.action.id}
                                                name="save_fail_description"
                                                as="textarea"
                                                disabled={this.props.action.save_stat_id == null || this.props.action.save_stat_id == ''}
                                                rows={7}
                                                value={this.props.action.save_fail_description}
                                                onChange={this.handleChange}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>WEAPON ATTACK SUBTYPE</Form.Label>
                                            <Form.Control
                                                disabled={this.props.action.action_type != 1}
                                                name="attack_sub_type"
                                                as="select"
                                                onChange={this.handleChangeIntOrNull}
                                                custom
                                            >
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(WeaponActionSubTypes).map((action_type_id) => (
                                                        <option key={action_type_id} selected={this.props.action.attack_sub_type == action_type_id} value={action_type_id}>{WeaponActionSubTypes[action_type_id]}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>DAMAGE TYPE</Form.Label>
                                            <Form.Control name="damage_type_id" as="select" onChange={this.handleChangeIntOrNull} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(DamageTypes).map((action_type_id) => (
                                                        <option key={action_type_id} selected={this.props.action.damage_type_id == action_type_id} value={action_type_id}>{DamageTypes[action_type_id]}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>DISPLAY AS ATTACK</Form.Label>
                                            <Form.Control name="display_as_attack" as="select" onChange={this.handleChange} custom>
                                                    <option selected={this.props.action.display_as_attack == true} value={true}>True</option>
                                                    <option selected={this.props.action.display_as_attack == false} value={false}>False</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>SPELL RANGE TYPE</Form.Label>
                                            <Form.Control
                                                disabled={this.props.action.action_type != 2}
                                                name="spell_range_type"
                                                as="select"
                                                onChange={this.handleChange}
                                                custom
                                            >
                                                    <option selected="true" value="">-</option>
                                                    {SpellRangeTypes.map((range_type) => (
                                                        <option key={range_type} selected={this.props.action.spell_range_type == range_type} value={range_type}>{range_type}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>RANGE</Form.Label>
                                            <Form.Control
                                                disabled={
                                                    (this.props.action.action_type == 1 && this.props.action.attack_type_range != 2) ||
                                                    (this.props.action.action_type == 2 && this.props.action.spell_range_type != "Ranged")
                                                }
                                                value={this.props.action.range != null && this.props.action.range.range}
                                                required
                                                name="range"
                                                min={0}
                                                type="number"
                                                onChange={this.update_range}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicWis">
                                            <Form.Label>LONG RANGE</Form.Label>
                                            <Form.Control
                                                disabled={this.props.action.action_type != 1}
                                                value={this.props.action.range != null && this.props.action.range.longRange}
                                                required
                                                name="longRange"
                                                min={0}
                                                type="number"
                                                onChange={this.update_range}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSchool">
                                            <Form.Label>AOE Type</Form.Label>
                                            <Form.Control name="aoeType" as="select" onChange={this.update_range} custom>
                                                    <option selected="true" value="">-</option>
                                                    {SpellAoETypes.map((spell_aoe) => (
                                                        <option
                                                            key={spell_aoe}
                                                            selected={this.props.action.range != null ? this.props.action.range.aoeType == spell_aoe : false}
                                                            value={spell_aoe}
                                                        >
                                                            {spell_aoe}
                                                        </option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicTime">
                                            <Form.Label>AOE Size</Form.Label>
                                            <Form.Control
                                                value={this.props.action.range.aoeSize}
                                                disabled={
                                                    (this.props.action.range == null) ||
                                                    (this.props.action.range.aoeType == null || this.props.action.range.aoeType == "")
                                                }
                                                name="aoeSize"
                                                type="number"
                                                min={0}
                                                onChange={this.update_range}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>

                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicType">
                                        <Form.Label>ACTIVATION TYPE</Form.Label>
                                            <Form.Control name="activationType" as="select" onChange={this.update_activation} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(SpellActivationTypes).map((activation_type_display) => (
                                                        <option 
                                                            key={SpellActivationTypes[activation_type_display]}
                                                            selected={this.props.action.activation != null ? this.props.action.activation.activationType == SpellActivationTypes[activation_type_display] : false}
                                                            value={SpellActivationTypes[activation_type_display]}
                                                            >
                                                                {activation_type_display}
                                                            </option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicTime">
                                            <Form.Label>ACTIVATION TIME</Form.Label>
                                            <Form.Control
                                                value={this.props.action.activation.activationTime}
                                                disabled={
                                                    (this.props.action.activation == null) ||
                                                    (this.props.action.activation.activationType != 6 && this.props.action.activation.activationType != 7 && this.props.action.activation.activationType != 8)
                                                }
                                                name="activationTime"
                                                type="number"
                                                min={0}
                                                onChange={this.update_activation}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <hr/>
                                        <h3>Limited Use</h3>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>RESET TYPE</Form.Label>
                                            <Form.Control
                                                name="resetType"
                                                as="select"
                                                onChange={this.update_limit}
                                                custom
                                            >
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(ActionResetTypes).map((reset_type_id) => (
                                                        <option
                                                            key={reset_type_id}
                                                            selected={this.props.action.limited_use != null ? this.props.action.limited_use.resetType == reset_type_id : false}
                                                            value={reset_type_id}
                                                        >
                                                            {ActionResetTypes[reset_type_id].display_name}
                                                        </option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>MAX USES</Form.Label>
                                            <Form.Control
                                                value={this.props.action.limited_use != null && this.props.action.limited_use.maxUses}
                                                required
                                                name="maxUses"
                                                min={0}
                                                type="number"
                                                onChange={this.update_limit}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>MAX USES ABILITY SCORE MODIFIER</Form.Label>
                                            <Form.Control
                                                name="statModifierUsesId"
                                                as="select"
                                                onChange={this.update_limit}
                                                value={this.props.action.limited_use != null ? this.props.action.limited_use.statModifierUsesId : ''}
                                                custom
                                            >
                                                <option value="">-</option>
                                                {Object.keys(AttributeID).map((attribute_id) => (
                                                    <option key={attribute_id} value={attribute_id}>{AttributeID[attribute_id]}</option>
                                                ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <Form.Group controlId="formBasicTime">
                                            <Form.Label>SNIPPET</Form.Label>
                                            <Form.Control
                                                value={this.props.action.snippet}
                                                name="snippet"
                                                type="text"
                                                onChange={this.handleChange}
                                            />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <Form.Label>DESCRIPTION</Form.Label>
                                        <MDEditor
                                            value={this.props.action.description}
                                            preview="edit"
                                            onChange={this.update_text}
                                        />
                                        <br/>
                                    </Col>

                                </Row>
                                :
                                <Row>
                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicSize">
                                            <Form.Label>ACTION TYPE</Form.Label>
                                            <Form.Control name="action_type" as="select" onChange={this.handleChange} custom>
                                                    <option selected="true" value="">-</option>
                                                    {Object.keys(ActionTypes).map((action_type_id) => (
                                                        <option key={action_type_id} selected={this.props.action.action_type == action_type_id} value={action_type_id}>{ActionTypes[action_type_id]}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>
                                </Row>
                            }
                            
                            </Modal.Body>
                        }
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: false})}}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </Col>
            </Row>
        )
    }
}


export const ListActions = ({ actions, update_action, delete_action}) => {

    return (
        <div className="action-items">
            <Row>
                <Col xs={4} md={4}>
                    <p>NAME</p>
                </Col>
                <Col xs={4} md={4}>
                    <p>RESET TYPE</p>
                </Col>
                <Col xs={3} md={3}>
                    <p>ACTIONS</p>
                </Col>
                <Col xs={1} md={1}>
                </Col>
            </Row>
            {actions.map((action) => (
                <ActionItem action={action} update_action={update_action} delete_action={delete_action}/>
            ))}
        </div>
    );
}