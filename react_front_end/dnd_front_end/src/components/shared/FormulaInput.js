import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';

export function EvaluateFormula(base_stats, extra_stats, formula){
    function evil(fn) {
        return new Function('return ' + fn)();
    }

    for(let i = 0; i < Object.keys(base_stats).length; i++){
        let variable =  Object.keys(base_stats)[i]
        formula = formula.replaceAll(variable, base_stats[variable])
    }
    for(let i = 0; i < Object.keys(extra_stats).length; i++){
        let variable =  Object.keys(extra_stats)[i]
        formula = formula.replaceAll(variable, extra_stats[variable])
    }
    return evil(formula)
}


export class FormulaInput extends Component {

    allowed_variables = [
        'strength-score',
        'constitution-score',
        'dexterity-score',
        'intelligence-score',
        'wisdom-score',
        'charisma-score',
        'strength-modifier',
        'constitution-modifier',
        'dexterity-modifier',
        'intelligence-modifier',
        'wisdom-modifier',
        'charisma-modifier',
        'class-level',
        'character-level'
    ]

    render () {
        return (
            <div>
                <Form.Group>
                    <Form.Label>{this.props.name}</Form.Label>
                    <p className='formula-label'>Please Enter Formula</p>
                    <Form.Control value={this.props.value} required name={this.props.name} type="text" placeholder="4+2*(charisma-score+2)" onChange={(e) => {this.props.handle_change(e.target.value)}} />
                    <p className='allowed-variables'>Allowed Variables:
                        {this.allowed_variables.map((allowed_variable) => (
                            <span>{allowed_variable}</span>
                        ))}
                    </p>
                </Form.Group>
            </div>
        )
    }
}