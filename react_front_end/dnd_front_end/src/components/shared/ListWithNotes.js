import React, { useState, useEffect } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const ListWithNotes = ({input_value, items, onChange}) => {

    const [isVisible, setIsVisible] = useState(false);

    const [data, setData] = useState(input_value);

    const toggle = () => setIsVisible(!isVisible);

    useEffect(() => {
        setData(input_value)
      }, [input_value]);

    const process_selection = (e) => {
        var new_data = data
        new_data[e.target.value] = ""
        setData(new_data)
        onChange(new_data)
        toggle()
    }

    const remove_item = (item) => {
        var new_data = data
        delete new_data[item]
        setData(new_data)
        onChange(new_data)
    }

    const update_note = (item, value) => {
        var new_data = data
        new_data[item] = value
        setData(new_data)
        onChange(new_data)
    }

    var popover = <Popover className="list-selector-popover">
        <Form.Control name="list" as="select" onChange={process_selection} custom>
            <option selected="selected" disabled="disabled" value="">Click to Select</option>
            {items.map((item, index) => 
                { if (!Object.keys(data).includes(item))
                    return (<option key={index} value={item}>{item}</option>)
                }
            )}
        </Form.Control>
    </Popover>

    return (
        <div className="list-with-notes">
            <OverlayTrigger show={isVisible} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                <div className="list-selector-items">
                    {Object.keys(data).map((key, index) => ( 
                        <div key={key} className="list-selector-item">
                            <Row>
                                <Col xs={1}>
                                    <i className="fas fa-times clickable-icon" onClick={() => remove_item(key)}></i>
                                </Col>
                                <Col xs={4}>
                                    <p>{key}</p>
                                </Col>
                                <Col xs={7}>
                                    <Form.Control required value={data[key]} name="name" type="text" onChange={(e) => {update_note(key, e.target.value)}} />
                                </Col>
                            </Row>
                        </div>
                    ))}
                </div>
            </OverlayTrigger>
        </div>
    );
}

export default ListWithNotes;