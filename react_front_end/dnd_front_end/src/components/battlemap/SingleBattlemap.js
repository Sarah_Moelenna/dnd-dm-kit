import React, { Component } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { get_api_url } from '../shared/Config';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'

class SingleBattlemap extends Component {

    state = {
        cell_size: 1,
        control_state: "MOVEMENT",
        control_data: {}
    }

    constructor(props) {
        super(props);
        this.battlemapPageRef = React.createRef();
    }

    componentDidMount(){
        var result =  this.battlemapPageRef.current.offsetWidth / this.props.battlemap.columns
        if(result > 80){
            this.setState({cell_size: 80})
        } else {
            this.setState({cell_size: result})
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.cell_size == this.state.cell_size){
            var result =  this.battlemapPageRef.current.offsetWidth / this.props.battlemap.columns
            var new_size = 80
            if(result < 80){
                new_size = result
            }
            if(new_size != this.state.cell_size){
                this.setState({cell_size: new_size})
            }
        }
    }

    handleOnDragEnd = (result) => {
        if (!result.destination) return;

        if(result.destination.droppableId.startsWith("cell") && result.draggableId.startsWith("object")){
            var object_id = result.draggableId.replaceAll("object:", "")
            var cell_items = result.destination.droppableId.split("-")
            var x = cell_items[1]
            var y = cell_items[2]

            var data = {
                x: x,
                y: y
            }
            this.props.update_battlemap_object(object_id, data)
        }
        
    }

    is_cell_blocked = (x,y) => {
        return this.props.battlemap.blocked_cells.split(",").includes(x + "-" + y)
    }

    is_cell_disabled = (x, y, objects_dict) => {
        if(this.is_cell_blocked(x, y) == true){
            return true
        } else if(objects_dict[x] == undefined){
            return false
        } else if(objects_dict[x][y] == undefined){
            return false
        } else {
            return true
        }
    }

    get_objects_dict = (objects) => {
        var objects_dict = {
            unplaced_player: [],
            unplaced_ally: [],
            unplaced_monster: [],
            unplaced_custom: []
        }
        for(var i = 0; i < objects.length; i++){
            var object = objects[i]
            if(object.state=="DEAD"){
                continue;
            }
            if(object.delayed==1){
                continue;
            }
            object.index = i
            if(object.x == null || object.y == null){
                if(object.type == "CHARACTER"){
                    objects_dict.unplaced_player.push(object)
                }
                if(object.type == "ALLY"){
                    objects_dict.unplaced_ally.push(object)
                }
                if(object.type == "MONSTER"){
                    objects_dict.unplaced_monster.push(object)
                }
                if(object.type == "CUSTOM"){
                    objects_dict.unplaced_custom.push(object)
                }
            } else {
                if (objects_dict[object.x] == undefined){
                    objects_dict[object.x] = {}
                }
                objects_dict[object.x][object.y] = object
            }
        }
        return objects_dict
    }

    get_image_for_object = (object) => {
        if(object.image != undefined){
            return object.image.replaceAll(" ", "%20")
        }
        return get_api_url() + "/static/devil.png"
    }

    get_fof_for_object = (object) => {
        if(object.type == "CHARACTER" || object.type == "ALLY"){
            return "friend"
        }
        if(object.type == "MONSTER" || object.type == "CUSTOM"){
            return "foe"
        }
    }

    get_control_description = () => {
        if(this.state.control_state == "MOVEMENT"){
            return "Click and Hold on object then drag to move it."
        }
        if(this.state.control_state == "MEASURE"){
            return "Click one cell then a hover over another to get distance measurement."
        }
        return ''
    }

    handle_click = (x, y) =>{
        if(this.state.control_state == "MEASURE"){
            var new_control_data = this.state.control_data
            new_control_data.marked_x = x
            new_control_data.marked_y = y
            this.setState({control_data: new_control_data})
        }
    }

    handle_hover = (x, y) =>{
        if(this.state.control_state == "MEASURE"){
            if(this.state.control_data.marked_x != undefined && this.state.control_data.marked_y != undefined){
                var new_control_data = this.state.control_data
                new_control_data.hover_x = x
                new_control_data.hover_y = y
                this.setState({control_data: new_control_data})
            }
        }
    }

    is_cell_marked = (x, y) => {
        if(this.state.control_state == "MEASURE"){
            if(x == this.state.control_data.marked_x && y == this.state.control_data.marked_y){
                return true
            }
        }
        return false
    }

    get_cell_distance_popover = (x, y) => {
        if(this.state.control_state == "MEASURE"){
            if(this.state.control_data.marked_x != undefined && this.state.control_data.marked_y != undefined && x == this.state.control_data.hover_x && y == this.state.control_data.hover_y){
                var a = this.state.control_data.marked_x - this.state.control_data.hover_x;
                var b = this.state.control_data.marked_y - this.state.control_data.hover_y;
                var distance = Math.sqrt( a*a + b*b );
                var popover = <Popover className="delete-button-popover">
                    <Popover.Title>{(distance*5).toFixed(1)} ft</Popover.Title>
                </Popover>
                return (
                    <OverlayTrigger show={true} placement="top" overlay={popover}><div></div></OverlayTrigger>
                );
            }
        }
        return null
    }

    get_draggable_object = (object, provided, index) => {

        var popover = <Popover className="delete-button-popover">
            <Popover.Title>{object.display_name != undefined ? object.display_name.replace(" - ", " ") : object.name}{object.damage_taken != undefined ? " -" + object.damage_taken + " HP" : ""}</Popover.Title>
        </Popover>

        if(this.state.control_state != "MOVEMENT" || (this.props.player_read == true && (object.type == "MONSTER" || object.type == "CUSTOM" || object.type == "ALLY"))){
            return (<div className={"battlemap-object " + this.get_fof_for_object(object)}
                style={{  
                    height: this.state.cell_size + "px",
                    width: this.state.cell_size + "px"
                }}
            >
                <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popover}>
                    <div className="battlemap-object-image"
                        style={{  
                            backgroundImage: "url(" + this.get_image_for_object(object) + ")",
                        }}
                    />
                </OverlayTrigger>
            </div>)
        }
        
        return (
            <Draggable key={"object:" + object.id} draggableId={"object:" + object.id} index={index}>
                {(provided) => (
                    <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                    >
                        <div className={"battlemap-object " + this.get_fof_for_object(object)}
                            style={{  
                                height: this.state.cell_size + "px",
                                width: this.state.cell_size + "px"
                            }}
                        >
                            <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popover}>
                                <div className="battlemap-object-image"
                                    style={{  
                                        backgroundImage: "url(" + this.get_image_for_object(object) + ")",
                                    }}
                                />
                            </OverlayTrigger>
                        </div>
                    </div>
                )}
            </Draggable>
        )
    }

    get_cell_objects = (x, y, objects_dict) => {
        if (objects_dict[x] != undefined && objects_dict[x][y] != undefined){
            return [objects_dict[x][y]]
        }
        return [];
    }


    render(){
        var rows = [...Array(this.props.battlemap.rows).keys()];
        var columns = [...Array(this.props.battlemap.columns).keys()];
        var objects_dict = this.get_objects_dict(this.props.objects)

        var container_style={}
        if(this.props.battlemap.image != null){
            container_style={  
                backgroundImage: "url(" + get_api_url() + "/" + this.props.battlemap.image.replaceAll(" ", "%20") + ")",
            }
        }

        var row_style={height: this.state.cell_size + "px"}
        var cell_style={height: this.state.cell_size + "px", width: this.state.cell_size + "px"}

        return(
            <div className="battlemap-play">
                <DragDropContext onDragEnd={this.handleOnDragEnd}>
                    <Row>
                        {this.props.player_read != true &&
                            <Col xs={2}>
                                {objects_dict.unplaced_player.length > 0 &&
                                    <div>
                                        <h2>Players</h2>
                                        {objects_dict.unplaced_player.map((object) => (
                                            object.x == null && object.y == null &&
                                            <Droppable droppableId={"object-" + object.index}>
                                                    {(provided) => (
                                                        <div ref={provided.innerRef} className="battlemap-selector" {...provided.droppableProps} ref={provided.innerRef}>
                                                            {this.get_draggable_object(object, provided, object.index)}
                                                            {provided.placeholder}
                                                        </div>
                                                    )}
                                            </Droppable>
                                        ))}
                                    </div>
                                }
                                {objects_dict.unplaced_ally.length > 0 &&
                                    <div>
                                        <h2>Allies</h2>
                                        {objects_dict.unplaced_ally.map((object) => (
                                            object.x == null && object.y == null &&
                                            <Droppable droppableId={"object-" + object.index}>
                                                    {(provided) => (
                                                        <div ref={provided.innerRef} className="battlemap-selector" {...provided.droppableProps} ref={provided.innerRef}>
                                                            {this.get_draggable_object(object, provided, object.index)}
                                                            {provided.placeholder}
                                                        </div>
                                                    )}
                                            </Droppable>
                                        ))}
                                    </div>
                                }
                                {objects_dict.unplaced_monster.length > 0 &&
                                    <div>
                                        <h2>Monsters</h2>
                                        {objects_dict.unplaced_monster.map((object) => (
                                            object.x == null && object.y == null &&
                                            <Droppable droppableId={"object-" + object.index}>
                                                    {(provided) => (
                                                        <div ref={provided.innerRef} className="battlemap-selector" {...provided.droppableProps} ref={provided.innerRef}>
                                                            {this.get_draggable_object(object, provided, object.index)}
                                                            {provided.placeholder}
                                                        </div>
                                                    )}
                                            </Droppable>
                                        ))}
                                    </div>
                                }
                                {objects_dict.unplaced_custom.length > 0 &&
                                    <div>
                                        <h2>Custom</h2>
                                        {objects_dict.unplaced_custom.map((object) => (
                                            object.x == null && object.y == null &&
                                            <Droppable droppableId={"object-" + object.index}>
                                                    {(provided) => (
                                                        <div ref={provided.innerRef} className="battlemap-selector" {...provided.droppableProps} ref={provided.innerRef}>
                                                            {this.get_draggable_object(object, provided, object.index)}
                                                            {provided.placeholder}
                                                        </div>
                                                    )}
                                            </Droppable>
                                        ))}
                                    </div>
                                }
                            </Col>
                        }
                        <Col xs={this.props.player_read == true ? 12 : 10}>
                            <div className="battlemap-controls">
                                <div className={this.state.control_state == "MOVEMENT" ? "active" : "inactive"} onClick={() => {this.setState({control_state: "MOVEMENT", control_data: {}})}}>
                                    <i className="far fa-hand-rock"></i>
                                </div>
                                <div className={this.state.control_state == "MEASURE" ? "active" : "inactive"} onClick={() => {this.setState({control_state: "MEASURE", control_data: {}})}}>
                                    <i className="fas fa-ruler"></i>
                                </div>
                                <span>{this.get_control_description()}</span>
                            </div>
                            <div ref={this.battlemapPageRef}>
                                <div className="battlemap-container" style={container_style}>
                                    {rows.map((row) => (
                                        <div className="battlemap-row" style={row_style}>
                                        {columns.map((column) => (
                                            <Droppable droppableId={"cell-" + column + "-" + row} isDropDisabled={this.is_cell_disabled(column, row, objects_dict)}>
                                                {(provided) => (
                                                    <div ref={provided.innerRef} {...provided.droppableProps} ref={provided.innerRef}
                                                        id={"cell-" + column + "-" + row}
                                                        className={"battlemap-cell" + (row == rows.length-1 ? " row-end" : "") + (column == columns.length-1 ? " column-end" : "") + (this.is_cell_blocked(column, row) ? " blocked" : "") + (this.is_cell_marked(column, row) ? " marked" : "")}
                                                        style={cell_style}
                                                        onClick={() => {this.handle_click(column, row)}}
                                                        onMouseEnter={() => {this.handle_hover(column, row)}}
                                                    >
                                                        {this.get_cell_distance_popover(column, row)}
                                                        {this.get_cell_objects(column, row, objects_dict).map((object) => (
                                                            this.get_draggable_object(object, provided, object.index)
                                                        ))}
                                                        {provided.placeholder}
                                                    </div>
                                                )}
                                            </Droppable>
                                        ))}
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </Col>
                    </Row>
                </DragDropContext>
            </div>
        );
    };
}

export default SingleBattlemap;