import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import { get_api_url } from '../shared/Config';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_PUT } from '../shared/DNDRequests';
import ContentCollectionResource from '../shared/ContentCollectionResource';
import UploadImage from '../images/UploadImage';

class EditBattlemap extends Component {

    state = {
        battlemap: {
            image: null,
            rows: 10,
            columns: 10,
            blocked_cells: [],
        },
        cell_size: 80,
    }

    constructor(props) {
        super(props);
        this.battlemapPageRef = React.createRef();
        this.state = {
            battlemap: {
                image: this.props.battlemap.image,
                rows: this.props.battlemap.rows,
                columns: this.props.battlemap.columns,
                blocked_cells: this.props.battlemap.blocked_cells.split(","),
            },
            cell_size: 80,
        };
    }

    componentDidMount(){
        var result =  this.battlemapPageRef.current.offsetWidth / this.props.battlemap.columns
        if(result > 80){
            this.setState({cell_size: 80})
        } else {
            this.setState({cell_size: result})
        }
    }

    update_battlemap = (map_data) => {

        var data = {
            "image": map_data.image,
            "rows": map_data.rows,
            "columns": map_data.columns,
            "blocked_cells": map_data.blocked_cells.join(","),
        }

        DND_PUT(
            '/battlemap/' + this.props.battlemap.id,
            data,
            null,
            null
        )
          
    }

    set_image = (image) => {
        var new_battlemap = this.state.battlemap
        new_battlemap.image = image
        this.setState({battlemap: new_battlemap})
        this.update_battlemap(new_battlemap)
    }

    is_cell_blocked = (x,y) => {
        return this.state.battlemap.blocked_cells.includes(x + "-" + y)
    }

    calculate_cell_size = (column_count) => {
        var result =  this.battlemapPageRef.current.offsetWidth / column_count
        if(result > 80){
            return 80
        } else {
            return result
        }
    }

    toggle_cell_block = (x, y) => {

        var new_battlemap = this.state.battlemap

        var cell = x + "-" + y
        var new_blocked_cells = this.state.battlemap.blocked_cells
        if(new_blocked_cells.includes(cell)){
            new_blocked_cells = new_blocked_cells.filter(item => item !== cell)
        } else {
            new_blocked_cells.push(cell)
        }

        new_battlemap.blocked_cells = new_blocked_cells
        this.setState({battlemap: new_battlemap})
        this.update_battlemap(new_battlemap)
    }

    validate_blocked_cells = (columns, rows) => {
        var new_cells = []
        for(var i = 0; i < this.state.battlemap.blocked_cells.length; i++){
            var cell = this.state.battlemap.blocked_cells[i]
            var x = cell.split("-")[0]
            var y = cell.split("-")[1]
            if(x < columns && y < rows){
                new_cells.push(cell)
            }
        }
        return new_cells
    }

    handleChange = e => {
        var new_battlemap = this.state.battlemap
        var new_value = e.target.value

        if(e.target.name == "columns"){

            if (new_value == null || new_value == 0 || new_value == ""){
                new_value = 1
            }
            
            new_battlemap.columns = parseInt(new_value)
            new_battlemap.blocked_cells = this.validate_blocked_cells(parseInt(new_value), this.state.battlemap.rows)

            this.setState({ 
                cell_size: this.calculate_cell_size(new_value),
                battlemap: new_battlemap
            });
            this.update_battlemap(new_battlemap)
        } else if(e.target.name == "rows"){
            if (new_value == null || new_value == 0 || new_value == ""){
                new_value = 1
            }

            new_battlemap.rows = parseInt(new_value)
            new_battlemap.blocked_cells = this.validate_blocked_cells(parseInt(new_value), this.state.battlemap.rows)
            this.setState({
                battlemap: new_battlemap,
                blocked_cells: this.validate_blocked_cells(this.state.battlemap.columns, parseInt(new_value))
            });
            this.update_battlemap(new_battlemap)
        }
        
    };

    render(){
        if (this.props.active == false){
            return null;
        }

        var rows = [...Array(this.state.battlemap.rows).keys()];
        var columns = [...Array(this.state.battlemap.columns).keys()];

        var container_style={}
        if(this.state.battlemap.image != null){
            container_style={  
                backgroundImage: "url(" + get_api_url() + "/" + this.state.battlemap.image.replaceAll(" ", "%20") + ")",
            }
        }

        var row_style={height: this.state.cell_size + "px"}
        var cell_style={height: this.state.cell_size + "px", width: this.state.cell_size + "px"}
        

        return(
            <div className="battlemap-edit" ref={this.battlemapPageRef}>
                <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_map_fuction}></i> {this.props.battlemap.name}</h2>
                { this.state.battlemap.image == null &&
                    <div>
                        <p>Upload Map</p>
                        <UploadImage callback={this.set_image} exclude_api_url={true}/>
                    </div>
                }
                <Row>
                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Row Count</Form.Label>
                            <Form.Control required value={this.state.battlemap.rows} name="rows" min={2} type="number" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>
                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Column Count</Form.Label>
                            <Form.Control required value={this.state.battlemap.columns} name="columns" min={2} type="number" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>
                </Row>

                <div className="battlemap-container" style={container_style}>
                    {rows.map((row) => (
                        <div className="battlemap-row" style={row_style}>
                            {columns.map((column) => (
                                <div
                                    id={"cell-" + row + "-" + column}
                                    className={"battlemap-cell editing" + (row == rows.length-1 ? " row-end" : "") + (column == columns.length-1 ? " column-end" : "") + (this.is_cell_blocked(column, row) ? " blocked" : "")}
                                    style={cell_style}
                                    onClick={() => this.toggle_cell_block(column, row)}
                                />
                            ))}
                        </div>
                    ))}
                </div>
                <ContentCollectionResource resource='battlemap' resource_id={this.props.battlemap.id}/>
            </div>
        );
    };
}

export default EditBattlemap;