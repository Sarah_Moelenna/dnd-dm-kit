import React from 'react';
import { get_api_url } from '../shared/Config';
import { DND_DELETE } from '.././shared/DNDRequests';
import Card from 'react-bootstrap/Card'
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListBattlemaps = ({ maps, refresh_function, view_map_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/battlemap/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(maps === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="battlemaps">
            {maps.map((map) => (
                <div key={map.id} className="battlemap-search-item">
                    <Card>
                        { map.image &&
                            <Card.Img variant="top" src={get_api_url() + "/" + map.image} />
                        }
                        <Card.Body>
                            <Card.Title>{map.name}</Card.Title>
                            { map.can_edit == true && 
                                <a className="btn btn-primary" onClick={() => view_map_function(map.id)}>Edit Map</a>
                            }
                            { map.can_edit == true && 
                                <DeleteConfirmationButton id={map.id} name="Map" delete_function={delete_page_function} />
                            }
                        </Card.Body>
                    </Card>
                </div>
            ))}
        </div>
    );
}

export default ListBattlemaps;