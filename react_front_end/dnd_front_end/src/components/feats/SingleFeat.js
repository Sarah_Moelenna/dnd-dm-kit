import React, { Component } from 'react';
import ContentCollectionResource from '../shared/ContentCollectionResource';
import { Link } from 'react-router-dom';
import Markdown from 'react-markdown';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class SingleFeat extends Component {
            
    render(){

                return(
                    <div className="feat">
                        <h2>
                            <Link to="/feat" onClick={this.props.close_feat_fuction}>
                                <i className="fas fa-chevron-left clickable-icon"></i>
                            </Link >
                            {this.props.feat.name}
                        </h2>

                        <Row>
                            {this.props.feat.description &&
                                <Col xs={12} md={12}>
                                    <h3>Description</h3>
                                    <Markdown children={this.props.feat.description}/>
                                    <hr/>
                                </Col>
                            }
                        </Row>
                        <ContentCollectionResource resource='feat' resource_id={this.props.feat.id}/>
                    </div>
                )

    };
}

export default SingleFeat;