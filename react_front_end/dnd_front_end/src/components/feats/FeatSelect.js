import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '../shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import FilterFeat from './FilterFeat';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Markdown from 'react-markdown'

const ListFeats = ({ feats, set_sort, current_sort_by, current_sort_value, select_function, disabled_ids}) => {

    function get_sort(sort_by){
        if(current_sort_by == sort_by){
            if(current_sort_value == "DESC"){
                return (
                    <div className="clickable-div" onClick={() => {set_sort(null, null)}}>
                        <i className="fas fa-sort-down"></i>
                    </div>
                ) 
            } else {
                return (
                    <div className="clickable-div" onClick={() => {set_sort(sort_by, "DESC")}}>
                        <i className="fas fa-sort-up"></i>
                    </div>
                ) 
            }
        } else {
            return (
                <div className="clickable-div" onClick={() => {set_sort(sort_by, "ASC")}}>
                    <i className="fas fa-sort"></i>
                </div>
            )
        }
    }

    return (
        <div className="feat-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={8} md={8}>
                        <p className="card-text"><b>Description</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                    </Col>
                </Row>
            </div>
            {feats.map((feat) => (
                <div key={feat.id} className={disabled_ids.includes(feat.id) ? "feat-item disabled" : "feat-item"}>
                    <Row>
                        <Col xs={2} md={2}>
                            <p className="card-text">{feat.name}</p>
                        </Col>
                        <Col xs={8} md={8}>
                            <Markdown children={feat.description}/>
                        </Col>
                        <Col xs={2} md={2}>
                            {!disabled_ids.includes(feat.id) &&
                                <Button onClick={() => {select_function(feat.id, feat.name)}}>Select Feat</Button>
                            }
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

class FeatSelect extends Component {

    state = {
        feats: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false
    }

    componentDidMount() {
        this.refresh_feats(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    select_feat = (feat) => {

        this.setState({toggle: false})
        this.props.callback(feat)
    }

    refresh_feats = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        params['results_per_page'] = 10
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/feat?' + stringify(params),
          (jsondata) => {
            this.setState({ feats: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_feats(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
        this.setState(
          {sort_by: sort_by, sort_value: sort_value},
          this.refresh_feats(this.state.page, this.state.filters, sort_by, sort_value)
        )
      };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_feats(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_feats(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_feats(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id, name) => {

        this.props.select_function(id, name)
        this.setState({toggle: false})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>{this.props.override_button != undefined ? this.props.override_button : "Choose Feat"}</Button>
                }
                <Modal show={this.state.toggle} size="md" className="feat-select-modal select-modal">
                        <Modal.Header>Feats</Modal.Header>
                        <Modal.Body>
                            <FilterFeat filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                            <ListFeats
                                feats={this.state.feats}
                                set_sort={this.set_sort}
                                current_sort_by={this.state.sort_by}
                                current_sort_value={this.state.sort_value}
                                select_function={this.select_function}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default FeatSelect;