import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '../shared/DNDRequests';
import { Link } from 'react-router-dom';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import Markdown from 'react-markdown';


const ListFeatItem = ({ feats, view_feat_function, refresh_function, edit_function}) => {

    function delete_function(id){
        DND_DELETE(
            '/feat/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(feats === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="feat-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={8} md={8}>
                        <p className="card-text"><b>Description</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                    </Col>
                </Row>
            </div>
            {feats.map((feat) => (
                <div key={feat.id} className="feat-item">
                    <hr/>
                    <Row>
                        <Col xs={2} md={2}>
                        <p className="card-text">{feat.name}</p>
                        </Col>
                        <Col xs={8} md={8}>
                            <Markdown children={feat.description}/>
                        </Col>
                        <Col xs={2} md={2}>
                            <Link className="btn btn-primary" to={"/feat/" + feat.id} onClick={() => view_feat_function(feat.id)}>
                                View
                            </Link >
                            { feat.can_edit == true && 
                                <a className="btn btn-primary" onClick={() => edit_function(feat.id)}>Edit</a>
                            }
                            { feat.can_edit == true &&
                                <DeleteConfirmationButton id={feat.id} override_button="Delete" name="Feat" delete_function={delete_function} />
                            }
                        </Col>
                    </Row>
                </div>
            ))}
        </div>
    );
}

export default ListFeatItem;