import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { ListModifiers, createModifier } from '../shared/Modifier';
import { DND_GET, DND_PUT, DND_POST } from '../shared/DNDRequests';
import FeatSelect from './FeatSelect';
import MDEditor from '@uiw/react-md-editor';
import BackConfirmationButton from '../shared/BackConfirmationButton';
import { ListPrerequisites, createPrerequisite } from '../shared/Prerequisites';
import { createAction, ListActions } from '../shared/Action';
import { ListOptionSets, createOptionSet } from '../shared/OptionSet';
import { ListResources, createResource } from '../class/CreateClass';
import SpellSelection from '../spell/SpellSelection';
import Button from 'react-bootstrap/Button'


class CreteFeat extends Component {

    state = {
        name: null,
        description: null,
        gives_spells: false,
        spell_selection: null,
        is_selectable: true,

        prerequisites: [],
        modifiers: [],
        actions: [],
        option_sets: [],
        resources: [],

        has_changes: false,
    }

    componentWillMount() {
        if(this.props.edit_feat_id != undefined && this.props.edit_feat_id != null){
            this.copy_feat(this.props.edit_feat_id)
        }
    };

    copy_feat = (feat_id) => {

        DND_GET(
            '/feat/' + feat_id,
            (response) => {
                this.populate_with_feat_data(response)
            },
            null
        )
    };

    populate_with_feat_data = (feat_data) => {
        var name = feat_data.name
        if(this.props.edit_feat_id == undefined || this.props.edit_feat_id == null){
            name = "Copy of " + name
        }

        this.setState(
            {
                name: name,
                description: feat_data.description,
                gives_spells: feat_data.gives_spells,
                spell_selection: feat_data.spell_selection,
                is_selectable: feat_data.is_selectable,
                modifiers: feat_data.modifiers,
                prerequisites: feat_data.prerequisites,
                actions: feat_data.actions,
                option_sets: feat_data.option_sets,
                resources: feat_data.resources,
            }
        )
    }

    save_feat = () => {

        var final_feat = {
            name: this.state.name,
            description: this.state.description,
            gives_spells: this.state.gives_spells,
            spell_selection: this.state.spell_selection,
            is_selectable: this.state.is_selectable,
            modifiers: this.state.modifiers,
            prerequisites: this.state.prerequisites,
            actions: this.state.actions,
            option_sets: this.state.option_sets,
            resources: this.state.resources,
        }

        if(this.props.edit_feat_id != undefined && this.props.edit_feat_id != null){
            DND_PUT(
                '/feat/' + this.props.edit_feat_id,
                {data: final_feat},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        } else {
            DND_POST(
                '/feat',
                {data: final_feat},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        }
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.setState({ [e.target.name]: new_value, has_changes: true });
    }

    update_text = (attribute, text) => {
        this.setState({[attribute]: text, has_changes: true})
    }

    add_modifier = () => {
        var new_modifier = createModifier()
        var modifiers = this.state.modifiers
        modifiers.push(new_modifier)
        this.setState({modifiers: modifiers, has_changes: true})
    }

    update_modifier = (id, new_object) => {
        var modifiers = []
        for(var i = 0; i < this.state.modifiers.length; i++){
            var modifier = this.state.modifiers[i]
            if(modifier.id == id){
                modifiers.push(new_object)
            } else {
                modifiers.push(modifier)
            }
        }
        this.setState({modifiers: modifiers, has_changes: true})
    }

    delete_modifier = (id) => {
        var modifiers = []
        for(var i = 0; i < this.state.modifiers.length; i++){
            var modifier = this.state.modifiers[i]
            if(modifier.id != id){
                modifiers.push(modifier)
            }
        }
        this.setState({modifiers: modifiers, has_changes: true})
    }

    add_resource = () => {
        var new_resource = createResource()
        var resources = this.state.resources
        resources.push(new_resource)
        this.setState({resources: resources, has_changes: true})
    }

    delete_resource = (id) => {
        var resources = []
        for(var i = 0; i < this.state.resources.length; i++){
            var resource = this.state.resources[i]
            if(resource.id != id){
                resources.push(resource)
            }
        }
        this.setState({resources: resources, has_changes: true})
    }

    update_resource = (id, attribute, value) => {
        var resources = []
        for(var i = 0; i < this.state.resources.length; i++){
            var resource = this.state.resources[i]
            if(resource.id == id){
                resource[attribute] = value
            }
            resources.push(resource)
        }
        this.setState({resources: resources, has_changes: true})
    }

    add_prerequisite = () => {
        var new_prerequisite = createPrerequisite()
        var prerequisites = this.state.prerequisites
        prerequisites.push(new_prerequisite)
        this.setState({prerequisites: prerequisites, has_changes: true})
    }

    update_prerequisite = (id, new_object) => {
        var prerequisites = []
        for(var i = 0; i < this.state.prerequisites.length; i++){
            var prerequisite = this.state.prerequisites[i]
            if(prerequisite.id == id){
                prerequisites.push(new_object)
            } else {
                prerequisites.push(prerequisite)
            }
        }
        this.setState({prerequisites: prerequisites, has_changes: true})
    }

    delete_prerequisite = (id) => {
        var prerequisites = []
        for(var i = 0; i < this.state.prerequisites.length; i++){
            var prerequisite = this.state.prerequisites[i]
            if(prerequisite.id != id){
                prerequisites.push(prerequisite)
            }
        }
    }

    add_action = () => {
        var new_action = createAction()
        var actions = this.state.actions
        actions.push(new_action)
        this.setState({actions: actions, has_changes: true})
    }

    delete_action = (id) => {
        var actions = []
        for(var i = 0; i < this.state.actions.length; i++){
            var action = this.state.actions[i]
            if(action.id != id){
                actions.push(action)
            }
        }
        this.setState({actions: actions, has_changes: true})
    }

    update_action = (id, attribute, value) => {
        var actions = []
        for(var i = 0; i < this.state.actions.length; i++){
            var action = this.state.actions[i]
            if(action.id == id){
                action[attribute] = value
            }
            actions.push(action)
        }
        this.setState({actions: actions, has_changes: true})
    }

    add_option_set = () => {
        var new_option_set = createOptionSet()
        var option_sets = this.state.option_sets
        option_sets.push(new_option_set)
        this.setState({option_sets: option_sets, has_changes: true})
    }

    delete_option_set = (id) => {
        var option_sets = []
        for(var i = 0; i < this.state.option_sets.length; i++){
            var option_set = this.state.option_sets[i]
            if(option_set.id != id){
                option_sets.push(option_set)
            }
        }
        this.setState({option_sets: option_sets, has_changes: true})
    }

    update_option_set = (id, attribute, value) => {
        var option_sets = []
        for(var i = 0; i < this.state.option_sets.length; i++){
            var option_set = this.state.option_sets[i]
            if(option_set.id == id){
                option_set[attribute] = value
            }
            option_sets.push(option_set)
        }
        this.setState({option_sets: option_sets, has_changes: true})
    }
            
    render(){

        return(
            <div className="feat">
                {(this.props.edit_feat_id == undefined || this.props.edit_feat_id == null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Create Feat</h2>
                        <FeatSelect select_function={this.copy_feat} override_button="Copy Existing Feat"/>
                    </div>
                }
                {(this.props.edit_feat_id != undefined && this.props.edit_feat_id != null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Edit Feat</h2>
                    </div>
                }
                <Row className="feat-settings">
                    <Col xs={12} md={12}>
                        <h3>Basic Information</h3>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control value={this.state.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group>
                            <Form.Label>Gives Spells?</Form.Label>
                            <Form.Control name="gives_spells" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.gives_spells == true} value={true}>True</option>
                                    <option selected={this.state.gives_spells == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group>
                            <Form.Label>Is Selectable?</Form.Label>
                            <Form.Control name="is_selectable" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.is_selectable == true} value={true}>True</option>
                                    <option selected={this.state.is_selectable == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicSpecialtraits">
                            <Form.Label>Description</Form.Label>
                            <MDEditor
                                value={this.state.description}
                                preview="edit"
                                onChange={(text) => {this.update_text("description", text)}}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.modifiers.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.modifiers.length} Modifiers
                                    <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListModifiers modifiers={this.state.modifiers} update_modifier={this.update_modifier} delete_modifier={this.delete_modifier} display_type="SPELL" scale_type={this.state.scale_type}/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.modifiers.length} Modifiers
                                    <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.actions.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.actions.length} Actions
                                    <span className="clickable-div add-button" onClick={() => {this.add_action()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListActions actions={this.state.actions} update_action={this.update_action} delete_action={this.delete_action} display_type="RACE"/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.actions.length} Actions
                                    <span className="clickable-div add-button" onClick={() => {this.add_action()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    {this.state.gives_spells == true &&
                        <Col xs={12} md={12}>
                            <h4>Spells Selection</h4>
                            <SpellSelection
                                spell_selection={this.state.spell_selection}
                                update_function={(spell_selection_data) => {this.setState({spell_selection: spell_selection_data, has_changes: true})}}
                                disallow_groups={true}    
                            />
                        </Col>
                    }

                    <Col xs={12} md={12}>
                        {this.state.option_sets.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.option_sets.length} Option Sets
                                    <span className="clickable-div add-button" onClick={() => {this.add_option_set()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListOptionSets option_sets={this.state.option_sets} update_option_set ={this.update_option_set} delete_option_set={this.delete_option_set}/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.option_sets.length} Option Sets
                                    <span className="clickable-div add-button" onClick={() => {this.add_option_set()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.resources.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.resources.length} Resources
                                    <span className="clickable-div add-button" onClick={() => {this.add_resource()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListResources resources={this.state.resources} update_resource={this.update_resource} delete_resource={this.delete_resource}/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.resources.length} Resources
                                    <span className="clickable-div add-button" onClick={() => {this.add_resource()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.prerequisites.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.prerequisites.length} Prerequisites
                                    <span className="clickable-div add-button" onClick={() => {this.add_prerequisite()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListPrerequisites prerequisites={this.state.prerequisites} update_prerequisite={this.update_prerequisite} delete_prerequisite={this.delete_prerequisite}/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.prerequisites.length} Prerequisites
                                    <span className="clickable-div add-button" onClick={() => {this.add_prerequisite()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                </Row>
                <br/>
                <Button variant="primary" type="submit" onClick={this.save_feat}>
                    Save Feat
                </Button>
            </div>
        )

    };
}

export default CreteFeat;