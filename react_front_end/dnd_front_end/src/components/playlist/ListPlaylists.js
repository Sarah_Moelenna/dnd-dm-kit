import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '../shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListPlaylists = ({ playlists, refresh_function, view_playlist_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/playlist/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(playlists === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="playlist-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={3} md={3} className="cenetered-column">
                        <p className="card-text"><b>No. Items</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        
                    </Col>
                    <Col xs={1} md={1}>
                        <p className="card-text"><b>Controls</b></p>
                    </Col>
                </Row>
                <hr/>
            </div>
            {playlists.map((playlist) => (
                <div key={playlist.id} className="playlist-item">
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{playlist.name}</p>
                        </Col>
                        <Col xs={3} md={3} className="cenetered-column">
                        <p className="card-text">{playlist.audio_item_count}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <a className="btn btn-primary" onClick={() => view_playlist_function(playlist.id)}>View Playlist</a>
                        </Col>
                        <Col xs={1} md={1}>
                            <DeleteConfirmationButton id={playlist.id} name="Playlist" delete_function={delete_page_function} />
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListPlaylists;