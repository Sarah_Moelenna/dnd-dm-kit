import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'

class FilterPlaylist extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="playlist_filters">
                <Form.Group controlId="formBasicName">
                    <Form.Label>Playlist Name</Form.Label>
                    <Form.Control value={this.props.filters["name"]} name="name" type="text" placeholder="Search Playlist Names" onChange={this.handleChange}/>
                </Form.Group>
            </Form>
        );
    };
}

export default FilterPlaylist;