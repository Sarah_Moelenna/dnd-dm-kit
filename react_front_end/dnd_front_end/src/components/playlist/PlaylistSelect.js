import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '../shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import FilterPlaylist from './FilterPlaylist';
import Card from 'react-bootstrap/esm/Card';
import APIButton from '../shared/APIButton';

const create_playlist_command = (id, play_type, playlist_type) => {
    var data = {
        "music_play_type": play_type,
        "playlist_id": id,
        "type": playlist_type
    }
    return {
        "command_code": "CMD_PLAYLIST",
        "data": data
    }
}

export const SinglePlaylistListItem = ({playlist_item, select_function, disabled_ids}) => {
    return (
        <Card>
            <Card.Title>{playlist_item.name} <APIButton data={create_playlist_command(playlist_item.id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton></Card.Title>
            
            <Card.Body>
                <div className='icon'>
                    { playlist_item.audio_type == 'MUSIC' &&
                        <i class="fas fa-music"></i>
                    }
                    { playlist_item.audio_type == 'AMBIENCE' &&
                        <i class="fas fa-cloud-rain"></i>
                    }
                </div>
                {!disabled_ids.includes(playlist_item.id) &&
                    <Button onClick={() => {select_function(playlist_item.id, playlist_item)}}>Select Playlist</Button>
                }
            </Card.Body>
        </Card>
    )
}

const ListPlaylist = ({ playlist_items, select_function, disabled_ids}) => {

    return (
        <div className="audio-items">
            {playlist_items.map((playlist_item) => (
                <div key={playlist_item.id} className={disabled_ids.includes(playlist_item.id) ? "audio-item disabled" : "audio-item"}>
                    <SinglePlaylistListItem playlist_item={playlist_item} select_function={select_function} disabled_ids={disabled_ids}/>
                </div>
            ))}
        </div>
    );
}

class PlaylistSelect extends Component {

    state = {
        playlist_items: [],
        page: 1,
        total_pages: 1,
        filters: {},
        toggle: false
    }

    componentDidMount() {
        this.refresh_playlist(this.state.page, this.state.filters)
    };

    select_playlist = (playlist) => {

        this.setState({toggle: false})
        this.props.callback(playlist)
    }

    refresh_playlist = (page, filters) => {
        var params = filters
        
        params['page'] = page
        params['results_per_page'] = 20
        if(this.props.audio_type_filter != undefined && this.props.audio_type_filter != null){
            params['audio_type'] = this.props.audio_type_filter
        }

        DND_GET(
          '/playlist?' + stringify(params),
          (jsondata) => {
            this.setState({ playlist_items: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_playlist(page, this.state.filters)
    };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_playlist(1, new_filters))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_playlist(1, []))
    };

    quick_refresh = () => {
      this.refresh_playlist(this.state.page, this.state.filters)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id, object) => {

        this.props.select_function(id, object)
        this.setState({toggle: false})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>Choose Playlist</Button>
                }
                <Modal show={this.state.toggle} size="md" className="playlist-select-modal select-modal">
                        <Modal.Header>Playlist</Modal.Header>
                        <Modal.Body>
                            <FilterPlaylist filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters} hide_type_filter={true}/>
                            <ListPlaylist
                                playlist_items={this.state.playlist_items}
                                select_function={this.select_function}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default PlaylistSelect;