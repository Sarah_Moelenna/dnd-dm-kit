import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_POST } from '../shared/DNDRequests';

class CreatePlaylist extends Component {

    state = {
        name: null,
        audio_type: null
    }

    handleSubmit = (event) => {
        
        if( this.state.name == null){
            event.preventDefault();
            return
        }

        var data = {
            name: this.state.name,
            audio_type: this.state.audio_type
        }

        DND_POST(
            '/playlist',
            data,
            (response) => {
                this.props.refresh_function()
                this.setState({ name: null, audio_type: null })
            },
            null
        )

        event.preventDefault();
      
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };


    render(){
            return(
                <Form onSubmit={this.handleSubmit} className="create-playlist">
                    <Row>

                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control required value={this.state.name ? this.state.name : ''} name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicSize">
                                <Form.Label>Audio Type</Form.Label>
                                <Form.Control required name="audio_type" as="select" onChange={this.handleChange} custom>
                                        <option selected="true" value="">-</option>
                                        <option value="AMBIENCE" selected={this.state.audio_type == "AMBIENCE"}>Ambient Sound</option>
                                        <option value="MUSIC" selected={this.state.audio_type == "MUSIC"}>Music</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>

                        <Col xs={2} md={2}>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Col>
                    </Row>
                </Form>
            );
    };
}

export default CreatePlaylist;