import React, { Component } from 'react';
import { DND_POST, DND_DELETE } from '../shared/DNDRequests';
import MusicSelect from '../music/MusicSelect';
import APIButton from '.././shared/APIButton';
import Card from 'react-bootstrap/esm/Card';
import Button from 'react-bootstrap/Button';

const create_audio_command = (id, play_type) => {
    var data = {
        "music_play_type": play_type,
        "audio_id": id
    }
    return {
        "command_code": "CMD_PLAY",
        "data": data
    }
}

const create_playlist_command = (id, play_type, playlist_type) => {
    var data = {
        "music_play_type": play_type,
        "playlist_id": id,
        "type": playlist_type
    }
    return {
        "command_code": "CMD_PLAYLIST",
        "data": data
    }
}

class SinglePlaylist extends Component {

    add_audio = (audio_id) => {
        DND_POST(
            '/playlist/' + this.props.playlist.id + '/audio/' + audio_id,
            null,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    }

    remove_audio = (audio_id) => {
        DND_DELETE(
            '/playlist/' + this.props.playlist.id + '/audio/' + audio_id,
            null,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    }

    get_audio_ids = () => {
        let ids = []
        for(let i = 0; i < this.props.playlist.audio_items.length; i++){
            ids.push(this.props.playlist.audio_items[i].id)
        }
        
        return ids
    }

    render(){

        return(
            <div>
                <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_playlist_fuction}></i> {this.props.playlist.name}</h2>
                <MusicSelect select_function={this.add_audio} disabled_ids={this.get_audio_ids()} audio_type_filter={this.props.playlist.audio_type}/>
                <h3>Current Audio <APIButton data={create_playlist_command(this.props.playlist.id, "LOOP", this.props.playlist.audio_type)} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton></h3>
                <div className='audio-items'>
                    {this.props.playlist.audio_items.map((audio_item) => (
                        <div key={audio_item.id} className="audio-item">
                            <Card>
                                <Card.Title>{audio_item.name} <APIButton data={create_audio_command(audio_item.id, "LOOP")} api_endpoint="/commands" method="POST" icon="fas fa-play"></APIButton></Card.Title>
                                
                                <Card.Body>
                                    <div className='icon'>
                                        { audio_item.audio_type == 'MUSIC' &&
                                            <i class="fas fa-music"></i>
                                        }
                                        { audio_item.audio_type == 'AMBIENCE' &&
                                            <i class="fas fa-cloud-rain"></i>
                                        }
                                        { audio_item.audio_type == 'EFFECT' &&
                                            <i class="fas fa-volume-up"></i>
                                        }
                                    </div>
                                    <Button onClick={()=>{this.remove_audio(audio_item.id)}}>Remove</Button>
                                </Card.Body>
                            </Card>
                        </div>
                    ))}
                </div>
            </div>
        )

    };
}

export default SinglePlaylist;