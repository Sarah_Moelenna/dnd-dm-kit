import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card'
import Spinner from '.././shared/Spinner'

const ListCharacters = ({ characters}) => {
    if(characters === null) {
        return (<p>No Active Characters or Campaign to track</p>)
    }
    if(characters === undefined || characters.length == 0) {
        return (
            <Spinner/>
        )
    }
    return (
        <div className="characters">
            {characters.map((character) => (
                <div key={character.name}>
                    <Card  style={{ maxWidth: '18rem' }} className="character">
                        <Row>
                            <Col xs={8} md={8}>
                                <Card.Body>
                                    <Card.Title>{character.name}</Card.Title>
                                    <Card.Text>
                                    <b>Health:</b> {character.current_health} / {character.max_health}
                                    </Card.Text>
                                    { character.url != null &&                                        
                                        <a className="btn btn-primary" target="_blank" href={character.url}>Character Sheet</a>
                                    }
                                </Card.Body>
                            </Col>
                            <Col xs={4} md={4}>
                                <Card.Img className="avatar" variant="top" src={character.avatar} />
                            </Col>
                        </Row>
                    </Card>
                    <br/>
                </div>
            ))}
        </div>
    );
}

export default ListCharacters;