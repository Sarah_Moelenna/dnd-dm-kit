import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Markdown from 'react-markdown';
import ContentCollectionResource from '../shared/ContentCollectionResource';


class SingleRace extends Component {

    render(){

        return(
            <div className="single-race">
                <div>
                    <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_function}></i> {this.props.race.full_name}</h2>
                </div>

                <Row>
                    <Col xs={6} md={6}>
                        <Markdown children={this.props.race.description}/>
                    </Col>

                    <Col xs={6} md={6} className="avatar">
                        <div className="avatar"
                            style={{  
                                backgroundImage: "url(" + this.props.race.large_avatar_url + ")",
                            }}
                        />
                    </Col>
                </Row>
                
                <div className="racial-trait-items">
                    {this.props.race.racial_traits.map((racial_trait) => (
                        <div className="racial-trait-item">
                            <h3>{racial_trait.name}</h3>
                            <Markdown children={racial_trait.description}/>
                        </div>
                    ))}
                </div>

                <ContentCollectionResource resource='race' resource_id={this.props.race.id}/>
            </div>
        )

    };
}

export default SingleRace;