import React, { Component, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '../shared/DNDRequests';
import { stringify } from 'query-string';
import FilterRace from './FilterRace';
import { Collapse } from 'reactstrap';
import { get_api_url } from '../shared/Config';

const RaceItem = ({race, select_race_function}) => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    function get_image(image){
        if(image == null){
            return get_api_url() + "/static/race-default.jpg"
        }
        return image
    }

    return (
        <div key={race.name}>
            <div className="race-item">
                <Row>
                    <Col xs={6} md={6}>
                        <div className="race-info">
                            <div className="race-image"
                                style={{  
                                    backgroundImage: "url(" + get_image(race.image) + ")",
                                }}
                            />
                            <div className="race-name">
                            {race.races.length > 1 ?
                                <p className="card-text">{race.name}</p>
                                :
                                <p className="card-text">{race.races[0].full_name}</p>
                            }
                            </div>
                        </div>
                    </Col>
                    <Col xs={6} md={6} className="race-button">
                        {race.races.length > 1 ?
                            <div className="clickable-div" onClick={toggle}><i className={isOpen == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            :
                            <div>
                                <Button className="btn btn-primary" onClick={() => select_race_function(race.races[0].id)}>Select Race</Button>
                            </div>
                        }
                    </Col>
                </Row>
            </div>
            {race.races.length > 1 &&
                <Collapse isOpen={isOpen}>
                    <div key={race.name + '-subs'} className="sub-race-items">
                        {race.races.map((sub_race) => (
                            <div key={race.name + sub_race.full_name} className="sub-race-item">
                                <Row>
                                    <Col xs={6} md={6}>
                                        <div className="race-info">
                                            <div className="race-image"
                                                style={{  
                                                    backgroundImage: "url(" + get_image(sub_race.portrait_avatar_url) + ")",
                                                }}
                                            />
                                            <div className="race-name">
                                                <p className="card-text">{sub_race.full_name}</p>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col xs={6} md={6} className="race-button">
                                        <div>
                                            <Button className="btn btn-primary" onClick={() => select_race_function(sub_race.id)}>Select Race</Button>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        ))}
                    </div>
                </Collapse>
            }
        </div>
    );
}

const ListRaces = ({ races, select_race_function}) => {

    if(races === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="race-items">
            {races.map((race) => (
                <RaceItem race={race} select_race_function={select_race_function} />
            ))}
        </div>
    );
}

class RaceSelect extends Component {

    state = {
        races: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false
    }

    componentDidMount() {
        this.refresh_races(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    select_race = (race) => {

        this.setState({toggle: false})
        this.props.callback(race)
    }

    refresh_races = () => {
        DND_GET(
          '/race?' + stringify(this.state.filters),
          (jsondata) => {
            this.setState({ races: jsondata})
          },
          null
        )
    };

    get_structured_races = () => {
        var base_races = {}
        var races = []

        for(var i = 0; i < this.state.races.length; i++){
            var race = this.state.races[i]
            if(base_races[race.base_name] == undefined){
                base_races[race.base_name] = {
                    races: [],
                    name: race.base_name,
                    image: race.portrait_avatar_url
                }
            }

            base_races[race.base_name].races.push(race)
        }

        var keys = Object.keys(base_races).sort()
        for(var i = 0; i < keys.length; i++){
            races.push(base_races[keys[i]])
        }

        return races
    }

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_races(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_races(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_races(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id) => {
        this.props.select_function(id)
        this.setState({toggle: false})
    }

    render(){
        var races = this.get_structured_races()

        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>{this.props.override_button != undefined ? this.props.override_button : "Choose Race"}</Button>
                }
                <Modal show={this.state.toggle} size="md" className="race-select-modal select-modal race-page">
                        <Modal.Header>Races</Modal.Header>
                        <Modal.Body>
                            <FilterRace filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                            <ListRaces
                                races={races}
                                select_race_function={this.select_function}
                            />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default RaceSelect;