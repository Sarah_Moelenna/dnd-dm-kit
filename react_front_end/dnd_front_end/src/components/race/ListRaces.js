import React, { useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { get_api_url } from '../shared/Config';
import { DND_DELETE } from '.././shared/DNDRequests';
import { Collapse, Button } from 'reactstrap';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const RaceItem = ({race, refresh_function, view_race_function, edit_race_function}) => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    function delete_function(id){
        DND_DELETE(
            '/race/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    function get_image(image){
        if(image == null){
            return get_api_url() + "/static/race-default.jpg"
        }
        return image
    }

    return (
        <div key={race.name}>
            <div className="race-item">
                <Row>
                    <Col xs={6} md={6}>
                        <div className="race-info">
                            <div className="race-image"
                                style={{  
                                    backgroundImage: "url(" + get_image(race.image) + ")",
                                }}
                            />
                            <div className="race-name">
                            {race.races.length > 1 ?
                                <p className="card-text">{race.name}</p>
                                :
                                <p className="card-text">{race.races[0].full_name}</p>
                            }
                            </div>
                        </div>
                    </Col>
                    <Col xs={6} md={6} className="race-button">
                        {race.races.length > 1 ?
                            <div className="clickable-div" onClick={toggle}><i className={isOpen == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            :
                            <div>
                                <Button className="btn btn-primary" onClick={() => view_race_function(race.races[0].id)}>View Race</Button>
                                { race.races[0].can_edit == true && 
                                    <Button className="btn btn-primary" onClick={() => edit_race_function(race.races[0].id)}>Edit Race</Button>
                                }
                                { race.races[0].can_edit == true &&
                                    <DeleteConfirmationButton id={race.races[0].id} override_button="Delete" name="Race" delete_function={delete_function} />
                                }
                            </div>
                        }
                    </Col>
                </Row>
            </div>
            {race.races.length > 1 &&
                <Collapse isOpen={isOpen}>
                    <div key={race.name} className="sub-race-items">
                        {race.races.map((sub_race) => (
                            <div key={race.name} className="sub-race-item">
                                <Row>
                                    <Col xs={6} md={6}>
                                        <div className="race-info">
                                            <div className="race-image"
                                                style={{  
                                                    backgroundImage: "url(" + get_image(sub_race.portrait_avatar_url) + ")",
                                                }}
                                            />
                                            <div className="race-name">
                                                <p className="card-text">{sub_race.full_name}</p>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col xs={6} md={6} className="race-button">
                                        <div>
                                            <Button className="btn btn-primary" onClick={() => view_race_function(sub_race.id)}>View Race</Button>
                                            { sub_race.can_edit == true && 
                                                <Button className="btn btn-primary" onClick={() => edit_race_function(sub_race.id)}>Edit Race</Button>
                                            }
                                            { sub_race.can_edit == true &&
                                                <DeleteConfirmationButton id={sub_race.id} override_button="Delete" name="Race" delete_function={delete_function} />
                                            }
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        ))}
                    </div>
                </Collapse>
            }
        </div>
    );
}

const ListRaces = ({ races, refresh_function, view_race_function, edit_race_function}) => {

    if(races === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="race-items">
            {races.map((race) => (
                <RaceItem race={race} refresh_function={refresh_function} view_race_function={view_race_function} edit_race_function={edit_race_function} />
            ))}
        </div>
    );
}

export default ListRaces;