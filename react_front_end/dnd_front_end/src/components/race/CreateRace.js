import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MDEditor from '@uiw/react-md-editor';
import { Collapse } from 'reactstrap';
import Form from 'react-bootstrap/Form'
import { DND_GET, DND_PUT, DND_POST } from '.././shared/DNDRequests';
import { Sizes } from '../monster/Data';
import { v4 as uuidv4 } from 'uuid';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton'
import { ListModifiers, createModifier } from '../shared/Modifier';
import { ListActions, createAction } from '../shared/Action';
import { ListOptionSets, createOptionSet } from '../shared/OptionSet';
import UploadImage from '../images/UploadImage';
import SpellSelection from '../spell/SpellSelection';
import Button from 'react-bootstrap/Button';
import BackConfirmationButton from '../shared/BackConfirmationButton';


const createRacialTrait = (starting_order) => {
    return {
        "id": uuidv4(),
        "display_order": starting_order,
        "name": "Unnamed Racial Trait",
        "description": null,
        "snippet": null,
        "hide_in_builder": null,
        "hide_in_sheet": null,
        "actions": [],
        "modifiers": [],
        "option_sets": [],
        "spell_selection": null,
    }
}

class RacialTrait extends Component {

    state = {
        toggle: false,
    }

    update_text = (text) => {
        this.props.update_racial_trait(this.props.racial_trait.id, "description", text)
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.props.update_racial_trait(this.props.racial_trait.id, e.target.name, new_value)
    }

    add_modifier = () => {
        var new_modifier = createModifier()
        var modifiers = this.props.racial_trait.modifiers
        modifiers.push(new_modifier)
        this.props.update_racial_trait(this.props.racial_trait.id, "modifiers", modifiers)
    }

    add_action = () => {
        var new_action = createAction()
        var actions = this.props.racial_trait.actions
        actions.push(new_action)
        this.props.update_racial_trait(this.props.racial_trait.id, "actions", actions)
    }

    add_option_set = () => {
        var new_option_set = createOptionSet()
        var option_sets = this.props.racial_trait.option_sets
        option_sets.push(new_option_set)
        this.props.update_racial_trait(this.props.racial_trait.id, "option_sets", option_sets)
    }

    delete_modifier = (id) => {
        var modifiers = []
        for(var i = 0; i < this.props.racial_trait.modifiers.length; i++){
            var modifier = this.props.racial_trait.modifiers[i]
            if(modifier.id != id){
                modifiers.push(modifier)
            }
        }
        this.props.update_racial_trait(this.props.racial_trait.id, "modifiers", modifiers)
    }

    delete_action = (id) => {
        var actions = []
        for(var i = 0; i < this.props.racial_trait.actions.length; i++){
            var action = this.props.racial_trait.actions[i]
            if(action.id != id){
                actions.push(action)
            }
        }
        this.props.update_racial_trait(this.props.racial_trait.id, "actions", actions)
    }

    delete_option_set = (id) => {
        var option_sets = []
        for(var i = 0; i < this.props.racial_trait.option_sets.length; i++){
            var option_set = this.props.racial_trait.option_sets[i]
            if(option_set.id != id){
                option_sets.push(option_set)
            }
        }
        this.props.update_racial_trait(this.props.racial_trait.id, "option_sets", option_sets)
    }


    update_modifier = (id, new_object) => {
        var modifiers = []
        for(var i = 0; i < this.props.racial_trait.modifiers.length; i++){
            var modifier = this.props.racial_trait.modifiers[i]
            if(modifier.id == id){
                modifiers.push(new_object)
            } else {
                modifiers.push(modifier)
            }
        }
        this.props.update_racial_trait(this.props.racial_trait.id, "modifiers", modifiers)
    }

    update_action = (id, new_object) => {
        var actions = []
        for(var i = 0; i < this.props.racial_trait.actions.length; i++){
            var action = this.props.racial_trait.actions[i]
            if(action.id == id){
                actions.push(new_object)
            } else {
                actions.push(action)
            }
        }
        this.props.update_racial_trait(this.props.racial_trait.id, "actions", actions)
    }

    update_option_set = (id, new_object) => {
        var option_sets = []
        for(var i = 0; i < this.props.racial_trait.option_sets.length; i++){
            var option_set = this.props.racial_trait.option_sets[i]
            if(option_set.id == id){
                option_sets.push(new_object)
            } else {
                option_sets.push(option_set)
            }
        }
        this.props.update_racial_trait(this.props.racial_trait.id, "option_sets", option_sets)
    }

    update_spell_selection = (spell_selection) => {
        this.props.update_racial_trait(this.props.racial_trait.id, "spell_selection", spell_selection)
    }

    render(){

        return(
            <Row key={this.props.racial_trait.id} className="racial-trait-item">
                <Col xs={4} md={4} className="racial-trait-name">
                    <p>{this.props.racial_trait.name}</p>
                </Col>
                <Col xs={4} md={4} className="racial-trait-snippet">
                    <p>{this.props.racial_trait.snippet}</p>
                </Col>
                <Col xs={2} md={2} className="racial-trait-snippet">
                    <DeleteConfirmationButton id={this.props.racial_trait.id} name="Racial Trait" delete_function={this.props.delete_racial_trait} override_button="Delete"/>
                </Col>
                <Col xs={2} md={2} className="racial-trait-dropper">
                    <div className="clickable-div" onClick={() => {this.setState({toggle: !this.state.toggle})}}><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                </Col>
                <Col xs={12} md={12}>
                    <Collapse isOpen={this.state.toggle}>
                        <Row>
                            <Col xs={6} md={6}>
                                <Form.Group controlId="formBasicName">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control value={this.props.racial_trait.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                                </Form.Group>
                            </Col>

                            <Col xs={6} md={6}>
                                <Form.Group controlId="formBasicName">
                                    <Form.Label>Snippet</Form.Label>
                                    <Form.Control value={this.props.racial_trait.snippet} required name="snippet" type="text" placeholder="" onChange={this.handleChange} />
                                </Form.Group>
                            </Col>

                            <Col xs={4} md={4}>
                                <Form.Group controlId="formBasicSize">
                                    <Form.Label>Hide In Builder</Form.Label>
                                    <Form.Control name="hide_in_builder" as="select" onChange={this.handleChange} custom>
                                            <option selected={this.props.racial_trait.hide_in_builder == true} value={true}>Yes</option>
                                            <option selected={this.props.racial_trait.hide_in_builder == false} value={false}>No</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>

                            <Col xs={4} md={4}>
                                <Form.Group controlId="formBasicSize">
                                    <Form.Label>Hide In Sheet</Form.Label>
                                    <Form.Control name="hide_in_sheet" as="select" onChange={this.handleChange} custom>
                                            <option selected={this.props.racial_trait.hide_in_sheet == true} value={true}>Yes</option>
                                            <option selected={this.props.racial_trait.hide_in_sheet == false} value={false}>No</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>

                            <Col xs={4} md={4}>

                            </Col>

                            <Col xs={6} md={6}>
                                <Form.Label>Description</Form.Label>
                                <MDEditor
                                    value={this.props.racial_trait.description}
                                    preview="edit"
                                    onChange={this.update_text}
                                />
                            </Col>
                            
                            <Col xs={12} md={12}>
                                {this.props.racial_trait.modifiers.length > 0 ?
                                    <div>
                                        <hr/>
                                        <h4>
                                            {this.props.racial_trait.modifiers.length} Modifiers
                                            <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                                <i className="fas fa-plus"></i>
                                            </span>
                                        </h4>
                                        <ListModifiers modifiers={this.props.racial_trait.modifiers} update_modifier={this.update_modifier} delete_modifier={this.delete_modifier} display_type="RACE"/>
                                    </div>
                                    :
                                    <div>
                                        <hr/>
                                        <h4>
                                            {this.props.racial_trait.modifiers.length} Modifiers
                                            <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                                <i className="fas fa-plus"></i>
                                            </span>
                                        </h4>
                                    </div>
                                }
                            </Col>

                            <Col xs={12} md={12}>
                                {this.props.racial_trait.actions.length > 0 ?
                                    <div>
                                        <hr/>
                                        <h4>
                                            {this.props.racial_trait.actions.length} Actions
                                            <span className="clickable-div add-button" onClick={() => {this.add_action()}}>
                                                <i className="fas fa-plus"></i>
                                            </span>
                                        </h4>
                                        <ListActions actions={this.props.racial_trait.actions} update_action={this.update_action} delete_action={this.delete_action}/>
                                    </div>
                                    :
                                    <div>
                                        <hr/>
                                        <h4>
                                            {this.props.racial_trait.actions.length} Actions
                                            <span className="clickable-div add-button" onClick={() => {this.add_action()}}>
                                                <i className="fas fa-plus"></i>
                                            </span>
                                        </h4>
                                    </div>
                                }
                            </Col>

                            <Col xs={12} md={12}>
                                <hr/>
                                <h4>Spells Selection</h4>
                                <SpellSelection
                                    spell_selection={this.props.racial_trait.spell_selection ? this.props.racial_trait.spell_selection : null}
                                    update_function={this.update_spell_selection}
                                />
                            </Col>

                            <Col xs={12} md={12}>
                                {this.props.racial_trait.option_sets.length > 0 ?
                                    <div>
                                        <hr/>
                                        <h4>
                                            {this.props.racial_trait.option_sets.length} Option Sets
                                            <span className="clickable-div add-button" onClick={() => {this.add_option_set()}}>
                                                <i className="fas fa-plus"></i>
                                            </span>
                                        </h4>
                                        <ListOptionSets option_sets={this.props.racial_trait.option_sets} update_option_set ={this.update_option_set} delete_option_set={this.delete_option_set}/>
                                    </div>
                                    :
                                    <div>
                                        <hr/>
                                        <h4>
                                            {this.props.racial_trait.option_sets.length} Option Sets
                                            <span className="clickable-div add-button" onClick={() => {this.add_option_set()}}>
                                                <i className="fas fa-plus"></i>
                                            </span>
                                        </h4>
                                    </div>
                                }
                            </Col>
                            
                        </Row>
                    </Collapse>
                </Col>
                
            </Row>
        )

    };
}


class CreateRace extends Component {

    state = {
        full_name: null,
        size: null,
        is_sub_race: null,
        walk: null,
        burrow: null,
        climb: null,
        swim: null,
        fly: null,
        description: null,
        avatar: null,
        portrait: null,
        racial_traits: [],

        has_changes: false,
    }

    componentWillMount() {
        if(this.props.edit_race_id != undefined && this.props.edit_race_id != null){
            this.copy_race(this.props.edit_race_id)
        }
    };

    copy_race = (race_id) => {

        DND_GET(
            '/race/' + race_id,
            (response) => {
                this.populate_with_race_data(response)
            },
            null
        )
    };

    populate_with_race_data = (race_data) => {
        this.setState(
            {
                full_name: race_data.full_name,
                size: race_data.size,
                is_sub_race: race_data.is_sub_race,
                walk: race_data.weight_speeds.normal.walk,
                fly: race_data.weight_speeds.normal.fly,
                burrow: race_data.weight_speeds.normal.burrow,
                swim: race_data.weight_speeds.normal.swim,
                climb: race_data.weight_speeds.normal.climb,
                description: race_data.description,
                avatar: race_data.large_avatar_url,
                portrait: race_data.portrait_avatar_url,
                racial_traits: race_data.racial_traits,
                base_name: race_data.base_name
            }
        )
    }

    save_race = () => {
        var speeds = {
            normal: {
                walk: this.state.walk,
                fly: this.state.fly,
                burrow: this.state.burrow,
                swim: this.state.swim,
                climb: this.state.climb,
            }
        }

        var final_race = {
            full_name: this.state.full_name,
            size: this.state.size,
            is_sub_race: this.state.is_sub_race,
            weight_speeds: speeds,
            description: this.state.description,
            large_avatar_url: this.state.avatar,
            portrait_avatar_url: this.state.portrait,
            racial_traits: this.state.racial_traits,
            base_name: this.state.base_name
        }

        if(this.props.edit_race_id != undefined && this.props.edit_race_id != null){
            DND_PUT(
                '/race/' + this.props.edit_race_id,
                {data: final_race},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        } else {
            DND_POST(
                '/race',
                {data: final_race},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        }
    }

    update_text = (text) => {
        this.setState({description: text, has_changes: true})
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.setState({ [e.target.name]: new_value, has_changes: true });
    }

    update_image = (image, attribute) =>{
        this.setState({[attribute]: image, has_changes: true})
    }

    get_highest_display_order = () => {
        var order = 0
        for(var i = 0; i < this.state.racial_traits.length; i++){
            var trait = this.state.racial_traits[i]
            if(trait.display_order > order){
                order = trait.display_order
            }
        }
        return order
    }

    add_racial_trait = () => {
        var new_trait = createRacialTrait(this.get_highest_display_order() + 1)
        var racial_traits = this.state.racial_traits
        racial_traits.push(new_trait)
        this.setState({racial_traits: racial_traits, has_changes: true})
    }

    delete_racial_trait = (id) => {
        var racial_traits = []
        for(var i = 0; i < this.state.racial_traits.length; i++){
            var trait = this.state.racial_traits[i]
            if(trait.id != id){
                racial_traits.push(trait)
            }
        }
        this.setState({racial_traits: racial_traits, has_changes: true})
    }

    update_racial_trait = (id, attribute, value) => {
        if(attribute == 'spell_selection'){
            var new_value = {...value}
        } else {
            var new_value = value
        }
        var new_racial_traits = []
        for(var i = 0; i < this.state.racial_traits.length; i++){
            var trait = this.state.racial_traits[i]
            if(trait.id == id){
                trait[attribute] = new_value
            }
            new_racial_traits.push(trait)
        }
        this.setState({racial_traits: new_racial_traits, has_changes: true})
    }

    render(){

        return(
            <div className="create-race">
                {(this.props.edit_race_id == undefined || this.props.edit_race_id == null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Create Race</h2>
                    </div>
                }
                {(this.props.edit_race_id != undefined && this.props.edit_race_id != null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Edit Race</h2>
                    </div>
                }
                <Row className="race-settings">
                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Race Name</Form.Label>
                            <Form.Control value={this.state.full_name} required name="full_name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Base Race Name</Form.Label>
                            <Form.Control value={this.state.base_name} required name="base_name" type="text" placeholder="Enter Base Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>
                    
                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Is Sub Race</Form.Label>
                            <Form.Control name="is_sub_race" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.is_sub_race == true} value={true}>True</option>
                                    <option selected={this.state.is_sub_race == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicWalk">
                            <Form.Label>Walk Speed</Form.Label>
                            <Form.Control value={this.state.walk} required name="walk" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicBurrow">
                            <Form.Label>Burrow Speed</Form.Label>
                            <Form.Control value={this.state.burrow} required name="burrow" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicClimb">
                            <Form.Label>Climb Speed</Form.Label>
                            <Form.Control value={this.state.climb} required name="climb" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSwim">
                            <Form.Label>Swim Speed</Form.Label>
                            <Form.Control value={this.state.swim} required name="swim" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicFly">
                            <Form.Label>Fly Speed</Form.Label>
                            <Form.Control value={this.state.fly} required name="fly" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Size</Form.Label>
                            <Form.Control name="size" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    {Sizes.map((size) => (
                                        <option key={size} selected={this.state.size == size} value={size}>{size}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={6} md={6}>
                        <Form.Label>Description</Form.Label>
                        <MDEditor
                            value={this.state.description}
                            preview="edit"
                            onChange={this.update_text}
                        />
                    </Col>
                    <Col xs={3} md={3}>
                        <Form.Label>Portrait Image</Form.Label>
                        <UploadImage callback={(image) => this.update_image(image, "portrait")} />
                        <br/>
                        <div className="race-portrait"
                            style={{  
                                backgroundImage: "url(" + this.state.portrait + ")",
                            }}
                        />
                    </Col>
                    <Col xs={3} md={3} className="race-avatar">
                        <Form.Label>Avatar Image</Form.Label>
                        <UploadImage callback={(image) => this.update_image(image, "avatar")} />
                        <br/>
                        <img src={this.state.avatar}/>
                    </Col>
                </Row>
                <h3>
                    Racial Traits
                    <span className="clickable-div add-button" onClick={() => {this.add_racial_trait()}}>
                        <i className="fas fa-plus"></i>
                    </span>
                </h3>
                <div className="racial-trait-items">
                    {this.state.racial_traits.map((racial_trait) => (
                        <RacialTrait racial_trait={racial_trait} update_racial_trait={this.update_racial_trait} delete_racial_trait={this.delete_racial_trait}/>
                    ))}
                </div>

                <br/>
                <Button variant="primary" type="submit" onClick={this.save_race}>
                    Save Race
                </Button>

            </div>
        )

    };
}

export default CreateRace;