import React, {useState, useContext, useEffect} from 'react';
import {SocketContext} from '.././socket/Socket';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import SingleRoll from '.././rolling_history/SingleRoll'
import Markdown from 'react-markdown'

const image_extensions = [".bmp", ".jpg", ".jpeg", ".gif", ".png"]
const url_regex = new RegExp('(?:(?:https?|ftp|file):\\/\\/|www\\.|ftp\\.)(?:\\([-A-Z0-9+&@#\\/%=~_|$?!:,.]*\\)|[-A-Z0-9+&@#\\/%=~_|$?!:,.])*(?:\\([-A-Z0-9+&@#\\/%=~_|$?!:,.]*\\)|[A-Z0-9+&@#\\/%=~_|$])', 'igm');

export const ChatBox = ({name}) => {

  const socket = useContext(SocketContext);

  const [messages, setMessages] = useState([]);

  const handleSendMessage = (e) => {
    var data = {
      type: "TEXT",
      name: name,
      content: e.target[0].value
    }
    socket.emit("send_message", data);
    e.preventDefault();
    e.target.elements[0].value = "";
  };

  const getMessageContents = (text) => {
    var completed_matches = []
    var matches = [...text.matchAll(url_regex)]
    for(var i = 0; i < matches.length; i++){
      var match = matches[i][0]
        if(!completed_matches.includes(match)){
        var is_image = image_extensions.some(function (image_extension) {
          return match.endsWith(image_extension);
        });

        if(is_image == true){
          text = text.replaceAll(match, "![](" + match + ")")
        } else {
          text = text.replaceAll(match, "[" + match + "](" + match + ")")
        }
        completed_matches.push(match)
    }
  }

    return (<Markdown children={text} />);
  }

  useEffect(() => {
    socket.on("messages", data => {
        setMessages(data)
    });
  }, []);

  return (
    <div className="chat-box">
        <Form onSubmit={handleSendMessage}>
            <Form.Group name="message_area" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Send Message</Form.Label>
                <Form.Control as="textarea" rows={3} />
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
        <div className="chat-messages">
          {messages.map((message) => (
              <div>
                {message.type == "TEXT" &&
                  <div>
                    <b>{message.name}:</b>
                    <div>
                      {getMessageContents(message.content)}
                    </div>
                  </div>
                }
                {message.type == "URL" &&
                  <div>
                    <a href={message.url} target="_blank"><img src={message.url} /></a>
                  </div>
                }
                {message.type == "ROLL" &&
                  <div>
                    <SingleRoll roll={message.data} />
                  </div>
                }
              </div>
          ))}
        </div>
    </div>
  );
};