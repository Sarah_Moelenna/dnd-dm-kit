import React, {useState, useContext, useRef, useEffect} from 'react';
import {SocketContext} from '.././socket/Socket';
import SingleCombat from '../encounter/combat/SingleCombat';

export const Combat = ({set_tab_availability}) => {

  const socket = useContext(SocketContext);

  const [combat, setCombat] = useState(null);
  const combat_ref = useRef(combat);

  function set_member_location(id, data, current_combat, no_emit){
    var new_combat = { ...current_combat }
    if(current_combat == undefined){
        var new_combat = { ...combat }
    }
    for(var i = 0; i < new_combat.state.members.length; i ++){
        
        if(new_combat.state.members[i].id == id){
            new_combat.state.members[i].x = data.x
            new_combat.state.members[i].y = data.y
        }
    }
    combat_ref.current=new_combat
    setCombat(new_combat)
    if(no_emit != true){
        socket.emit("combat_move", id, data);
    }
  }

  useEffect(() => {
    socket.on("combat", data => {
        if(data == undefined || data == null || data == {}){
          set_tab_availability("Combat", false)
        } else {
          set_tab_availability("Combat", true)
        }
        combat_ref.current=data
        setCombat(data)
    });
    socket.on("combat_move", data => {
        var new_combat = combat_ref.current
        set_member_location(data.id, data.values, new_combat, true)
    });
  }, []);

  return (
    <div>
        {combat != null &&
            <SingleCombat
            player_read={true}
            combat={combat}
            refresh_function={() => {}}
            update_combat_function={() => {}}
            close_combat_fuction={() => {}}
            roll_dice_function={() => {}}
            set_member_location_function={set_member_location}
        />
        }
    </div>
  );
};