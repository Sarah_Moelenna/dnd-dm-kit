import React, {useState, useContext, useCallback, useEffect} from 'react';
import {SocketContext} from '.././socket/Socket';
import SingleMap from '.././map/SingleMap'

export const Map = ({set_tab_availability}) => {

  const socket = useContext(SocketContext);

  const [map, setMap] = useState(null);

  useEffect(() => {
    socket.on("map", data => {
        if(data == undefined || data == null || data == {}){
          set_tab_availability("Dungeon Map", false)
        } else {
          set_tab_availability("Dungeon Map", true)
        }
        setMap(data)
    });
  }, []);

  return (
    <div>
        {map != null &&
            <SingleMap key={map.id} map={map} map_data={map.data} player_read={true} />
        }
    </div>
  );
};