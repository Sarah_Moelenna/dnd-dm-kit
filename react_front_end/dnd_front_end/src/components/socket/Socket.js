import socketio from "socket.io-client";
import React from 'react';
import {get_socket_url} from '.././shared/Config'

const REACT_APP_SOCKET_URL = get_socket_url()

export const socket = socketio.connect(REACT_APP_SOCKET_URL, {path: "/socket"});
export const SocketContext = React.createContext();
