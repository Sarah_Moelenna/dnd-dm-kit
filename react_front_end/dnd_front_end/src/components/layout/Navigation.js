import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { PlayerContext } from '../shared/PlayerContext';
import { NavDropdown } from 'react-bootstrap';

const dm_pages = {
    player_tools: {
        display_name: "Player Tools",
        pages: [
            {
                path: "/character",
                name: "CHARACTER",
                display_name: "Characters",
                icon: "fas fa-user-shield",
                is_logout: false,
                user_type: null
            }
        ]
    },
    campaign_tools: {
        display_name: "Campaign Tools",
        pages: [
            {
                path: "/dm-session",
                name: "DMSESSION",
                display_name: "Session",
                icon: "fas fa-tv",
                is_logout: false,
                user_type: null
            },
            {
                path: "/campaign",
                name: "CAMPAIGN",
                display_name: "My Campaigns",
                icon: "fas fa-users",
                is_logout: false,
                user_type: null
            },
            {
                path: "/music-player",
                name: "MUSICPLAYER",
                display_name: "Music Player",
                icon: "fas fa-music",
                is_logout: false,
                user_type: null
            },
            {
                path: "/notes",
                name: "NOTES",
                display_name: "Notes",
                icon: "fas fa-book-open",
                is_logout: false,
                user_type: null
            },
            {
                path: "/encounter",
                name: "ENCOUNTER",
                display_name: "Encounters",
                icon: "fas fa-dice-d20",
                is_logout: false,
                user_type: null
            },
            {
                path: "/mapping",
                name: "MAPPING",
                display_name: "Maps",
                icon: "fas fa-compass",
                is_logout: false,
                user_type: null
            },
            {
                path: "/effect-deck",
                name: "EFFECTDECK",
                display_name: "Sound Decks",
                icon: "fas fa-tablet-alt",
                is_logout: false,
                user_type: null
            },
        ]
    },
    creation_tools: {
        display_name: "Creation Tools",
        pages: [
            {
                path: "/class",
                name: "CLASS",
                display_name: "Classes",
                icon: "fas fa-shield-alt",
                is_logout: false,
                user_type: null
            },
            {
                path: "/item",
                name: "ITEM",
                display_name: "Items",
                icon: "fas fa-wine-bottle",
                is_logout: false,
                user_type: "TYPE_ADMIN"
            },
            {
                path: "/spell",
                name: "SPELL",
                display_name: "Spells",
                icon: "fas fa-magic",
                is_logout: false,
                user_type: null
            },
            {
                path: "/spell-list",
                name: "SPELLLIST",
                display_name: "Spell Lists",
                icon: "fas fa-list",
                is_logout: false,
                user_type: null
            },
            {
                path: "/monster",
                name: "MONSTER",
                display_name: "Monsters",
                icon: "fas fa-spider",
                is_logout: false,
                user_type: null
            },
            {
                path: "/race",
                name: "RACE",
                display_name: "Races",
                icon: "fas fa-paw",
                is_logout: false,
                user_type: null
            },
            {
                path: "/battlemap",
                name: "BATTLEMAP",
                display_name: "Battle Maps",
                icon: "fas fa-fist-raised",
                is_logout: false,
                user_type: null
            },
            {
                path: "/background",
                name: "BACKGROUND",
                display_name: "Backgrounds",
                icon: "fas fa-user-tag",
                is_logout: false,
                user_type: null
            },
            {
                path: "/feat",
                name: "FEAT",
                display_name: "Feats",
                icon: "far fa-arrow-alt-circle-up",
                is_logout: false,
                user_type: null
            },
            {
                path: "/playlist",
                name: "PLAYLIST",
                display_name: "Playlists",
                icon: "fas fa-music",
                is_logout: false,
                user_type: null
            },
        ]
    },
    account: {
        display_name: "Account",
        pages: [
            {
                path: "/my-content",
                name: "MYCONTENTCOLLECTIONS",
                display_name: "My Content Collections",
                icon: "fas fa-layer-group",
                is_logout: false,
                user_type: null
            },
            {
                path: "/content-collections",
                name: "CONTENTCOLLECTIONS",
                display_name: "Content Share",
                icon: "fas fa-globe",
                is_logout: false,
                user_type: null
            },
            {
                path: "/my-images",
                name: "MYIMAGES",
                display_name: "My Images",
                icon: "far fa-images",
                is_logout: false,
                user_type: null
            },
            {
                path: "/audio",
                name: "MUSIC",
                display_name: "Audio",
                icon: "fas fa-file-audio",
                is_logout: false,
                user_type: null
            },
            {
                path: "/settings",
                name: "SETTINGS",
                display_name: "Settings",
                icon: "fas fa-cogs",
                is_logout: false,
                user_type: null
            },
            {
                path: "/logout",
                name: "LOGOUT",
                display_name: "Logout",
                icon: "fas fa-sign-out-alt",
                is_logout: true,
                user_type: null
            }
        ]
    }
}

const user_pages = [
    {
        path: "/session",
        name: "SESSION",
        display_name: "Session",
        icon: "fas fa-users",
        is_logout: false,
        user_type: null,
    },
    {
        path: "/player-notes",
        name: "PLAYER_NOTES",
        display_name: "Notes",
        icon: "fas fa-book-open",
        is_logout: false,
        user_type: null,
    },
    {
        path: "/logout",
        name: "LOGOUT",
        display_name: "Logout",
        icon: "fas fa-sign-out-alt",
        is_logout: true,
        user_type: null,
    }
]

export const get_page_from_path = (current_path) => {
    if(current_path.includes("/login")){
        return "LOGIN"
    }
    if(current_path.includes("/home")){
        return "HOME"
    }
    if(current_path.includes("/dm-session")){
        return "DMSESSION"
    }
    if(current_path.includes("/session")){
        return "SESSION"
    }
    if(current_path.includes("/campaign")){
        return "CAMPAIGN"
    }
    if(current_path.includes("/notes")){
        return "NOTES"
    }
    if(current_path.includes("/player-notes")){
        return "PLAYER_NOTES"
    }
    if(current_path.includes("/music-player")){
        return "MUSICPLAYER"
    }
    if(current_path.includes("/music")){
        return "MUSIC"
    }
    if(current_path.includes("/item")){
        return "ITEM"
    }
    if(current_path.includes("/monster")){
        return "MONSTER"
    }
    if(current_path.includes("/encounter")){
        return "ENCOUNTER"
    }
    if(current_path.includes("/mapping")){
        return "MAPPING"
    }
    if(current_path.includes("/battlemap")){
        return "BATTLEMAP"
    }
    if(current_path.includes("/settings")){
        return "SETTINGS"
    }
    if(current_path.includes("/race")){
        return "RACE"
    }

    if(current_path.includes("/spell")){
        return "SPELL"
    }
}


export class DMNavigation extends Component {

    static contextType = PlayerContext

    generate_page_link = (page) => {
        const player = this.context

        if(page.user_type != null && player.user.user_type != page.user_type){
            return null
        }

        if(page.is_logout == true){
            return(
                <div  key={"page-name-" + page.name} className="clickable-div" onClick={() => this.props.logout_function()}>
                    <span><i className="fas fa-sign-out-alt"></i></span>
                    <span><p>Logout</p></span>
                </div >
            )
        } else {
            return (
                <Link key={"page-name-" + page.name} to={page.path} className={"clickable-div" + (this.props.current_page == page.name ? " active" : "")} onClick={() => this.props.page_switch(page.name)}>
                    <span><i className={page.icon}></i></span>
                    <span><p>{page.display_name}</p></span>
                </Link >
            )
        }
    }
    
    render(){
        return (
            <div className="navigation-bg-dm">
                {Object.keys(dm_pages).map((submenu_key) => (
                    <NavDropdown title={dm_pages[submenu_key].display_name} className="submenu" key={this.props.current_page + "-" + submenu_key}>
                            {dm_pages[submenu_key].pages.map((page) => (
                                this.generate_page_link(page)
                            ))}
                    </NavDropdown>
                ))}
            </div>
        );
    }
}

export class UserNavigation extends Component {
    static contextType = PlayerContext

    generate_page_link = (page) => {
        const player = this.context

        if(page.is_logout == true){
            return(
                <div className="clickable-div" onClick={() => this.props.logout_function()}>
                    <i className="fas fa-sign-out-alt"></i>
                    <p>Logout</p>
                </div >
            )
        } else {
            return (
                <Link to={page.path} className={"clickable-div" + (this.props.current_page == page.name ? " active" : "")} onClick={() => this.props.page_switch(page.name)}>
                    <i className={page.icon}></i>
                    <p>{page.display_name}</p>
                </Link >
            )
        }
    }
    
    render(){
        return (
            <div className="navigation-bg">
                {user_pages.map((page) => (
                    this.generate_page_link(page)
                ))}
            </div>
        )
    };
}

export class Navigation extends Component {    

    static contextType = PlayerContext

    
    
    render(){
    
        const player = this.context
    
        return(
            <div className="navigation-bg">
                    {this.props.user_type == null &&
                        <Link to="/login" className={"clickable-div" + (this.props.current_page == "LOGIN" ? " active" : "")} onClick={() => this.props.page_switch("LOGIN")}>
                            <i className="fas fa-sign-in-alt"></i>
                            <p>Login</p>
                        </Link >
                    }
                    {this.props.user_type != "DM" &&
                        <Link to="/home" className={"clickable-div" + (this.props.current_page == "HOME" ? " active" : "")} onClick={() => this.props.page_switch("HOME")}>
                            <i className="fas fa-home"></i>
                            <p>Home</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/dm-session" className={"clickable-div" + (this.props.current_page == "DMSESSION" ? " active" : "")} onClick={() => this.props.page_switch("DMSESSION")}>
                            <i className="fas fa-tv"></i>
                            <p>Session</p>
                        </Link >
                    }
                    {this.props.user_type == "USER" &&
                        <Link to="/session" className={"clickable-div" + (this.props.current_page == "SESSION" ? " active" : "")} onClick={() => this.props.page_switch("SESSION")}>
                            <i className="fas fa-users"></i>
                            <p>Session</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/campaign" className={"clickable-div" + (this.props.current_page == "CAMPAIGN" ? " active" : "")} onClick={() => this.props.page_switch("CAMPAIGN")}>
                            <i className="fas fa-users"></i>
                            <p>Campaigns</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/notes" className={"clickable-div" + (this.props.current_page == "NOTES" ? " active" : "")} onClick={() => this.props.page_switch("NOTES")}>
                            <i className="fas fa-book-open"></i>
                            <p>Notes</p>
                        </Link >
                    }
                    {this.props.user_type == "USER" &&
                        <Link to="/player-notes" className={"clickable-div" + (this.props.current_page == "PLAYER_NOTES" ? " active" : "")} onClick={() => this.props.page_switch("PLAYER_NOTES")}>
                            <i className="fas fa-book-open"></i>
                            <p>Notes</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/music" className={"clickable-div" + (this.props.current_page == "MUSIC" ? " active" : "")} onClick={() => this.props.page_switch("MUSIC")}>
                            <i className="fas fa-music"></i>
                            <p>Music</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" && player.user.user_type == "TYPE_ADMIN" &&
                        <Link to="/item" className={"clickable-div" + (this.props.current_page == "ITEM" ? " active" : "")} onClick={() => this.props.page_switch("ITEM")}>
                            <i className="fas fa-wine-bottle"></i>
                            <p>Items</p>
                        </Link >
                    }{this.props.user_type == "DM" && player.user.user_type == "TYPE_ADMIN" &&
                        <Link to="/spell" className={"clickable-div" + (this.props.current_page == "SPELL" ? " active" : "")} onClick={() => this.props.page_switch("SPELL")}>
                            <i className="fas fa-magic"></i>
                            <p>Spells</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/monster" className={"clickable-div" + (this.props.current_page == "MONSTER" ? " active" : "")} onClick={() => this.props.page_switch("MONSTER")}>
                            <i className="fas fa-spider"></i>
                            <p>Monsters</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" && player.user.user_type == "TYPE_ADMIN" &&
                        <Link to="/race" className={"clickable-div" + (this.props.current_page == "RACE" ? " active" : "")} onClick={() => this.props.page_switch("RACE")}>
                            <i className="fas fa-paw"></i>
                            <p>Races</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/encounter" className={"clickable-div" + (this.props.current_page == "ENCOUNTER" ? " active" : "")} onClick={() => this.props.page_switch("ENCOUNTER")}>
                            <i className="fas fa-dice-d20"></i>
                            <p>Encounters</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/mapping" className={"clickable-div" + (this.props.current_page == "MAPPING" ? " active" : "")} onClick={() => this.props.page_switch("MAPPING")}>
                            <i className="fas fa-compass"></i>
                            <p>Maps</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/battlemap" className={"clickable-div" + (this.props.current_page == "BATTLEMAP" ? " active" : "")} onClick={() => this.props.page_switch("BATTLEMAP")}>
                            <i className="fas fa-fist-raised"></i>
                            <p>Battle Maps</p>
                        </Link >
                    }
                    {this.props.user_type == "DM" &&
                        <Link to="/settings" className={"clickable-div" + (this.props.current_page == "SETTINGS" ? " active" : "")} onClick={() => this.props.page_switch("SETTINGS")}>
                            <i className="fas fa-cogs"></i>
                            <p>Settings</p>
                        </Link >
                    }
                    {this.props.user_type != null &&
                        <div className="clickable-div" onClick={() => this.props.logout_function()}>
                            <i className="fas fa-sign-out-alt"></i>
                            <p>Logout</p>
                        </div >
                    }
                <hr></hr>
            </div>
            );
 };
}