import React, { useEffect, useContext, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import APIButton from '.././shared/APIButton';
import {SocketContext} from '.././socket/Socket';
import Form from 'react-bootstrap/Form';

export const Footer = () => {

    const socket = useContext(SocketContext);

    const [music, setMusic] = useState(null);
    const [ambience, setAmbience] = useState(null);

    const [music_volume, setMusicVolume] = useState(1);
    const [ambience_volume, setAmbienceVolume] = useState(1);

    function create_music_stop_command(){
        return {
            "command_code": "CMD_STOP_MUSIC",
            "data": {}
        }
    }

    function create_ambience_stop_command(){
        return {
            "command_code": "CMD_STOP_AMBIENCE",
            "data": {}
        }
    }

    function updateMusicVolume(e){
        setMusicVolume(e.target.value/1000)
        socket.emit("music_volume", e.target.value/1000)
    }

    function updateAmbienceVolume(e){
        setAmbienceVolume(e.target.value/1000)
        socket.emit("ambience_volume", e.target.value/1000)
    }

    useEffect(() => {
        socket.on("music_name", name => {
            setMusic(name)
        });
        socket.on("ambience_name", name => {
            setAmbience(name)
        });
        socket.on("music_volume", value => {
            setMusicVolume(value)
        });
        socket.on("ambience_volume", value => {
            setAmbienceVolume(value)
        });
      }, []);
    
    return(
        <div className="footer">
            <Row>
                <Col xs={2} md={2}>
                    <p className="copyright">© Sarah Anderson</p>
                </Col>
                <Col xs={8} md={8}>
                    <div className="audio-info">
                        { music &&
                            <span class="audio-control">
                                <div className='info'><i className="fas fa-music"></i> Playing Music: {music} <APIButton data={create_music_stop_command()} alt_style={true} api_endpoint="/commands" method="POST" icon="fas fa-pause"></APIButton></div>
                                <div className='volume'><Form.Control type="range" value={music_volume*1000} min={0} max={1000} onChange={updateMusicVolume}/></div>
                            </span>
                        }

                        { ambience &&
                            <span class="audio-control">
                                <div className='info'><i className="fas fa-music"></i> Playing Ambience: {ambience} <APIButton data={create_ambience_stop_command()} alt_style={true} api_endpoint="/commands" method="POST" icon="fas fa-pause"></APIButton></div>
                                <div className='volume'><Form.Control type="range" value={ambience_volume*1000} min={0} max={1000} onChange={updateAmbienceVolume}/></div>
                            </span>
                        }
                    </div>
                </Col>
                <Col xs={2} md={2}>

                </Col>
            </Row>
        </div>
    )
}

export default Footer;