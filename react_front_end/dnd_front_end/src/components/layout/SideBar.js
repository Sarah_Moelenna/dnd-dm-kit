import React, { Component } from 'react';
import CharacterTracker from '../sidebars/CharacterTracker';
import DiscordTracker from '../sidebars/DiscordTracker';
import RollingHistory from '../sidebars/RollingHistory';
import CampaignNotesSidebar from '../sidebars/CampaignNotesSidebar';
import RelatedNotesSidebar from '../sidebars/RelatedNotesSidebar';
import {ChatBox} from '../socket/ChatBox'
import Error from '../Error';

class Sidebar extends Component {

    state = {
        current_focus: "CHARACTER",
    }

    focus_switch = (focus) => {
        this.setState({ current_focus: focus })
    };
    
    render(){
        return(
            <div className="sidebar">
                <div className="sidebar-navigation">
                        {this.props.user_type == "DM" &&
                          <div className={"tab first-tab" + (this.state.current_focus == "CHARACTER" ? " active" : "")} onClick={() => this.focus_switch("CHARACTER")}>
                              <i className="fas fa-user-friends"></i>
                          </div>
                        }
                        {this.props.user_type == "DM" &&
                          <div className={"tab" + (this.state.current_focus == "DICE" ? " active" : "")} onClick={() => this.focus_switch("DICE")}>
                              <i className="fas fa-dice-d20"></i>
                          </div>
                        }
                        {this.props.user_type == "DM" &&
                          <div className={"tab" + (this.state.current_focus == "CAMPAIGN" ? " active" : "")} onClick={() => this.focus_switch("CAMPAIGN")}>
                              <i className="far fa-clipboard"></i>
                          </div>
                        }
                        {this.props.user_type == "DM" &&
                          <div className={"tab" + (this.state.current_focus == "NOTES" ? " active" : "")} onClick={() => this.focus_switch("NOTES")}>
                              <i className="fas fa-book"></i>
                          </div>
                        }
                        {this.props.user_type == "DM" &&
                          <div className={"tab last-tab" + (this.state.current_focus == "CHAT" ? " active" : "")} onClick={() => this.focus_switch("CHAT")}>
                              <i className="far fa-comments"></i>
                          </div>
                        }
                </div>
                <div className={"sidebar-tab" + (this.state.current_focus == "CHARACTER" ? " active" : " inactive")}>
                  <CharacterTracker refresh_characters_function={this.props.refresh_characters_function} characters={this.props.characters}/>
                </div>
                <div className={"sidebar-tab" + (this.state.current_focus == "DICE" ? " active" : " inactive")}>
                  <RollingHistory rolling_history={this.props.rolling_history} roll_dice_function={this.props.roll_dice_function}/>
                </div>
                <div className={"sidebar-tab" + (this.state.current_focus == "CAMPAIGN" ? " active" : " inactive")}>
                  <CampaignNotesSidebar refresh_campaign_function={this.props.refresh_campaign_function} current_campaign={this.props.current_campaign} update_campaign_notes_function={this.props.update_campaign_notes_function}/>
                </div>
                <div className={"sidebar-tab" + (this.state.current_focus == "NOTES" ? " active" : " inactive")}>
                  <RelatedNotesSidebar refresh_campaign_function={this.props.refresh_campaign_function} current_campaign={this.props.current_campaign} override_function={this.props.override_function}/>
                </div>
                <div className={"sidebar-tab" + (this.state.current_focus == "CHAT" ? " active" : " inactive")}>
                  <ChatBox name={this.props.name}/>
                </div>
            </div>
            );
 };
}

export default Sidebar;