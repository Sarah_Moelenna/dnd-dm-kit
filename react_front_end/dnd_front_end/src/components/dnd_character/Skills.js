import React, { Component, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Collapse } from 'reactstrap';
import Card from 'react-bootstrap/Card';
import { ProficiencyIcon } from './display_blocks/ProficiencyIcon';

const Skill = ({ stats, skill_property, skill}) => {
    let has_advantage = 0
    let has_disadvantage = 0
    stats['advantage'].forEach((advantage) => {
        if(advantage.name == skill_property){
            has_advantage = has_advantage + 1
        }
    })
    stats['disadvantage'].forEach((disadvantage) => {
        if(disadvantage.name == skill_property){
            has_disadvantage = has_disadvantage + 1
        }
    })

    return (
        <tr key={'single-skill-view ' + skill_property} className="skill-item">
            <td><ProficiencyIcon value={stats['proficiencies'][skill_property]} /></td>
            <td>{skill.mod}</td>
            <td>{skill.name}</td>
            <td>
                {has_advantage - has_disadvantage > 0 &&
                    <div className='advantage-icon'>A</div>
                }
                {has_advantage - has_disadvantage < 0 &&
                    <div className='disadvantage-icon'>D</div>
                }
                {has_advantage - has_disadvantage == 0 &&
                    <div className='advantage-placholder'></div>
                }
            </td>
            <td>{stats[skill_property] >= 0 && '+'}{stats[skill_property]}</td>
        </tr>
    )
}

const Skills = {
    "acrobatics": {name: "Acrobatics", mod: 'DEX'},
    "animal-handling": {name: "Animal Handling", mod: 'WIS'},
    "arcana": {name: "Arcana", mod: 'INT'},
    "athletics": {name: "Athletics", mod: 'STR'},
    "deception": {name: "Deception", mod: 'CHA'},
    "history": {name: "History", mod: 'INT'},
    "insight": {name: "Insight", mod: 'WIS'},
    "intimidation": {name: "Intimidation", mod: 'CHA'},
    "investigation": {name: "Investigation", mod: 'INT'},
    "medicine": {name: "Medicine", mod: 'WIS'},
    "nature": {name: "Nature", mod: 'INT'},
    "perception": {name: "Perception", mod: 'WIS'},
    "performance": {name: "Performance", mod: 'CHA'},
    "persuasion": {name: "Persuasion", mod: 'CHA'},
    "religion": {name: "Religion", mod: 'INT'},
    "sleight-of-hand": {name: "Sleight of Hand", mod: 'DEX'},
    "stealth": {name: "Stealth", mod: 'DEX'},
    "survival": {name: "Survival", mod: 'WIS'},
}

export const ListSkills = ({ stats }) => {

    return (
        <table>
            <tr className='titles'>
                <td>PROF</td>
                <td>MOD</td>
                <td>SKILL</td>
                <td></td>
                <td>BONUS</td>
            </tr>
            {Object.keys(Skills).map((skill_property) => (
                <Skill 
                    skill_property={skill_property}
                    skill={Skills[skill_property]}
                    stats={stats}
                />
            ))}
        </table>
    );
}

export const ListSkillSection = ({ stats, toggled, toggle_function}) => {

    return (
        <div className="skill-items">
            <Card>
                <Card.Title className='clickable-div' onClick={() => {toggle_function(toggled == true ? null : 'SKILLS')}}>
                    <Row>
                        <Col xs={10} md={10}>
                            <div>Skills</div>
                        </Col>
                        <Col xs={2} md={2} className='chevrons'>
                            <div><i className={toggled == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                        </Col>
                    </Row>
                </Card.Title>
                <Collapse isOpen={toggled}>
                    <Card.Body>
                        <table>
                            <tr className='titles'>
                                <td>PROF</td>
                                <td>MOD</td>
                                <td>SKILL</td>
                                <td></td>
                                <td>BONUS</td>
                            </tr>
                            {Object.keys(Skills).map((skill_property) => (
                                <Skill 
                                    skill_property={skill_property}
                                    skill={Skills[skill_property]}
                                    stats={stats}
                                />
                            ))}
                        </table>
                    </Card.Body>
                </Collapse>
            </Card>
        </div>
    );
}