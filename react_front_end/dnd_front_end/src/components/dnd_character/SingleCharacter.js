import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { DND_GET, DND_PUT, DND_POST, DND_PATCH } from '../shared/DNDRequests';
import RaceSelect from '../race/RaceSelect';
import Race from './Race';
import Form from 'react-bootstrap/Form';
import UploadImage from '../images/UploadImage';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import StandardArray from './ability_score/StandardArray';
import ClassSelect from '../class/ClassSelect';
import DndClass from './DndClass';
import { Levels } from '../monster/Data';
import BackgroundSelect from '../backgrounds/BackgroundSelect';
import Background from './Background';
import {Equipment, ListInventoryItem} from './Equipment';
import SpellDisplay from './Spell';
import { calculate_stats } from '../shared/StatCalculator';
import Button from 'react-bootstrap/Button'
import Extras from './Extras';
import AbilityScoreDisplay from './ability_score/Display';
import {ListSkills} from './Skills';
import { SavingThrow } from './display_blocks/SavingThrow';
import { Sense } from './display_blocks/Sense';
import { Proficiencies } from './display_blocks/Proficiencies';
import { Experience } from './display_blocks/Experience';
import { LevelAdvancement } from '../monster/Data';
import { Action } from './display_blocks/Action';
import { OtherStats } from './display_blocks/OtherStats';
import { Features } from './display_blocks/Features';
import {ViewSpellDisplay} from './display_blocks/Spell';
import Spinner from '../shared/Spinner'

export class ChooseCampaign extends Component {
    state = {
        campaigns: [],
        campaign_id: null
    }

    componentWillMount() {
        this.refresh_campaigns()
    };

    refresh_campaigns = () => {
        DND_GET(
          '/campaign?page=1&results_per_page=999&simple=true',
          (jsondata) => {
            this.setState({ campaigns: jsondata.results})
          },
          null
        )
    };

    handleChange = (e) => {
        var new_value = e.target.value
        this.setState({ [e.target.name]: new_value });
    }

    create_character = () => {
        if(this.state.campaign_id){
            this.props.create_function(this.state.campaign_id)
        }
    }

    render(){
        return (
            <div className="character-data edit">
                <h2>
                    <Link to="/character" onClick={this.props.close_creating_fuction}>
                        <i className="fas fa-chevron-left clickable-icon"></i>
                    </Link >
                    Create Character
                </h2>
                <Row>
                    <Col xs={6} md={6}>
                        <Form.Group controlId="formBasicLevel">
                            <Form.Label>Choose a Campaign</Form.Label>
                            <Form.Control name="campaign_id" as="select" onChange={this.handleChange} custom value={this.state.camapign_id}>
                                <option value={""}> - </option>
                                {this.state.campaigns.map((campaign) => (
                                    <option key={'campaign_id-' + campaign.id} value={campaign.id}>{campaign.name}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                        <Button variant="primary" type="submit" onClick={this.create_character} disabled={!this.state.campaign_id}>
                            Create Character
                        </Button>
                    </Col>
                </Row>
            </div>
        )
    }
}

export class SingleCharacter extends Component {

    state = {
        name: null,
        race: null,
        race_choices: {},
        avatar_src: null,

        strength: null,
        dexterity: null,
        wisdom: null,
        constitution: null,
        intelligence: null,
        charisma: null,

        class_data: {},
        subclass_data: {},
        classes: {},
        class_spell_choices: {},
        feat_choices: {},

        level: 1,

        background: null,
        background_choices: {},

        equipment_choices: {},

        selected_tab: 'RACE',
        selected_view_tab: 'ACTIONS',
        campaigns: [],
        campaign_id: null,

        inventory: [],

        copper: 0,
        silver: 0,
        electrum: 0,
        gold: 0,
        platinum: 0,

        feat_data: {},
        spell_data: {},
        current_hp: null,
        rolled_hp: 0,

        experience: 0,
        action_info: {},
        spell_slot_info: {},

        character_loaded: false,
        character_data_loaded: false,
        character_inventory_loaded: false,
        feats_loaded: false,
        spells_loaded: false,
    }

    componentWillMount() {
        this.refresh_campaigns()
        this.copy_character(this.props.edit_character_id)
        this.copy_character_data(this.props.edit_character_id)
        this.refresh_character_inventory(this.props.edit_character_id)
    };

    refresh_campaigns = () => {
        DND_GET(
          '/campaign?page=1&results_per_page=999&simple=true',
          (jsondata) => {
            this.setState({ campaigns: jsondata.results})
          },
          null
        )
    };

    refresh_feats = (feat_ids) => {
        let feat_ids_to_retrieve = []
        feat_ids.forEach((feat_id) => {
            if(!Object.keys(this.state.feat_data).includes(feat_id)){
                feat_ids_to_retrieve.push(feat_id)
            }
        })

        if(feat_ids_to_retrieve.length > 0){
            DND_POST(
                '/feat/lookup',
                {feat_ids: feat_ids_to_retrieve},
                (response) => {
                    let new_feat_data = {...this.state.feat_data}
                    Object.keys(this.state.feat_data).forEach((feat_id) => {
                        if(!feat_ids.includes(feat_id)){
                            delete new_feat_data[feat_id]
                        }
                    })
                    response.forEach((feat_item) => {
                        new_feat_data[feat_item.id] = feat_item
                    })

                    this.setState({feat_data: new_feat_data, feats_loaded: true})
                },
                null
            )
        } else {
            let new_feat_data = {...this.state.feat_data}
            let removed = false
            Object.keys(this.state.feat_data).forEach((feat_id) => {
                if(!feat_ids.includes(feat_id)){
                    delete new_feat_data[feat_id]
                    removed = true
                }
            })
            if(removed){
                this.setState({feat_data: new_feat_data})
            }
            if(this.state.feats_loaded == false){
                this.setState({feats_loaded: true})
            }
        }
    };

    refresh_spells = (spell_ids) => {
        let spell_ids_to_retrieve = []
        spell_ids.forEach((spell_id) => {
            if(!Object.keys(this.state.spell_data).includes(spell_id)){
                spell_ids_to_retrieve.push(spell_id)
            }
        })

        if(spell_ids_to_retrieve.length > 0){
            DND_POST(
                '/spell/lookup',
                {spell_ids: spell_ids_to_retrieve},
                (response) => {
                    let new_spell_data = {...this.state.spell_data}
                    Object.keys(this.state.spell_data).forEach((spell_id) => {
                        if(!spell_ids.includes(spell_id)){
                            delete new_spell_data[spell_id]
                        }
                    })
                    response.forEach((spell_item) => {
                        new_spell_data[spell_item.id] = spell_item
                    })

                    this.setState({spell_data: new_spell_data, spells_loaded: true})
                },
                null
            )
        } else {
            let new_spell_data = {...this.state.spell_data}
            let removed = false
            Object.keys(this.state.spell_data).forEach((spell_id) => {
                if(!spell_ids.includes(spell_id)){
                    delete new_spell_data[spell_id]
                    removed = true
                }
            })
            if(removed){
                this.setState({spell_data: new_spell_data})
            }
            if(this.state.spells_loaded == false){
                this.setState({spells_loaded: true})
            }
        }
    };

    save_character = () => {

        var final_character = {
            name: this.state.name,
            campaign_id: this.state.campaign_id,
            race_choices: this.state.race_choices,
            avatar_src: this.state.avatar_src,
            strength: this.state.strength,
            dexterity: this.state.dexterity,
            wisdom: this.state.wisdom,
            constitution: this.state.constitution,
            intelligence: this.state.intelligence,
            charisma: this.state.charisma,
            classes: Object.values(this.state.classes),
            background_choices: this.state.background_choices,
            equipment_choices: this.state.equipment_choices,
            class_spell_choices: this.state.class_spell_choices,
            feat_choices: this.state.feat_choices,
            level: this.state.level,
            race_id: this.state.race != null ? this.state.race.id : null,
            background_id: this.state.background != null ? this.state.background.id : null,
            copper: this.state.copper,
            silver: this.state.silver,
            electrum: this.state.electrum,
            gold: this.state.gold,
            platinum: this.state.platinum,
            current_hp: this.state.current_hp,
            rolled_hp: this.state.rolled_hp,
            experience: this.state.experience,
            action_info: this.state.action_info,
            spell_slot_info: this.state.spell_slot_info
        }
        console.log(final_character)

        DND_PUT(
            '/dnd-character/' + this.props.edit_character_id,
            {data: final_character},
            (response) => {this.setState({has_changes: false})},
            null
        )
    }

    patch_character = (property, value) => {

        var data = {}
        data[property] = value
        this.setState({[property]: value})

        DND_PATCH(
            '/dnd-character/' + this.props.edit_character_id,
            {data: data},
            (response) => {},
            null
        )
    }

    update_attribute = (name, value) => {
        if(name == 'strength'){
            this.setState({strength: value})
        }
        else if(name == 'dexterity'){
            this.setState({dexterity: value})
        }
        else if(name == 'wisdom'){
            this.setState({wisdom: value})
        }
        else if(name == 'constitution'){
            this.setState({constitution: value})
        }
        else if(name == 'intelligence'){
            this.setState({intelligence: value})
        }
        else if(name == 'charisma'){
            this.setState({charisma: value})
        }
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.setState({ [e.target.name]: new_value, has_changes: true });
    }

    edit_race_choice = (id, value) => {
        var new_race_choices = this.state.race_choices
        new_race_choices[id] = value
        this.setState({race_choices: new_race_choices})
    }

    copy_character = (character_id) => {

        DND_GET(
            '/dnd-character/' + character_id,
            (response) => {
                this.populate_with_character_data(response)
            },
            null
        )
    };

    copy_character_data = (character_id) => {

        DND_GET(
            '/dnd-character/' + character_id + '/data',
            (response) => {
                this.populate_with_data(response)
            },
            null
        )
    };

    refresh_character_inventory = (character_id) => {

        DND_GET(
            '/dnd-character/' + character_id + '/inventory',
            (response) => {
                this.setState({inventory: response.inventory, character_inventory_loaded: true})
            },
            null
        )
    };

    get_race = (race_id) => {

        DND_GET(
            '/race/' + race_id,
            (json_data) => {
                this.setState({race: json_data, race_choices: {}})
            },
            null
        )
    };

    get_background = (background_id) => {

        DND_GET(
            '/background/' + background_id,
            (json_data) => {
                this.setState({background: json_data, background_choices: {}})
            },
            null
        )
    };

    edit_background_choice = (id, value) => {
        var new_background_choices = this.state.background_choices
        new_background_choices[id] = value
        this.setState({background_choices: new_background_choices})
    }

    edit_equipment_choice = (id, value) => {
        var new_equipment_choices = this.state.equipment_choices
        new_equipment_choices[id] = value
        this.setState({equipment_choices: new_equipment_choices})
    }

    edit_feat_choice = (id, value) => {
        var new_feat_choices = this.state.feat_choices
        new_feat_choices[id] = value
        this.setState({feat_choices: new_feat_choices})
    }

    get_class = (class_id) => {

        DND_GET(
            '/class/' + class_id,
            (json_data) => {
                var class_data = this.state.class_data
                class_data[json_data.id] = json_data
                this.setState({class_data: class_data})
            },
            null
        )
    };

    get_subclass = (subclass_id) => {

        DND_GET(
            '/sub-class/' + subclass_id,
            (json_data) => {
                var subclass_data = this.state.subclass_data
                subclass_data[json_data.id] = json_data
                this.setState({subclass_data: subclass_data})
            },
            null
        )
    };

    add_class = (class_id) => {

        var classes = this.state.classes
        classes[class_id] = {
            "id": class_id,
            "class_choice": {},
            "subclass_id": null,
            "level": 1
        }
        this.get_class(class_id)
    };

    remove_class = (class_id) => {
        var classes = this.state.classes
        var class_data = this.state.class_data
        var subclass_data = this.state.subclass_data
        var class_spell_choices = this.state.class_spell_choices

        let subclass_id = classes[class_id].subclass_id
        if(subclass_id){
            delete subclass_data[subclass_id]
        }
        delete class_data[class_id]
        delete class_spell_choices[class_id]
        delete classes[class_id]

        this.setState({
            classes: classes,
            class_data: class_data,
            subclass_data: subclass_data,
            class_spell_choices: class_spell_choices
        })

    }

    update_class = (dnd_class) => {
        var classes = this.state.classes
        classes[dnd_class.id] = dnd_class
        this.setState({classes: classes})
    }

    edit_class_spell_choice = (class_id, spell_id, add) => {
        var new_class_spell_choices = this.state.class_spell_choices
        if(!Object.keys(new_class_spell_choices).includes(class_id)){
            new_class_spell_choices[class_id] = []
        }

        if(add == true){
            new_class_spell_choices[class_id].push(spell_id)
        } else {
            let index = new_class_spell_choices[class_id].indexOf(spell_id);
            new_class_spell_choices[class_id].splice(index, 1);
        }
        this.setState({class_spell_choices: new_class_spell_choices})
    }

    set_image = (image) => {
        this.setState({avatar_src: image})
    }

    populate_with_character_data = (character_data) => {
        this.setState(
            {
                name: character_data.name,
                race_choices: character_data.race_choices,
                avatar_src: character_data.avatar_src,
        
                strength: character_data.strength,
                dexterity: character_data.dexterity,
                wisdom: character_data.wisdom,
                constitution: character_data.constitution,
                intelligence: character_data.intelligence,
                charisma: character_data.charisma,
                classes: character_data.classes,
                class_spell_choices: character_data.class_spell_choices,
                level: character_data.level,
                background_choices: character_data.background_choices,
                equipment_choices: character_data.equipment_choices,
                feat_choices: character_data.feat_choices,
                campaign_id: character_data.campaign_id,
                copper: character_data.copper,
                silver: character_data.silver,
                electrum: character_data.electrum,
                gold: character_data.gold,
                platinum: character_data.platinum,
                current_hp: character_data.current_hp,
                rolled_hp: character_data.rolled_hp,
                experience: character_data.experience,
                action_info: character_data.action_info,
                spell_slot_info: character_data.spell_slot_info,
                character_loaded: true
            }
        )
    }

    populate_with_data = (character_data) => {
        this.setState(
            {
                race: character_data.race,
                class_data: character_data.class_data,
                subclass_data: character_data.subclass_data,
                background: character_data.background,
                character_data_loaded: true
            }
        )
    }

    get_remaining_levels = () => {
        let remaining_levels = this.state.level
        for(let i = 0 ; i < Object.keys(this.state.classes).length; i++){
            let dnd_class = this.state.classes[Object.keys(this.state.classes)[i]]
            remaining_levels = remaining_levels - dnd_class.level
        }
        return remaining_levels
    }

    get_equipment_sets = () => {
        let equipment_sets = []
        for(let i =0; i < Object.keys(this.state.class_data).length; i++){
            let dnd_class = this.state.class_data[Object.keys(this.state.class_data)[i]]
            equipment_sets.push(dnd_class.equipment_set)
        }
        if(this.state.background){
            equipment_sets.push(this.state.background.equipment_set)
        }
        return equipment_sets
    }

    get_base_stats = () => {
        return {
            "strength-score": this.state.strength ? parseInt(this.state.strength) : 0,
            "dexterity-score": this.state.dexterity ? parseInt(this.state.dexterity) : 0,
            "constitution-score": this.state.constitution ? parseInt(this.state.constitution) : 0,
            "wisdom-score": this.state.wisdom ? parseInt(this.state.wisdom) : 0,
            "intelligence-score": this.state.intelligence ? parseInt(this.state.intelligence) : 0,
            "charisma-score": this.state.charisma ? parseInt(this.state.charisma) : 0,
            "character-level": this.state.level,
            'current-hit-points': this.state.current_hp ? parseInt(this.state.current_hp) : null,
            'rolled-hit-points': parseInt(this.state.rolled_hp),
            'experience': this.state.experience
        }
    }

    select_subclass = (class_id, subclass_id) => {
        let classes = this.state.classes
        let old_subclass_id = classes[class_id]['subclass_id']
        classes[class_id]['subclass_id'] = subclass_id
        if(subclass_id == null){
            let subclass_data = this.state.subclass_data
            delete subclass_data[old_subclass_id]
            this.setState({classes: classes, subclass_data: subclass_data})
        } else {
            this.get_subclass(subclass_id)
            this.setState({classes: classes})
        }
    }

    get_subclass_data_for_class_id = (class_id) => {
        let subclass_id = this.state.classes[class_id].subclass_id
        if(!subclass_id){
            return null
        }

        if(Object.keys(this.state.subclass_data).includes(subclass_id)){
            return this.state.subclass_data[subclass_id]
        } else {
            return null
        }
    }

    update_level = (e) => {
        let new_level = e.target.value
        let new_experience = this.state.experience
        let level_object = LevelAdvancement[Math.min(new_level)]
        if(new_experience < level_object.experience){
            new_experience = level_object.experience
        }
        if(new_level != 20){
            let next_level_object = LevelAdvancement[Math.min(new_level + 1, 20)]
            if(new_experience > next_level_object.experience){
                new_experience = level_object.experience
            }
        }
        this.setState({level: new_level, experience: new_experience, has_changes: true})
    }

    update_experience = (e) => {
        let new_experience = e.target.value
        let new_level = 1
        Object.keys(LevelAdvancement).forEach((level_key) => {
            let level_object = LevelAdvancement[level_key]
            if(new_experience >= level_object.experience){
                new_level = level_key
            }
        })
        this.setState({level: new_level, experience: new_experience, has_changes: true})
    }


    render(){
        if(this.state.character_data_loaded == false || this.state.character_loaded == false || this.state.character_inventory_loaded == false){
            return (
                <div className="character-data edit">
                    <Spinner/>
                </div>
            )
        }

        let remaining_levels = this.get_remaining_levels()
        let stats = calculate_stats(
            this.get_base_stats(),
            this.state.class_data,
            this.state.subclass_data,
            this.state.classes,
            this.state.race,
            this.state.race_choices,
            this.state.background,
            this.state.background_choices,
            this.state.feat_data,
            this.state.feat_choices,
            this.state.inventory,
            this.state.class_spell_choices,
            this.state.spell_data
        )
        this.refresh_feats(stats.feat_ids)
        this.refresh_spells(stats.spell_ids)

        if(this.state.feats_loaded == false || this.state.spells_loaded == false){
            return (
                <div className="character-data edit">
                    <Spinner/>
                </div>
            )
        }

        if(this.props.view_mode == false){
            return(
                <div className="character-data edit">
                        {(this.props.edit_character_id == undefined || this.props.edit_character_id == null) &&
                            <h2>
                                <Link to="/character" onClick={this.props.close_creating_fuction}>
                                    <i className="fas fa-chevron-left clickable-icon"></i>
                                </Link >
                                Create Character
                            </h2>
                        }
                        {(this.props.edit_character_id != undefined && this.props.edit_character_id != null) &&
                            <h2>
                                <Link to="/character" onClick={this.props.close_creating_fuction}>
                                    <i className="fas fa-chevron-left clickable-icon"></i>
                                </Link >
                                Editing {this.state.name} <i class="fas fa-scroll clickable-icon" onClick={() => {this.save_character(); this.props.change_view_mode()}}></i>
                            </h2>
                        }

                        <h2>Details</h2>

                        <Row>
                            <Col xs={6} md={6}>
                                <Row>
                                    <Col xs={4} md={4}>
                                        <Form.Group>
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control value={this.state.name ? this.state.name : ''} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicLevel">
                                            <Form.Label>Level</Form.Label>
                                            <Form.Control name="level" as="select" onChange={this.update_level} custom value={this.state.level}>
                                                    {Levels.map((level) => (
                                                        <option key={'character-level-' + level} value={level}>{level}</option>
                                                    ))}
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>

                                    <Col xs={4} md={4}>
                                        <Form.Group controlId="formBasicLevel">
                                            <Form.Label>Experience</Form.Label>
                                            <Form.Control value={this.state.experience ? this.state.experience : 0} required name="experience" type="number" onChange={this.update_experience} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={12} md={12}>
                                        <StandardArray 
                                            update_attribute={this.update_attribute}
                                            strength={this.state.strength}
                                            constitution={this.state.constitution}
                                            dexterity={this.state.dexterity}
                                            intelligence={this.state.intelligence}
                                            wisdom={this.state.wisdom}
                                            charisma={this.state.charisma}
                                            display_strength={stats['strength-score']}
                                            display_constitution={stats['constitution-score']}
                                            display_dexterity={stats['dexterity-score']}
                                            display_intelligence={stats['intelligence-score']}
                                            display_wisdom={stats['wisdom-score']}
                                            display_charisma={stats['charisma-score']}
                                            modifier_strength={stats['strength-modifier']}
                                            modifier_constitution={stats['constitution-modifier']}
                                            modifier_dexterity={stats['dexterity-modifier']}
                                            modifier_intelligence={stats['intelligence-modifier']}
                                            modifier_wisdom={stats['wisdom-modifier']}
                                            modifier_charisma={stats['charisma-modifier']}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={6} md={6}>
                                <Row>
                                    <Col xs={3} md={3}></Col>
                                    <Col xs={6} md={6}>
                                        <Card className='hit-points'>
                                            <Card.Title>Hit Points</Card.Title>
                                                <Card.Body>
                                                <table>
                                                    <tr>
                                                        <td className='faded'>CURRENT</td>
                                                        <td></td>
                                                        <td className='faded'>MAX</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <Form.Control value={stats['current-hit-points']} required name="current_hp" min={0} max={stats['hit-points']} type="number" onChange={this.handleChange} />
                                                        </td>
                                                        <td className='faded'>/</td>
                                                        <td>{stats['hit-points']}</td>
                                                    </tr>
                                                </table>
                                                <p>Hit Dice:
                                                    {Object.keys(stats['hit-die']).map((hit_die) => (
                                                        <span>{stats['hit-die'][hit_die]}d{hit_die}</span>
                                                    ))}
                                                </p>
                                                <Form.Group className='rolled-hp'>
                                                    <Form.Label>Rolled HP</Form.Label>
                                                    <Form.Control value={this.state.rolled_hp ? this.state.rolled_hp : 0} required name="rolled_hp" min={0} type="number" onChange={this.handleChange} />
                                                </Form.Group>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col xs={3} md={3}>
                                        <div className='character-avatar'>
                                            <p>Avatar Image</p>
                                            <UploadImage callback={this.set_image}/>
                                            { this.state.avatar_src &&
                                                <Card>
                                                    <Card.Img variant="top" src={this.state.avatar_src} />
                                                </Card>
                                            }
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>

                        <div className='tabs'>
                            <span onClick={() => {this.setState({selected_tab: 'RACE'})}} className={this.state.selected_tab == 'RACE' ? 'selected' : ''}>Race</span>
                            <span onClick={() => {this.setState({selected_tab: 'CLASS'})}} className={this.state.selected_tab == 'CLASS' ? 'selected' : ''}>Class</span>
                            <span onClick={() => {this.setState({selected_tab: 'SPELL'})}} className={this.state.selected_tab == 'SPELL' ? 'selected' : ''}>Spells</span>
                            <span onClick={() => {this.setState({selected_tab: 'BACKGROUND'})}} className={this.state.selected_tab == 'BACKGROUND' ? 'selected' : ''}>Background</span>
                            <span onClick={() => {this.setState({selected_tab: 'EQUIPMENT'})}} className={this.state.selected_tab == 'EQUIPMENT' ? 'selected' : ''}>Equipment</span>
                            <span onClick={() => {this.setState({selected_tab: 'EXTRAS'})}} className={this.state.selected_tab == 'EXTRAS' ? 'selected' : ''}>Extras</span>
                        </div>

                        <br/>
                        {this.state.selected_tab == 'RACE' &&
                            <div>
                                <RaceSelect
                                    select_function={this.get_race}
                                />
                                {this.state.race != null &&
                                    <Race
                                        race={this.state.race}
                                        choices={this.state.race_choices}
                                        save_choice_function={this.edit_race_choice}
                                        character_level={this.state.level}
                                    />
                                }
                            </div>
                        }

                        {this.state.selected_tab == 'CLASS' &&
                            <div>
                                {remaining_levels > 0 &&
                                    <ClassSelect
                                        select_function={this.add_class}
                                        exclude={Object.keys(this.state.classes)}
                                    />
                                }
                                {Object.keys(this.state.classes).map((class_id) => (
                                    <DndClass
                                        key={class_id}
                                        character_class={this.state.classes[class_id]}
                                        class_data={this.state.class_data[class_id]}
                                        max_level={parseInt(remaining_levels) + parseInt(this.state.classes[class_id].level)}
                                        update_character={this.update_class}
                                        select_subclass_function={this.select_subclass}
                                        subclass_data = {this.get_subclass_data_for_class_id(class_id)}
                                        remove_function={this.remove_class}
                                    />
                                ))}
                            </div>
                        }

                        {this.state.selected_tab == 'SPELL' &&
                            <div>
                                <SpellDisplay
                                    stats={stats}
                                    class_data={this.state.class_data}
                                    classes={this.state.classes}
                                    class_spell_choices={this.state.class_spell_choices}
                                    edit_class_spell_choice={this.edit_class_spell_choice}
                                />
                            </div>
                        }
                        
                        {this.state.selected_tab == 'BACKGROUND' &&
                            <div>
                                <BackgroundSelect
                                    disabled_ids={[this.state.background]}
                                    select_function={this.get_background}
                                />
                                { this.state.background &&
                                    <div className='background-page'>
                                        <Background
                                            background={this.state.background}
                                            choices={this.state.background_choices}
                                            save_choice_function={this.edit_background_choice}
                                        />
                                    </div>
                                }
                            </div>
                        }

                        {this.state.selected_tab == 'EQUIPMENT' &&
                            <div>
                                <Equipment
                                    equipment_sets={this.get_equipment_sets()}
                                    choices={this.state.equipment_choices}
                                    save_choice_function={this.edit_equipment_choice}
                                    inventory={this.state.inventory}
                                    character_id={this.props.edit_character_id}
                                    update_inventory_function={(new_inventory) => {this.setState({inventory: new_inventory})}}
                                    gold={this.state.gold}
                                    update_gold={(new_gold) => {this.setState({gold: new_gold, has_changes: true})}}
                                />
                            </div>
                        }

                        {this.state.selected_tab == 'EXTRAS' &&
                            <div>
                                <Extras
                                    feats={Object.values(this.state.feat_data)}
                                    feat_choices={this.state.feat_choices}
                                    save_feat_choice_function={this.edit_feat_choice}
                                    character_level={this.state.level}
                                    stats={stats}
                                />
                            </div>
                        }

                        <hr/>
                        <Button variant="primary" type="submit" onClick={this.save_character}>
                            Save Character
                        </Button>

                    
                </div>
            )
        } else {
            return (
                <div className="character-data edit view-mode">
                    <h2>
                        <Link to="/character" onClick={this.props.close_creating_fuction}>
                            <i className="fas fa-chevron-left clickable-icon"></i>
                        </Link >
                        {this.state.name}  <i class="fas fa-cog clickable-icon" onClick={this.props.change_view_mode}></i>
                    </h2>

                    <Row>
                        <Col xs={6} md={6}>
                            <Row>
                                <Col xs={12} md={12}>
                                    <Experience stats={stats} />
                                </Col>

                                <Col xs={12} md={12}>
                                    <AbilityScoreDisplay 
                                        display_strength={stats['strength-score']}
                                        display_constitution={stats['constitution-score']}
                                        display_dexterity={stats['dexterity-score']}
                                        display_intelligence={stats['intelligence-score']}
                                        display_wisdom={stats['wisdom-score']}
                                        display_charisma={stats['charisma-score']}
                                        modifier_strength={stats['strength-modifier']}
                                        modifier_constitution={stats['constitution-modifier']}
                                        modifier_dexterity={stats['dexterity-modifier']}
                                        modifier_intelligence={stats['intelligence-modifier']}
                                        modifier_wisdom={stats['wisdom-modifier']}
                                        modifier_charisma={stats['charisma-modifier']}
                                    />
                                </Col>
                                <Col xs={6} md={6} className='character-sheet-section'>
                                    <Card>
                                        <Card.Title>Saving Throws</Card.Title>
                                        <Card.Body>
                                            <SavingThrow stats={stats} />
                                        </Card.Body>
                                    </Card>
                                    <Card>
                                        <Card.Title>Senses</Card.Title>
                                        <Card.Body>
                                            <Sense stats={stats} />
                                        </Card.Body>
                                    </Card>
                                    <Card>
                                        <Card.Title>Proficiencies</Card.Title>
                                        <Card.Body>
                                            <Proficiencies stats={stats} />
                                        </Card.Body>
                                    </Card>
                                </Col>
                                <Col xs={6} md={6} className='skill-items character-sheet-section'>
                                    <Card>
                                        <Card.Title>Skills</Card.Title>
                                        <Card.Body>
                                            <ListSkills stats={stats} />
                                        </Card.Body>
                                    </Card>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={6} md={6}>
                            <Row>
                                <Col xs={5} md={5}>
                                    <OtherStats stats={stats} />
                                </Col>
                                <Col xs={4} md={4}>
                                    <Card className='hit-points'>
                                        <Card.Title>Hit Points</Card.Title>
                                            <Card.Body>
                                            <table>
                                                <tr>
                                                    <td className='faded'>CURRENT</td>
                                                    <td></td>
                                                    <td className='faded'>MAX</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    <Form.Control value={stats['current-hit-points']} required name="current_hp" min={0} max={stats['hit-points']} type="number" onChange={this.handleChange} />
                                                    </td>
                                                    <td className='faded'>/</td>
                                                    <td>{stats['hit-points']}</td>
                                                </tr>
                                            </table>
                                        </Card.Body>
                                    </Card>
                                </Col>
                                <Col xs={3} md={3}>
                                    <div className='character-avatar'>
                                        { this.state.avatar_src &&
                                            <Card>
                                                <Card.Img variant="top" src={this.state.avatar_src} />
                                            </Card>
                                        }
                                    </div>
                                </Col>
                                <Col xs={12} md={12}>
                                    <Card>
                                        <Card.Body>
                                            <div className='view-tabs'>
                                                <span onClick={() => {this.setState({selected_view_tab: 'ACTIONS'})}} className={this.state.selected_view_tab == 'ACTIONS' ? 'selected' : ''}>ACTIONS</span>
                                                <span onClick={() => {this.setState({selected_view_tab: 'SPELLS'})}} className={this.state.selected_view_tab == 'SPELLS' ? 'selected' : ''}>SPELLS</span>
                                                <span onClick={() => {this.setState({selected_view_tab: 'INVENTORY'})}} className={this.state.selected_view_tab == 'INVENTORY' ? 'selected' : ''}>INVENTORY</span>
                                                <span onClick={() => {this.setState({selected_view_tab: 'FEATURES'})}} className={this.state.selected_view_tab == 'FEATURES' ? 'selected' : ''}>FEATURES</span>
                                                <span onClick={() => {this.setState({selected_view_tab: 'NOTES'})}} className={this.state.selected_view_tab == 'NOTES' ? 'selected' : ''}>NOTES</span>
                                                <span onClick={() => {this.setState({selected_view_tab: 'EXTRAS'})}} className={this.state.selected_view_tab == 'EXTRAS' ? 'selected' : ''}>EXTRAS</span>
                                            </div>
                                            <br/>
                                            {this.state.selected_view_tab == 'ACTIONS' &&
                                                <div className='actions'>
                                                    <Action
                                                        stats={stats}
                                                        patch_character={this.patch_character}
                                                        action_info={this.state.action_info}
                                                    />
                                                </div>
                                            }
                                            {this.state.selected_view_tab == 'FEATURES' &&
                                                <div className='features'>
                                                    <Features
                                                        stats={stats}
                                                        feat_data={this.state.feat_data}
                                                    />
                                                </div>
                                            }{this.state.selected_view_tab == 'SPELLS' &&
                                            <div className='spells'>
                                                <ViewSpellDisplay
                                                    stats={stats}
                                                    patch_character={this.patch_character}
                                                    spell_slot_info={this.state.spell_slot_info}
                                                    spell_data={this.state.spell_data}
                                                />
                                            </div>
                                        }
                                            {this.state.selected_view_tab == 'INVENTORY' &&
                                                <div className='equipment item-page'>
                                                    <ListInventoryItem
                                                        character_id={this.props.edit_character_id}
                                                        inventory={this.state.inventory}
                                                        update_inventory_function={(new_inventory) => {this.setState({inventory: new_inventory})}}
                                                        use_small_fill_box={true}
                                                    />
                                                </div>
                                            }
                                        </Card.Body>
                                    </Card>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            )
        }

    };
}