import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Markdown from 'react-markdown';
import ModifierGroup from './ModifierGroup';
import Card from 'react-bootstrap/Card';
import { Collapse } from 'reactstrap';
import OptionSetList from './OptionSet';

class RacialTrait extends Component {


    render(){

        return (
            <div key={'racial-trait' + this.props.racial_trait.id} className="racial-trait-item">
                <Card>
                    <Card.Title className='clickable-div' onClick={() => {this.props.toggle_function(this.props.racial_trait.id)}}>
                        <Row>
                            <Col xs={10} md={10} className="class-feature-name">
                                <div>{this.props.racial_trait.name}</div>
                            </Col>
                            <Col xs={2} md={2} className='chevrons'>
                                <div><i className={this.props.toggled == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            </Col>
                        </Row>
                    </Card.Title>
                    <Collapse isOpen={this.props.toggled}>
                        <Card.Body>
                            <Markdown children={this.props.racial_trait.description}/>
                            <ModifierGroup
                                object_id={this.props.racial_trait.id}
                                modifiers={this.props.racial_trait.modifiers}
                                choices={this.props.choices}
                                save_choice_function={this.props.save_choice_function}
                            />
                            { this.props.racial_trait.option_sets &&
                                <OptionSetList
                                    option_sets={this.props.racial_trait.option_sets}
                                    level={this.props.character_level}
                                    choices={this.props.choices}
                                    object_id={this.props.racial_trait.id}
                                    save_choice_function={this.props.save_choice_function}
                                />
                            }
                        </Card.Body>
                    </Collapse>
                </Card>
            </div>
        )
    }
}

class Race extends Component {

    state = {
        toggled_racial_trait: null,
    }

    toggle_racial_trait = (trait_id) => {
        if(this.state.toggled_racial_trait == trait_id){
            this.setState({toggled_racial_trait: null})
        } else {
            this.setState({toggled_racial_trait: trait_id})
        }
    }

    render(){

        return(
            <div className="single-race">
                <div>
                    <h2>{this.props.race.full_name}</h2>
                </div>

                <Row>
                    <Col xs={this.props.race.large_avatar_url ? 8 : 12} md={this.props.race.large_avatar_url ? 8 : 12}>
                        <Markdown children={this.props.race.description}/>

                        <div className="racial-trait-items">
                            {this.props.race.racial_traits.map((racial_trait) => (
                                <RacialTrait
                                    racial_trait={racial_trait}
                                    choices={this.props.choices}
                                    save_choice_function={this.props.save_choice_function}
                                    toggle_function={this.toggle_racial_trait}
                                    toggled={racial_trait.id == this.state.toggled_racial_trait}
                                    character_level={this.props.character_level}
                                />
                            ))}
                        </div>
                    </Col>

                    {this.props.race.large_avatar_url &&
                        <Col xs={4} md={4} className="avatar">
                            <img src={this.props.race.large_avatar_url}/>
                        </Col>
                    }
                </Row>
                
                
            </div>
        )

    };
}

export default Race;