import { Component } from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

class AbilityScoreDisplay extends Component {


    render () {
        return (
            <Row className="ability-score-display">
                <Col xs={2} md={2}>
                    <Card className='ability-score-box'>
                        <span>STRENGTH</span>
                        <span>{this.props.modifier_strength >= 0 && '+'}{this.props.modifier_strength}</span>
                        <span>{this.props.display_strength}</span>
                    </Card>
                </Col>

                <Col xs={2} md={2}>
                    <Card className='ability-score-box'>
                        <span>DEXTERITY</span>
                        <span>{this.props.modifier_dexterity >= 0 && '+'}{this.props.modifier_dexterity }</span>
                        <span>{this.props.display_dexterity}</span>
                    </Card>
                </Col>

                <Col xs={2} md={2}>
                    <Card className='ability-score-box'>
                        <span>CONSTITUTION</span>
                        <span>{this.props.modifier_constitution >= 0 && '+'}{this.props.modifier_constitution}</span>
                        <span>{this.props.display_constitution}</span>
                    </Card>
                </Col>

                <Col xs={2} md={2}>
                    <Card className='ability-score-box'>
                        <span>INTELLIGENCE</span>
                        <span>{this.props.modifier_intelligence >= 0 && '+'}{this.props.modifier_intelligence}</span>
                        <span>{this.props.display_intelligence}</span>
                    </Card>
                </Col>

                <Col xs={2} md={2}>
                    <Card className='ability-score-box'>
                        <span>WISDOM</span>
                        <span>{this.props.modifier_wisdom >= 0 && '+'}{this.props.modifier_wisdom}</span>
                        <span>{this.props.display_wisdom}</span>
                    </Card>
                </Col>

                <Col xs={2} md={2}>
                    <Card className='ability-score-box'>
                        <span>CHARISMA</span>
                        <span>{this.props.modifier_charisma >= 0 && '+'}{this.props.modifier_charisma}</span>
                        <span>{this.props.display_charisma}</span>
                    </Card>
                </Col>
            </Row>
        )
    }
}

export default AbilityScoreDisplay;