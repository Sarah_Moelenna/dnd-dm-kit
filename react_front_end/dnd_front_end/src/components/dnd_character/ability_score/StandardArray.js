import { Component } from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

class StandardArray extends Component {

    handleChange = (e) => {
        this.props.update_attribute(e.target.name, e.target.value)
    }

    get_options = (value) => {
        var new_options = [8, 10, 12, 13, 14 ,15]
        new_options = new_options.filter(item => item != this.props.strength)
        new_options = new_options.filter(item => item != this.props.dexterity)
        new_options = new_options.filter(item => item != this.props.constitution)
        new_options = new_options.filter(item => item != this.props.wisdom)
        new_options = new_options.filter(item => item != this.props.intelligence)
        new_options = new_options.filter(item => item != this.props.charisma)
        if(value != undefined && value != null){
            new_options.push(value)
        }
        new_options.sort((a,b) => {
            if(a > b) return 1;
            if(a < b) return -1;
            return 0;
        });
        return new_options
    }

    render () {
        return (
            <Row>
                <Col xs={4} md={4}>
                    <Form.Group>
                        <Form.Label>Strength</Form.Label>
                        <Form.Control name="strength" as="select" onChange={this.handleChange} value={this.props.strength ? this.props.strength : ''} custom>
                        <option value={null}>--</option>
                        {this.get_options(this.props.strength).map((option) => (
                            <option key={"strength-" + option} value={option}>{option}</option>
                        ))}
                        </Form.Control>
                        <Form.Label>Total: {this.props.display_strength} ({this.props.modifier_strength > 0 && '+'}{this.props.modifier_strength})</Form.Label>
                    </Form.Group>
                </Col>

                <Col xs={4} md={4}>
                    <Form.Group>
                        <Form.Label>Dexterity</Form.Label>
                        <Form.Control name="dexterity" as="select" onChange={this.handleChange} value={this.props.dexterity ? this.props.dexterity : ''} custom>
                        <option value={null}>--</option>
                        {this.get_options(this.props.dexterity).map((option) => (
                            <option key={"dexterity-" + option} value={option}>{option}</option>
                        ))}
                        </Form.Control>
                    </Form.Group>
                        <Form.Label>Total: {this.props.display_dexterity} ({this.props.modifier_dexterity > 0 && '+'}{this.props.modifier_dexterity })</Form.Label>
                </Col>

                <Col xs={4} md={4}>
                    <Form.Group>
                        <Form.Label>Constitution</Form.Label>
                        <Form.Control name="constitution" as="select" onChange={this.handleChange} value={this.props.constitution ? this.props.constitution : ''} custom>
                        <option value={null}>--</option>
                        {this.get_options(this.props.constitution).map((option) => (
                            <option key={"constitution-" + option} value={option}>{option}</option>
                        ))}
                        </Form.Control>
                    </Form.Group>
                        <Form.Label>Total: {this.props.display_constitution} ({this.props.modifier_constitution > 0 && '+'}{this.props.modifier_constitution})</Form.Label>
                </Col>

                <Col xs={4} md={4}>
                    <Form.Group>
                        <Form.Label>Intelligence</Form.Label>
                        <Form.Control name="intelligence" as="select" onChange={this.handleChange} value={this.props.intelligence ? this.props.intelligence : ''} custom>
                        <option value={null}>--</option>
                        {this.get_options(this.props.intelligence).map((option) => (
                            <option key={"intelligence-" + option} value={option}>{option}</option>
                        ))}
                        </Form.Control>
                        <Form.Label>Total: {this.props.display_intelligence} ({this.props.modifier_intelligence > 0 && '+'}{this.props.modifier_intelligence})</Form.Label>
                    </Form.Group>
                </Col>

                <Col xs={4} md={4}>
                    <Form.Group>
                        <Form.Label>Wisdom</Form.Label>
                        <Form.Control name="wisdom" as="select" onChange={this.handleChange} value={this.props.wisdom ? this.props.wisdom : ''} custom>
                        <option value={null}>--</option>
                        {this.get_options(this.props.wisdom).map((option) => (
                            <option key={"wisdom-" + option} value={option}>{option}</option>
                        ))}
                        </Form.Control>
                        <Form.Label>Total: {this.props.display_wisdom} ({this.props.modifier_wisdom > 0 && '+'}{this.props.modifier_wisdom})</Form.Label>
                    </Form.Group>
                </Col>

                <Col xs={4} md={4}>
                    <Form.Group>
                        <Form.Label>Charisma</Form.Label>
                        <Form.Control name="charisma" as="select" onChange={this.handleChange} value={this.props.charisma ? this.props.charisma : ''} custom>
                        <option value={null}>--</option>
                        {this.get_options(this.props.charisma).map((option) => (
                            <option key={"charisma-" + option} value={option}>{option}</option>
                        ))}
                        </Form.Control>
                        <Form.Label>Total: {this.props.display_charisma} ({this.props.modifier_charisma > 0 && '+'}{this.props.modifier_charisma})</Form.Label>
                    </Form.Group>
                </Col>
            </Row>
        )
    }
}

export default StandardArray;