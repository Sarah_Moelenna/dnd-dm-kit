import React, { Component } from 'react';
import Markdown from 'react-markdown';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import { DND_GET } from '../shared/DNDRequests';
import SingleSpell from '../spell/SingleSpell';
import { Collapse } from 'reactstrap';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { SpellLevels } from '../monster/Data';
import { EvaluateFormula } from '../shared/FormulaInput';
import Button from 'react-bootstrap/Button';

class SpellDisplay extends Component {

    render(){
        return (
            <div>
                {Object.keys(this.props.classes).map((class_id) => (
                    <ClassSpellSelection
                        class_data={this.props.class_data[class_id]}
                        character_class_data={this.props.classes[class_id]}
                        class_spell_choices={this.props.class_spell_choices[class_id] ? this.props.class_spell_choices[class_id] : []}
                        edit_class_spell_choice={this.props.edit_class_spell_choice}
                        stats={this.props.stats}
                    />
                ))}
            </div>
        )
    }
}

class ClassSpellSelection extends Component {

    state = {
        spells: {},
        spell_toggle: null,
        toggle: false
    }

    componentWillMount() {
        this.get_spell_lists()
    };

    get_allowance = () => {
        let spell_data = this.props.class_data.spell_data[this.props.character_class_data.level]
        let max_spell_level = 0
        for(let i = 1; i < 10; i++){
            if(spell_data[i] > 0){
                max_spell_level = i
            }
        }

        if(this.props.class_data.has_static_spell_count == false){
            spell_data.spells = Math.max(
                EvaluateFormula(
                    this.props.stats,
                    {
                        'class-level': this.props.character_class_data.level
                    },
                    this.props.class_data.custom_spell_count
                ),
                1
            )
        }

        return {
            "cantrips": spell_data.cantrip,
            "spells": spell_data.spells,
            "max_spell_level": max_spell_level
        }
    }

    get_current_spells = (spell_list) => {
        let cantrips = 0
        let spells = 0
        for(let i = 0; i < spell_list.length; i++){
            let spell = spell_list[i]
            if(this.props.class_spell_choices.includes(spell.id)){
                if(spell.level == 0){
                    cantrips = cantrips + 1
                } else {
                    spells = spells + 1
                }
            }
        }
        return {
            "cantrips": cantrips,
            "spells": spells
        }
    }

    get_spell_lists = () => {

        if(!this.props.class_data){
            return null;
        }

        DND_GET(
            '/spell-list/class/' + this.props.class_data.id,
            (json_data) => {
                let spells = {}
                let spell_ids = []
                for(let i = 0; i < json_data.results.length; i++){
                    let spell_list = json_data.results[i]
                    for(let j = 0; j < spell_list.spells.length; j++){
                        let spell = spell_list.spells[j]
                        if(!spell_ids.includes(spell.id)){
                            spell_ids.push(spell.id)
                            if(!Object.keys(spells).includes(spell.level.toString())){
                                spells[spell.level] = []
                            }
                            spells[spell.level].push(spell)
                        }
                    }
                }
                for(let i = 0; i < Object.keys(spells).length; i++){
                    let spell_list = spells[Object.keys(spells)[i]]
                    spells[Object.keys(spells)[i]] = spell_list.sort(function(a, b) {
                        const nameA = a.name.toUpperCase()
                        const nameB = b.name.toUpperCase()
                        if (nameA < nameB) {
                          return -1
                        }
                        if (nameA > nameB) {
                          return 1
                        }
                        return 0
                      });
                }
                this.setState({spells: spells})
            },
            null
        )
    };

    get_spells = (max_spell_level) => {
        let spells = []
        for(let i = 0; i < Object.keys(this.state.spells).length; i++){
            let spell_list = this.state.spells[Object.keys(this.state.spells)[i]]
            for(let j = 0; j < spell_list.length; j++){
                let spell = spell_list[j]
                if(spell.level <= max_spell_level){
                    spells.push(spell)
                }
            }
        }
        return spells
    }

    toggle_spell = (spell_id) => {
        if(this.state.spell_toggle == spell_id){
            this.setState({spell_toggle: null})
        } else {
            this.setState({spell_toggle: spell_id})
        }
    }

    get_spell_level = (value) => {
        var names = Object.keys(SpellLevels)
        for(var i = 0; i < names.length; i++){
            if(SpellLevels[names[i]] == value){
                return names[i]
            }
        }
    }

    is_spell_learnt = (spell_id) => {
        return this.props.class_spell_choices.includes(spell_id)
    }

    edit_class_spell_choice = (e, class_id, spell_id, add) => {
        e.stopPropagation();
        this.props.edit_class_spell_choice(class_id, spell_id, add)
    }

    can_learn_spell = (spell, current_spells, spell_allowance) => {
        if(spell.level == 0){
            return current_spells.cantrips < spell_allowance.cantrips
        } else {
            return current_spells.spells < spell_allowance.spells
        }
    }

    render(){
        if(!this.props.class_data){
            return null;
        }

        let spell_allowance = this.get_allowance()
        let spells = this.get_spells(spell_allowance.max_spell_level)
        let current_spells = this.get_current_spells(spells)

        if(spells.length == 0){
            return null;
        }

        let learn_text = this.props.class_data.are_spells_prepared ? 'Prepare' : 'Learn'
        let unlearn_text = this.props.class_data.are_spells_prepared ? 'Unprepare' : 'Remove Spell'
        let extra_count_text = this.props.class_data.are_spells_prepared ? 'Prepared' : 'Learnt'

        return (
            <Card className='spell-page' key={this.props.class_data.id}>
                <Card.Title onClick={() => {this.setState({toggle: !this.state.toggle})}} className='clickable-div'>
                    <Row>
                        <Col xs={2} md={2}>
                            {this.props.class_data.name} Spells
                        </Col>
                        <Col xs={2} md={2}>
                            {current_spells.cantrips}/{spell_allowance.cantrips} Cantrips
                        </Col>
                        <Col xs={2} md={2}>
                            {current_spells.spells}/{spell_allowance.spells} Spells {extra_count_text}
                        </Col>
                        <Col xs={4} md={4}></Col>
                        <Col xs={2} md={2} className='chevrons'>
                            <div><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div>
                        </Col>
                    </Row>
                </Card.Title>
                <Collapse isOpen={this.state.toggle}>
                    <Card.Body>
                        {spells.map((spell) => (
                            <Card key={this.props.class_data.id + '-' + spell.id}>
                                <Card.Title onClick={() => {this.toggle_spell(spell.id)}} className='clickable-div'>
                                <Row>
                                    <Col xs={8} md={8}>
                                        {spell.name}<br/>
                                        {this.get_spell_level(spell.level)} - {spell.school}
                                    </Col>
                                    <Col xs={2} md={2}>
                                        { this.is_spell_learnt(spell.id) == true && spell.level != 0 &&
                                            <Button variant="secondary" onClick={(e) => {this.edit_class_spell_choice(e, this.props.class_data.id, spell.id, false)}}>{unlearn_text}</Button>
                                        }
                                        { this.is_spell_learnt(spell.id) == true && spell.level == 0 &&
                                            <Button variant="secondary" onClick={(e) => {this.edit_class_spell_choice(e, this.props.class_data.id, spell.id, false)}}>Remove Spell</Button>
                                        }
                                        { this.is_spell_learnt(spell.id) == false && this.can_learn_spell(spell, current_spells, spell_allowance) == true &&
                                            <Button variant="primary" onClick={(e) => {this.edit_class_spell_choice(e, this.props.class_data.id, spell.id, true)}}>{learn_text}</Button>
                                        }
                                        { this.is_spell_learnt(spell.id) == false && this.can_learn_spell(spell, current_spells, spell_allowance) == false &&
                                            <Button variant="primary" disabled='disabled'>Learn</Button>
                                        }
                                    </Col>
                                    <Col xs={2} md={2} className='chevrons'>
                                        <div><i className={this.state.spell_toggle == spell.id == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                                    </Col>
                                </Row>
                                </Card.Title>
                                <Collapse isOpen={this.state.spell_toggle == spell.id}>
                                    <Card.Body>
                                        <SingleSpell spell={spell} mini_view={true}/>
                                    </Card.Body>
                                </Collapse>
                            </Card>
                        ))}

                    </Card.Body>
                </Collapse>
            </Card>
        )
    }
}

export default SpellDisplay