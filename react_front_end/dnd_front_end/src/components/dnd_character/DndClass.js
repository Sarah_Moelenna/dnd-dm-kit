import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Markdown from 'react-markdown';
import { LevelNames } from '../monster/Data';
import { Collapse } from 'reactstrap';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import ModifierGroup from './ModifierGroup';
import OptionSetList from './OptionSet';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

class DndClassFeature extends Component {
    render(){
        return (
            <div key={this.props.class_feature.id} className="class-feature-item">
                <Card>
                    <Card.Title className='clickable-div' onClick={() => {this.props.toggle_function(this.props.class_feature.id)}}>
                        <Row>
                            <Col xs={10} md={10} className="class-feature-name">
                                <div>{this.props.class_feature.name}</div>
                                { this.props.class_feature.level &&
                                    <div className='subtitle'>{LevelNames[this.props.class_feature.level]} Level</div>
                                }
                            </Col>
                            <Col xs={2} md={2} className='chevrons'>
                                <div><i className={this.props.toggled == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            </Col>
                        </Row>
                    </Card.Title>
                    <Collapse isOpen={this.props.toggled}>
                        <Card.Body>
                            <Markdown children={this.props.class_feature.description}/>
                            {this.props.show_options == true &&
                                <ModifierGroup
                                    object_id={this.props.class_feature.id}
                                    modifiers={this.props.class_feature.modifiers}
                                    choices={this.props.character_class.class_choice}
                                    save_choice_function={this.props.save_choice_function}
                                />
                            }
                            { this.props.show_options == true && this.props.class_feature.option_sets &&
                                <OptionSetList
                                    option_sets={this.props.class_feature.option_sets}
                                    level={this.props.character_class.level}
                                    choices={this.props.character_class.class_choice}
                                    object_id={this.props.class_feature.id}
                                    save_choice_function={this.props.save_choice_function}
                                />
                            }
                        </Card.Body>
                    </Collapse>
                </Card>
            </div>
        )
    }
}

class SubclassSelection extends Component {

    handleChange = (e) => {
        if (e.target.value){
            this.props.select_subclass_function(this.props.class_data.id, e.target.value)
        } else {
            this.props.select_subclass_function(this.props.class_data.id, null)
        }
    }

    render(){
        return (
            <div key={this.props.class_data.name + '-subclass-select'} className="class-feature-item">
                <Card>
                    <Card.Title className='clickable-div' onClick={() => {this.props.toggle_function('subclass-select')}}>
                        <Row>
                            <Col xs={10} md={10} className="class-feature-name">
                                <div>{this.props.class_data.subclass_title}</div>
                                <div className='subtitle'>{LevelNames[this.props.class_data.subclass_level]} Level</div>
                            </Col>
                            <Col xs={2} md={2} className='chevrons'>
                                <div><i className={this.props.toggled == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            </Col>
                        </Row>
                    </Card.Title>
                    <Collapse isOpen={this.props.toggled}>
                        <Card.Body>
                            <p>{this.props.class_data.subclass_description}</p>
                            {this.props.show_options == true &&
                                <Form.Group>
                                    <Form.Control name={this.props.class_data.name + '-subclass-select'} as="select" onChange={this.handleChange} custom value={this.props.character_class.subclass_id}>
                                        <option value={''}>-</option>
                                        {this.props.class_data.subclasses.map((subclass) => (
                                            <option
                                                key={this.props.class_data.name + '-' + subclass.id}
                                                value={subclass.id} 
                                            >{subclass.name}</option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            }
                        </Card.Body>
                    </Collapse>
                </Card>
            </div>
        )
    }
}

class DndClass extends Component {

    state = {
        toggle: false,
        toggled_class_feature: null,
        toggle_other_features: false
    }

    get_class_features = () => {
        let class_features = []
        let higher_class_features = []
        for(var i = 0; i < this.props.class_data.class_features.length; i++){
            let class_feature = this.props.class_data.class_features[i]
            if(class_feature.level <= this.props.character_class.level){
                class_features.push(class_feature)
            } else {
                higher_class_features.push(class_feature)
            }
        }

        // add subclass select
        if(this.props.class_data.subclass_level){
            let subclass_fake_feature = {
                'display_subclass_choice': true,
                'level': this.props.class_data.subclass_level,
                'name': '0'
            }
            if(this.props.class_data.subclass_level <= this.props.character_class.level){
                class_features.push(subclass_fake_feature)
            } else {
                higher_class_features.push(subclass_fake_feature)
            }
        }

        //add subclass features
        if(this.props.subclass_data){
            for(var i = 0; i < this.props.subclass_data.class_features.length; i++){
                let class_feature = this.props.subclass_data.class_features[i]
                if(class_feature.level <= this.props.character_class.level){
                    class_features.push(class_feature)
                } else {
                    higher_class_features.push(class_feature)
                }
            }
        }

        class_features = class_features.sort(function(a, b) {
            if (a.level < b.level) {
              return -1
            }
            if (a.level > b.level) {
              return 1
            }
            return 0
        });

        higher_class_features = higher_class_features.sort(function(a, b) {
            if (a.level < b.level) {
              return -1
            }
            if (a.level > b.level) {
              return 1
            }
            return 0
        });

        return [class_features, higher_class_features]
    }

    toggle_class_feature = (feature_id) => {
        if(this.state.toggled_class_feature == feature_id){
            this.setState({toggled_class_feature: null})
        } else {
            this.setState({toggled_class_feature: feature_id})
        }
    }

    update_attribute = (attribute, value, as_int) => {
        let new_character_class = this.props.character_class
        if(as_int == true){
            new_character_class[attribute] = parseInt(value)
        } else {
            new_character_class[attribute] = value
        }
        this.props.update_character(new_character_class)
    }

    get_allowed_levels = () => {
        let levels = []
        for(var i = 1; i <= this.props.max_level; i++){
            levels.push(i)
        }
        return levels
    }

    update_choice = (choice_id, value) => {
        let new_character_class = this.props.character_class
        new_character_class.class_choice[choice_id] = value
        this.props.update_character(new_character_class)
    }

    render(){
        if(this.props.class_data == undefined || this.props.class_data == null){
            return null
        }

        let result = this.get_class_features()
        let class_features = result[0]
        let higher_class_features = result[1]
        let levels = this.get_allowed_levels()

        return(
            <div className="character-class-data edit" key={'class-block-' + this.props.class_data.id}>
                <Card><Card.Body>
                    <h3 className="card-text">{this.props.class_data.name}</h3>
                    <Row>
                        <Col xs={2} md={2}>
                            <div className="dnd-class-image"
                                style={{  
                                    backgroundImage: "url(" + this.props.class_data.portrait_url + ")",
                                }}
                            />
                        </Col>
                        <Col xs={6} md={6}>
                            <Markdown children={this.props.class_data.description}/>
                        </Col>
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicLevel">
                                <Form.Label>Level</Form.Label>
                                <Form.Control name="level" as="select" onChange={(e) => {this.update_attribute('level', e.target.value, true)}} value={this.props.character_class.level} custom>
                                        {levels.map((level) => (
                                            <option key={'class-level-' + level} value={level}>{level}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                        <Col xs={2} md={2} className='chevrons'>
                            <div className="clickable-div" onClick={() => {this.setState({toggle: !this.state.toggle})}}><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                        </Col>
                    </Row>
                    <Collapse isOpen={this.state.toggle}>
                        {this.state.toggle == true &&
                            <Row>
                                <Col xs={12} md={12}>
                                    <div className="class-feature-items">
                                        {class_features.map(class_feature => {
                                            if(class_feature.display_subclass_choice == true){
                                                return (<SubclassSelection
                                                    class_data={this.props.class_data}
                                                    toggled={this.state.toggled_class_feature == 'subclass-select'}
                                                    toggle_function={this.toggle_class_feature}
                                                    show_options={true}
                                                    select_subclass_function={this.props.select_subclass_function}
                                                    character_class={this.props.character_class}
                                                />)
                                            } else {
                                                return (<DndClassFeature
                                                    class_feature={class_feature}
                                                    character_class={this.props.character_class}
                                                    save_choice_function={this.update_choice}
                                                    toggled={this.state.toggled_class_feature == class_feature.id}
                                                    toggle_function={this.toggle_class_feature}
                                                    show_options={true}
                                                />)
                                            }
                                        })}
                                    </div>
                                </Col>
                            </Row>
                        }
                        <br/>
                        <p><b>At Higher Levels </b><span className="clickable-div" onClick={() => {this.setState({toggle_other_features: !this.state.toggle_other_features})}}><i className={this.state.toggle_other_features == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></span></p>
                        <Collapse isOpen={this.state.toggle_other_features}>
                            {this.state.toggle_other_features == true &&
                                <Row>
                                    <Col xs={12} md={12}>
                                        <div className="class-feature-items">
                                            {higher_class_features.map(class_feature => {
                                                if(class_feature.display_subclass_choice == true){
                                                    return (<SubclassSelection
                                                        class_data={this.props.class_data}
                                                        toggled={this.state.toggled_class_feature == 'subclass-select'}
                                                        toggle_function={this.toggle_class_feature}
                                                        show_options={false}
                                                        select_subclass_function={this.props.select_subclass_function}
                                                        character_class={this.props.character_class}
                                                    />)
                                                } else {
                                                    return (<DndClassFeature
                                                            class_feature={class_feature}
                                                            character_class={this.props.character_class}
                                                            save_choice_function={this.update_choice}
                                                            toggled={this.state.toggled_class_feature == class_feature.id}
                                                            toggle_function={this.toggle_class_feature}
                                                            show_options={false}
                                                        />)
                                                }
                                            })}
                                        </div>
                                    </Col>
                                </Row>
                            }
                        </Collapse>
                        <br/>
                        <DeleteConfirmationButton id={this.props.class_data.id} override_button="Remove Class" name="Class" delete_function={this.props.remove_function} />
                    </Collapse>
                    
                </Card.Body></Card>
            </div>
        )

    };
}

export default DndClass;