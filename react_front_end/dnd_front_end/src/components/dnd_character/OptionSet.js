import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import ModifierGroup from './ModifierGroup';
import Card from 'react-bootstrap/Card';
import Markdown from 'react-markdown';

class OptionSetList extends Component {

    get_option_sets = () => {
        let option_sets = []
        for(let i = 0; i < this.props.option_sets.length; i++){
            let option_set = this.props.option_sets[i]
            if(option_set.level == undefined || option_set.level == null){
                option_sets.push(option_set)
            } else if(option_set.level <= this.props.level){
                option_sets.push(option_set)
            } 
        }
        return option_sets
    }

    render(){
        let option_sets = this.get_option_sets()

        return (
            <div>
                {option_sets.map((option_set) => (
                    <div key={option_set.id}>
                        <OptionSet
                            option_set={option_set}
                            object_id={this.props.object_id}
                            choices={this.props.choices}
                            save_choice_function={this.props.save_choice_function}
                        />
                    </div>
                ))}
            </div>
        )
    }
}

class OptionSet extends Component {

    handleChange = (e) => {
        this.props.save_choice_function(e.target.name, e.target.value)
    }

    get_option_choice_id = () => {
        return this.props.object_id + this.props.option_set.id
    }

    get_selected_option = () => {
        let selected_option = null
        let option_choice_id = this.get_option_choice_id()
        let choice_value = this.props.choices[option_choice_id]
        for(let i = 0; i <  this.props.option_set.options.length; i++){
            let option = this.props.option_set.options[i]
            if(option.id == choice_value){
                selected_option=option
            }
        }
        return selected_option
    }

    render(){
        let option_choice_id = this.get_option_choice_id()
        let selected_option = this.get_selected_option()

        return (
            <div>
                {this.props.option_set.name &&
                    <p>{this.props.option_set.name}</p>
                }
                <Form.Group>
                    <Form.Control name={option_choice_id} as="select" onChange={this.handleChange} custom>
                        <option value={null}>-</option>
                        {this.props.option_set.options.map((option) => (
                            <option
                                key={option_choice_id + '-' +option.id}
                                value={option.id} 
                                selected={this.props.choices[option_choice_id] == option.id}
                            >{option.name}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
                { selected_option &&
                    <Card className='selected-option'><Card.Body>
                        { selected_option.description &&
                            <Markdown children={selected_option.description}/>
                        }
                        <ModifierGroup
                            object_id={this.props.object_id}
                            modifiers={selected_option.modifiers}
                            choices={this.props.choices}
                            save_choice_function={this.props.save_choice_function}
                        />
                    </Card.Body></Card>
                }
            </div>
        )
    }
}

export default OptionSetList