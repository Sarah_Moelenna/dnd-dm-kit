import React, { Component, useState } from 'react';
import ModifierGroup from './ModifierGroup';
import OptionSetList from './OptionSet';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Collapse } from 'reactstrap';
import Card from 'react-bootstrap/Card';
import Markdown from 'react-markdown';

class Feat extends Component {
    render(){
        return (
            <div key={'FeatSelection' + this.props.feat.id} className="class-feature-item">
                <Card>
                    <Card.Title className='clickable-div' onClick={() => {this.props.toggle_function(this.props.toggled == true ? null : this.props.feat.id)}}>
                        <Row>
                            <Col xs={10} md={10} className="class-feature-name">
                                <div>{this.props.feat.name}</div>
                            </Col>
                            <Col xs={2} md={2} className='chevrons'>
                                <div><i className={this.props.toggled == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            </Col>
                        </Row>
                    </Card.Title>
                    <Collapse isOpen={this.props.toggled}>
                        <Card.Body>
                            <Markdown children={this.props.feat.description}/>
                            <ModifierGroup
                                object_id={this.props.feat.id}
                                modifiers={this.props.feat.modifiers}
                                choices={this.props.choices}
                                save_choice_function={this.props.save_choice_function}
                            />
                            <OptionSetList
                                option_sets={this.props.feat.option_sets}
                                level={this.props.level}
                                choices={this.props.choices}
                                object_id={this.props.feat.id}
                                save_choice_function={this.props.save_choice_function}
                            />
                        </Card.Body>
                    </Collapse>
                </Card>
            </div>
        )
    }
}

const ListFeats = ({ feats, choices, save_choice_function, level, toggled, toggle_function}) => {

    const [isOpen, setIsOpen] = useState(null);

    const setOpen = (value) => setIsOpen(value);


    if(!feats || feats.length == 0){
        return (
            <div className="feat-items">
                <Card>
                    <Card.Title className='clickable-div' onClick={() => {toggle_function(toggled == true ? null : 'FEATS')}}>
                        <Row>
                            <Col xs={10} md={10}>
                                <div>Feats</div>
                            </Col>
                            <Col xs={2} md={2} className='chevrons'>
                                <div><i className={toggled == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            </Col>
                        </Row>
                    </Card.Title>
                    <Collapse isOpen={toggled}>
                        <Card.Body>
                            <p>No feats found.</p>
                        </Card.Body>
                    </Collapse>
                </Card>
            </div>
        )
    }

    return (
        <div className="feat-items">
            <Card>
                <Card.Title className='clickable-div' onClick={() => {toggle_function(toggled == true ? null : 'FEATS')}}>
                    <Row>
                        <Col xs={10} md={10}>
                            <div>Feats</div>
                        </Col>
                        <Col xs={2} md={2} className='chevrons'>
                            <div><i className={toggled == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                        </Col>
                    </Row>
                </Card.Title>
                <Collapse isOpen={toggled}>
                    <Card.Body>
                        {feats.map((feat) => (
                            <Feat 
                                feat={feat}
                                choices={choices}
                                save_choice_function={save_choice_function}
                                level={level}
                                toggled={isOpen==feat.id}
                                toggle_function={setOpen}
                            />
                        ))}
                    </Card.Body>
                </Collapse>
            </Card>
        </div>
    );
}

export default ListFeats;