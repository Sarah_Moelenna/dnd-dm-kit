import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Button } from 'reactstrap';

const ListCharacters = ({ characters, refresh_function, edit_character_function, view_character_function}) => {
    if(characters === undefined) {
        return
    }
    return (
        <div className="character-items">
            <hr></hr>
            <Row>
                <Col xs={1} md={1}>

                </Col>
                <Col xs={2} md={2}>
                    <p className="card-text"><b>Name</b></p>
                </Col>
                <Col xs={2} md={2}>
                    <p className="card-text"><b>Race</b></p>
                </Col>
                <Col xs={2} md={2}>
                    <p className="card-text"><b>Classes</b></p>
                </Col>
            </Row>
            {characters.map((character) => (
                <div key={character.id} className="character-item">
                    <Row>
                        <Col xs={1} md={1}>
                            <img src={character.avatar_src}/>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{character.name}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{character.race}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{character.classes}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <Button className="btn btn-primary" onClick={() => {view_character_function(character.id)}}>View Character</Button>
                            <Button className="btn btn-primary" onClick={() => {edit_character_function(character.id)}}>Edit Character</Button>
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListCharacters;