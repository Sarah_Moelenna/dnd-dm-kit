import Markdown from 'react-markdown';

export const Features = ({ stats, feat_data}) => {

    
    return (
        <div className='features-display'>
            <p className="sub-section">Class Features</p>
            {stats['features']['class'].map((feature) => (
                <Feature feature={feature}/>
            ))}
            <p className="sub-section">Racial Traits</p>
            {stats['features']['race'].map((feature) => (
                <Feature feature={feature}/>
            ))}
            <p className="sub-section">Feats</p>
            {Object.values(feat_data).map((feat) => (
                <Feat feat={feat}/>
            ))}
        </div>
    );
}

const Feature = ({ feature }) => {
    return (
        <div key={feature.id} className='single-feature'>
            <p className="title">{feature.name}</p>
            <Markdown children={feature.description}/>
        </div>
    )
}
const Feat = ({ feat }) => {
    return (
        <div key={feat.id} className='single-feature'>
            <p className="title">{feat.name}</p>
            <Markdown children={feat.description}/>
        </div>
    )
}