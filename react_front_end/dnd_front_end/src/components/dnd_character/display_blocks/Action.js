import FillBox from "../../shared/FillBox";
import { DamageTypes } from "../../monster/Data";

export const Action = ({ stats, patch_character, action_info }) => {

    let sorted_actions = {}
    //add actions
    stats['actions'].forEach((action) => {
        if(action.activation && action.activation.activationType){
            let key = action.activation.activationType + '-' + action.action_type
            if(!Object.keys(sorted_actions).includes(key)){
                sorted_actions[key] = []
            }
            sorted_actions[key].push(action)
        }
    })
    //turn resources into actions
    stats['resources'].forEach((resource) => {
        let key = resource.activation_type + '-' + resource.action_type
        if(!Object.keys(sorted_actions).includes(key)){
            sorted_actions[key] = []
        }
        sorted_actions[key].push(resource)
    })

    return (
        <div className='action-display'>
            <p className="sub-section">Actions</p>
            <table className="action-table">
                <tr>
                    <td>
                        <span>Attack</span>
                    </td>
                    <td>
                        <span>Range</span>
                    </td>
                    <td>
                        <span>Hit/DC</span>
                    </td>
                    <td>
                        <span>Damage</span>
                    </td>
                    <td>
                        <span>Notes</span>
                    </td>
                </tr>
                {sorted_actions['1-1'] && sorted_actions['1-1'].map((action) => (
                    <SingleAttackAction stats={stats} action={action}/>
                ))}
            </table>
            {sorted_actions['1-4'] && sorted_actions['1-4'].map((action) => (
                <SingleDefaultAction action={action}/>
            ))}
            {sorted_actions['1-3'] && sorted_actions['1-3'].map((action) => (
                <SingleGeneralAction stats={stats} action={action} action_info={action_info} patch_character={patch_character}/>
            ))}
            <p className="sub-section">Bonus Actions</p>
            {sorted_actions['3-4'] && sorted_actions['3-4'].map((action) => (
                <SingleDefaultAction action={action}/>
            ))}
            {sorted_actions['3-3'] && sorted_actions['3-3'].map((action) => (
                <SingleGeneralAction stats={stats} action={action} action_info={action_info} patch_character={patch_character}/>
            ))}
            <p className="sub-section">Reactions</p>
            {sorted_actions['4-4'] && sorted_actions['4-4'].map((action) => (
                <SingleDefaultAction action={action}/>
            ))}
            {sorted_actions['4-3'] && sorted_actions['4-3'].map((action) => (
                <SingleGeneralAction stats={stats} action={action} action_info={action_info} patch_character={patch_character}/>
            ))}
            <p className="sub-section">Other</p>
            {sorted_actions['8-4'] && sorted_actions['8-4'].map((action) => (
                <SingleDefaultAction action={action}/>
            ))}
            {sorted_actions['8-3'] && sorted_actions['8-3'].map((action) => (
                <SingleGeneralAction stats={stats} action={action} action_info={action_info} patch_character={patch_character}/>
            ))}
            {sorted_actions['7-3'] && sorted_actions['7-3'].map((action) => (
                <SingleGeneralAction stats={stats} action={action} action_info={action_info} patch_character={patch_character}/>
            ))}
            {sorted_actions['2-3'] && sorted_actions['2-3'].map((action) => (
                <SingleGeneralAction stats={stats} action={action} action_info={action_info} patch_character={patch_character}/>
            ))}
            {sorted_actions['6-3'] && sorted_actions['6-3'].map((action) => (
                <SingleGeneralAction stats={stats} action={action} action_info={action_info} patch_character={patch_character}/>
            ))}
        </div>
    );
}

const SingleDefaultAction = ({ action }) => {
    return (
        <div key={"action-display-"+action.id} className='single-action'>
            <div>
                <p className="title">{action.name}</p>
                <blockquote>{action.snippet}</blockquote>
            </div>
        </div>
    );
}

const SingleGeneralAction = ({ stats, action, patch_character, action_info }) => {
    return (
        <div key={"action-display-"+action.id} className='single-action'>
            <div>
                <p className="title">{action.name}</p>
                <p className="snippet">{action.snippet}</p>
                <LimitedUse stats={stats} limited_use={action.limited_use} action_id={action.id} action_info={action_info} patch_character={patch_character}/>
            </div>
        </div>
    );
}

const SingleAttackAction = ({ stats, action }) => {
    let to_hit = 0
    let extra_damage = 0
    let damage_display = ''
    if(action.notes && action.notes.includes('Finesse')){
        //finesse - pick highest between str and dex
        if(stats['strength-modifier'] > stats['dexterity-modifier']){
            to_hit = stats['strength-modifier']
            extra_damage = stats['strength-modifier']
        } else {
            to_hit = stats['dexterity-modifier']
            extra_damage = stats['dexterity-modifier']
        }
    } else {
        if(action.ability_modifier_stat_id == 1){
            to_hit = stats['strength-modifier']
            extra_damage = stats['strength-modifier']
        } else if(action.ability_modifier_stat_id == 2){
            to_hit = stats['dexterity-modifier']
            extra_damage = stats['dexterity-modifier']
        } else if(action.ability_modifier_stat_id == 3){
            to_hit = stats['constitution-modifier']
            extra_damage = stats['constitution-modifier']
        } else if(action.ability_modifier_stat_id == 4){
            to_hit = stats['intelligence-modifier']
            extra_damage = stats['intelligence-modifier']
        } else if(action.ability_modifier_stat_id == 5){
            to_hit = stats['wisdom-modifier']
            extra_damage = stats['wisdom-modifier']
        } else if(action.ability_modifier_stat_id == 6){
            to_hit = stats['charisma-modifier']
            extra_damage = stats['charisma-modifier']
        }
    }
    if(action.is_proficient == true){
        to_hit = to_hit + stats['proficiency-bonus']
    }
    if(action.bonus_to_damage){
        extra_damage = extra_damage + action.bonus_to_damage
    }
    if(action.bonus_to_hit){
        to_hit = to_hit + action.bonus_to_hit
    }
    if(action.dice && action.dice.diceValue){
        damage_display = action.dice.diceCount + 'd' + action.dice.diceValue
        if(extra_damage > 0){
            damage_display = damage_display + '+' + extra_damage
        } else {
            damage_display = damage_display + extra_damage
        }
    }
    if(action.dice && action.dice.fixedValue){
        damage_display = Math.max(parseInt(action.dice.fixedValue) + extra_damage, 0)
    }

    return (
        <tr key={"action-display-"+action.id} className='single-attack-action'>
            <td>
                <span>{action.name}</span>
            </td>
            <td>
                {action.range && !action.range.longRange &&
                    <span>{action.range.range}ft</span>
                }
                {action.range && action.range.longRange &&
                    <span>{action.range.range}<span className="long-range"> ({action.range.longRange})</span></span>
                }
            </td>
            <td>
                <span>{to_hit >= 0 && '+'}{to_hit}</span>
            </td>
            <td>
                <span>{damage_display}
                <span className="damage-type"> ({DamageTypes[action.damage_type_id]})</span>
                </span>
            </td>
            <td className="notes-container">
                <span className="notes">{action.notes}</span>
            </td>
        </tr>
    );
}

const patch_use = (add, patch_character, action_info, max_charges, action_id) => {
    let new_action_info = {...action_info}
    if(!Object.keys(new_action_info).includes(action_id)){
        new_action_info[action_id] = {uses: 0}
    }

    if(add == true){
        new_action_info[action_id].uses = Math.min(new_action_info[action_id].uses + 1, max_charges)
    } else {
        new_action_info[action_id].uses = Math.max(new_action_info[action_id].uses - 1, 0)
    }
    patch_character('action_info', new_action_info)
}

const LimitedUse = ({ stats, action_id, limited_use, patch_character, action_info }) => {
    if(!limited_use || !limited_use.resetType){
        return null
    }

    let current_uses = action_info[action_id] ? action_info[action_id].uses : 0
    let max_charges = limited_use['maxUses']
    if(limited_use.statModifierUsesId == 1){
        max_charges = Math.max(1, stats['strength-modifier'])
    } else if(limited_use.statModifierUsesId == 2){
        max_charges = Math.max(1, stats['dexterity-modifier'])
    } else if(limited_use.statModifierUsesId == 3){
        max_charges = Math.max(1, stats['constitution-modifier'])
    } else if(limited_use.statModifierUsesId == 4){
        max_charges = Math.max(1, stats['intelligence-modifier'])
    } else if(limited_use.statModifierUsesId == 5){
        max_charges = Math.max(1, stats['wisdom-modifier'])
    } else if(limited_use.statModifierUsesId == 6){
        max_charges = Math.max(1, stats['charisma-modifier'])
    }

    

    return (
        <div className='limited-use'>
            {[...Array(max_charges).keys()].map((index) => (
                <div key={action_id + '-' + index} className="fill-box-container">
                    <FillBox value={index < current_uses} on_click={() => {patch_use(!(index < current_uses), patch_character, action_info, max_charges, action_id)}} use_small_fill_box={true} />
                </div>
            ))}
        </div>
    );
}