import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export const SavingThrow = ({ stats }) => {

    let check_for = [
        "charisma-saving-throws",
        "constitution-saving-throws",
        "dexterity-saving-throws",
        "saving-throws",
        "strength-saving-throws",
        "wisdom-saving-throws"
    ]

    let advantages = []
    let disadvantages = []

    stats['advantage'].forEach((advantage) => {
        if(check_for.includes(advantage.name)){
            advantages.push(advantage)
        }
    })
    stats['disadvantage'].forEach((disadvantage) => {
        if(check_for.includes(disadvantage.name)){
            disadvantages.push(disadvantage)
        }
    })

    return (
        <Row className='saving-throw-display'>
            <Col xs={6} md={6}>
                <div className='single-saving-throw'>
                    <span>STR</span>
                    <span>{stats["strength-saving-throws"] >= 0 && '+'}{stats["strength-saving-throws"]}</span>
                </div>
            </Col>
            <Col xs={6} md={6}>
                <div className='single-saving-throw'>
                    <span>INT</span>
                    <span>{stats["intelligence-saving-throws"] >= 0 && '+'}{stats["intelligence-saving-throws"]}</span>
                </div>
            </Col>
            <Col xs={6} md={6}>
                <div className='single-saving-throw'>
                    <span>DEX</span>
                    <span>{stats["dexterity-saving-throws"] >= 0 && '+'}{stats["dexterity-saving-throws"]}</span>
                </div>
            </Col>
            <Col xs={6} md={6}>
                <div className='single-saving-throw'>
                    <span>WIS</span>
                    <span>{stats["wisdom-saving-throws"] >= 0 && '+'}{stats["wisdom-saving-throws"]}</span>
            </div>
            </Col>
            <Col xs={6} md={6}>
                <div className='single-saving-throw'>
                    <span>CON</span>
                    <span>{stats["constitution-saving-throws"] >= 0 && '+'}{stats["constitution-saving-throws"]}</span>
                </div>
            </Col>
            <Col xs={6} md={6}>
                <div className='single-saving-throw'>
                    <span>CHA</span>
                    <span>{stats["charisma-saving-throws"] >= 0 && '+'}{stats["charisma-saving-throws"]}</span>
                </div>
            </Col>
            {advantages.map((advantage) => (
                <Col xs={12} md={12} className='advantage-container'>
                    <Row>
                        <Col xs={1} md={1}>
                            <div className='advantage-icon'>A</div>
                        </Col>
                        <Col xs={10} md={10}>
                            {advantage.restriction ?
                                <span>{advantage.restriction}</span>
                                : 
                                <span>{advantage.display}</span>
                            }
                        </Col>
                    </Row>
                </Col>
            ))}
            {disadvantages.map((advantage) => (
                <Col xs={12} md={12} className='advantage-container'>
                    <Row>
                        <Col xs={1} md={1}>
                            <div className='disadvantage-icon'>A</div>
                        </Col>
                        <Col xs={10} md={10}>
                            {advantage.restriction ?
                                <span>{advantage.restriction}</span>
                                : 
                                <span>{advantage.display}</span>
                            }
                        </Col>
                    </Row>
                </Col>
            ))}
        </Row>
    );
}