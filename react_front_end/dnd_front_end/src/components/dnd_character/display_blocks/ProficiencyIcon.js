export const ProficiencyIcon = ({value}) => {
    if(parseInt(value) == 2){
        return (
            <div className='prof-icon'><i class="fas fa-plus-circle"></i></div>
        )
    } else if(parseInt(value) == 1){
        return (
            <div className='prof-icon'><i class="fas fa-circle"></i></div>
        )
    } else if(parseInt(value) == 0.5){
        return (
            <div className='prof-icon'><i class="fas fa-adjust"></i></div>
        )
    } else {
        return (
            <div className='prof-icon'><i class="far fa-circle"></i></div>
        )
    }
}