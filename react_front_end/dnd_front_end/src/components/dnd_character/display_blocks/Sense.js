export const Sense = ({ stats }) => {

    return (
        <div className='sense-display'>
            <div>
                <div className='single-passive-sense'>
                    <span>{stats['passive-perception']}</span>
                    <span>PASSIVE PERCEPTION</span>
                </div>
            </div>
                <div>
                <div className='single-passive-sense'>
                    <span>{stats['passive-investigation']}</span>
                    <span>PASSIVE INVESTIGATION</span>
                </div>
            </div>
                <div>
                <div className='single-passive-sense'>
                    <span>{stats['passive-insight']}</span>
                    <span>PASSIVE INSIGHT</span>
                </div>
            </div>
            {Object.values(stats['senses']).map((sense) => (
                <p key={'sense-display-'+sense.display}>{sense.display} {sense.value}ft</p>
            ))}
        </div>
    );
}