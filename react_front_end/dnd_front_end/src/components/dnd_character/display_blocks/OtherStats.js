
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

export const OtherStats = ({ stats }) => {

    return (
        <Row className="other-stats-display">
            <Col xs={6} md={6}>
                <Card className='other-stats-box'>
                    <span className='faded'>PROFICIENCY</span>
                    <span>{stats['proficiency-bonus'] >= 0 && '+'}{stats['proficiency-bonus']}</span>
                    <span className='faded'>BONUS</span>
                </Card>
            </Col>
            <Col xs={6} md={6}>
                <Card className='other-stats-box'>
                    <span className='faded'>WALKING</span>
                    <span>{(stats['innate-speed-walking'] + stats['speed-walking'])}ft</span>
                    <span className='faded'>SPEED</span>
                </Card>
            </Col>
            <Col xs={6} md={6}>
                <Card className='other-stats-box'>
                    <span className='faded'>INITIATIVE</span>
                    <span>{stats['initiative'] >= 0 && '+'}{stats['initiative']}</span>
                    <span className='faded'>&nbsp;</span>
                </Card>
            </Col>
            <Col xs={6} md={6}>
                <Card className='other-stats-box'>
                    <span className='faded'>ARMOUR</span>
                    <span>{stats['armor-class']}</span>
                    <span className='faded'>CLASS</span>
                </Card>
            </Col>
        </Row>
    );
}