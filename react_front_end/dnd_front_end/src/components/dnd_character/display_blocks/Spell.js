import FillBox from "../../shared/FillBox";
import { AttributeID, SpellRangeTypes, SpellAoETypes, SpellActivationTypes } from "../../monster/Data";
import React, { useState } from "react";
import Markdown from 'react-markdown';
import { Collapse } from 'reactstrap';

const patch_use = (add, patch_character, spell_slot_info, max_uses, slot_id) => {
    let new_spell_slot_info = {...spell_slot_info}
    if(!Object.keys(new_spell_slot_info).includes(slot_id)){
        new_spell_slot_info[slot_id] = 0
    }

    if(add == true){
        new_spell_slot_info[slot_id] = Math.min(new_spell_slot_info[slot_id] + 1, max_uses)
    } else {
        new_spell_slot_info[slot_id] = Math.max(new_spell_slot_info[slot_id] - 1, 0)
    }
    patch_character('spell_slot_info', new_spell_slot_info)
}

export const ViewSpellDisplay = ({ stats, spell_slot_info, patch_character, spell_data}) => {

    const [isOpen, setIsOpen] = useState(null);

    const setOpen = (value) => setIsOpen(value);

    return (
        <div className='spell-display'>
            <div>
                <span className="sub-section">Cantrip</span>
            </div>
            <SpellList 
                spell_list={stats['spells']['cantrip']}
                spell_level={0}
                spell_data={spell_data}
                stats={stats}
                toggle={setOpen}
                open_spell={isOpen}
            />
            { stats['spell_data']['1'] > 0 &&
                <SpellSlot
                    slot_display="1st Level"
                    slot_id='1'
                    slot_max={stats['spell_data']['1']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['1'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['1']}
                    spell_level={1}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
            { stats['spell_data']['2'] > 0 &&
                <SpellSlot
                    slot_display="2nd Level"
                    slot_id='2'
                    slot_max={stats['spell_data']['2']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['2'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['2']}
                    spell_level={2}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
            { stats['spell_data']['3'] > 0 &&
                <SpellSlot
                    slot_display="3rd Level"
                    slot_id='3'
                    slot_max={stats['spell_data']['3']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['3'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['3']}
                    spell_level={3}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
            { stats['spell_data']['4'] > 0 &&
                <SpellSlot
                    slot_display="4th Level"
                    slot_id='4'
                    slot_max={stats['spell_data']['4']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['4'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['4']}
                    spell_level={4}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
            { stats['spell_data']['5'] > 0 &&
                <SpellSlot
                    slot_display="5th Level"
                    slot_id='5'
                    slot_max={stats['spell_data']['5']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['5'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['5']}
                    spell_level={5}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
            { stats['spell_data']['6'] > 0 &&
                <SpellSlot
                    slot_display="6th Level"
                    slot_id='6'
                    slot_max={stats['spell_data']['6']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['6'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['6']}
                    spell_level={6}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
            { stats['spell_data']['7'] > 0 &&
                <SpellSlot
                    slot_display="7th Level"
                    slot_id='7'
                    slot_max={stats['spell_data']['7']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['7'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['7']}
                    spell_level={7}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
            { stats['spell_data']['8'] > 0 &&
                <SpellSlot
                    slot_display="8th Level"
                    slot_id='8'
                    slot_max={stats['spell_data']['8']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['8'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['8']}
                    spell_level={8}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
            { stats['spell_data']['9'] > 0 &&
                <SpellSlot
                    slot_display="9th Level"
                    slot_id='9'
                    slot_max={stats['spell_data']['9']}
                    spell_slot_info={spell_slot_info}
                    patch_character={patch_character}
                />
            }
            { stats['spell_data']['9'] > 0 &&
                    <SpellList 
                    spell_list={stats['spells']['9']}
                    spell_level={9}
                    spell_data={spell_data}
                    stats={stats}
                    toggle={setOpen}
                    open_spell={isOpen}
                />
            }
        </div>
    );
}

const SpellList = ({spell_list, spell_level, spell_data, stats, toggle, open_spell}) => {
    return (
        <table className="spell-table">
            <tr>
                <td>
                    <span>Attack</span>
                </td>
                <td>
                    <span>Time</span>
                </td>
                <td>
                    <span>Range</span>
                </td>
                <td>
                    <span>Hit/DC</span>
                </td>
                <td>
                    <span>Effect</span>
                </td>
                <td>
                    <span>Notes</span>
                </td>
            </tr>
            {spell_list.map((spell) => (
                <Spell stats={stats} spell={spell} spell_data={spell_data[spell.id]} spell_level={spell_level} toggle={toggle} open_spell={open_spell}/>
            ))}
        </table>
    )
}

const Spell = ({spell, spell_level, spell_data, stats, toggle, open_spell}) => {

    //to hit and save dc stuff
    let modifier_value = 0
    let notes = []
    if(spell.spell_casting_attribute == 1){
        modifier_value = stats['strength-modifier']
    } else if(spell.spell_casting_attribute == 2){
        modifier_value = stats['dexterity-modifier']
    } else if(spell.spell_casting_attribute == 3){
        modifier_value = stats['constitution-modifier']
    } else if(spell.spell_casting_attribute == 4){
        modifier_value = stats['intelligence-modifier']
    } else if(spell.spell_casting_attribute == 5){
        modifier_value = stats['wisdom-modifier']
    } else if(spell.spell_casting_attribute == 6){
        modifier_value = stats['charisma-modifier']
    }

    let time = ''
    if(spell_data.activation){
        let activation_name = ''
        Object.keys(SpellActivationTypes).forEach((activation_type_name) => {
            if(SpellActivationTypes[activation_type_name] == spell_data.activation.activationType){
                activation_name = activation_type_name
            }
        })
        if(spell_data.activation.activationType != 8 && spell_data.activation.activationType != 2){
            time = spell_data.activation.activationTime + ' ' + activation_name
        }
    }

    //range
    let range = ''
    let aoe = ''
    if(SpellRangeTypes.includes(spell_data.range.origin)){
        if(spell_data.range.origin == 'Ranged'){
            range = spell_data.range.rangeValue + 'ft'
        } else {
            range = spell_data.range.origin
        }
    }

    //modifier stuff
    let modifier_data = {}
    for (let modifier of spell_data['modifiers']){
        if(modifier.type=='damage'){
            modifier_data = get_modifier_display_info(modifier, spell_level, spell_data, stats)
        }
    }

    //components
    let components = []
    if(spell_data.components.includes(1)){
        components .push('V')
    }
    if(spell_data.components.includes(2)){
        components .push('S')
    }
    if(spell_data.components.includes(2)){
        components .push('M')
    }
    if(components.length > 0){
        notes.push(components.join('/'))
    }
    let unique_id = spell_data.id + '-' + spell_level
    let is_toggled = open_spell == unique_id

    //higher level data
    let higher_level_effects = {}
    if(spell_data.at_higher_levels.length > 0){
        let higher_level = get_modifier_higher_levels(spell_data.at_higher_levels, spell_level, spell_data, stats)
        higher_level_effects = get_effects_for_data({}, higher_level)
        if(higher_level_effects.creatures > 0){
            notes.push('+'+higher_level_effects.creatures + ' Creatures')
        }
        if(higher_level_effects.targets > 0){
            notes.push('+'+higher_level_effects.targets + ' Targets')
        }
        if(higher_level_effects.count > 0){
            notes.push('+'+higher_level_effects.count + ' Count')
        }
    }
    let duration_value = null
    if(spell_data.duration && spell_data.duration.durationType){
        if(spell_data.duration.durationType == 'Time'){
            let duration = parseInt(spell_data.duration.durationInterval)
            if(higher_level_effects.duration){
                duration += higher_level_effects.duration
            }
            duration_value = duration + ' ' + spell_data.duration.durationUnit + '(s)'
            notes.push(duration_value)
        } else {
            duration_value = spell_data.duration.durationType
            notes.push(duration_value)
        }
    }

    if(SpellAoETypes.includes(spell_data.range.aoeType)){
        let aoe_value = spell_data.range.aoeValue
        if(higher_level_effects.area){
            aoe_value += higher_level_effects.area
        }
        aoe = aoe_value + 'ft ' + spell_data.range.aoeType
        notes.push(aoe)
    }

    return (
        <React.Fragment>
            <tr className="clickable" onClick={() => {is_toggled ? toggle(null) : toggle(unique_id)}}>
                <td>
                    <span>{spell_data.name}</span>
                </td>
                <td>
                    <span>{time}</span>
                </td>
                <td>
                    <span>{range}</span>
                </td>
                <td>
                    {spell_data.requires_attack_roll == true &&
                        <span className="to-hit">{(modifier_value + stats['proficiency-bonus']) > 0 && '+'}{modifier_value + stats['proficiency-bonus']}</span>
                    }
                    {spell_data.requires_saving_throw == true &&
                        <div  className="save-dc">
                            <span>{AttributeID[spell_data.save_dc_ability_id]}</span>
                            <span>{8 + modifier_value + stats['proficiency-bonus']}</span>
                        </div>
                    }
                </td>
                <td>
                    {modifier_data.effect &&
                        <span>{modifier_data.effect}</span>
                    }
                    {modifier_data.sub_effect && 
                        <span className="sub-effect"> ({modifier_data.sub_effect})</span>
                    }
                </td>
                <td className="notes">
                    <span>{notes.join(', ')}</span>
                </td>
            </tr>
            <tr className='details'>
                <td colSpan={5}>
                    <Collapse isOpen={is_toggled}>
                        <div>
                            {spell_data['modifiers'].map((spell_modifier) => (
                                <React.Fragment>
                                    <SpellModifierDisplay
                                        spell_modifier={spell_modifier}
                                        spell_level={spell_level}
                                        spell={spell}
                                        spell_data={spell_data}
                                        stats={stats}
                                    />
                                </React.Fragment>
                            ))}
                            {spell_data['modifiers'].length > 0 &&
                                <hr/>
                            }
                            <p>Casting Time: {time} {spell_data.casting_time_description}</p>
                            <p>Range/Area: {range} {aoe}</p>
                            <p>Components: {components.join(' / ')} {spell_data.components_description}</p>
                            <p>Duration: {duration_value}</p>
                            <p>Attack/Save:&nbsp;
                                {spell_data.requires_attack_roll == true &&
                                    <span>{(modifier_value + stats['proficiency-bonus']) > 0 && '+'}{modifier_value + stats['proficiency-bonus']}</span>
                                }
                                {spell_data.requires_saving_throw == true &&
                                    <span>{AttributeID[spell_data.save_dc_ability_id]} {8 + modifier_value + stats['proficiency-bonus']}</span>
                                }
                            </p>
                            <hr/>
                            <Markdown children={spell_data.description}/>
                        </div>
                    </Collapse>
                </td>
            </tr>
        </React.Fragment>
    )
}

function get_modifier_higher_levels(at_higher_levels, spell_level, spell_data, stats){
    let data = {
        additional_points: [],
        additional_targets: [],
        additional_creatures: [],
        extended_duration: [],
        extended_area: [],
        special: [], 
        additional_count: [], 
    }

    if(at_higher_levels){
        if(spell_data.scale_type == 'characterlevel' || spell_data.scale_type == 'spelllevel'){
            // find the highest relevant higher level definition
            let valid_definition = null;
            let current_level = spell_level
            if(spell_data.scale_type == 'characterlevel'){
                current_level = stats['character-level']
            }
            at_higher_levels.forEach((higher_level) => {
                if(higher_level.level){
                    if(!valid_definition && higher_level.level < current_level){
                        valid_definition = higher_level
                    } else if(valid_definition && valid_definition.level < higher_level.level && higher_level.level <= current_level){
                        valid_definition = higher_level
                    }
                }
            })
            if(valid_definition){
                if(valid_definition.typeId == 15){
                    data.additional_points = [
                        {
                            diceCount: valid_definition.dice && valid_definition.dice.diceCount ? valid_definition.dice.diceCount : null,
                            diceValue: valid_definition.dice && valid_definition.dice.diceValue ? valid_definition.dice.diceValue : null,
                            fixedValue: valid_definition.dice && valid_definition.dice.fixedValue ? valid_definition.dice.fixedValue : null,
                        }
                    ]
                } else if(valid_definition.typeId == 1){
                    data.additional_targets = [valid_definition.dice.fixedValue]
                } else if(valid_definition.typeId == 3){
                    data.extended_duration = [valid_definition.dice.fixedValue]
                } else if(valid_definition.typeId == 9){
                    data.extended_area = [valid_definition.dice.fixedValue]
                } else if(valid_definition.typeId == 12){
                    data.special = [valid_definition.details]
                } else if(valid_definition.typeId == 16){
                    data.additional_count = [valid_definition.dice.fixedValue]
                } else if(valid_definition.typeId == 11){
                    data.additional_creatures = [valid_definition.dice.fixedValue]
                }
            }
        } else if(spell_data.scale_type == 'spellscale'){
            // loop over all definitions
            at_higher_levels.forEach((higher_level) => {
                if(higher_level.level){
                    let difference = spell_level - spell_data.level
                    if(difference > 0){
                        let multiplier = Math.floor(difference/higher_level.level)
                        if(multiplier > 0){
                            if(higher_level.typeId == 15){
                                data.additional_points.push(
                                    {
                                        diceCount: higher_level.dice && higher_level.dice.diceCount ? higher_level.dice.diceCount * multiplier : null,
                                        diceValue: higher_level.dice && higher_level.dice.diceValue ? higher_level.dice.diceValue : null,
                                        fixedValue: higher_level.dice && higher_level.dice.fixedValue ? higher_level.dice.fixedValue * multiplier : null,
                                    }
                                )
                            } else if(higher_level.typeId == 1){
                                data.additional_targets.push(higher_level.dice.fixedValue * multiplier)
                            } else if(higher_level.typeId == 3){
                                data.extended_duration.push(higher_level.dice.fixedValue * multiplier)
                            } else if(higher_level.typeId == 9){
                                data.extended_area.push(higher_level.dice.fixedValue * multiplier)
                            } else if(higher_level.typeId == 12){
                                data.special.push(higher_level.details)
                            } else if(higher_level.typeId == 16){
                                data.additional_count.push(higher_level.dice.fixedValue * multiplier)
                            } else if(higher_level.typeId == 11){
                                data.additional_creatures.push(higher_level.dice.fixedValue * multiplier)
                            }
                        }
                    }
                }
            })
        }

    }
    return data
}

function get_effects_for_data(modifier, at_higher_level){
    // reducer to sum arrays
    const reducer = (accumulator, curr) => accumulator + curr;
    let dice = {
        '4': 0,
        '6': 0,
        '8': 0,
        '10': 0,
        '12': 0,
        '20': 0,
        '100': 0,
        fixedValue: 0
    }
    let data = {
        count: at_higher_level.additional_count.reduce(reducer, 0) + (modifier.count ? modifier.count : 0),
        creatures: at_higher_level.additional_creatures.reduce(reducer, 0),
        targets: at_higher_level.additional_targets.reduce(reducer, 0),
        area: at_higher_level.extended_area.reduce(reducer, 0),
        duration: at_higher_level.extended_duration.reduce(reducer, 0) + (modifier.duration ? modifier.duration.durationInterval : 0),
        type: modifier.type,
        type_name: modifier.friendly_type_name,
        sub_type_name: modifier.friendly_sub_type_name,
        restriction: modifier.restriction,
        value_display: ''
    }
    if(modifier.duration && modifier.duration.durationUnit){
        data.duration = data.duration + modifier.duration.durationUnit
    }

    if(modifier.dice){
        dice[modifier.dice.diceValue] += modifier.dice.diceCount
        dice.fixedValue += modifier.dice.fixedValue
    }
    at_higher_level.additional_points.forEach((additional_point) => {
        dice[additional_point.diceValue] += additional_point.diceCount
        dice.fixedValue += additional_point.fixedValue
    })
    let dice_displays = []
    let dice_values = [4,6,8,10,12,20,100]
    dice_values.forEach((dice_value) => {
        if(dice[dice_value] > 0){
            dice_displays.push(dice[dice_value] + 'd' + dice_value)
        }
    })
    data.value_display = dice_displays.join(' + ')
    if(data.value_display != ''){
        if(dice.fixedValue > 0){
            data.value_display = data.value_display + ' + ' + dice.fixedValue
        } else if(dice.fixedValue < 0){
            data.value_display = data.value_display + ' - ' + (dice.fixedValue*-1)
        }
    } else {
        data.value_display = dice.fixedValue
    }
    return data
}

function get_modifier_display_info(spell_modifier, spell_level, spell_data, stats){
    let data = {
        effect: null,
        sub_effect: null,
        restriction: null,
        id: spell_modifier.id,
        notes: [],
    }
    let higher_level = get_modifier_higher_levels(spell_modifier.at_higher_levels, spell_level, spell_data, stats)
    let effects = get_effects_for_data(spell_modifier, higher_level)

    if(["bonus", 'damage'].includes(spell_modifier.type)){
        data.effect = effects.value_display
        data.sub_effect = effects.sub_type_name
        data.restriction = effects.restriction
    } else if(["resistance", "immunity", "vulnerability"].includes(spell_modifier.type)){
        data.effect = effects.type_name
        data.sub_effect = spell_modifier.sub_type_name
        data.restriction = effects.restriction
    } else if(["advantage", "disadvantage"].includes(spell_modifier.type)){
        data.effect = effects.type_name + ' on'
        data.sub_effect = effects.sub_type_name
        data.restriction = effects.restriction
    } else {
        console.log('HEY SARAH NEW SPELL MODIFIER')
        console.log(spell_modifier)
    }

    
    if(data.duration > 0){
        data.notes.push(data.duration)
    }
    if(data.creatures > 0){
        data.notes.push('+'+data.creatures + ' Creatures')
    }
    if(data.targets > 0){
        data.notes.push('+'+data.targets + ' Targets')
    }
    if(data.area > 0){
        data.notes.push('+'+data.area + ' Area')
    }
    if(data.count > 0){
        data.notes.push('+'+data.count + ' Count')
    }
    return data
};

const SpellModifierDisplay = ({spell_modifier, spell_level, spell, spell_data, stats}) => {
    let data = get_modifier_display_info(spell_modifier, spell_level, spell_data, stats)
    if(data.effect){
        return (
            <div key={spell_level + spell_data.id + data.id} className='spell-modifier'>
                <div>{data.effect} {data.sub_effect}</div>
                {data.restriction && data.restriction != '' &&
                    <div>{data.restriction}</div>
                }
            </div>
        )
    }

    return null
}

const SpellSlot = ({slot_display, slot_id, slot_max, spell_slot_info, patch_character}) => {
    let current_uses = spell_slot_info[slot_id] ? spell_slot_info[slot_id] : 0

    return (
        <div className="sub-section">
            <span>{slot_display}</span>
            <div className="fill-boxs">
                {[...Array(slot_max).keys()].map((index) => (
                    <div key={"spell-slot-cantrip" + '-' + index} className="fill-box-container">
                        <FillBox
                            value={index < current_uses}
                            on_click={() => {patch_use(!(index < current_uses), patch_character, spell_slot_info, slot_max, slot_id)}}
                            use_small_fill_box={true}
                        />
                    </div>
                ))}
                <span>&nbsp;Slots</span>
            </div>
        </div>
    )
}