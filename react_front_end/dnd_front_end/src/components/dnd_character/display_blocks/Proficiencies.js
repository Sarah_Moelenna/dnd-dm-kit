import { Proficiences } from '../../monster/Data';

export const Proficiencies = ({ stats }) => {

    let dict_proficiences = {}
    Proficiences.forEach((proficiency) => {
        dict_proficiences[proficiency.name] = proficiency
    })
    let current_proficiencies = {
        'WEAPON': [],
        'TOOL': [],
        'ARMOUR': []
    }
    Object.keys(stats['misc_proficiencies']).forEach((misc_proficiency) => {
        let prof = dict_proficiences[misc_proficiency]
        if(prof){
            current_proficiencies[prof.group].push(prof.display_name)
        }
    })

    return (
        <div className='proficiencies-display'>
            <div className='proficiencies-set lined'>
                <p>WEAPON</p>
                {current_proficiencies['WEAPON'].map((prof) => (
                    <span key={'prof-display-'+prof}>{prof}</span>
                ))}
                {current_proficiencies['WEAPON'].length == 0 &&
                    <span>None</span>
                }
            </div>
            <div className='proficiencies-set lined'>
                <p>ARMOUR</p>
                {current_proficiencies['ARMOUR'].map((prof) => (
                    <span key={'prof-display-'+prof}>{prof}</span>
                ))}
                {current_proficiencies['ARMOUR'].length == 0 &&
                    <span>None</span>
                }
            </div>
            <div className='proficiencies-set lined'>
                <p>TOOL</p>
                {current_proficiencies['TOOL'].map((prof) => (
                    <span key={'prof-display-'+prof}>{prof}</span>
                ))}
                {current_proficiencies['TOOL'].length == 0 &&
                    <span>None</span>
                }
            </div>

            <div className='proficiencies-set'>
                <p>LANGUAGE</p>
                {[...stats['language']].map((prof) => (
                    <span key={'prof-display-'+prof}>{prof}</span>
                ))}
                {stats['language'].size == 0 &&
                    <span>None</span>
                }
            </div>
        </div>
    );
}