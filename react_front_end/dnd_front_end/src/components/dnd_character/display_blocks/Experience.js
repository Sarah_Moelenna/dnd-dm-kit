import { LevelAdvancement } from "../../monster/Data";

export const Experience = ({ stats }) => {

    let current_level_object = LevelAdvancement[stats['character-level']]
    let next_level_object = LevelAdvancement[Math.min(parseInt(stats['character-level']) + 1, 20)]

    let xp_percent = (stats['experience'] - current_level_object.experience) / (next_level_object.experience - current_level_object.experience) * 100

    return (
        <div className='experience-display'>
            <div className='levels'>
                <span>LVL {stats['character-level']}</span>
                <span>LVL {Math.min(parseInt(stats['character-level']) + 1, 20)}</span>
            </div>
            <div className="experience-bar-container">
                <div style={{width: xp_percent +'%'}} className="experience-bar"/>
            </div>
            <div className='experience'>
                <span>{stats['experience']} / {next_level_object.experience} XP</span>
            </div>
        </div>
    );
}