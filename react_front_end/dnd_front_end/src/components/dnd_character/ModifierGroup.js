import React, { Component } from 'react';
import { ModifierSubTypes} from '../monster/Data';
import { get_choices } from '../shared/Choices';
import Form from 'react-bootstrap/Form';

class ModifierGroup extends Component {

    state = {
        stored_choices: {}
    }

    find_modifier_subtype = (type_id, subtype_id) => {
        let modifer_subtypes = ModifierSubTypes[type_id]
        for(let i = 0; i < modifer_subtypes.length; i++){
            let subtype=modifer_subtypes[i]
            if(subtype.id == parseInt(subtype_id)){
                return subtype
            }
        }
        return null
    }

    get_choice_modifiers = () => {
        let existing_selections = []
        let choice_modifiers = []
        let choice_modifiers_found = false

        for(let i = 0; i < this.props.modifiers.length; i++){
            let modifier = this.props.modifiers[i]
            let subtype = this.find_modifier_subtype(modifier.modifier_type_id, modifier.modifier_sub_type_id)
            
            if(subtype != null){
                existing_selections.push(subtype.name)
            }

            if(subtype != null && subtype.choice_groups != undefined){
                choice_modifiers_found = true
                let choice_id = this.props.object_id + '-' + modifier.modifier_type_id + '-' + modifier.modifier_sub_type_id + '-' + i
                let choice_value = this.props.choices[choice_id]
                if(choice_value != undefined && choice_value != null){
                    existing_selections.push(choice_value)
                }
  
            }
        }
        if(choice_modifiers_found == true){
            for(let i = 0; i < this.props.modifiers.length; i++){
                let modifier = this.props.modifiers[i]
                let subtype = this.find_modifier_subtype(modifier.modifier_type_id, modifier.modifier_sub_type_id)
                if(subtype != null && subtype.choice_groups != undefined){
                    let choice_id = this.props.object_id + '-' + modifier.modifier_type_id + '-' + modifier.modifier_sub_type_id + '-' + i
                    let choice_value = this.props.choices[choice_id]
                    let exclusion = (subtype.exclude != undefined && subtype.exclude != undefined) ? subtype.exclude : []
                    let only = subtype.only
                    if(modifier.limits && modifier.limits.length > 0){
                        only = modifier.limits
                    }
                    exclusion.push(...existing_selections)
                    modifier.options = get_choices(subtype.choice_groups, only, exclusion, [choice_value], this.store_choices, this.state.stored_choices)
                    modifier.subtype = subtype
                    modifier.choice_id = choice_id
                    modifier.choice_value = choice_value
                    choice_modifiers.push(modifier)
                }
            }
        }

        return choice_modifiers
    }

    store_choices = (data) => {
        this.setState({stored_choices: data})
    }


    render(){
        let choice_modifiers = this.get_choice_modifiers()

        return (
            <div className="modifier_group">
                {choice_modifiers.map((modifier, index) => (
                    <div key={modifier.choice_id}>
                        <SubtypeModifier
                            object_id={this.props.object_id}
                            modifier={modifier}
                            index={index}
                            choices={this.props.choices}
                            save_choice_function={this.props.save_choice_function}
                        />
                    </div>
                ))}
            </div>
        )
    }
}

class SubtypeModifier extends Component {

    handleChange = (e) => {
        this.props.save_choice_function(e.target.name, e.target.value)
    }
    
    render(){
        return (
            <div>
                <Form.Group>
                    <Form.Control name={this.props.modifier.choice_id} as="select" onChange={this.handleChange} custom>
                        <option value={null}>-{this.props.modifier.subtype.display_name}-</option>
                        {this.props.modifier.options.map((option) => (
                            <option
                                key={this.props.modifier.choice_id + '-' + option.id}
                                value={option.id} 
                                selected={this.props.modifier.choice_value == option.id}
                            >{option.name}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
            </div>
        )
    }
}

export default ModifierGroup;