import React, { Component } from 'react';
import ListFeats from "./Feat";
import {ListSkillSection} from './Skills';

class Extras extends Component{
    state = {
        toggle: false
    }

    render(){
        return (
            <div>
                <ListFeats
                    feats={this.props.feats}
                    choices={this.props.feat_choices}
                    save_choice_function={this.props.save_feat_choice_function}
                    level={this.props.character_level}
                    toggled={this.state.toggle == 'FEATS'}
                    toggle_function={(value) => {this.setState({toggle: value})}}
                />
                <ListSkillSection
                    stats={this.props.stats}
                    toggled={this.state.toggle == 'SKILLS'}
                    toggle_function={(value) => {this.setState({toggle: value})}}
                />
            </div>
        )
    }
}

export default Extras