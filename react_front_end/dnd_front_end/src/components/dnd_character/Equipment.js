import React, { Component } from 'react';
import Markdown from 'react-markdown';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Collapse } from 'reactstrap';
import { DND_POST, DND_PATCH, DND_DELETE } from '../shared/DNDRequests';
import Button from 'react-bootstrap/Button'
import {ItemDetails} from '../items/SingleItem'
import ItemSelect from '../items/ItemSelect'
import Modal from 'react-bootstrap/Modal';
import MDEditor from '@uiw/react-md-editor';
import FillBox from '../shared/FillBox';

export class Equipment extends Component {

    state = {
        choices_toggle: false,
        inventory_toggle: false,
    }

    add_all = () => {
        let items = []
        let gold = 0
        this.props.equipment_sets.forEach((equipment_set) => {
            equipment_set.groups.forEach((group) => {
                let group_id = equipment_set.id + '-' + group.id
                group.options.forEach((option) => {
                    let is_selected = this.props.choices[group_id] == option.id
                    if(is_selected){
                        //provide all custom items
                        if(option.custom_items){
                            let new_custom_items = option.custom_items.split("|")
                            new_custom_items.forEach((custom_item) => {
                                items.push({
                                    item_name: custom_item
                                })
                            })
                        }
                        gold = gold + option.gold
                        //option itself has items and provides all of them
                        if(option.items && option.select_type == 'PROVIDE ALL'){
                            option.items.forEach((item) => {
                                items.push({
                                    item_id: item.id
                                })
                            })
                        } else if (option.items && option.select_type == 'SELECT_ONE'){
                            option.items.map((item) => {
                                let is_option_item_selected = this.props.choices[group_id + '-' + option.id] == item.id
                                if(is_option_item_selected){
                                    items.push({
                                        item_id: item.id
                                    })
                                }
                            })
                        }

                        //if option has subsets
                        if(option.subsets){
                            option.subsets.forEach((subset) => {
                                if(subset.select_type == 'PROVIDE ALL'){
                                    subset.items.map((item) => {
                                        items.push({
                                            item_id: item.id
                                        })
                                    })
                                } else if (subset.select_type == 'SELECT_ONE'){
                                    subset.items.map((item) => {
                                        let is_subset_selected = this.props.choices[group_id + '-' + option.id + '-' + subset.id] == item.id
                                        if(is_subset_selected){
                                            items.push({
                                                item_id: item.id
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    }
                })
            })
        })
        this.props.update_gold(this.props.gold + gold)
        DND_POST(
            '/dnd-character/' + this.props.character_id + '/inventory',
            {data: items},
            (response) => {
                this.props.update_inventory_function(response.inventory)
                this.setState({choices_toggle: false})
            },
            null
        )
    }

    render(){
        return (
            <div className='equipment'>
                <Card>
                    <Card.Title className='clickable-div' onClick={() => {this.setState({choices_toggle: !this.state.choices_toggle})}}>
                        <Row>
                            <Col xs={10} md={10} className="class-feature-name">
                                <div>Equipment Choices</div>
                            </Col>
                            <Col xs={2} md={2} className='chevrons'>
                                <div><i className={this.state.choices_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            </Col>
                        </Row>
                    </Card.Title>
                    <Collapse isOpen={this.state.choices_toggle}>
                        <Card.Body>
                            {this.props.equipment_sets.map((equipment_set) => (
                                <EquipmentSet
                                    equipment_set={equipment_set}
                                    choices={this.props.choices}
                                    save_choice_function={this.props.save_choice_function}
                                />
                            ))}
                            <Button variant="primary" type="submit" onClick={this.add_all}>
                                Add Selected
                            </Button>
                        </Card.Body>
                        
                    </Collapse>
                </Card>

                <Card>
                    <Card.Title className='clickable-div' onClick={() => {this.setState({inventory_toggle: !this.state.inventory_toggle})}}>
                        <Row>
                            <Col xs={10} md={10} className="class-feature-name">
                                <div>Inventory</div>
                            </Col>
                            <Col xs={2} md={2} className='chevrons'>
                                <div><i className={this.state.inventory_toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                            </Col>
                        </Row>
                    </Card.Title>
                    <Collapse isOpen={this.state.inventory_toggle}>
                        <Card.Body className='item-page'>
                            <ListInventoryItem
                                character_id={this.props.character_id}
                                inventory={this.props.inventory}
                                update_inventory_function={this.props.update_inventory_function}
                            />
                        
                        </Card.Body>
                        
                    </Collapse>
                </Card>
                
            </div>
        )
    }
}

export class ListInventoryItem extends Component {
    state={
        item_toggle: null,
        custom_item_toggle: false,
        custom_item: null
    }

    delete_character_item = (id) => {
        DND_DELETE(
            '/dnd-character/' + this.props.character_id + '/inventory/' + id,
            null,
            (response) => {
                this.props.update_inventory_function(response.inventory)
            },
            null
        )
    }

    edit_character_item = (id, property, value) => {
        let data = {}
        data[property] = value
        let new_items = this.props.inventory
        for(let i = 0; i < new_items.length; i++){
            if (new_items[i].id == id){
                new_items[i][property] = value
            }
        }
        this.props.update_inventory_function(new_items)

        DND_PATCH(
            '/dnd-character/' + this.props.character_id + '/inventory/' + id,
            {data: data},
            (response) => {},
            null
        )
    }

    add_item_by_id = (item_id) => {
        DND_POST(
            '/dnd-character/' + this.props.character_id + '/inventory',
            {data: [{item_id: item_id}]},
            (response) => {
                this.props.update_inventory_function(response.inventory)
            },
            null
        )
    }

    add_custom_item = (item_name) => {
        this.setState({custom_item: null, custom_item_toggle: false})
        if(!item_name){
            return
        }
        DND_POST(
            '/dnd-character/' + this.props.character_id + '/inventory',
            {data: [{item_name: item_name}]},
            (response) => {
                this.props.update_inventory_function(response.inventory)
            },
            null
        )
    }

    render () {
        return (
            <div>
                <ItemSelect select_function={this.add_item_by_id} override_button="Add Item"/>
                <Button onClick={()=>{this.setState({custom_item_toggle: true})}}>Add Custom Item</Button>
                <Modal show={this.state.custom_item_toggle} size="md" className="custom-item-add-modal">
                    <Modal.Header>Add Custom item</Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control
                                value={this.state.custom_item}
                                name="name"
                                type="text"
                                onChange={(e) => {this.setState({custom_item: e.target.value})}}
                            />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => {this.add_custom_item(this.state.custom_item)}}>Add</Button>
                        <Button onClick={()=>{this.setState({custom_item_toggle: !this.state.custom_item_toggle})}}>Close</Button>
                    </Modal.Footer>
                </Modal>
                {this.props.inventory.map((inventory_item) => (
                    <InventoryItem
                        inventory_item={inventory_item}
                        toggle_item={(item_id) => {
                            if(item_id == this.state.item_toggle){
                                this.setState({item_toggle: null})
                            } else {
                                this.setState({item_toggle: item_id})
                            }
                        }}
                        toggled={this.state.item_toggle==inventory_item.id}
                        delete_character_item={this.delete_character_item}
                        edit_character_item={this.edit_character_item}
                        use_small_fill_box={this.props.use_small_fill_box}
                    />
                ))}
            </div>
        )
    }
}

class InventoryItem extends Component {
    
    render(){
        return (
            <Card key={this.props.inventory_item.id} className='item'>
                <Card.Title className='clickable-div' onClick={() => {this.props.toggle_item(this.props.inventory_item.id)}}>
                    <Row>
                        <Col xs={1} md={1} className="class-feature-name">
                            { this.props.inventory_item.item && this.props.inventory_item.item.can_equip == true &&
                                <FillBox
                                    value={this.props.inventory_item.is_equipped}
                                    on_click={(value) => {this.props.edit_character_item(this.props.inventory_item.id,'is_equipped',value)}}
                                    use_small_fill_box={this.props.use_small_fill_box}
                                />
                            }
                        </Col>
                        <Col xs={9} md={9} className="class-feature-name">
                            <div>
                                { (!this.props.inventory_item.item || this.props.inventory_item.item.stackable == true) &&
                                    <span>{this.props.inventory_item.count} </span>
                                }
                                {this.props.inventory_item.item_name}
                            </div>
                        </Col>
                        <Col xs={2} md={2} className='chevrons'>
                            <div><i className={this.props.toggled == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                        </Col>
                    </Row>
                </Card.Title>
                <Collapse isOpen={this.props.toggled}>
                    <Card.Body>
                    { this.props.inventory_item.item &&
                        <ItemDetails item={this.props.inventory_item.item}/>
                    }
                    { !this.props.inventory_item.item &&
                        <div>
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                value={this.props.inventory_item.item_name}
                                name="name"
                                type="text"
                                placeholder="Enter Name"
                                onChange={(e) => {this.props.edit_character_item(this.props.inventory_item.id,'item_name',e.target.value)}}
                            />
                            <Form.Label>Description</Form.Label>
                            <MDEditor
                                value={this.props.inventory_item.description}
                                preview="edit"
                                onChange={(value) => {this.props.edit_character_item(this.props.inventory_item.id,'description',value)}}
                            />
                        </div>
                    }
                    <div>
                        { (!this.props.inventory_item.item || this.props.inventory_item.item.stackable == true) &&
                            <p>
                                <span>Quantity: </span>
                                { this.props.inventory_item.count > 1 ?
                                    <span><i className="fas fa-minus clickable-icon" onClick={() => this.props.edit_character_item(this.props.inventory_item.id,'count',this.props.inventory_item.count-1)}></i></span>
                                    :
                                    <span><i className="fas fa-minus clickable-icon"></i></span>
                                }
                                <span> {this.props.inventory_item.count} </span>
                                <span><i className="fas fa-plus clickable-icon" onClick={() => this.props.edit_character_item(this.props.inventory_item.id,'count',this.props.inventory_item.count+1)}></i></span>
                            </p>
                        }
                        <div className='clickable-div clickable-icon' onClick={() => {this.props.delete_character_item(this.props.inventory_item.id)}}><i class="fas fa-times"></i> Remove Item</div>
                    </div>
                    </Card.Body>
                </Collapse>
            </Card>
        )
    }
}

class EquipmentSet extends Component {

    render(){
        return (
            <div>
                {this.props.equipment_set.description &&
                    <Markdown children={this.props.equipment_set.description}/>
                }
                {this.props.equipment_set.groups.map((group) => (
                    <EquipmentSetGroup
                        group={group}
                        choices={this.props.choices}
                        save_choice_function={this.props.save_choice_function}
                        object_id={this.props.equipment_set.id}
                    />
                ))}
                <hr/>
            </div>
        )
    }
}

class EquipmentSetGroup extends Component {

    get_group_choice_id = () => {
        return this.props.object_id + '-' + this.props.group.id
    }

    handleChange = (e) => {
        this.props.save_choice_function(e.target.name, e.target.id)
    }

    handleChoice = (e) => {
        this.props.save_choice_function(e.target.name, e.target.value)
    }

    render(){
        let group_choice_id = this.get_group_choice_id()

        return (
            <Card><Card.Body>
                <p>{this.props.group.name}</p>
                <Form.Group>
                        {this.props.group.options.map((option) => (
                            <div>
                                <Form.Check 
                                    type='radio'
                                    id={option.id} 
                                    label={option.name}
                                    name={group_choice_id}
                                    checked={this.props.choices[group_choice_id] == option.id}
                                    onChange={this.handleChange}
                                />
                                { this.props.choices[group_choice_id] == option.id && option.select_type == 'SELECT_ONE' && option.subsets.length == 0 &&
                                    <div>
                                        <Form.Control name={group_choice_id + '-' + option.id} as="select" onChange={this.handleChoice} custom>
                                            <option value={null}>-</option>
                                            {option.items.map((item) => (
                                                <option
                                                    key={group_choice_id + '-' + option.id + '-' + item.id}
                                                    value={item.id} 
                                                    selected={this.props.choices[group_choice_id + '-' + option.id] == item.id}
                                                >{item.name}</option>
                                            ))}
                                        </Form.Control>
                                    </div>
                                }
                                { this.props.choices[group_choice_id] == option.id && option.select_type == 'SELECT_ONE' && option.subsets.length != 0 &&
                                    <div>
                                        {option.subsets.map(subset => {
                                            if(subset.select_type == 'SELECT_ONE'){
                                                return (<Form.Control name={group_choice_id + '-' + option.id + '-' + subset.id} as="select" onChange={this.handleChoice} custom>
                                                    <option value={null}>-</option>
                                                    {subset.items.map((item) => (
                                                        <option
                                                            key={group_choice_id + '-' + option.id + '-' + subset.id + '-' + item.id}
                                                            value={item.id} 
                                                            selected={this.props.choices[group_choice_id + '-' + option.id + '-' + subset.id] == item.id}
                                                        >{item.name}</option>
                                                    ))}
                                                </Form.Control>)
                                            }
                                        })}
                                    </div>
                                }
                            </div>
                        ))}
                    
                </Form.Group>
            </Card.Body></Card>
        )
    }
}