import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'

class FilterCharacter extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="character-filters">
                <Row>
                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control value={this.props.filters["name"]} name="name" type="text" placeholder="Search Names" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>


                    <Col xs={3} md={3}>
                        <a className="btn btn-primary" onClick={() => this.props.clear_filter_function()}>Clear Filters</a>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default FilterCharacter;