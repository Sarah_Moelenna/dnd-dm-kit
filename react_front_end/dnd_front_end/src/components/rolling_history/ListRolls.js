import React from 'react';
import SingleRoll from './SingleRoll'

const ListRolls = ({ rolls}) => {
    if(rolls === null || rolls === undefined || rolls.length == 0) {
        return (<p>No Dice Rolls</p>)
    }
    return (
        <div className="rolls">
            {rolls.map((roll, i) => (
                <div key={i}>
                    <SingleRoll roll={roll} />
                </div>
            ))}
        </div>
    );
}

export default ListRolls;