import React from 'react';

const SingleRoll = ({ roll }) => {

    return (
        <div className='roll'>
            <h2 className='title'>{roll.identity}</h2>
            <p className='description'>{roll.roll.startsWith("-") && "d20"}{roll.roll.startsWith("+") && "d20"}{roll.roll} {roll.roll_modifier != "DEFAULT" && "- " + roll.roll_modifier.toLowerCase()}</p>
            <p className='result selected'>
                {roll.selected_roll.dice.map((dice, i) => (
                    <i key={i}>{(dice.result == 1 || dice.result == dice.sides) ? <b>({dice.result})</b> : <p>({dice.result})</p>}</i>
                ))}
                {roll.selected_roll.modifier != "0" && roll.selected_roll.modifier.replaceAll('+', ' + ').replaceAll('-', ' - ')} Result: {roll.selected_roll.total_result}
            </p>
            {roll.unselected_roll != null && 
                <p className='result unselected'>
                    {roll.unselected_roll.dice.map((dice, i) => (
                    <i key={i}>{(dice.result == 1 || dice.result == dice.sides) ? <b>({dice.result})</b> : <p>({dice.result})</p>}</i>
                ))}
                    {roll.unselected_roll.modifier != "0" && roll.unselected_roll.modifier.replaceAll('+', ' + ').replaceAll('-', ' - ')} Result: {roll.unselected_roll.total_result}
                </p>
            }
            <hr/>
        </div>
    );
}

export default SingleRoll;