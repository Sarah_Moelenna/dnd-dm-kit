import React, {Component} from 'react';
import {UserNavigation, get_page_from_path} from './layout/Navigation';
import Header from './layout/Header';
import Footer from './layout/Footer';
import HomePage from './pages/HomePage';
import SessionPage from './pages/SessionPage';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_GET } from './shared/DNDRequests';
import { SocketContext, socket } from './socket/Socket';
import {ChatBox} from './socket/ChatBox'
import PlayerNotesPage from './pages/PlayerNotesPage';
import {withRouter} from 'react-router-dom';
import { PlayerContext } from './shared/PlayerContext';

class App extends Component {

  state = {
    current_page: "SESSION",
    characters: null,
    current_campaign: null,
    override: {},
    session_end: false,
    collections: []
  }

  get_player_context = ()=> {
    return {
      'user': {},
      'characters': this.state.characters != null ? this.state.characters : {},
      'campaigns': {},
      'current_campaign': this.state.current_campaign != null ? this.state.current_campaign : {}
    }
  }

  componentWillMount(){
    this.refresh_collections()
    socket.emit('join', this.props.session_id);
  }

  componentDidMount(){
    
    var page = get_page_from_path(this.props.location.pathname)
    if(page != undefined && page != null){
      this.set_page(page)
    }

    socket.on("session_end", data => {
      this.setState({session_end: true})
    });
  }

  refresh_collections = () => {
    this.setState({ collections: []})

    DND_GET(
      '/session/collection',
      (json) => {
        if(json.length>0){
            this.setState({ collections: json})
        } else {
            this.setState({ collections: null})
        }
      },
      null
    )
  };


  set_override = (new_value) => {
    if(new_value.page != undefined){
      this.setState({ override: new_value, current_page: new_value.page})
    } else {
      this.setState({ override: new_value})
    }
  };

  set_page = (page) => {
    this.setState({ current_page: page })
  };

  roll_dice = (input, type, modifier, identity) => {
    var url = '/dice-roll/?roll=' + encodeURIComponent(input) + "&roll_type=" + type + "&roll_modifier=" + modifier + "&roll_identity=" + encodeURIComponent(identity)

    DND_GET(
      url,
      (jsondata) => {
        var new_roll_history = this.state.roll_history
        new_roll_history.unshift(jsondata)
        this.setState({ roll_history: new_roll_history})
      },
      null
    )
    
  }

  render () {
    if(this.state.session_end == true){
      return(
        <div className="session-ended">
          Thanks for Playing
        </div>
      )
    }

    return(
      <div>
        <PlayerContext.Provider value={this.get_player_context()}>
          <Header/>
          { this.props.user_data != undefined &&
            <UserNavigation page_switch={this.set_page} current_page={this.state.current_page} logout_function={this.props.logout}/>
          }
          <SocketContext.Provider value={socket}>
              <div className="page-container">
                { this.props.user_data != undefined ?
                  <Row>
                    <Col xs={2} md={2} className="sidebar-section">
                      <Row className="user-character">
                        <Col xs={4} md={4}>
                            <img className="avatar" src={this.props.user_data.avatar} />
                        </Col>
                        <Col xs={8} md={8}>
                                <h2>{this.props.user_data.name}</h2>
                                  <a className="btn btn-primary" target="_blank" href={this.props.user_data.url}>Character Sheet</a>
                        </Col>
                      </Row>
                      <hr/>
                      <ChatBox name={this.props.user_data.name}/>
                    </Col>
                    <Col xs={10} md={10}  className="page-view-section">
                        <div className="page-bg">
                          <HomePage active={this.state.current_page=="HOME"}/>
                          <SessionPage active={this.state.current_page=="SESSION"} is_player={true}/>
                          <PlayerNotesPage active={this.state.current_page=="PLAYER_NOTES"} collections={this.state.collections}/>
                        </div>
                    </Col>
                  </Row>
                :
                  <div className="page-view-section">
                    <div className="page-bg">
                      <SessionPage active={this.state.current_page=="SESSION"} is_player={true} is_observer={true}/>
                    </div>
                  </div>
                }
              </div>
              
          </SocketContext.Provider>
          <div className="fake-footer"></div>
          <Footer bot_state={null}/>
        </PlayerContext.Provider>
      </div>
    )

  }
}

export default withRouter(App);
