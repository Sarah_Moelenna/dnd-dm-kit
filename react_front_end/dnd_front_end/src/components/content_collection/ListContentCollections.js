import React, { useContext, Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE, DND_PUT, DND_POST } from '../shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import { Collapse } from 'reactstrap';
import Button from 'react-bootstrap/Button'
import { PlayerContext } from '../shared/PlayerContext';
import MDEditor from '@uiw/react-md-editor';
import Markdown from 'react-markdown'

class ContentCollectionItem extends Component {

    state = {
        toggle: false,
    }

    static contextType = PlayerContext

    update_text = (text) => {
        this.props.edit_collection_function(this.props.collection.id, {"description": text})
    }

    render(){
        const player = this.context

        return(
            <Row key={this.props.collection.id} className="content-collection-item">
                <Col xs={(player.user.user_type == "TYPE_ADMIN" && this.props.display_type == "edit") ? 2 : 4} md={(player.user.user_type == "TYPE_ADMIN" && this.props.display_type == "edit") ? 2 : 4} className="content-collection-name">
                    <p>{this.props.collection.name}</p>
                </Col>
                {this.props.display_type == "edit" &&
                    <Col xs={2} md={2} className="content-collection-snippet">
                        {this.props.collection.is_shareable == true ?
                            <p>Shared</p>
                            :
                            <Button onClick={() => {this.props.edit_collection_function(this.props.collection.id, {"is_shareable": true})}}>Share Collection</Button>
                        } 
                    </Col>
                }
                {player.user.user_type == "TYPE_ADMIN" && this.props.display_type == "edit" && this.props.collection.can_edit == true &&
                    <Col xs={2} md={2} className="content-collection-snippet">
                        {this.props.collection.is_default == true ?
                            <p>Default</p>
                            :
                            <Button onClick={() => {this.props.edit_collection_function(this.props.collection.id, {"is_default": true})}}>Set as Default</Button>
                        } 
                    </Col>
                }
                {player.user.user_type == "TYPE_ADMIN" && this.props.display_type == "edit" && this.props.collection.can_edit == false &&
                    <Col xs={2} md={2} className="content-collection-snippet">
                        {this.props.collection.is_default == true ?
                            <p>Default</p>
                            :
                            <p>Not Default</p>
                        } 
                    </Col>
                }
                <Col xs={2} md={2} className="content-collection-created-by">
                    {this.props.collection.created_by}
                </Col>
                {(this.props.display_type == "read" || this.props.collection.can_edit == false) &&
                    <Col xs={2} md={2} className="content-collection-snippet">
                        {this.props.collection.added == true ?
                            <Button onClick={() => {this.props.delete_user_collection_function(this.props.collection.id)}}>Remove Collection</Button>
                            :
                            <Button onClick={() => {this.props.add_user_collection_function(this.props.collection.id)}}>Add Collection</Button>
                        }
                    </Col>
                }
                {this.props.collection.can_edit == true &&
                    <Col xs={2} md={2} className="content-collection-snippet">
                        {this.props.collection.is_shareable != true &&
                            <DeleteConfirmationButton id={this.props.collection.id} name="Content Collection" delete_function={this.props.delete_collection_function} override_button="Delete"/>
                        }
                    </Col>
                }
                <Col xs={2} md={2} className="content-collection-dropper">
                    <div className="clickable-div" onClick={() => {this.setState({toggle: !this.state.toggle})}}><i className={this.state.toggle == false ? "fas fa-chevron-down" : "fas fa-chevron-up"}></i></div> 
                </Col>
                <Col xs={12} md={12}>
                    <Collapse isOpen={this.state.toggle}>
                        <Row>
                            <Col xs={6} md={6}>
                                <p><b>Description:</b></p>
                                {(this.props.collection.can_edit == true && this.props.display_type == "edit") ?
                                    <MDEditor
                                        value={this.props.collection.description}
                                        preview="edit"
                                        onChange={this.update_text}
                                    />
                                    :
                                    <Markdown children={this.props.collection.description} />
                                }
                            </Col>
                            <Col xs={6} md={6}>
                                <p><b>Included Content:</b></p>
                                <div className="details">
                                    { this.props.collection.monster_count > 0 &&
                                        <div>
                                            <i className="fas fa-spider"></i>
                                            <p>{this.props.collection.monster_count} Monsters</p>
                                        </div >
                                    }
                                    { this.props.collection.item_count > 0 &&
                                        <div>
                                            <i className="fas fa-wine-bottle"></i>
                                            <p>{this.props.collection.item_count} Items</p>
                                        </div >
                                    }
                                    { this.props.collection.spell_count > 0 &&
                                        <div>
                                            <i className="fas fa-magic"></i>
                                            <p>{this.props.collection.spell_count} Spells</p>
                                        </div >
                                    }
                                    { this.props.collection.spell_list_count > 0 &&
                                        <div>
                                            <i className="fas fa-list"></i>
                                            <p>{this.props.collection.spell_list_count} Spell Lists</p>
                                        </div >
                                    }
                                    { this.props.collection.race_count > 0 &&
                                        <div>
                                            <i className="fas fa-paw"></i>
                                            <p>{this.props.collection.race_count} Races</p>
                                        </div >
                                    }
                                    { this.props.collection.battlemap_count > 0 &&
                                        <div>
                                            <i className="fas fa-fist-raised"></i>
                                            <p>{this.props.collection.battlemap_count} Battle Maps</p>
                                        </div >
                                    }
                                    { this.props.collection.background_count > 0 &&
                                        <div>
                                            <i className="fas fa-user-tag"></i>
                                            <p>{this.props.collection.background_count} Backgrounds</p>
                                        </div >
                                    }
                                    { this.props.collection.class_count > 0 &&
                                        <div>
                                            <i className="fas fa-shield-alt"></i>
                                            <p>{this.props.collection.class_count} Classes</p>
                                        </div >
                                    }
                                    { this.props.collection.subclass_count > 0 &&
                                        <div>
                                            <i className="fas fa-shield-alt"></i>
                                            <p>{this.props.collection.subclass_count} Subclasses</p>
                                        </div >
                                    }
                                    { this.props.collection.feat_count > 0 &&
                                        <div>
                                            <i className="far fa-arrow-alt-circle-up"></i>
                                            <p>{this.props.collection.feat_count} Feats</p>
                                        </div >
                                    }
                                </div>
                            </Col>
                        </Row>
                    </Collapse>
                </Col>
                
            </Row>
        )

    };

}

const ListContentCollections = ({ content_collections, refresh_function, display_type}) => {

    const player = useContext(PlayerContext);

    function delete_collection_function(id){
        DND_DELETE(
            '/content-collection/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    function edit_collection_function(id, data){
        DND_PUT(
            '/content-collection/' + id,
            data,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    function delete_user_collection_function(id){
        DND_DELETE(
            '/user-content-collection/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    function add_user_collection_function(id){
        DND_POST(
            '/user-content-collection/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(content_collections === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="content-collection-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={(player.user.user_type == "TYPE_ADMIN" && display_type == "edit") ? 2 : 4} md={(player.user.user_type == "TYPE_ADMIN" && display_type == "edit") ? 2 : 4} className="cenetered-column">
                        <p><b>Name</b></p>
                    </Col>
                    {display_type == "edit" &&
                        <Col xs={2} md={2} className="cenetered-column">
                            <p><b>Is Shareable</b></p>
                        </Col>
                    }
                    {player.user.user_type == "TYPE_ADMIN" && display_type == "edit" &&
                        <Col xs={2} md={2} className="cenetered-column">
                            <p><b>Is Default</b></p>
                        </Col>
                    }
                    <Col xs={2} md={2} className="cenetered-column">
                        <p><b>Created By</b></p>
                    </Col>
                    {display_type == "read" &&
                        <Col xs={2} md={2} className="cenetered-column">
                        </Col>
                    }
                    <Col xs={2} md={2}>
                    </Col>
                    <Col xs={2} md={2}>
                    </Col>
                </Row>
                <hr/>
            </div>
            {content_collections.map((content_collection) => (
                <div key={content_collection.id} className="content-collection-item">
                        <ContentCollectionItem
                            collection={content_collection}
                            delete_collection_function={delete_collection_function}
                            edit_collection_function={edit_collection_function}
                            delete_user_collection_function={delete_user_collection_function}
                            add_user_collection_function={add_user_collection_function}
                            display_type={display_type}
                        />
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListContentCollections;