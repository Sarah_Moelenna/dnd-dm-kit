import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'

class FilterContentCollection extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="content-collection-filters">
                <Form.Group controlId="formBasicName">
                    <Form.Label>Content Collection Name</Form.Label>
                    <Form.Control value={this.props.filters["name"]} name="name" type="text" placeholder="Search Content Collection Names" onChange={this.handleChange}/>
                </Form.Group>
            </Form>
        );
    };
}

export default FilterContentCollection;