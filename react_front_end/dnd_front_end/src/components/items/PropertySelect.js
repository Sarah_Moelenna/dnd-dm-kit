import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { ItemProperties } from '../monster/Data';

const ListItems = ({ properties, select_function, disabled_ids}) => {

    return (
        <div className="property-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={8} md={8}>
                        <p className="card-text"><b>Description</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                    </Col>
                </Row>
            </div>
            {properties.map((item) => (
                <div key={item.id} className={disabled_ids.includes(item.id) ? "property-item disabled" : "property-item"}>
                    <Row>
                        <Col xs={2} md={2}>
                            <p className="card-text">{item.name}</p>
                        </Col>
                        <Col xs={8} md={8}>
                            <p className="card-text">{item.description}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            {!disabled_ids.includes(item.id) &&
                                <Button onClick={() => {select_function(item)}}>Select Property</Button>
                            }
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

class PropertySelect extends Component {

    state = {
        toggle: false
    }

    select_property = (property) => {

        this.setState({toggle: false})
        this.props.callback(property)
    }

    open = () => {
        this.setState({toggle: true})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>{this.props.override_button != undefined ? this.props.override_button : "Choose Property"}</Button>
                }
                <Modal show={this.state.toggle} size="md" className="property-select-modal select-modal">
                        <Modal.Header>Properties</Modal.Header>
                        <Modal.Body>
                            <ListItems
                                properties={ItemProperties}
                                select_function={this.select_property}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default PropertySelect;