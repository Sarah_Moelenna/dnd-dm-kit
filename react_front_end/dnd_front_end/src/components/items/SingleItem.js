import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ContentCollectionResource from '../shared/ContentCollectionResource';
import Markdown from 'react-markdown'
import { Link } from 'react-router-dom';

export class ItemDetails extends Component {
    render(){

        return(
            <Row>
                <Col xs={8} >
                    <div className="type-rarity">
                        <span>{this.props.item.type}</span>
                        {this.props.item.rarity != undefined &&this.props.item.rarity != null &&
                            <span>{this.props.item.rarity}</span>
                        }
                    </div>
                    <hr/>
                    <Markdown children={this.props.item.description}/>
                    {this.props.item.Tags != undefined &&this.props.item.Tags != null &&
                        <div className="tags">
                            <p><b>Tags:</b> {this.props.item.tags.replaceAll(",", ", ")}</p>
                        </div>
                    }
                </Col>
                <Col xs={4}>
                    {this.props.item.large_avatar_url != undefined && this.props.item.large_avatar_url != null &&
                        <img src={this.props.item.large_avatar_url}/>
                    }
                </Col>
            </Row>
        )

    };
}

export class SingleItem extends Component {
            
    render(){

                return(
                    <div className="item">
                        <h2>
                            <Link to="/item" onClick={this.props.close_item_fuction}>
                                <i className="fas fa-chevron-left clickable-icon"></i>
                            </Link >
                            {this.props.item.name}
                        </h2>
                        <ItemDetails item={this.props.item}/>
                        <ContentCollectionResource resource='item' resource_id={this.props.item.id}/>
                    </div>
                )

    };
}