import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import MDEditor from '@uiw/react-md-editor';
import { ItemTags,ItemRarity, ItemBaseType, ItemType, ItemArmourDexBonus, ItemArmourStealthCheck, ItemResetTypes, ItemArmourType, DiceValues, AttackRanges, ItemSubTypes, DamageTypes } from '../monster/Data';
import { ListModifiers, createModifier } from '../shared/Modifier';
import { DND_GET, DND_PUT, DND_POST } from '.././shared/DNDRequests';
import ItemSelect from './ItemSelect';
import ListSelector from '../shared/ListSelector';
import Button from 'react-bootstrap/Button';
import UploadImage from '../images/UploadImage';
import PropertySelect from './PropertySelect';
import BackConfirmationButton from '../shared/BackConfirmationButton';

class SingleItem extends Component {

    state = {
        base_type: null,
        dex_bonus: null,
        can_equip: false,
        magic: true,
        name: null,
        snippet: null,
        weight: 0,
        type: null,
        description: null,
        can_attune: false,
        attunement_description: null,
        rarity: null,
        stackable: false,
        bundle_size: 1,
        avatar_url: null,
        large_avatar_url: null,
        cost: null,
        tags: [],
        sub_type: null,
        is_consumable: false,
        weapon_behaviors: null,
        base_item_id: null,
        base_armor_name: null,
        strength_requirement: null,
        armor_class: null,
        stealth_check: null,
        damage: null,
        damage_type: null,
        fixed_damage: null,
        properties: [],
        attack_type: null,
        category_id: null,
        range: null,
        long_range: null,
        is_monk_weapon: false,
        armor_type_id: null,
        gear_type_id: null,
        grouped_id: null,
        can_be_added_to_inventory: true,
        parent_id: null,
        modifiers: [],
        has_charges: false,
        charge_number: 0,
        charge_reset: null,
        charge_reset_description: null,
        is_simple: true,
        bonus_to_hit: 0,
        bonus_to_damage: 0,

        item_options: {weapons: [], armours: []},

        has_changes: false,
    }

    componentWillMount() {
        this.load_options()
        if(this.props.edit_item_id != undefined && this.props.edit_item_id != null){
            this.copy_item(this.props.edit_item_id)
        }
    };

    copy_item = (item_id) => {

        DND_GET(
            '/item/' + item_id,
            (response) => {
                this.populate_with_item_data(response)
            },
            null
        )
    };

    load_options = () => {

        DND_GET(
            '/item/options',
            (response) => {
                this.setState({item_options: response})
            },
            null
        )
    };

    populate_with_item_data = (item_data) => {
        var name = item_data.name
        if(this.props.edit_item_id == undefined || this.props.edit_item_id == null){
            name = "Copy of " + name
        }

        this.setState(
            {
                base_type: item_data.base_type,
                dex_bonus: item_data.dex_bonus,
                can_equip: item_data.can_equip,
                magic: item_data.magic,
                name: name,
                snippet: item_data.snippet,
                weight: item_data.weight,
                type: item_data.type,
                description: item_data.description,
                can_attune: item_data.can_attune,
                attunement_description: item_data.attunement_description,
                rarity: item_data.rarity,
                stackable: item_data.stackable,
                bundle_size: item_data.bundle_size,
                avatar_url: item_data.avatar_url,
                large_avatar_url: item_data.large_avatar_url,
                cost: item_data.cost,
                tags: item_data.tags != null ? item_data.tags : [],
                sub_type: item_data.sub_type,
                is_consumable: item_data.is_consumable,
                weapon_behaviors: item_data.weapon_behaviors,
                base_item_id: item_data.base_item_id,
                base_armor_name: item_data.base_armor_name,
                strength_requirement: item_data.strength_requirement,
                armor_class: item_data.armor_class,
                stealth_check: item_data.stealth_check,
                damage: item_data.damage,
                damage_type: item_data.damage_type,
                fixed_damage: item_data.fixed_damage,
                properties: item_data.properties != null ? item_data.properties : [],
                attack_type: item_data.attack_type,
                category_id: item_data.category_id,
                range: item_data.range,
                long_range: item_data.long_range,
                is_monk_weapon: item_data.is_monk_weapon,
                armor_type_id: item_data.armor_type_id,
                gear_type_id: item_data.gear_type_id,
                grouped_id: item_data.grouped_id,
                can_be_added_to_inventory: item_data.can_be_added_to_inventory,
                modifiers: item_data.modifiers,
                parent_id: item_data.parent_id,
                has_charges: item_data.has_charges,
                charge_number: item_data.charge_number,
                charge_reset: item_data.charge_reset,
                charge_reset_description: item_data.charge_reset_description,
                is_simple: item_data.is_simple,
                bonus_to_hit: item_data.bonus_to_hit,
                bonus_to_damage: item_data.bonus_to_damage
            }
        )
    }

    save_item = () => {

        var final_item = {
            base_type: this.state.base_type,
            dex_bonus: this.state.dex_bonus,
            can_equip: this.state.can_equip,
            magic: this.state.magic,
            name: this.state.name,
            snippet: this.state.snippet,
            weight: this.state.weight,
            type: this.state.type,
            description: this.state.description,
            can_attune: this.state.can_attune,
            attunement_description: this.state.attunement_description,
            rarity: this.state.rarity,
            stackable: this.state.stackable,
            bundle_size: this.state.bundle_size,
            avatar_url: this.state.avatar_url,
            large_avatar_url: this.state.large_avatar_url,
            cost: this.state.cost,
            tags: this.state.tags,
            sub_type: this.state.sub_type,
            is_consumable: this.state.is_consumable,
            weapon_behaviors: this.state.weapon_behaviors,
            base_item_id: this.state.base_item_id,
            base_armor_name: this.state.base_armor_name,
            strength_requirement: this.state.strength_requirement,
            armor_class: this.state.armor_class,
            stealth_check: this.state.stealth_check,
            damage: this.state.damage,
            damage_type: this.state.damage_type,
            fixed_damage: this.state.fixed_damage,
            properties: this.state.properties,
            attack_type: this.state.attack_type,
            category_id: this.state.category_id,
            range: this.state.range,
            long_range: this.state.long_range,
            is_monk_weapon: this.state.is_monk_weapon,
            armor_type_id: this.state.armor_type_id,
            gear_type_id: this.state.gear_type_id,
            grouped_id: this.state.grouped_id,
            can_be_added_to_inventory: this.state.can_be_added_to_inventory,
            modifiers: this.state.modifiers,
            parent_id: this.state.parent_id,
            has_charges: this.state.has_charges,
            charge_number: this.state.charge_number,
            charge_reset: this.state.charge_reset,
            charge_reset_description: this.state.charge_reset_description,
            is_simple: this.state.is_simple,
            bonus_to_hit: this.state.bonus_to_hit,
            bonus_to_damage: this.state.bonus_to_damage
        }

        if(this.props.edit_item_id != undefined && this.props.edit_item_id != null){
            DND_PUT(
                '/item/' + this.props.edit_item_id,
                {data: final_item},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        } else {
            DND_POST(
                '/item',
                {data: final_item},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        }
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.setState({ [e.target.name]: new_value, has_changes: true });
    }

    update_text = (text) => {
        this.setState({description: text, has_changes: true})
    }

    update_is_magic = (value_in) => {
        var new_value = value_in
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.setState({
            base_type: null,
            type: null,
            base_armor_name: null,
            magic: new_value,
            sub_type: null,
            has_changes: true
        })
    }

    update_base_type = (base_type) => {
        
        if (base_type == "Weapon") {
            var new_type = this.state.magic == true ? null : this.state.name

            this.setState({
                base_type: base_type,
                type: new_type,
                base_armor_name: null,
                sub_type: null,
                has_changes: true
            })

        } else {
            this.setState({
                base_type: base_type,
                type: null,
                base_armor_name: null,
                sub_type: null,
                has_changes: true
            })
        }
    }

    update_image = (image, attribute) =>{
        this.setState({[attribute]: image, has_changes: true})
    }

    update_damage = (e) => {
        var new_value = e.target.value
        var new_damage = this.state.damage
        if(new_damage == null){
            new_damage = {}
        }

        if(e.target.name == 'diceCount' || e.target.name == 'diceValue' || e.target.name == 'fixedValue'){
            if(new_value == ''){
                new_value = null
            } else {
                new_value = parseInt(new_value)
            }
        }

        new_damage[e.target.name] = new_value
        this.setState({damage: new_damage, has_changes: true})
    }

    add_modifier = () => {
        var new_modifier = createModifier()
        var modifiers = this.state.modifiers
        modifiers.push(new_modifier)
        this.setState({modifiers: modifiers, has_changes: true})
    }

    update_modifier = (id, new_object) => {
        var modifiers = []
        for(var i = 0; i < this.state.modifiers.length; i++){
            var modifier = this.state.modifiers[i]
            if(modifier.id == id){
                modifiers.push(new_object)
            } else {
                modifiers.push(modifier)
            }
        }
        this.setState({modifiers: modifiers, has_changes: true})
    }

    delete_modifier = (id) => {
        var modifiers = []
        for(var i = 0; i < this.state.modifiers.length; i++){
            var modifier = this.state.modifiers[i]
            if(modifier.id != id){
                modifiers.push(modifier)
            }
        }
        this.setState({modifiers: modifiers, has_changes: true})
    }

    add_property = (property) => {
        var new_properties = this.state.properties
        new_properties.push(property)
        this.setState({properties: new_properties, has_changes: true})
    }

    remove_property = (property_id) => {
        var new_properties = []
        for(var i = 0; i < this.state.properties.length; i++){
            var property = this.state.properties[i]
            if(property.id != property_id){
                new_properties.push(property)
            }
        }
        this.setState({properties: new_properties, has_changes: true})
    }

    get_property_ids = () => {
        var ids = []
        for(var i = 0; i < this.state.properties.length; i++){
            var property = this.state.properties[i]
            ids.push(property.id)
        }
        return ids
    }
            
    render(){

        return(
            <div className="item">
                {(this.props.edit_item_id == undefined || this.props.edit_item_id == null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Create Item</h2>
                        <ItemSelect select_function={this.copy_item} override_button="Copy Existing Item"/>
                    </div>
                }
                {(this.props.edit_item_id != undefined && this.props.edit_item_id != null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Edit Item</h2>
                    </div>
                }
                <Row className="item-settings">
                    <Col xs={12} md={12}>
                        <h3>Basic Information</h3>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control value={this.state.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Is Magic?</Form.Label>
                            <Form.Control name="magic" as="select" onChange={(e) => {this.update_is_magic(e.target.value)}} custom>
                                    <option selected={this.state.magic == true} value={true}>Yes</option>
                                    <option selected={this.state.magic == false} value={false}>No</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicRarity">
                            <Form.Label>Rarity</Form.Label>
                            <Form.Control name="rarity" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    {ItemRarity.map((rarity) => (
                                        <option key={rarity} selected={this.state.rarity == rarity} value={rarity}>{rarity}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicRarity">
                            <Form.Label>Type</Form.Label>
                            <Form.Control name="base_type" as="select" onChange={(e) => {this.update_base_type(e.target.value)}} custom>
                                    <option selected="true" value="">-</option>
                                    {ItemBaseType.map((base_type) => (
                                        <option key={base_type} selected={this.state.base_type == base_type} value={base_type}>{base_type}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    {this.state.base_type != null && this.state.base_type != "" &&
                        <Col xs={12} md={12}>
                            <hr/>
                        </Col>
                    }

                    {this.state.base_type == "Item" &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Item Type</Form.Label>
                                <Form.Control
                                    name="type"
                                    as="select"
                                    onChange={this.handleChange}
                                    custom
                                    disabled={this.state.base_type != "Item"}
                                >
                                        <option selected="true" value="">-</option>
                                        {ItemType.map((type) => (
                                            <option key={type} selected={this.state.type == type} value={type}>{type}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Item" && this.state.type == "Gear" &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Item Type</Form.Label>
                                <Form.Control
                                    name="type"
                                    as="select"
                                    onChange={this.handleChange}
                                    custom
                                >
                                    <option selected="true" value="">-</option>
                                    {ItemSubTypes.map((sub_type) => (
                                        <option key={sub_type} selected={this.state.sub_type == sub_type} value={sub_type}>{sub_type}</option>
                                    ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Armour" &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Armour Type</Form.Label>
                                <Form.Control
                                    name="parent_id"
                                    as="select"
                                    onChange={this.handleChange}
                                    custom
                                    disabled={this.state.base_type != "Armour" || this.state.magic == false}
                                >
                                        <option selected="true" value="">-</option>
                                        {this.state.item_options.armours.map((armour) => (
                                            <option key={armour.id} selected={this.state.parent_id == armour.id && this.state.base_type == "Armour"} value={armour.id}>{armour.name}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Armour" &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Dex Bonus</Form.Label>
                                <Form.Control
                                    name="dex_bonus"
                                    as="select"
                                    onChange={this.handleChange}
                                    custom
                                    disabled={this.state.base_type != "Armour"}
                                >
                                        <option selected="true" value="">-</option>
                                        {ItemArmourDexBonus.map((dex_bonus) => (
                                            <option key={dex_bonus} selected={this.state.dex_bonus == dex_bonus} value={dex_bonus}>{dex_bonus}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Armour" &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>STR Requirement</Form.Label>
                                <Form.Control
                                    value={this.state.strength_requirement}
                                    name="strength_requirement"
                                    type="number"
                                    onChange={this.handleChange}
                                    disabled={this.state.base_type != "Armour"}
                                />
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Armour" &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Stealth Check</Form.Label>
                                <Form.Control
                                    name="stealth_check"
                                    as="select"
                                    onChange={this.handleChange}
                                    custom
                                    disabled={this.state.base_type != "Armour"}
                                >
                                        <option selected="true" value="">-</option>
                                        {Object.keys(ItemArmourStealthCheck).map((stealth_check) => (
                                            <option key={stealth_check} selected={this.state.stealth_check == stealth_check} value={stealth_check}>{ItemArmourStealthCheck[stealth_check]}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Armour" &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Armour Class</Form.Label>
                                <Form.Control
                                    value={this.state.armor_class}
                                    name="armor_class"
                                    type="number"
                                    onChange={this.handleChange}
                                    disabled={this.state.base_type != "Armour"}
                                />
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Armour" &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Type</Form.Label>
                                <Form.Control
                                    name="armour_type_id"
                                    as="select"
                                    onChange={this.handleChange}
                                    custom
                                    disabled={this.state.base_type != "Armour"}
                                >
                                        <option selected="true" value="">-</option>
                                        {Object.keys(ItemArmourType).map((armor_type_id) => (
                                            <option key={armor_type_id} selected={this.state.armor_type_id == armor_type_id} value={armor_type_id}>{ItemArmourType[armor_type_id]}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Weapon" && this.state.magic == true &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Base Weapon</Form.Label>
                                <Form.Control
                                    name="parent_id"
                                    as="select"
                                    onChange={this.handleChange}
                                    custom
                                    disabled={this.state.base_type != "Weapon" || this.state.magic == false}
                                >
                                        <option selected="true" value="">-</option>
                                        {this.state.item_options.weapons.map((weapon) => (
                                            <option key={weapon.id} selected={this.state.parent_id == weapon.id && this.state.base_type == "Weapon"} value={weapon.id}>{weapon.name}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Weapon" && this.state.magic == false &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicSize">
                                <Form.Label>Is Simple Weapon?</Form.Label>
                                <Form.Control name="is_simple" as="select" onChange={this.handleChange} custom>
                                        <option selected={this.state.is_simple == true} value={true}>Yes</option>
                                        <option selected={this.state.is_simple == false} value={false}>No</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Attack Range</Form.Label>
                                <Form.Control
                                    name="attack_type"
                                    as="select"
                                    onChange={this.handleChange}
                                    custom
                                >
                                        <option selected="true" value="">-</option>
                                        {Object.keys(AttackRanges).map((attack_type) => (
                                            <option key={attack_type} selected={this.state.attack_type == attack_type} value={attack_type}>{AttackRanges[attack_type]}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Range</Form.Label>
                                <Form.Control
                                    value={this.state.range}
                                    name="range"
                                    type="number"
                                    onChange={this.handleChange}
                                />
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Long Range</Form.Label>
                                <Form.Control
                                    value={this.state.long_range}
                                    name="long_range"
                                    type="number"
                                    onChange={this.handleChange}
                                />
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicSize">
                                <Form.Label>Is Monk Weapon?</Form.Label>
                                <Form.Control name="is_monk_weapon" as="select" onChange={this.handleChange} custom>
                                        <option selected={this.state.is_monk_weapon == true} value={true}>Yes</option>
                                        <option selected={this.state.is_monk_weapon == false || this.state.is_monk_weapon == null} value={false}>No</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={12} md={12}></Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicWis">
                                <Form.Label>Dice Count</Form.Label>
                                <Form.Control value={this.state.damage != null && this.state.damage.diceCount} required name="diceCount" type="number" onChange={this.update_damage} />
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicSize">
                                <Form.Label>Die Type</Form.Label>
                                <Form.Control name="diceValue" as="select" onChange={this.update_dice} custom>
                                        <option selected="true" value="">-</option>
                                        {DiceValues.map((dice_value) => (
                                            <option key={dice_value} selected={this.state.damage != null && this.state.damage.diceValue == dice_value} value={dice_value}>d{dice_value}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicWis">
                                <Form.Label>Fixed Value</Form.Label>
                                <Form.Control value={this.state.damage != null && this.state.damage.fixedValue} required name="fixedValue" type="number" onChange={this.update_damage} />
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={3} md={3}>
                            <Form.Group controlId="formBasicWis">
                                <Form.Label>Damage Type</Form.Label>
                                <Form.Control name="damage_type" as="select" onChange={this.handleChange} value={this.state.damage_type} custom>
                                        <option selected="true" value="">-</option>
                                        {Object.values(DamageTypes).map((damage_type) => (
                                            <option key={damage_type} value={damage_type}>{damage_type}</option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Weapon" && this.state.magic == true &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Bonus to Hit</Form.Label>
                                <Form.Control value={this.state.bonus_to_hit} required name="bonus_to_hit" type="number" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>
                    }

                    {this.state.base_type == "Weapon" && this.state.magic == true &&
                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicRarity">
                                <Form.Label>Bonus to Damage</Form.Label>
                                <Form.Control value={this.state.bonus_to_damage} required name="bonus_to_damage" type="number" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>
                    }

                    {(this.state.base_type == "Weapon" || (this.state.base_type == "Item" && this.state.type == "Staff")) &&
                        <Col xs={12} md={12}>
                            <Form.Group controlId="formBasicWis">
                                <Form.Label>Properties</Form.Label>
                                { this.state.properties.length > 0 &&
                                    <div className="property-list">
                                        <div className="headers">
                                            <Row>
                                                <Col xs={2} md={2}>
                                                    <p className="card-text"><b>Name</b></p>
                                                </Col>
                                                <Col xs={8} md={8}>
                                                    <p className="card-text"><b>Description</b></p>
                                                </Col>
                                                <Col xs={2} md={2}>
                                                </Col>
                                            </Row>
                                        </div>
                                        {this.state.properties.map((property) => (
                                            <div key={property.id} className="property-item">
                                                <Row>
                                                    <Col xs={2} md={2}>
                                                        <p className="card-text">{property.name}</p>
                                                    </Col>
                                                    <Col xs={8} md={8}>
                                                        <p className="card-text">{property.description}</p>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Button onClick={() => {this.remove_property(property.id)}}>Remove Property</Button>
                                                    </Col>
                                                </Row>
                                            </div>
                                        ))}
                                    </div>
                                }
                                <br/><br/>
                                <PropertySelect
                                    disabled_ids={this.get_property_ids()}
                                    callback={this.add_property}
                                />
                            </Form.Group>
                        </Col>
                    }

                    <Col xs={12} md={12}>
                        <hr/>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Requires Attunement</Form.Label>
                            <Form.Control name="can_attune" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.can_attune == true} value={true}>Yes</option>
                                    <option selected={this.state.can_attune == false} value={false}>No</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={8} md={8}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Attunement Description</Form.Label>
                            <Form.Control
                                value={this.state.attunement_description}
                                name="attunement_description"
                                type="text"
                                placeholder="Enter Attunement Description"
                                onChange={this.handleChange}
                                disabled={this.state.can_attune == false}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Snippet</Form.Label>
                            <Form.Control
                                value={this.state.snippet}
                                name="snippet"
                                type="text"
                                placeholder="Enter Snippet"
                                onChange={this.handleChange}
                            />
                        </Form.Group>
                    </Col>
                
                    <Col xs={12} md={12}>
                        <Form.Label>Description</Form.Label>
                        <MDEditor
                            value={this.state.description}
                            preview="edit"
                            onChange={this.update_text}
                        />
                        <br/>
                    </Col>


                    <Col xs={12} md={12}>
                        <hr/>
                    </Col>
                    <Col xs={12} md={12}>
                        <h3>Additional Information</h3>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Has Charges?</Form.Label>
                            <Form.Control name="has_charges" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.has_charges == true} value={true}>Yes</option>
                                    <option selected={this.state.has_charges == false} value={false}>No</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>No. Charges</Form.Label>
                            <Form.Control
                                value={this.state.charge_number}
                                name="charge_number"
                                type="number"
                                min={0}
                                onChange={this.handleChange}
                                disabled={this.state.has_charges == false}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicRarity">
                            <Form.Label>Charge Reset</Form.Label>
                            <Form.Control
                                name="charge_reset"
                                as="select"
                                onChange={this.handleChange}
                                custom
                                disabled={this.state.has_charges == false}
                            >
                                <option selected="true" value="">-</option>
                                {ItemResetTypes.map((charge_reset) => (
                                    <option key={charge_reset} selected={this.state.charge_reset == charge_reset} value={charge_reset}>{charge_reset}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Charge Reset Description</Form.Label>
                            <Form.Control
                                value={this.state.charge_reset_description}
                                name="snippet"
                                type="text"
                                placeholder="Enter Charge Reset Description"
                                onChange={this.handleChange}
                                disabled={this.state.has_charges == false}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={6} md={6}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Weight (lbs)</Form.Label>
                            <Form.Control
                                value={this.state.weight}
                                name="weight"
                                type="number"
                                min={0}
                                onChange={this.handleChange}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={6} md={6}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Cost (Gold)</Form.Label>
                            <Form.Control
                                value={this.state.cost}
                                name="cost"
                                type="number"
                                onChange={this.handleChange}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={6} md={6} className="item-avatar">
                        <Form.Label>Avatar</Form.Label>
                        <UploadImage callback={(image) => this.update_image(image, "avatar_url")} />
                        <img src={this.state.avatar_url}/>
                    </Col>

                    <Col xs={6} md={6} className="item-avatar">
                        <Form.Label>Large Avatar</Form.Label>
                        <UploadImage callback={(image) => this.update_image(image, "large_avatar_url")} />
                        <img src={this.state.large_avatar_url}/>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Can be Added To Inventory?</Form.Label>
                            <Form.Control name="can_be_added_to_inventory" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.can_be_added_to_inventory == true} value={true}>Yes</option>
                                    <option selected={this.state.can_be_added_to_inventory == false} value={false}>No</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Can Equip?</Form.Label>
                            <Form.Control name="can_equip" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.can_equip == true} value={true}>Yes</option>
                                    <option selected={this.state.can_equip == false} value={false}>No</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Is Consumable?</Form.Label>
                            <Form.Control name="is_consumable" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.is_consumable == true} value={true}>Yes</option>
                                    <option selected={this.state.is_consumable == false} value={false}>No</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Is Stackable?</Form.Label>
                            <Form.Control name="stackable" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.stackable == true} value={true}>Yes</option>
                                    <option selected={this.state.stackable == false} value={false}>No</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    {this.state.stackable == true &&
                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicWis">
                                <Form.Label>Max Stack Size</Form.Label>
                                <Form.Control value={this.state.bundle_size} required name="bundle_size" type="number" min={1} onChange={this.handleChange} />
                            </Form.Group>
                        </Col>
                    }

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicFly">
                            <Form.Label>Tags</Form.Label>
                            <ListSelector input_value={this.state.tags} items={ItemTags} onChange={(data) => {this.setState({tags: data, has_changes: true})}} />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.modifiers.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.modifiers.length} Modifiers
                                    <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListModifiers modifiers={this.state.modifiers} update_modifier={this.update_modifier} delete_modifier={this.delete_modifier} display_type="SPELL" scale_type={this.state.scale_type}/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.modifiers.length} Modifiers
                                    <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    <Col xs={12} md={12}>
                        <br/>
                        <Button variant="primary" type="submit" onClick={this.save_item}>
                            Save Item
                        </Button>
                    </Col>

                </Row>
            </div>
        )

    };
}

export default SingleItem;