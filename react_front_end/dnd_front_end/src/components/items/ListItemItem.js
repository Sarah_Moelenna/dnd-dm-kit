import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '.././shared/DNDRequests';
import { Link } from 'react-router-dom';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';


const ListItemItem = ({ items, view_item_function, refresh_function, edit_function}) => {

    function delete_function(id){
        DND_DELETE(
            '/item/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(items === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="item-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Rarity</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Type</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Sub Type</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Tags</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                    </Col>
                </Row>
            </div>
            {items.map((item) => (
                <div key={item.id} className="item-item">
                    <hr/>
                    <Row>
                        <Col xs={2} md={2}>
                        <p className="card-text">{item.name}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{item.rarity}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{item.type}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{item.sub_type}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            {item.tags &&
                                <p className="card-text">{item.tags.replaceAll(",", ", ")}</p>
                            }
                        </Col>
                        <Col xs={2} md={2}>
                            <Link className="btn btn-primary" to={"/item/" + item.id} onClick={() => view_item_function(item.id)}>
                                View
                            </Link >
                            { item.can_edit == true && 
                                <a className="btn btn-primary" onClick={() => edit_function(item.id)}>Edit</a>
                            }
                            { item.can_edit == true &&
                                <DeleteConfirmationButton id={item.id} override_button="Delete" name="Item" delete_function={delete_function} />
                            }
                        </Col>
                    </Row>
                </div>
            ))}
        </div>
    );
}

export default ListItemItem;