import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_GET } from '.././shared/DNDRequests';

class FilterItem extends Component {

    state = {
        filters: {
            rarity: [],
            type: [],
            sub_type: []
        }
    }

    componentWillMount() {
        this.load_filters()
    };

    load_filters = () => {
        DND_GET(
            '/item/filters',
            (response) => {
                this.setState({filters: response})
            },
            null
        )
    };

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="item_filters object-filter">
                <Row>
                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Item Name</Form.Label>
                            <Form.Control value={this.props.filters["name"] ? this.props.filters["name"] : ''} name="name" type="text" placeholder="Search Item Names" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicEnvironment">
                            <Form.Label>Is Magic?</Form.Label>
                            <Form.Control name="magic" as="select" onChange={this.handleChange} custom>
                                    <option selected="" value="">-</option>
                                    <option value={true} selected={this.props.filters["magic"] == true}>True</option>
                                    <option value={false} selected={this.props.filters["magic"] == false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Type</Form.Label>
                            <Form.Control name="type" as="select" onChange={this.handleChange} custom>
                                <option selected="true" value="">-</option>
                                {this.state.filters.type.map((type) => (
                                    <option key={type} selected={this.props.filters["type"] == type} value={type}>{type}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Sub Type</Form.Label>
                            <Form.Control name="sub_type" as="select" onChange={this.handleChange} custom>
                                <option selected="true" value="">-</option>
                                {this.state.filters.sub_type.map((sub_type) => (
                                    <option key={sub_type} selected={this.props.filters["sub_type"] == sub_type} value={sub_type}>{sub_type}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Rarity</Form.Label>
                            <Form.Control name="rarity" as="select" onChange={this.handleChange} custom>
                                <option selected="true" value="">-</option>
                                {this.state.filters.rarity.map((rarity) => (
                                    <option key={rarity} selected={this.props.filters["rarity"] == rarity} value={rarity}>{rarity}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Tags</Form.Label>
                            <Form.Control value={this.props.filters["tags"] ? this.props.filters["tags"] : ''} name="tags" type="text" placeholder="Search Item Tags" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicEnvironment">
                            <Form.Label>Homebrew</Form.Label>
                            <Form.Control name="homebrew" as="select" onChange={this.handleChange} custom>
                                    <option selected="" value="">-</option>
                                    <option value={true} selected={this.props.filters["homebrew"] == true}>True</option>
                                    <option value={false} selected={this.props.filters["homebrew"] == false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <a className="btn btn-primary" onClick={() => this.props.clear_filter_function()}>Clear Filters</a>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default FilterItem;