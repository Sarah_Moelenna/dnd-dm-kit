import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import FilterItem from './FilterItem';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const ListItems = ({ items, set_sort, current_sort_by, current_sort_value, select_function, disabled_ids}) => {

    function get_sort(sort_by){
        if(current_sort_by == sort_by){
            if(current_sort_value == "DESC"){
                return (
                    <div className="clickable-div" onClick={() => {set_sort(null, null)}}>
                        <i className="fas fa-sort-down"></i>
                    </div>
                ) 
            } else {
                return (
                    <div className="clickable-div" onClick={() => {set_sort(sort_by, "DESC")}}>
                        <i className="fas fa-sort-up"></i>
                    </div>
                ) 
            }
        } else {
            return (
                <div className="clickable-div" onClick={() => {set_sort(sort_by, "ASC")}}>
                    <i className="fas fa-sort"></i>
                </div>
            )
        }
    }

    return (
        <div className="item-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Rarity</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Type</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Sub Type</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Tags</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                    </Col>
                </Row>
            </div>
            {items.map((item) => (
                <div key={item.id} className={disabled_ids.includes(item.id) ? "item-item disabled" : "item-item"}>
                    <Row>
                        <Col xs={2} md={2}>
                            <p className="card-text">{item.name}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{item.rarity}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{item.type}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{item.sub_type}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            {item.tags &&
                                <p className="card-text">{item.tags.replaceAll(",", ", ")}</p>
                            }
                        </Col>
                        <Col xs={2} md={2}>
                            {!disabled_ids.includes(item.id) &&
                                <Button onClick={() => {select_function(item.id, item.name)}}>Select Item</Button>
                            }
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

class ItemSelect extends Component {

    state = {
        items: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false
    }

    componentDidMount() {
    };

    select_item = (item) => {

        this.setState({toggle: false})
        this.props.callback(item)
    }

    refresh_items = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        params['results_per_page'] = 10
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/item?' + stringify(params),
          (jsondata) => {
            this.setState({ items: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_items(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
        this.setState(
          {sort_by: sort_by, sort_value: sort_value},
          this.refresh_items(this.state.page, this.state.filters, sort_by, sort_value)
        )
      };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_items(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_items(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_items(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id, name) => {

        this.props.select_function(id, name)
        this.setState({toggle: false})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false && this.props.use_button != false &&
                    <Button onClick={this.open}>{this.props.override_button != undefined ? this.props.override_button : "Choose Item"}</Button>
                }
                {this.state.toggle == false && this.props.use_button == false &&
                    <span className="clickable-div add-button" onClick={this.open}>
                        <i className="fas fa-plus"></i>
                    </span>
                }
                <Modal show={this.state.toggle} size="md" className="item-select-modal select-modal">
                        <Modal.Header>Items</Modal.Header>
                        <Modal.Body>
                            <FilterItem filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                            <ListItems
                                items={this.state.items}
                                set_sort={this.set_sort}
                                current_sort_by={this.state.sort_by}
                                current_sort_value={this.state.sort_value}
                                select_function={this.select_function}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default ItemSelect;