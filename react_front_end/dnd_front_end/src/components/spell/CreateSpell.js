import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import ListToggleButtons from '../shared/ListToggleButtons';
import MDEditor from '@uiw/react-md-editor';
import { SpellActivationTypes, SpellAoETypes, SpellLevels, SpellRangeTypes, SpellSchools, SpellComponents, ModifierDurationOptions, SpellDurationType, ScaleTypes, AttributeID, SpellConditionType, ConditionID, SpellTags } from '../monster/Data';
import { ListModifiers, createModifier } from '../shared/Modifier';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton'
import { ListAtHigherLevel, createAtHigherLevel } from '../shared/AtHigherLevel'
import { DND_GET, DND_PUT, DND_POST } from '.././shared/DNDRequests';
import SpellSelect from './SpellSelect';
import ListSelector from '../shared/ListSelector';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import BackConfirmationButton from '../shared/BackConfirmationButton';

const createCondition = () => {
    return {
        "type": null,
        "conditionId": null,
        "conditionDuration": null,
        "durationUnit": null,
        "exception": null,
    }
}

class ConditionItem extends Component {

    state = {
        toggle: false,
    }

    update_attribute = (e) => {
        var new_condition = this.props.condition
        new_condition[e.target.name] = e.target.value
        this.props.update_condition(this.props.id, new_condition)
    }

    update_attribute_by_name = (name, value) => {
        var new_condition = this.props.condition
        new_condition[name] = value
        this.props.update_condition(this.props.id, new_condition)
    }

    render(){

        return(
            <Row key={this.props.id} className="condition-item">
                <Col xs={2} md={2}>
                    <p>{SpellConditionType[this.props.condition.type]}</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>{ConditionID[this.props.condition.conditionId]}</p>
                </Col>
                <Col xs={2} md={2}>
                    {this.props.condition.conditionDuration != null && this.props.condition.conditionDuration != 0 &&
                        <p>{this.props.condition.conditionDuration + " " + this.props.condition.durationUnit}</p>
                    }
                </Col>
                <Col xs={3} md={3}>
                    <p>{this.props.condition.exception}</p>
                </Col>
                <Col xs={1} md={1}>
                    <Button onClick={() => {this.setState({toggle: true})}}>Edit</Button>
                </Col>
                <Col xs={2} md={2}>
                    <DeleteConfirmationButton id={this.props.id} name="Condition" delete_function={this.props.delete_condition} override_button="Delete"/>
                </Col>
                <Col xs={12} md={12}>
                    <Modal show={this.state.toggle} size="md" className="select-modal">
                        <Modal.Header>Edit Condition</Modal.Header>
                        <Modal.Body>
                        <Row>
                            <Col xs={2} md={2}>
                                <Form.Group controlId="formBasicType">
                                <Form.Label>Effect</Form.Label>
                                    <Form.Control name="type" as="select" onChange={(e) => {this.update_attribute_by_name("type", e.target.value)}} custom>
                                        <option selected="true" value="">-</option>
                                        {Object.keys(SpellConditionType).map((type_id) => (
                                            <option key={type_id} selected={this.props.condition.type == type_id} value={type_id}>{SpellConditionType[type_id]}</option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            </Col>

                            <Col xs={2} md={2}>
                                <Form.Group controlId="formBasicType">
                                <Form.Label>Condition</Form.Label>
                                    <Form.Control name="conditionId" as="select" onChange={(e) => {this.update_attribute_by_name("conditionId", e.target.value)}} custom>
                                        <option selected="true" value="">-</option>
                                        {Object.keys(ConditionID).map((condition_id) => (
                                            <option key={condition_id} selected={this.props.condition.conditionId == condition_id} value={condition_id}>{ConditionID[condition_id]}</option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            </Col>

                            <Col xs={2} md={2}>
                                <Form.Group controlId="formBasicTime">
                                    <Form.Label>Condition Duration</Form.Label>
                                    <Form.Control value={this.state.activation_time} required name="conditionDuration" type="number" min={0} onChange={this.update_attribute} />
                                </Form.Group>
                            </Col>

                            <Col xs={2} md={2}>
                                <Form.Group controlId="formBasicType">
                                <Form.Label>{'\u00A0'}</Form.Label>
                                    <Form.Control name="durationUnit" as="select" onChange={(e) => {this.update_attribute_by_name("durationUnit", e.target.value)}} custom>
                                        <option selected="true" value="">-</option>
                                        {ModifierDurationOptions.map((unit) => (
                                            <option key={unit} selected={this.props.condition.durationUnit == unit} value={unit}>{unit}</option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            </Col>

                            <Col xs={12} md={12}>
                                <Form.Group controlId="formBasicName">
                                    <Form.Label>Exception</Form.Label>
                                    <Form.Control value={this.props.condition.exception} required name="exception" type="text" placeholder="" onChange={this.update_attribute} />
                                </Form.Group>
                            </Col>
                        </Row>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: false})}}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </Col>
            </Row>
        )
    }
}

const ListConditions = ({ conditions, update_condition, delete_condition}) => {

    return (
        <div className="condition-items">
            <Row>
                <Col xs={2} md={2}>
                    <p>TYPE</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>APPLICATON</p>
                </Col>
                <Col xs={2} md={2}>
                    <p>DURATION</p>
                </Col>
                <Col xs={3} md={3}>
                    <p>EXCEPTION</p>
                </Col>
                <Col xs={2} md={2}>
                </Col>
                <Col xs={1} md={1}>
                </Col>
            </Row>
            {conditions.map((condition, index) => (
                <ConditionItem condition={condition} update_condition={update_condition} delete_condition={delete_condition} id={index}/>
            ))}
        </div>
    );
}

class SingleSpell extends Component {

    state = {
        name: null,
        level: null,
        school: null,

        duration_interval: null,
        duration_unit: null,
        duration_type: null,

        activation_time: null,
        activation_type: null,

        range_type: null,
        range_value: null,
        aoe_type: null,
        aoe_value: null,

        as_part_of_weapon_attack: null,
        description: null,
        snippet: null,
        concentration: null,
        ritual: null,
        components: [],
        components_description: null,
        save_dc_ability_id: null,
        attack_type: null,
        can_cast_at_higher_level: null,
        at_higher_levels: [],
        conditions: [],
        casting_time_description: null,
        tags: [],
        scale_type: null,
        modifiers: [],

        has_changes: false,
    }

    componentWillMount() {
        if(this.props.edit_spell_id != undefined && this.props.edit_spell_id != null){
            this.copy_spell(this.props.edit_spell_id)
        }
    };

    copy_spell = (spell_id) => {

        DND_GET(
            '/spell/' + spell_id,
            (response) => {
                this.populate_with_spell_data(response)
            },
            null
        )
    };

    populate_with_spell_data = (spell_data) => {
        var name = spell_data.name
        if(this.props.edit_spell_id == undefined || this.props.edit_spell_id == null){
            name = "Copy of " + name
        }

        this.setState(
            {
                name: name,
                level: spell_data.level,
                school: spell_data.school,
                duration_interval: spell_data.duration.durationInterval,
                duration_unit: spell_data.duration.durationUnit,
                duration_type: spell_data.duration.durationType,
                activation_time: spell_data.activation.activationTime,
                activation_type: spell_data.activation.activationType,
                range_type: spell_data.range.origin,
                range_value: spell_data.range.rangeValue,
                aoe_type: spell_data.range.aoeType,
                aoe_value: spell_data.range.aoeValue,
                as_part_of_weapon_attack: spell_data.as_part_of_weapon_attack,
                description: spell_data.description,
                snippet: spell_data.snippet,
                concentration: spell_data.concentration,
                ritual: spell_data.ritual,
                components: spell_data.components,
                components_description: spell_data.components_description,
                save_dc_ability_id: spell_data.save_dc_ability_id,
                attack_type: spell_data.attack_type,
                can_cast_at_higher_level: spell_data.can_cast_at_higher_level,
                at_higher_levels: spell_data.at_higher_levels,
                conditions: spell_data.conditions != null ? spell_data.conditions: [],
                casting_time_description: spell_data.casting_time_description,
                tags: spell_data.tags != null ? spell_data.tags : [],
                scale_type: spell_data.scale_type,
                modifiers: spell_data.modifiers
            }
        )
    }

    save_spell = () => {

        var final_spell = {
            name: this.state.name,
            level: this.state.level,
            school: this.state.school,
            duration: JSON.stringify({
                durationInterval: this.state.duration_interval,
                durationUnit: this.state.duration_unit,
                durationType: this.state.duration_type
            }),
            activation: JSON.stringify({
                activationTime: this.state.activation_time,
                activationType: this.state.activation_type,
            }),
            range: JSON.stringify({
                origin: this.state.range_type,
                rangeValue: this.state.range_value,
                aoeType: this.state.aoe_type,
                aoeValue: this.state.aoe_value
            }),
            as_part_of_weapon_attack: this.state.as_part_of_weapon_attack,
            description: this.state.description,
            snippet: this.state.snippet,
            concentration: this.state.concentration,
            ritual: this.state.ritual,
            components: JSON.stringify(this.state.components),
            components_description: this.state.components_description,
            save_dc_ability_id: this.state.save_dc_ability_id,
            attack_type: this.state.attack_type,
            can_cast_at_higher_level: this.state.can_cast_at_higher_level,
            requires_saving_throw: this.state.save_dc_ability_id ? true : false,
            requires_attack_roll: this.state.attack_type ? true : false,
            at_higher_levels: JSON.stringify(this.state.at_higher_levels),
            conditions: JSON.stringify(this.state.conditions),
            casting_time_description: this.state.casting_time_description,
            tags: JSON.stringify(this.state.tags),
            scale_type: this.state.scale_type,
            modifiers: this.state.modifiers
        }

        if(this.props.edit_spell_id != undefined && this.props.edit_spell_id != null){
            DND_PUT(
                '/spell/' + this.props.edit_spell_id,
                {data: final_spell},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        } else {
            DND_POST(
                '/spell',
                {data: final_spell},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        }
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        }
        if(new_value == ""){
            new_value= null
        }
        this.setState({ [e.target.name]: new_value, has_changes: true });
    }

    update_text = (text) => {
        this.setState({description: text, has_changes: true})
    }

    add_condition = () => {
        var new_condition = createCondition()
        var conditions = this.state.conditions
        conditions.push(new_condition)
        this.setState({conditions: conditions, has_changes: true})
    }

    update_condition = (index, new_object) => {
        var conditions = this.state.conditions
        conditions[index] = new_object
        this.setState({conditions: conditions, has_changes: true})
    }

    delete_condition = (index) => {
        var conditions = this.state.conditions
        conditions.splice(index, 1)
        this.setState({conditions: conditions, has_changes: true})
    }

    add_modifier = () => {
        var new_modifier = createModifier()
        var modifiers = this.state.modifiers
        modifiers.push(new_modifier)
        this.setState({modifiers: modifiers, has_changes: true})
    }

    update_modifier = (id, new_object) => {
        var modifiers = []
        for(var i = 0; i < this.state.modifiers.length; i++){
            var modifier = this.state.modifiers[i]
            if(modifier.id == id){
                modifiers.push(new_object)
            } else {
                modifiers.push(modifier)
            }
        }
        this.setState({modifiers: modifiers, has_changes: true})
    }

    delete_modifier = (id) => {
        var modifiers = []
        for(var i = 0; i < this.state.modifiers.length; i++){
            var modifier = this.state.modifiers[i]
            if(modifier.id != id){
                modifiers.push(modifier)
            }
        }
        this.setState({modifiers: modifiers, has_changes: true})
    }

    add_at_higher_level = () => {
        var at_higher_levels = this.state.at_higher_levels
        at_higher_levels.push(createAtHigherLevel())
        this.setState({at_higher_levels: at_higher_levels, has_changes: true})
    }

    update_at_higher_level = (index, new_object) => {
        var at_higher_levels = this.state.at_higher_levels
        at_higher_levels[index] = new_object
        this.setState({at_higher_levels: at_higher_levels, has_changes: true})
    }

    delete_at_higher_level = (index) => {
        var at_higher_levels = this.state.at_higher_levels
        at_higher_levels.splice(index, 1)
        this.setState({at_higher_levels: at_higher_levels, has_changes: true})
    }
            
    render(){

        return(
            <div className="spell">
                {(this.props.edit_spell_id == undefined || this.props.edit_spell_id == null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Create Spell</h2>
                        <SpellSelect select_function={this.copy_spell} override_button="Copy Existing Spell"/>
                    </div>
                }
                {(this.props.edit_spell_id != undefined && this.props.edit_spell_id != null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Edit Spell</h2>
                    </div>
                }
                <Row className="spell-settings">
                    <Col xs={12} md={12}>
                        <h3>Basic Information</h3>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control value={this.state.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>
                

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicLevel">
                            <Form.Label>Spell Level</Form.Label>
                            <Form.Control name="spellLevel" as="select" onChange={(e) => {this.setState({level: e.target.value, has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {Object.keys(SpellLevels).map((spell_level_display) => (
                                        <option key={SpellLevels[spell_level_display]} selected={this.state.level == SpellLevels[spell_level_display]} value={SpellLevels[spell_level_display]}>{spell_level_display}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSchool">
                            <Form.Label>Spell School</Form.Label>
                            <Form.Control name="spellSchool" as="select" onChange={(e) => {this.setState({school: e.target.value, has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {SpellSchools.map((spell_school) => (
                                        <option key={spell_school} selected={this.state.school == spell_school} value={spell_school}>{spell_school}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>


                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicTime">
                            <Form.Label>Casting Time</Form.Label>
                            <Form.Control value={this.state.activation_time} required name="activation_time" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicType">
                        <Form.Label>{'\u00A0'}</Form.Label>
                            <Form.Control name="activation_type" as="select" onChange={(e) => {this.setState({activation_type: parseInt(e.target.value), has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {Object.keys(SpellActivationTypes).map((activation_type_display) => (
                                        <option key={SpellActivationTypes[activation_type_display]} selected={this.state.activation_type == SpellActivationTypes[activation_type_display]} value={SpellActivationTypes[activation_type_display]}>{activation_type_display}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={8} md={8}>
                        <Form.Group controlId="formBasicTime">
                            <Form.Label>Reaction Casting Time Description</Form.Label>
                            <Form.Control value={this.state.casting_time_description} disabled={this.state.activation_type != 4} name="casting_time_description" type="text" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicTime">
                            <Form.Label>Components</Form.Label>
                            <ListToggleButtons values={this.state.components} item_dict={SpellComponents} on_change={(value) => {this.setState({components: value, has_changes: true})}} />
                        </Form.Group>
                    </Col>

                    <Col xs={10} md={10}>
                        <Form.Group controlId="formBasicTime">
                            <Form.Label>Material Components Description</Form.Label>
                            <Form.Control value={this.state.components_description} disabled={this.state.components.includes(3) == false} name="components_description" type="text" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSchool">
                            <Form.Label>Range Type</Form.Label>
                            <Form.Control name="spellRange" as="select" onChange={(e) => {this.setState({range_type: e.target.value, has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {SpellRangeTypes.map((spell_range) => (
                                        <option key={spell_range} selected={this.state.range_type == spell_range} value={spell_range}>{spell_range}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicTime">
                            <Form.Label>Range Distance</Form.Label>
                            <Form.Control value={this.state.range_value} disabled={this.state.range_type != "Ranged"} name="range_value" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSchool">
                            <Form.Label>Duration Type</Form.Label>
                            <Form.Control name="spellDuration" as="select" onChange={(e) => {this.setState({duration_type: e.target.value, has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {SpellDurationType.map((spell_duration) => (
                                        <option key={spell_duration} selected={this.state.duration_type == spell_duration} value={spell_duration}>{spell_duration}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicTime">
                            <Form.Label>Duration</Form.Label>
                            <Form.Control 
                                value={this.state.duration_interval}
                                disabled={(this.state.duration_type != "Concentration" && this.state.duration_type != "Time" && this.state.duration_type != "Special") == true} 
                                name="duration_interval" 
                                type="number" 
                                min={0} 
                                onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicType">
                        <Form.Label>{'\u00A0'}</Form.Label>
                            <Form.Control 
                                disabled={(this.state.duration_type != "Concentration" && this.state.duration_type != "Time" && this.state.duration_type != "Special") == true} 
                                name="duration_unit" 
                                as="select" 
                                onChange={(e) => {this.setState({duration_unit: e.target.value, has_changes: true})}} 
                                custom
                            >
                                    <option selected="true" value="">-</option>
                                    {ModifierDurationOptions.map((duration_unit) => (
                                        <option key={duration_unit} selected={this.state.duration_unit == duration_unit} value={duration_unit}>{duration_unit}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Label>Description</Form.Label>
                        <MDEditor
                            value={this.state.description}
                            preview="edit"
                            onChange={this.update_text}
                        />
                        <br/>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicFly">
                            <Form.Label>TAGS</Form.Label>
                            <ListSelector input_value={this.state.tags} items={SpellTags} onChange={(data) => {this.setState({tags: data, has_changes: true})}} />
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Is Ritual Spell</Form.Label>
                            <Form.Control name="ritual" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.ritual == true} value={true}>True</option>
                                    <option selected={this.state.ritual == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Should Scale At Higher Levels</Form.Label>
                            <Form.Control name="can_cast_at_higher_level" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.can_cast_at_higher_level == true} value={true}>True</option>
                                    <option selected={this.state.can_cast_at_higher_level == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicType">
                        <Form.Label>Higher Level Scaling Type</Form.Label>
                            <Form.Control 
                                disabled={this.state.can_cast_at_higher_level != true} 
                                name="scale_type" 
                                as="select" 
                                onChange={(e) => {this.setState({scale_type: e.target.value, has_changes: true})}} 
                                custom
                            >
                                    <option selected="true" value="">-</option>
                                    {Object.keys(ScaleTypes).map((scale_type) => (
                                        <option key={scale_type} selected={this.state.scale_type == scale_type} value={scale_type}>{ScaleTypes[scale_type]}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <hr/>
                    </Col>

                    <Col xs={12} md={12}>
                        <h3>Additional Information</h3>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSchool">
                            <Form.Label>AoE Type</Form.Label>
                            <Form.Control name="spellAoe" as="select" onChange={(e) => {this.setState({aoe_type: e.target.value, has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {SpellAoETypes.map((spell_aoe) => (
                                        <option key={spell_aoe} selected={this.state.aoe_type == spell_aoe} value={spell_aoe}>{spell_aoe}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicTime">
                            <Form.Label>AoE Size</Form.Label>
                            <Form.Control value={this.state.aoe_value} disabled={(this.state.aoe_type == null || this.state.aoe_type == "")} name="aoe_value" type="number" min={0} onChange={this.handleChange} />
                        </Form.Group>
                    </Col>
                    
                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSchool">
                            <Form.Label>Save Type</Form.Label>
                            <Form.Control name="spellSave" as="select" onChange={(e) => {this.setState({save_dc_ability_id: e.target.value, has_changes: true})}} custom>
                                    <option selected="true" value="">-</option>
                                    {Object.keys(AttributeID).map((attribute_id) => (
                                        <option key={attribute_id} selected={this.state.save_dc_ability_id == attribute_id} value={attribute_id}>{AttributeID[attribute_id]}</option>
                                    ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={6} md={6}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>As Part Of Weapon Attack</Form.Label>
                            <Form.Control name="as_part_of_weapon_attack" as="select" onChange={this.handleChange} custom>
                                    <option selected={this.state.as_part_of_weapon_attack == true} value={true}>True</option>
                                    <option selected={this.state.as_part_of_weapon_attack == false} value={false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={6} md={6}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Attack Type</Form.Label>
                            <Form.Control name="attack_type" as="select" onChange={this.handleChange} custom>
                                    <option selected="true" value="">-</option>
                                    <option selected={this.state.attack_type == 1} value={1}>Melee</option>
                                    <option selected={this.state.attack_type == 2} value={2}>Ranged</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.modifiers.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.modifiers.length} Modifiers
                                    <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListModifiers modifiers={this.state.modifiers} update_modifier={this.update_modifier} delete_modifier={this.delete_modifier} display_type="SPELL" scale_type={this.state.scale_type}/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.modifiers.length} Modifiers
                                    <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.conditions.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.conditions.length} Conditions
                                    <span className="clickable-div add-button" onClick={() => {this.add_condition()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListConditions conditions={this.state.conditions} update_condition={this.update_condition} delete_condition={this.delete_condition}/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.conditions.length} Conditions
                                    <span className="clickable-div add-button" onClick={() => {this.add_condition()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.at_higher_levels.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.at_higher_levels.length} At Higher Level
                                    <span className="clickable-div add-button" onClick={() => {this.add_at_higher_level()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListAtHigherLevel
                                    items={this.state.at_higher_levels}
                                    update_function={this.update_at_higher_level}
                                    delete_function={this.delete_at_higher_level}
                                    scale_type={this.state.scale_type}
                                />
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.at_higher_levels.length} At Higher Level
                                    <span className="clickable-div add-button" onClick={() => {this.add_at_higher_level()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    <Col xs={12} md={12}>
                        <br/>
                        <Button variant="primary" type="submit" onClick={this.save_spell}>
                            Save Spell
                        </Button>
                    </Col>

                </Row>
            </div>
        )

    };
}

export default SingleSpell;