import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'
import { SpellLevels, SpellSchools } from '../monster/Data';

class FilterSpell extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="spell-filters">
                <Row>
                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Spell Name</Form.Label>
                            <Form.Control value={this.props.filters["name"]} name="name" type="text" placeholder="Search Spell Names" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Spell School</Form.Label>
                            <Form.Control name="school" as="select" onChange={this.handleChange} custom>
                                <option selected="true" value="">-</option>
                                {SpellSchools.map((spell_school) => (
                                    <option key={spell_school} selected={this.props.filters["school"] == spell_school} value={spell_school}>{spell_school}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Spell Level</Form.Label>
                            <Form.Control name="level" as="select" onChange={this.handleChange} custom>
                                <option selected="true" value="">-</option>
                                {Object.keys(SpellLevels).map((spell_level_display) => (
                                    <option key={SpellLevels[spell_level_display]} selected={this.props.filters["level"] == SpellLevels[spell_level_display]} value={SpellLevels[spell_level_display]}>{spell_level_display}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <a className="btn btn-primary" onClick={() => this.props.clear_filter_function()}>Clear Filters</a>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default FilterSpell;