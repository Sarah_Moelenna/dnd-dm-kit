import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { SpellLevels } from '../monster/Data';

const ListSpellItem = ({ spells, add_spell_function, button_text, disabled_ids}) => {

    function get_spell_level(value){
        var names = Object.keys(SpellLevels)
        for(var i = 0; i < names.length; i++){
            if(SpellLevels[names[i]] == value){
                return names[i]
            }
        }
    }

    function is_disabled(id){
        if(disabled_ids == undefined){
            return false
        }
        return disabled_ids.includes(id)
    }

    if(spells === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="spell-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Spell School</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Spell Level</b></p>
                    </Col>
                    <Col xs={4} md={4}>
                        
                    </Col>
                </Row>
                <hr/>
            </div>
            {spells.map((spell) => (
                <div key={spell.id} className={is_disabled(spell.id) ? "spell-item disabled" : "spell-item"}>
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{spell.name}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <p className="card-text">{spell.school}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{get_spell_level(spell.level)}</p>
                        </Col>
                        <Col xs={4} md={4}>
                            {is_disabled(spell.id) == false &&
                                <a className="btn btn-primary" onClick={() => add_spell_function(spell.id, spell.name)}>{button_text == undefined ? "Add Spell" : button_text}</a>
                            }
                            <a className="btn btn-primary" target="_blank" href={"/spell/" + spell.id}>View</a>
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListSpellItem;