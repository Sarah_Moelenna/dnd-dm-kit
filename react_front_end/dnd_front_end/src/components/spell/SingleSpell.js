import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Markdown from 'react-markdown';
import ContentCollectionResource from '../shared/ContentCollectionResource';

import { SpellLevels, SpellActivationTypes, SpellComponents, AttributeID, SpellTags } from '../monster/Data';

class SingleSpell extends Component {

    get_spell_level = (value) => {
        var names = Object.keys(SpellLevels)
        for(var i = 0; i < names.length; i++){
            if(SpellLevels[names[i]] == value){
                return names[i]
            }
        }
    }

    get_spell_component = (value) => {
        var name = ""

        var names = Object.keys(SpellComponents)
        for(var i = 0; i < names.length; i++){
            if(SpellComponents[names[i]] == value){
                name = names[i]
            }
        }

        if(value == 3 && this.props.spell.components_description != null && this.props.spell.components_description != ""){
            name = name + " *"
        }

        return name
    }

    get_spell_casting_type = (value) => {
        var names = Object.keys(SpellActivationTypes)
        for(var i = 0; i < names.length; i++){
            if(SpellActivationTypes[names[i]] == value){
                return names[i]
            }
        }
    }

    get_spell_range_and_area = () => {
        var string = ''

       if(this.props.spell.range != undefined && this.props.spell.range != null){
            if(this.props.spell.range.origin == "Ranged"){
                string = string + " " + this.props.spell.range.rangeValue + " ft"
            } else {
                string = string + " " + this.props.spell.range.origin
            }
            if(this.props.spell.range.aoeType != null){
                string = string + " (" + this.props.spell.range.aoeType + " - " + this.props.spell.range.aoeValue + " ft)"
            }
       }

       return string
    }
            
    render(){

                return(
                    <div className="spell-data">
                        {this.props.mini_view != true &&
                            <h2>
                                <Link to="/spell" onClick={this.props.close_spell_fuction}>
                                    <i className="fas fa-chevron-left clickable-icon"></i>
                                </Link >
                                {this.props.spell.name}
                            </h2>
                        }

                        <Row>
                            <Col xs={3} md={3}>
                                <h3>LEVEL</h3>
                                <p>{this.get_spell_level(this.props.spell.level)}</p>
                            </Col>

                            <Col xs={3} md={3}>
                                <h3>CASTING TIME</h3>
                                <p>{this.props.spell.activation.activationTime != undefined ? this.props.spell.activation.activationTime + " " : ""}{this.get_spell_casting_type(this.props.spell.activation.activationType)}</p>
                            </Col>

                            <Col xs={3} md={3}>
                                <h3>RANGE/AREA</h3>
                                <p>{this.get_spell_range_and_area()}</p>
                            </Col>

                            <Col xs={3} md={3} className="components">
                                <h3>COMPONENTS</h3>
                                <p>
                                    {this.props.spell.components.map((component_value, index) => (
                                        <span>{this.get_spell_component(component_value)}{index == this.props.spell.components.length - 1 ? "": ","}</span>
                                    ))}
                                </p>
                            </Col>

                            <Col xs={3} md={3}>
                                <h3>DURATION</h3>
                                <p>{this.props.spell.duration.durationInterval != null && this.props.spell.duration.durationInterval != "" ? this.props.spell.duration.durationInterval + " ": ""}{this.props.spell.duration.durationUnit}</p>
                            </Col>

                            <Col xs={3} md={3}>
                                <h3>School</h3>
                                <p>{this.props.spell.school}</p>
                            </Col>

                            <Col xs={3} md={3}>
                                <h3>ATTACK/SAVE</h3>
                                <p>{this.props.spell.save_dc_ability_id != null && this.props.spell.save_dc_ability_id != "" ? AttributeID[this.props.spell.save_dc_ability_id] + " Save" : "None"}</p>
                            </Col>

                            <Col xs={3} md={3}>
                                <h3>EFFECT</h3>
                                <p>{this.props.spell.tags.length > 0 ? this.props.spell.tags[0] : ""}</p>
                            </Col>

                            <Col xs={12} md={12}>
                                <hr/>
                                <Markdown children={this.props.spell.description}/>
                            </Col>

                            { this.props.spell.components_description != null && this.props.spell.components_description != "" &&
                                <Col xs={12} md={12}>
                                    <p>* - ({this.props.spell.components_description})</p>
                                </Col>
                            }

                            <Col xs={12} md={12} className="tags">
                                <p>Spell Tags:
                                    {this.props.spell.tags.map((tag, index) => (
                                        <span key={this.props.spell.id + '-' + index}>{tag}</span>
                                    ))}
                                </p>
                            </Col>
                        </Row>

                        {this.props.mini_view != true &&
                            <ContentCollectionResource resource='spell' resource_id={this.props.spell.id}/>
                        }
                        
                    </div>
                )

    };
}

export default SingleSpell;