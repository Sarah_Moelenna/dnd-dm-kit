import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { SpellLevels } from '../monster/Data';
import { Link } from 'react-router-dom';
import { DND_DELETE } from '.././shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListSpellItem = ({ spells, refresh_function, view_spell_function, edit_spell_function, set_sort, current_sort_by, current_sort_value, view_only}) => {

    function delete_function(id){
        DND_DELETE(
            '/spell/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    function get_spell_level(value){
        var names = Object.keys(SpellLevels)
        for(var i = 0; i < names.length; i++){
            if(SpellLevels[names[i]] == value){
                return names[i]
            }
        }
    }

    function get_sort(sort_by){
        if(view_only == true){
            return null
        }

        if(current_sort_by == sort_by){
            if(current_sort_value == "DESC"){
                return (
                    <div className="clickable-div" onClick={() => {set_sort(null, null)}}>
                        <i className="fas fa-sort-down"></i>
                    </div>
                ) 
            } else {
                return (
                    <div className="clickable-div" onClick={() => {set_sort(sort_by, "DESC")}}>
                        <i className="fas fa-sort-up"></i>
                    </div>
                ) 
            }
        } else {
            return (
                <div className="clickable-div" onClick={() => {set_sort(sort_by, "ASC")}}>
                    <i className="fas fa-sort"></i>
                </div>
            )
        }
    }

    if(spells === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="spell-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name{get_sort("name")}</b></p>
                    </Col>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Spell School{get_sort("school")}</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Spell Level{get_sort("level")}</b></p>
                    </Col>
                    <Col xs={4} md={4}>
                        
                    </Col>
                </Row>
                <hr/>
            </div>
            {spells.map((spell) => (
                <div key={spell.id} className="spell-item">
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{spell.name}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <p className="card-text">{spell.school}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <p className="card-text">{get_spell_level(spell.level)}</p>
                        </Col>
                        <Col xs={4} md={4}>
                            {view_only != true &&
                                <Link className="btn btn-primary" to={"/spell/" + spell.id} onClick={() => view_spell_function(spell.id)}>
                                    View
                                </Link >
                            }
                            { spell.can_edit == true && view_only != true &&
                                <a className="btn btn-primary" onClick={() => edit_spell_function(spell.id)}>Edit</a>
                            }
                            { spell.can_edit == true && view_only != true &&
                                <DeleteConfirmationButton id={spell.id} override_button="Delete" name="Spell" delete_function={delete_function} />
                            }
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListSpellItem;