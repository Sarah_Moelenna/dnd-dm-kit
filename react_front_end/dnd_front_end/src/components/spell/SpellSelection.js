import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import Form from 'react-bootstrap/Form'
import ListSpellItem from './ListSpellItemSearch';
import FilterSpell from '../class/FilterClass';
import Card from 'react-bootstrap/Card'
import { SpellLevels, SpellSchools } from '../monster/Data';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { v4 as uuidv4 } from 'uuid';

class GroupSelection extends Component {
    state = {
        level: null,
        school: null,
        class: null
    }

    add_group = () => {
        var group = {}
        if (this.state.level){
            group.level = this.state.level
        }
        if (this.state.school){
            group.school = this.state.school
        }
        if (this.state.class){
            group.class = this.state.class
        }
        this.props.add_group(group)
        this.setState({level: null,school: null,class: null})
    }

    render(){
        return (
            <div>
                <Row>
                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Spell School</Form.Label>
                            <Form.Control name="school" as="select" onChange={(e) => {this.setState({school: e.target.value})}} custom>
                                <option selected="true" value="">-</option>
                                {SpellSchools.map((spell_school) => (
                                    <option key={spell_school} selected={this.state.school == spell_school} value={spell_school}>{spell_school}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Spell Level</Form.Label>
                            <Form.Control name="level" as="select" onChange={(e) => {this.setState({level: e.target.value})}} custom>
                                <option selected="true" value="">-</option>
                                {Object.keys(SpellLevels).map((spell_level_display) => (
                                    <option key={SpellLevels[spell_level_display]} selected={this.state.level == SpellLevels[spell_level_display]} value={SpellLevels[spell_level_display]}>{spell_level_display}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}>
                        <Form.Group controlId="formBasicSize">
                            <Form.Label>Class</Form.Label>
                            <Form.Control name="level" as="select" onChange={(e) => {this.setState({class: e.target.value})}} custom>
                                <option selected="true" value="">-</option>
                                {this.props.classes.map((player_class) => (
                                    <option key={player_class.id} selected={this.state.class == player_class.id} value={player_class.id}>{player_class.name}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={4} md={4}></Col>
                    <Col xs={4} md={4}>
                        <Button onClick={this.add_group}>Add Group</Button>
                    </Col>
                </Row>
            </div>
        )
    }
}


class SpellSelection extends Component {

    state = {
        spells: [],
        classes: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        toggle: false,
        group_toggle: false,
    }

    refresh_spells = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/spell?' + stringify(params),
          (jsondata) => {
            this.setState({ spells: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    refresh_classes = () => {

        DND_GET(
          '/class',
          (jsondata) => {
            this.setState({ classes: jsondata.results})
          },
          null
        )

    };

    get_class_name = (class_id) => {
        for(var i = 0; i < this.state.classes.length; i++){
            var player_class = this.state.classes[i]
            if(player_class.id == class_id){
                return player_class.name
            }
        }
        return "Unknown Class"
    }

    get_spell_level = (value) => {
        var names = Object.keys(SpellLevels)
        for(var i = 0; i < names.length; i++){
            if(SpellLevels[names[i]] == value){
                return names[i]
            }
        }
    }

    set_page = (page) => {
      this.refresh_spells(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_spells(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_spells(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_spells(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    open_group = () => {
        this.refresh_spells(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
        this.refresh_classes()
        this.setState({group_toggle: true})
    }

    add_spell_function = (spell_id, spell_name) => {
        var new_spell_selection = this.props.spell_selection
        if(new_spell_selection == null){
            new_spell_selection = {}
        }   

        if(new_spell_selection.spells == undefined){
            new_spell_selection.spells = {}
        }

        new_spell_selection.spells[spell_id] = {name: spell_name}
        this.props.update_function(new_spell_selection)
    }

    remove_spell = (spell_id) => {
        var new_spell_selection = this.props.spell_selection

        delete new_spell_selection.spells[spell_id]
        this.props.update_function(new_spell_selection)
    }

    get_spells = () => {
        if(this.props.spell_selection == null){
            return []
        }
        if(this.props.spell_selection.spells == undefined){
            return []
        }
        return Object.keys(this.props.spell_selection.spells)
    }

    add_group_function = (group) => {
        var new_spell_selection = this.props.spell_selection
        if(new_spell_selection == null){
            new_spell_selection = {}
        }   

        if(new_spell_selection.groups == undefined){
            new_spell_selection.groups = {}
        }

        new_spell_selection.groups[uuidv4()] = group
        this.props.update_function(new_spell_selection)
    }

    remove_group = (group_id) => {
        var new_spell_selection = this.props.spell_selection

        delete new_spell_selection.groups[group_id]
        this.props.update_function(new_spell_selection)
    }

    get_groups = () => {
        if(this.props.spell_selection == null){
            return []
        }
        if(this.props.spell_selection.groups == undefined){
            return []
        }
        
        var groups = []
        for(var i = 0; i < Object.keys(this.props.spell_selection.groups).length; i++){
            var key = Object.keys(this.props.spell_selection.groups)[i]
            var group_obj = this.props.spell_selection.groups[key]
            var display_parts = []
            if(group_obj.level){
                display_parts.push(this.get_spell_level(group_obj.level))
            }
            if(group_obj.school){
                display_parts.push(group_obj.school)
            }
            if(group_obj.class){
                display_parts.push(this.get_class_name(group_obj.class))
            }

            var group = {
                id: key,
                display: display_parts.join(' - ')
            }
            groups.push(group)
        }

        return groups
    }

    set_type = (selection_type) => {
        var new_spell_selection = this.props.spell_selection
        if(new_spell_selection == null){
            new_spell_selection = {}
        }   

        new_spell_selection.selection_type = selection_type

        this.props.update_function(new_spell_selection)
    }

    get_type = () => {
        if(this.props.spell_selection == null){
            return null
        }
        if(this.props.spell_selection.selection_type == undefined){
            return null
        }

        return this.props.spell_selection.selection_type
    }

    set_choice_amount = (choice_amount) => {
        var new_spell_selection = this.props.spell_selection
        if(new_spell_selection == null){
            new_spell_selection = {}
        }   

        new_spell_selection.choice_amount = choice_amount

        this.props.update_function(new_spell_selection)
    }

    get_choice_amount = () => {
        if(this.props.spell_selection == null){
            return null
        }
        if(this.props.spell_selection.choice_amount == undefined){
            return null
        }

        return this.props.spell_selection.choice_amount
    }

    render(){
        var spells = this.get_spells()
        var groups = this.get_groups()
        var selection_type = this.get_type()
        var choice_amount = this.get_choice_amount()

        return(
            <Card className="spell-select">
                <Card.Body>
                    <div>
                        { this.props.allow_type_change != false && 
                            <Row>
                                <Col xs={4} md={4}>
                                    <Form.Group controlId="formBasicSize">
                                        <Form.Label>Selection Type</Form.Label>
                                        <Form.Control name="gives_spells" as="select" onChange={(e) => {this.set_type(e.target.value)}} custom>
                                                <option selected="true" value="">-</option>
                                                <option selected={selection_type == "PROVIDE ALL"} value={"PROVIDE ALL"}>Provide All Spells</option>
                                                <option selected={selection_type == "CHOOSE"} value={"CHOOSE"}>Choose Spells</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                { selection_type == 'CHOOSE' && 
                                <Col xs={4} md={4}>
                                    <Form.Group controlId="formBasicName">
                                        <Form.Label>Amount</Form.Label>
                                        <Form.Control value={choice_amount} required name="choice_amount" type="number" onChange={(e) => {this.set_choice_amount(e.target.value)}} />
                                    </Form.Group>
                                </Col>
                                }
                            </Row>
                        }
                    </div>
                    <div>
                        <h2>Spells</h2>
                        {spells.length > 0 &&
                            <div className="list-selector list-selector-items">
                                {spells.map((id) => (
                                    <div className="list-selector-item">
                                        <i className="fas fa-times clickable-icon" onClick={() => {this.remove_spell(id)}}></i>
                                        {this.props.spell_selection.spells[id]['name']}
                                    </div>
                                ))}
                            </div>
                        }
                        {this.state.toggle == false &&
                            <Button onClick={this.open}>Choose Spell</Button>
                        }
                        <Modal show={this.state.toggle} size="md" className="spell-select-modal">
                                <Modal.Header>Spells</Modal.Header>
                                <Modal.Body>
                                    <FilterSpell filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                                    <ListSpellItem
                                        spells={this.state.spells}
                                        add_spell_function={this.add_spell_function}
                                        disabled_ids={(this.props.spell_selection != null && this.props.spell_selection.spells != undefined) ? Object.keys(this.props.spell_selection.spells) : []}
                                        />
                                    <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                                </Modal.Footer>
                        </Modal>
                    </div>
                    { this.props.disallow_groups != true &&
                        <div>
                            <br/>
                            <h2>Groups</h2>
                            {groups.length > 0 &&
                                <div className="list-selector list-selector-items">
                                    {groups.map((group) => (
                                        <div className="list-selector-item">
                                            <i className="fas fa-times clickable-icon" onClick={() => {this.remove_group(group.id)}}></i>
                                            {group.display}
                                        </div>
                                    ))}
                                </div>
                            }
                            {this.state.group_toggle == false &&
                                <Button onClick={this.open_group}>Add Group</Button>
                            }
                            <Modal show={this.state.group_toggle} size="md" className="spell-select-modal">
                                    <Modal.Header>Group</Modal.Header>
                                    <Modal.Body>
                                        <GroupSelection classes={this.state.classes} add_group={this.add_group_function} />
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button onClick={()=>{this.setState({group_toggle: !this.state.group_toggle})}}>Close</Button>
                                    </Modal.Footer>
                            </Modal>
                        </div>
                    }
                </Card.Body>
            </Card>
        );
    };
}

export default SpellSelection;