import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class FilterBackground extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="background_filters object-filter">
                <Row>
                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Background Name</Form.Label>
                            <Form.Control value={this.props.filters["name"] ? this.props.filters["name"] : ''} name="name" type="text" placeholder="Search Background Names" onChange={this.handleChange}/>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <Form.Group controlId="formBasicEnvironment">
                            <Form.Label>Homebrew</Form.Label>
                            <Form.Control name="homebrew" as="select" onChange={this.handleChange} custom>
                                    <option selected="" value="">-</option>
                                    <option value={true} selected={this.props.filters["homebrew"] == true}>True</option>
                                    <option value={false} selected={this.props.filters["homebrew"] == false}>False</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={3} md={3}>
                        <a className="btn btn-primary" onClick={() => this.props.clear_filter_function()}>Clear Filters</a>
                    </Col>
                </Row>
            </Form>
        );
    };
}

export default FilterBackground;