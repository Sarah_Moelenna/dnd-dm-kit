import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { ListModifiers, createModifier } from '../shared/Modifier';
import { DND_GET, DND_PUT, DND_POST } from '.././shared/DNDRequests';
import BackgroundSelect from './BackgroundSelect';
import Button from 'react-bootstrap/Button';
import MDEditor from '@uiw/react-md-editor';
import { v4 as uuidv4 } from 'uuid';
import { EditEquipmentSet, createEquipmentSet } from '../shared/EquipmentSet';
import BackConfirmationButton from '../shared/BackConfirmationButton';

const createPersonalityObject = () => {
    return {
        "id":  uuidv4(),
        "description": null,
    }
}

class SingleBackground extends Component {

    state = {
        name: null,
        short_description: null,
        skill_proficiencies_description: null,
        tool_proficiencies_description: null,
        languages_description: null,
        feature_name: null,
        feature_description: null,
        suggested_characteristics_description: null,

        traits: [],
        ideals: [],
        bonds: [],
        flaws: [],

        modifiers: [],

        equipment_set: createEquipmentSet(),

        has_changes: false,
    }

    componentWillMount() {
        if(this.props.edit_background_id != undefined && this.props.edit_background_id != null){
            this.copy_background(this.props.edit_background_id)
        }
    };

    copy_background = (background_id) => {

        DND_GET(
            '/background/' + background_id,
            (response) => {
                this.populate_with_background_data(response)
            },
            null
        )
    };

    populate_with_background_data = (background_data) => {
        var name = background_data.name
        if(this.props.edit_background_id == undefined || this.props.edit_background_id == null){
            name = "Copy of " + name
        }

        this.setState(
            {
                name: name,
                short_description: background_data.short_description,
                skill_proficiencies_description: background_data.skill_proficiencies_description,
                tool_proficiencies_description: background_data.tool_proficiencies_description,
                languages_description: background_data.languages_description,
                feature_name: background_data.feature_name,
                feature_description: background_data.feature_description,
                suggested_characteristics_description: background_data.suggested_characteristics_description,
                traits: background_data.traits,
                ideals: background_data.ideals,
                bonds: background_data.bonds,
                flaws: background_data.flaws,
                modifiers: background_data.modifiers,
                equipment_set: background_data.equipment_set,
            }
        )
    }

    save_background = () => {

        var final_background = {
            name: this.state.name,
            short_description: this.state.short_description,
            skill_proficiencies_description: this.state.skill_proficiencies_description,
            tool_proficiencies_description: this.state.tool_proficiencies_description,
            languages_description: this.state.languages_description,
            feature_name: this.state.feature_name,
            feature_description: this.state.feature_description,
            suggested_characteristics_description: this.state.suggested_characteristics_description,
            traits: this.state.traits,
            ideals: this.state.ideals,
            bonds: this.state.bonds,
            flaws: this.state.flaws,
            modifiers: this.state.modifiers,
            equipment_set: this.state.equipment_set,
        }

        console.log(final_background)

        if(this.props.edit_background_id != undefined && this.props.edit_background_id != null){
            DND_PUT(
                '/background/' + this.props.edit_background_id,
                {data: final_background},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        } else {
            DND_POST(
                '/background',
                {data: final_background},
                (response) => {this.props.close_creating_fuction()},
                null
            )
        }
    }

    handleChange = (e) => {
        var new_value = e.target.value
        if(new_value == "true"){
            new_value= true
        } else if(new_value == "false"){
            new_value= false
        } 
        this.setState({ [e.target.name]: new_value, has_changes: true });
    }

    update_text = (attribute, text) => {
        this.setState({[attribute]: text, has_changes: true})
    }

    add_modifier = () => {
        var new_modifier = createModifier()
        var modifiers = this.state.modifiers
        modifiers.push(new_modifier)
        this.setState({modifiers: modifiers, has_changes: true})
    }

    update_modifier = (id, new_object) => {
        var modifiers = []
        for(var i = 0; i < this.state.modifiers.length; i++){
            var modifier = this.state.modifiers[i]
            if(modifier.id == id){
                modifiers.push(new_object)
            } else {
                modifiers.push(modifier)
            }
        }
        this.setState({modifiers: modifiers, has_changes: true})
    }

    delete_modifier = (id) => {
        var modifiers = []
        for(var i = 0; i < this.state.modifiers.length; i++){
            var modifier = this.state.modifiers[i]
            if(modifier.id != id){
                modifiers.push(modifier)
            }
        }
        this.setState({modifiers: modifiers, has_changes: true})
    }

    update_personality_data = (e) => {
        var attribute = e.target.dataset.personalityAttribute
        var id = e.target.name
        var value = e.target.value

        var personality_objects = this.state[attribute]
        var new_personality_objects = []
        for(var i = 0; i < personality_objects.length; i++){
            var p_object = personality_objects[i]
            if(p_object.id == id){
                p_object.description = value
            }
            new_personality_objects.push(p_object)
        }
        
        this.setState({[attribute]: new_personality_objects, has_changes: true})
    }

    add_personality_object = (attribute) => {
        var new_object = createPersonalityObject()
        var p_objects = this.state[attribute]
        p_objects.push(new_object)
        this.setState({[attribute]: p_objects, has_changes: true})
    }

    delete_personality_object = (id, attribute) => {
        var new_objects = []
        for(var i = 0; i < this.state[attribute].length; i++){
            var p_object = this.state[attribute][i]
            if(p_object.id != id){
                console.log(id, p_object.id)
                new_objects.push(p_object)
            }
        }
        console.log(attribute)
        console.log({attribute: new_objects})
        this.setState({[attribute]: new_objects, has_changes: true})
    }
            
    render(){

        return(
            <div className="background">
                {(this.props.edit_background_id == undefined || this.props.edit_background_id == null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Create Background</h2>
                        <BackgroundSelect select_function={this.copy_background} override_button="Copy Existing Background"/>
                    </div>
                }
                {(this.props.edit_background_id != undefined && this.props.edit_background_id != null) &&
                    <div>
                        <h2><BackConfirmationButton back_function={this.props.close_creating_fuction} has_changes={this.state.has_changes}/> Edit Background</h2>
                    </div>
                }
                <Row className="background-settings">
                    <Col xs={12} md={12}>
                        <h3>Basic Information</h3>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control value={this.state.name} required name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicSpecialtraits">
                            <Form.Label>Description</Form.Label>
                            <MDEditor
                                value={this.state.short_description}
                                preview="edit"
                                onChange={(text) => {this.update_text("short_description", text)}}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicSpecialtraits">
                            <Form.Label>Skill Proficiencies Description</Form.Label>
                            <MDEditor
                                value={this.state.skill_proficiencies_description}
                                preview="edit"
                                onChange={(text) => {this.update_text("skill_proficiencies_description", text)}}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicSpecialtraits">
                            <Form.Label>Tool Proficiencies Description</Form.Label>
                            <MDEditor
                                value={this.state.tool_proficiencies_description}
                                preview="edit"
                                onChange={(text) => {this.update_text("tool_proficiencies_description", text)}}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicSpecialtraits">
                            <Form.Label>Languages Description</Form.Label>
                            <MDEditor
                                value={this.state.languages_description}
                                preview="edit"
                                onChange={(text) => {this.update_text("languages_description", text)}}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Feature Name</Form.Label>
                            <Form.Control value={this.state.feature_name} required name="feature_name" type="text" placeholder="Enter Feature Name" onChange={this.handleChange} />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicSpecialtraits">
                            <Form.Label>Feature Description</Form.Label>
                            <MDEditor
                                value={this.state.feature_description}
                                preview="edit"
                                onChange={(text) => {this.update_text("feature_description", text)}}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <Form.Group controlId="formBasicSpecialtraits">
                            <Form.Label>Suggested Description</Form.Label>
                            <MDEditor
                                value={this.state.suggested_characteristics_description}
                                preview="edit"
                                onChange={(text) => {this.update_text("suggested_characteristics_description", text)}}
                            />
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={12}>
                        <table className="table-data">
                            <tbody>
                                <tr className="headers">
                                    <td className="action-column">
                                        <span><i className="fas fa-plus clickable-icon" onClick={() => {this.add_personality_object("traits")}}></i></span>
                                    </td>
                                    <td>Personality Traits</td>
                                </tr>
                                {this.state.traits.map((trait) => (
                                    <tr key={trait.id}>
                                        <td className="action-column">
                                            <span><i className="fas fa-trash-alt clickable-icon" onClick={() => {this.delete_personality_object(trait.id, 'traits')}}></i></span>
                                        </td>
                                        <td>
                                             <Form.Control
                                                value={trait.description}
                                                data-personality-attribute="traits"
                                                name={trait.id}
                                                type="text"
                                                onChange={this.update_personality_data}
                                                placeholder="-" />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <br/>
                    </Col>

                    <Col xs={12} md={12}>
                        <table className="table-data">
                            <tbody>
                                <tr className="headers">
                                    <td className="action-column">
                                        <span><i className="fas fa-plus clickable-icon" onClick={() => {this.add_personality_object("ideals")}}></i></span>
                                    </td>
                                    <td>Ideals</td>
                                </tr>
                                {this.state.ideals.map((ideal) => (
                                    <tr key={ideal.id}>
                                        <td className="action-column">
                                            <span><i className="fas fa-trash-alt clickable-icon" onClick={() => {this.delete_personality_object(ideal.id, 'ideals')}}></i></span>
                                        </td>
                                        <td>
                                             <Form.Control
                                                value={ideal.description}
                                                data-personality-attribute="ideals"
                                                name={ideal.id}
                                                type="text"
                                                onChange={this.update_personality_data}
                                                placeholder="-" />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <br/>
                    </Col>

                    <Col xs={12} md={12}>
                        <table className="table-data">
                            <tbody>
                                <tr className="headers">
                                    <td className="action-column">
                                        <span><i className="fas fa-plus clickable-icon" onClick={() => {this.add_personality_object("bonds")}}></i></span>
                                    </td>
                                    <td>Bonds</td>
                                </tr>
                                {this.state.bonds.map((bond) => (
                                    <tr key={bond.id}>
                                        <td className="action-column">
                                            <span><i className="fas fa-trash-alt clickable-icon" onClick={() => {this.delete_personality_object(bond.id, 'bonds')}}></i></span>
                                        </td>
                                        <td>
                                             <Form.Control
                                                value={bond.description}
                                                data-personality-attribute="bonds"
                                                name={bond.id}
                                                type="text"
                                                onChange={this.update_personality_data}
                                                placeholder="-" />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <br/>
                    </Col>

                    <Col xs={12} md={12}>
                        <table className="table-data">
                            <tbody>
                                <tr className="headers">
                                    <td className="action-column">
                                        <span><i className="fas fa-plus clickable-icon" onClick={() => {this.add_personality_object("flaws")}}></i></span>
                                    </td>
                                    <td>Flaws</td>
                                </tr>
                                {this.state.flaws.map((flaw) => (
                                    <tr key={flaw.id}>
                                        <td className="action-column">
                                            <span><i className="fas fa-trash-alt clickable-icon" onClick={() => {this.delete_personality_object(flaw.id, 'flaws')}}></i></span>
                                        </td>
                                        <td>
                                             <Form.Control
                                                value={flaw.description}
                                                data-personality-attribute="flaws"
                                                name={flaw.id}
                                                type="text"
                                                onChange={this.update_personality_data}
                                                placeholder="-" />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <br/>
                    </Col>

                    <Col xs={12} md={12}>
                        {this.state.modifiers.length > 0 ?
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.modifiers.length} Modifiers
                                    <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                                <ListModifiers modifiers={this.state.modifiers} update_modifier={this.update_modifier} delete_modifier={this.delete_modifier} display_type="SPELL" scale_type={this.state.scale_type}/>
                            </div>
                            :
                            <div>
                                <hr/>
                                <h4>
                                    {this.state.modifiers.length} Modifiers
                                    <span className="clickable-div add-button" onClick={() => {this.add_modifier()}}>
                                        <i className="fas fa-plus"></i>
                                    </span>
                                </h4>
                            </div>
                        }
                    </Col>

                    <Col xs={12} md={12}>
                        <hr/>
                        <h3>Equipment</h3>
                    </Col>
                    
                    <Col xs={12} md={12}>
                        <EditEquipmentSet
                            equipment_set={this.state.equipment_set}
                            update_equipment_set={(id, data) => {this.setState({equipment_set: {...data}, has_changes: true})}}
                        />
                    </Col>

                    <Col xs={12} md={12}>
                        <br/>
                        <Button variant="primary" type="submit" onClick={this.save_background}>
                            Save Background
                        </Button>
                    </Col>

                </Row>
            </div>
        )

    };
}

export default SingleBackground;