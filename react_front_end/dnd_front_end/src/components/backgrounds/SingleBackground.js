import React, { Component } from 'react';
import ContentCollectionResource from '../shared/ContentCollectionResource';
import { Link } from 'react-router-dom';
import Markdown from 'react-markdown';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class SingleBackground extends Component {
            
    render(){

                return(
                    <div className="background">
                        <h2>
                            <Link to="/background" onClick={this.props.close_background_fuction}>
                                <i className="fas fa-chevron-left clickable-icon"></i>
                            </Link >
                            {this.props.background.name}
                        </h2>

                        <Row>
                            {this.props.background.short_description &&
                                <Col xs={12} md={12}>
                                    <h3>Description</h3>
                                    <Markdown children={this.props.background.short_description}/>
                                    <hr/>
                                </Col>
                            }
                            {this.props.background.skill_proficiencies_description &&
                                <Col xs={12} md={12}>
                                    <h3>Skill Proficiencies</h3>
                                    <Markdown children={this.props.background.skill_proficiencies_description}/>
                                </Col>
                            }
                            {this.props.background.tool_proficiencies_description &&
                                <Col xs={12} md={12}>
                                    <h3>Tool Proficiencies</h3>
                                    <Markdown children={this.props.background.tool_proficiencies_description}/>
                                </Col>
                            }
                            {this.props.background.languages_description &&
                                <Col xs={12} md={12}>
                                    <h3>Language Proficiencies</h3>
                                    <Markdown children={this.props.background.languages_description}/>
                                </Col>
                            }
                            {this.props.background.equipment_set &&
                                <Col xs={12} md={12}>
                                    <h3>Equipment</h3>
                                    <Markdown children={this.props.background.equipment_set.description}/>
                                </Col>
                            }
                            {this.props.background.feature_name &&
                                <Col xs={12} md={12}>
                                    <hr/>
                                    <h3>{this.props.background.feature_name}</h3>
                                    <Markdown children={this.props.background.feature_description}/>
                                </Col>
                            }
                            {this.props.background.suggested_characteristics_description &&
                                <Col xs={12} md={12}>
                                    <hr/>
                                    <h3>Suggested Characteristics</h3>
                                    <Markdown children={this.props.background.suggested_characteristics_description}/>
                                </Col>
                            }
                            {this.props.background.traits &&
                                <Col xs={12} md={12}>
                                    <table className="table-data">
                                        <tbody>
                                            <tr className="headers">
                                                <td className="action-column"></td>
                                                <td>Personality Traits</td>
                                            </tr>
                                            {this.props.background.traits.map((trait, index) => (
                                                <tr key={trait.id}>
                                                    <td className="action-column">{index + 1}</td>
                                                    <td>
                                                        <p>{trait.description}</p>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                    <br/>
                                </Col>
                            }
                            {this.props.background.ideals &&
                                <Col xs={12} md={12}>
                                    <table className="table-data">
                                        <tbody>
                                            <tr className="headers">
                                                <td className="action-column"></td>
                                                <td>Ideals</td>
                                            </tr>
                                            {this.props.background.ideals.map((ideal, index) => (
                                                <tr key={ideal.id}>
                                                    <td className="action-column">{index + 1}</td>
                                                    <td>
                                                        <p>{ideal.description}</p>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                    <br/>
                                </Col>
                            }
                            {this.props.background.bonds &&
                                <Col xs={12} md={12}>
                                    <table className="table-data">
                                        <tbody>
                                            <tr className="headers">
                                                <td className="action-column"></td>
                                                <td>Bonds</td>
                                            </tr>
                                            {this.props.background.bonds.map((bond, index) => (
                                                <tr key={bond.id}>
                                                    <td className="action-column">{index + 1}</td>
                                                    <td>
                                                        <p>{bond.description}</p>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                    <br/>
                                </Col>
                            }
                            {this.props.background.flaws &&
                                <Col xs={12} md={12}>
                                    <table className="table-data">
                                        <tbody>
                                            <tr className="headers">
                                                <td className="action-column"></td>
                                                <td>Flaws</td>
                                            </tr>
                                            {this.props.background.flaws.map((flaw, index) => (
                                                <tr key={flaw.id}>
                                                    <td className="action-column">{index + 1}</td>
                                                    <td>
                                                        <p>{flaw.description}</p>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                    <br/>
                                </Col>
                            }
                        </Row>
                        <ContentCollectionResource resource='background' resource_id={this.props.background.id}/>
                    </div>
                )

    };
}

export default SingleBackground;