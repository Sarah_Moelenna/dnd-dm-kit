import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '.././shared/DNDRequests';
import { Link } from 'react-router-dom';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import Markdown from 'react-markdown';


const ListBackgroundItem = ({ backgrounds, view_background_function, refresh_function, edit_function}) => {

    function delete_function(id){
        DND_DELETE(
            '/background/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(backgrounds === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="background-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={2} md={2}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={8} md={8}>
                        <p className="card-text"><b>Description</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                    </Col>
                </Row>
            </div>
            {backgrounds.map((background) => (
                <div key={background.id} className="background-item">
                    <hr/>
                    <Row>
                        <Col xs={2} md={2}>
                        <p className="card-text">{background.name}</p>
                        </Col>
                        <Col xs={8} md={8}>
                            <Markdown children={background.short_description}/>
                        </Col>
                        <Col xs={2} md={2}>
                            <Link className="btn btn-primary" to={"/background/" + background.id} onClick={() => view_background_function(background.id)}>
                                View
                            </Link >
                            { background.can_edit == true && 
                                <a className="btn btn-primary" onClick={() => edit_function(background.id)}>Edit</a>
                            }
                            { background.can_edit == true &&
                                <DeleteConfirmationButton id={background.id} override_button="Delete" name="Background" delete_function={delete_function} />
                            }
                        </Col>
                    </Row>
                </div>
            ))}
        </div>
    );
}

export default ListBackgroundItem;