import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '../shared/Paginator';
import { stringify } from 'query-string';
import FilterEffectDeck from './FilterEffectDeck';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const ListEffectDecks = ({ effect_decks, select_function, disabled_ids}) => {


    return (
        <div className="effect_deck-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={3} md={3}>
                    </Col>
                </Row>
            </div>
            {effect_decks.map((effect_deck) => (
                <div key={effect_deck.id} className={disabled_ids.includes(effect_deck.id) ? "effect_deck-item disabled" : "effect_deck-item"}>
                    <Row>
                    <Col xs={3} md={3}>
                            <p className="card-text">{effect_deck.name}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            {!disabled_ids.includes(effect_deck.id) &&
                                <Button onClick={() => {select_function(effect_deck.id, effect_deck.name, effect_deck.dexterity, effect_deck.xp, effect_deck.image)}}>Select EffectDeck</Button>
                            }
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

class EffectDeckSelect extends Component {

    state = {
        effect_decks: [],
        page: 1,
        total_pages: 1,
        filters: {},
        toggle: false
    }

    componentDidMount() {
        this.refresh_effect_decks(this.state.page, this.state.filters)
    };

    select_effect_deck = (effect_deck) => {

        this.setState({toggle: false})
        this.props.callback(effect_deck)
    }

    refresh_effect_decks = (page, filters) => {
        var params = filters
        
        params['page'] = page
        params['results_per_page'] = 10

        DND_GET(
          '/effect_deck?' + stringify(params),
          (jsondata) => {
            this.setState({ effect_decks: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_effect_decks(page, this.state.filters)
    };

    set_sort = (sort_by, sort_value) => {
        this.setState(
          {sort_by: sort_by, sort_value: sort_value},
          this.refresh_effect_decks(this.state.page, this.state.filters, sort_by, sort_value)
        )
      };


    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_effect_decks(1, new_filters))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_effect_decks(1, []))
    };

    quick_refresh = () => {
      this.refresh_effect_decks(this.state.page, this.state.filters)
    };

    open = () => {
        this.quick_refresh()
        this.setState({toggle: true})
    }

    select_function = (id) => {

        this.props.select_function(id)
        this.setState({toggle: false})
    }

    render(){
        return(
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>{this.props.override_button != undefined ? this.props.override_button : "Choose Sound Deck"}</Button>
                }
                <Modal show={this.state.toggle} size="md" className="effect_deck-select-modal select-modal">
                        <Modal.Header>Sound Decks</Modal.Header>
                        <Modal.Body>
                            <FilterEffectDeck filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>
                            <ListEffectDecks
                                effect_decks={this.state.effect_decks}
                                select_function={this.select_function}
                                disabled_ids={Array.isArray(this.props.disabled_ids) ? this.props.disabled_ids : []}
                            />
                            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                        </Modal.Footer>
                </Modal>
            </div>
        );
    };
}

export default EffectDeckSelect;