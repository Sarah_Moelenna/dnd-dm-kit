import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_POST } from '../shared/DNDRequests';

class CreateEffectDeck extends Component {

    state = {
        name: null,
    }

    handleSubmit = (event) => {
        
        if( this.state.name == null){
            event.preventDefault();
            return
        }

        var data = {
            name: this.state.name,
        }

        DND_POST(
            '/effect_deck',
            data,
            (response) => {
                this.setState({ name: null })
            },
            null
        )

        event.preventDefault();
      
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };


    render(){
            return(
                <Form onSubmit={this.handleSubmit} className="create-effect_deck">
                    <Row>

                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control required value={this.state.name ? this.state.name : ''} name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={2} md={2}>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Col>
                    </Row>
                </Form>
            );
    };
}

export default CreateEffectDeck;