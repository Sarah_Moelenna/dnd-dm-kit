import React, { Component, useContext } from 'react';
import { DND_POST, DND_PUT } from '../shared/DNDRequests';
import MusicSelect from '../music/MusicSelect';
import PlaylistSelect from '../playlist/PlaylistSelect';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'
import { SingleMusicListItem } from '../music/MusicSelect';
import { SinglePlaylistListItem } from '../playlist/PlaylistSelect';
import {SocketContext} from '.././socket/Socket';
import BackConfirmationButton from '../shared/BackConfirmationButton';

const ICONS = {
    "Creatures": {
        "otter": "fas fa-otter",
        "hippo": "fas fa-hippo",
        "dog": "fas fa-dog",
        "spider": "fas fa-spider",
        "kiwi-bird": "fas fa-kiwi-bird",
        "horse": "fas fa-horse",
        "frog": "fas fa-frog",
        "dragon": "fas fa-dragon",
        "dove": "fas fa-dove",
        "cat": "fas fa-cat",
        "crow": "fas fa-crow",
        "eldritch": "fas fa-pastafarianism",
        "bug": "fas fa-bug",
        "ghost": "fab fa-snapchat-ghost",
        "mule": "fab fa-sticker-mule",
        "phoenix": "fab fa-phoenix-framework",
        "octopus": "fab fa-octopus-deploy",
        "penguin": "fab fa-linux",
    },
    "People": {
        "carry": "fas fa-people-carry",
        "walking": "fas fa-walking",
        "spy": "fas fa-user-secret",
        "hiking": "fas fa-hiking",
        "baby": "fas fa-baby",
        "running": "fas fa-running",
        "male": "fas fa-male",
        "female": "fas fa-female",
        "ninja": "fas fa-user-ninja",
        "doctor": "fas fa-user-md",
        "child": "fas fa-child",
        "student": "fas fa-user-graduate",
        "prayer": "fas fa-pray",
        "swimming": "fas fa-swimmer",
        "clapping": "fas fa-sign-language",
    },
    "Shapes": {
        "square": "fas fa-square",
        "plus": "fas fa-plus",
        "star": "fas fa-star",
        "triangle": "fas fa-play",
        "heart": "fas fa-heart",
        "broken-heart": "fas fa-heart-broken",
        "drop": "fas fa-map-marker",
        "cloud": "fas fa-cloud",
        "circle": "fas fa-circle",
        "speech-bubble": "fas fa-comment",
        "star": "fas fa-certificate",
        "minus": "fas fa-minus",
        "cross": "fas fa-times",
    },
    "Objects": {
        "hourglass": "far fa-hourglass",
        "calendar": "fas fa-calendar-alt",
        "bomb": "fas fa-bomb",
        "fist": "fas fa-fist-raised",
        "wrench": "fas fa-wrench",
        "bottle": "fas fa-wine-bottle",
        "glass": "fas fa-wine-glass",
        "weight": "fas fa-weight-hanging",
        "wallet": "fas fa-wallet",
        "ball": "fas fa-volleyball-ball",
        "vial": "fas fa-vial",
        "knife-fork": "fas fa-utensils",
        "spoon": "fas fa-utensil-spoon",
        "umbrella": "fas fa-umbrella",
        "shirt": "fas fa-tshirt",
        "trophy": "fas fa-trophy",
        "tree": "fas fa-tree",
        "trash-bin": "fas fa-trash",
        "toilet": "fas fa-toilet",
        "thermometer": "fas fa-thermometer-quarter",
        "teeth": "fas fa-teeth",
        "syringe": "fas fa-syringe",
        "stop-watch": "fas fa-stopwatch",
        "stamp": "fas fa-stamp",
        "snowman": "fas fa-snowman",
        "snowflake": "fas fa-snowflake",
        "skull": "fas fa-skull",
        "sign": "fas fa-sign",
        "shopping-basket": "fas fa-shopping-basket",
        "shower": "fas fa-shower",
        "ship-boat": "fas fa-ship",
        "screwdriver": "fas fa-screwdriver",
        "ring": "fas fa-ring",
        "broomstick": "fas fa-quidditch",
        "puzzle-piece": "fas fa-puzzle-piece",
        "piggy-bank": "fas fa-piggy-bank",
        "pizza-food": "fas fa-pizza-slice",
        "pills-medicine": "fas fa-piggy-bank",
        "phone": "fas fa-phone-volume",
        "chilli": "fas fa-pepper-hot",
        "pen": "fas fa-pen-alt",
        "mug-coffee-tea": "fas fa-mug-hot",
        "money": "fas fa-money-bill-wave",
        "meteor-comet": "fas fa-meteor",
        "map": "fas fa-map",
        "magnet": "fas fa-magnet",
        "wand-magic": "fas fa-magic",
        "lungs": "fas fa-lungs",
        "lemon": "fas fa-lemon",
        "leaf": "fas fa-leaf",
        "key": "fas fa-key",
        "waizard-hat": "fas fa-hat-wizard",
        "cowboy-hat": "fas fa-hat-cowboy",
        "hamburger": "fas fa-hamburger",
        "glasses": "fas fa-glasses",
        "fish": "fas fa-fish",
        "tap-faucet": "fas fa-faucet",
        "drum": "fas fa-drum",
        "dice-d6": "fas fa-dice-d6",
        "dice-d20": "fas fa-dice-d20",
        "crown": "fas fa-crown",
        "compass": "fas fa-compass",
        "bell": "fas fa-concierge-bell",
        "cogs": "fas fa-cogs",
        "chess": "fas fa-chess",
        "cheese": "fas fa-cheese",
        "brain": "fas fa-brain",
        "book": "fas fa-book-open",
        "book-evil": "fas fa-book-dead",
        "bone": "fas fa-bone",
        "bicycle-bike": "fas fa-bicycle",
        "bell": "fas fa-bell",
        "bed": "fas fa-bed",
        "beer": "fas fa-beer",
        "bullseye": "fas fa-bullseye",
        "shield": "fas fa-shield-alt",
    },
    "Symbols": {
        "biohazard": "fas fa-biohazard",
        "yin-yang": "fas fa-yin-yang",
        "virus": "fas fa-virus",
        "venus": "fas fa-venus",
        "unlock": "fas fa-unlock",
        "lock": "fas fa-lock",
        "thumbs-up": "fas fa-thumbs-up",
        "thumbs-down": "fas fa-thumbs-down",
        "sun": "fas fa-sun",
        "star-of-david": "fas fa-star-of-david",
        "rainbow": "fas fa-rainbow",
        "radiation": "fas fa-radiation",
        "poop": "fas fa-poop",
        "poop-storm": "fas fa-poo-storm",
        "music": "fas fa-music",
        "moon": "fas fa-moon",
        "globe": "fas fa-globe-americas",
        "fire": "fas fa-fire",
        "fingerprint": "fas fa-fingerprint",
        "cloud-rain": "fas fa-cloud-rain",
        "crash": "fas fa-car-crash",
        "lighting-bolt": "fas fa-bolt",
        "scales": "fas fa-balance-scale",
        "atom": "fas fa-atom",
        "wind": "fas fa-wind",
    },
    "Places": {
        "warehouse": "fas fa-warehouse",
        "university": "fas fa-university",
        "theatre": "fas fa-theater-masks",
        "shop": "fas fa-store",
        "synagogue": "fas fa-synagogue",
        "school": "fas fa-school",
        "road": "fas fa-road",
        "mountain": "fas fa-mountain",
        "mosque": "fas fa-mosque",
        "monument": "fas fa-monument",
        "landmark": "fas fa-landmark",
        "igloo": "fas fa-igloo",
        "home": "fas fa-home",
        "hotel": "fas fa-hotel",
        "hospital": "fas fa-hospital",
        "dungeon": "fas fa-dungeon",
        "church": "fas fa-church",
        "city": "fas fa-city",
        "archway": "fas fa-archway",
    },
    "Faces": {
        "smile": "fas fa-smile",
        "wink": "fas fa-smile-wink",
        "tired": "fas fa-tired",
        "sad": "fas fa-sad-tear",
        "crying": "fas fa-sad-cry",
        "robot": "fas fa-robot",
        "bored-meh": "fas fa-meh",
        "laugh": "fas fa-laugh",
        "kiss": "fas fa-kiss",
        "tongue": "fas fa-grin-tongue",
        "grimace": "fas fa-grimace",
        "frown": "fas fa-frown",
        "angry": "fas fa-angry",
    }
}

export const DeckButton = ({deck_item}) => {

    const socket = useContext(SocketContext);

    function on_play(){
        socket.emit("sound_effect_play", deck_item.value)
    }

    function create_audio_command(){
        var data = {
            "music_play_type": "LOOP",
            "audio_id": deck_item.value,
        }
        return {
            "command_code": "CMD_PLAY",
            "data": data
        }
    }

    function create_playlist_command(){
        var data = {
            "music_play_type": "LOOP",
            "playlist_id": deck_item.value,
        }
        return {
            "command_code": "CMD_PLAYLIST",
            "data": data
        }
    }

    function handleClick (data){

        DND_POST(
            "/commands",
            data,
            (response) => {},
            null
        )
      
    }

    if(deck_item.type == "PLAYLIST"){
        return (
            <span class="sound-deck-button" title={deck_item.name} onClick={(e) => {handleClick(create_playlist_command())}}>
                <i class={deck_item.icon}></i>
            </span>
        )
    }
    if(deck_item.type == "MUSIC" || deck_item.type == "AMBIENCE"){
        return (
            <span class="sound-deck-button" title={deck_item.name} onClick={(e) => {handleClick(create_audio_command())}}>
                <i class={deck_item.icon}></i>
            </span>
        )
    }
    if(deck_item.type == "EFFECT"){
        return (
            <span class="sound-deck-button" title={deck_item.name} onClick={() => {on_play()}}>
                <i class={deck_item.icon}></i>
            </span>
        )
    }
    return null
}

class SingleEffectDeck extends Component {

    state = {
        data: {},
        selected_audio_id: null,
        selected_audio_object: null,
        selected_playlist_id: null,
        selected_playlist_object: null,
        selected_icon: null,
        name: null,
        filter: '',
        has_changed: false,
    }

    componentWillMount() {
        this.setState({data: this.props.effect_deck.data})
    };

    add_audio = (audio_id, audio_object) => {
        this.setState({selected_audio_id: audio_id, selected_audio_object: audio_object, selected_playlist_id: null, selected_playlist_object: null})
    }

    add_playlist = (playlist_id, playlist_object) => {
        this.setState({selected_audio_id: null, selected_audio_object: null, selected_playlist_id: playlist_id, selected_playlist_object: playlist_object})
    }

    add_to_deck = () => {
        let data = this.state.data
        if (data == null){
            data = {}
        }

        if(this.state.selected_playlist_id != null){
            data[this.state.selected_icon] = {
                name: this.state.name,
                icon: this.state.selected_icon,
                type: "PLAYLIST",
                value: this.state.selected_playlist_object.id
            }
        } else {
            let value = null
            if(this.state.selected_audio_object.audio_type != 'EFFECT'){
                value = this.state.selected_audio_object.id
            } else {
                value = this.state.selected_audio_object.file_name
            }
            data[this.state.selected_icon] = {
                name: this.state.name,
                icon: this.state.selected_icon,
                type: this.state.selected_audio_object.audio_type,
                value: value
            }
        }

        this.setState(
            {
                selected_audio_id: null,
                selected_audio_object: null,
                selected_playlist_id: null,
                selected_playlist_object: null,
                name: null,
                selected_icon: null,
                filter: '',
                data: data,
                has_changed: true
            }
        )
    }

    remove = (id) => {
        let data = this.state.data
        delete data[id]
        this.setState({data: data, has_changed: true})
    }

    save = () => {

        let data = {
            "data": this.state.data
        }

        DND_PUT(
            '/effect_deck/' + this.props.effect_deck.id,
            data,
            (response) => {
                this.setState({ has_changed: false })
            },
            null
        )

    }

    titleCase = (str) => {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        // Directly return the joined string
        return splitStr.join(' '); 
    }

    get_sections = () => {
        let final_sections = {}

        let sections = Object.keys(ICONS).sort()
        for(let i = 0; i < sections.length; i++){
            let icon_section = sections[i]
            let section_icons = Object.keys(ICONS[icon_section]).sort().filter(e => e.includes(this.state.filter))
            if(section_icons.length > 0){
                final_sections[icon_section] = {}
                for(let j = 0; j < section_icons.length; j++){
                    final_sections[icon_section][section_icons[j]] = ICONS[icon_section][section_icons[j]]
                }
            }
        }
        return final_sections
    }

    render(){
        let sections = this.get_sections()
        console.log(sections)

        return(
            <div>
                <h2><BackConfirmationButton back_function={this.props.close_effect_deck_function} has_changes={this.state.has_changed}/> {this.props.effect_deck.name}</h2>
                <hr/>
                <h3>Add New Button</h3>
                    <Row>
                        <Col xs={4} md={4}>
                            <Form.Control value={this.state.filter} name="filter" type="text" placeholder="Filter Icons" onChange={(e) => {this.setState({filter: e.target.value})}}/>
                            <div className='effect-deck-icons'>
                            {Object.keys(sections).sort().map((icon_section) => (
                                <div key={icon_section} className="icon_section">
                                    <p>{icon_section}</p>
                                    <div className="icon_selection">
                                        {Object.keys(sections[icon_section]).sort().map((icon_name) => (
                                            <span 
                                                key={icon_section + "-" + icon_name}
                                                className={this.state.selected_icon == sections[icon_section][icon_name] ? 'icon selected' : 'icon'}
                                                onClick={() => {this.setState({selected_icon: sections[icon_section][icon_name]})}}
                                                title={this.titleCase(icon_name.replace('-', ' '))}
                                            >
                                                <i class={sections[icon_section][icon_name]}></i>
                                            </span>
                                        ))}
                                    </div>
                                </div>
                            ))}
                            </div>
                        </Col>
                        <Col xs={4} md={4}>
                            <div>
                                <MusicSelect select_function={this.add_audio} disabled_ids={this.state.selected_audio_id != null ? [this.state.selected_audio_id] : []}/>
                                <PlaylistSelect select_function={this.add_playlist} disabled_ids={this.state.selected_playlist_id != null ? [this.state.selected_playlist_id] : []}/>
                                {this.state.selected_audio_id != null &&
                                    <div className="effect-deck audio-items">
                                        <div className="audio-item">
                                            <SingleMusicListItem
                                                audio_item={this.state.selected_audio_object}
                                                select_function={() => {}}
                                                disabled_ids={[this.state.selected_audio_id]}
                                            />
                                        </div>
                                    </div>
                                }
                                {this.state.selected_playlist_id != null &&
                                    <div className="effect-deck audio-items">
                                        <div className="audio-item">
                                            <SinglePlaylistListItem
                                                playlist_item={this.state.selected_playlist_object}
                                                select_function={() => {}}
                                                disabled_ids={[this.state.selected_playlist_id]}
                                            />
                                        </div>
                                    </div>
                                }
                            </div>
                        </Col>
                        <Col xs={4} md={4}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control value={this.state.name} name="name" type="text" required onChange={(e) => {this.setState({name: e.target.value})}}/>
                            </Form.Group>

                            {this.state.name && this.state.selected_icon && (this.state.selected_audio_id || this.state.selected_playlist_id) &&
                                <Button variant="primary" type="submit" onClick={() => this.add_to_deck()}>
                                    Add to Deck
                                </Button>
                            }
                        </Col>
                    </Row>
                <hr/>
                <h3>Current Deck</h3>
                <Button variant="primary" type="submit" onClick={() => this.save()}>Save</Button>
                {this.state.data &&
                    <div className='sound-deck-container'>
                        {Object.keys(this.state.data).map((icon_id) => (
                            <div>
                                <DeckButton deck_item={this.state.data[icon_id]}/>
                                <i class="fas fa-trash-alt clickable-icon" onClick={() => this.remove(icon_id)}></i>
                            </div>
                        ))}
                    </div>
                }
                
            </div>
        )

    };
}

export default SingleEffectDeck;