import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'

class FilterEffectDeck extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Form onSubmit={this.handleSubmit} className="effect_deck_filters">
                <Form.Group controlId="formBasicName">
                    <Form.Label>Deck Name</Form.Label>
                    <Form.Control value={this.props.filters["name"]} name="name" type="text" placeholder="Search Deck Names" onChange={this.handleChange}/>
                </Form.Group>
            </Form>
        );
    };
}

export default FilterEffectDeck;