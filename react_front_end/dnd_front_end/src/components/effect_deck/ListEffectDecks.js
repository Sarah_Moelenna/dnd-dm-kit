import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '../shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListEffectDecks = ({ effect_decks, refresh_function, view_effect_deck_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/effect_deck/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(effect_decks === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="effect_deck-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={6} md={6}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        
                    </Col>
                    <Col xs={1} md={1}>
                        <p className="card-text"><b>Controls</b></p>
                    </Col>
                </Row>
                <hr/>
            </div>
            {effect_decks.map((effect_deck) => (
                <div key={effect_deck.id} className="effect_deck-item">
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{effect_deck.name}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <a className="btn btn-primary" onClick={() => view_effect_deck_function(effect_deck.id)}>View Effect Deck</a>
                        </Col>
                        <Col xs={1} md={1}>
                            <DeleteConfirmationButton id={effect_deck.id} name="Effect Deck" delete_function={delete_page_function} />
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListEffectDecks;