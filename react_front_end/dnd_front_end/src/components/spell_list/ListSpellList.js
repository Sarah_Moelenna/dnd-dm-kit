import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '../shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListSpellLists = ({ spell_lists, refresh_function, view_spell_list_function}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/spell-list/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(spell_lists === undefined) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="spell_list-items">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Class Name</b></p>
                    </Col>
                    <Col xs={3} md={3} className="cenetered-column">
                        <p className="card-text"><b>Spell Count</b></p>
                    </Col>
                    <Col xs={2} md={2}>
                        
                    </Col>
                    <Col xs={1} md={1}>
                        <p className="card-text"><b>Controls</b></p>
                    </Col>
                </Row>
                <hr/>
            </div>
            {spell_lists.map((spell_list) => (
                <div key={spell_list.id} className="spell_list-item">
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{spell_list.class}</p>
                        </Col>
                        <Col xs={3} md={3} className="cenetered-column">
                            <p className="card-text">{spell_list.spell_count}</p>
                        </Col>
                        <Col xs={2} md={2}>
                            <a className="btn btn-primary" onClick={() => view_spell_list_function(spell_list.id)}>View SpellList</a>
                        </Col>
                        <Col xs={1} md={1}>
                            <DeleteConfirmationButton id={spell_list.id} name="SpellList" delete_function={delete_page_function} />
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListSpellLists;