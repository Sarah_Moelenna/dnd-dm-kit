import React, { Component } from 'react';
import { DND_POST, DND_DELETE } from '.././shared/DNDRequests';
import SpellSelect from '.././spell/SpellSelect'
import ListSpellItem from '.././spell/ListSpellItem'
import ContentCollectionResource from '../shared/ContentCollectionResource';

class SingleSpellList extends Component {

    add_spell = (spell_id) => {
        
        DND_POST(
            '/spell-list/' + this.props.spell_list.id + '/spell/' + spell_id,
            {},
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    }

    delete_spell = (spell_id) => {

        DND_DELETE(
            '/spell-list/' + this.props.spell_list.id + '/spell/' + spell_id,
            {},
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    }

    get_spell_ids = () => {
        let ids = []
        for(let i = 0; i < this.props.spell_list.spells.length; i++){
            ids.push(this.props.spell_list.spells[i].id)
        }
        return ids
    }

    render(){

        return(
            <div>
                <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_spell_list_fuction}></i> {this.props.spell_list.class} Spell List</h2>
                <SpellSelect
                    select_function={this.add_spell}
                    override_button="Add Spell"
                    disabled_ids={this.get_spell_ids()}
                />
                <hr/>
                <ListSpellItem
                    spells={this.props.spell_list.spells}
                    view_only={true}
                />
                <ContentCollectionResource resource='spelllist' resource_id={this.props.spell_list.id}/>
            </div>
        )

    };
}

export default SingleSpellList;