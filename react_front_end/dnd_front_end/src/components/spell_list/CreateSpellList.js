import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_POST } from '../shared/DNDRequests';
import ClassSelect from '../class/ClassSelect';

class CreateSpellList extends Component {

    state = {
        class: null,
        class_name: null
    }

    handleSubmit = (event) => {
        
        if( this.state.class == null){
            event.preventDefault();
            return
        }

        var data = {
            class_id: this.state.class
        }

        DND_POST(
            '/spell-list',
            data,
            (response) => {
                this.props.refresh_function()
                this.setState({ class: null, class_name: null})
            },
            null
        )

        event.preventDefault();
      
    }

    set_class = (class_id, name) =>{
        this.setState({class: class_id, class_name: name})
    }


    render(){
            return(
                <Form onSubmit={this.handleSubmit} className="create-spell_list">
                    <Row>

                        <Col xs={2} md={2}>
                            <p>{this.state.class_name}</p>
                            <ClassSelect
                                select_function={this.set_class}
                                exclude={[]}
                            />
                        </Col>

                        <Col xs={2} md={2}>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Col>
                    </Row>
                </Form>
            );
    };
}

export default CreateSpellList;