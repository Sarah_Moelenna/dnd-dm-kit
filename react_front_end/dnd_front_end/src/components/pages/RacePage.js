import React, { Component } from 'react';
import ListRaces from '.././race/ListRaces';
import CreateRace from '.././race/CreateRace';
import Collapsible from '.././shared/Collapsible';
import FilterRace from '.././race/FilterRace'
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';
import SingleRace from '../race/SingleRace';

class RacePage extends Component {

    state = {
        races: [],
        race_focus: null,
        edit_race_id: null,
        creating: false,
        filters: {}
      }

    componentDidMount() {
        this.refresh_races()
    };

    shouldComponentUpdate(nextProps, nextState) {
      if (this.state != nextState){
          return true
      }
      if (this.props === undefined || nextProps === undefined){
          return true;
      }
      if (this.props.active === undefined){
          return true
      }
      if (this.props.active != nextProps.active){
          return true
      }
      return false
  }
            
  refresh_races = () => {
      DND_GET(
        '/race?' + stringify(this.state.filters),
        (jsondata) => {
          this.setState({ races: jsondata})
        },
        null
      )
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_races())
    };

    display_race = (race_id) => {
      DND_GET(
        '/race/' + race_id,
        (jsondata) => {
          this.setState({ race_focus: jsondata})
        },
        null
      )
    };

    edit_race = (race_id) => {
      DND_GET(
        '/race/' + race_id,
        (jsondata) => {
          this.setState({ race_focus: jsondata})
        },
        null
      )
    };

    refresh_race_in_focus = () => {
      this.refresh_races()

      DND_GET(
        '/race/' + this.state.race_focus.id,
        (jsondata) => {
          this.setState({ race_focus: jsondata})
        },
        null
      )
    };

    close_race = () => {
        this.setState({ race_focus: null})
    };

    get_structured_races = () => {
        var base_races = {}
        var races = []

        for(var i = 0; i < this.state.races.length; i++){
            var race = this.state.races[i]
            if(base_races[race.base_name] == undefined){
                base_races[race.base_name] = {
                    races: [],
                    name: race.base_name,
                    image: race.portrait_avatar_url
                }
            }

            base_races[race.base_name].races.push(race)
        }

        var keys = Object.keys(base_races).sort()
        for(var i = 0; i < keys.length; i++){
            races.push(base_races[keys[i]])
        }

        return races
    }

    render(){
      if (this.props.active == false){
        return null;
      }

      var races = this.get_structured_races()

      if (this.state.creating == true){
        return (
          <div className="race-page">
            <CreateRace
              edit_race_id={this.state.edit_race_id}
              close_creating_fuction={() => {this.setState({creating: false, edit_race_id: null})}}
            />
          </div>
        )
      } else if (this.state.race_focus == null){
          return(
              <div className="race-page">
                  <button className="btn btn-primary" onClick={() => {this.setState({creating: true})}}>Create Race</button>
                  <h2>Current Races <i className="fas fa-sync clickable-icon" onClick={this.refresh_races}></i></h2>
                  <Collapsible contents={<FilterRace filters={this.state.filters} update_filter_function={this.set_filters}/>} title="Filter Races"/>
                  <ListRaces 
                    races={races}
                    refresh_function={this.refresh_races}
                    view_race_function={this.display_race}
                    edit_race_function={(id) => {this.setState({creating: true, edit_race_id: id, race_focus: null})}}
                    />
              </div>
          )
      } else{
          return(
              <SingleRace race={this.state.race_focus} close_function={this.close_race} />
          )
      }

    };
}

export default RacePage;