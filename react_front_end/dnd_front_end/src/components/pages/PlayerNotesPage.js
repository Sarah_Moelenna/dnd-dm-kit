import React, { Component } from 'react';
import ListCollections from '../notes/ListCollections'
import SingleCollection from '../notes/SingleCollection'
import { DND_GET } from '../shared/DNDRequests';

class PlayerNotesPage extends Component {

    state = {
        collections: [],
        collection_focus: null,
        page_focus: null
      }


    shouldComponentUpdate(nextProps, nextState) {
      if (this.state != nextState){
        return true
      }
      if (this.props === undefined || nextProps === undefined){
        return true;
      }
      if (this.props.active === undefined){
        return true
      }
      if (this.props.active != nextProps.active){
        return true
      }
      return false
  }


    display_collection = (collection_id) => {

      for(var i = 0; i < this.props.collections.length; i++){
          if(this.props.collections[i].id == collection_id){
              this.setState({collection_focus: this.props.collections[i]})
              this.display_page(this.props.collections[i].pages[0].id)
          }
      }

    };


    close_collection = () => {
        this.setState({ collection_focus: null, page_focus: null})
    };

    display_page = (page_id) => {
      DND_GET(
        '/session/page/' + page_id,
        (jsondata) => {
          this.setState({ page_focus: jsondata})
        },
        null
      )
    };

  close_page = () => {
      this.setState({ page_focus: null})
  };

    render(){
        if (this.props.active == false){
            return null;
        }

        if (this.state.collection_focus == null){

            return(
                <div>
                    <h2>Shared Collections</h2>
                    <ListCollections player_read={true} collections={this.props.collections} refresh_function={() => {}}  view_collection_function={this.display_collection}/>
                </div>
            );

        } else{
            return(
                <div>
                    <SingleCollection 
                      key={this.state.collection_focus.id}
                      read={true}
                      collection={this.state.collection_focus}
                      refresh_function={() => {}}
                      close_collection_fuction={this.close_collection}
                      display_page={this.display_page}
                      refresh_page_in_focus={() => {}}
                      close_page={this.close_page}
                      page_focus={this.state.page_focus}
                      override_function={() => {}}
                      player_read={true}
                    />
                </div>
            )
        }
    };
}

export default PlayerNotesPage;