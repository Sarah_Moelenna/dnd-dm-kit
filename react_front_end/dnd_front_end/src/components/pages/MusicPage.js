import React, { Component } from 'react';
import ListAudioItem from '.././music/ListAudioItem';
import DownloadForm from '.././music/DownloadForm';
import UploadForm from '../music/UploadForm';
import FilterMusic from '.././music/FilterMusic'
import Collapsible from '.././shared/Collapsible';
import Paginator from '.././shared/Paginator';
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class MusicPage extends Component {

    state = {
        audio_items: [],
        filters: {},
        page: 1,
        total_pages: 1,
      }

    componentDidMount() {
        this.refresh_music(this.state.filters, 1)
    };

    basic_refresh_music = () => {
      this.refresh_music({}, 1)
    }
            
    refresh_music = (filters, page) => {
        var params = filters
        params['page'] = page
        params['results_per_page'] = 20

        DND_GET(
          '/audio?' + stringify(params),
          (jsondata) => {
            this.setState({ audio_items: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_music(new_filters, 1))
    };

    set_page = (page) => {
      this.refresh_music(this.state.filters, page)
    };

    clear_filters = () => {
      var new_filters = []
      this.setState({ filters: new_filters, page: 1, audio_items: [], total_pages: 1}, this.refresh_music(new_filters, 1))
  };

    render(){
      if (this.props.active == false){
        return null;
      }

            return(
                <div className="music-page">
                    <Collapsible contents={<DownloadForm refresh_function={this.basic_refresh_music}/>} title="Upload From Youtube"/>
                    <Collapsible contents={<UploadForm refresh_function={this.basic_refresh_music}/>} title="Upload File"/>
                    <Collapsible contents={<FilterMusic filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Audio"/>
                    <h2>Current Audio <i className="fas fa-sync clickable-icon" onClick={this.basic_refresh_music}></i></h2>
                    <ListAudioItem audio_items={this.state.audio_items} refresh_function={this.basic_refresh_music}/>
                    <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                </div>
            );
    };
}

export default MusicPage;