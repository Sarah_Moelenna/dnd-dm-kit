import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { stringify } from 'query-string';
import Cookies from 'universal-cookie';
import { DND_GET } from '../shared/DNDRequests';
import { get_api_url } from '../shared/Config';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card'

const cookies = new Cookies();
const REACT_APP_API_URL = get_api_url()

const get_random_image = () => {
    var images = [
        REACT_APP_API_URL + "/static/background_1.jpg",
        REACT_APP_API_URL + "/static/background_2.jpg",
        REACT_APP_API_URL + "/static/background_3.jpg",
        REACT_APP_API_URL + "/static/background_4.jpg",
        REACT_APP_API_URL + "/static/background_5.jpg"
    ]
    return images[Math.floor(Math.random() * images.length)];
};

class LoginPage extends Component {

    state = {
        login_type: null,
        username: null,
        password: null,
        error: false,
        session_key: null,
        background: get_random_image(),
        character: null,
        session_data: null
      }

    componentWillMount() {
        var existing_auth = cookies.get('auth_token')
        if(existing_auth != undefined && existing_auth!= null && existing_auth!= "null" && existing_auth != "undefined"){
            this.validate_auth_token(existing_auth)
        }
    
        var existing_session = cookies.get('session_id')
        if(existing_session != undefined && existing_session!= null && existing_session!= "null" && existing_session != "undefined"){
            this.validate_session_id(existing_session)
        }
    }

    validate_auth_token = (auth_token) => {
        var url = '/validate?auth_token=' + auth_token
        DND_GET(
            url,
            (jsondata) => {
                cookies.set('auth_token', jsondata.token, { path: '/' });
                this.props.set_user_type("DM", null)
            },
            (e) => { 
                cookies.remove('auth_token', { path: '/' });
            }
        )
    }

    validate_session_id = (session_id) => {
        var url = '/session/login?session_id=' + session_id

        DND_GET(
            url,
            (jsondata) => {
                this.setState({session_data: jsondata, login_type: "CHARACTER_SELECTION"})
            },
            (e) => { 
                cookies.remove('session_id', { path: '/' });
            }
        )
    }

    login = (event) => {
        var params = {}
        params['username'] = this.state.username
        params['password'] = this.state.password
        var url = '/login?' + stringify(params)

        DND_GET(
            url,
            (jsondata) => {
                cookies.set('auth_token', jsondata.token, { path: '/' });
                this.props.set_user_type("DM", null)
            },
            (e) => { this.setState({error: true}) }
        )

        event.preventDefault();
    };

    session_login = (event) => {
        var params = {}
        params['session_key'] = this.state.session_key
        var url = '/session/login?' + stringify(params)

        DND_GET(
            url,
            (jsondata) => {
                this.setState({session_data: jsondata, login_type: "CHARACTER_SELECTION"})
            },
            (e) => { this.setState({error: true}) }
        )

        event.preventDefault();
    };

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    character_select = (character) => {
        cookies.set('session_id', this.state.session_data.id, { path: '/' });
        this.props.set_user_type("USER", this.state.session_data.id, character)
    };

    observer_select = () => {
        cookies.set('session_id', this.state.session_data.id, { path: '/' });
        this.props.set_user_type("USER", this.state.session_data.id)
    };

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    

    render(){


        return(
            <div className="login" style={{ 
                backgroundImage: 'url("' + this.state.background + '")'
                }}>
                {this.state.login_type == null &&
                    <div className="login-selection">
                        <div className="option" onClick={() => {this.setState({login_type: "DM_LOGIN"})}}>
                            <i className="fas fa-hat-wizard"></i>
                            <p>Login as Dungeon Master!</p>
                        </div>
                        <div className="option" onClick={() => {this.setState({login_type: "PLAYER_LOGIN"})}}>
                            <i className="fas fa-user"></i>
                            <p>Join a Game Session!</p>
                        </div>
                    </div>
                }
                {this.state.login_type == "DM_LOGIN" &&
                    <div className="login-selection">
                        <h2><i className="fas fa-chevron-left clickable-icon" onClick={() => {this.setState({login_type: null, error: false, username: null, password: null})}}></i> Dungeon Master Login</h2>
                        <Form onSubmit={this.login}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control required value={this.state.username} name="username" type="text" placeholder="Username" onChange={this.handleChange} />
                            </Form.Group>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Password</Form.Label>
                                <Form.Control required value={this.state.password} name="password" type="password" placeholder="Password" onChange={this.handleChange} />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                    Submit
                            </Button>
                            {
                                this.state.error == true &&
                                <p className="error">Login Failed</p>
                            }
                        </Form>
                    </div>
                }
                {this.state.login_type == "PLAYER_LOGIN" &&
                    <div className="login-selection">
                        <h2><i className="fas fa-chevron-left clickable-icon" onClick={() => {this.setState({login_type: null, error: false, session_key: null})}}></i> Join a Session</h2>
                        <Form onSubmit={(e) => {this.session_login(e)}}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Session Key</Form.Label>
                                <Form.Control required value={this.state.session_key} name="session_key" type="text" placeholder="dragon-kobold-silver-gem" onChange={this.handleChange} />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                    Submit
                            </Button>
                            {
                                this.state.error == true &&
                                <p className="error">Unable To Join Session</p>
                            }
                        </Form>
                    </div>
                }
                {this.state.login_type == "CHARACTER_SELECTION" &&
                <div className="login-selection">
                    <div className="character-selection">
                        <h2><i className="fas fa-chevron-left clickable-icon" onClick={() => {this.setState({login_type: null, error: false, session_key: null, session_data: null})}}></i> Select A Character</h2>
                        {this.state.session_data.characters.map((character) => (
                                <Card  style={{ maxWidth: '18rem' }} className="character"  key={character.id}>
                                    <Row>
                                        <Col xs={8} md={8}>
                                            <Card.Body>
                                                <Card.Title>{character.name}</Card.Title>
                                                { character.url != null &&                                        
                                                    <Button className="btn btn-primary" onClick={() => {this.character_select(character)}}>Select Character</Button>
                                                }
                                            </Card.Body>
                                        </Col>
                                        <Col xs={4} md={4}>
                                            <Card.Img className="avatar" variant="top" src={character.avatar} />
                                        </Col>
                                    </Row>
                                </Card>
                        ))}
                        <Card  style={{ maxWidth: '18rem' }} className="character">
                            <Row>
                                <Col xs={12} md={12}>
                                    <Card.Body>
                                        <Card.Title>Observer</Card.Title>                                     
                                        <Button className="btn btn-primary" onClick={() => {this.observer_select()}}>Watch as Observer</Button>
                                    </Card.Body>
                                </Col>
                            </Row>
                        </Card>
                    </div>
                </div>
            }
            </div>
        );
    };
}

export default LoginPage;