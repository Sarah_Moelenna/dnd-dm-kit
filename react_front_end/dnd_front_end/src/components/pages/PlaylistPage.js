import React, { Component } from 'react';
import ListPlaylists from '../playlist/ListPlaylists';
import CreatePlaylist from '../playlist/CreatePlaylist';
import SinglePlaylist from '../playlist/SinglePlaylist';
import Collapsible from '../shared/Collapsible';
import FilterPlaylist from '../playlist/FilterPlaylist'
import { stringify } from 'query-string';
import { DND_GET } from '../shared/DNDRequests';
import Paginator from '../shared/Paginator'
class PlaylistPage extends Component {

    state = {
        playlists: [],
        playlist_focus: null,
        filters: {},
        page: 1,
        total_pages: 1,
      }

    componentDidMount() {
        this.quick_refresh()
    };

    shouldComponentUpdate(nextProps, nextState) {
      if (this.state != nextState){
          return true
      }
      if (this.props === undefined || nextProps === undefined){
          return true;
      }
      if (this.props.active === undefined){
          return true
      }
      if (this.props.active != nextProps.active){
          return true
      }
      return false
  }
            
    refresh_playlists = (page, filters) => {
      var params = filters
      params['page'] = page
      DND_GET(
        '/playlist?' + stringify(params),
        (jsondata) => {
          this.setState({ playlists: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
        },
        null
      )
    };

    quick_refresh = () => {
      this.refresh_playlists(this.state.page, this.state.filters)
    };

    set_page = (page) => {
      this.refresh_playlists(page, this.state.filters)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_playlists(1, new_filters))
    };

    display_playlist = (playlist_id) => {
      DND_GET(
        '/playlist/' + playlist_id,
        (jsondata) => {
          this.setState({ playlist_focus: jsondata})
        },
        null
      )
    };

    refresh_playlist_in_focus = () => {
      this.quick_refresh()

      DND_GET(
        '/playlist/' + this.state.playlist_focus.id,
        (jsondata) => {
          this.setState({ playlist_focus: jsondata})
        },
        null
      )
    };

    close_playlist = () => {
        this.setState({ playlist_focus: null})
    };

    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.playlist_focus == null){
          return(
              <div className="playlist-page">
                  <Collapsible contents={<CreatePlaylist refresh_function={this.quick_refresh}/>} title="Create New Playlist"/>
                  <Collapsible contents={<FilterPlaylist filters={this.state.filters} update_filter_function={this.set_filters}/>} title="Filter Playlists"/>
                  <h2>Current Playlists <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
                  <ListPlaylists playlists={this.state.playlists} refresh_function={this.quick_refresh} view_playlist_function={this.display_playlist}/>
                  <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
              </div>
          )
      } else{
        return(
          <div className="playlist-page">
            <SinglePlaylist
              playlist={this.state.playlist_focus}
              refresh_function={this.refresh_playlist_in_focus}
              close_playlist_fuction={this.close_playlist}
            />
          </div>
        )
      }

    };
}

export default PlaylistPage;