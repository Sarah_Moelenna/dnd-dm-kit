import React, { Component } from 'react';
import ListBackgroundItem from '.././backgrounds/ListBackgroundItem';
import SingleBackground from '.././backgrounds/SingleBackground'
import FilterBackground from '../backgrounds/FilterBackground';
import CreateBackground from '../backgrounds/CreateBackground';
import Paginator from '.././shared/Paginator';
import Collapsible from '.././shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class BackgroundPage extends Component {

    state = {
        backgrounds: [],
        page: 1,
        background_focus: null,
        creating: false,
        edit_background_id: null,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null
      }

      componentWillMount() {
        if(this.props.pathname.includes("/background")){
          var initial_id = this.props.pathname.replace("/background", "").replace("/", "")
          if (initial_id != ""){
            this.display_background(initial_id)
          }
        }
      };

    componentDidMount() {
        this.refresh_backgrounds(this.state.page, this.state.filters)
    };
            
    refresh_backgrounds = (page, filters) => {
        var params = filters
        params['page'] = page

        DND_GET(
          '/background?' + stringify(params),
          (jsondata) => {
            this.setState({ backgrounds: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    display_background = (background_id) => {
      DND_GET(
        '/background/' + background_id,
        (jsondata) => {
          this.setState({ background_focus: jsondata, creating: false, edit_background_id: null})
        },
        null
      )
    };

    close_background = () => {
        this.setState({ background_focus: null})
    };

    set_page = (page) => {
      this.refresh_backgrounds(page, this.state.filters)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_backgrounds(1, new_filters))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_backgrounds(1, []))
    };

    quick_refresh = () => {
      this.refresh_backgrounds(this.state.page, this.state.filters)
    };


    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.creating == true){
        return(
          <div className="background-page">
            <CreateBackground edit_background_id={this.state.edit_background_id} close_creating_fuction={() => {this.setState({creating: false, edit_background_id: null})}}/>
          </div>
        )
      } else if (this.state.background_focus == null){
        return(
          <div className="background-page">
            <Collapsible contents={<FilterBackground filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Backgrounds"/>
            <h2>Current Backgrounds <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
            <button className="btn btn-primary" onClick={() => {this.setState({creating: true})}}>Create Background</button>
            <ListBackgroundItem
              backgrounds={this.state.backgrounds}
              view_background_function={this.display_background}
              refresh_function={this.quick_refresh}
              edit_function={(id) => {this.setState({creating: true, edit_background_id: id, background_focus: null})}}
              />
            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
          </div>
        )
      } else{
        return(
          <div className="background-page">
            <SingleBackground background={this.state.background_focus} close_background_fuction={this.close_background}/>
          </div>
        )
      }
    };
}

export default BackgroundPage;