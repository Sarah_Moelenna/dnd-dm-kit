import React, { Component } from 'react';
import ListFeatItem from '../feats/ListFeatItem';
import SingleFeat from '../feats/SingleFeat'
import FilterFeat from '../feats/FilterFeat';
import CreateFeat from '../feats/CreateFeat';
import Paginator from '../shared/Paginator';
import Collapsible from '../shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET } from '../shared/DNDRequests';

class FeatPage extends Component {

    state = {
        feats: [],
        page: 1,
        feat_focus: null,
        creating: false,
        edit_feat_id: null,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null
      }

      componentWillMount() {
        if(this.props.pathname.includes("/feat")){
          var initial_id = this.props.pathname.replace("/feat", "").replace("/", "")
          if (initial_id != ""){
            this.display_feat(initial_id)
          }
        }
      };

    componentDidMount() {
        this.refresh_feats(this.state.page, this.state.filters)
    };
            
    refresh_feats = (page, filters) => {
        var params = filters
        params['page'] = page

        DND_GET(
          '/feat?' + stringify(params),
          (jsondata) => {
            this.setState({ feats: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    display_feat = (feat_id) => {
      DND_GET(
        '/feat/' + feat_id,
        (jsondata) => {
          this.setState({ feat_focus: jsondata, creating: false, edit_feat_id: null})
        },
        null
      )
    };

    close_feat = () => {
        this.setState({ feat_focus: null})
    };

    set_page = (page) => {
      this.refresh_feats(page, this.state.filters)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_feats(1, new_filters))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_feats(1, []))
    };

    quick_refresh = () => {
      this.refresh_feats(this.state.page, this.state.filters)
    };


    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.creating == true){
        return(
          <div className="feat-page">
            <CreateFeat edit_feat_id={this.state.edit_feat_id} close_creating_fuction={() => {this.setState({creating: false, edit_feat_id: null})}}/>
          </div>
        )
      } else if (this.state.feat_focus == null){
        return(
          <div className="feat-page">
            <Collapsible contents={<FilterFeat filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Feats"/>
            <h2>Current Feats <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
            <button className="btn btn-primary" onClick={() => {this.setState({creating: true})}}>Create Feat</button>
            <ListFeatItem
              feats={this.state.feats}
              view_feat_function={this.display_feat}
              refresh_function={this.quick_refresh}
              edit_function={(id) => {this.setState({creating: true, edit_feat_id: id, feat_focus: null})}}
              />
            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
          </div>
        )
      } else{
        return(
          <div className="feat-page">
            <SingleFeat feat={this.state.feat_focus} close_feat_fuction={this.close_feat}/>
          </div>
        )
      }
    };
}

export default FeatPage;