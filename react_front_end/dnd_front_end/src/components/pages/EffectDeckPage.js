import React, { Component } from 'react';
import ListEffectDecks from '../effect_deck/ListEffectDecks';
import CreateEffectDeck from '../effect_deck/CreateEffectDeck';
import SingleEffectDeck from '../effect_deck/SingleEffectDeck';
import Collapsible from '../shared/Collapsible';
import FilterEffectDeck from '../effect_deck/FilterEffectDeck'
import { stringify } from 'query-string';
import { DND_GET } from '../shared/DNDRequests';
import Paginator from '../shared/Paginator'
class EffectDeckPage extends Component {

    state = {
        effect_decks: [],
        effect_deck_focus: null,
        filters: {},
        page: 1,
        total_pages: 1,
      }

    componentDidMount() {
        this.quick_refresh()
    };

    shouldComponentUpdate(nextProps, nextState) {
      if (this.state != nextState){
          return true
      }
      if (this.props === undefined || nextProps === undefined){
          return true;
      }
      if (this.props.active === undefined){
          return true
      }
      if (this.props.active != nextProps.active){
          return true
      }
      return false
  }
            
    refresh_effect_decks = (page, filters) => {
      var params = filters
      params['page'] = page
      DND_GET(
        '/effect_deck?' + stringify(params),
        (jsondata) => {
          this.setState({ effect_decks: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
        },
        null
      )
    };

    quick_refresh = () => {
      this.refresh_effect_decks(this.state.page, this.state.filters)
    };

    set_page = (page) => {
      this.refresh_effect_decks(page, this.state.filters)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_effect_decks(1, new_filters))
    };

    display_effect_deck = (effect_deck_id) => {
      DND_GET(
        '/effect_deck/' + effect_deck_id,
        (jsondata) => {
          this.setState({ effect_deck_focus: jsondata})
        },
        null
      )
    };

    refresh_effect_deck_in_focus = () => {
      this.quick_refresh()

      DND_GET(
        '/effect_deck/' + this.state.effect_deck_focus.id,
        (jsondata) => {
          this.setState({ effect_deck_focus: jsondata})
        },
        null
      )
    };

    close_effect_deck = () => {
        this.setState({ effect_deck_focus: null})
    };

    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.effect_deck_focus == null){
          return(
              <div className="effect_deck-page">
                  <Collapsible contents={<CreateEffectDeck refresh_function={this.quick_refresh}/>} title="Create New EffectDeck"/>
                  <Collapsible contents={<FilterEffectDeck filters={this.state.filters} update_filter_function={this.set_filters}/>} title="Filter EffectDecks"/>
                  <h2>Current EffectDecks <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
                  <ListEffectDecks effect_decks={this.state.effect_decks} refresh_function={this.quick_refresh} view_effect_deck_function={this.display_effect_deck}/>
                  <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
              </div>
          )
      } else{
        return(
          <div className="effect_deck-page">
            <SingleEffectDeck
              effect_deck={this.state.effect_deck_focus}
              refresh_function={this.refresh_effect_deck_in_focus}
              close_effect_deck_function={this.close_effect_deck}
            />
          </div>
        )
      }

    };
}

export default EffectDeckPage;