import React, { Component } from 'react';
import ListMapItem from '.././map/ListMaps';
import SingleMap from '.././map/SingleMap'
import FilterMap from '.././map/FilterMap'
import CreateMap from '.././map/CreateMap'
import Paginator from '.././shared/Paginator'
import Collapsible from '.././shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class MappingPage extends Component {

    state = {
        maps: [],
        page: 1,
        map_focus: null,
        total_pages: 1,
        filters: {}
      }

    componentDidMount() {
        this.refresh_maps(this.state.page, this.state.filters)
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
      if(prevProps.override.map_focus == undefined && this.props.override.map_focus != undefined){
        this.display_map(this.props.override.map_focus)
        this.props.override_function({})
      }
    }

    shouldComponentUpdate(nextProps, nextState) {
      if (this.state != nextState){
          return true
      }
      if (this.props === undefined || nextProps === undefined){
          return true;
      }
      if (this.props.active === undefined){
          return true
      }
      if (this.props.active != nextProps.active){
          return true
      }
      if(this.props.override.map_focus == undefined && nextProps.override.map_focus != undefined){
          return true
      }
      return false
  }
            
    refresh_maps = (page, filters) => {
        var params = filters
        params['page'] = page

        DND_GET(
          '/map?' + stringify(params),
          (jsondata) => {
            this.setState({ maps: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )
    };

    basic_refresh_all = () => {
      this.refresh_maps(1, {})
    }

    display_map = (map_id) => {
      DND_GET(
        '/map/' + map_id,
        (jsondata) => {
          this.setState({ map_focus: jsondata})
        },
        null
      )
    };

    refresh_focus = () => {
      this.display_map(this.state.map_focus.id)
    }

    close_map = () => {
        this.setState({ map_focus: null})
    };

    set_page = (page) => {
      this.refresh_maps(page, this.state.filters)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_maps(1, new_filters))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_maps(1, []))
    };

    render(){
      if (this.props.active == false){
        return null;
      }

            if (this.state.map_focus == null){
              return(
                <div className="map-page">
                    <Collapsible contents={<CreateMap refresh_function={this.basic_refresh_all}/>} title="Create New Map"/>
                    <Collapsible contents={<FilterMap filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter maps"/>
                    <h2>Current Maps <i className="fas fa-sync clickable-icon" onClick={this.basic_refresh_all}></i></h2>
                    <ListMapItem maps={this.state.maps} view_map_function={this.display_map} refresh_function={this.basic_refresh_all}/>
                    <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                </div>
              )
          } else{
              return(
                <div className="map-page">
                  <SingleMap refresh_function={this.refresh_focus} map={this.state.map_focus} close_map_fuction={this.close_map} override_function={this.props.override_function}/>
                </div>
              )
          }
    };
}

export default MappingPage;