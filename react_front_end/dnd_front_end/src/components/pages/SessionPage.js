import React, { Component } from 'react';
import DisplaySession from '../dmsession/DisplaySession';
import Collapsible from '../shared/Collapsible';
import { Map } from '../socket/Map';
import { Combat } from '../socket/Combat';

class SessionPage extends Component {

    state = {
        current_tab: null,
        tab_availability: {}
    }


    shouldComponentUpdate(nextProps, nextState) {
        if (this.state != nextState){
            return true
        }
        if (this.props === undefined || nextProps === undefined){
            return true;
        }
        if (this.props.active === undefined){
            return true
        }
        if (this.props.active != nextProps.active){
            return true
        }
        return false
    }

    set_tab_availability = (tab, availability) => {
        var new_tab_availability = this.state.tab_availability
        var new_current_tab = this.state.current_tab
        if(availability == false){
            delete new_tab_availability[tab]
        } else {
            new_tab_availability[tab] = true
        }
        if(new_current_tab == null && availability == true){
            new_current_tab = tab
        }
        if(availability == false && this.state.current_tab == tab){
            var keys = Object.keys(new_tab_availability)
            if(keys.length > 0){
                new_current_tab=keys[0]
            } else {
                new_current_tab = null
            }
        }
        this.setState({tab_availability: new_tab_availability, current_tab: new_current_tab})
    }

    get_tabs = () => {
        return Object.keys(this.state.tab_availability)
    }

    render(){
        var visisbility = this.props.active == true ? "visisble" : "hidden"
        var tabs = this.get_tabs()
        return(
            <div className={"player-page " + visisbility}>
                {this.props.is_observer != true &&
                    <h1>Session</h1>
                }
                {this.props.is_player == false && this.props.active_campaign != null &&
                    <Collapsible contents={
                        <DisplaySession 
                            session={this.props.active_campaign.session}
                            refresh_function={this.props.refresh_function}
                        />}
                        title="Current Session"
                    />                    
                }
                <div className="tab-controls">
                    {tabs.map((tab, index) => (
                        <div 
                            key={index}
                            className={"tab-control" + (index == 0 ? " first-tab" : "") + (index == tabs.length-1 ? " last-tab" : "") + (tab == this.state.current_tab ? " active" : "")}
                            onClick={() => {this.setState({current_tab: tab})}
                        }>
                            {tab}
                        </div>
                    ))}
                </div>
                <div className={"session-tab" + (this.state.current_tab == "Dungeon Map" ? " active" : " inactive")}>
                    <Map set_tab_availability={this.set_tab_availability}/>
                </div>
                <div className={"session-tab" + (this.state.current_tab == "Combat" ? " active" : " inactive")}>
                    <Combat set_tab_availability={this.set_tab_availability}/>
                </div>
            </div>
        );
    };
}

export default SessionPage;