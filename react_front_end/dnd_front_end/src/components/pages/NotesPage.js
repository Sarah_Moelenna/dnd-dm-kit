import React, { Component } from 'react';
import CreateCollection from '.././notes/CreateCollection';
import FilterCollections from '.././notes/FilterCollections';
import ListCollections from '.././notes/ListCollections'
import SingleCollection from '.././notes/SingleCollection'
import Collapsible from '.././shared/Collapsible';
import Paginator from '.././shared/Paginator';
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class NotesPage extends Component {

    state = {
        collections: [],
        filters: {},
        page: 1,
        total_pages: 1,
        collection_focus: null,
        collection_read: false,
        page_focus: null
      }

    componentDidMount() {
        this.refresh_collections(this.state.filters, 1)
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
      if((prevProps.override.page_focus == undefined && this.props.override.page_focus != undefined) || (prevProps.override.collection_focus == undefined && this.props.override.collection_focus != undefined)){
        if(this.props.override.page_focus != undefined){
          this.display_collection(this.props.override.collection_focus, true, true)
        } else {
          this.display_collection(this.props.override.collection_focus, true)
        }
        this.display_page(this.props.override.page_focus)
        this.props.override_function({})
      }
    }

    shouldComponentUpdate(nextProps, nextState) {
      if (this.state != nextState){
        return true
      }
      if (this.props === undefined || nextProps === undefined){
        return true;
      }
      if (this.props.active === undefined){
        return true
      }
      if (this.props.active != nextProps.active){
        return true
      }
      if(this.props.override.page_focus == undefined && nextProps.override.page_focus != undefined){
        return true
      }
      if(this.props.override.collection_focus == undefined && nextProps.override.collection_focus != undefined){
        return true
      }
      return false
  }

    basic_refresh_collections = () => {
        this.refresh_collections({}, 1)
    }
            
    refresh_collections = (filters, page) => {
        var params = filters
        params['page'] = page
        params['results_per_page'] = 20

        DND_GET(
          '/collection?' + stringify(params),
          (jsondata) => {
            this.setState({ collections: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_collections(new_filters, 1))
    };

    set_page = (page) => {
      this.refresh_collections(this.state.filters, page)
    };

    clear_filters = () => {
      var new_filters = []
      this.setState({ filters: new_filters, page: 1, audio_items: [], total_pages: 1}, this.refresh_collections(new_filters, 1))
    };

    display_collection = (collection_id, read, no_auto_display) => {

      DND_GET(
        '/collection/' + collection_id,
        (jsondata) => {
          if(read == true && jsondata.pages.length > 0){
            this.setState({ collection_focus: jsondata, collection_read: read})
            if(no_auto_display == undefined || no_auto_display == false){
              this.display_page(jsondata.pages[0].id)
            }
          } else {
            this.setState({ collection_focus: jsondata, collection_read: read})
          }
        },
        null
      )

    };

    refresh_collection_in_focus = () => {
      this.basic_refresh_collections()

      DND_GET(
        '/collection/' + this.state.collection_focus.id,
        (jsondata) => {
          this.setState({ collection_focus: jsondata})
        },
        null
      )

    };

    close_collection = () => {
        this.setState({ collection_focus: null, page_focus: null})
    };

    display_page = (page_id) => {
      DND_GET(
        '/page/' + page_id,
        (jsondata) => {
          this.setState({ page_focus: jsondata})
        },
        null
      )
    };

  refresh_page_in_focus = () => {
      this.basic_refresh_collections()
      DND_GET(
        '/page/' + this.state.page_focus.id,
        (jsondata) => {
          this.setState({ page_focus: jsondata})
        },
        null
      )
  };

  close_page = () => {
      this.setState({ page_focus: null})
  };

    render(){
        if (this.props.active == false){
            return null;
        }

        if (this.state.collection_focus == null){

            return(
                <div>
                    <Collapsible contents={<CreateCollection refresh_function={this.basic_refresh_collections}/>} title="Create Collection"/>
                    <Collapsible contents={<FilterCollections filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Collections"/>
                    <h2>Current Collections <i className="fas fa-sync clickable-icon" onClick={this.basic_refresh_collections}></i></h2>
                    <ListCollections collections={this.state.collections} refresh_function={this.basic_refresh_collections}  view_collection_function={this.display_collection}/>
                    <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                </div>
            );

        } else{
            return(
                <div>
                    <SingleCollection 
                      key={this.state.collection_focus.id}
                      read={this.state.collection_read}
                      collection={this.state.collection_focus}
                      refresh_function={this.refresh_collection_in_focus}
                      close_collection_fuction={this.close_collection}
                      display_page={this.display_page}
                      refresh_page_in_focus={this.refresh_page_in_focus}
                      close_page={this.close_page}
                      page_focus={this.state.page_focus}
                      override_function={this.props.override_function}
                    />
                </div>
            )
        }
    };
}

export default NotesPage;