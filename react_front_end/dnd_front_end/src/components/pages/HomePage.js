import React, { Component } from 'react';

class HomePage extends Component {

    render(){
        if (this.props.active == false){
            return null;
        }

            return(
                <div>
                    <h1>Welcome to the DM's Toolkit</h1>
                    <p>An all in one solution for the DM with too many chrome tabs.</p>
                    <h2>Features:</h2>
                    <p><b>Campaign Management</b> - create and manage your ongoing campaigns.</p>
                    <p><b>Music Bot</b> - download and loop music in discord from youtube.</p>
                    <p><b>DNDBeyond Integration</b> - track characters in your campaign on demand and import monsters from your owned content.</p>
                    <p><b>Encounter Builder</b> - build encounters from owned monsters, and run them from the encounter control panel.</p>
                    <p><b>Battlemap</b> - create battlemaps connected to encounters that your players can interact with.</p>
                    <p><b>Campaign Organisation</b> - keep all your notes in one place with links to maps, items, monsters and music.</p>
                </div>
            );
    };
}

export default HomePage;