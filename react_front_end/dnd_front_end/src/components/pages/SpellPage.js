import React, { Component } from 'react';
import ListSpellItem from '.././spell/ListSpellItem';
import CreateSpell from '../spell/CreateSpell'
import SingleSpell from '../spell/SingleSpell'
import FilterSpell from '.././spell/FilterSpell'
import Paginator from '.././shared/Paginator'
import Collapsible from '.././shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class SpellPage extends Component {

    state = {
        spells: [],
        page: 1,
        spell_focus: null,
        creating: false,
        edit_spell_id: null,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null
      }

    componentWillMount() {
      if(this.props.pathname.includes("/spell")){
        var initial_id = this.props.pathname.replace("/spell", "").replace("/", "")
        if (initial_id != ""){
          this.display_spell(initial_id)
        }
      }
    };

    componentDidMount() {
        this.refresh_spells(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
      if(prevProps.override.spell_focus == undefined && this.props.override.spell_focus != undefined){
        this.display_spell(this.props.override.spell_focus)
        this.props.override_function({})
      }
    }
            
    refresh_spells = (page, filters, sort_by, sort_value) => {
        var params = filters
        params['page'] = page
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/spell?' + stringify(params),
          (jsondata) => {
            this.setState({ spells: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    display_spell = (spell_id) => {
      DND_GET(
        '/spell/' + spell_id,
        (jsondata) => {
          this.setState({ spell_focus: jsondata})
        },
        null
      )
    };

    close_spell = () => {
        this.setState({ spell_focus: null})
    };

    set_page = (page) => {
      this.refresh_spells(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_spells(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_spells(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_spells(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
      this.setState(
        {sort_by: sort_by, sort_value: sort_value},
        this.refresh_spells(this.state.page, this.state.filters, sort_by, sort_value)
      )
    };


    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.creating == true){
        return(
          <div className="spell-page">
            <CreateSpell edit_spell_id={this.state.edit_spell_id} close_creating_fuction={() => {this.setState({creating: false, edit_spell_id: null})}}/>
          </div>
        )
      } else if (this.state.spell_focus == null){
        return(
          <div className="spell-page">
            <Collapsible contents={<FilterSpell filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Spells"/>
            <h2>Current Spells <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
            <button className="btn btn-primary" onClick={() => {this.setState({creating: true})}}>Create Spell</button>
            <ListSpellItem
              spells={this.state.spells}
              view_spell_function={this.display_spell}
              refresh_function={this.quick_refresh}
              edit_spell_function={(id) => {this.setState({creating: true, edit_spell_id: id, spell_focus: null})}}
              set_sort={this.set_sort}
              current_sort_by={this.state.sort_by}
              current_sort_value={this.state.sort_value}
              />
            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
          </div>
        )
      } else{
        return(
          <div className="spell-page">
            <SingleSpell spell={this.state.spell_focus} close_spell_fuction={this.close_spell}/>
          </div>
        )
      }
    };
}

export default SpellPage;