import React, { Component } from 'react';
import ListImages from '../images/ListImages';
import Paginator from '.././shared/Paginator'
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class ImageUploadsPage extends Component {

    state = {
        images: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null
      }


    componentDidMount() {
        this.refresh_images(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

            
    refresh_images = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/user-images?' + stringify(params),
          (jsondata) => {
            this.setState({ images: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_images(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
      this.setState(
        {sort_by: sort_by, sort_value: sort_value},
        this.refresh_images(this.state.page, this.state.filters, sort_by, sort_value)
      )
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_images(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_images(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_images(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };


    render(){
        if (this.props.active == false){
            return null;
        }

        return(
            <div className="images-page">
                <h2>My Images <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
                <ListImages
                    images={this.state.images}
                    refresh_function={this.quick_refresh}
                    set_sort={this.set_sort}
                    current_sort_by={this.state.sort_by}
                    current_sort_value={this.state.sort_value}
                    />
                <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
            </div>
        )
    };
}

export default ImageUploadsPage;