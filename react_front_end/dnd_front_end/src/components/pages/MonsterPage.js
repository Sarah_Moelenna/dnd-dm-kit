import React, { Component } from 'react';
import ListMonsterItem from '.././monster/ListMonsterItem';
import SingleMonster from '.././monster/SingleMonster'
import FilterMonster from '.././monster/FilterMonster'
import CreateMonster from '.././monster/CreateMonster'
import Paginator from '.././shared/Paginator'
import Collapsible from '.././shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class MonsterPage extends Component {

    state = {
        monsters: [],
        page: 1,
        monster_focus: null,
        total_pages: 1,
        filters: {},
        creating: false,
        edit_monster_id: null,
        sort_by: null,
        sort_value: null
      }

    componentWillMount() {
      if(this.props.pathname.includes("/monster")){
        var initial_id = this.props.pathname.replace("/monster", "").replace("/", "")
        if (initial_id != ""){
          this.display_monster(initial_id)
          }
        }
    };

    componentDidMount() {
        this.refresh_monsters(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
      if(prevProps.override.monster_focus == undefined && this.props.override.monster_focus != undefined){
        this.display_monster(this.props.override.monster_focus)
        this.props.override_function({})
      }
    }
            
    refresh_monsters = (page, filters, sort_by, sort_value) => {
        var params = filters
        params['page'] = page
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/monsters?' + stringify(params),
          (jsondata) => {
            this.setState({ monsters: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    display_monster = (monster_id) => {
      DND_GET(
        '/monster/' + monster_id,
        (jsondata) => {
          this.setState({ monster_focus: jsondata, creating: false, edit_monster_id: null})
        },
        null
      )
    };

    close_monster = () => {
        this.setState({ monster_focus: null})
    };

    set_page = (page) => {
      this.refresh_monsters(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
      this.setState(
        {sort_by: sort_by, sort_value: sort_value},
        this.refresh_monsters(this.state.page, this.state.filters, sort_by, sort_value)
      )
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_monsters(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_monsters(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_monsters(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };


    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.creating == true){
        return(
          <div className="monster-page">
            <CreateMonster edit_monster_id={this.state.edit_monster_id} close_creating_fuction={() => {this.setState({creating: false, edit_monster_id: null})}}/>
          </div>
        )
      } else if (this.state.monster_focus == null){
        return(
          <div className="monster-page">
            <Collapsible contents={<FilterMonster filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Monsters"/>
            <h2>Current Monsters <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
            <button className="btn btn-primary" onClick={() => {this.setState({creating: true})}}>Create Monster</button>
            <ListMonsterItem
              monsters={this.state.monsters}
              view_monster_function={this.display_monster}
              refresh_function={this.quick_refresh}
              edit_function={(id) => {this.setState({creating: true, edit_monster_id: id, monster_focus: null})}}
              set_sort={this.set_sort}
              current_sort_by={this.state.sort_by}
              current_sort_value={this.state.sort_value}
              />
            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
          </div>
        )
      } else{
        return(
          <div className="monster-page">
            <SingleMonster monster={this.state.monster_focus} close_monster_fuction={this.close_monster}/>
          </div>
        )
      }
    };
}

export default MonsterPage;