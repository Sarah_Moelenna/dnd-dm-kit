import React, { Component } from 'react';
import SingleBattlemap from '../battlemap/SingleBattlemap';
import CreateBattlemap from '../battlemap/CreateBattlemap';
import FilterBattlemap from '../battlemap/FilterBattlemap';
import ListBattlemaps from '../battlemap/ListBattlemaps';
import { DND_GET } from '../shared/DNDRequests';
import Paginator from '.././shared/Paginator'
import Collapsible from '.././shared/Collapsible';
import { stringify } from 'query-string';
import EditBattlemap from '../battlemap/EditBattlemap';

const example_battlemap = {
    rows: 9,
    columns: 15,
    image: "images/2b6910d1-72b6-4e8c-8275-7cc420417947_room_1.jpg",
    objects: [
        {
            image: "https://media-waterdeep.cursecdn.com/avatars/thumbnails/8/442/264/315/636306375308939571.jpeg",
            x: null,
            y: null
        },
        {
            image: "https://media-waterdeep.cursecdn.com/avatars/thumbnails/0/24/243/315/636238958915127190.jpeg",
            x: null,
            y: null
        },
        {
            image: "https://media-waterdeep.cursecdn.com/avatars/thumbnails/278/53/169/315/636601988998266586.png",
            x: null,
            y: null
        }
    ],
    blocked_cells: ["5-1", "3-1", "1-5", "1-4", "1-3"]
}

class BattleMapPage extends Component {

    state = {
        battlemaps: [],
        page: 1,
        map_focus: null,
        total_pages: 1,
        filters: {}
    }

    componentDidMount() {
        this.refresh_maps(this.state.page, this.state.filters)
    };

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state != nextState){
            return true
        }
        if (this.props === undefined || nextProps === undefined){
            return true;
        }
        if (this.props.active === undefined){
            return true
        }
        if (this.props.active != nextProps.active){
            return true
        }
        return false
    }

    refresh_maps = (page, filters) => {
        var params = filters
        params['page'] = page

        DND_GET(
          '/battlemap?' + stringify(params),
          (jsondata) => {
            this.setState({ battlemaps: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )
    };
  

    basic_refresh_all = () => {
        this.refresh_maps(1, {})
    }

    display_map = (map_id) => {
        DND_GET(
          '/battlemap/' + map_id,
          (jsondata) => {
            this.setState({ map_focus: jsondata})
          },
          null
        )
    };

    refresh_focus = () => {
        this.display_map(this.state.map_focus.id)
    }

    close_map = () => {
        this.setState({ map_focus: null})
    };

    set_page = (page) => {
      this.refresh_maps(page, this.state.filters)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_maps(1, new_filters))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_maps(1, []))
    };

    render(){
        if (this.props.active == false){
          return null;
        }
  
        if (this.state.map_focus == null){
            return(
                <div className="battlemap">
                    <Collapsible contents={<CreateBattlemap refresh_function={this.basic_refresh_all}/>} title="Create New Battlemap"/>
                    <Collapsible contents={<FilterBattlemap filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter battlemaps"/>
                    <h2>Current Maps <i className="fas fa-sync clickable-icon" onClick={this.basic_refresh_all}></i></h2>
                    <ListBattlemaps maps={this.state.battlemaps} view_map_function={this.display_map} refresh_function={this.basic_refresh_all}/>
                    <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
                </div>
            )
        } else{
            return(
                <div className="battlemap">
                <EditBattlemap refresh_function={this.refresh_focus} battlemap={this.state.map_focus} close_map_fuction={this.close_map}/>
                </div>
            )
        }
      };
}

export default BattleMapPage;