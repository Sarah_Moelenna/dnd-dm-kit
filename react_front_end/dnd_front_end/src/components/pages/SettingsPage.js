import React, { Component } from 'react';
import ListSettingsGroups from '.././settings/ListSettingsGroups'
import Importer from '.././settings/Importer'
import { DND_GET } from '.././shared/DNDRequests';

class SettingsPage extends Component {

    state = {
        settings: [],
    }

    componentDidMount() {
        this.refresh_settings()
    };
            
    refresh_settings = () => {
      DND_GET(
        '/user-settings',
        (jsondata) => {
          this.setState({ settings: jsondata})
        },
        null
      )
    };

    render(){
        if (this.props.active == false){
            return null;
          }

            return(
                <div className="settings-page">
                    <h2>Settings <i className="fas fa-sync clickable-icon" onClick={this.refresh_settings}></i></h2>
                    <ListSettingsGroups settings_groups={this.state.settings} refresh_function={this.refresh_settings} user={this.props.user}/>
                    <h2>Import Data</h2>
                    <Importer/>
                </div>
            );
    };
}

export default SettingsPage;