import React, { Component } from 'react';
import ListContentCollections from '../content_collection/ListContentCollections';
import FilterContentCollection from '.././content_collection/FilterContentCollection'
import Paginator from '.././shared/Paginator'
import Collapsible from '.././shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class ContentCollectionPage extends Component {

    state = {
        content_collections: [],
        page: 1,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null
      }


    componentDidMount() {
        this.refresh_content_collections(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

            
    refresh_content_collections = (page, filters, sort_by, sort_value) => {
        var params = filters
        
        params['page'] = page
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/content-collection?' + stringify(params),
          (jsondata) => {
            this.setState({ content_collections: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    set_page = (page) => {
      this.refresh_content_collections(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
      this.setState(
        {sort_by: sort_by, sort_value: sort_value},
        this.refresh_content_collections(this.state.page, this.state.filters, sort_by, sort_value)
      )
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_content_collections(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_content_collections(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_content_collections(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };


    render(){
        if (this.props.active == false){
            return null;
        }

        return(
            <div className="content-collection-page">
            <Collapsible contents={<FilterContentCollection filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Content Collections"/>
            <h2>Global Content Collections <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
            <ListContentCollections
                content_collections={this.state.content_collections}
                view_content_collection_function={this.display_content_collection}
                refresh_function={this.quick_refresh}
                set_sort={this.set_sort}
                current_sort_by={this.state.sort_by}
                current_sort_value={this.state.sort_value}
                display_type="read"
                />
            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
            </div>
        )
    };
}

export default ContentCollectionPage;