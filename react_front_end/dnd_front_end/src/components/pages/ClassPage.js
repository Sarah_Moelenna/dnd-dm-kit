import React, { Component } from 'react';
import ListClassItem from '../class/ListClassItem';
import {CreateClass} from '../class/CreateClass'
import SingleClass from '../class/SingleClass'
import FilterClass from '../class/FilterClass'
import Paginator from '../shared/Paginator'
import Collapsible from '../shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET } from '../shared/DNDRequests';
import CreateSubClass from '../class/CreateSubClass';
import SingleSubClass from '../class/SingleSubClass';

class ClassPage extends Component {

    state = {
        classes: [],
        page: 1,
        class_focus: null,
        sub_class_focus: null,
        creating: false,
        edit_class_id: null,
        edit_sub_class_id: null,
        edit_sub_class_class_id: null,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null
      }

    componentWillMount() {
      if(this.props.pathname.includes("/class")){
        var initial_id = this.props.pathname.replace("/class", "").replace("/", "")
        if (initial_id != ""){
          this.display_class(initial_id)
        }
      }
    };

    componentDidMount() {
        this.refresh_classes(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
      if(prevProps.override.class_focus == undefined && this.props.override.class_focus != undefined){
        this.display_class(this.props.override.class_focus)
        this.props.override_function({})
      }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state != nextState){
            return true
        }
        if (this.props === undefined || nextProps === undefined){
            return true;
        }
        if (this.props.active === undefined){
            return true
        }
        if (this.props.active != nextProps.active){
            return true
        }
        return false
    }
            
    refresh_classes = (page, filters, sort_by, sort_value) => {
        var params = filters
        params['page'] = page
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/class?' + stringify(params),
          (jsondata) => {
            this.setState({ classes: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    display_class = (class_id) => {
      DND_GET(
        '/class/' + class_id,
        (jsondata) => {
          this.setState({ class_focus: jsondata})
        },
        null
      )
    };

    close_class = () => {
        this.setState({ class_focus: null})
    };

    display_sub_class = (sub_class_id) => {
      DND_GET(
        '/sub-class/' + sub_class_id,
        (jsondata) => {
          this.setState({ sub_class_focus: jsondata})
        },
        null
      )
    };

    close_sub_class = () => {
        this.setState({ sub_class_focus: null})
    };

    set_page = (page) => {
      this.refresh_classes(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_classes(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_classes(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_classes(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
      this.setState(
        {sort_by: sort_by, sort_value: sort_value},
        this.refresh_classes(this.state.page, this.state.filters, sort_by, sort_value)
      )
    };


    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.creating == 'CLASS'){
        return(
          <div className="class-page">
            <CreateClass edit_class_id={this.state.edit_class_id} close_creating_fuction={() => {this.setState({creating: false, edit_class_id: null})}}/>
          </div>
        )
      } else if (this.state.creating == 'SUBCLASS'){
        return(
          <div className="class-page">
            <CreateSubClass
              edit_sub_class_id={this.state.edit_sub_class_id}
              close_creating_fuction={() => {this.setState({creating: false, edit_sub_class_id: null, edit_sub_class_class_id: null})}}
              base_class_id={this.state.edit_sub_class_class_id}
            />
          </div>
        )
      } else if (this.state.class_focus == null && this.state.sub_class_focus == null){
        return(
          <div className="class-page">
            <Collapsible contents={<FilterClass filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Classs"/>
            <h2>Current Classes <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
            <button className="btn btn-primary" onClick={() => {this.setState({creating: 'CLASS'})}}>Create Class</button>
            <ListClassItem
              classes={this.state.classes}
              view_class_function={this.display_class}
              refresh_function={this.quick_refresh}
              edit_class_function={(id) => {this.setState({creating: 'CLASS', edit_class_id: id, class_focus: null})}}
              view_sub_class_function={this.display_sub_class}
              edit_sub_class_function={(id, base_id) => {this.setState({creating: 'SUBCLASS', edit_sub_class_id: id, sub_class_focus: null, edit_sub_class_class_id: base_id})}}
              set_sort={this.set_sort}
              current_sort_by={this.state.sort_by}
              current_sort_value={this.state.sort_value}
              />
            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
          </div>
        )
      } else if (this.state.class_focus){
        return(
          <div className="class-page">
            <SingleClass class={this.state.class_focus} close_class_fuction={this.close_class}/>
          </div>
        )
      } else if (this.state.sub_class_focus){
        return(
          <div className="class-page">
            <SingleSubClass subclass={this.state.sub_class_focus} close_class_fuction={this.close_sub_class}/>
          </div>
        )
      } else {
        return null
      }
    };
}

export default ClassPage;