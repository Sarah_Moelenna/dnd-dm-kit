import React, { Component } from 'react';
import CreateEncounter from '.././encounter/CreateEncounter';
import ListEncounters from '.././encounter/ListEncounters';
import SingleEncounter from '.././encounter/SingleEncounter';
import Collapsible from '.././shared/Collapsible';
import FilterEncounters from '.././encounter/FilterEncounters'
import { stringify } from 'query-string';
import ListCombats from '.././encounter/combat/ListCombats';
import SingleCombat from '.././encounter/combat/SingleCombat'
import { DND_GET, DND_POST } from '.././shared/DNDRequests';
import { SocketContext } from '../socket/Socket';
import { get_auth_token } from '../shared/AuthToken';
import Paginator from '.././shared/Paginator'

class EncounterPage extends Component {

    static contextType = SocketContext

    state = {
        encounters: [],
        encounter_focus: null,
        encounter_page: 1,
        encounter_total_pages: 1,
        combats: [],
        combat_focus: null,
        filters: {},
        combat_share_id: null
    }
    
    componentDidMount() {
        this.quick_refresh_encounters()
        this.refresh_combats()

        const socket = this.context
        socket.on("combat_move", data => {
            var new_combat = {...this.state.combat_focus}
            for(var i = 0; i < new_combat.state.members.length; i++){
                if(new_combat.state.members[i].id == data.id){
                    new_combat.state.members[i].x = data.values.x
                    new_combat.state.members[i].y = data.values.y
                }
            }
            this.set_combat_focus(new_combat, false)
        });
        socket.once("combat", data => {
            this.set_combat_focus(data, false)
        });
        socket.on("combat_id", combat_id => {
            this.setState({combat_share_id: combat_id})
        });
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.override.encounter_focus == undefined && this.props.override.encounter_focus != undefined){
          this.display_encounter(this.props.override.encounter_focus)
          this.props.override_function({})
        }
      }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state != nextState){
            return true
        }
        if (this.props === undefined || nextProps === undefined){
            return true;
        }
        if (this.props.active === undefined){
            return true
        }
        if (this.props.active != nextProps.active){
            return true
        }
        if(this.props.override.encounter_focus == undefined && nextProps.override.encounter_focus != undefined){
            return true
        }
        return false
    }

    clear_share = () => {
        this.setState({combat_share_id: null})
    }
            
    refresh_encounters = (filters, page) => {
        var params = filters
        params['page'] = page
        params['results_per_page'] = 10
        DND_GET(
            '/encounter?' + stringify(params),
            (jsondata) => {
                this.setState({ encounters: jsondata.results, encounter_total_pages: jsondata.total_pages, encounter_page: jsondata.current_page})
            },
            null
        )
    };

    quick_refresh_encounters = () => {
        this.refresh_encounters(this.state.filters, this.state.encounter_page)
    }

    set_encounter_page = (page) => {
        this.refresh_encounters(this.state.filters, page)
    };

    refresh_combats = () => {
        DND_GET(
            '/combat/',
            (jsondata) => {
                this.setState({ combats: jsondata})
            },
            null
        )
    };

    display_encounter = (encounter_id) => {
        DND_GET(
            '/encounter/' + encounter_id,
            (jsondata) => {
                this.setState({ encounter_focus: jsondata, combat_focus: null})
            },
            null
        )
    };

    display_combat = (combat_id) => {
        DND_GET(
            '/combat/' + combat_id,
            (jsondata) => {
                this.setState({ combat_focus: jsondata, encounter_focus: null})
            },
            null
        )
    };

    set_filters = (filter, value) => {
        var new_filters = this.state.filters
        new_filters[filter] = value
        this.setState({ filters: new_filters, encounter_page: 1}, this.refresh_encounters(new_filters, 1))
    };

    clear_filters = () => {
        this.setState({ filters: {}, page: 1}, this.refresh_encounters({}, 1))
      };

    refresh_encounter_in_focus = () => {
        this.quick_refresh_encounters()
        DND_GET(
            '/encounter/' + this.state.encounter_focus.id,
            (jsondata) => {
                this.setState({ encounter_focus: jsondata})
            },
            null
        )
      };

    refresh_combat_in_focus = () => {
        DND_GET(
            '/combat' + this.state.combat_focus.id,
            (jsondata) => {
                this.setState({ combat_focus: jsondata})
            },
            null
        )
    };
  
    close_encounter = () => {
        this.setState({ encounter_focus: null})
        this.quick_refresh_encounters()
        this.refresh_combats()
    };

    close_combat = () => {
        const socket = this.context
        socket.emit("combat_clear");
        this.setState({ combat_focus: null, combat_share_id: null})
        this.quick_refresh_encounters()
        this.refresh_combats()
    };

    set_combat_focus = (combat, share, callback) => {
        if(combat != undefined && combat != null){
            var data = {
                state: combat.state,
            }

            DND_POST(
                '/combat/' + combat.id,
                data,
                (response) => {
                    if(share == true){
                        const socket = this.context
                        socket.emit("combat_update", combat.id, get_auth_token());
                    }
                    if (callback){
                        callback()
                    }
                },
                null
            )
        }
        this.setState({ combat_focus: combat, encounter_focus: null})
    };

    render(){
        if (this.props.active == false){
            return null;
          }

        if (this.state.encounter_focus == null && this.state.combat_focus == null){
            return(
                <div className="encounter-page">
                    <h2>Ongoing Combats <i className="fas fa-sync clickable-icon" onClick={this.refresh_combats}></i></h2>
                    <ListCombats combats={this.state.combats} refresh_function={this.refresh_combats} view_combat_function={this.display_combat}/>
                    <h2>All Encounters <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh_encounters}></i></h2>
                    <Collapsible contents={<CreateEncounter refresh_function={this.quick_refresh_encounters}/>} title="Create New Encounter"/>
                    <Collapsible contents={<FilterEncounters filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Encounters"/>
                    <ListEncounters encounters={this.state.encounters} refresh_function={this.quick_refresh_encounters} view_encounter_function={this.display_encounter}/>
                    <Paginator current_page={this.state.encounter_page} total_pages={this.state.encounter_total_pages} page_change_function={this.set_encounter_page}/>
                </div>
            )
        } else if (this.state.encounter_focus != null){
            return(
                <div>
                    <SingleEncounter
                        encounter={this.state.encounter_focus}
                        refresh_function={this.refresh_encounter_in_focus}
                        close_encounter_fuction={this.close_encounter}
                        set_combat_function={this.set_combat_focus}
                    />
                </div>
            )
        } else if (this.state.combat_focus != null){
            return(
                <div className="combat">
                    <SingleCombat
                        combat={this.state.combat_focus}
                        refresh_function={this.refresh_combat_in_focus}
                        update_combat_function={this.set_combat_focus}
                        close_combat_fuction={this.close_combat}
                        roll_dice_function={this.props.roll_dice_function}
                        share={this.state.combat_share_id == this.state.combat_focus.id}
                        clear_share={this.clear_share}
                    />
                </div>
            )
        }

};
}

export default EncounterPage;