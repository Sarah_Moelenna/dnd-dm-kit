import React, { Component } from 'react';
import ListCampaigns from '.././campaign/ListCampaigns';
import CreateCampaign from '.././campaign/CreateCampaign';
import SingleCampaign from '.././campaign/SingleCampaign';
import Collapsible from '.././shared/Collapsible';
import FilterCampaign from '.././campaign/FilterCampign'
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '.././shared/Paginator'
class CampaignPage extends Component {

    state = {
        campaigns: [],
        campaign_focus: null,
        filters: {},
        page: 1,
        total_pages: 1,
      }

    componentDidMount() {
        this.quick_refresh()
    };

    shouldComponentUpdate(nextProps, nextState) {
      if (this.state != nextState){
          return true
      }
      if (this.props === undefined || nextProps === undefined){
          return true;
      }
      if (this.props.active === undefined){
          return true
      }
      if (this.props.active != nextProps.active){
          return true
      }
      return false
  }
            
    refresh_campaigns = (page, filters) => {
      var params = filters
      params['page'] = page
      DND_GET(
        '/campaign?' + stringify(params),
        (jsondata) => {
          this.setState({ campaigns: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
        },
        null
      )
    };

    quick_refresh = () => {
      this.refresh_campaigns(this.state.page, this.state.filters)
    };

    set_page = (page) => {
      this.refresh_campaigns(page, this.state.filters)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_campaigns(1, new_filters))
    };

    display_campaign = (campaign_id) => {
      DND_GET(
        '/campaign/' + campaign_id,
        (jsondata) => {
          this.setState({ campaign_focus: jsondata})
        },
        null
      )
    };

    refresh_campaign_in_focus = () => {
      this.quick_refresh()

      DND_GET(
        '/campaign/' + this.state.campaign_focus.id,
        (jsondata) => {
          this.setState({ campaign_focus: jsondata})
        },
        null
      )
    };

    close_campaign = () => {
        this.setState({ campaign_focus: null})
    };

    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.campaign_focus == null){
          return(
              <div className="campaign-page">
                  <Collapsible contents={<CreateCampaign refresh_function={this.quick_refresh}/>} title="Create New Campaign"/>
                  <Collapsible contents={<FilterCampaign filters={this.state.filters} update_filter_function={this.set_filters}/>} title="Filter Campaigns"/>
                  <h2>Current Campaigns <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
                  <ListCampaigns campaigns={this.state.campaigns} refresh_function={this.quick_refresh} view_campaign_function={this.display_campaign}/>
                  <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
              </div>
          )
      } else{
          return(
              <div className="campaign-page">
                  <SingleCampaign
                    campaign={this.state.campaign_focus}
                    refresh_function={this.refresh_campaign_in_focus}
                    close_campaign_fuction={this.close_campaign}
                    bot_state={this.props.bot_state}
                    override_function={this.props.override_function}
                    roll_dice_function={this.props.roll_dice_function}
                  />
              </div>
          )
      }

    };
}

export default CampaignPage;