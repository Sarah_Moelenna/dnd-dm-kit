import React, { Component } from 'react';
import ListSpellLists from '.././spell_list/ListSpellList';
import CreateSpellList from '.././spell_list/CreateSpellList';
import SingleSpellList from '.././spell_list/SingleSpellList';
import Collapsible from '.././shared/Collapsible';
import { DND_GET } from '.././shared/DNDRequests';
import Paginator from '.././shared/Paginator'
class SpellListPage extends Component {

    state = {
        spell_lists: [],
        spell_list_focus: null,
        page: 1,
        total_pages: 1,
      }

    componentDidMount() {
        this.quick_refresh()
    };

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state != nextState){
            return true
        }
        if (this.props === undefined || nextProps === undefined){
            return true;
        }
        if (this.props.active === undefined){
            return true
        }
        if (this.props.active != nextProps.active){
            return true
        }
        return false
    }
            
    refresh_spell_lists = (page) => {
        let params = {}
        params['page'] = page
        DND_GET(
            '/spell-list',
            (jsondata) => {
            this.setState({ spell_lists: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
            },
            null
        )
    };

    quick_refresh = () => {
      this.refresh_spell_lists(this.state.page, this.state.filters)
    };

    set_page = (page) => {
      this.refresh_spell_lists(page, this.state.filters)
    };

    display_spell_list = (spell_list_id) => {
      DND_GET(
        '/spell-list/' + spell_list_id,
        (jsondata) => {
          this.setState({ spell_list_focus: jsondata})
        },
        null
      )
    };

    refresh_spell_list_in_focus = () => {
      DND_GET(
        '/spell-list/' + this.state.spell_list_focus.id,
        (jsondata) => {
          this.setState({ spell_list_focus: jsondata})
          this.quick_refresh()
        },
        null
      )
    };

    close_spell_list = () => {
        this.setState({ spell_list_focus: null})
    };

    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.spell_list_focus == null){
          return(
              <div className="spell_list-page">
                  <Collapsible contents={<CreateSpellList refresh_function={this.quick_refresh}/>} title="Create New Spell List"/>
                  <h2>Current Spell Lists <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
                  <ListSpellLists spell_lists={this.state.spell_lists} refresh_function={this.quick_refresh} view_spell_list_function={this.display_spell_list}/>
                  <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
              </div>
          )
      } else{
          return(
              <div className="spell_list-page">
                  <SingleSpellList
                    spell_list={this.state.spell_list_focus}
                    refresh_function={this.refresh_spell_list_in_focus}
                    close_spell_list_fuction={this.close_spell_list}
                  />
              </div>
          )
      }

    };
}

export default SpellListPage;