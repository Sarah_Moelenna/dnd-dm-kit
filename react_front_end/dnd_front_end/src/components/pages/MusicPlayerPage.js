import React, { Component, useContext, useRef, useEffect, useState } from 'react';
import { DND_GET } from '.././shared/DNDRequests';
import { get_api_url } from '../shared/Config';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {SocketContext} from '.././socket/Socket';
import { v4 as uuidv4 } from 'uuid';
const REACT_APP_API_URL = get_api_url()

class MusicPlayerPage extends Component {

    render(){
      if (this.props.active == false){
        return null;
      }

        return(
            <MusicPlayer/>
        );
    };
}

const AudioPlayer = ({audioRef, current_file, flag_state, on_end, on_loaded}) => {
  return (
    <audio
      ref={audioRef}
      key={current_file + "-" + flag_state}
      src={REACT_APP_API_URL  + "/audio/file/" + current_file}
      autoPlay controls
      onEnded={(e) => on_end()}
      onLoadedData={(e) => {on_loaded()}}
    />
  )
}

const SoundEffectPlayer = ({file_name, on_end_function, id}) => {
  return (
    <audio
      src={REACT_APP_API_URL  + "/audio/file/" + file_name}
      autoPlay
      onEnded={(e) => {on_end_function({"id": id, "file": null})}}
      onLoadedData={(e) => {}}
    />
  )
}

const SoundEffects = () => {

  const socket = useContext(SocketContext);
  const [effect, setEffect] = useState(null);
  const prevEffectsRef = useRef();

  useEffect(() => {
    if(effect != null){
      let new_effects = {...prevEffectsRef.current}
      if(effect.file != null){
        new_effects[effect.id] = effect.file
      } else {
        delete new_effects[effect.id]
      }
      prevEffectsRef.current = new_effects;
      setEffect(null)
    }
  }, [effect]);

  useEffect(() => {
    socket.on("sound_effect_play", file => {
      setEffect({"file": file, "id": uuidv4()})
    });
  }, []);

  return (
    <div>
      {prevEffectsRef.current && Object.keys(prevEffectsRef.current).map((effect_id) => (
        <div key={"sound_effect_id-"+effect_id}>
          <SoundEffectPlayer file_name={prevEffectsRef.current[effect_id]} on_end_function={setEffect} id={effect_id}/>
        </div>
      ))}
    </div>
  );
}

const TrackDisplay = ({ prefix, playlist, tracks, current_song, current_file, update_current_song, flag_state, type}) => {

  const socket = useContext(SocketContext);
  const audioRef = useRef(null);
  const [volume, setVolume] = useState(0.5);
  socket.emit("song_change_" + type.toLowerCase(), current_song);

  function on_end(){
    let next_number = 0
    for(let i = 0; i < tracks.length; i++){
      if(tracks[i].file_name == current_file){
        next_number = i + 1
        break;
      }
    }
    if(next_number > tracks.length - 1){
      next_number = 0
    }
    update_current_song(
      { 
        "type": type,
        "current_song_file": tracks[next_number].file_name,
        "current_song_name": tracks[next_number].display_name,
        "change_flag": !flag_state
      }
    )
  }

  function on_loaded(){
    if(audioRef.current != null){
      audioRef.current.volume = volume
    }
  }

  let volume_listener = type.toLowerCase() + "_volume"

  useEffect(() => {
    socket.on(volume_listener, value => {
      if(audioRef.current != null){
        audioRef.current.volume = value
      }
      setVolume(value)
    });
    socket.emit(volume_listener, volume)
  }, []);

  if(current_song == undefined || current_song == null){
    return null
  }

  return (
    <Col xs={6} md={6}>
      <div>
        {playlist &&
          <h3>{prefix}: {playlist}</h3>
        }
        <table className="track-table"><tbody>
          {tracks.map((track) => (
                <tr key={track.id} className={track.display_name == current_song ? 'track active' : 'track'}>
                    <td>
                        <span>{track.display_name}</span>
                    </td>
                </tr>
          ))}
          </tbody></table>
          <AudioPlayer
            audioRef={audioRef}
            current_file={current_file}
            flag_state={flag_state}
            on_end={on_end}
            on_loaded={on_loaded}
          />
      </div>
    </Col>
  );
}

class MusicPlayer extends Component {

  state = {
      music_tracks: [],
      music_playlist: null,
      music_current_song_file: null,
      music_current_song_name: null,
      music_change_flag: true,

      ambience_tracks: [],
      ambience_playlist: null,
      ambience_current_song_file: null,
      ambience_current_song_name: null,
      ambience_change_flag: true,
    }

  componentDidMount() {
      this.refresh_music()
      this.timer = setInterval(()=> this.refresh_music(), 2500);
  };

  componentWillUnmount() {
    clearInterval(this.timer)
    this.timer = null;
  }
          
  refresh_music = () => {

      DND_GET(
        '/user/music',
        (jsondata) => {
          let new_data = {}
          // music
          if(this.state.music_current_song_file == null && jsondata.music.tracks.length > 0){
            new_data.music_tracks = jsondata.music.tracks
            new_data.music_playlist = jsondata.music.playlist
            new_data.music_current_song_file = jsondata.music.tracks[0].file_name
            new_data.music_current_song_name = jsondata.music.tracks[0].display_name
          }
          if(JSON.stringify(jsondata.music.tracks) != JSON.stringify(this.state.music_tracks)){
            new_data.music_tracks = jsondata.music.tracks
            new_data.music_playlist = jsondata.music.playlist
            new_data.music_current_song_file = null
            new_data.music_current_song_name = null
          }

          //ambiance
          if(this.state.ambience_current_song_file == null && jsondata.ambience.tracks.length > 0){
            new_data.ambience_tracks = jsondata.ambience.tracks
            new_data.ambience_playlist = jsondata.ambience.playlist
            new_data.ambience_current_song_file = jsondata.ambience.tracks[0].file_name
            new_data.ambience_current_song_name = jsondata.ambience.tracks[0].display_name
          }
          if(JSON.stringify(jsondata.ambience.tracks) != JSON.stringify(this.state.ambience_tracks)){
            new_data.ambience_tracks = jsondata.ambience.tracks
            new_data.ambience_playlist = jsondata.ambience.playlist
            new_data.ambience_current_song_file = null
            new_data.ambience_current_song_name = null
          }
          if (Object.keys(new_data).length == 0){
            return
          }
          this.setState(new_data)
        },
        null
      )
  };

  update_current_song = (data) => {
    if(data.type =='MUSIC'){
      this.setState(
        {
          music_current_song_file: data.current_song_file,
          music_current_song_name: data.current_song_name,
          music_change_flag: data.change_flag
        }
      )
    }
    else if(data.type =='AMBIENCE'){
      this.setState(
        {
          ambience_current_song_file: data.current_song_file,
          ambience_current_song_name: data.current_song_name,
          ambience_change_flag: data.change_flag
        }
      )
    }
  }

  render(){
    if (this.props.active == false){
      return null;
    }

          return(
              <div className="music-page">
                  <h1>Audio Player</h1>
                  <Row>
                      <TrackDisplay
                        prefix="Music"
                        playlist={this.state.music_playlist}
                        tracks={this.state.music_tracks}
                        current_song={this.state.music_current_song_name}
                        current_file={this.state.music_current_song_file}
                        type="MUSIC"
                        flag_state={this.state.music_change_flag}
                        update_current_song={this.update_current_song}
                      />
                        <TrackDisplay
                          prefix="Ambience"
                          playlist={this.state.ambience_playlist}
                          tracks={this.state.ambience_tracks}
                          current_song={this.state.ambience_current_song_name}
                          current_file={this.state.ambience_current_song_file}
                          type="AMBIENCE"
                          flag_state={this.state.ambience_change_flag}
                          update_current_song={this.update_current_song}
                        />
                  </Row>
                  <SoundEffects/>
              </div>
          );
  };
}

export default MusicPlayerPage;