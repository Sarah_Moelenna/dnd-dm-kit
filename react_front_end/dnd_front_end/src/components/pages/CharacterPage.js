import React, { Component } from 'react';
import ListCharacters from '../dnd_character/ListCharacters';
import {SingleCharacter, ChooseCampaign} from '../dnd_character/SingleCharacter'
import FilterCharacter from '../dnd_character/FilterCharacter'
import Paginator from '../shared/Paginator'
import Collapsible from '../shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET, DND_POST } from '../shared/DNDRequests';

class CharacterPage extends Component {

    state = {
        characters: [],
        page: 1,
        character_focus: null,
        creating: false,
        edit_character_id: null,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null,
        view_mode: false
      }

    componentWillMount() {
      if(this.props.pathname.includes("/character")){
        var initial_id = this.props.pathname.replace("/character", "").replace("/", "")
        if (initial_id != ""){
          this.display_character(initial_id)
        }
      }
    };

    shouldComponentUpdate(nextProps, nextState) {
      if (this.state != nextState){
          return true
      }
      if (this.props === undefined || nextProps === undefined){
          return true;
      }
      if (this.props.active === undefined){
          return true
      }
      if (this.props.active != nextProps.active){
          return true
      }
      return false
  }

    componentDidMount() {
        this.refresh_characters(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };
            
    refresh_characters = (page, filters, sort_by, sort_value) => {
        var params = filters
        params['page'] = page
        if(sort_by != null){
          if(sort_value == "DESC"){
            params['sort'] = "-" + sort_by
          } else{
            params['sort'] = sort_by
          }
        }
        DND_GET(
          '/dnd-character?' + stringify(params),
          (jsondata) => {
            this.setState({ characters: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    create_character = (campaign_id) => {
      DND_POST(
          '/dnd-character',
          {data: {campaign_id: campaign_id}},
          (json_data) => {this.setState({edit_character_id: json_data.id, creating:true})},
          null
      )
  }

    display_character = (character_id) => {
      DND_GET(
        '/dnd-character/' + character_id,
        (jsondata) => {
          this.setState({ character_focus: jsondata})
        },
        null
      )
    };

    close_character = () => {
        this.setState({ character_focus: null})
    };

    set_page = (page) => {
      this.refresh_characters(page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_characters(1, new_filters, this.state.sort_by, this.state.sort_value))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_characters(1, [], this.state.sort_by, this.state.sort_value))
    };

    quick_refresh = () => {
      this.refresh_characters(this.state.page, this.state.filters, this.state.sort_by, this.state.sort_value)
    };

    set_sort = (sort_by, sort_value) => {
      this.setState(
        {sort_by: sort_by, sort_value: sort_value},
        this.refresh_characters(this.state.page, this.state.filters, sort_by, sort_value)
      )
    };


    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.creating == true && !this.state.edit_character_id && this.state.view_mode == false){
        return(
          <div className="character-page">
            <ChooseCampaign create_function={this.create_character} close_creating_fuction={() => {this.setState({creating: false, edit_character_id: null})}}/>
          </div>
        )
      } else if (this.state.creating == true && this.state.edit_character_id && this.state.view_mode == false){
        return(
          <div className="character-page">
            <SingleCharacter
              edit_character_id={this.state.edit_character_id}
              close_creating_fuction={() => {this.setState({creating: false, edit_character_id: null})}}
              view_mode={false}
              change_view_mode={() => {this.setState({view_mode: true})}}
            />
          </div>
        )
      } else if (this.state.edit_character_id == null){
        return(
          <div className="character-page">
            <Collapsible contents={<FilterCharacter filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Characters"/>
            <h2>Current Characters <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
            <button className="btn btn-primary" onClick={() => (this.setState({creating: true}))}>Create Character</button>
            <ListCharacters
              characters={this.state.characters}
              view_character_function={(id) => {this.setState({creating: true, edit_character_id: id, character_focus: null, view_mode:true})}}
              refresh_function={this.quick_refresh}
              edit_character_function={(id) => {this.setState({creating: true, edit_character_id: id, character_focus: null, view_mode:false})}}
              set_sort={this.set_sort}
              current_sort_by={this.state.sort_by}
              current_sort_value={this.state.sort_value}
              />
            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
          </div>
        )
      } else{
        return(
          <div className="character-page">
            <SingleCharacter
              edit_character_id={this.state.edit_character_id}
              close_creating_fuction={() => {this.setState({creating: false, edit_character_id: null})}}
              view_mode={true}
              change_view_mode={() => {this.setState({view_mode: false})}}
            />
          </div>
        )
      }
    };
}

export default CharacterPage;