import React, { Component } from 'react';
import ListItemItem from '.././items/ListItemItem';
import {SingleItem} from '.././items/SingleItem'
import FilterItem from '../items/FilterItem';
import CreateItem from '../items/CreateItem';
import Paginator from '.././shared/Paginator';
import Collapsible from '.././shared/Collapsible';
import { stringify } from 'query-string';
import { DND_GET } from '.././shared/DNDRequests';

class ItemPage extends Component {

    state = {
        items: [],
        page: 1,
        item_focus: null,
        creating: false,
        edit_item_id: null,
        total_pages: 1,
        filters: {},
        sort_by: null,
        sort_value: null
      }

      componentWillMount() {
        if(this.props.pathname.includes("/item")){
          var initial_id = this.props.pathname.replace("/item", "").replace("/", "")
          if (initial_id != ""){
            this.display_item(initial_id)
          }
        }
      };

    componentDidMount() {
        this.refresh_items(this.state.page, this.state.filters)
    };
            
    refresh_items = (page, filters) => {
        var params = filters
        params['page'] = page

        DND_GET(
          '/item?' + stringify(params),
          (jsondata) => {
            this.setState({ items: jsondata.results, total_pages: jsondata.total_pages, page: jsondata.current_page})
          },
          null
        )

    };

    display_item = (item_id) => {
      DND_GET(
        '/item/' + item_id,
        (jsondata) => {
          this.setState({ item_focus: jsondata, creating: false, edit_item_id: null})
        },
        null
      )
    };

    close_item = () => {
        this.setState({ item_focus: null})
    };

    set_page = (page) => {
      this.refresh_items(page, this.state.filters)
    };

    set_filters = (filter, value) => {
      var new_filters = this.state.filters
      new_filters[filter] = value
      this.setState({ filters: new_filters, page: 1}, this.refresh_items(1, new_filters))
    };

    clear_filters = () => {
      this.setState({ filters: [], page: 1}, this.refresh_items(1, []))
    };

    quick_refresh = () => {
      this.refresh_items(this.state.page, this.state.filters)
    };


    render(){
      if (this.props.active == false){
        return null;
      }

      if (this.state.creating == true){
        return(
          <div className="item-page">
            <CreateItem edit_item_id={this.state.edit_item_id} close_creating_fuction={() => {this.setState({creating: false, edit_item_id: null})}}/>
          </div>
        )
      } else if (this.state.item_focus == null){
        return(
          <div className="item-page">
            <Collapsible contents={<FilterItem filters={this.state.filters} update_filter_function={this.set_filters} clear_filter_function={this.clear_filters}/>} title="Filter Items"/>
            <h2>Current Items <i className="fas fa-sync clickable-icon" onClick={this.quick_refresh}></i></h2>
            <button className="btn btn-primary" onClick={() => {this.setState({creating: true})}}>Create Item</button>
            <ListItemItem
              items={this.state.items}
              view_item_function={this.display_item}
              refresh_function={this.quick_refresh}
              edit_function={(id) => {this.setState({creating: true, edit_item_id: id, item_focus: null})}}
              />
            <Paginator current_page={this.state.page} total_pages={this.state.total_pages} page_change_function={this.set_page}/>
          </div>
        )
      } else{
        return(
          <div className="item-page">
            <SingleItem item={this.state.item_focus} close_item_fuction={this.close_item}/>
          </div>
        )
      }
    };
}

export default ItemPage;