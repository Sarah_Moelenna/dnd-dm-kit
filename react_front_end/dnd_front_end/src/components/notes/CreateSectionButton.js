import React, { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const CreateSectionButton = ({order_position, create_function}) => {

    const [isVisible, setIsVisible] = useState(false);

    const toggle = () => setIsVisible(!isVisible);

    function process_selection(order_position, section_type){
        toggle()
        create_function(order_position, section_type)
    }

    var popover = <Popover className="create-section-popover">
        <Popover.Title>Insert New Section</Popover.Title>
        <Popover.Content className="content">
            <p>Select Column Count</p>
            <Row>
                <Col xs={3} md={3} className="column-button">
                    <div onClick={() => process_selection(order_position, "COLUMN_1")}>
                        <a>1</a>
                    </div>
                </Col>
                <Col xs={3} md={3} className="column-button">
                    <div onClick={() => process_selection(order_position, "COLUMN_2")}>
                        <a>2</a>
                    </div>
                </Col>
                <Col xs={3} md={3} className="column-button">
                    <div onClick={() => process_selection(order_position, "COLUMN_3")}>
                        <a>3</a>
                    </div>
                </Col>
                <Col xs={3} md={3} className="column-button">
                    <div onClick={() => process_selection(order_position, "COLUMN_4")}>
                        <a >4</a>
                    </div>
                </Col>
            </Row>
        </Popover.Content>
    </Popover>

    return (
        <div className="create-section">
            <div>
                <OverlayTrigger show={isVisible} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                    <p>Create Section</p>
                </OverlayTrigger>
            </div>
        </div>
    );
}

export default CreateSectionButton;