import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_PUT } from '.././shared/DNDRequests';

class RenamePage extends Component {

    state = {
        name: null
    }

    handleSubmit = (event) => {
        
        if( this.state.name == null){
            event.preventDefault();
            return
        }

        var data = {
            name: this.state.name
        }

        DND_PUT(
            '/page/' + this.props.page_id,
            data,
            (response) => {
                this.props.refresh_function()
                this.setState({ name: null })
            },
            null
        )
        
        event.preventDefault();
      
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };


    render(){
            return(
                <Form onSubmit={this.handleSubmit} className="rename-page">
                    <Row>

                        <Col xs={2} md={2}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control required value={this.state.name} name="name" type="text" placeholder="Enter Name" onChange={this.handleChange} />
                            </Form.Group>
                        </Col>

                        <Col xs={2} md={2}>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Col>
                    </Row>
                </Form>
            );
    };
}

export default RenamePage;