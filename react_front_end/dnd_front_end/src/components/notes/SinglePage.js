import React, { Component } from 'react';
import RenamePage from './RenamePage';
import ListSections from './ListSections';
import Collapsible from '../shared/Collapsible';
import PageShortCodeGenerator from './PageShortCodeGenerator'

class SinglePage extends Component {

    render(){
            if(this.props.read == false){
                return(
                    <div>
                        <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_page_fuction}></i> {this.props.page.name} <PageShortCodeGenerator page={this.props.page} collection_id={this.props.collection_id} /></h2>
                        <Collapsible contents={<RenamePage refresh_function={this.props.refresh_function} page_id={this.props.page.id}/>} title="Rename Page"/>
                        <hr/>
                        <ListSections read={this.props.read} page_id={this.props.page.id} sections={this.props.page.sections} refresh_function={this.props.refresh_function}/>
                    </div>
                );
            } else {
                return(
                    <div className="page-read">
                        <hr/>
                        <h2>{this.props.page.name}</h2>
                        <ListSections player_read={this.props.player_read} override_function={this.props.override_function} read={this.props.read} page_id={this.props.page.id} sections={this.props.page.sections} refresh_function={this.props.refresh_function}/>
                    </div>
                );
            }

    };
}

export default SinglePage;