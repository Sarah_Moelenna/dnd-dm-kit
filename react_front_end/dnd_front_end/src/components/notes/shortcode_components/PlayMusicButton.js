import { DND_POST } from '../.././shared/DNDRequests';

const play_music = (id) => {
    var data = {
        "music_play_type": "LOOP",
        "audio_id": id
    }
    var body = {
        "command_code": "CMD_PLAY",
        "data": data
    }

    DND_POST(
        '/commands',
        body,
        null,
        null
    )
}

const PlayMusicButton = ({string_in}) => {

    var name = ''
    var id = ''
    var type = ''

    var string_components = string_in.split(' ')

    for(var i = 0; i < string_components.length; i++){
        var key_values = string_components[i].split('=')
        if(key_values.length == 2){
            if(key_values[0]=='name'){
                name = key_values[1]
            }
            if(key_values[0]=='id'){
                id = key_values[1]
            }
            if(key_values[0]=='type'){
                type = key_values[1]
            }
        }
    }

    return (
        <div className="play-music-button">
            <div onClick={() => {play_music(id)}}>
                <i className="fas fa-play"></i><p>{name.replaceAll("%20", " ")}</p>
            </div>
        </div>
    );
}

export default PlayMusicButton;