const PageLink = ({string_in, override_function}) => {

    var name = ''
    var id = ''
    var collection_id = ''

    var string_components = string_in.split(' ')

    for(var i = 0; i < string_components.length; i++){
        var key_values = string_components[i].split('=')
        if(key_values.length == 2){
            if(key_values[0]=='name'){
                name = key_values[1]
            }
            if(key_values[0]=='id'){
                id = key_values[1]
            }
            if(key_values[0]=='collection_id'){
                collection_id = key_values[1]
            }
        }
    }

    function goto_page(){
        var override = {
            "page": "NOTES",
            "page_focus": id,
            "collection_focus": collection_id
        }
        override_function(override)
    }

    return (
        <div className="page-component-link" onClick={() => {goto_page()}}>
            {name.replaceAll("%20", " ")}
        </div>
    );
}

export default PageLink;