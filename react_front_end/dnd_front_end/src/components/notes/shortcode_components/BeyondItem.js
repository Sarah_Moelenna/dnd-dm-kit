import React, { useContext } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import {SocketContext} from '../.././socket/Socket';

const titleCase = (str) => {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
    }
    // Directly return the joined string
    return splitStr.join(' '); 
 }

const BeyondItem = ({string_in}) => {

    
    const socket = useContext(SocketContext);

    /*function create_text_share_command(url){
        var data = {
            "text": url,
            "location": "ROLLING"
        }
        return {
            "command_code": "CMD_SHARE_TXT",
            "data": data
        }
    }*/

    function emit_text_message(url){
        var data = {
            type: "TEXT",
            name: "DM",
            content: url
          }
          socket.emit("send_message", data);
    }

    var string_components = string_in.split(' ')
    var name = string_components[1]
    var url = string_components[0]
    name = name.replaceAll('-', ' ').replaceAll(' 1', ', +1').replaceAll(' 2', ', +2').replaceAll(' 3', ', +3')
    const homebrew_regex = "([0-9]{4,})([\\w',+\\-\\!\\?\\(\\) ]*)"
    var matches = [...name.matchAll(homebrew_regex)]
    for(var i = 0; i < matches.length; i++){
        var name = matches[i][2]
    }
    name = titleCase(name)

    var popover = <Popover id="popover-basic" className="item-button-popover">
        <Popover.Content>
            <p>Share <i className="fas fa-share clickable-icon" onClick={() => {emit_text_message(url)}}></i></p>
        </Popover.Content>
    </Popover>

    return (
        <div className="item-button">
            <OverlayTrigger delay={{ show: 0, hide: 1000 }} trigger={['hover','focus']} placement="right" overlay={popover}>
                <a href={url} target="_blank">
                    <p>{name}</p>
                </a>
            </OverlayTrigger>
        </div>
    );
}

export default BeyondItem;