const MonsterLink = ({string_in, override_function, player_read}) => {

    var name = ''
    var id = ''

    var string_components = string_in.split(' ')

    for(var i = 0; i < string_components.length; i++){
        var key_values = string_components[i].split('=')
        if(key_values.length == 2){
            if(key_values[0]=='name'){
                name = key_values[1]
            }
            if(key_values[0]=='id'){
                id = key_values[1]
            }
        }
    }

    function goto_monster(){
        var override = {
            "page": "MONSTER",
            "monster_focus": id
        }
        override_function(override)
    }

    if(player_read == true){
        return(name.replaceAll("%20", " "))
    }

    return (
        <div className="monster-link" onClick={() => {goto_monster()}}>
            {name.replaceAll("%20", " ")}
        </div>
    );
}

export default MonsterLink;