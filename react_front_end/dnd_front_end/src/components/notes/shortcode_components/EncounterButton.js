import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'

const EncounterButton = ({string_in, override_function}) => {

    var name = ''
    var id = ''
    var summary = ''

    var string_components = string_in.split(' ')

    for(var i = 0; i < string_components.length; i++){
        var key_values = string_components[i].split('=')
        if(key_values.length == 2){
            if(key_values[0]=='name'){
                name = key_values[1]
            }
            if(key_values[0]=='id'){
                id = key_values[1]
            }
            if(key_values[0]=='summary'){
                summary = key_values[1]
            }
        }
    }

    function goto_encounter(){
        var override = {
            "page": "ENCOUNTER",
            "encounter_focus": id
        }
        override_function(override)
    }

    var popover = <Popover id="popover-basic">
        <Popover.Content>
            <p>{summary.replaceAll("%20", " ")}</p>
        </Popover.Content>
    </Popover>


    return (
        <div className="encounter-button">
            <OverlayTrigger delay={{ show: 0, hide: 1000 }} trigger={['hover', 'focus']} placement="bottom" overlay={popover}>
                <div onClick={() => {goto_encounter()}}>
                    <i className="fas fa-play"></i><p>{name.replaceAll("%20", " ")}</p>
                </div>
            </OverlayTrigger>
        </div>
    );
}

export default EncounterButton;