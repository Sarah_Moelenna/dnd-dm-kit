import React, { Component } from 'react';
import SingleMap from '../../map/SingleMap';
import Spinner from '../../shared/Spinner';
import { DND_GET } from '../.././shared/DNDRequests';

class Map extends Component {

    state = {
        map_focus: null,
      }

    get_id = () => {
        var id = ''
        var string_components = this.props.string_in.split(' ')

        for(var i = 0; i < string_components.length; i++){
            var key_values = string_components[i].split('=')
            if(key_values.length == 2){
                if(key_values[0]=='id'){
                    id = key_values[1]
                }
            }
        }

        return id
    }

    componentDidMount() {
        this.get_map(this.get_id())
    };

    get_map = (map_id) => {
        DND_GET(
            '/map/' + map_id,
            (jsondata) => {
                this.setState({ map_focus: jsondata})
            },
            null
        )
      };

    render(){
        if(this.state.map_focus == null){
            return (<Spinner/>)
        }

        return(
            <div>
                <SingleMap override_function={this.props.override_function} map={this.state.map_focus}  read={true}/>
            </div>
        );
    };
}

export default Map;