import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';
import { DND_DELETE } from '.././shared/DNDRequests';

const ListPageItem = ({ page, refresh_function, view_page_function, delete_page_function, update_indentation_function, max_indentation, read}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/page/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(read == false){
        return (
            <div className="page" style={{"margin-left": (page.indentation * 30) + "px"}}>
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text">{page.name}</p>
                    </Col>
                    <Col xs={3} md={3}>
                        { page.indentation > 0 &&
                            <i className="fas fa-chevron-left clickable-icon" onClick={() => {update_indentation_function(page.id, page.indentation - 1)}}></i>
                        }
                        { page.indentation < max_indentation &&
                            <i className="fas fa-chevron-right clickable-icon" onClick={() => {update_indentation_function(page.id, page.indentation + 1)}}></i>
                        }
                    </Col>
                    <Col xs={6} md={6} className="controls">
                        <a className="btn btn-primary" onClick={() => view_page_function(page.id)}>Edit Page</a>
                        <DeleteConfirmationButton id={page.id} name="Page" delete_function={delete_page_function} />
                    </Col>
                </Row>
            </div>

        );
    } else {
        return (
            <div className="page read" style={{"margin-left": (page.indentation * 30) + "px"}}  onClick={() => view_page_function(page.id)}>
                <Row>
                    <Col xs={12} md={12}>
                        <p className="card-text">{page.name}</p>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default ListPageItem;