import React, { Component } from 'react';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import Markdown from 'react-markdown';
import {process_markdown, create_link} from '.././MarkdownHandlers';

class EditableTable extends Component {

    /*
    FORMAT EXAMPLE
    headers: ["blarg", "HURG", "nurgling"]
    rows: {
        1: ["ghretyrew", "bgerbtgre", "nbhbiotrnoi5t"],
        2: ["kuyikt", "kdtuyt", "kuytu"],
        3: ["jryfud", "arwuehbdw", "grdtr"],
        4: ["gtrtrgyt", "4h36463hrt", "4h3t6f"]
    }
    */
    state = {
        headers: [],
        rows: {},
        title: ""
    }

    constructor(props) {
        super(props);
        this.state = {
            headers: this.props.headers,
            rows: this.props.rows,
            title: this.props.title
        };
    }


    components = () => {
        return {
            a: props => {return create_link(props, this.props.override_function, this.props.player_read)}
        }
    };

    change_cell = e => {
        var new_rows = this.state.rows
        var row_index = e.target.dataset.rowIndex
        var column_index = e.target.dataset.columnIndex
        var value = e.target.value
        new_rows[row_index][column_index] = value
        this.props.update_table_function(new_rows, this.state.headers, this.state.title)
        this.setState({rows: new_rows, headers: this.state.headers})
    };

    change_header = e => {
        var new_headers = this.state.headers
        var column_index = e.target.dataset.columnIndex
        var value = e.target.value
        new_headers[column_index] = value
        this.props.update_table_function(this.state.rows, new_headers, this.state.title)
        this.setState({rows: this.state.rows, headers: new_headers})
    };

    remove_row = (row_number) => {
        var new_rows = this.state.rows
        delete new_rows[row_number]
        this.props.update_table_function(new_rows, this.state.headers, this.state.title)
        this.setState({rows: new_rows, headers: this.state.headers})
    };

    change_title = e => {
        var value = e.target.value
        this.props.update_table_function(this.state.rows, this.state.headers, value, this.state.title)
        this.setState({title: value})
    };

    remove_column = (column_number) => {
        var new_rows = this.state.rows
        var new_headers = []
        
        for(var i = 0; i < this.state.headers.length; i++){
            if(i != column_number){
                new_headers.push(this.state.headers[i])
            }
        }

        for(var i = 0; i < Object.keys(new_rows).length; i++){
            var values = new_rows[Object.keys(new_rows)[i]]
            delete values[column_number]
            new_rows[Object.keys(new_rows)[i]] = values
        }
        this.props.update_table_function(new_rows, new_headers, this.state.title)
        this.setState({rows: new_rows, headers: new_headers})
    };

    add_column = () => {
        var new_rows = this.state.rows
        var new_headers = this.state.headers
        new_headers.push("")
        for(var i = 0; i < Object.keys(new_rows).length; i++){
            var values = new_rows[Object.keys(new_rows)[i]]
            values.push("")
            new_rows[Object.keys(new_rows)[i]] = values
        }
        this.props.update_table_function(new_rows, new_headers, this.state.title)
        this.setState({rows: new_rows, headers: new_headers})
    };

    add_row = () => {
        var highest = 0;
        var row_length = this.state.headers.length;
        var new_rows = this.state.rows
        if (Object.keys(new_rows).length > 0){
            for(var i = 0; i < Object.keys(new_rows).length; i++){
                if(Object.keys(new_rows)[i] >= highest){
                    highest = Object.keys(new_rows)[i] + 1
                }
            }
        }
        new_rows[highest] = []
        for(var i = 0; i < row_length; i++){
            new_rows[highest].push("")
        }
        this.props.update_table_function(new_rows, this.state.headers, this.state.title)
        this.setState({rows: new_rows, headers: this.state.headers})
    }

    render(){
        return(
            <div className="editable-table">
                {this.props.read == false &&
                    <Form.Control type="text" value={this.state.title}  onChange={this.change_title} placeholder="Table Title"/>
                }
                {this.props.read == true && this.state.title != "" && this.state.title != null && this.state.title != undefined &&
                    <h2>{this.state.title}</h2>
                }
                
                <Table responsive="xl">
                    <thead>
                        <tr>
                            {this.props.read == false &&
                                <th></th>
                            }
                            {this.state.headers.map((header, index) => (
                                <th key={index}>
                                    {this.props.read == false &&
                                        <i className="fas fa-trash clickable-icon" onClick={() => {this.remove_column(index)}}></i>
                                    }
                                    {this.props.read == false ?
                                        <Form.Control data-column-index={index} type="text" value={header} onChange={this.change_header} placeholder="Column Name"/>
                                        :
                                        <p>{header}</p>
                                    }
                                </th>
                            ))}
                            {this.props.read == false &&
                                <th>
                                    <i className="fas fa-plus clickable-icon" onClick={() => {this.add_column()}}></i>
                                </th>
                            }
                        </tr>
                    </thead>
                    <tbody>
                        {Object.keys(this.state.rows).map((row_number) => (
                            <tr key={row_number}>
                                {this.props.read == false &&
                                    <td><i className="fas fa-trash clickable-icon" onClick={() => {this.remove_row(row_number)}}></i></td>
                                }
                                {this.state.rows[row_number].map((value, index) => (
                                    <td>
                                        {this.props.read == false ?
                                            <Form.Control data-row-index={row_number} data-column-index={index} type="text" value={value}  onChange={this.change_cell}/>
                                            :
                                            <Markdown components={this.components()} children={process_markdown(value)} />
                                        }
                                    </td>
                                ))}
                                {this.props.read == false &&
                                    <td></td>
                                }
                            </tr>
                        ))}
                        {this.props.read == false &&
                            <tr>
                                    <td><i className="fas fa-plus clickable-icon" onClick={() => {this.add_row()}}></i></td>
                            </tr>
                        }
                    </tbody>
                </Table>
            </div>
        );
    };
}

export default EditableTable;