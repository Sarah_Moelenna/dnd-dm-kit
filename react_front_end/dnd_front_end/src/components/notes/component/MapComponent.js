import React, { Component } from 'react';
import SingleMap from '../../map/SingleMap';
import { DND_GET, DND_PUT } from '../.././shared/DNDRequests';
import MapSelect from '../../map/MapSelect';
import Spinner from '../../shared/Spinner';

class MapComponent extends Component {

    state = {
        map_focus: null,
    }

    componentDidMount() {
        this.get_map(this.props.component.contents.map_id)
    };

    get_map = (map_id) => {
        DND_GET(
            '/map/' + map_id,
            (jsondata) => {
                this.setState({ map_focus: jsondata})
            },
            null
        )
    };

    update_map_id = (id) => {
        var contents = this.props.component.contents
        contents.map_id = id

        var data = {
            "contents": contents,
        }

        DND_PUT(
            '/component/' + this.props.component.id,
            data,
            (jsondata) => {
                this.get_map(id)
            },
            null
        )
    }

    render(){
        if(this.state.map_focus == null){
            return (
                <div>
                    { this.props.read == false ?
                        <MapSelect
                            select_function={this.update_map_id}
                            disabled_ids={[this.props.component.contents.map_id]}
                        />
                        :
                        <Spinner />
                    }
                </div>
            )
        }

        return(
            <div key={this.props.component.contents.map_id}>
                { this.props.read == false &&
                    <MapSelect
                        select_function={this.update_map_id}
                        disabled_ids={[this.props.component.contents.map_id]}
                    />
                }
                <SingleMap override_function={this.props.override_function} map={this.state.map_focus}  read={true} player_read={this.props.player_read}/>
            </div>
        );
    };
}

export default MapComponent;