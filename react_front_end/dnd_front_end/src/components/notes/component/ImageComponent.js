import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Spinner from '../.././shared/Spinner';
import APIButton from '../.././shared/APIButton';
import { DND_PUT } from '../.././shared/DNDRequests';
import { get_api_url } from '../../shared/Config';
import Button from 'react-bootstrap/Button'
import { SocketContext } from '../../socket/Socket';
import UploadImage from '../../images/UploadImage';

const REACT_APP_API_URL = get_api_url()

class ImageComponent extends Component {

    static contextType = SocketContext

    state = {
        uploading: false,
        url: null,
        scale: 100
    }

    constructor(props) {
        super(props);
        this.state = {
            url: this.props.component.contents.url,
            uploading: false,
            scale: this.props.component.contents.scale != undefined ? this.props.component.contents.scale : 100
        };
    }

    update_image = (url) => {
        var contents = this.props.component.contents
        contents.url = url

        var data = {
            "contents": contents,
        }

        DND_PUT(
            '/component/' + this.props.component.id,
            data,
            null,
            null
        )

        this.setState({uploading: false, url: url})
    }

    update_scale = (e) => {
        var contents = this.props.component.contents
        contents.scale = e.target.value

        var data = {
            "contents": contents,
        }

        DND_PUT(
            '/component/' + this.props.component.id,
            data,
            null,
            null
        )

        this.setState({scale: e.target.value})
    }

    create_image_upload_command = (url) => {
        var data = {
            "filepath": url,
            "location": "INFO"
        }
        return {
            "command_code": "CMD_UPLOAD_IMG",
            "data": data
        }
    }

    emit_image_message = (url) => {
        const socket = this.context
        var data = {
            type: "URL",
            name: "",
            url: REACT_APP_API_URL + "/" + url
          }
          socket.emit("send_message", data);
    }

    render(){
        if(this.props.read == false){
            return(
                <div className="image-component">
                    {this.state.url != null &&
                        <Form.Control type="range" value={this.state.scale} min={1} max={100} onChange={this.update_scale}/>
                    }
                    {this.state.url != null &&
                        <img style={{maxWidth:this.state.scale + "%"}} src={REACT_APP_API_URL + "/" + this.state.url}/>
                    }
                    <UploadImage callback = {this.update_image} exclude_api_url={true}/>
                </div>
            );
        } else {
            return(
                <div className="image-component">
                    {this.state.url != null &&
                        <div>
                            <a href={REACT_APP_API_URL + "/" + this.state.url} target="_blank"><img style={{maxWidth:this.state.scale + "%"}} src={REACT_APP_API_URL + "/" + this.state.url}/></a>
                            {this.props.player_read != true &&
                                <div>
                                    <Button className="btn btn-primary" onClick={() => {this.emit_image_message(this.state.url)}} >Share to Chat</Button>
                                </div>
                            }
                        </div>
                    }
                </div>
            );
        }
    };
}

export default ImageComponent;