import React, { Component } from 'react';
import MDEditor from '@uiw/react-md-editor';
import Markdown from 'react-markdown'
import {process_markdown, create_link} from '.././MarkdownHandlers';
import { DND_PUT } from '../.././shared/DNDRequests';

class TextComponent extends Component {

    update_text = (text) => {
        var contents = this.props.component.contents
        contents.text = text

        var data = {
            "contents": contents,
        }

        DND_PUT(
            '/component/' + this.props.component.id,
            data,
            null,
            null
        )

    }

    components = () => {
        return {
            a: props => {return create_link(props, this.props.override_function, this.props.player_read)}
        }
    };

    render(){

        var components = this.components()

        /*<MDEditor.Markdown source={this.props.component.contents.text} />*/

        return(
            <div className="text-component">
                {this.props.read == false ?
                    <MDEditor
                        value={this.props.component.contents.text}
                        preview="edit"
                        onChange={this.update_text}
                    />
                    :
                    <Markdown components={components} children={process_markdown(this.props.component.contents.text)} />
                }
            </div>
        );
    };
}

export default TextComponent;