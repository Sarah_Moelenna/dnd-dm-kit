import React, { Component } from 'react';
import { DND_GET, DND_PUT } from '../.././shared/DNDRequests';
import Spinner from '../../shared/Spinner';
import EffectDeckSelect from '../../effect_deck/EffectDeckSelect';
import { DeckButton } from '../../effect_deck/SingleEffectDeck';

class SoundDeckComponent extends Component {

    state = {
        effect_deck_focus: null,
    }

    componentDidMount() {
        this.get_effect_deck(this.props.component.contents.effect_deck_id)
    };

    get_effect_deck = (effect_deck_id) => {
        DND_GET(
            '/effect_deck/' + effect_deck_id,
            (jsondata) => {
                this.setState({ effect_deck_focus: jsondata})
            },
            null
        )
    };

    update_effect_deck_id = (id) => {
        var contents = this.props.component.contents
        contents.effect_deck_id = id

        var data = {
            "contents": contents,
        }

        DND_PUT(
            '/component/' + this.props.component.id,
            data,
            (jsondata) => {
                this.get_effect_deck(id)
            },
            null
        )
    }


    render(){
        if(this.state.effect_deck_focus == null){
            return (
                <div>
                    { this.props.read == false ?
                        <EffectDeckSelect
                            select_function={this.update_effect_deck_id}
                            disabled_ids={[this.props.component.contents.effect_deck_id]}
                        />
                        :
                        <Spinner />
                    }
                </div>
            )
        }

        return(
            <div key={this.props.component.contents.effect_deck_id}>
                { this.props.read == false &&
                    <EffectDeckSelect
                        select_function={this.update_effect_deck_id}
                        disabled_ids={[this.props.component.contents.effect_deck_id]}
                    />
                }
                <div className='sound-deck-component-container'>
                    <div className='sound-deck-component'>
                        <p className='title'>{this.state.effect_deck_focus.name} Deck</p>
                        <div className='sound-deck-container'>
                            {Object.keys(this.state.effect_deck_focus.data).map((icon_id) => (
                                <DeckButton deck_item={this.state.effect_deck_focus.data[icon_id]}/>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

export default SoundDeckComponent;