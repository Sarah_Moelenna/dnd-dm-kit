import React, { Component } from 'react';
import EditableTable from './EditableTable'
import { DND_PUT } from '../.././shared/DNDRequests';

class TableComponent extends Component {

    update_table = (rows, headers, title) => {
        var contents = this.props.component.contents
        contents.rows = rows
        contents.headers = headers
        contents.title = title

        var data = {
            "contents": contents,
        }

        DND_PUT(
            '/component/' + this.props.component.id,
            data,
            null,
            null
        )
    }

    render(){
        return(
            <div className="table-component">
                <EditableTable
                    override_function={this.props.override_function}
                    read={this.props.read}
                    rows={this.props.component.contents.rows}
                    headers={this.props.component.contents.headers}
                    update_table_function={this.update_table}
                    title={this.props.component.contents.title != undefined ? this.props.component.contents.title : ""}
                    player_read={this.props.player_read}
                    />
            </div>
        );
    };
}

export default TableComponent;