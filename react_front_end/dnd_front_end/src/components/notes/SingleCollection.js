import React, { Component } from 'react';
import Collapsible from '../shared/Collapsible';
import CreatePage from './CreatePage';
import ListPages from './ListPages';
import SinglePage from './SinglePage';
import { DND_PUT } from '.././shared/DNDRequests';

class SingleCollection extends Component {
    
    update_order = (pages) => {
        var new_heirachy = {}

        for(var i = 0; i < pages.length; i++){
            new_heirachy[i] = pages[i]
        }

        var data = {
            "heirachy": new_heirachy
        }

        DND_PUT(
            '/collection/' + this.props.collection.id,
            data,
            (response) => {
                this.props.refresh_function()
            },
            null
        )
    }

    get_name_for_page_id = (page_id) => {
        for(var i = 0; i < this.props.collection.pages.length; i++){
            if (this.props.collection.pages[i].id == page_id){
                return this.props.collection.pages[i].name
            }
        }
    }

    get_page_order = () => {
        var pages = []
        var ids = []
        var heirachy = this.props.collection.heirachy

        if (heirachy == null){
            heirachy = this.create_initial_heirachy()
        }

        for(var i = 0; i < Object.keys(heirachy).length; i++){
            pages.push(
                heirachy[i]
            )
            ids.push(
                heirachy[i].id
            )
        }

        for(var i = 0; i < this.props.collection.pages.length; i++){
            if(!ids.includes(this.props.collection.pages[i].id)){
                var page = this.props.collection.pages[i]
                page["indentation"] = 0
                pages.push(
                    page
                )
            }
        }

        for(var i = 0; i < pages.length; i++){
            pages[i].name = this.get_name_for_page_id(pages[i].id)
        }

        return pages
    }

    create_initial_heirachy = () => {
        var heirachy = {}

        for(var i = 0; i < this.props.collection.pages.length; i++){
            heirachy[i] = this.props.collection.pages[i]
            heirachy[i]["indentation"] = 0
        }

        return heirachy
    }

    render(){
        if(this.props.read == false){
            if(this.props.page_focus == null){
                return(
                    <div>
                        <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_collection_fuction}></i> {this.props.collection.name}</h2>
                        <Collapsible contents={<CreatePage refresh_function={this.props.refresh_function} collection_id={this.props.collection.id}/>} title="Add New Page"/>
                        <h3>Pages</h3>
                        <ListPages read={this.props.read} pages={this.get_page_order()} refresh_function={this.props.refresh_function} update_order_function={this.update_order} view_page_function={this.props.display_page}/>
                    </div>
                )
            } else {
                return(
                    <div>
                        <SinglePage collection_id={this.props.collection.id} read={this.props.read} page={this.props.page_focus} refresh_function={this.props.refresh_page_in_focus} close_page_fuction={this.props.close_page}/>
                    </div>
                )
            }
        } else {
            return (
                <div>
                    <h2><i className="fas fa-chevron-left clickable-icon" onClick={this.props.close_collection_fuction}></i> {this.props.collection.name}</h2>
                    <Collapsible contents={
                        <ListPages read={this.props.read} pages={this.get_page_order()} refresh_function={this.props.refresh_function} update_order_function={this.update_order} view_page_function={this.props.display_page}/>
                    } title="Show Contents"/>
                    {
                        this.props.page_focus != null &&
                        <SinglePage player_read={this.props.player_read} read={this.props.read} override_function={this.props.override_function} page={this.props.page_focus} refresh_function={this.props.refresh_page_in_focus} close_page_fuction={this.props.close_page}/>
                    }
                </div>
            )
        }

    };
}

export default SingleCollection;