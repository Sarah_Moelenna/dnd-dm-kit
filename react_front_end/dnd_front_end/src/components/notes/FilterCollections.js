import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class FilterCollections extends Component {

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handleChange = e => {
        this.props.update_filter_function(e.target.name, e.target.value)
    };


    render(){

        return(
            <Row className="collection_filters">
                <Col xs={2} md={2}>
                    <Form onSubmit={this.handleSubmit}>

                                <Form.Group controlId="formBasicName">
                                    <Form.Label>Collection Name</Form.Label>
                                    <Form.Control value={this.props.filters["name"]  ? this.props.filters["name"] : ''} name="name" type="text" placeholder="Search Collection Names" onChange={this.handleChange}/>
                                </Form.Group>
                    </Form>
                </Col>
                <Col xs={2} md={2}>
                    <a className="btn btn-primary" onClick={() => this.props.clear_filter_function()}>Clear Filters</a>
                </Col>
            </Row>
        );
    };
}

export default FilterCollections;