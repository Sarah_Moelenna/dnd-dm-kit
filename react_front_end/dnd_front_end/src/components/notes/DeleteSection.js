import React, { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const DeleteSection = ({section_id, delete_function}) => {

    const [isVisible, setIsVisible] = useState(false);

    const toggle = () => setIsVisible(!isVisible);

    function process_selection(){
        toggle()
        delete_function(section_id)
    }

    var popover = <Popover className="delete-section-popover">
        <Popover.Title>Delete Section</Popover.Title>
        <Popover.Content className="content">
            <p>Are You Sure?</p>
            <Row>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => process_selection()}>
                        <p>Yes</p>
                    </div>
                </Col>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => toggle()}>
                        <p>No</p>
                    </div>
                </Col>
            </Row>
        </Popover.Content>
    </Popover>

    return (
        <div className="delete-section">
            <div>
                <OverlayTrigger show={isVisible} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                    <p>DELETE SECTION</p>
                </OverlayTrigger>
            </div>
        </div>
    );
}

export default DeleteSection;