import React, { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const DeleteComponent = ({component_id, delete_function}) => {

    const [isVisible, setIsVisible] = useState(false);

    const toggle = () => setIsVisible(!isVisible);

    function process_selection(){
        toggle()
        delete_function(component_id)
    }

    var popover = <Popover className="delete-component-popover">
        <Popover.Title>Delete Component</Popover.Title>
        <Popover.Content className="content">
            <p>Are You Sure?</p>
            <Row>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => process_selection()}>
                        <p>Yes</p>
                    </div>
                </Col>
                <Col xs={6} md={6} className="column-button">
                    <div onClick={() => toggle()}>
                        <p>No</p>
                    </div>
                </Col>
            </Row>
        </Popover.Content>
    </Popover>

    return (
        <div className="delete-component">
            <div>
                <OverlayTrigger show={isVisible} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                    <p>DELETE COMPONENT</p>
                </OverlayTrigger>
            </div>
        </div>
    );
}

export default DeleteComponent;