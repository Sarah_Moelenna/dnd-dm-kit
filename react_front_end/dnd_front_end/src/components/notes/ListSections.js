import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CreateSectionButton from './CreateSectionButton';
import DeleteSection from './DeleteSection';
import ListComponents from './ListComponents';
import { DND_POST, DND_DELETE } from '.././shared/DNDRequests';

const ListSections = ({ page_id, sections, refresh_function, read, override_function, player_read}) => {

    function create_section(order, type){
        
        var data = {
            "type": type,
            "order": order,
        }

        DND_POST(
            '/page/' + page_id + "/section",
            data,
            (response) => {
                refresh_function()
            },
            null
        )
      
    }

    function delete_section(section_id){
        DND_DELETE(
            '/section/' + section_id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
      
    }

    function get_components_for_column(section_index, column_number){
        var components = []
        for(var i = 0; i < sections[section_index].components.length; i++){
            var component = sections[section_index].components[i]
            if(component.column == column_number){
                components.push(component)
            }
        }
        return components
    }

    if(sections === undefined || sections.length == 0) {
        if(read == false){
            return(
                <div>
                    <CreateSectionButton order_position={0} create_function={create_section}/>
                </div>
            )
        } else {
            return null
        }
    }

    return (
        <div className="sections-items">
            {read == false &&
                <CreateSectionButton order_position={0} create_function={create_section}/>
            }
            {sections.map((section, index) => (
                <div key={section.id}>
                    <div className="section-item">
                        {section.section_type == "COLUMN_1" &&
                            <Row>
                                <Col xs={12} md={12} className="col first last">
                                    <div className="column first last">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={0} components={get_components_for_column(index, 0)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                            </Row>
                        }
                        {section.section_type == "COLUMN_2" &&
                            <Row>
                                <Col xs={6} md={6} className="col first">
                                    <div className="column first">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={0} components={get_components_for_column(index, 0)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                                <Col xs={6} md={6} className="col last">
                                    <div className="column last">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={1} components={get_components_for_column(index, 1)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                            </Row>
                        }
                        {section.section_type == "COLUMN_3" &&
                            <Row>
                                <Col xs={4} md={4}  className="col first">
                                    <div className="column first">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={0} components={get_components_for_column(index, 0)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                                <Col xs={4} md={4}  className="col">
                                    <div className="column">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={1} components={get_components_for_column(index, 1)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                                <Col xs={4} md={4}  className="col last">
                                    <div className="column last">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={2} components={get_components_for_column(index, 2)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                            </Row>
                        }
                        {section.section_type == "COLUMN_4" &&
                            <Row>
                                <Col xs={3} md={3}  className="col first">
                                    <div className="column first">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={0} components={get_components_for_column(index, 0)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                                <Col xs={3} md={3}  className="col">
                                    <div className="column">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={1} components={get_components_for_column(index, 1)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                                <Col xs={3} md={3}  className="col">
                                    <div className="column">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={2} components={get_components_for_column(index, 2)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                                <Col xs={3} md={3}  className="col last">
                                    <div className="column last">
                                        <ListComponents player_read={player_read} override_function={override_function} read={read} section_id={section.id} section_column={3} components={get_components_for_column(index, 3)} refresh_function={refresh_function}/>
                                    </div>
                                </Col>
                            </Row>
                        }
                        {read == false &&
                            <DeleteSection section_id={section.id} delete_function={delete_section}/>
                        }
                    </div>
                    {read == false &&
                        <CreateSectionButton order_position={section.order+1} create_function={create_section}/>
                    }
                </div>
            ))}
        </div>
    );
}

export default ListSections;