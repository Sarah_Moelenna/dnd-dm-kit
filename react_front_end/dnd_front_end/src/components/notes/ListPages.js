import React, { Component } from 'react';
import ListPageItem from './ListPageItem';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

const findUniques = (a,b) => {
    return a.filter(x => !b.includes(x)).concat(b.filter(x=> !a.includes(x)))
}

class ListPages extends Component {

    state = {
        ordered_list: [],
    }

    componentDidMount() {
        this.setState({ ordered_list: this.props.pages})
    };

    componentDidUpdate(){
        var differences = findUniques(this.state.ordered_list,  this.props.pages)
        if (differences.length > 0){
            this.setState({ ordered_list: this.props.pages})
        }
    }

    handleOnDragEnd = (result) => {
        if (!result.destination) return;

        const items = Array.from(this.state.ordered_list);
        const [reorderedItem] = items.splice(result.source.index, 1);
        items.splice(result.destination.index, 0, reorderedItem);

        var corrected_items = this.correct_indentation(items)

        this.setState({ ordered_list: corrected_items}, this.props.update_order_function(corrected_items))
        
    }

    correct_indentation = (items) =>{
        var new_items = items
        for(var i = 0; i < new_items.length; i++){
            if (i == 0 && new_items[i].indentation != 0){
                new_items[i].indentation = 0
            }
            if (i > 0){
                var previous_indentation = new_items[i-1].indentation
                if (new_items[i].indentation > previous_indentation + 1){
                    new_items[i].indentation = previous_indentation + 1
                }
            }
        }
        return new_items
    }

    update_indentation = (page_id, indentation_level) => {
        var items = []

        for(var i = 0; i < this.state.ordered_list.length; i++){
            var new_item = this.state.ordered_list[i]
            if (new_item.id == page_id){
                new_item.indentation = indentation_level
            }
            items.push(new_item)
        }
        this.setState({ ordered_list: items}, this.props.update_order_function(items))
    }

    get_max_identation_for_page = (page_id) => {
        var max = 0

        for(var i = 0; i < this.state.ordered_list.length; i++){
            var curr_item = this.state.ordered_list[i]
            if (i != 0){
                if (curr_item.id == page_id){
                    max = this.state.ordered_list[i - 1].indentation + 1
                }
            }
        }
        return max
    }

    render(){
        if(this.state.ordered_list === undefined) {
            return(
                <div>
                    Nothing is here yet.
                </div>
            )
        }

        if(this.props.read == false){
            return (
                <div className="pages">
                    <hr></hr>
                    <DragDropContext onDragEnd={this.handleOnDragEnd}>
                        <Droppable droppableId="pages">
                            {(provided) => (
                                <ul className="page-list" {...provided.droppableProps} ref={provided.innerRef}>
                                    {this.state.ordered_list.map((page, index) => (
                                        <Draggable key={page.id} draggableId={page.id} index={index}>
                                            {(provided) => (
                                                <li ref={provided.innerRef} className={index % 2 == 0 ? "light" : "dark"} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                    <ListPageItem
                                                        read={this.props.read}
                                                        page={page}
                                                        refresh_function={this.props.refresh_function}
                                                        update_indentation_function={this.update_indentation}
                                                        max_indentation={this.get_max_identation_for_page(page.id)}
                                                        view_page_function={this.props.view_page_function}/>
                                                </li>
                                            )}
                                        </Draggable>
                                    ))}
                                {provided.placeholder}
                                </ul>
                            )}
                        </Droppable>
                    </DragDropContext>
                </div>
            );
        } else {
            return (
                <div className="pages">
                    <hr></hr>
                    <ul className={this.props.read == false ? "page-list": "page-list read"}>
                        {this.state.ordered_list.map((page, index) => (
                            <li key={page.id} className={index % 2 == 0 ? "light" : "dark"}>
                                <ListPageItem
                                    read={this.props.read}
                                    page={page}
                                    refresh_function={this.props.refresh_function}
                                    update_indentation_function={this.update_indentation}
                                    max_indentation={this.get_max_identation_for_page(page.id)}
                                    view_page_function={this.props.view_page_function}/>
                            </li>
                        ))}
                    </ul>
                </div>
            );
        }
    };
}
    
export default ListPages;