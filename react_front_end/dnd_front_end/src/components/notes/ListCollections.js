import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { DND_DELETE } from '.././shared/DNDRequests';
import DeleteConfirmationButton from '../shared/DeleteConfirmationButton';

const ListCollections = ({ collections, refresh_function, view_collection_function, player_read}) => {

    function delete_page_function(id){
        DND_DELETE(
            '/collection/' + id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
    }

    if(collections === undefined || collections === null) {
        return(
            <div>
                Nothing is here yet.
            </div>
        )
    }
    return (
        <div className="collections">
            <hr></hr>
            <div className="headers">
                <Row>
                    <Col xs={3} md={3}>
                        <p className="card-text"><b>Name</b></p>
                    </Col>
                    <Col xs={3} md={3}>
                    </Col>
                    <Col xs={3} md={3}>
                    </Col>
                    <Col xs={1} md={1}>
                    </Col>
                </Row>
                <hr/>
            </div>
            {collections.map((collection) => (
                <div key={collection.id} className="collection">
                    <Row>
                        <Col xs={3} md={3}>
                            <p className="card-text">{collection.name}</p>
                        </Col>
                        <Col xs={3} md={3}>
                            <a className="btn btn-primary" onClick={() => view_collection_function(collection.id, true)}>Read Collection</a>
                        </Col>
                        <Col xs={3} md={3}>
                            {player_read != true &&
                                <a className="btn btn-primary" onClick={() => view_collection_function(collection.id, false)}>Edit Collection</a>
                            }
                        </Col>
                        <Col xs={1} md={1}>
                            {player_read != true &&
                                <DeleteConfirmationButton id={collection.id} name="Collection" delete_function={delete_page_function} />
                            }
                        </Col>
                    </Row>
                    <hr/>
                </div>
            ))}
        </div>
    );
}

export default ListCollections;