import React, { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const CreateComponentButton = ({order_position, column_number, create_function}) => {

    const [isVisible, setIsVisible] = useState(false);

    const toggle = () => setIsVisible(!isVisible);

    function process_selection(order_position, column_number, component_type){
        toggle()
        create_function(order_position, column_number, component_type)
    }

    var popover = <Popover className="create-component-popover">
        <Popover.Title>Insert New Component</Popover.Title>
        <Popover.Content className="content">
            <p>Select Component Type</p>
            <Row>
                <Col xs={4} md={4} className="column-button">
                    <div onClick={() => process_selection(order_position, column_number, "TEXT")}>
                        <i className="fas fa-font"></i>
                        <p>Text</p>
                    </div>
                </Col>
                <Col xs={4} md={4} className="column-button">
                    <div onClick={() => process_selection(order_position, column_number, "TABLE")}>
                        <i className="fas fa-table"></i>
                        <p>Table</p>
                    </div>
                </Col>
                <Col xs={4} md={4} className="column-button">
                    <div onClick={() => process_selection(order_position, column_number, "IMAGE")}>
                        <i className="fas fa-image"></i>
                        <p>Image</p>
                    </div>
                </Col>
                <Col xs={4} md={4} className="column-button">
                    <div onClick={() => process_selection(order_position, column_number, "MAP")}>
                        <i className="fas fa-compass"></i>
                        <p>Map</p>
                    </div>
                </Col>
                <Col xs={4} md={4} className="column-button">
                    <div onClick={() => process_selection(order_position, column_number, "SOUNDDECK")}>
                        <i class="fas fa-tablet-alt"></i>
                        <p>Sound Deck</p>
                    </div>
                </Col>
            </Row>
        </Popover.Content>
    </Popover>

    return (
        <div className="create-component">
            <div>
                <OverlayTrigger show={isVisible} trigger={['click']} placement="bottom" overlay={popover} onToggle={toggle}>
                    <i className="fas fa-plus"></i>
                </OverlayTrigger>
            </div>
        </div>
    );
}

export default CreateComponentButton;