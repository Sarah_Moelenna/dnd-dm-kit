import PlayMusicButton from './shortcode_components/PlayMusicButton'
import EncounterButton from './shortcode_components/EncounterButton'
import BeyondItem from './shortcode_components/BeyondItem'
import MonsterLink from './shortcode_components/MonsterLink'
import PageLink from './shortcode_components/PageLink'
import Map from './shortcode_components/Map'

export const process_markdown = (markdown) => {
    if(markdown == null){
        return null;
    }
    
    //MUSIC
    const music_regex = "(<MUSIC)([\\=a-zA-z0-9\\ \\-\%\']*)(>)"

    var music_matches = [...markdown.matchAll(music_regex)]
    for(var i = 0; i < music_matches.length; i++){
        var contents = music_matches[i][2]
        var full_match = music_matches[i][0]
        var new_markdown = "[" + contents + "](MUSIC)"
        
        markdown = markdown.replaceAll(full_match, new_markdown)
    }


    //ENCOUNTER
    const encounter_regex = "(<ENCOUNTER)([\\=a-zA-z0-9\\ \\-\%,\\*]*)(>)"

    var encounter_matches = [...markdown.matchAll(encounter_regex)]
    for(var i = 0; i < encounter_matches.length; i++){
        var contents = encounter_matches[i][2]
        var full_match = encounter_matches[i][0]
        var new_markdown = "[" + contents + "](ENCOUNTER)"
        
        markdown = markdown.replaceAll(full_match, new_markdown)
    }

    //Monster
    const monster_regex = "(<MONSTER)([\\=a-zA-z0-9\\ \\-\%,\\*]*)(>)"

    var monster_matches = [...markdown.matchAll(monster_regex)]
    for(var i = 0; i < monster_matches.length; i++){
        var contents = monster_matches[i][2]
        var full_match = monster_matches[i][0]
        var new_markdown = "[" + contents + "](MONSTER)"
        
        markdown = markdown.replaceAll(full_match, new_markdown)
    }

    //Page
    const page_regex = "(<PAGE)([\\=a-zA-z0-9\\ \\-\%,\\*\\'\\!\\?]*)(>)"

    var page_matches = [...markdown.matchAll(page_regex)]
    for(var i = 0; i < page_matches.length; i++){
        var contents = page_matches[i][2]
        var full_match = page_matches[i][0]
        var new_markdown = "[" + contents + "](PAGE)"
        
        markdown = markdown.replaceAll(full_match, new_markdown)
    }

    //Map
    const map_regex = "(<MAP)([\\=a-zA-z0-9\\ \\-\%,\\*\\'\\!\\?]*)(>)"

    var map_matches = [...markdown.matchAll(map_regex)]
    for(var i = 0; i < map_matches.length; i++){
        var contents = map_matches[i][2]
        var full_match = map_matches[i][0]
        var new_markdown = "[" + contents + "](MAP)"
        
        markdown = markdown.replaceAll(full_match, new_markdown)
    }

    //Item
    const dnd_beyond_item_markdown = "(https:\\/\\/www\\.dndbeyond\\.com\\/magic-items\\/|https:\\/\\/www\\.dndbeyond\\.com\\/equipment\\/)([a-zA-Z0-9\\-]*)"
    var item_matches = [...markdown.matchAll(dnd_beyond_item_markdown)]
    for(var i = 0; i < item_matches.length; i++){
        var contents = item_matches[i][0] + " " + item_matches[i][2]
        var full_match = item_matches[i][0]
        var new_markdown = "[" + contents + "](ITEM)"
        
        markdown = markdown.replaceAll(full_match, new_markdown)
    }
    

    return markdown
}

export const create_link = (props, override_function, player_read, block_map) => {

    if(props.href=="ITEM"){
        return (<BeyondItem string_in={props.children[0]}/>)
    }
    if(props.href=="PAGE"){
        return (<PageLink string_in={props.children[0]} override_function={override_function}/>)
    }
    
    if(props.href=="ENCOUNTER"){
        if(player_read == true){ return ''}
        return (<EncounterButton string_in={props.children[0]} override_function={override_function}/>)
    }
    if(props.href=="MUSIC"){
        if(player_read == true){ return ''}
        return (<PlayMusicButton string_in={props.children[0]} />)
    }
    if(props.href=="MONSTER"){
        return (<MonsterLink string_in={props.children[0]} override_function={override_function} player_read={player_read}/>)
    }
    if(props.href=="MAP" && block_map != true){
        if(player_read == true){ return ''}
        return (<Map string_in={props.children[0]} override_function={override_function}/>)
    }
    if(props.children.length > 0){
        return (<a href={props.href} target="_blank">{props.children[0]}</a>);
    }

    return ''
}
