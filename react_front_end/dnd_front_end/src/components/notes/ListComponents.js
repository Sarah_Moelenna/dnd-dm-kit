import React from 'react';
import CreateComponentButton from './CreateComponentButton';
import DeleteComponent from './DeleteComponent';
import TextComponent from './component/TextComponent'
import TableComponent from './component/TableComponent'
import ImageComponent from './component/ImageComponent'
import MapComponent from './component/MapComponent'
import SoundDeckComponent from './component/SoundDeck';
import { DND_POST, DND_DELETE } from '.././shared/DNDRequests';

const ListComponents = ({ override_function, section_id, section_column, components, refresh_function, read, player_read}) => {

    function create_component(order_position, column_number, component_type){

        var contents = null;

        if (component_type == "TEXT"){
            contents = {
                "type": "TEXT",
                "text": null,
            }
        } else if (component_type == "TABLE"){
            contents = {
                "type": "TABLE",
                "headers": [""],
                "rows": {0: [""]},
            }
        } else if (component_type == "IMAGE"){
            contents = {
                "type": "IMAGE",
                "url": null,
            }
        } else if (component_type == "MAP"){
            contents = {
                "type": "MAP",
                "map_id": null,
            }
        } else if (component_type == "SOUNDDECK"){
            contents = {
                "type": "SOUNDDECK",
                "effect_deck_id": null,
            }
        }
        
        var data = {
            "column": column_number,
            "order": order_position,
            "contents": contents,
        }

        DND_POST(
            '/section/' + section_id + "/component",
            data,
            (response) => {
                refresh_function()
            },
            null
        )
      
    }

    function delete_component(component_id){

        DND_DELETE(
            '/component/' + component_id,
            null,
            (response) => {
                refresh_function()
            },
            null
        )
      
    }

    if(components === undefined || components.length == 0) {
        if(read == false){
            return(
                <div>
                    <CreateComponentButton order_position={0} column_number={section_column} create_function={create_component}/>
                </div>
            );
        } else {
            return null
        }
    }
    return (
        <div className="component-items">
            {read == false &&
                <CreateComponentButton order_position={0} column_number={section_column} create_function={create_component}/>
            }
            {components.map((component, index) => (
                <div key={component.id}>
                    <div className={index == components.length - 1 ? "component-item last" : "component-item"} >
                        { component.contents.type == "TEXT" &&
                            <TextComponent  player_read={player_read} override_function={override_function} read={read} component={component} />
                        }
                        { component.contents.type == "TABLE" &&
                            <TableComponent  player_read={player_read} override_function={override_function} read={read} component={component} />
                        }
                        { component.contents.type == "IMAGE" &&
                            <ImageComponent  player_read={player_read} read={read} component={component} />
                        }
                        { component.contents.type == "MAP" &&
                            <MapComponent  player_read={player_read} override_function={override_function} read={read} component={component} />
                        }
                        { component.contents.type == "SOUNDDECK" &&
                            <SoundDeckComponent  player_read={player_read} read={read} component={component} />
                        }
                        {read == false &&
                            <DeleteComponent component_id={component.id} delete_function={delete_component}/>
                        }
                    </div>
                    {read == false &&
                        <CreateComponentButton order_position={component.order+1} column_number={section_column} create_function={create_component}/>
                    }
                </div>
            ))}
        </div>
    );
}

export default ListComponents;