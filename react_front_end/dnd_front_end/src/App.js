import React, {Component} from 'react';

import DungeonMasterApp from './components/DungeonMasterApp';
import PlayerApp from './components/PlayerApp';
import LoginPage from './components/pages/LoginPage';
import Cookies from 'universal-cookie';
import {BrowserRouter } from "react-router-dom";

const cookies = new Cookies();

class App extends Component {

  state = {
    user_type: null,
    session_id: null,
    user_data: null
  }

  logout = () => {
    cookies.remove('auth_token', { path: '/' });
    cookies.remove('session_id', { path: '/' });
    this.setState({user_type: null, session_id: null})
  }

  set_user_type = (new_value, session_id, user_data) => {
    if(user_data != undefined){
      this.setState({ user_type: new_value, session_id: session_id, user_data: user_data})
    } else {
      this.setState({ user_type: new_value, session_id: session_id})
    }
  };


  render () {
    if (this.state.user_type == "DM"){
      return <BrowserRouter><DungeonMasterApp logout={this.logout}/></BrowserRouter>
    }
    else if (this.state.user_type == "USER"){
      return <BrowserRouter><PlayerApp logout={this.logout} session_id={this.state.session_id} user_data={this.state.user_data}/></BrowserRouter>
    }
    else{
      return <BrowserRouter><LoginPage set_user_type={this.set_user_type}/></BrowserRouter>;
    }
  }
}

export default App;
