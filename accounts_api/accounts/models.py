from __future__ import annotations
from xmlrpc.client import Boolean
from django.db.models import CASCADE
from django.db import models
import uuid
from django.contrib.auth.hashers import make_password, check_password
from django.db.utils import IntegrityError
from rest_framework.exceptions import NotFound
from . utils import generate_salt
from typing import Optional, Dict, List
from datetime import datetime, timedelta
from django.utils import timezone

class UserType(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    type = models.CharField(max_length=50)
    base_storage_allowance = models.IntegerField()
    
    @property
    def permissions(self) -> List[Permissions]:
        #Uses usertypepermission_set to retrieve the permissions tied to the current instance of UserType
        return [usertypepermission.permission for usertypepermission in self.usertypepermission_set.all()]

class Permissions (models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    key = models.CharField(max_length=200, unique=True)

    def to_dict(self):
        return {
            "id" : self.id,
            "key" : self.key
        }

class UserTypePermission(models.Model):
    user_type = models.ForeignKey(UserType, on_delete=CASCADE)
    permission = models.ForeignKey(Permissions, on_delete=CASCADE)

class User(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    forename = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    email = models.CharField(max_length=200, unique=True)
    display_name = models.CharField(unique=True, max_length=50)
    password = models.CharField(max_length = 1000)
    salt = models.CharField(max_length=50)
    is_deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    deleted_at = models.DateTimeField(null=True)
    user_type = models.ForeignKey(UserType, on_delete=CASCADE)
    has_confirmed_email = models.BooleanField(default=False)

    @property
    def permissions(self) -> List[str]:
        return [permission.key for permission in self.user_type.permissions]

    @staticmethod
    def create_user(forename: str, surname: str, email:str, display_name:str, password: str, user_type:Optional[UserType]=None) -> User:
        if not user_type:
            user_type = UserType.objects.filter(type="USER").get()
        salt = generate_salt()
        hashed_password = make_password(password+salt)
        user = User(
            forename=forename, 
            surname=surname, 
            email=email, 
            display_name=display_name, 
            password=hashed_password, 
            salt=salt,
            user_type=user_type)
        
        try:
            user.save()
        except IntegrityError as e:
            print (f'Caught exception while saving User: {str(e)}')
            raise ValueError("Duplicate column value.")

        return user
    
    @staticmethod
    def login(email: str, password:str):
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            raise ValueError("Invalid email.")
        if not check_password(password+user.salt, user.password):
            raise ValueError("Invalid password.")
        return user
    
    def update_password(self, current_password: str, new_password: str) -> None:
        if not check_password(current_password+self.salt, self.password):
            raise ValueError("Invalid password.")
        salt = generate_salt()
        hashed_password = make_password(new_password+salt)
        self.salt = salt
        self.password = hashed_password
        self.save()

    def to_dict(self) -> Dict[str, str]:
        return {
            "id": str(self.id),
            "forename": self.forename,
            "surname": self.surname,
            "email": self.email,
            "display_name": self.display_name,
            "user_type": self.user_type.type,
            "permissions": [permission.key for permission in self.user_type.permissions]
        }

    def update_user(self, forename:str, surname:str, display_name:str) -> None:
        if forename:
            self.forename = forename
        if surname:
            self.surname = surname
        if display_name:
            self.display_name = display_name
        self.save()
    
    def update_user_admin(self, forename:str, surname:str, email:str, display_name:str, password:str, user_type:UserType) -> None:
        if forename:
            self.forename = forename
        if surname:
            self.surname = surname
        if email:
            self.email = email
        if display_name:
            self.display_name = display_name
        if password:
            salt = generate_salt()
            hashed_password = make_password(password+salt)
            self.password = hashed_password
            self.salt = salt
        if user_type:
            self.user_type = user_type
        self.save()
    
    def has_permissions(self, permissions: List[str]) -> Boolean:
        """
        Returns whether a user has all of the given permissions
        """
        return set(permissions).issubset(self.permissions)
    
    @staticmethod
    def get_user_by_id(user_id: str) -> User:
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise NotFound("User not found.")
        if user.is_deleted == True:
            raise NotFound(detail="User not found.")
        return user
    
    @staticmethod
    def delete_user_by_id(user_id: str) -> User:
        user = User.get_user_by_id(user_id)
        user.is_deleted = True
        user.deleted_at = datetime.now()
        user.save()

class SubscriptionType(models.Model):
    slug = models.CharField(max_length=100, primary_key=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=2000)
    monthly_price = models.FloatField()
    storage_allowance = models.IntegerField()

class RenewalPeriod(models.Model):
    slug = models.CharField(max_length=100, primary_key=True)
    name = models.CharField(max_length=100)
    months = models.IntegerField()
    monthly_price = models.IntegerField()

class Subscription(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    user = models.ForeignKey(User, on_delete=CASCADE)
    subscription_type = models.OneToOneField(SubscriptionType, on_delete=CASCADE)
    renewal_period = models.OneToOneField(RenewalPeriod, on_delete=CASCADE)
    is_active = models.BooleanField(default= False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    ended_at = models.DateTimeField(null=True)

class AuthToken(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE)
    token = models.CharField(primary_key=True, max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    expires_at = models.DateTimeField()

    @staticmethod
    def create_token(user: User) -> AuthToken:
        token = AuthToken(
            token=str(uuid.uuid4()),
            user=user,
            expires_at=(datetime.now() + timedelta(hours=48)))
        token.save()
        return token
    
    @staticmethod
    def validate_token(token: str) -> User:
        try:
            token = AuthToken.objects.get(token=token)
        except AuthToken.DoesNotExist:
            raise ValueError("Invalid token.")
        if token.expires_at < timezone.now():
            raise ValueError("Expired token.")
        return token.user

    def to_dict(self) -> Dict[str, str]:
        return {
            "token": self.token,
            "expires_at": self.expires_at
        }

class Invoice(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    user = models.ForeignKey(User, on_delete=CASCADE)
    title = models.CharField(max_length=50)
    amount = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)

class InvoiceItem(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    invoice = models.ForeignKey(Invoice,on_delete=CASCADE)
    title = models.CharField(max_length=50)
    amount = models.FloatField()

class EmailConfirmationToken(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE)
    token = models.CharField(primary_key=True, max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    expires_at = models.DateTimeField()

    @staticmethod
    def create_email_confirmation(user: User) -> EmailConfirmationToken:
        confirmation = EmailConfirmationToken(
            token=str(uuid.uuid4()),
            user=user,
            expires_at=(datetime.now() + timedelta(hours=48)))
        confirmation.save()
        return confirmation




