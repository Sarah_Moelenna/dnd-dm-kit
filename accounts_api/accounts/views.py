from __future__ import annotations
from django.shortcuts import render
from django.http import JsonResponse
from .models import EmailConfirmationToken, User, AuthToken, UserType, Permissions
from django.http import HttpResponse
from rest_framework.request import Request
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework.exceptions import MethodNotAllowed, ParseError, NotAuthenticated, NotFound
from accounts.middleware.auth import accounts_auth_middleware, is_authenticated, with_permissions, with_permissions_by_method
from django.core.paginator import Paginator

from .models import User

#End-point for registering a new user into the service.
@api_view(["POST"])
@parser_classes((JSONParser, ))
def register(request: Request):
    if request.method == 'POST':
        forename = request.data.get("forename")
        surname = request.data.get("surname")
        email = request.data.get("email")
        display_name = request.data.get("display_name")
        password = request.data.get("password")
        if None in [display_name, password, forename, surname, email]:
            raise ParseError(detail="Username, Password and Email must be provided.")
        try:
            user = User.create_user(
                forename=forename,
                surname=surname,
                display_name=display_name,
                password=password,
                email=email
            )
        except ValueError:
            raise ParseError(detail="Username and Email must be unique.")
        token = AuthToken.create_token(user)
        EmailConfirmationToken.create_email_confirmation(user=user)
        data = token.to_dict()

        return JsonResponse(data, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["POST"])
@parser_classes((JSONParser, ))
def login(request: Request):
    if request.method == 'POST':
        email = request.data.get("email")
        password = request.data.get("password")
        if email is None or password is None:
            raise ParseError(detail="Email and Password must be provided.")
        try:
            user = User.login(email, password)
        except ValueError:
            raise ParseError(detail="Invalid login.")
        token = AuthToken.create_token(user)

        return JsonResponse(token.to_dict(), safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["GET", "PUT"])
@parser_classes((JSONParser, ))
@accounts_auth_middleware
@is_authenticated
def user_self(request: Request):
    if request.method == 'GET':
        return JsonResponse(request.user.to_dict(), safe=False)
    if request.method == 'PUT':
        forename = request.data.get("forename")
        surname = request.data.get("surname")
        display_name = request.data.get("display_name")
        request.user.update_user(forename, surname, display_name)
        return JsonResponse(request.user.to_dict(), safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["PUT"])
@parser_classes((JSONParser, ))
@accounts_auth_middleware
@is_authenticated
def user_self_password(request: Request):
    if request.method == 'PUT':
        current_password = request.data.get("current_password")
        new_password = request.data.get("new_password")
        if current_password is None or new_password is None:
            raise ParseError(detail="Missing password(s).")
        request.user.update_password(current_password=current_password, new_password=new_password)
        return HttpResponse(status=201)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@accounts_auth_middleware
@is_authenticated
@with_permissions(["ACCOUNTS.USER.MANAGEMENT"])
def users(request: Request):
    if request.method == 'GET':
        display_name = request.GET.get("display_name", None)
        email = request.GET.get("email", None)
        user_type = request.GET.get("user_type", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        users = User.objects.exclude(is_deleted=True)
        if display_name:
            users = users.filter(display_name__icontains=display_name)
        if email:
            users = users.filter(email__icontains=email)
        if user_type:
            users = users.filter(user_type__type__icontains=user_type)
        
        p = Paginator(users, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [user.to_dict() for user in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    if request.method == 'POST':
        forename = request.data.get("forename")
        surname = request.data.get("surname")
        email = request.data.get("email")
        display_name = request.data.get("display_name")
        password = request.data.get("password")
        user_type = request.data.get("user_type")
        user_type_object = UserType.objects.filter(type=user_type).first()
        if not user_type_object:
            raise ParseError(detail="Specified user type not found.")
        if user_type == "SUPERADMIN":
            raise NotAuthenticated("Only one SUPERADMIN is allowed.")
        if None in [display_name, password, forename, surname, email, user_type]:
            raise ParseError(detail="Insufficient information provided. Need display name, password, forename, surname, email and user type.")
        try:
            user = User.create_user(
                forename=forename,
                surname=surname,
                display_name=display_name,
                password=password,
                email=email,
                user_type=user_type_object
            )
        except ValueError:
            raise ParseError(detail="Username and Email must be unique.")
        EmailConfirmationToken.create_email_confirmation(user=user)
        data = user.to_dict()
        return JsonResponse(data, safe=False)
    
@api_view(["GET", "DELETE", "PUT"])
@parser_classes((JSONParser, ))
@accounts_auth_middleware
@is_authenticated
@with_permissions(["ACCOUNTS.USER.MANAGEMENT"])
def users_by_id(request: Request, user_id):
    if request.method == 'GET':
        user = User.get_user_by_id(user_id=user_id)
        data = user.to_dict()
        
        return JsonResponse(data, safe=False)
    
    if request.method == 'DELETE':
        user = User.delete_user_by_id(user_id=user_id)

        return HttpResponse(status=204)
    
    if request.method == 'PUT':
        forename = request.data.get("forename")
        surname = request.data.get("surname")
        email = request.data.get("email")
        display_name = request.data.get("display_name")
        password = request.data.get("password")
        user_type = request.data.get("user_type")
        user_type_object = UserType.objects.filter(type=user_type).first()
        if not user_type_object:
            raise NotFound(detail="Specified user type not found.")
        if user_type == "SUPERADMIN":
            raise NotAuthenticated(detail="Only one SUPERADMIN is allowed.")
        user = User.get_user_by_id(user_id=user_id)
        if user.user_type.type == "SUPERADMIN" or user.is_deleted == True:
            raise NotFound("User not found.")
        user.update_user_admin(forename=forename, surname=surname, email=email, display_name=display_name, password=password, user_type=user_type_object)
        data = user.to_dict()

        return JsonResponse(data, safe=False)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@accounts_auth_middleware
@is_authenticated
@with_permissions(["ACCOUNTS.USER.MANAGEMENT"])
def user_types(request: Request):
    if request.method == 'GET':
        user_types_list= [user_type_object.type for user_type_object in UserType.objects.all()]

        return JsonResponse(user_types_list, safe=False)
    
@api_view(["GET"])
@parser_classes((JSONParser, ))
@accounts_auth_middleware
@is_authenticated
@with_permissions(["ACCOUNTS.USER.MANAGEMENT"])
def permissions(request: Request):
    if request.method == 'GET':
        permissions_list= [permission_object.key for permission_object in Permissions.objects.all()]

        return JsonResponse(permissions_list, safe=False)