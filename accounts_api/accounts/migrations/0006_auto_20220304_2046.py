# Generated by Django 3.2 on 2022-03-04 20:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_auto_20220304_2021'),
    ]

    operations = [
        migrations.RenameField(
            model_name='emailconfirmationtoken',
            old_name='user_id',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='invoice',
            old_name='user_id',
            new_name='user',
        ),
    ]
