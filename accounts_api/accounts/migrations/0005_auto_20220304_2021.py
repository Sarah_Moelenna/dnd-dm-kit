# Generated by Django 3.1.5 on 2022-03-04 20:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20220215_1656'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='has_confirmed_email',
            field=models.BooleanField(default=True),
        ),
        migrations.CreateModel(
            name='EmailConfirmationToken',
            fields=[
                ('token', models.CharField(max_length=200, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('expires_at', models.DateTimeField()),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.user')),
            ],
        ),
    ]
