from accounts.models import AuthToken
from rest_framework.exceptions import NotAuthenticated, MethodNotAllowed
from functools import wraps
from typing import List, Dict

#Authentication middleware function located in middleware/auth.py; either generates a user object or None
def accounts_auth_middleware(get_response):

    def middleware(request, *args, **kwargs):
        headers = request.META
        auth_token = headers.get("HTTP_TOKEN")
        try:
            user = AuthToken.validate_token(auth_token)
        except Exception:
            user = None
        request.user = user

        response = get_response(request, *args, **kwargs)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware

#decorator that is symbiotic with accounts_auth_middleware designed to follow DRY
def is_authenticated(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):

        user = request.user
        if user is not None:
            return function(request, *args, **kwargs)
        else:
            raise NotAuthenticated("Must provide valid user.")

    return wrap

def auth_admin_or_superadmin(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):

        user = request.user
        if user is not None:
            user_type = user.user_type
            if user_type == "ADMIN" or user_type == "SUPERADMIN":
                return function(request, *args, **kwargs)
            else: 
                raise NotAuthenticated("Insufficient permissions.")
                
        else:
            raise NotAuthenticated("Must provide user.")

    return wrap

def with_permissions(permissions: List[str]):
    """
    returns a new decorator that errors if a user doesn't have all the given permissions for any method

    e.g. ["ACCOUNTS.USER.MANAGEMENT", "ACCOUNTS.USER.CREATE"]
    """

    def decorator(function):
        """
        the decorator
        """

        @wraps(function)
        def wrap(request, *args, **kwargs):
            user = request.user
            if user is not None:
                if user.has_permissions(permissions):
                    return function(request, *args, **kwargs)
                else: 
                    raise NotAuthenticated("Insufficient permissions.")
                    
            else:
                raise NotAuthenticated("Must provide user.")

        return wrap
    return decorator

def with_permissions_by_method(permission_dict: Dict[str, List[str]]):
    """
    returns a new decorator that errors if a user doesn't have all the given permissions for a specific method

    example dict:
    {
        "GET": ["ACCOUNTS.USER.MANAGEMENT"],
        "POST": ["ACCOUNTS.USER.MANAGEMENT", "ACCOUNTS.USER.CREATE"]
    }
    """

    def decorator(function):
        """
        the decorator
        """

        @wraps(function)
        def wrap(request, *args, **kwargs):

            if request.method not in permission_dict:
                raise MethodNotAllowed("Method not allowed")

            permissions = permission_dict[request.method]
            user = request.user
            if user is not None:
                if user.has_permissions(permissions):
                    return function(request, *args, **kwargs)
                else: 
                    raise NotAuthenticated("Insufficient permissions.")
                    
            else:
                raise NotAuthenticated("Must provide user.")

        return wrap
    return decorator