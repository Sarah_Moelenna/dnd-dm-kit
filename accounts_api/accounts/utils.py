import random

def generate_salt() -> str:
    abc = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!£$%^&*()-=_+"
    chars=[]
    for i in range(16):
        chars.append(random.choice(abc))

    return "".join(chars)