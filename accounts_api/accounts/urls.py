from django.urls import path

from . import views

urlpatterns = [
    path('register', views.register, name='register'),
    path('login', views.login, name='login'),
    path('permissions', views.permissions, name='permissions'),
    path('self/password', views.user_self_password, name='self-password'),
    path('self', views.user_self, name='self'),
    path('users/<str:user_id>', views.users_by_id, name='users_by_id'),
    path('users', views.users, name='users'),
    path('user-types', views.user_types, name='user_types')
    ]