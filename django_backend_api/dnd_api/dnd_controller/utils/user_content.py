from __future__ import annotations

from typing import TYPE_CHECKING

from django.db.models import Q

from dnd_controller.models.all import (
    User,
    ContentCollection,
    Monster,
    Spell,
    Item,
    Race,
    BattleMap,
    Background,
    DndClass,
    SpellList,
    DndSubClass,
    Feat
)


def get_content_collection(user: User):
    return ContentCollection.objects.filter(Q(user=user) | Q(usercontentcollection__user=user))

def get_monster(user: User):
    if user.user_type == User.TYPE_ADMIN:
        return Monster.objects.filter(Q(user=user) | Q(contentcollectionmonster__content_collection__usercontentcollection__user=user))
    else:
        return Monster.objects.filter(Q(user=user, user_made_content=True) | Q(contentcollectionmonster__content_collection__usercontentcollection__user=user))

def get_item(user: User):
    if user.user_type == User.TYPE_ADMIN:
        return Item.objects.filter(Q(user=user) | Q(contentcollectionitem__content_collection__usercontentcollection__user=user))
    else:
        return Item.objects.filter(Q(user=user, user_made_content=True) | Q(contentcollectionitem__content_collection__usercontentcollection__user=user))

def get_race(user: User):
    if user.user_type == User.TYPE_ADMIN:
        return Race.objects.filter(Q(user=user) | Q(contentcollectionrace__content_collection__usercontentcollection__user=user))
    else:
        return Race.objects.filter(Q(user=user, is_homebrew=True) | Q(contentcollectionrace__content_collection__usercontentcollection__user=user))

def get_spell(user: User):
    
    if user.user_type == User.TYPE_ADMIN:
        return Spell.objects.filter(Q(user=user) | Q(contentcollectionspell__content_collection__usercontentcollection__user=user))
    else:
        return Spell.objects.filter(Q(user=user, is_homebrew=True) | Q(contentcollectionspell__content_collection__usercontentcollection__user=user))

def get_spell_list(user: User):
    
    if user.user_type == User.TYPE_ADMIN:
        return SpellList.objects.filter(Q(user=user) | Q(contentcollectionspelllist__content_collection__usercontentcollection__user=user))
    else:
        return SpellList.objects.filter(Q(user=user, is_homebrew=True) | Q(contentcollectionspelllist__content_collection__usercontentcollection__user=user))

def get_battlemap(user: User):
    return BattleMap.objects.filter(Q(user=user) | Q(contentcollectionbattlemap__content_collection__usercontentcollection__user=user))

def get_background(user: User):
    if user.user_type == User.TYPE_ADMIN:
        return Background.objects.filter(Q(user=user) | Q(contentcollectionbackground__content_collection__usercontentcollection__user=user))
    else:
        return Background.objects.filter(Q(user=user, is_homebrew=True) | Q(contentcollectionbackground__content_collection__usercontentcollection__user=user))

def get_dnd_class(user: User):
    if user.user_type == User.TYPE_ADMIN:
        return DndClass.objects.filter(Q(user=user) | Q(contentcollectiondndclass__content_collection__usercontentcollection__user=user))
    else:
        return DndClass.objects.filter(Q(user=user, is_homebrew=True) | Q(contentcollectiondndclass__content_collection__usercontentcollection__user=user))

def get_dnd_sub_class(user: User):
    if user.user_type == User.TYPE_ADMIN:
        return DndSubClass.objects.filter(Q(user=user) | Q(contentcollectiondndsubclass__content_collection__usercontentcollection__user=user))
    else:
        return DndSubClass.objects.filter(Q(user=user, is_homebrew=True) | Q(contentcollectiondndsubclass__content_collection__usercontentcollection__user=user))

def get_feat(user: User):
    if user.user_type == User.TYPE_ADMIN:
        return Feat.objects.filter(Q(user=user) | Q(contentcollectionfeat__content_collection__usercontentcollection__user=user))
    else:
        return Feat.objects.filter(Q(user=user, is_homebrew=True) | Q(contentcollectionfeat__content_collection__usercontentcollection__user=user))