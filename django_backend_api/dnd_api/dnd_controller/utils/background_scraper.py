from markdownify import markdownify

from dnd_controller.models.all import (
    Background,
    EquipmentSet,
    BackgroundModifier,
    EquipmentSetGroup,
    EquipmentSetGroupOption,
    Item,
    EquipmentSetGroupOptionItem,
    EquipmentSetGroupOptionSubset,
    EquipmentSetGroupOptionSubsetItem,
    BackgroundEquipmentSet,
    BackgroundTrait,
    BackgroundIdeal,
    BackgroundBond,
    BackgroundFlaw,
    User
)
from dnd_controller.utils.race_scraper import create_modifier
from dnd_controller.utils.type_data import get_type_for_name, get_subtype_for_name_and_type_id
from typing import List, Dict

PROVIDE_ALL = "PROVIDE ALL"
SELECT_ONE = "SELECT_ONE"

def get_item(id, name):
    items = Item.objects.filter(source_id=id,name=name).all()
    if len(items) > 1:
        raise ValueError(f'Found Too Many Items {id} {name}')
    elif len(items) == 0:
        raise ValueError(f'No Item Found {id} {name}')
    else:
        return items[0]

def create_equipment_set(data: List[Dict], description: str) -> EquipmentSet:
    equipment_set = EquipmentSet(
        description = markdownify(description)
    )
    equipment_set.save()

    for group in data:
        equipment_set_group = EquipmentSetGroup(
            name = markdownify(group.get('name')),
            equipment_set = equipment_set
        )
        equipment_set_group.save()

        for option in group.get('options', []):
            equipment_set_group_option = EquipmentSetGroupOption(
                name = markdownify(option.get('name')),
                gold = option.get('gold'),
                item_quantity = option.get('item_quantity', 1),
                custom_items = ",".join(option.get('custom_items')),
                equipment_set_group = equipment_set_group
            )

            if len(option.get('items', [])) > 0:
                item_objs = option.get('items', [])
                if isinstance(item_objs[0], list):
                    equipment_set_group_option.select_type = SELECT_ONE
                else:
                    equipment_set_group_option.select_type = PROVIDE_ALL

            equipment_set_group_option.save()

            are_lists_extra = False
            lists_found = 0
            singles_found = 0
            for item_collection in option.get('items', []):
                if isinstance(item_collection, list):
                    lists_found = lists_found + 1
                else:
                    singles_found = singles_found + 1
            if singles_found > 0 and lists_found > 0:
                are_lists_extra = True
            if lists_found > 1:
                are_lists_extra = True


            for item_collection in option.get('items', []):
                if isinstance(item_collection, list):
                    if are_lists_extra == False:
                        for item_obj in item_collection:
                            item = get_item(item_obj.get('id'), item_obj.get('name'))
                            equip_item =EquipmentSetGroupOptionItem(
                                item=item,
                                equipment_set_group_option = equipment_set_group_option
                            )
                            equip_item.save()
                    else:
                        new_subset = EquipmentSetGroupOptionSubset(
                            select_type=SELECT_ONE,
                            equipment_set_group_option=equipment_set_group_option
                        )
                        new_subset.save()
                        for item_obj in item_collection:
                            item = get_item(item_obj.get('id'), item_obj.get('name'))
                            equip_item =EquipmentSetGroupOptionSubsetItem(
                                item=item,
                                equipment_set_group_option_subset = new_subset
                            )
                            equip_item.save()
                else:
                    item = get_item(item_collection.get('id'), item_collection.get('name'))
                    equip_item =EquipmentSetGroupOptionItem(
                        item=item,
                        equipment_set_group_option = equipment_set_group_option
                    )
                    equip_item.save()

    return equipment_set


def scrape_background(background):

    user = User.objects.get(pk="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc")
    try:
        background_obj = Background.objects.get(name=background.get('name'), user=user)
    except Exception as e:
        background_obj = Background(name=background.get('name'), user=user)

    background_obj.short_description = markdownify(background.get('shortDescription'))
    background_obj.skill_proficiencies_description = markdownify(background.get('skillProficienciesDescription'))
    background_obj.tool_proficiencies_description = markdownify(background.get('toolProficienciesDescription'))
    background_obj.languages_description =markdownify(background.get('languagesDescription'))
    background_obj.feature_name = markdownify(background.get('featureName'))
    background_obj.feature_description = markdownify(background.get('featureDescription'))
    background_obj.suggested_characteristics_description = markdownify(background.get('suggestedCharacteristicsDescription'))
    background_obj.is_homebrew = background.get('isHomebrew')

    background_obj.save()

    # personality stuff
    for personality_traits in background.get('personalityTraits', []):
        trait = BackgroundTrait(
            description = personality_traits['description'],
            background = background_obj
        )
        trait.save()

    for ideal in background.get('ideals', []):
        trait = BackgroundIdeal(
            description = ideal['description'],
            background = background_obj
        )
        trait.save()

    for bond in background.get('bonds', []):
        trait = BackgroundBond(
            description = bond['description'],
            background = background_obj
        )
        trait.save()

    for flaw in background.get('flaws', []):
        trait = BackgroundFlaw(
            description = flaw['description'],
            background = background_obj
        )
        trait.save()

    # equipment
    equipment_set = create_equipment_set(background.get('equipment'), background.get('equipmentDescription'))
    background_equipment_obj = BackgroundEquipmentSet(
        background = background_obj,
        equipment_set = equipment_set
    )
    background_equipment_obj.save()


    # modifiers
    modifiers = background.get('modifiers')
    for modifier_in in modifiers:

        if modifier_in['modifierTypeId'] == 0:
            type_obj = get_type_for_name(modifier_in['type'])
            if type_obj is not None:
                modifier_in['modifierTypeId'] = type_obj['id']

                subtype_obj = get_subtype_for_name_and_type_id(modifier_in['modifierTypeId'], modifier_in['subType'])
                if subtype_obj is not None:
                    modifier_in['modifierSubTypeId'] = subtype_obj['id']

        modifier_obj = create_modifier(modifier_in)

        background_modifier_obj = BackgroundModifier(
                background=background_obj,
                modifier=modifier_obj
            )
        background_modifier_obj.save()