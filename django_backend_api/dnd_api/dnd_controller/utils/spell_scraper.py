from dnd_controller.models.all import Spell, SpellModifier, User
from dnd_controller.utils.race_scraper import create_modifier
from typing import Dict, List
from markdownify import markdownify
import json
from dnd_controller.utils.type_data import get_type_for_name, get_subtype_for_name_and_type_id

def scrape_spells(spells: List[Dict]):
    for spell in spells:
        user = User.objects.get(pk="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc")

        spell_obj = Spell(user=user)

        spell_obj.name = spell['name']
        spell_obj.level = spell['level']
        spell_obj.school = spell['school']
        spell_obj.duration = json.dumps(spell['duration']) if spell['duration'] else None #json
        spell_obj.activation = json.dumps(spell['activation']) if spell['activation'] else None #json
        spell_obj.range = json.dumps(spell['range']) if spell['range'] else None #json
        spell_obj.as_part_of_weapon_attack = spell['asPartOfWeaponAttack']
        spell_obj.description = markdownify(spell['description']) if spell['description'] else None
        spell_obj.snippet = spell['snippet']
        spell_obj.concentration = spell['concentration']
        spell_obj.ritual = spell['ritual']
        spell_obj.range_area = json.dumps(spell['rangeArea']) if spell['rangeArea'] else None #json
        spell_obj.damage_effect = json.dumps(spell['damageEffect']) if spell['damageEffect'] else None #json
        spell_obj.components = json.dumps(spell['components']) if spell['components'] else None #json
        spell_obj.components_description = spell['componentsDescription']
        spell_obj.save_dc_ability_id = spell['saveDcAbilityId']
        spell_obj.healing = json.dumps(spell['healing']) if spell['healing'] else None  #json
        spell_obj.healing_dice = json.dumps(spell['healingDice']) if spell['healingDice'] else None #json
        spell_obj.temp_hp_dice = json.dumps(spell['tempHpDice']) if spell['tempHpDice'] else None #json
        spell_obj.attack_type = spell['attackType']
        spell_obj.can_cast_at_higher_level = spell['canCastAtHigherLevel']
        spell_obj.is_homebrew = spell['isHomebrew']
        spell_obj.requires_saving_throw = spell['requiresSavingThrow']
        spell_obj.requires_attack_roll = spell['requiresAttackRoll']
        spell_obj.at_higher_levels = json.dumps(spell['atHigherLevels']) if spell['atHigherLevels'] else None  #json
        spell_obj.conditions = json.dumps(spell['conditions']) if spell['conditions'] else None  #json
        spell_obj.tags = json.dumps(spell['tags']) if spell['tags'] else None #json
        spell_obj.casting_time_description = spell['castingTimeDescription']
        spell_obj.scale_type = spell['scaleType']
        spell_obj.spell_groups = json.dumps(spell['spellGroups']) if spell['spellGroups'] else None #json

        spell_obj.save()


        modifiers = spell.get('modifiers')
        for modifier_in in modifiers:

            modifier_in["dice"] = modifier_in["die"]

            if modifier_in['modifierTypeId'] == 0:
                type_obj = get_type_for_name(modifier_in['type'])
                if type_obj is not None:
                    modifier_in['modifierTypeId'] = type_obj['id']

                    subtype_obj = get_subtype_for_name_and_type_id(modifier_in['modifierTypeId'], modifier_in['subType'])
                    if subtype_obj is not None:
                        modifier_in['modifierSubTypeId'] = subtype_obj['id']

            modifier_obj = create_modifier(modifier_in)

            spell_modifier_obj = SpellModifier(
                    spell=spell_obj,
                    modifier=modifier_obj
                )
            spell_modifier_obj.save()