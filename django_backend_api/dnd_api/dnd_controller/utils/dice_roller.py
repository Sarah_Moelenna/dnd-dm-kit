from typing import Dict
from random import randint
from dnd_controller.utils import bot_commands
from dnd_controller.models.all import User, Campaign

class Dice():
    
    def __init__(self, sides: int):
        self.sides = sides
        self.result = randint(1, self.sides)

    def get_result(self) -> int:
        return self.result

    def to_dict(self) -> Dict:
        return {
            "sides": self.sides,
            "result": self.result,
        }


def get_roll_result(roll: str) -> Dict:

    results = {
        "dice": [],
        "total_result": 0,
        "modifier": "0",
    }

    # just plus
    if roll.startswith('+'):
        dice = Dice(20)
        results["dice"].append(dice.to_dict())
        results["total_result"] = results["total_result"] + dice.get_result()

        modifier = int(roll.replace("+", ""))
        results["modifier"] = f"+{modifier}"
        results["total_result"] = results["total_result"] + modifier
        return results

    # just minus
    elif roll.startswith('-'):
        dice = Dice(20)
        results["dice"].append(dice.to_dict())
        results["total_result"] = results["total_result"] + dice.get_result()

        modifier = int(roll.replace("+", ""))
        results["modifier"] = f"-{modifier}"
        results["total_result"] = results["total_result"] - modifier
        return results

    # dice specified
    elif roll[0] == "d":
        modifier = 0
        modifier_plus = False
        roll_split = roll.split("d")
        roll_sides = roll_split[1]
        if "+" in roll_sides:
            roll_split_modifier = roll_sides.split("+")

            dice = Dice(int(roll_split_modifier[0]))
            results["dice"].append(dice.to_dict())
            results["total_result"] = results["total_result"] + dice.get_result()

            modifier = int(roll_split_modifier[1])
            results["modifier"] = f"+{modifier}"
            modifier_plus = True
        elif "-" in roll_sides:
            roll_split_modifier = roll_sides.split("-")

            dice = Dice(int(roll_split_modifier[0]))
            results["dice"].append(dice.to_dict())
            results["total_result"] = results["total_result"] + dice.get_result()

            modifier = int(roll_split_modifier[1])
            results["modifier"] = f"-{modifier}"
            modifier_plus = False
        else:
            dice = Dice(int(roll_sides))
            results["dice"].append(dice.to_dict())
            results["total_result"] = results["total_result"] + dice.get_result()

        if modifier_plus:
            results["total_result"] = results["total_result"] + modifier
        else:
            results["total_result"] = results["total_result"] - modifier
        return results
    elif roll[0].isdigit():
        modifier = 0
        modifier_plus = False
        roll_split = roll.split("d")
        roll_sides = roll_split[1]
        for i in range(0, int(roll_split[0])):
            if "+" in roll_sides:
                roll_split_modifier = roll_sides.split("+")

                dice = Dice(int(roll_split_modifier[0]))
                results["dice"].append(dice.to_dict())
                results["total_result"] = results["total_result"] + dice.get_result()

                modifier = int(roll_split_modifier[1])
                results["modifier"] = f"+{modifier}"
                modifier_plus = True
            elif "-" in roll_sides:
                roll_split_modifier = roll_sides.split("-")

                dice = Dice(int(roll_split_modifier[0]))
                results["dice"].append(dice.to_dict())
                results["total_result"] = results["total_result"] + dice.get_result()

                modifier = int(roll_split_modifier[1])
                results["modifier"] = f"-{modifier}"
                modifier_plus = False
            else:
                dice = Dice(int(roll_sides))
                results["dice"].append(dice.to_dict())
                results["total_result"] = results["total_result"] + dice.get_result()

        if modifier_plus:
            results["total_result"] = results["total_result"] + modifier
        else:
            results["total_result"] = results["total_result"] - modifier
        return results

def post_roll_result(roll_result: Dict, user: User):

    roll = (f'd20{roll_result["roll"]}' if (roll_result['roll'].startswith("+") or roll_result['roll'].startswith("-")) else roll_result['roll']).replace("+", " + ").replace("-", " - ")

    message = f"**{roll_result['identity']}** rolled {roll}"

    if roll_result['roll_modifier'] != "DEFAULT":
        message = message + f" with {roll_result['roll_modifier'].lower().title()}"
    
    message = message + "\n"

    roll_line = "("
    for roll in roll_result['selected_roll']['dice']:
        if roll['result'] == 1 or roll['result'] == roll['sides']:
            roll_line = roll_line + f"**{roll['result']}**, "
        else:
            roll_line = roll_line + f"{roll['result']}, "
    roll_line = roll_line[:-2]

    roll_line = roll_line + ") " + (roll_result['selected_roll']['modifier'].replace("+", "+ ").replace("-", "- ") if roll_result['selected_roll']['modifier'] != "0" else "")
    roll_line = roll_line + f" Result: {roll_result['selected_roll']['total_result']}\n"
    message = message + roll_line

    if roll_result['unselected_roll']:
        roll_line = "~~("
        for roll in roll_result['unselected_roll']['dice']:
            if roll['result'] == 1 or roll['result'] == roll['sides']:
                roll_line = roll_line + f"**{roll['result']}**, "
            else:
                roll_line = roll_line + f"{roll['result']}, "
        roll_line = roll_line[:-2]

        roll_line = roll_line + ") " + (roll_result['unselected_roll']['modifier'].replace("+", "+ ").replace("-", "- ") if roll_result['unselected_roll']['modifier'] != "0" else "")
        roll_line = roll_line + f" Result: {roll_result['unselected_roll']['total_result']}~~\n"
        message = message + roll_line

    data = {
        "text": message,
        "location": "ROLLING"
    }

    
    campaign = Campaign.get_active(user)
    if campaign is not None:
        bot_commands.process_bot_command("CMD_SHARE_TXT", data, user, str(campaign.id))