from typing import Dict
from dnd_controller.utils.redis import delete_data, post_data, append_data
from dnd_controller.models.all import User
from dnd_controller.models import AudioFile, Playlist
import random

def process_play(data: Dict, user: User, campaign_id: str):
    music_play_type = data['music_play_type']
    audio_file: AudioFile = AudioFile.objects.get(pk=data['audio_id'], user=user)
    file_name = audio_file.file_name

    if audio_file.audio_type == AudioFile.EFFECT:
        return

    command_data_to_send = {
        "command_code": "CMD_PLAY",
        "campaign_id": campaign_id,
        "music_play_type": music_play_type,
        "file_name": file_name
    }
    
    post_data(
        f"{user.id}-{audio_file.audio_type.lower()}",
        {
            "tracks": [
                {
                    "id": str(audio_file.id),
                    "file_name": file_name,
                    "display_name": audio_file.display_name
                }
            ],
            "playlist": None
        }
    )
    append_data("COMMANDS", command_data_to_send)
    return True

def process_playlist_play(data: Dict, user: User, campaign_id: str):
    playlist: Playlist = Playlist.objects.get(pk=data['playlist_id'], user=user)

    tracks = []
    for audio_item in playlist.audio_items:
        tracks.append(
            {
                "id": str(audio_item.id),
                "file_name": audio_item.file_name,
                "display_name": audio_item.display_name
            }
        )
    random.shuffle(tracks)
    post_data(
        f"{user.id}-{playlist.audio_type.lower()}",
        {
            "tracks": tracks,
            "playlist": playlist.name
        }
    )
    return True

def process_stop_music(data: Dict, user: User, campaign_id: str):
    delete_data(f"{user.id}-music")

def process_stop_ambience(data: Dict, user: User, campaign_id: str):
    delete_data(f"{user.id}-ambience")

def process_image_share(data: Dict, user: User, campaign_id: str):
    command_data_to_send = {
        "command_code": "CMD_SHARE_IMG",
        "campaign_id": campaign_id,
        "url": data['url'],
        "location": data['location'],
    }

    append_data("COMMANDS", command_data_to_send)
    return True

def process_image_upload(data: Dict, user: User, campaign_id: str):
    command_data_to_send = {
        "command_code": "CMD_UPLOAD_IMG",
        "campaign_id": campaign_id,
        "filepath": data['filepath'],
        "location": data['location'],
        "delete": data['auto_delete'] if 'auto_delete' in data.keys() else False,
    }

    append_data("COMMANDS", command_data_to_send)
    return True

def process_text_share(data: Dict, user: User, campaign_id: str):
    command_data_to_send = {
        "command_code": "CMD_SHARE_TXT",
        "campaign_id": campaign_id,
        "text": data['text'],
        "location": data['location'],
    }

    append_data("COMMANDS", command_data_to_send)
    return True


command_mapping = {
    "CMD_PLAYLIST": process_playlist_play,
    "CMD_PLAY": process_play,
    "CMD_STOP": process_stop_music,
    "CMD_STOP_MUSIC": process_stop_music,
    "CMD_STOP_AMBIENCE": process_stop_ambience,
    "CMD_SHARE_IMG": process_image_share,
    "CMD_UPLOAD_IMG": process_image_upload,
    "CMD_SHARE_TXT": process_text_share
}

def process_bot_command(command_code: str, data: Dict, user: User, campaign_id: str):
    print(f'Command Code: {command_code} - Data: {str(data)}')
    return command_mapping[command_code](data, user, campaign_id)


