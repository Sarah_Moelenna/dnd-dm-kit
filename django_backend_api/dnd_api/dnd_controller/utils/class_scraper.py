from markdownify import markdownify
import json


from dnd_controller.models.all import (
    DndClass,
    DndClassFeature,
    DndClassFeatureAction,
    DndClassFeatureModifier,
    DndClassFeatureOptionSet,
    DndClassDndClassFeature,
    DndClassEquipmentSet,
    User,
    OptionSet,
    OptionSetOption,
    Option,
    OptionAction,
    OptionModifier,
)
from dnd_controller.utils.race_scraper import create_modifier, create_action
from dnd_controller.utils.type_data import get_type_for_name, get_subtype_for_name_and_type_id
from dnd_controller.utils.background_scraper import create_equipment_set
from dnd_controller.utils.user_content import get_spell
from uuid import uuid4


def resolve_spell_list_to_spell_selection_obj(spell_list):
    if spell_list is None:
        return None

    user = User.objects.get(pk="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc")
    spell_selection = {
        "selection_type": "CHOOSE",
        "groups": {},
        "spells": {}
    }

    for spell in spell_list:
        definition = spell.get('definition')
        spell_obj = get_spell(user).filter(name=definition.get('name')).first()
        spell_selection["spells"][str(spell_obj.id)] = {"name": spell_obj.name}
        
    return json.dumps(spell_selection)

def create_option(parent, option):
    description = markdownify(option.get('description')) if option.get('description') else None

    option_obj = Option(
        name = option.get('label'),
        description = description,
        spell_selection = resolve_spell_list_to_spell_selection_obj(option.get('spells')),
    )

    option_obj.save()

    relation_obj = OptionSetOption(
        option_set = parent,
        option = option_obj
    )
    relation_obj.save()

    # MODIFIERS
    modifiers = option.get('modifiers')
    if modifiers is not None:
        for modifier_in in modifiers:
            if modifier_in['modifierTypeId'] == 0:
                type_obj = get_type_for_name(modifier_in['type'])
                if type_obj is not None:
                    modifier_in['modifierTypeId'] = type_obj['id']

                    subtype_obj = get_subtype_for_name_and_type_id(modifier_in['modifierTypeId'], modifier_in['subType'])
                    if subtype_obj is not None:
                        modifier_in['modifierSubTypeId'] = subtype_obj['id']
            modifier_obj = create_modifier(modifier_in)
            option_modifier_obj = OptionModifier(
                    option=option_obj,
                    modifier=modifier_obj
                )
            option_modifier_obj.save()
    
    # ACTIONS
    actions = option.get('actions')
    if actions is not None:
        for action in actions:
            action_obj = create_action(action)

            feature_action_obj = OptionAction(
                option=option_obj,
                action=action_obj
            )
            feature_action_obj.save()

def create_option_set(option_set):
    level = None
    name = option_set.get('label')
    if isinstance(name, str) and 'level' in name.lower():
        level = [int(s) for s in name.split() if s.isdigit()][0]

    option_set_obj = OptionSet(
        name = name,
        level = level,
    )
    option_set_obj.save()

    # OPTIONS
    for option in option_set.get('choice_options'):
        create_option(option_set_obj, option)

    return option_set_obj

def create_feature(parent: DndClass, feature):
    feature_has_spells = 'spells' in feature.keys()
    spell_data = None
    if feature_has_spells:
        spell_data = resolve_spell_list_to_spell_selection_obj(feature.get('spells'))

    class_feature_obj = DndClassFeature(
        name = feature.get('name'),
        description = markdownify(feature.get('description')),
        level = feature.get('requiredLevel'),
        spell_selection = spell_data,
        gives_spells = feature_has_spells,
    )
    class_feature_obj.save()

    relation_obj = DndClassDndClassFeature(
        dnd_class = parent,
        dnd_class_feature = class_feature_obj
    )
    relation_obj.save()

    # MODIFIERS
    modifiers = feature.get('modifiers')
    for modifier_in in modifiers:
        if modifier_in['modifierTypeId'] == 0:
            type_obj = get_type_for_name(modifier_in['type'])
            if type_obj is not None:
                modifier_in['modifierTypeId'] = type_obj['id']

                subtype_obj = get_subtype_for_name_and_type_id(modifier_in['modifierTypeId'], modifier_in['subType'])
                if subtype_obj is not None:
                    modifier_in['modifierSubTypeId'] = subtype_obj['id']
        modifier_obj = create_modifier(modifier_in)
        feature_modifier_obj = DndClassFeatureModifier(
                dnd_class_feature=class_feature_obj,
                modifier=modifier_obj
            )
        feature_modifier_obj.save()
    
    # ACTIONS
    for action in feature.get('actions'):
        action_obj = create_action(action)

        feature_action_obj = DndClassFeatureAction(
            dnd_class_feature=class_feature_obj,
            action=action_obj
        )
        feature_action_obj.save()

    # CHOICES / OPTION SET
    for choice in feature.get('choices'):
        option_set_obj = create_option_set(choice)

        feature_option_set_obj = DndClassFeatureOptionSet(
            dnd_class_feature=class_feature_obj,
            option_set=option_set_obj
        )
        feature_option_set_obj.save()

def process_prequisite_data(prequisites):
    final_prequisites = []
    for prequisite in prequisites:
        new_prequisite_obj = {
            "id": str(uuid4()),
            "description": prequisite.get('description'),
            "mappings": []
        }
        for mapping in prequisite.get("prerequisiteMappings"):
            new_mapping_obj = {
                "id": str(uuid4()),
                "type": mapping.get('type'),
                "subtype": mapping.get('subType'),
                "value": mapping.get('value'),
            }
            new_prequisite_obj['mappings'].append(new_mapping_obj)
        final_prequisites.append(new_prequisite_obj)
    return json.dumps(final_prequisites)


def scrape_class(class_data):

    user = User.objects.get(pk="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc")
    try:
        class_obj = DndClass.objects.get(name=class_data.get('name'), user=user)
    except Exception as e:
        class_obj = DndClass(name=class_data.get('name'), user=user)

    class_obj.description = markdownify(class_data.get('description'))
    class_obj.avatar_url = class_data.get('largeAvatarUrl')
    class_obj.portrait_url = class_data.get('portraitAvatarUrl')
    class_obj.spell_casting_attribute_id = class_data.get('spellCastingAbilityId')
    class_obj.hit_dice = class_data.get('hitDice')
    class_obj.base_hit_points = class_data.get('hitDice')
    class_obj.hit_point_attribute_id = 3
    class_obj.primary_attribute_ids = json.dumps(class_data.get('primaryAbilities'))
    class_obj.has_spells = class_data.get('canCastSpells')
    class_obj.knows_all_spells = class_data.get('knowsAllSpells', False)
    class_obj.are_spells_prepared = True if class_data.get('spellPrepareType') else None
    class_obj.spell_container_name = class_data.get('spellContainerName')
    class_obj.is_ritual_spell_caster = class_data.get('isRitualSpellCaster')
    class_obj.subclass_level = class_data.get('subclass_level')
    class_obj.subclass_title = class_data.get('subclass_title')
    class_obj.subclass_description = markdownify(class_data.get('subclass_description'))
    class_obj.proficiency_bonus = json.dumps({
            "1": None,
            "2": None,
            "3": None,
            "4": None,
            "5": None,
            "6": None,
            "7": None,
            "8": None,
            "9": None,
            "10": None,
            "11": None,
            "12": None,
            "13": None,
            "14": None,
            "15": None,
            "16": None,
            "17": None,
            "18": None,
            "19": None,
            "20": None
        })

    class_obj.spell_data = json.dumps(class_data.get('spell_data'))

    class_obj.prerequisites = process_prequisite_data(class_data.get('prerequisites'))

    class_obj.save()

    # class features
    for feature in class_data.get('classFeatures'):
        create_feature(class_obj, feature)

    # equipment
    equipment_set = create_equipment_set(class_data.get('equipment'), class_data.get('equipmentDescription'))
    class_equipment_obj = DndClassEquipmentSet(
        dnd_class = class_obj,
        equipment_set = equipment_set
    )
    class_equipment_obj.save()