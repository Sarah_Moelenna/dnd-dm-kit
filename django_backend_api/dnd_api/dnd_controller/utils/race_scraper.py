import json
from typing import Dict
from dnd_controller.models.all import Race, RacialTrait, Modifier, Action, RacialTraitAction, RacialTraitModifier
from markdownify import markdownify

def create_modifier(modifier: Dict):
    modifier_obj = Modifier(
        fixed_value = modifier['fixedValue'],
        type = modifier['type'],
        sub_type = modifier['subType'],
        dice = json.dumps(modifier['dice']),
        restriction = modifier['restriction'],
        stat_id = modifier['statId'],
        requires_attunement = modifier['requiresAttunement'],
        duration = json.dumps(modifier['duration']),
        friendly_type_name = modifier['friendlyTypeName'],
        friendly_sub_type_name = modifier['friendlySubtypeName'],
        is_granted = modifier['isGranted'],
        bonus_types = modifier['bonusTypes'],
        value = modifier['value'],
        available_to_multiclass = modifier['availableToMulticlass'],
        modifier_type_id = modifier['modifierTypeId'],
        modifier_sub_type_id = modifier['modifierSubTypeId'],
        count = modifier.get('count', None),
        duration_unit = modifier.get('durationUnit', None),
        use_primary_stat = modifier.get('usePrimaryStat', None),
        at_higher_levels = json.dumps(modifier.get('atHigherLevels')) if modifier.get('atHigherLevels', None) else None,
        limits = json.dumps(modifier.get('defaults')) if modifier.get('defaults', None) else None,
    )
    modifier_obj.save()

    return modifier_obj

def create_action(action: Dict):
    action_obj = Action(
        limited_use = json.dumps(action['limitedUse']),
        name = action['name'],
        description = markdownify(action['description']) if action['description'] is not None else None,
        snippet = action['snippet'],
        ability_modifier_stat_id = action['abilityModifierStatId'],
        on_miss_description = action['onMissDescription'],
        save_fail_description = action['saveFailDescription'],
        save_success_description = action['saveSuccessDescription'],
        save_stat_id = action['saveStatId'],
        fixed_save_dc = action['fixedSaveDc'],
        attack_type_range = action['attackTypeRange'],
        action_type = action['actionType'],
        attack_sub_type = action['attackSubtype'],
        dice = json.dumps(action['dice']),
        value = action['value'],
        damage_type_id = action['damageTypeId'],
        is_martial_arts = action['isMartialArts'],
        is_proficient = action['isProficient'],
        spell_range_type = action['spellRangeType'],
        display_as_attack = action['displayAsAttack'],
        range = json.dumps(action['range']),
        activation = json.dumps(action['activation']),
        number_of_targets = action['numberOfTargets'],
        fixed_to_hit = action['fixedToHit'],
        ammunition = action['ammunition'],
    )
    action_obj.save()

    return action_obj

def scrape_race(race, racial_traits, user):
    race_obj = Race(
        is_sub_race = race['isSubRace'],
        base_race_name = race['baseRaceName'],
        full_name = race['fullName'],
        description = markdownify(race['description']) if race['description'] is not None else None,
        avatar_url = race['avatarUrl'],
        large_avatar_url = race['largeAvatarUrl'],
        portrait_avatar_url = race['portraitAvatarUrl'],
        is_homebrew = race['isHomebrew'],
        sub_race_short_name = race['subRaceShortName'],
        base_name = race['baseName'],
        weight_speeds = json.dumps(race['weightSpeeds']),
        size = race['size'],
        user = user,
    )

    race_obj.save()

    
    for racial_trait in racial_traits.values():
        racial_trait_obj = RacialTrait(
            display_order = racial_trait['definition']['displayOrder'],
            name = racial_trait['definition']['name'],
            description = markdownify(racial_trait['definition']['description']) if racial_trait['definition']['description'] is not None else None,
            snippet = racial_trait['definition']['snippet'],
            hide_in_builder = racial_trait['definition']['hideInBuilder'],
            hide_in_sheet = racial_trait['definition']['hideInSheet'],
            race = race_obj
        )

        racial_trait_obj.save()

        if 'modifiers' in racial_trait.keys():
            for modifier in racial_trait['modifiers']:
                modifier_obj = create_modifier(modifier)

                racial_trait_modifier_obj = RacialTraitModifier(
                    racial_trait=racial_trait_obj,
                    modifier=modifier_obj
                )
                racial_trait_modifier_obj.save()

        if 'actions' in racial_trait.keys():
            for action in racial_trait['actions']:
                action_obj = create_action(action)

                racial_trait_action_obj = RacialTraitAction(
                    racial_trait=racial_trait_obj,
                    action=action_obj
                )
                racial_trait_action_obj.save()