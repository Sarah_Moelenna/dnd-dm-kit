import discord
import youtube_dl
import asyncio
import shutil
import os
from django.conf import settings
from dnd_controller.models.all import Upload
from dnd_controller.models import AudioFile

youtube_dl.utils.bug_reports_message = lambda: ''

ytdl_format_options = {
    'format': 'bestaudio/best',
    'restrictfilenames': True,
    'forcefilename': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)

class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)
        self.data = data
        self.title = data.get('title')
        self.url = ""

    @classmethod
    def from_url(cls, url, audio_file: AudioFile):
        try:
            data = ytdl.extract_info(url, download=True)
            if 'entries' in data:
                # take first item from a playlist
                data = data['entries'][0]
            filename = ytdl.prepare_filename(data)
            file_size = os.path.getsize(f'./{filename}') 
            shutil.move(f'./{filename}', f'{settings.MEDIA_ROOT}/music/{filename}')
            os.chmod(f'{settings.MEDIA_ROOT}/music/{filename}' , 0o777)
            audio_file.file_name = filename
            audio_file.status = AudioFile.STATUS_READY
            audio_file.save()

            upload_obj = Upload(user=audio_file.user, file_size=file_size, file_location=f'/music/{filename}', upload_type=Upload.TYPE_AUDIO)
            upload_obj.save()
            audio_file.upload = upload_obj
            audio_file.save()
        except Exception as e:
            print(str(e))
            audio_file.status = AudioFile.STATUS_ERROR
            audio_file.save()
            raise Exception

        