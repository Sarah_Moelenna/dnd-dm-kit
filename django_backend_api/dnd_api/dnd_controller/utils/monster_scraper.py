from dnd_controller.models.all import Monster, User
from django.core.exceptions import MultipleObjectsReturned
from bs4 import BeautifulSoup

from io import StringIO
from html.parser import HTMLParser

from markdownify import markdownify

class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.text = StringIO()
    def handle_data(self, d):
        self.text.write(d)
    def get_data(self):
        return self.text.getvalue()

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def string_challenge_number_to_number(challenge_rating: str):
    if challenge_rating:
        if challenge_rating == "1/8":
            return 0.125
        elif challenge_rating == "1/4":
            return 0.25
        elif challenge_rating == "1/2":
            return 0.5
        else:
            return int(challenge_rating)

def scrape_monster(html: str):

    result = {}

    soup = BeautifulSoup(html, 'html.parser')

    # attributes
    attribute_elements = soup.findAll("div", {"class": "mon-stat-block__attribute"})
    for attribute_element in attribute_elements:
        attribute_name = attribute_element.find("span", {"class": "mon-stat-block__attribute-label"}).contents[0].strip().replace('/n', '').upper()
        attribute_extra_value_element = attribute_element.find("span", {"class": "mon-stat-block__attribute-data-extra"})
        attribute_extra_value = attribute_extra_value_element.contents[0].strip().replace('/n', '') if attribute_extra_value_element else ''

        attribute_value = attribute_element.find("span", {"class": "mon-stat-block__attribute-data-value"}).contents[0].strip().replace('/n', '') + ' ' + attribute_extra_value
        if attribute_name == "ARMOR CLASS":
            result['armour_class'] = attribute_value
        elif attribute_name == "HIT POINTS":
            result['hit_points'] = attribute_value
        elif attribute_name == "SPEED":
            result['speed'] = attribute_value

    # ability
    ability_elements = soup.findAll("div", {"class": "ability-block__stat"})
    for ability_element in ability_elements:
        ability_name = ability_element.find("div", {"class": "ability-block__heading"}).contents[0].strip().replace('/n', '').upper()

        ability_value = ability_element.find("span", {"class": "ability-block__score"}).contents[0].strip().replace('/n', '')
        if ability_name == "STR":
            result['strength'] = int(ability_value)
        elif ability_name == "DEX":
            result['dexterity'] = int(ability_value)
        elif ability_name == "CON":
            result['constitution'] = int(ability_value)
        elif ability_name == "INT":
            result['intelligence'] = int(ability_value)
        elif ability_name == "WIS":
            result['wisdom'] = int(ability_value)
        elif ability_name == "CHA":
            result['charisma'] = int(ability_value)

    # ability
    tidbit_elements = soup.findAll("div", {"class": "mon-stat-block__tidbit"})
    for tidbit_element in tidbit_elements:
        tidbit_name = tidbit_element.find("span", {"class": "mon-stat-block__tidbit-label"}).contents[0].strip().replace('/n', '').upper()

        tidbit_value = tidbit_element.find("span", {"class": "mon-stat-block__tidbit-data"}).contents[0].strip().replace('/n', '')
        if tidbit_value == '':
            tidbit_value = strip_tags(str(tidbit_element.find("span", {"class": "mon-stat-block__tidbit-data"}))).replace('/n', '').replace('\n', '')

        if tidbit_name == "SAVING THROWS":
            result['saving_throws'] = tidbit_value
        elif tidbit_name == "SKILLS":
            result['skills'] = tidbit_value
        elif tidbit_name == "CONDITION IMMUNITIES":
            result['condition_immunities'] = tidbit_value
        elif tidbit_name == "DAMAGE IMMUNITIES":
            result['damage_immunities'] = tidbit_value
        elif tidbit_name == "DAMAGE VULNERABILITIES":
            result['damage_vulnerabilities'] = tidbit_value
        elif tidbit_name == "DAMAGE RESISTANCES":
            result['damage_resistances'] = tidbit_value
        elif tidbit_name == "SENSES":
            result['senses'] = tidbit_value
        elif tidbit_name == "LANGUAGES":
            result['languages'] = tidbit_value
        elif tidbit_name == "CHALLENGE":
            result['challenge'] = tidbit_value
        elif tidbit_name == "PROFICIENCY BONUS":
            result['proficiency_bonus'] = tidbit_value


    # descriptions
    description_elements = soup.findAll("div", {"class": "mon-stat-block__description-block"})
    for description_element in description_elements:
        description_name_element = description_element.find("div", {"class": "mon-stat-block__description-block-heading"})
        description_name = description_name_element.contents[0].strip().replace('/n', '').upper() if description_name_element else "DESCRIPTION"
        
        description_value_element = description_element.find("div", {"class": "mon-stat-block__description-block-content"})
        description_value = ''
        if description_value_element:
            description_value = str(description_value_element)
            link_elements = description_value_element.findAll("a")
            for link_element in link_elements:
                description_value = description_value.replace(str(link_element), str(link_element.contents[0]))
        description_value = markdownify(str(description_value)).replace('/n', '<br>').replace('\n', '<br>')

        if description_name == "DESCRIPTION":
            result['description'] = description_value
        elif description_name == "ACTIONS":
            result['actions'] = description_value
        elif description_name == "LEGENDARY ACTIONS":
            result['legendary_actions'] = description_value
        elif description_name == "REACTIONS":
            result['reactions'] = description_value
        elif description_name == "MYTHIC ACTIONS":
            result['mythic'] = description_value
        elif description_name == "BONUS ACTIONS":
            result['bonus'] = description_value

    # environments
    environment_elements = soup.findAll("span", {"class": "environment-tag"})
    for environment_element in environment_elements:
        if 'environment' not in result:
            result['environment'] = []
        result['environment'].append(environment_element.contents[0].strip().replace('/n', '').replace('\n', '<br>'))


    name_element = soup.find("a", {"class": "mon-stat-block__name-link"})
    type_element = soup.find("div", {"class": "mon-stat-block__meta"})
    more_info_element = soup.find("div", {"class": "more-info-content"})
    image_element = soup.find("img", {"class": "monster-image"})

    result['name'] = name_element.contents[0].strip().replace('/n', '') if name_element else None
    result['types'] = type_element.contents[0].strip() if type_element else None
    result['more_info'] = markdownify(str(more_info_element)).replace('/n', '<br>').replace('\n', '<br>') if more_info_element else None
    result['image'] = image_element['src'] if image_element else None

    try:
        user = User.objects.get(pk="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc")
        monster = Monster.objects.get(name=result.get('name', None), user_made_content=False, user=user)
    except MultipleObjectsReturned as e:
        return
    except Exception as e:
        monster = None
    
    if monster:
            monster.image = result.get('image', None)
            monster.actions = result.get('actions', None)
            monster.legendary_actions = result.get('legendary_actions', None)
            monster.reactions = result.get('reactions', None)
            monster.more_info = result.get('more_info', None)
            monster.description = result.get('description', None)
            monster.mythic = result.get('mythic', None)
            monster.bonus = result.get('bonus', None)
            monster.armour_class = result.get('armour_class', None)
            monster.hit_points = result.get('hit_points', None)
            monster.speed = result.get('speed', None)
            monster.saving_throws = result.get('saving_throws', None)
            monster.skills = result.get('skills', None)
            monster.condition_immunities = result.get('condition_immunities', None)
            monster.damage_immunities = result.get('damage_immunities', None)
            monster.damage_vulnerabilities = result.get('damage_vulnerabilities', None)
            monster.damage_resistances = result.get('damage_resistances', None)
            monster.senses = result.get('senses', None)
            monster.languages = result.get('languages', None)
            monster.types = result.get('types', None)
            monster.environments = ", ".join(result['environment']) if result.get('environment', None) else None
            monster.strength = result.get('strength', None)
            monster.dexterity = result.get('dexterity', None)
            monster.constitution = result.get('constitution', None)
            monster.intelligence = result.get('intelligence', None)
            monster.wisdom = result.get('wisdom', None)
            monster.charisma = result.get('charisma', None)
            monster.challenge = result.get('challenge', None)
            monster.challenge_number = string_challenge_number_to_number(result.get('challenge', '').split(" ")[0])
            monster.proficiency_bonus = result.get('proficiency_bonus', None)
    else:
        monster = Monster(
            name = result.get('name', None),
            image = result.get('image', None),
            actions = result.get('actions', None),
            legendary_actions = result.get('legendary_actions', None),
            reactions = result.get('reactions', None),
            more_info = result.get('more_info', None),
            description = result.get('description', None),
            mythic = result.get('mythic', None),
            bonus = result.get('bonus', None),
            armour_class = result.get('armour_class', None),
            hit_points = result.get('hit_points', None),
            speed = result.get('speed', None),
            saving_throws = result.get('saving_throws', None),
            skills = result.get('skills', None),
            condition_immunities = result.get('condition_immunities', None),
            damage_immunities = result.get('damage_immunities', None),
            damage_vulnerabilities = result.get('damage_vulnerabilities', None),
            damage_resistances = result.get('damage_resistances', None),
            senses = result.get('senses', None),
            languages = result.get('languages', None),
            types = result.get('types', None),
            environments = ", ".join(result['environment']) if result.get('environment', None) else None,
            strength = result.get('strength', None),
            dexterity = result.get('dexterity', None),
            constitution = result.get('constitution', None),
            intelligence = result.get('intelligence', None),
            wisdom = result.get('wisdom', None),
            charisma = result.get('charisma', None),
            challenge = result.get('challenge', None),
            challenge_number = string_challenge_number_to_number(result.get('challenge', '').split(" ")[0]),
            proficiency_bonus = result.get('proficiency_bonus', None),
            user = User.objects.get(pk="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc"),
            user_made_content=False
        )

    try:
        monster.save()
    except Exception as e:
        print(str(e))