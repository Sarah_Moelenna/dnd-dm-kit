from redis import StrictRedis
from django.conf import settings
from typing import Dict, List
import json

def post_data(key: str, data: Dict):
    redis = StrictRedis.from_url(settings.REDIS_URL)
    redis.set(key, json.dumps(data))

def append_data(key: str, data: Dict):
    redis = StrictRedis.from_url(settings.REDIS_URL)
    cache = redis.get(key)
    new_data = []
    if cache:
        current_data = json.loads(cache)
        current_data.append(data)
        new_data = current_data
    else:
        new_data = [data]
    redis.set(key, json.dumps(new_data))

def get_data(key: str):
    redis = StrictRedis.from_url(settings.REDIS_URL)
    cache = redis.get(key)
    if cache:
        return json.loads(cache)

def delete_data(key: str):
    redis = StrictRedis.from_url(settings.REDIS_URL)
    cache = redis.delete(key)