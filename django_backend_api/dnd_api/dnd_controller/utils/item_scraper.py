from dnd_controller.models.all import Item, User, ItemModifier
from dnd_controller.utils.race_scraper import create_modifier
from typing import Dict, List
import json
from dnd_controller.utils.type_data import get_type_for_name, get_subtype_for_name_and_type_id
from markdownify import markdownify
import re

def intepret_base_type(base_type):
    if base_type == 1782728300:
        return "Weapon"
    if base_type == 701257905:
        return "Armour"
    return "Item"

def identify_charge_data(description):
    has_charges = False
    charge_reset = None
    charge_number = 0

    if "charges" in description.lower():
        has_charges = True

    if has_charges is True:
        matches = re.findall("(\d+) charge", description.lower())
        if matches:
            charge_number = matches[0]

        if "long rest" in description.lower():
            charge_reset = "Long Rest"
        elif "short rest" in description.lower():
            charge_reset = "Short Rest"
        elif "dawn" in description.lower():
            charge_reset = "Dawn"
        else:
            charge_reset = "Other"

    return has_charges, charge_reset, charge_number

def identify_dex_modifier(description):
    dex_modifier = None

    if "max 2" in description.lower():
        dex_modifier = "Max 2"

    return dex_modifier

def scrape_items(items: List[Dict]):
    for item in items:
        user = User.objects.get(pk="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc")
        try:
            item_obj = Item.objects.get(name=item.get('name'), user=user)
        except Exception as e:
            item_obj = Item(source_id=item.get('id'), user=user)

        description = markdownify(item.get('description', ''))
        has_charges, charge_reset, charge_number = identify_charge_data(description)
        dex_modifier = identify_dex_modifier(description)

        item_obj.base_type = intepret_base_type(item.get('baseTypeId'))
        item_obj.can_equip = item.get('canEquip')
        item_obj.magic = item.get('magic')
        item_obj.name = item.get('name')
        item_obj.snippet = item.get('snippet')
        item_obj.weight = item.get('weight')
        item_obj.type = item.get('type')
        item_obj.description = description
        item_obj.can_attune = item.get('canAttune')
        item_obj.attunement_description = item.get('attunementDescription')
        item_obj.rarity = item.get('rarity')
        item_obj.stackable = item.get('stackable')
        item_obj.bundle_size = item.get('bundleSize')
        item_obj.avatar_url = item.get('avatarUrl')
        item_obj.large_avatar_url = item.get('largeAvatarUrl')
        item_obj.filter_type = item.get('filterType')
        item_obj.cost = item.get('cost')
        item_obj.tags = ",".join(item.get('tags'))
        item_obj.sub_type = item.get('subType')
        item_obj.is_consumable = item.get('isConsumable')
        item_obj.base_item_id = item.get('baseItemId')
        item_obj.base_armor_name = item.get('baseArmorName')
        item_obj.strength_requirement = item.get('strengthRequirement')
        item_obj.armor_class = item.get('armorClass')
        item_obj.stealth_check = item.get('stealthCheck')
        item_obj.damage = json.dumps(item.get('damage')) if item.get('damage') else None #json blob for now
        item_obj.damage_type = item.get('damageType')
        item_obj.fixed_damage = item.get('fixedDamage')
        item_obj.properties = json.dumps(item.get('properties')) if item.get('properties') else None #json blob for now
        item_obj.attack_type = item.get('attackType')
        item_obj.category_id = item.get('categoryId')
        item_obj.range = item.get('range')
        item_obj.long_range = item.get('longRange')
        item_obj.is_monk_weapon = item.get('isMonkWeapon')
        item_obj.level_infusion_granted = item.get('levelInfusionGranted')
        item_obj.armor_type_id = item.get('armorTypeId')
        item_obj.gear_type_id = item.get('gearTypeId')
        item_obj.grouped_id = item.get('groupedId')
        item_obj.can_be_added_to_inventory = item.get('canBeAddedToInventory')
        item_obj.user_made_content = item.get('isHomebrew')
        item_obj.has_charges = has_charges
        item_obj.charge_number = charge_number
        item_obj.charge_reset = charge_reset
        item_obj.dex_bonus = dex_modifier

        
        weapon_behaviors = item.get('weaponBehaviors')[0] if item.get('weaponBehaviors') else {}
        if 'damage' in weapon_behaviors.keys():
            item_obj.damage = json.dumps(weapon_behaviors['damage'])
        if 'properties' in weapon_behaviors.keys():
            item_obj.properties = json.dumps(weapon_behaviors['properties'])
        if 'baseItemId' in weapon_behaviors.keys():
            item_obj.base_item_id = weapon_behaviors['baseItemId']
        if 'damageType' in weapon_behaviors.keys():
            item_obj.damage_type = weapon_behaviors['damageType']
        if 'range' in weapon_behaviors.keys():
            item_obj.range = weapon_behaviors['range']
        if 'longRange' in weapon_behaviors.keys():
            item_obj.long_range = weapon_behaviors['longRange']
        if 'isMonkWeapon' in weapon_behaviors.keys():
            item_obj.is_monk_weapon = weapon_behaviors['isMonkWeapon']
        if 'attackType' in weapon_behaviors.keys():
            item_obj .attack_type = weapon_behaviors['attackType']


        item_obj.save()

        modifiers = item.get('grantedModifiers')
        for modifier_in in modifiers:

            if modifier_in['modifierTypeId'] == 0:
                type_obj = get_type_for_name(modifier_in['type'])
                if type_obj is not None:
                    modifier_in['modifierTypeId'] = type_obj['id']

                    subtype_obj = get_subtype_for_name_and_type_id(modifier_in['modifierTypeId'], modifier_in['subType'])
                    if subtype_obj is not None:
                        modifier_in['modifierSubTypeId'] = subtype_obj['id']

            modifier_obj = create_modifier(modifier_in)

            item_modifier_obj = ItemModifier(
                    item=item_obj,
                    modifier=modifier_obj
                )
            item_modifier_obj.save()

    # fix base_item_ids
    items_to_fix = Item.objects.exclude(base_item_id__isnull=True).all()

    for item in items_to_fix:
        if item.base_item_id == item.source_id:
            item.base_item_id = None
        else:
            parent = Item.objects.filter(source_id=item.base_item_id, base_type=item.base_type).first()
            if not parent:
                raise ValueError(f'No parent found for item: {item.name} base_item_id: {item.base_item_id} type: {item.base_type}')
            item.parent_item = parent
            item.base_item_id = None

        item.save()