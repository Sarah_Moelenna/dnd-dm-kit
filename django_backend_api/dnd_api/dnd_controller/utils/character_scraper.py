from dnd_controller.utils import redis
from typing import List
import uuid
from time import sleep

def scrape_character_data(character_sheet_urls: List[str]):
    character_data = [{"url": url} for url in character_sheet_urls]
    request_id = str(uuid.uuid4())
    redis.delete_data("CHARACTER_TRACKNG_SETTINGS-" + request_id)
    redis.delete_data("CHARACTER_TRACKNG_DATA-" + request_id)
    redis.post_data("CHARACTER_TRACKNG_SETTINGS-" + request_id, character_data)

    while True:
        cache = redis.get_data("CHARACTER_TRACKNG_DATA-" + request_id)
        if not cache:
            sleep(2)
        else:
            return cache