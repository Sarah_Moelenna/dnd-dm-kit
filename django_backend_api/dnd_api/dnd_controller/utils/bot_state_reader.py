from dnd_controller.utils import redis
from dnd_controller.models import AudioFile

def get_bot_state(campaign_id: str):
    finalised_state = {
            "guilds": [],
            "audio": {
                "is_playing": False,
                "song": None,
            },
            "discord_channels": {
                "server": None,
                "voice": None,
                "info": None,
                "rolling": None,
            },
            "messages": [],
        }

    if campaign_id is not None:
        campaign_state = redis.get_data(f"{campaign_id}:BOT_STATE")
        if campaign_state is not None:
            finalised_state["audio"] = {
                "is_playing": campaign_state['should_music_play'],
                "song": AudioFile.get_by_filename(campaign_state['song_to_play'].replace('media/music/', '')).to_dict() if campaign_state['song_to_play'] else None,
            }

            finalised_state["discord_channels"] = {
                "server": campaign_state.get('active_server_id'),
                "voice": campaign_state.get('active_voice_channel'),
                "info": campaign_state.get('active_voice_channel'),
                "rolling": campaign_state.get('active_voice_channel'),
            }

            finalised_state["messages"] = campaign_state.get('rolling_messages')

    bot_state = redis.get_data(f"BOT_STATE")
    if bot_state is not None:
        finalised_state['guilds'] = bot_state['guilds']
    

    return finalised_state
