from typing import Dict
import json

types = """
[
  {
    "display_name": "Bonus",
    "name": "bonus",
    "id": 1
  },
  {
    "display_name": "Damage",
    "name": "damage",
    "id": 2
  },
  {
    "display_name": "Advantage",
    "name": "advantage",
    "id": 3
  },
  {
    "display_name": "Disadvantage",
    "name": "disadvantage",
    "id": 4
  },
  {
    "display_name": "Resistance",
    "name": "resistance",
    "id": 5
  },
  {
    "display_name": "Immunity",
    "name": "immunity",
    "id": 6
  },
  {
    "display_name": "Vulnerability",
    "name": "vulnerability",
    "id": 7
  },
  {
    "display_name": "Sense",
    "name": "sense",
    "id": 8
  },
  {
    "display_name": "Set",
    "name": "set",
    "id": 9
  },
  {
    "display_name": "Proficiency",
    "name": "proficiency",
    "id": 10
  },
  {
    "display_name": "Language",
    "name": "language",
    "id": 11
  },
  {
    "display_name": "Expertise",
    "name": "expertise",
    "id": 12
  },
  {
    "display_name": "Half Proficiency",
    "name": "half-proficiency",
    "id": 13
  },
  {
    "display_name": "Feat",
    "name": "feat",
    "id": 14
  },
  {
    "display_name": "Carrying Capacity",
    "name": "carrying-capacity",
    "id": 17
  },
  {
    "display_name": "Natural Weapon",
    "name": "natural-weapon",
    "id": 19
  },
  {
    "display_name": "Stealth Disadvantage",
    "name": "stealth-disadvantage",
    "id": 23
  },
  {
    "display_name": "Speed Reduction",
    "name": "speed-reduction",
    "id": 24
  },
  {
    "display_name": "Melee Weapon Attack",
    "name": "melee-weapon-attack",
    "id": 25
  },
  {
    "display_name": "Ranged Weapon Attack",
    "name": "ranged-weapon-attack",
    "id": 26
  },
  {
    "display_name": "Weapon Property",
    "name": "weapon-property",
    "id": 28
  },
  {
    "display_name": "Half Proficiency Round Up",
    "name": "half-proficiency-round-up",
    "id": 29
  },
  {
    "display_name": "Favored Enemy",
    "name": "favored-enemy",
    "id": 30
  },
  {
    "display_name": "Ignore",
    "name": "ignore",
    "id": 31
  },
  {
    "display_name": "Eldritch Blast",
    "name": "eldritch-blast",
    "id": 32
  },
  {
    "display_name": "Replace Damage Type",
    "name": "replace-damage-type",
    "id": 33
  },
  {
    "display_name": "Twice Proficiency",
    "name": "twice-proficiency",
    "id": 34
  },
  {
    "display_name": "Protection",
    "name": "protection",
    "id": 37
  },
  {
    "display_name": "Stacking Bonus",
    "name": "stacking-bonus",
    "id": 38
  },
  {
    "display_name": "Set Base",
    "name": "set-base",
    "id": 39
  },
  {
    "display_name": "Ignore Weapon Property",
    "name": "ignore-weapon-property",
    "id": 40
  },
  {
    "display_name": "Size",
    "name": "size",
    "id": 41
  }
]
"""

def get_type_for_name(name: str) -> Dict:
    loaded_types = json.loads(types)
    for type_obj in loaded_types:
        if type_obj['name'] == name:
            return type_obj
    return None

subtypes = """
{
  "1": [
    {
      "id": 15,
      "display_name": "Ability Checks",
      "name": "ability-checks"
    },
    {
      "id": 848,
      "display_name": "Ability Score Maximum",
      "name": "ability-score-maximum"
    },
    {
      "id": 770,
      "display_name": "AC Max Dex Modifier",
      "name": "ac-max-dex-modifier"
    },
    {
      "id": 23,
      "display_name": "Acrobatics",
      "name": "acrobatics"
    },
    {
      "id": 31,
      "display_name": "Animal Handling",
      "name": "animal-handling"
    },
    {
      "id": 26,
      "display_name": "Arcana",
      "name": "arcana"
    },
    {
      "id": 1,
      "display_name": "Armor Class",
      "name": "armor-class"
    },
    {
      "id": 806,
      "display_name": "Armored Armor Class",
      "name": "armored-armor-class"
    },
    {
      "id": 22,
      "display_name": "Athletics",
      "name": "athletics"
    },
    {
      "id": 21,
      "display_name": "Charisma Ability Checks",
      "name": "charisma-ability-checks"
    },
    {
      "id": 14,
      "display_name": "Charisma Saving Throws",
      "name": "charisma-saving-throws"
    },
    {
      "id": 7,
      "display_name": "Charisma Score",
      "name": "charisma-score"
    },
    {
      "id": 695,
      "display_name": "Choose an Ability Score",
      "name": "choose-an-ability-score"
    },
    {
      "id": 1574,
      "display_name": "Choose Constitution or Intelligence",
      "name": "choose-constitution-or-intelligence"
    },
    {
      "id": 1575,
      "display_name": "Choose Constitution or Wisdom",
      "name": "choose-constitution-or-wisdom"
    },
    {
      "id": 1718,
      "display_name": "Choose Dexterity or Charisma",
      "name": "choose-dexterity-or-charisma"
    },
    {
      "id": 1573,
      "display_name": "Choose Dexterity or Constitution",
      "name": "choose-dexterity-or-constitution"
    },
    {
      "id": 1065,
      "display_name": "Choose Dexterity or Intelligence",
      "name": "choose-dexterity-or-intelligence"
    },
    {
      "id": 1678,
      "display_name": "Choose Dexterity or Wisdom",
      "name": "choose-dexterity-or-wisdom"
    },
    {
      "id": 1772,
      "display_name": "Choose Dexterity, Constitution, Intelligence, Wisdom, or Charisma",
      "name": "choose-dexterity-constitution-intelligence-wisdom-or-charisma"
    },
    {
      "id": 1066,
      "display_name": "Choose Dexterity, Constitution, or Charisma",
      "name": "choose-dexterity-constitution-or-charisma"
    },
    {
      "id": 1063,
      "display_name": "Choose Dexterity, Intelligence, Wisdom, or Charisma",
      "name": "choose-dexterity-intelligence-wisdom-or-charisma"
    },
    {
      "id": 1064,
      "display_name": "Choose Intelligence or Charisma",
      "name": "choose-intelligence-or-charisma"
    },
    {
      "id": 773,
      "display_name": "Choose Intelligence or Wisdom",
      "name": "choose-intelligence-or-wisdom"
    },
    {
      "id": 777,
      "display_name": "Choose Strength or Constitution",
      "name": "choose-strength-or-constitution"
    },
    {
      "id": 764,
      "display_name": "Choose Strength or Dexterity",
      "name": "choose-strength-or-dexterity"
    },
    {
      "id": 1679,
      "display_name": "Choose Strength or Wisdom",
      "name": "choose-strength-or-wisdom"
    },
    {
      "id": 1773,
      "display_name": "Choose Strength, Constitution, Intelligence, Wisdom, or Charisma",
      "name": "choose-strength-constitution-intelligence-wisdom-or-charisma"
    },
    {
      "id": 1684,
      "display_name": "Choose Strength, Constitution, or Charisma",
      "name": "choose-strength-constitution-or-charisma"
    },
    {
      "id": 1061,
      "display_name": "Choose Strength, Constitution, or Dexterity",
      "name": "choose-strength-constitution-or-dexterity"
    },
    {
      "id": 1770,
      "display_name": "Choose Strength, Dexterity, Constitution, Intelligence, or Charisma",
      "name": "choose-strength-dexterity-constitution-intelligence-or-charisma"
    },
    {
      "id": 1775,
      "display_name": "Choose Strength, Dexterity, Constitution, Intelligence, or Wisdom",
      "name": "choose-strength-dexterity-constitution-intelligence-or-wisdom"
    },
    {
      "id": 1771,
      "display_name": "Choose Strength, Dexterity, Constitution, Wisdom, or Charisma",
      "name": "choose-strength-dexterity-constitution-wisdom-or-charisma"
    },
    {
      "id": 1774,
      "display_name": "Choose Strength, Dexterity, Intelligence, Wisdom, or Charisma",
      "name": "choose-strength-dexterity-intelligence-wisdom-or-charisma"
    },
    {
      "id": 1717,
      "display_name": "Choose Strength, Dexterity, or Wisdom ",
      "name": "choose-strength-dexterity-or-wisdom"
    },
    {
      "id": 393,
      "display_name": "Cleric Cantrip Damage",
      "name": "cleric-cantrip-damage"
    },
    {
      "id": 18,
      "display_name": "Constitution Ability Checks",
      "name": "constitution-ability-checks"
    },
    {
      "id": 11,
      "display_name": "Constitution Saving Throws",
      "name": "constitution-saving-throws"
    },
    {
      "id": 4,
      "display_name": "Constitution Score",
      "name": "constitution-score"
    },
    {
      "id": 36,
      "display_name": "Deception",
      "name": "deception"
    },
    {
      "id": 17,
      "display_name": "Dexterity Ability Checks",
      "name": "dexterity-ability-checks"
    },
    {
      "id": 189,
      "display_name": "Dexterity Attacks",
      "name": "dexterity-attacks"
    },
    {
      "id": 10,
      "display_name": "Dexterity Saving Throws",
      "name": "dexterity-saving-throws"
    },
    {
      "id": 3,
      "display_name": "Dexterity Score",
      "name": "dexterity-score"
    },
    {
      "id": 844,
      "display_name": "Domain Spell",
      "name": "domain-spell"
    },
    {
      "id": 1039,
      "display_name": "Dual Wield Armor Class",
      "name": "dual-wield-armor-class"
    },
    {
      "id": 698,
      "display_name": "Eldritch Blast Damage",
      "name": "eldritch-blast-damage"
    },
    {
      "id": 355,
      "display_name": "Extra Attacks",
      "name": "extra-attacks"
    },
    {
      "id": 411,
      "display_name": "Half Proficiency",
      "name": "half-proficiency"
    },
    {
      "id": 27,
      "display_name": "History",
      "name": "history"
    },
    {
      "id": 192,
      "display_name": "Hit Points",
      "name": "hit-points"
    },
    {
      "id": 752,
      "display_name": "Hit Points per Level",
      "name": "hit-points-per-level"
    },
    {
      "id": 218,
      "display_name": "Initiative",
      "name": "initiative"
    },
    {
      "id": 32,
      "display_name": "Insight",
      "name": "insight"
    },
    {
      "id": 19,
      "display_name": "Intelligence Ability Checks",
      "name": "intelligence-ability-checks"
    },
    {
      "id": 12,
      "display_name": "Intelligence Saving Throws",
      "name": "intelligence-saving-throws"
    },
    {
      "id": 5,
      "display_name": "Intelligence Score",
      "name": "intelligence-score"
    },
    {
      "id": 37,
      "display_name": "Intimidation",
      "name": "intimidation"
    },
    {
      "id": 28,
      "display_name": "Investigation",
      "name": "investigation"
    },
    {
      "id": 312,
      "display_name": "Magic",
      "name": "magic"
    },
    {
      "id": 1768,
      "display_name": "Magic Item Attack With Charisma",
      "name": "magic-item-attack-with-charisma"
    },
    {
      "id": 1765,
      "display_name": "Magic Item Attack With Constitution",
      "name": "magic-item-attack-with-constitution"
    },
    {
      "id": 1764,
      "display_name": "Magic Item Attack With Dexterity",
      "name": "magic-item-attack-with-dexterity"
    },
    {
      "id": 1766,
      "display_name": "Magic Item Attack With Intelligence",
      "name": "magic-item-attack-with-intelligence"
    },
    {
      "id": 1763,
      "display_name": "Magic Item Attack With Strength",
      "name": "magic-item-attack-with-strength"
    },
    {
      "id": 1767,
      "display_name": "Magic Item Attack With Wisdom",
      "name": "magic-item-attack-with-wisdom"
    },
    {
      "id": 33,
      "display_name": "Medicine",
      "name": "medicine"
    },
    {
      "id": 802,
      "display_name": "Melee Attacks",
      "name": "melee-attacks"
    },
    {
      "id": 766,
      "display_name": "Melee Reach",
      "name": "melee-reach"
    },
    {
      "id": 804,
      "display_name": "Melee Spell Attacks",
      "name": "melee-spell-attacks"
    },
    {
      "id": 45,
      "display_name": "Melee Weapon Attacks",
      "name": "melee-weapon-attacks"
    },
    {
      "id": 1047,
      "display_name": "Natural Attacks",
      "name": "natural-attacks"
    },
    {
      "id": 29,
      "display_name": "Nature",
      "name": "nature"
    },
    {
      "id": 775,
      "display_name": "Passive Investigation",
      "name": "passive-investigation"
    },
    {
      "id": 774,
      "display_name": "Passive Perception",
      "name": "passive-perception"
    },
    {
      "id": 34,
      "display_name": "Perception",
      "name": "perception"
    },
    {
      "id": 38,
      "display_name": "Performance",
      "name": "performance"
    },
    {
      "id": 39,
      "display_name": "Persuasion",
      "name": "persuasion"
    },
    {
      "id": 313,
      "display_name": "Proficiency",
      "name": "proficiency"
    },
    {
      "id": 814,
      "display_name": "Proficiency Bonus",
      "name": "proficiency-bonus"
    },
    {
      "id": 803,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 805,
      "display_name": "Ranged Spell Attacks",
      "name": "ranged-spell-attacks"
    },
    {
      "id": 46,
      "display_name": "Ranged Weapon Attacks",
      "name": "ranged-weapon-attacks"
    },
    {
      "id": 30,
      "display_name": "Religion",
      "name": "religion"
    },
    {
      "id": 8,
      "display_name": "Saving Throws",
      "name": "saving-throws"
    },
    {
      "id": 1744,
      "display_name": "Shield AC on Dex Saves",
      "name": "shield-ac-on-dex-saves"
    },
    {
      "id": 24,
      "display_name": "Sleight of Hand",
      "name": "sleight-of-hand"
    },
    {
      "id": 40,
      "display_name": "Speed",
      "name": "speed"
    },
    {
      "id": 44,
      "display_name": "Speed (Burrowing)",
      "name": "speed-burrowing"
    },
    {
      "id": 42,
      "display_name": "Speed (Climbing)",
      "name": "speed-climbing"
    },
    {
      "id": 41,
      "display_name": "Speed (Flying)",
      "name": "speed-flying"
    },
    {
      "id": 43,
      "display_name": "Speed (Swimming)",
      "name": "speed-swimming"
    },
    {
      "id": 1697,
      "display_name": "Speed (Walking)",
      "name": "speed-walking"
    },
    {
      "id": 700,
      "display_name": "Spell Attack Range Multiplier",
      "name": "spell-attack-range-multiplier"
    },
    {
      "id": 47,
      "display_name": "Spell Attacks",
      "name": "spell-attacks"
    },
    {
      "id": 1829,
      "display_name": "Spell Group - Healing",
      "name": "spell-group--healing"
    },
    {
      "id": 316,
      "display_name": "Spell Save DC",
      "name": "spell-save-dc"
    },
    {
      "id": 25,
      "display_name": "Stealth",
      "name": "stealth"
    },
    {
      "id": 16,
      "display_name": "Strength Ability Checks",
      "name": "strength-ability-checks"
    },
    {
      "id": 186,
      "display_name": "Strength Attacks",
      "name": "strength-attacks"
    },
    {
      "id": 9,
      "display_name": "Strength Saving Throws",
      "name": "strength-saving-throws"
    },
    {
      "id": 2,
      "display_name": "Strength Score",
      "name": "strength-score"
    },
    {
      "id": 35,
      "display_name": "Survival",
      "name": "survival"
    },
    {
      "id": 193,
      "display_name": "Temporary Hit Points",
      "name": "temporary-hit-points"
    },
    {
      "id": 319,
      "display_name": "Twice Proficiency Bonus",
      "name": "twice-proficiency-bonus"
    },
    {
      "id": 1048,
      "display_name": "Unarmed Attacks",
      "name": "unarmed-attacks"
    },
    {
      "id": 801,
      "display_name": "Unarmored Armor Class",
      "name": "unarmored-armor-class"
    },
    {
      "id": 1685,
      "display_name": "Unarmored Movement",
      "name": "unarmored-movement"
    },
    {
      "id": 765,
      "display_name": "Weapon Attack Range Multiplier",
      "name": "weapon-attack-range-multiplier"
    },
    {
      "id": 20,
      "display_name": "Wisdom Ability Checks",
      "name": "wisdom-ability-checks"
    },
    {
      "id": 13,
      "display_name": "Wisdom Saving Throws",
      "name": "wisdom-saving-throws"
    },
    {
      "id": 6,
      "display_name": "Wisdom Score",
      "name": "wisdom-score"
    }
  ],
  "2": [
    {
      "id": 48,
      "display_name": "Acid",
      "name": "acid"
    },
    {
      "id": 49,
      "display_name": "Bludgeoning",
      "name": "bludgeoning"
    },
    {
      "id": 50,
      "display_name": "Cold",
      "name": "cold"
    },
    {
      "id": 51,
      "display_name": "Fire",
      "name": "fire"
    },
    {
      "id": 52,
      "display_name": "Force",
      "name": "force"
    },
    {
      "id": 53,
      "display_name": "Lightning",
      "name": "lightning"
    },
    {
      "id": 809,
      "display_name": "Longbow",
      "name": "longbow"
    },
    {
      "id": 1687,
      "display_name": "Melee Weapon Attacks",
      "name": "melee-weapon-attacks"
    },
    {
      "id": 1050,
      "display_name": "Natural Attacks",
      "name": "natural-attacks"
    },
    {
      "id": 54,
      "display_name": "Necrotic",
      "name": "necrotic"
    },
    {
      "id": 810,
      "display_name": "One-Handed Melee Attacks",
      "name": "onehanded-melee-attacks"
    },
    {
      "id": 55,
      "display_name": "Piercing",
      "name": "piercing"
    },
    {
      "id": 56,
      "display_name": "Poison",
      "name": "poison"
    },
    {
      "id": 57,
      "display_name": "Psychic",
      "name": "psychic"
    },
    {
      "id": 58,
      "display_name": "Radiant",
      "name": "radiant"
    },
    {
      "id": 808,
      "display_name": "Shortbow",
      "name": "shortbow"
    },
    {
      "id": 59,
      "display_name": "Slashing",
      "name": "slashing"
    },
    {
      "id": 60,
      "display_name": "Thunder",
      "name": "thunder"
    },
    {
      "id": 1049,
      "display_name": "Unarmed Attacks",
      "name": "unarmed-attacks"
    }
  ],
  "3": [
    {
      "id": 68,
      "display_name": "Ability Checks",
      "name": "ability-checks"
    },
    {
      "id": 76,
      "display_name": "Acrobatics",
      "name": "acrobatics"
    },
    {
      "id": 84,
      "display_name": "Animal Handling",
      "name": "animal-handling"
    },
    {
      "id": 79,
      "display_name": "Arcana",
      "name": "arcana"
    },
    {
      "id": 75,
      "display_name": "Athletics",
      "name": "athletics"
    },
    {
      "id": 74,
      "display_name": "Charisma Ability Checks",
      "name": "charisma-ability-checks"
    },
    {
      "id": 67,
      "display_name": "Charisma Saving Throws",
      "name": "charisma-saving-throws"
    },
    {
      "id": 71,
      "display_name": "Constitution Ability Checks",
      "name": "constitution-ability-checks"
    },
    {
      "id": 64,
      "display_name": "Constitution Saving Throws",
      "name": "constitution-saving-throws"
    },
    {
      "id": 1690,
      "display_name": "Death Saving Throws",
      "name": "death-saving-throws"
    },
    {
      "id": 89,
      "display_name": "Deception",
      "name": "deception"
    },
    {
      "id": 70,
      "display_name": "Dexterity Ability Checks",
      "name": "dexterity-ability-checks"
    },
    {
      "id": 190,
      "display_name": "Dexterity Attacks",
      "name": "dexterity-attacks"
    },
    {
      "id": 63,
      "display_name": "Dexterity Saving Throws",
      "name": "dexterity-saving-throws"
    },
    {
      "id": 80,
      "display_name": "History",
      "name": "history"
    },
    {
      "id": 217,
      "display_name": "Initiative",
      "name": "initiative"
    },
    {
      "id": 85,
      "display_name": "Insight",
      "name": "insight"
    },
    {
      "id": 72,
      "display_name": "Intelligence Ability Checks",
      "name": "intelligence-ability-checks"
    },
    {
      "id": 65,
      "display_name": "Intelligence Saving Throws",
      "name": "intelligence-saving-throws"
    },
    {
      "id": 90,
      "display_name": "Intimidation",
      "name": "intimidation"
    },
    {
      "id": 81,
      "display_name": "Investigation",
      "name": "investigation"
    },
    {
      "id": 86,
      "display_name": "Medicine",
      "name": "medicine"
    },
    {
      "id": 93,
      "display_name": "Melee Attacks",
      "name": "melee-attacks"
    },
    {
      "id": 82,
      "display_name": "Nature",
      "name": "nature"
    },
    {
      "id": 87,
      "display_name": "Perception",
      "name": "perception"
    },
    {
      "id": 91,
      "display_name": "Performance",
      "name": "performance"
    },
    {
      "id": 92,
      "display_name": "Persuasion",
      "name": "persuasion"
    },
    {
      "id": 94,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 83,
      "display_name": "Religion",
      "name": "religion"
    },
    {
      "id": 61,
      "display_name": "Saving Throws",
      "name": "saving-throws"
    },
    {
      "id": 77,
      "display_name": "Sleight of Hand",
      "name": "sleight-of-hand"
    },
    {
      "id": 95,
      "display_name": "Spell Attacks",
      "name": "spell-attacks"
    },
    {
      "id": 78,
      "display_name": "Stealth",
      "name": "stealth"
    },
    {
      "id": 69,
      "display_name": "Strength Ability Checks",
      "name": "strength-ability-checks"
    },
    {
      "id": 187,
      "display_name": "Strength Attacks",
      "name": "strength-attacks"
    },
    {
      "id": 62,
      "display_name": "Strength Saving Throws",
      "name": "strength-saving-throws"
    },
    {
      "id": 88,
      "display_name": "Survival",
      "name": "survival"
    },
    {
      "id": 73,
      "display_name": "Wisdom Ability Checks",
      "name": "wisdom-ability-checks"
    },
    {
      "id": 66,
      "display_name": "Wisdom Saving Throws",
      "name": "wisdom-saving-throws"
    }
  ],
  "4": [
    {
      "id": 103,
      "display_name": "Ability Checks",
      "name": "ability-checks"
    },
    {
      "id": 111,
      "display_name": "Acrobatics",
      "name": "acrobatics"
    },
    {
      "id": 119,
      "display_name": "Animal Handling",
      "name": "animal-handling"
    },
    {
      "id": 114,
      "display_name": "Arcana",
      "name": "arcana"
    },
    {
      "id": 110,
      "display_name": "Athletics",
      "name": "athletics"
    },
    {
      "id": 858,
      "display_name": "Attack Rolls Against You",
      "name": "attack-rolls-against-you"
    },
    {
      "id": 109,
      "display_name": "Charisma Ability Checks",
      "name": "charisma-ability-checks"
    },
    {
      "id": 102,
      "display_name": "Charisma Saving Throws",
      "name": "charisma-saving-throws"
    },
    {
      "id": 106,
      "display_name": "Constitution Ability Checks",
      "name": "constitution-ability-checks"
    },
    {
      "id": 99,
      "display_name": "Constitution Saving Throws",
      "name": "constitution-saving-throws"
    },
    {
      "id": 1691,
      "display_name": "Death Saving Throws",
      "name": "death-saving-throws"
    },
    {
      "id": 124,
      "display_name": "Deception",
      "name": "deception"
    },
    {
      "id": 105,
      "display_name": "Dexterity Ability Checks",
      "name": "dexterity-ability-checks"
    },
    {
      "id": 191,
      "display_name": "Dexterity Attacks",
      "name": "dexterity-attacks"
    },
    {
      "id": 98,
      "display_name": "Dexterity Saving Throws",
      "name": "dexterity-saving-throws"
    },
    {
      "id": 115,
      "display_name": "History",
      "name": "history"
    },
    {
      "id": 120,
      "display_name": "Insight",
      "name": "insight"
    },
    {
      "id": 107,
      "display_name": "Intelligence Ability Checks",
      "name": "intelligence-ability-checks"
    },
    {
      "id": 100,
      "display_name": "Intelligence Saving Throws",
      "name": "intelligence-saving-throws"
    },
    {
      "id": 125,
      "display_name": "Intimidation",
      "name": "intimidation"
    },
    {
      "id": 116,
      "display_name": "Investigation",
      "name": "investigation"
    },
    {
      "id": 121,
      "display_name": "Medicine",
      "name": "medicine"
    },
    {
      "id": 128,
      "display_name": "Melee Attacks",
      "name": "melee-attacks"
    },
    {
      "id": 117,
      "display_name": "Nature",
      "name": "nature"
    },
    {
      "id": 122,
      "display_name": "Perception",
      "name": "perception"
    },
    {
      "id": 126,
      "display_name": "Performance",
      "name": "performance"
    },
    {
      "id": 127,
      "display_name": "Persuasion",
      "name": "persuasion"
    },
    {
      "id": 129,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 118,
      "display_name": "Religion",
      "name": "religion"
    },
    {
      "id": 96,
      "display_name": "Saving Throws",
      "name": "saving-throws"
    },
    {
      "id": 112,
      "display_name": "Sleight of Hand",
      "name": "sleight-of-hand"
    },
    {
      "id": 130,
      "display_name": "Spell Attacks",
      "name": "spell-attacks"
    },
    {
      "id": 113,
      "display_name": "Stealth",
      "name": "stealth"
    },
    {
      "id": 104,
      "display_name": "Strength Ability Checks",
      "name": "strength-ability-checks"
    },
    {
      "id": 188,
      "display_name": "Strength Attacks",
      "name": "strength-attacks"
    },
    {
      "id": 97,
      "display_name": "Strength Saving Throws",
      "name": "strength-saving-throws"
    },
    {
      "id": 123,
      "display_name": "Survival",
      "name": "survival"
    },
    {
      "id": 1699,
      "display_name": "Weapon Attacks",
      "name": "weapon-attacks"
    },
    {
      "id": 108,
      "display_name": "Wisdom Ability Checks",
      "name": "wisdom-ability-checks"
    },
    {
      "id": 101,
      "display_name": "Wisdom Saving Throws",
      "name": "wisdom-saving-throws"
    }
  ],
  "5": [
    {
      "id": 131,
      "display_name": "Acid",
      "name": "acid"
    },
    {
      "id": 212,
      "display_name": "All",
      "name": "all"
    },
    {
      "id": 132,
      "display_name": "Bludgeoning",
      "name": "bludgeoning"
    },
    {
      "id": 215,
      "display_name": "Bludgeoning, Piercing, and Slashing from Nonmagical Weapons",
      "name": "bludgeoning-piercing-and-slashing-from-nonmagical-weapons"
    },
    {
      "id": 133,
      "display_name": "Cold",
      "name": "cold"
    },
    {
      "id": 769,
      "display_name": "Damage Dealt by Traps",
      "name": "damage-dealt-by-traps"
    },
    {
      "id": 134,
      "display_name": "Fire",
      "name": "fire"
    },
    {
      "id": 135,
      "display_name": "Force",
      "name": "force"
    },
    {
      "id": 136,
      "display_name": "Lightning",
      "name": "lightning"
    },
    {
      "id": 137,
      "display_name": "Necrotic",
      "name": "necrotic"
    },
    {
      "id": 138,
      "display_name": "Piercing",
      "name": "piercing"
    },
    {
      "id": 139,
      "display_name": "Poison",
      "name": "poison"
    },
    {
      "id": 140,
      "display_name": "Psychic",
      "name": "psychic"
    },
    {
      "id": 141,
      "display_name": "Radiant",
      "name": "radiant"
    },
    {
      "id": 318,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 142,
      "display_name": "Slashing",
      "name": "slashing"
    },
    {
      "id": 143,
      "display_name": "Thunder",
      "name": "thunder"
    }
  ],
  "6": [
    {
      "id": 144,
      "display_name": "Acid",
      "name": "acid"
    },
    {
      "id": 213,
      "display_name": "All",
      "name": "all"
    },
    {
      "id": 194,
      "display_name": "Blinded",
      "name": "blinded"
    },
    {
      "id": 145,
      "display_name": "Bludgeoning",
      "name": "bludgeoning"
    },
    {
      "id": 216,
      "display_name": "Bludgeoning, Piercing, and Slashing from Nonmagical Weapons",
      "name": "bludgeoning-piercing-and-slashing-from-nonmagical-weapons"
    },
    {
      "id": 195,
      "display_name": "Charmed",
      "name": "charmed"
    },
    {
      "id": 146,
      "display_name": "Cold",
      "name": "cold"
    },
    {
      "id": 221,
      "display_name": "Critical Hits",
      "name": "critical-hits"
    },
    {
      "id": 196,
      "display_name": "Deafened",
      "name": "deafened"
    },
    {
      "id": 767,
      "display_name": "Disease",
      "name": "disease"
    },
    {
      "id": 197,
      "display_name": "Exhaustion",
      "name": "exhaustion"
    },
    {
      "id": 147,
      "display_name": "Fire",
      "name": "fire"
    },
    {
      "id": 148,
      "display_name": "Force",
      "name": "force"
    },
    {
      "id": 198,
      "display_name": "Frightened",
      "name": "frightened"
    },
    {
      "id": 199,
      "display_name": "Grappled",
      "name": "grappled"
    },
    {
      "id": 200,
      "display_name": "Incapacitated",
      "name": "incapacitated"
    },
    {
      "id": 201,
      "display_name": "Invisible",
      "name": "invisible"
    },
    {
      "id": 149,
      "display_name": "Lightning",
      "name": "lightning"
    },
    {
      "id": 150,
      "display_name": "Necrotic",
      "name": "necrotic"
    },
    {
      "id": 202,
      "display_name": "Paralyzed",
      "name": "paralyzed"
    },
    {
      "id": 203,
      "display_name": "Petrified",
      "name": "petrified"
    },
    {
      "id": 151,
      "display_name": "Piercing",
      "name": "piercing"
    },
    {
      "id": 152,
      "display_name": "Poison",
      "name": "poison"
    },
    {
      "id": 205,
      "display_name": "Poisoned",
      "name": "poisoned"
    },
    {
      "id": 206,
      "display_name": "Prone",
      "name": "prone"
    },
    {
      "id": 153,
      "display_name": "Psychic",
      "name": "psychic"
    },
    {
      "id": 154,
      "display_name": "Radiant",
      "name": "radiant"
    },
    {
      "id": 207,
      "display_name": "Restrained",
      "name": "restrained"
    },
    {
      "id": 155,
      "display_name": "Slashing",
      "name": "slashing"
    },
    {
      "id": 208,
      "display_name": "Stunned",
      "name": "stunned"
    },
    {
      "id": 156,
      "display_name": "Thunder",
      "name": "thunder"
    },
    {
      "id": 209,
      "display_name": "Unconscious",
      "name": "unconscious"
    }
  ],
  "7": [
    {
      "id": 157,
      "display_name": "Acid",
      "name": "acid"
    },
    {
      "id": 214,
      "display_name": "All",
      "name": "all"
    },
    {
      "id": 158,
      "display_name": "Bludgeoning",
      "name": "bludgeoning"
    },
    {
      "id": 159,
      "display_name": "Cold",
      "name": "cold"
    },
    {
      "id": 160,
      "display_name": "Fire",
      "name": "fire"
    },
    {
      "id": 161,
      "display_name": "Force",
      "name": "force"
    },
    {
      "id": 162,
      "display_name": "Lightning",
      "name": "lightning"
    },
    {
      "id": 163,
      "display_name": "Necrotic",
      "name": "necrotic"
    },
    {
      "id": 164,
      "display_name": "Piercing",
      "name": "piercing"
    },
    {
      "id": 165,
      "display_name": "Poison",
      "name": "poison"
    },
    {
      "id": 166,
      "display_name": "Psychic",
      "name": "psychic"
    },
    {
      "id": 167,
      "display_name": "Radiant",
      "name": "radiant"
    },
    {
      "id": 168,
      "display_name": "Slashing",
      "name": "slashing"
    },
    {
      "id": 169,
      "display_name": "Thunder",
      "name": "thunder"
    }
  ],
  "8": [
    {
      "id": 170,
      "display_name": "Blindsight",
      "name": "blindsight"
    },
    {
      "id": 171,
      "display_name": "Darkvision",
      "name": "darkvision"
    },
    {
      "id": 172,
      "display_name": "Tremorsense",
      "name": "tremorsense"
    },
    {
      "id": 173,
      "display_name": "Truesight",
      "name": "truesight"
    }
  ],
  "9": [
    {
      "id": 1751,
      "display_name": "AC Max Dex Armored Modifier",
      "name": "ac-max-dex-armored-modifier"
    },
    {
      "id": 1045,
      "display_name": "AC Max Dex Modifier",
      "name": "ac-max-dex-modifier"
    },
    {
      "id": 1750,
      "display_name": "AC Max Dex Unarmored Modifier",
      "name": "ac-max-dex-unarmored-modifier"
    },
    {
      "id": 174,
      "display_name": "Armor Class",
      "name": "armor-class"
    },
    {
      "id": 1753,
      "display_name": "Attunement Slots",
      "name": "attunement-slots"
    },
    {
      "id": 180,
      "display_name": "Charisma Score",
      "name": "charisma-score"
    },
    {
      "id": 177,
      "display_name": "Constitution Score",
      "name": "constitution-score"
    },
    {
      "id": 176,
      "display_name": "Dexterity Score",
      "name": "dexterity-score"
    },
    {
      "id": 356,
      "display_name": "Extra Attacks",
      "name": "extra-attacks"
    },
    {
      "id": 185,
      "display_name": "Innate Speed (Burrowing)",
      "name": "innate-speed-burrowing"
    },
    {
      "id": 183,
      "display_name": "Innate Speed (Climbing)",
      "name": "innate-speed-climbing"
    },
    {
      "id": 182,
      "display_name": "Innate Speed (Flying)",
      "name": "innate-speed-flying"
    },
    {
      "id": 184,
      "display_name": "Innate Speed (Swimming)",
      "name": "innate-speed-swimming"
    },
    {
      "id": 181,
      "display_name": "Innate Speed (Walking)",
      "name": "innate-speed-walking"
    },
    {
      "id": 178,
      "display_name": "Intelligence Score",
      "name": "intelligence-score"
    },
    {
      "id": 1686,
      "display_name": "Minimum Base Armor",
      "name": "minimum-base-armor"
    },
    {
      "id": 1695,
      "display_name": "Speed (Burrowing)",
      "name": "speed-burrowing"
    },
    {
      "id": 1693,
      "display_name": "Speed (Climbing)",
      "name": "speed-climbing"
    },
    {
      "id": 1694,
      "display_name": "Speed (Flying)",
      "name": "speed-flying"
    },
    {
      "id": 1696,
      "display_name": "Speed (Swimming)",
      "name": "speed-swimming"
    },
    {
      "id": 1692,
      "display_name": "Speed (Walking)",
      "name": "speed-walking"
    },
    {
      "id": 175,
      "display_name": "Strength Score",
      "name": "strength-score"
    },
    {
      "id": 1005,
      "display_name": "Unarmed Damage Die",
      "name": "unarmed-damage-die"
    },
    {
      "id": 1006,
      "display_name": "Unarmored Armor Class",
      "name": "unarmored-armor-class"
    },
    {
      "id": 179,
      "display_name": "Wisdom Score",
      "name": "wisdom-score"
    }
  ],
  "10": [
    {
      "id": 229,
      "display_name": "Ability Checks",
      "name": "ability-checks"
    },
    {
      "id": 237,
      "display_name": "Acrobatics",
      "name": "acrobatics"
    },
    {
      "id": 320,
      "display_name": "Alchemist's Supplies",
      "name": "alchemists-supplies"
    },
    {
      "id": 245,
      "display_name": "Animal Handling",
      "name": "animal-handling"
    },
    {
      "id": 240,
      "display_name": "Arcana",
      "name": "arcana"
    },
    {
      "id": 236,
      "display_name": "Athletics",
      "name": "athletics"
    },
    {
      "id": 345,
      "display_name": "Bagpipes",
      "name": "bagpipes"
    },
    {
      "id": 277,
      "display_name": "Battleaxe",
      "name": "battleaxe"
    },
    {
      "id": 1815,
      "display_name": "Birdpipes",
      "name": "birdpipes"
    },
    {
      "id": 293,
      "display_name": "Blowgun",
      "name": "blowgun"
    },
    {
      "id": 1804,
      "display_name": "Boomerang",
      "name": "boomerang"
    },
    {
      "id": 305,
      "display_name": "Breastplate",
      "name": "breastplate"
    },
    {
      "id": 321,
      "display_name": "Brewer's Supplies",
      "name": "brewers-supplies"
    },
    {
      "id": 322,
      "display_name": "Calligrapher's Supplies",
      "name": "calligraphers-supplies"
    },
    {
      "id": 323,
      "display_name": "Carpenter's Tools",
      "name": "carpenters-tools"
    },
    {
      "id": 324,
      "display_name": "Cartographer's Tools",
      "name": "cartographers-tools"
    },
    {
      "id": 308,
      "display_name": "Chain Mail",
      "name": "chain-mail"
    },
    {
      "id": 304,
      "display_name": "Chain Shirt",
      "name": "chain-shirt"
    },
    {
      "id": 235,
      "display_name": "Charisma Ability Checks",
      "name": "charisma-ability-checks"
    },
    {
      "id": 228,
      "display_name": "Charisma Saving Throws",
      "name": "charisma-saving-throws"
    },
    {
      "id": 1742,
      "display_name": "Choose a Centaur Skill",
      "name": "choose-a-centaur-skill"
    },
    {
      "id": 811,
      "display_name": "Choose a Dwarf Artisan's Tool",
      "name": "choose-a-dwarf-artisans-tool"
    },
    {
      "id": 408,
      "display_name": "Choose a Gaming Set",
      "name": "choose-a-gaming-set"
    },
    {
      "id": 405,
      "display_name": "Choose a Heavy Armor Type",
      "name": "choose-a-heavy-armor-type"
    },
    {
      "id": 403,
      "display_name": "Choose a Light Armor Type",
      "name": "choose-a-light-armor-type"
    },
    {
      "id": 400,
      "display_name": "Choose a Martial Weapon",
      "name": "choose-a-martial-weapon"
    },
    {
      "id": 404,
      "display_name": "Choose a Medium Armor Type",
      "name": "choose-a-medium-armor-type"
    },
    {
      "id": 1740,
      "display_name": "Choose a Minotaur Skill",
      "name": "choose-a-minotaur-skill"
    },
    {
      "id": 409,
      "display_name": "Choose a Musical Instrument",
      "name": "choose-a-musical-instrument"
    },
    {
      "id": 800,
      "display_name": "Choose a Musical Instrument or Artisan's Tools",
      "name": "choose-a-musical-instrument-or-artisans-tools"
    },
    {
      "id": 793,
      "display_name": "Choose a Musical Instrument or Gaming Set",
      "name": "choose-a-musical-instrument-or-gaming-set"
    },
    {
      "id": 846,
      "display_name": "Choose a One-Handed Melee Weapon",
      "name": "choose-a-onehanded-melee-weapon"
    },
    {
      "id": 1743,
      "display_name": "Choose a Order Domain Skill",
      "name": "choose-a-order-domain-skill"
    },
    {
      "id": 768,
      "display_name": "Choose a Saving Throw",
      "name": "choose-a-saving-throw"
    },
    {
      "id": 401,
      "display_name": "Choose a Simple Weapon",
      "name": "choose-a-simple-weapon"
    },
    {
      "id": 1797,
      "display_name": "Choose a Simple Weapon or Tool",
      "name": "choose-a-simple-weapon-or-tool"
    },
    {
      "id": 1798,
      "display_name": "Choose a Simple Weapon, Martial Weapon, or Tool",
      "name": "choose-a-simple-weapon-martial-weapon-or-tool"
    },
    {
      "id": 1801,
      "display_name": "Choose a Simple Weapon, Martial Weapon, Tool, or Heavy Armor",
      "name": "choose-a-simple-weapon-martial-weapon-tool-or-heavy-armor"
    },
    {
      "id": 1799,
      "display_name": "Choose a Simple Weapon, Martial Weapon, Tool, or Light Armor",
      "name": "choose-a-simple-weapon-martial-weapon-tool-or-light-armor"
    },
    {
      "id": 1800,
      "display_name": "Choose a Simple Weapon, Martial Weapon, Tool, or Medium Armor",
      "name": "choose-a-simple-weapon-martial-weapon-tool-or-medium-armor"
    },
    {
      "id": 406,
      "display_name": "Choose a Skill",
      "name": "choose-a-skill"
    },
    {
      "id": 776,
      "display_name": "Choose a Skill or Tool",
      "name": "choose-a-skill-or-tool"
    },
    {
      "id": 1796,
      "display_name": "Choose a Skill, Simple Weapon, or Tool",
      "name": "choose-a-skill-simple-weapon-or-tool"
    },
    {
      "id": 1778,
      "display_name": "Choose a Telepathic Skill",
      "name": "choose-a-telepathic-skill"
    },
    {
      "id": 410,
      "display_name": "Choose a Tool",
      "name": "choose-a-tool"
    },
    {
      "id": 399,
      "display_name": "Choose a Weapon",
      "name": "choose-a-weapon"
    },
    {
      "id": 402,
      "display_name": "Choose an Armor Type",
      "name": "choose-an-armor-type"
    },
    {
      "id": 1749,
      "display_name": "Choose an Artificer Skill",
      "name": "choose-an-artificer-skill"
    },
    {
      "id": 407,
      "display_name": "Choose an Artisan's Tool",
      "name": "choose-an-artisans-tool"
    },
    {
      "id": 792,
      "display_name": "Choose an Intelligence, Wisdom, or Charisma Skill",
      "name": "choose-an-intelligence-wisdom-or-charisma-skill"
    },
    {
      "id": 799,
      "display_name": "Choose an Urban Bounty Hunter Tool",
      "name": "choose-an-urban-bounty-hunter-tool"
    },
    {
      "id": 264,
      "display_name": "Club",
      "name": "club"
    },
    {
      "id": 325,
      "display_name": "Cobbler's Tools",
      "name": "cobblers-tools"
    },
    {
      "id": 232,
      "display_name": "Constitution Ability Checks",
      "name": "constitution-ability-checks"
    },
    {
      "id": 225,
      "display_name": "Constitution Saving Throws",
      "name": "constitution-saving-throws"
    },
    {
      "id": 326,
      "display_name": "Cook's Utensils",
      "name": "cooks-utensils"
    },
    {
      "id": 260,
      "display_name": "Crossbow, Hand",
      "name": "crossbow-hand"
    },
    {
      "id": 294,
      "display_name": "Crossbow, Heavy",
      "name": "crossbow-heavy"
    },
    {
      "id": 273,
      "display_name": "Crossbow, Light",
      "name": "crossbow-light"
    },
    {
      "id": 262,
      "display_name": "Dagger",
      "name": "dagger"
    },
    {
      "id": 274,
      "display_name": "Dart",
      "name": "dart"
    },
    {
      "id": 250,
      "display_name": "Deception",
      "name": "deception"
    },
    {
      "id": 231,
      "display_name": "Dexterity Ability Checks",
      "name": "dexterity-ability-checks"
    },
    {
      "id": 258,
      "display_name": "Dexterity Attacks",
      "name": "dexterity-attacks"
    },
    {
      "id": 224,
      "display_name": "Dexterity Saving Throws",
      "name": "dexterity-saving-throws"
    },
    {
      "id": 343,
      "display_name": "Dice Set",
      "name": "dice-set"
    },
    {
      "id": 337,
      "display_name": "Disguise Kit",
      "name": "disguise-kit"
    },
    {
      "id": 1721,
      "display_name": "Double-Bladed Scimitar",
      "name": "doublebladed-scimitar"
    },
    {
      "id": 1044,
      "display_name": "Dragonchess Set",
      "name": "dragonchess-set"
    },
    {
      "id": 346,
      "display_name": "Drum",
      "name": "drum"
    },
    {
      "id": 347,
      "display_name": "Dulcimer",
      "name": "dulcimer"
    },
    {
      "id": 1712,
      "display_name": "Firearms",
      "name": "firearms"
    },
    {
      "id": 278,
      "display_name": "Flail",
      "name": "flail"
    },
    {
      "id": 348,
      "display_name": "Flute",
      "name": "flute"
    },
    {
      "id": 338,
      "display_name": "Forgery Kit",
      "name": "forgery-kit"
    },
    {
      "id": 261,
      "display_name": "Glaive",
      "name": "glaive"
    },
    {
      "id": 327,
      "display_name": "Glassblower's Tools",
      "name": "glassblowers-tools"
    },
    {
      "id": 1816,
      "display_name": "Glaur",
      "name": "glaur"
    },
    {
      "id": 279,
      "display_name": "Greataxe",
      "name": "greataxe"
    },
    {
      "id": 265,
      "display_name": "Greatclub",
      "name": "greatclub"
    },
    {
      "id": 280,
      "display_name": "Greatsword",
      "name": "greatsword"
    },
    {
      "id": 281,
      "display_name": "Halberd",
      "name": "halberd"
    },
    {
      "id": 306,
      "display_name": "Half Plate",
      "name": "half-plate"
    },
    {
      "id": 1817,
      "display_name": "Hand Drum",
      "name": "hand-drum"
    },
    {
      "id": 266,
      "display_name": "Handaxe",
      "name": "handaxe"
    },
    {
      "id": 398,
      "display_name": "Heavy Armor",
      "name": "heavy-armor"
    },
    {
      "id": 339,
      "display_name": "Herbalism Kit",
      "name": "herbalism-kit"
    },
    {
      "id": 303,
      "display_name": "Hide",
      "name": "hide"
    },
    {
      "id": 241,
      "display_name": "History",
      "name": "history"
    },
    {
      "id": 351,
      "display_name": "Horn",
      "name": "horn"
    },
    {
      "id": 778,
      "display_name": "Improvised Weapons",
      "name": "improvised-weapons"
    },
    {
      "id": 259,
      "display_name": "Initiative",
      "name": "initiative"
    },
    {
      "id": 246,
      "display_name": "Insight",
      "name": "insight"
    },
    {
      "id": 233,
      "display_name": "Intelligence Ability Checks",
      "name": "intelligence-ability-checks"
    },
    {
      "id": 226,
      "display_name": "Intelligence Saving Throws",
      "name": "intelligence-saving-throws"
    },
    {
      "id": 251,
      "display_name": "Intimidation",
      "name": "intimidation"
    },
    {
      "id": 242,
      "display_name": "Investigation",
      "name": "investigation"
    },
    {
      "id": 267,
      "display_name": "Javelin",
      "name": "javelin"
    },
    {
      "id": 328,
      "display_name": "Jeweler's Tools",
      "name": "jewelers-tools"
    },
    {
      "id": 282,
      "display_name": "Lance",
      "name": "lance"
    },
    {
      "id": 302,
      "display_name": "Leather",
      "name": "leather"
    },
    {
      "id": 329,
      "display_name": "Leatherworker's Tools",
      "name": "leatherworkers-tools"
    },
    {
      "id": 396,
      "display_name": "Light Armor",
      "name": "light-armor"
    },
    {
      "id": 268,
      "display_name": "Light Hammer",
      "name": "light-hammer"
    },
    {
      "id": 295,
      "display_name": "Longbow",
      "name": "longbow"
    },
    {
      "id": 1818,
      "display_name": "Longhorn",
      "name": "longhorn"
    },
    {
      "id": 263,
      "display_name": "Longsword",
      "name": "longsword"
    },
    {
      "id": 349,
      "display_name": "Lute",
      "name": "lute"
    },
    {
      "id": 350,
      "display_name": "Lyre",
      "name": "lyre"
    },
    {
      "id": 269,
      "display_name": "Mace",
      "name": "mace"
    },
    {
      "id": 394,
      "display_name": "Martial Weapons",
      "name": "martial-weapons"
    },
    {
      "id": 330,
      "display_name": "Mason's Tools",
      "name": "masons-tools"
    },
    {
      "id": 283,
      "display_name": "Maul",
      "name": "maul"
    },
    {
      "id": 247,
      "display_name": "Medicine",
      "name": "medicine"
    },
    {
      "id": 397,
      "display_name": "Medium Armor",
      "name": "medium-armor"
    },
    {
      "id": 254,
      "display_name": "Melee Attacks",
      "name": "melee-attacks"
    },
    {
      "id": 299,
      "display_name": "Mithril Plate Armor",
      "name": "mithril-plate-armor"
    },
    {
      "id": 284,
      "display_name": "Morningstar",
      "name": "morningstar"
    },
    {
      "id": 243,
      "display_name": "Nature",
      "name": "nature"
    },
    {
      "id": 340,
      "display_name": "Navigator's Tools",
      "name": "navigators-tools"
    },
    {
      "id": 296,
      "display_name": "Net",
      "name": "net"
    },
    {
      "id": 301,
      "display_name": "Padded",
      "name": "padded"
    },
    {
      "id": 331,
      "display_name": "Painter's Supplies",
      "name": "painters-supplies"
    },
    {
      "id": 352,
      "display_name": "Pan Flute",
      "name": "pan-flute"
    },
    {
      "id": 248,
      "display_name": "Perception",
      "name": "perception"
    },
    {
      "id": 252,
      "display_name": "Performance",
      "name": "performance"
    },
    {
      "id": 253,
      "display_name": "Persuasion",
      "name": "persuasion"
    },
    {
      "id": 285,
      "display_name": "Pike",
      "name": "pike"
    },
    {
      "id": 310,
      "display_name": "Plate",
      "name": "plate"
    },
    {
      "id": 344,
      "display_name": "Playing Card Set",
      "name": "playing-card-set"
    },
    {
      "id": 341,
      "display_name": "Poisoner's Kit",
      "name": "poisoners-kit"
    },
    {
      "id": 332,
      "display_name": "Potter's Tools",
      "name": "potters-tools"
    },
    {
      "id": 270,
      "display_name": "Quarterstaff",
      "name": "quarterstaff"
    },
    {
      "id": 255,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 286,
      "display_name": "Rapier",
      "name": "rapier"
    },
    {
      "id": 244,
      "display_name": "Religion",
      "name": "religion"
    },
    {
      "id": 307,
      "display_name": "Ring Mail",
      "name": "ring-mail"
    },
    {
      "id": 222,
      "display_name": "Saving Throws",
      "name": "saving-throws"
    },
    {
      "id": 298,
      "display_name": "Scale Mail",
      "name": "scale-mail"
    },
    {
      "id": 287,
      "display_name": "Scimitar",
      "name": "scimitar"
    },
    {
      "id": 1042,
      "display_name": "Self",
      "name": "self"
    },
    {
      "id": 353,
      "display_name": "Shawm",
      "name": "shawm"
    },
    {
      "id": 694,
      "display_name": "Shields",
      "name": "shields"
    },
    {
      "id": 275,
      "display_name": "Shortbow",
      "name": "shortbow"
    },
    {
      "id": 288,
      "display_name": "Shortsword",
      "name": "shortsword"
    },
    {
      "id": 271,
      "display_name": "Sickle",
      "name": "sickle"
    },
    {
      "id": 395,
      "display_name": "Simple Weapons",
      "name": "simple-weapons"
    },
    {
      "id": 238,
      "display_name": "Sleight of Hand",
      "name": "sleight-of-hand"
    },
    {
      "id": 276,
      "display_name": "Sling",
      "name": "sling"
    },
    {
      "id": 333,
      "display_name": "Smith's Tools",
      "name": "smiths-tools"
    },
    {
      "id": 1819,
      "display_name": "Songhorn",
      "name": "songhorn"
    },
    {
      "id": 272,
      "display_name": "Spear",
      "name": "spear"
    },
    {
      "id": 256,
      "display_name": "Spell Attacks",
      "name": "spell-attacks"
    },
    {
      "id": 309,
      "display_name": "Splint",
      "name": "splint"
    },
    {
      "id": 239,
      "display_name": "Stealth",
      "name": "stealth"
    },
    {
      "id": 300,
      "display_name": "Steel Shield",
      "name": "steel-shield"
    },
    {
      "id": 230,
      "display_name": "Strength Ability Checks",
      "name": "strength-ability-checks"
    },
    {
      "id": 257,
      "display_name": "Strength Attacks",
      "name": "strength-attacks"
    },
    {
      "id": 223,
      "display_name": "Strength Saving Throws",
      "name": "strength-saving-throws"
    },
    {
      "id": 297,
      "display_name": "Studded Leather",
      "name": "studded-leather"
    },
    {
      "id": 249,
      "display_name": "Survival",
      "name": "survival"
    },
    {
      "id": 1820,
      "display_name": "Tantan",
      "name": "tantan"
    },
    {
      "id": 1821,
      "display_name": "Thelarr",
      "name": "thelarr"
    },
    {
      "id": 342,
      "display_name": "Thieves' Tools",
      "name": "thieves-tools"
    },
    {
      "id": 1043,
      "display_name": "Three-Dragon Ante Set",
      "name": "threedragon-ante-set"
    },
    {
      "id": 334,
      "display_name": "Tinker's Tools",
      "name": "tinkers-tools"
    },
    {
      "id": 1822,
      "display_name": "Tocken",
      "name": "tocken"
    },
    {
      "id": 289,
      "display_name": "Trident",
      "name": "trident"
    },
    {
      "id": 796,
      "display_name": "Vehicles (Land)",
      "name": "vehicles-land"
    },
    {
      "id": 797,
      "display_name": "Vehicles (Water)",
      "name": "vehicles-water"
    },
    {
      "id": 354,
      "display_name": "Viol",
      "name": "viol"
    },
    {
      "id": 290,
      "display_name": "War pick",
      "name": "war-pick"
    },
    {
      "id": 1823,
      "display_name": "Wargong",
      "name": "wargong"
    },
    {
      "id": 291,
      "display_name": "Warhammer",
      "name": "warhammer"
    },
    {
      "id": 335,
      "display_name": "Weaver's Tools",
      "name": "weavers-tools"
    },
    {
      "id": 292,
      "display_name": "Whip",
      "name": "whip"
    },
    {
      "id": 1824,
      "display_name": "Whistle-Stick",
      "name": "whistlestick"
    },
    {
      "id": 234,
      "display_name": "Wisdom Ability Checks",
      "name": "wisdom-ability-checks"
    },
    {
      "id": 227,
      "display_name": "Wisdom Saving Throws",
      "name": "wisdom-saving-throws"
    },
    {
      "id": 336,
      "display_name": "Woodcarver's Tools",
      "name": "woodcarvers-tools"
    },
    {
      "id": 1825,
      "display_name": "Yarting",
      "name": "yarting"
    },
    {
      "id": 1805,
      "display_name": "Yklwa",
      "name": "yklwa"
    },
    {
      "id": 1826,
      "display_name": "Zulkoon",
      "name": "zulkoon"
    }
  ],
  "11": [
    {
      "id": 391,
      "display_name": "Aarakocra",
      "name": "aarakocra"
    },
    {
      "id": 365,
      "display_name": "Abyssal",
      "name": "abyssal"
    },
    {
      "id": 390,
      "display_name": "All",
      "name": "all"
    },
    {
      "id": 374,
      "display_name": "Aquan",
      "name": "aquan"
    },
    {
      "id": 375,
      "display_name": "Auran",
      "name": "auran"
    },
    {
      "id": 388,
      "display_name": "Blink Dog",
      "name": "blink-dog"
    },
    {
      "id": 366,
      "display_name": "Celestial",
      "name": "celestial"
    },
    {
      "id": 392,
      "display_name": "Choose a Language",
      "name": "choose-a-language"
    },
    {
      "id": 1054,
      "display_name": "Choose an Exotic Language",
      "name": "choose-an-exotic-language"
    },
    {
      "id": 1741,
      "display_name": "Choose Elvish or Vedalken",
      "name": "choose-elvish-or-vedalken"
    },
    {
      "id": 357,
      "display_name": "Common",
      "name": "common"
    },
    {
      "id": 1776,
      "display_name": "Daelkyr",
      "name": "daelkyr"
    },
    {
      "id": 368,
      "display_name": "Deep Speech",
      "name": "deep-speech"
    },
    {
      "id": 367,
      "display_name": "Draconic",
      "name": "draconic"
    },
    {
      "id": 378,
      "display_name": "Druidic",
      "name": "druidic"
    },
    {
      "id": 358,
      "display_name": "Dwarvish",
      "name": "dwarvish"
    },
    {
      "id": 359,
      "display_name": "Elvish",
      "name": "elvish"
    },
    {
      "id": 360,
      "display_name": "Giant",
      "name": "giant"
    },
    {
      "id": 379,
      "display_name": "Giant Eagle",
      "name": "giant-eagle"
    },
    {
      "id": 380,
      "display_name": "Giant Elk",
      "name": "giant-elk"
    },
    {
      "id": 381,
      "display_name": "Giant Owl",
      "name": "giant-owl"
    },
    {
      "id": 1713,
      "display_name": "Gith",
      "name": "gith"
    },
    {
      "id": 382,
      "display_name": "Gnoll",
      "name": "gnoll"
    },
    {
      "id": 361,
      "display_name": "Gnomish",
      "name": "gnomish"
    },
    {
      "id": 362,
      "display_name": "Goblin",
      "name": "goblin"
    },
    {
      "id": 1827,
      "display_name": "Grippli",
      "name": "grippli"
    },
    {
      "id": 1781,
      "display_name": "Grung",
      "name": "grung"
    },
    {
      "id": 363,
      "display_name": "Halfling",
      "name": "halfling"
    },
    {
      "id": 376,
      "display_name": "Ignan",
      "name": "ignan"
    },
    {
      "id": 369,
      "display_name": "Infernal",
      "name": "infernal"
    },
    {
      "id": 1738,
      "display_name": "Kraul",
      "name": "kraul"
    },
    {
      "id": 1790,
      "display_name": "Leonin",
      "name": "leonin"
    },
    {
      "id": 1736,
      "display_name": "Loxodon",
      "name": "loxodon"
    },
    {
      "id": 1787,
      "display_name": "Marquesian",
      "name": "marquesian"
    },
    {
      "id": 1737,
      "display_name": "Minotaur",
      "name": "minotaur"
    },
    {
      "id": 1788,
      "display_name": "Naush",
      "name": "naush"
    },
    {
      "id": 364,
      "display_name": "Orc",
      "name": "orc"
    },
    {
      "id": 383,
      "display_name": "Otyugh",
      "name": "otyugh"
    },
    {
      "id": 370,
      "display_name": "Primordial",
      "name": "primordial"
    },
    {
      "id": 1722,
      "display_name": "Quori",
      "name": "quori"
    },
    {
      "id": 1777,
      "display_name": "Riedran",
      "name": "riedran"
    },
    {
      "id": 384,
      "display_name": "Sahuagin",
      "name": "sahuagin"
    },
    {
      "id": 385,
      "display_name": "Sphinx",
      "name": "sphinx"
    },
    {
      "id": 371,
      "display_name": "Sylvan",
      "name": "sylvan"
    },
    {
      "id": 373,
      "display_name": "Telepathy",
      "name": "telepathy"
    },
    {
      "id": 377,
      "display_name": "Terran",
      "name": "terran"
    },
    {
      "id": 1783,
      "display_name": "Thievesâ€™ Cant",
      "name": "thievesâ€™-cant"
    },
    {
      "id": 372,
      "display_name": "Undercommon",
      "name": "undercommon"
    },
    {
      "id": 1739,
      "display_name": "Vedalken",
      "name": "vedalken"
    },
    {
      "id": 386,
      "display_name": "Winter Wolf",
      "name": "winter-wolf"
    },
    {
      "id": 387,
      "display_name": "Worg",
      "name": "worg"
    },
    {
      "id": 389,
      "display_name": "Yeti",
      "name": "yeti"
    },
    {
      "id": 1786,
      "display_name": "Zemnian",
      "name": "zemnian"
    }
  ],
  "12": [
    {
      "id": 419,
      "display_name": "Ability Checks",
      "name": "ability-checks"
    },
    {
      "id": 427,
      "display_name": "Acrobatics",
      "name": "acrobatics"
    },
    {
      "id": 501,
      "display_name": "Alchemist's Supplies",
      "name": "alchemists-supplies"
    },
    {
      "id": 435,
      "display_name": "Animal Handling",
      "name": "animal-handling"
    },
    {
      "id": 430,
      "display_name": "Arcana",
      "name": "arcana"
    },
    {
      "id": 426,
      "display_name": "Athletics",
      "name": "athletics"
    },
    {
      "id": 526,
      "display_name": "Bagpipes",
      "name": "bagpipes"
    },
    {
      "id": 467,
      "display_name": "Battleaxe",
      "name": "battleaxe"
    },
    {
      "id": 483,
      "display_name": "Blowgun",
      "name": "blowgun"
    },
    {
      "id": 495,
      "display_name": "Breastplate",
      "name": "breastplate"
    },
    {
      "id": 502,
      "display_name": "Brewer's Supplies",
      "name": "brewers-supplies"
    },
    {
      "id": 503,
      "display_name": "Calligrapher's Supplies",
      "name": "calligraphers-supplies"
    },
    {
      "id": 504,
      "display_name": "Carpenter's Tools",
      "name": "carpenters-tools"
    },
    {
      "id": 505,
      "display_name": "Cartographer's Tools",
      "name": "cartographers-tools"
    },
    {
      "id": 498,
      "display_name": "Chain Mail",
      "name": "chain-mail"
    },
    {
      "id": 494,
      "display_name": "Chain Shirt",
      "name": "chain-shirt"
    },
    {
      "id": 425,
      "display_name": "Charisma Ability Checks",
      "name": "charisma-ability-checks"
    },
    {
      "id": 418,
      "display_name": "Charisma Saving Throws",
      "name": "charisma-saving-throws"
    },
    {
      "id": 550,
      "display_name": "Choose a Gaming Set",
      "name": "choose-a-gaming-set"
    },
    {
      "id": 547,
      "display_name": "Choose a Heavy Armor Type",
      "name": "choose-a-heavy-armor-type"
    },
    {
      "id": 545,
      "display_name": "Choose a Light Armor Type",
      "name": "choose-a-light-armor-type"
    },
    {
      "id": 542,
      "display_name": "Choose a Martial Weapon",
      "name": "choose-a-martial-weapon"
    },
    {
      "id": 546,
      "display_name": "Choose a Medium Armor Type",
      "name": "choose-a-medium-armor-type"
    },
    {
      "id": 551,
      "display_name": "Choose a Musical Instrument",
      "name": "choose-a-musical-instrument"
    },
    {
      "id": 543,
      "display_name": "Choose a Simple Weapon",
      "name": "choose-a-simple-weapon"
    },
    {
      "id": 548,
      "display_name": "Choose a Skill",
      "name": "choose-a-skill"
    },
    {
      "id": 1794,
      "display_name": "Choose a Skill or Tool",
      "name": "choose-a-skill-or-tool"
    },
    {
      "id": 552,
      "display_name": "Choose a Tool",
      "name": "choose-a-tool"
    },
    {
      "id": 541,
      "display_name": "Choose a Weapon",
      "name": "choose-a-weapon"
    },
    {
      "id": 544,
      "display_name": "Choose an Armor Type",
      "name": "choose-an-armor-type"
    },
    {
      "id": 549,
      "display_name": "Choose an Artisan's Tool",
      "name": "choose-an-artisans-tool"
    },
    {
      "id": 807,
      "display_name": "Choose Rogue Expertise",
      "name": "choose-rogue-expertise"
    },
    {
      "id": 454,
      "display_name": "Club",
      "name": "club"
    },
    {
      "id": 506,
      "display_name": "Cobbler's Tools",
      "name": "cobblers-tools"
    },
    {
      "id": 422,
      "display_name": "Constitution Ability Checks",
      "name": "constitution-ability-checks"
    },
    {
      "id": 415,
      "display_name": "Constitution Saving Throws",
      "name": "constitution-saving-throws"
    },
    {
      "id": 507,
      "display_name": "Cook's Utensils",
      "name": "cooks-utensils"
    },
    {
      "id": 450,
      "display_name": "Crossbow, Hand",
      "name": "crossbow-hand"
    },
    {
      "id": 484,
      "display_name": "Crossbow, Heavy",
      "name": "crossbow-heavy"
    },
    {
      "id": 463,
      "display_name": "Crossbow, Light",
      "name": "crossbow-light"
    },
    {
      "id": 452,
      "display_name": "Dagger",
      "name": "dagger"
    },
    {
      "id": 464,
      "display_name": "Dart",
      "name": "dart"
    },
    {
      "id": 440,
      "display_name": "Deception",
      "name": "deception"
    },
    {
      "id": 421,
      "display_name": "Dexterity Ability Checks",
      "name": "dexterity-ability-checks"
    },
    {
      "id": 448,
      "display_name": "Dexterity Attacks",
      "name": "dexterity-attacks"
    },
    {
      "id": 414,
      "display_name": "Dexterity Saving Throws",
      "name": "dexterity-saving-throws"
    },
    {
      "id": 524,
      "display_name": "Dice Set",
      "name": "dice-set"
    },
    {
      "id": 518,
      "display_name": "Disguise Kit",
      "name": "disguise-kit"
    },
    {
      "id": 527,
      "display_name": "Drum",
      "name": "drum"
    },
    {
      "id": 528,
      "display_name": "Dulcimer",
      "name": "dulcimer"
    },
    {
      "id": 468,
      "display_name": "Flail",
      "name": "flail"
    },
    {
      "id": 529,
      "display_name": "Flute",
      "name": "flute"
    },
    {
      "id": 519,
      "display_name": "Forgery Kit",
      "name": "forgery-kit"
    },
    {
      "id": 451,
      "display_name": "Glaive",
      "name": "glaive"
    },
    {
      "id": 508,
      "display_name": "Glassblower's Tools",
      "name": "glassblowers-tools"
    },
    {
      "id": 469,
      "display_name": "Greataxe",
      "name": "greataxe"
    },
    {
      "id": 455,
      "display_name": "Greatclub",
      "name": "greatclub"
    },
    {
      "id": 470,
      "display_name": "Greatsword",
      "name": "greatsword"
    },
    {
      "id": 471,
      "display_name": "Halberd",
      "name": "halberd"
    },
    {
      "id": 496,
      "display_name": "Half Plate",
      "name": "half-plate"
    },
    {
      "id": 456,
      "display_name": "Handaxe",
      "name": "handaxe"
    },
    {
      "id": 540,
      "display_name": "Heavy Armor",
      "name": "heavy-armor"
    },
    {
      "id": 520,
      "display_name": "Herbalism Kit",
      "name": "herbalism-kit"
    },
    {
      "id": 493,
      "display_name": "Hide",
      "name": "hide"
    },
    {
      "id": 431,
      "display_name": "History",
      "name": "history"
    },
    {
      "id": 532,
      "display_name": "Horn",
      "name": "horn"
    },
    {
      "id": 449,
      "display_name": "Initiative",
      "name": "initiative"
    },
    {
      "id": 436,
      "display_name": "Insight",
      "name": "insight"
    },
    {
      "id": 423,
      "display_name": "Intelligence Ability Checks",
      "name": "intelligence-ability-checks"
    },
    {
      "id": 416,
      "display_name": "Intelligence Saving Throws",
      "name": "intelligence-saving-throws"
    },
    {
      "id": 441,
      "display_name": "Intimidation",
      "name": "intimidation"
    },
    {
      "id": 432,
      "display_name": "Investigation",
      "name": "investigation"
    },
    {
      "id": 457,
      "display_name": "Javelin",
      "name": "javelin"
    },
    {
      "id": 509,
      "display_name": "Jeweler's Tools",
      "name": "jewelers-tools"
    },
    {
      "id": 472,
      "display_name": "Lance",
      "name": "lance"
    },
    {
      "id": 492,
      "display_name": "Leather",
      "name": "leather"
    },
    {
      "id": 510,
      "display_name": "Leatherworker's Tools",
      "name": "leatherworkers-tools"
    },
    {
      "id": 538,
      "display_name": "Light Armor",
      "name": "light-armor"
    },
    {
      "id": 458,
      "display_name": "Light Hammer",
      "name": "light-hammer"
    },
    {
      "id": 485,
      "display_name": "Longbow",
      "name": "longbow"
    },
    {
      "id": 453,
      "display_name": "Longsword",
      "name": "longsword"
    },
    {
      "id": 530,
      "display_name": "Lute",
      "name": "lute"
    },
    {
      "id": 531,
      "display_name": "Lyre",
      "name": "lyre"
    },
    {
      "id": 459,
      "display_name": "Mace",
      "name": "mace"
    },
    {
      "id": 536,
      "display_name": "Martial Weapons",
      "name": "martial-weapons"
    },
    {
      "id": 511,
      "display_name": "Mason's Tools",
      "name": "masons-tools"
    },
    {
      "id": 473,
      "display_name": "Maul",
      "name": "maul"
    },
    {
      "id": 437,
      "display_name": "Medicine",
      "name": "medicine"
    },
    {
      "id": 539,
      "display_name": "Medium Armor",
      "name": "medium-armor"
    },
    {
      "id": 444,
      "display_name": "Melee Attacks",
      "name": "melee-attacks"
    },
    {
      "id": 489,
      "display_name": "Mithril Plate Armor",
      "name": "mithril-plate-armor"
    },
    {
      "id": 474,
      "display_name": "Morningstar",
      "name": "morningstar"
    },
    {
      "id": 433,
      "display_name": "Nature",
      "name": "nature"
    },
    {
      "id": 521,
      "display_name": "Navigator's Tools",
      "name": "navigators-tools"
    },
    {
      "id": 486,
      "display_name": "Net",
      "name": "net"
    },
    {
      "id": 491,
      "display_name": "Padded",
      "name": "padded"
    },
    {
      "id": 512,
      "display_name": "Painter's Supplies",
      "name": "painters-supplies"
    },
    {
      "id": 533,
      "display_name": "Pan Flute",
      "name": "pan-flute"
    },
    {
      "id": 438,
      "display_name": "Perception",
      "name": "perception"
    },
    {
      "id": 442,
      "display_name": "Performance",
      "name": "performance"
    },
    {
      "id": 443,
      "display_name": "Persuasion",
      "name": "persuasion"
    },
    {
      "id": 475,
      "display_name": "Pike",
      "name": "pike"
    },
    {
      "id": 500,
      "display_name": "Plate",
      "name": "plate"
    },
    {
      "id": 525,
      "display_name": "Playing Card Set",
      "name": "playing-card-set"
    },
    {
      "id": 522,
      "display_name": "Poisoner's Kit",
      "name": "poisoners-kit"
    },
    {
      "id": 513,
      "display_name": "Potter's Tools",
      "name": "potters-tools"
    },
    {
      "id": 460,
      "display_name": "Quarterstaff",
      "name": "quarterstaff"
    },
    {
      "id": 445,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 476,
      "display_name": "Rapier",
      "name": "rapier"
    },
    {
      "id": 434,
      "display_name": "Religion",
      "name": "religion"
    },
    {
      "id": 497,
      "display_name": "Ring Mail",
      "name": "ring-mail"
    },
    {
      "id": 412,
      "display_name": "Saving Throws",
      "name": "saving-throws"
    },
    {
      "id": 488,
      "display_name": "Scale Mail",
      "name": "scale-mail"
    },
    {
      "id": 477,
      "display_name": "Scimitar",
      "name": "scimitar"
    },
    {
      "id": 534,
      "display_name": "Shawm",
      "name": "shawm"
    },
    {
      "id": 465,
      "display_name": "Shortbow",
      "name": "shortbow"
    },
    {
      "id": 478,
      "display_name": "Shortsword",
      "name": "shortsword"
    },
    {
      "id": 461,
      "display_name": "Sickle",
      "name": "sickle"
    },
    {
      "id": 537,
      "display_name": "Simple Weapons",
      "name": "simple-weapons"
    },
    {
      "id": 428,
      "display_name": "Sleight of Hand",
      "name": "sleight-of-hand"
    },
    {
      "id": 466,
      "display_name": "Sling",
      "name": "sling"
    },
    {
      "id": 514,
      "display_name": "Smith's Tools",
      "name": "smiths-tools"
    },
    {
      "id": 462,
      "display_name": "Spear",
      "name": "spear"
    },
    {
      "id": 446,
      "display_name": "Spell Attacks",
      "name": "spell-attacks"
    },
    {
      "id": 499,
      "display_name": "Splint",
      "name": "splint"
    },
    {
      "id": 429,
      "display_name": "Stealth",
      "name": "stealth"
    },
    {
      "id": 490,
      "display_name": "Steel Shield",
      "name": "steel-shield"
    },
    {
      "id": 420,
      "display_name": "Strength Ability Checks",
      "name": "strength-ability-checks"
    },
    {
      "id": 447,
      "display_name": "Strength Attacks",
      "name": "strength-attacks"
    },
    {
      "id": 413,
      "display_name": "Strength Saving Throws",
      "name": "strength-saving-throws"
    },
    {
      "id": 487,
      "display_name": "Studded Leather",
      "name": "studded-leather"
    },
    {
      "id": 439,
      "display_name": "Survival",
      "name": "survival"
    },
    {
      "id": 523,
      "display_name": "Thieves' Tools",
      "name": "thieves-tools"
    },
    {
      "id": 515,
      "display_name": "Tinker's Tools",
      "name": "tinkers-tools"
    },
    {
      "id": 479,
      "display_name": "Trident",
      "name": "trident"
    },
    {
      "id": 535,
      "display_name": "Viol",
      "name": "viol"
    },
    {
      "id": 480,
      "display_name": "War pick",
      "name": "war-pick"
    },
    {
      "id": 481,
      "display_name": "Warhammer",
      "name": "warhammer"
    },
    {
      "id": 516,
      "display_name": "Weaver's Tools",
      "name": "weavers-tools"
    },
    {
      "id": 482,
      "display_name": "Whip",
      "name": "whip"
    },
    {
      "id": 424,
      "display_name": "Wisdom Ability Checks",
      "name": "wisdom-ability-checks"
    },
    {
      "id": 417,
      "display_name": "Wisdom Saving Throws",
      "name": "wisdom-saving-throws"
    },
    {
      "id": 517,
      "display_name": "Woodcarver's Tools",
      "name": "woodcarvers-tools"
    }
  ],
  "13": [
    {
      "id": 560,
      "display_name": "Ability Checks",
      "name": "ability-checks"
    },
    {
      "id": 568,
      "display_name": "Acrobatics",
      "name": "acrobatics"
    },
    {
      "id": 642,
      "display_name": "Alchemist's Supplies",
      "name": "alchemists-supplies"
    },
    {
      "id": 576,
      "display_name": "Animal Handling",
      "name": "animal-handling"
    },
    {
      "id": 571,
      "display_name": "Arcana",
      "name": "arcana"
    },
    {
      "id": 567,
      "display_name": "Athletics",
      "name": "athletics"
    },
    {
      "id": 667,
      "display_name": "Bagpipes",
      "name": "bagpipes"
    },
    {
      "id": 608,
      "display_name": "Battleaxe",
      "name": "battleaxe"
    },
    {
      "id": 624,
      "display_name": "Blowgun",
      "name": "blowgun"
    },
    {
      "id": 636,
      "display_name": "Breastplate",
      "name": "breastplate"
    },
    {
      "id": 643,
      "display_name": "Brewer's Supplies",
      "name": "brewers-supplies"
    },
    {
      "id": 644,
      "display_name": "Calligrapher's Supplies",
      "name": "calligraphers-supplies"
    },
    {
      "id": 645,
      "display_name": "Carpenter's Tools",
      "name": "carpenters-tools"
    },
    {
      "id": 646,
      "display_name": "Cartographer's Tools",
      "name": "cartographers-tools"
    },
    {
      "id": 639,
      "display_name": "Chain Mail",
      "name": "chain-mail"
    },
    {
      "id": 635,
      "display_name": "Chain Shirt",
      "name": "chain-shirt"
    },
    {
      "id": 566,
      "display_name": "Charisma Ability Checks",
      "name": "charisma-ability-checks"
    },
    {
      "id": 559,
      "display_name": "Charisma Saving Throws",
      "name": "charisma-saving-throws"
    },
    {
      "id": 595,
      "display_name": "Club",
      "name": "club"
    },
    {
      "id": 647,
      "display_name": "Cobbler's Tools",
      "name": "cobblers-tools"
    },
    {
      "id": 563,
      "display_name": "Constitution Ability Checks",
      "name": "constitution-ability-checks"
    },
    {
      "id": 556,
      "display_name": "Constitution Saving Throws",
      "name": "constitution-saving-throws"
    },
    {
      "id": 648,
      "display_name": "Cook's Utensils",
      "name": "cooks-utensils"
    },
    {
      "id": 591,
      "display_name": "Crossbow, Hand",
      "name": "crossbow-hand"
    },
    {
      "id": 625,
      "display_name": "Crossbow, Heavy",
      "name": "crossbow-heavy"
    },
    {
      "id": 604,
      "display_name": "Crossbow, Light",
      "name": "crossbow-light"
    },
    {
      "id": 593,
      "display_name": "Dagger",
      "name": "dagger"
    },
    {
      "id": 605,
      "display_name": "Dart",
      "name": "dart"
    },
    {
      "id": 581,
      "display_name": "Deception",
      "name": "deception"
    },
    {
      "id": 562,
      "display_name": "Dexterity Ability Checks",
      "name": "dexterity-ability-checks"
    },
    {
      "id": 589,
      "display_name": "Dexterity Attacks",
      "name": "dexterity-attacks"
    },
    {
      "id": 555,
      "display_name": "Dexterity Saving Throws",
      "name": "dexterity-saving-throws"
    },
    {
      "id": 665,
      "display_name": "Dice Set",
      "name": "dice-set"
    },
    {
      "id": 659,
      "display_name": "Disguise Kit",
      "name": "disguise-kit"
    },
    {
      "id": 668,
      "display_name": "Drum",
      "name": "drum"
    },
    {
      "id": 669,
      "display_name": "Dulcimer",
      "name": "dulcimer"
    },
    {
      "id": 609,
      "display_name": "Flail",
      "name": "flail"
    },
    {
      "id": 670,
      "display_name": "Flute",
      "name": "flute"
    },
    {
      "id": 660,
      "display_name": "Forgery Kit",
      "name": "forgery-kit"
    },
    {
      "id": 592,
      "display_name": "Glaive",
      "name": "glaive"
    },
    {
      "id": 649,
      "display_name": "Glassblower's Tools",
      "name": "glassblowers-tools"
    },
    {
      "id": 610,
      "display_name": "Greataxe",
      "name": "greataxe"
    },
    {
      "id": 596,
      "display_name": "Greatclub",
      "name": "greatclub"
    },
    {
      "id": 611,
      "display_name": "Greatsword",
      "name": "greatsword"
    },
    {
      "id": 612,
      "display_name": "Halberd",
      "name": "halberd"
    },
    {
      "id": 637,
      "display_name": "Half Plate",
      "name": "half-plate"
    },
    {
      "id": 597,
      "display_name": "Handaxe",
      "name": "handaxe"
    },
    {
      "id": 681,
      "display_name": "Heavy Armor",
      "name": "heavy-armor"
    },
    {
      "id": 661,
      "display_name": "Herbalism Kit",
      "name": "herbalism-kit"
    },
    {
      "id": 634,
      "display_name": "Hide",
      "name": "hide"
    },
    {
      "id": 572,
      "display_name": "History",
      "name": "history"
    },
    {
      "id": 673,
      "display_name": "Horn",
      "name": "horn"
    },
    {
      "id": 590,
      "display_name": "Initiative",
      "name": "initiative"
    },
    {
      "id": 577,
      "display_name": "Insight",
      "name": "insight"
    },
    {
      "id": 564,
      "display_name": "Intelligence Ability Checks",
      "name": "intelligence-ability-checks"
    },
    {
      "id": 557,
      "display_name": "Intelligence Saving Throws",
      "name": "intelligence-saving-throws"
    },
    {
      "id": 582,
      "display_name": "Intimidation",
      "name": "intimidation"
    },
    {
      "id": 573,
      "display_name": "Investigation",
      "name": "investigation"
    },
    {
      "id": 598,
      "display_name": "Javelin",
      "name": "javelin"
    },
    {
      "id": 650,
      "display_name": "Jeweler's Tools",
      "name": "jewelers-tools"
    },
    {
      "id": 613,
      "display_name": "Lance",
      "name": "lance"
    },
    {
      "id": 633,
      "display_name": "Leather",
      "name": "leather"
    },
    {
      "id": 651,
      "display_name": "Leatherworker's Tools",
      "name": "leatherworkers-tools"
    },
    {
      "id": 679,
      "display_name": "Light Armor",
      "name": "light-armor"
    },
    {
      "id": 599,
      "display_name": "Light Hammer",
      "name": "light-hammer"
    },
    {
      "id": 626,
      "display_name": "Longbow",
      "name": "longbow"
    },
    {
      "id": 594,
      "display_name": "Longsword",
      "name": "longsword"
    },
    {
      "id": 671,
      "display_name": "Lute",
      "name": "lute"
    },
    {
      "id": 672,
      "display_name": "Lyre",
      "name": "lyre"
    },
    {
      "id": 600,
      "display_name": "Mace",
      "name": "mace"
    },
    {
      "id": 677,
      "display_name": "Martial Weapons",
      "name": "martial-weapons"
    },
    {
      "id": 652,
      "display_name": "Mason's Tools",
      "name": "masons-tools"
    },
    {
      "id": 614,
      "display_name": "Maul",
      "name": "maul"
    },
    {
      "id": 578,
      "display_name": "Medicine",
      "name": "medicine"
    },
    {
      "id": 680,
      "display_name": "Medium Armor",
      "name": "medium-armor"
    },
    {
      "id": 585,
      "display_name": "Melee Attacks",
      "name": "melee-attacks"
    },
    {
      "id": 630,
      "display_name": "Mithril Plate Armor",
      "name": "mithril-plate-armor"
    },
    {
      "id": 615,
      "display_name": "Morningstar",
      "name": "morningstar"
    },
    {
      "id": 574,
      "display_name": "Nature",
      "name": "nature"
    },
    {
      "id": 662,
      "display_name": "Navigator's Tools",
      "name": "navigators-tools"
    },
    {
      "id": 627,
      "display_name": "Net",
      "name": "net"
    },
    {
      "id": 632,
      "display_name": "Padded",
      "name": "padded"
    },
    {
      "id": 653,
      "display_name": "Painter's Supplies",
      "name": "painters-supplies"
    },
    {
      "id": 674,
      "display_name": "Pan Flute",
      "name": "pan-flute"
    },
    {
      "id": 579,
      "display_name": "Perception",
      "name": "perception"
    },
    {
      "id": 583,
      "display_name": "Performance",
      "name": "performance"
    },
    {
      "id": 584,
      "display_name": "Persuasion",
      "name": "persuasion"
    },
    {
      "id": 616,
      "display_name": "Pike",
      "name": "pike"
    },
    {
      "id": 641,
      "display_name": "Plate",
      "name": "plate"
    },
    {
      "id": 666,
      "display_name": "Playing Card Set",
      "name": "playing-card-set"
    },
    {
      "id": 663,
      "display_name": "Poisoner's Kit",
      "name": "poisoners-kit"
    },
    {
      "id": 654,
      "display_name": "Potter's Tools",
      "name": "potters-tools"
    },
    {
      "id": 601,
      "display_name": "Quarterstaff",
      "name": "quarterstaff"
    },
    {
      "id": 586,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 617,
      "display_name": "Rapier",
      "name": "rapier"
    },
    {
      "id": 575,
      "display_name": "Religion",
      "name": "religion"
    },
    {
      "id": 638,
      "display_name": "Ring Mail",
      "name": "ring-mail"
    },
    {
      "id": 553,
      "display_name": "Saving Throws",
      "name": "saving-throws"
    },
    {
      "id": 629,
      "display_name": "Scale Mail",
      "name": "scale-mail"
    },
    {
      "id": 618,
      "display_name": "Scimitar",
      "name": "scimitar"
    },
    {
      "id": 675,
      "display_name": "Shawm",
      "name": "shawm"
    },
    {
      "id": 606,
      "display_name": "Shortbow",
      "name": "shortbow"
    },
    {
      "id": 619,
      "display_name": "Shortsword",
      "name": "shortsword"
    },
    {
      "id": 602,
      "display_name": "Sickle",
      "name": "sickle"
    },
    {
      "id": 678,
      "display_name": "Simple Weapons",
      "name": "simple-weapons"
    },
    {
      "id": 569,
      "display_name": "Sleight of Hand",
      "name": "sleight-of-hand"
    },
    {
      "id": 607,
      "display_name": "Sling",
      "name": "sling"
    },
    {
      "id": 655,
      "display_name": "Smith's Tools",
      "name": "smiths-tools"
    },
    {
      "id": 603,
      "display_name": "Spear",
      "name": "spear"
    },
    {
      "id": 587,
      "display_name": "Spell Attacks",
      "name": "spell-attacks"
    },
    {
      "id": 640,
      "display_name": "Splint",
      "name": "splint"
    },
    {
      "id": 570,
      "display_name": "Stealth",
      "name": "stealth"
    },
    {
      "id": 631,
      "display_name": "Steel Shield",
      "name": "steel-shield"
    },
    {
      "id": 561,
      "display_name": "Strength Ability Checks",
      "name": "strength-ability-checks"
    },
    {
      "id": 588,
      "display_name": "Strength Attacks",
      "name": "strength-attacks"
    },
    {
      "id": 554,
      "display_name": "Strength Saving Throws",
      "name": "strength-saving-throws"
    },
    {
      "id": 628,
      "display_name": "Studded Leather",
      "name": "studded-leather"
    },
    {
      "id": 580,
      "display_name": "Survival",
      "name": "survival"
    },
    {
      "id": 664,
      "display_name": "Thieves' Tools",
      "name": "thieves-tools"
    },
    {
      "id": 656,
      "display_name": "Tinker's Tools",
      "name": "tinkers-tools"
    },
    {
      "id": 620,
      "display_name": "Trident",
      "name": "trident"
    },
    {
      "id": 676,
      "display_name": "Viol",
      "name": "viol"
    },
    {
      "id": 621,
      "display_name": "War pick",
      "name": "war-pick"
    },
    {
      "id": 622,
      "display_name": "Warhammer",
      "name": "warhammer"
    },
    {
      "id": 657,
      "display_name": "Weaver's Tools",
      "name": "weavers-tools"
    },
    {
      "id": 623,
      "display_name": "Whip",
      "name": "whip"
    },
    {
      "id": 565,
      "display_name": "Wisdom Ability Checks",
      "name": "wisdom-ability-checks"
    },
    {
      "id": 558,
      "display_name": "Wisdom Saving Throws",
      "name": "wisdom-saving-throws"
    },
    {
      "id": 658,
      "display_name": "Woodcarver's Tools",
      "name": "woodcarvers-tools"
    }
  ],
  "14": [
    {
      "id": 702,
      "display_name": "Actor",
      "name": "actor"
    },
    {
      "id": 703,
      "display_name": "Alert",
      "name": "alert"
    },
    {
      "id": 704,
      "display_name": "Athlete",
      "name": "athlete"
    },
    {
      "id": 705,
      "display_name": "Charger",
      "name": "charger"
    },
    {
      "id": 696,
      "display_name": "Choose a Feat",
      "name": "choose-a-feat"
    },
    {
      "id": 706,
      "display_name": "Crossbow Expert",
      "name": "crossbow-expert"
    },
    {
      "id": 707,
      "display_name": "Defensive Duelist",
      "name": "defensive-duelist"
    },
    {
      "id": 708,
      "display_name": "Dual Wielder",
      "name": "dual-wielder"
    },
    {
      "id": 709,
      "display_name": "Dungeon Delver",
      "name": "dungeon-delver"
    },
    {
      "id": 710,
      "display_name": "Durable",
      "name": "durable"
    },
    {
      "id": 711,
      "display_name": "Elemental Adept",
      "name": "elemental-adept"
    },
    {
      "id": 701,
      "display_name": "Grappler",
      "name": "grappler"
    },
    {
      "id": 712,
      "display_name": "Great Weapon Master",
      "name": "great-weapon-master"
    },
    {
      "id": 713,
      "display_name": "Healer",
      "name": "healer"
    },
    {
      "id": 714,
      "display_name": "Heavily Armored",
      "name": "heavily-armored"
    },
    {
      "id": 715,
      "display_name": "Heavy Armor Master",
      "name": "heavy-armor-master"
    },
    {
      "id": 716,
      "display_name": "Inspiring Leader",
      "name": "inspiring-leader"
    },
    {
      "id": 717,
      "display_name": "Keen Mind",
      "name": "keen-mind"
    },
    {
      "id": 718,
      "display_name": "Lightly Armored",
      "name": "lightly-armored"
    },
    {
      "id": 719,
      "display_name": "Linguist",
      "name": "linguist"
    },
    {
      "id": 720,
      "display_name": "Lucky",
      "name": "lucky"
    },
    {
      "id": 721,
      "display_name": "Mage Slayer",
      "name": "mage-slayer"
    },
    {
      "id": 722,
      "display_name": "Magic Initiate",
      "name": "magic-initiate"
    },
    {
      "id": 723,
      "display_name": "Martial Adept",
      "name": "martial-adept"
    },
    {
      "id": 724,
      "display_name": "Medium Armor Master",
      "name": "medium-armor-master"
    },
    {
      "id": 725,
      "display_name": "Mobile",
      "name": "mobile"
    },
    {
      "id": 726,
      "display_name": "Moderately Armored",
      "name": "moderately-armored"
    },
    {
      "id": 727,
      "display_name": "Mounted Combatant",
      "name": "mounted-combatant"
    },
    {
      "id": 728,
      "display_name": "Observant",
      "name": "observant"
    },
    {
      "id": 729,
      "display_name": "Polearm Master",
      "name": "polearm-master"
    },
    {
      "id": 730,
      "display_name": "Resilient",
      "name": "resilient"
    },
    {
      "id": 731,
      "display_name": "Ritual Caster",
      "name": "ritual-caster"
    },
    {
      "id": 743,
      "display_name": "Savage Attacker",
      "name": "savage-attacker"
    },
    {
      "id": 732,
      "display_name": "Sentinel",
      "name": "sentinel"
    },
    {
      "id": 733,
      "display_name": "Sharpshooter",
      "name": "sharpshooter"
    },
    {
      "id": 734,
      "display_name": "Shield Master",
      "name": "shield-master"
    },
    {
      "id": 735,
      "display_name": "Skilled",
      "name": "skilled"
    },
    {
      "id": 736,
      "display_name": "Skulker",
      "name": "skulker"
    },
    {
      "id": 737,
      "display_name": "Spell Sniper",
      "name": "spell-sniper"
    },
    {
      "id": 738,
      "display_name": "Svirfneblin Magic",
      "name": "svirfneblin-magic"
    },
    {
      "id": 739,
      "display_name": "Tavern Brawler",
      "name": "tavern-brawler"
    },
    {
      "id": 740,
      "display_name": "Tough",
      "name": "tough"
    },
    {
      "id": 741,
      "display_name": "War Caster",
      "name": "war-caster"
    },
    {
      "id": 742,
      "display_name": "Weapon Master",
      "name": "weapon-master"
    }
  ],
  "17": [
    {
      "id": 750,
      "display_name": "Gargantuan",
      "name": "gargantuan"
    },
    {
      "id": 749,
      "display_name": "Huge",
      "name": "huge"
    },
    {
      "id": 748,
      "display_name": "Large",
      "name": "large"
    },
    {
      "id": 747,
      "display_name": "Medium",
      "name": "medium"
    },
    {
      "id": 842,
      "display_name": "Multiplier",
      "name": "multiplier"
    },
    {
      "id": 746,
      "display_name": "Small",
      "name": "small"
    },
    {
      "id": 745,
      "display_name": "Tiny",
      "name": "tiny"
    }
  ],
  "19": [
    {
      "id": 755,
      "display_name": "Bludgeoning",
      "name": "bludgeoning"
    },
    {
      "id": 756,
      "display_name": "Piercing",
      "name": "piercing"
    },
    {
      "id": 754,
      "display_name": "Slashing",
      "name": "slashing"
    }
  ],
  "23": [
    {
      "id": 772,
      "display_name": "Impose",
      "name": "impose"
    },
    {
      "id": 771,
      "display_name": "Remove",
      "name": "remove"
    }
  ],
  "24": [
    {
      "id": 813,
      "display_name": "Impose",
      "name": "impose"
    },
    {
      "id": 812,
      "display_name": "Remove",
      "name": "remove"
    }
  ],
  "25": [
    {
      "id": 815,
      "display_name": "Acid",
      "name": "acid"
    },
    {
      "id": 816,
      "display_name": "Bludgeoning",
      "name": "bludgeoning"
    },
    {
      "id": 817,
      "display_name": "Cold",
      "name": "cold"
    },
    {
      "id": 818,
      "display_name": "Fire",
      "name": "fire"
    },
    {
      "id": 819,
      "display_name": "Force",
      "name": "force"
    },
    {
      "id": 820,
      "display_name": "Lightning",
      "name": "lightning"
    },
    {
      "id": 821,
      "display_name": "Necrotic",
      "name": "necrotic"
    },
    {
      "id": 822,
      "display_name": "Piercing",
      "name": "piercing"
    },
    {
      "id": 823,
      "display_name": "Poison",
      "name": "poison"
    },
    {
      "id": 824,
      "display_name": "Psychic",
      "name": "psychic"
    },
    {
      "id": 825,
      "display_name": "Radiant",
      "name": "radiant"
    },
    {
      "id": 826,
      "display_name": "Slashing",
      "name": "slashing"
    },
    {
      "id": 827,
      "display_name": "Thunder",
      "name": "thunder"
    }
  ],
  "26": [
    {
      "id": 828,
      "display_name": "Acid",
      "name": "acid"
    },
    {
      "id": 829,
      "display_name": "Bludgeoning",
      "name": "bludgeoning"
    },
    {
      "id": 830,
      "display_name": "Cold",
      "name": "cold"
    },
    {
      "id": 831,
      "display_name": "Fire",
      "name": "fire"
    },
    {
      "id": 832,
      "display_name": "Force",
      "name": "force"
    },
    {
      "id": 833,
      "display_name": "Lightning",
      "name": "lightning"
    },
    {
      "id": 834,
      "display_name": "Necrotic",
      "name": "necrotic"
    },
    {
      "id": 835,
      "display_name": "Piercing",
      "name": "piercing"
    },
    {
      "id": 836,
      "display_name": "Poison",
      "name": "poison"
    },
    {
      "id": 837,
      "display_name": "Psychic",
      "name": "psychic"
    },
    {
      "id": 838,
      "display_name": "Radiant",
      "name": "radiant"
    },
    {
      "id": 839,
      "display_name": "Slashing",
      "name": "slashing"
    },
    {
      "id": 840,
      "display_name": "Thunder",
      "name": "thunder"
    }
  ],
  "28": [
    {
      "id": 861,
      "display_name": "Finesse",
      "name": "finesse"
    },
    {
      "id": 865,
      "display_name": "Heavy",
      "name": "heavy"
    },
    {
      "id": 866,
      "display_name": "Light",
      "name": "light"
    },
    {
      "id": 867,
      "display_name": "Loading",
      "name": "loading"
    },
    {
      "id": 868,
      "display_name": "Range",
      "name": "range"
    },
    {
      "id": 869,
      "display_name": "Reach",
      "name": "reach"
    },
    {
      "id": 862,
      "display_name": "Thrown",
      "name": "thrown"
    },
    {
      "id": 864,
      "display_name": "Two-Handed",
      "name": "twohanded"
    },
    {
      "id": 863,
      "display_name": "Versatile",
      "name": "versatile"
    }
  ],
  "29": [
    {
      "id": 877,
      "display_name": "Ability Checks",
      "name": "ability-checks"
    },
    {
      "id": 885,
      "display_name": "Acrobatics",
      "name": "acrobatics"
    },
    {
      "id": 959,
      "display_name": "Alchemist's Supplies",
      "name": "alchemists-supplies"
    },
    {
      "id": 893,
      "display_name": "Animal Handling",
      "name": "animal-handling"
    },
    {
      "id": 888,
      "display_name": "Arcana",
      "name": "arcana"
    },
    {
      "id": 884,
      "display_name": "Athletics",
      "name": "athletics"
    },
    {
      "id": 984,
      "display_name": "Bagpipes",
      "name": "bagpipes"
    },
    {
      "id": 925,
      "display_name": "Battleaxe",
      "name": "battleaxe"
    },
    {
      "id": 941,
      "display_name": "Blowgun",
      "name": "blowgun"
    },
    {
      "id": 953,
      "display_name": "Breastplate",
      "name": "breastplate"
    },
    {
      "id": 960,
      "display_name": "Brewer's Supplies",
      "name": "brewers-supplies"
    },
    {
      "id": 961,
      "display_name": "Calligrapher's Supplies",
      "name": "calligraphers-supplies"
    },
    {
      "id": 962,
      "display_name": "Carpenter's Tools",
      "name": "carpenters-tools"
    },
    {
      "id": 963,
      "display_name": "Cartographer's Tools",
      "name": "cartographers-tools"
    },
    {
      "id": 956,
      "display_name": "Chain Mail",
      "name": "chain-mail"
    },
    {
      "id": 952,
      "display_name": "Chain Shirt",
      "name": "chain-shirt"
    },
    {
      "id": 883,
      "display_name": "Charisma Ability Checks",
      "name": "charisma-ability-checks"
    },
    {
      "id": 876,
      "display_name": "Charisma Saving Throws",
      "name": "charisma-saving-throws"
    },
    {
      "id": 912,
      "display_name": "Club",
      "name": "club"
    },
    {
      "id": 964,
      "display_name": "Cobbler's Tools",
      "name": "cobblers-tools"
    },
    {
      "id": 880,
      "display_name": "Constitution Ability Checks",
      "name": "constitution-ability-checks"
    },
    {
      "id": 873,
      "display_name": "Constitution Saving Throws",
      "name": "constitution-saving-throws"
    },
    {
      "id": 965,
      "display_name": "Cook's Utensils",
      "name": "cooks-utensils"
    },
    {
      "id": 908,
      "display_name": "Crossbow, Hand",
      "name": "crossbow-hand"
    },
    {
      "id": 942,
      "display_name": "Crossbow, Heavy",
      "name": "crossbow-heavy"
    },
    {
      "id": 921,
      "display_name": "Crossbow, Light",
      "name": "crossbow-light"
    },
    {
      "id": 910,
      "display_name": "Dagger",
      "name": "dagger"
    },
    {
      "id": 922,
      "display_name": "Dart",
      "name": "dart"
    },
    {
      "id": 898,
      "display_name": "Deception",
      "name": "deception"
    },
    {
      "id": 879,
      "display_name": "Dexterity Ability Checks",
      "name": "dexterity-ability-checks"
    },
    {
      "id": 906,
      "display_name": "Dexterity Attacks",
      "name": "dexterity-attacks"
    },
    {
      "id": 872,
      "display_name": "Dexterity Saving Throws",
      "name": "dexterity-saving-throws"
    },
    {
      "id": 982,
      "display_name": "Dice Set",
      "name": "dice-set"
    },
    {
      "id": 976,
      "display_name": "Disguise Kit",
      "name": "disguise-kit"
    },
    {
      "id": 985,
      "display_name": "Drum",
      "name": "drum"
    },
    {
      "id": 986,
      "display_name": "Dulcimer",
      "name": "dulcimer"
    },
    {
      "id": 926,
      "display_name": "Flail",
      "name": "flail"
    },
    {
      "id": 987,
      "display_name": "Flute",
      "name": "flute"
    },
    {
      "id": 977,
      "display_name": "Forgery Kit",
      "name": "forgery-kit"
    },
    {
      "id": 909,
      "display_name": "Glaive",
      "name": "glaive"
    },
    {
      "id": 966,
      "display_name": "Glassblower's Tools",
      "name": "glassblowers-tools"
    },
    {
      "id": 927,
      "display_name": "Greataxe",
      "name": "greataxe"
    },
    {
      "id": 913,
      "display_name": "Greatclub",
      "name": "greatclub"
    },
    {
      "id": 928,
      "display_name": "Greatsword",
      "name": "greatsword"
    },
    {
      "id": 929,
      "display_name": "Halberd",
      "name": "halberd"
    },
    {
      "id": 954,
      "display_name": "Half Plate",
      "name": "half-plate"
    },
    {
      "id": 914,
      "display_name": "Handaxe",
      "name": "handaxe"
    },
    {
      "id": 998,
      "display_name": "Heavy Armor",
      "name": "heavy-armor"
    },
    {
      "id": 978,
      "display_name": "Herbalism Kit",
      "name": "herbalism-kit"
    },
    {
      "id": 951,
      "display_name": "Hide",
      "name": "hide"
    },
    {
      "id": 889,
      "display_name": "History",
      "name": "history"
    },
    {
      "id": 990,
      "display_name": "Horn",
      "name": "horn"
    },
    {
      "id": 1000,
      "display_name": "Improvised Weapons",
      "name": "improvised-weapons"
    },
    {
      "id": 907,
      "display_name": "Initiative",
      "name": "initiative"
    },
    {
      "id": 894,
      "display_name": "Insight",
      "name": "insight"
    },
    {
      "id": 881,
      "display_name": "Intelligence Ability Checks",
      "name": "intelligence-ability-checks"
    },
    {
      "id": 874,
      "display_name": "Intelligence Saving Throws",
      "name": "intelligence-saving-throws"
    },
    {
      "id": 899,
      "display_name": "Intimidation",
      "name": "intimidation"
    },
    {
      "id": 890,
      "display_name": "Investigation",
      "name": "investigation"
    },
    {
      "id": 915,
      "display_name": "Javelin",
      "name": "javelin"
    },
    {
      "id": 967,
      "display_name": "Jeweler's Tools",
      "name": "jewelers-tools"
    },
    {
      "id": 930,
      "display_name": "Lance",
      "name": "lance"
    },
    {
      "id": 950,
      "display_name": "Leather",
      "name": "leather"
    },
    {
      "id": 968,
      "display_name": "Leatherworker's Tools",
      "name": "leatherworkers-tools"
    },
    {
      "id": 996,
      "display_name": "Light Armor",
      "name": "light-armor"
    },
    {
      "id": 916,
      "display_name": "Light Hammer",
      "name": "light-hammer"
    },
    {
      "id": 943,
      "display_name": "Longbow",
      "name": "longbow"
    },
    {
      "id": 911,
      "display_name": "Longsword",
      "name": "longsword"
    },
    {
      "id": 988,
      "display_name": "Lute",
      "name": "lute"
    },
    {
      "id": 989,
      "display_name": "Lyre",
      "name": "lyre"
    },
    {
      "id": 917,
      "display_name": "Mace",
      "name": "mace"
    },
    {
      "id": 994,
      "display_name": "Martial Weapons",
      "name": "martial-weapons"
    },
    {
      "id": 969,
      "display_name": "Mason's Tools",
      "name": "masons-tools"
    },
    {
      "id": 931,
      "display_name": "Maul",
      "name": "maul"
    },
    {
      "id": 895,
      "display_name": "Medicine",
      "name": "medicine"
    },
    {
      "id": 997,
      "display_name": "Medium Armor",
      "name": "medium-armor"
    },
    {
      "id": 902,
      "display_name": "Melee Attacks",
      "name": "melee-attacks"
    },
    {
      "id": 947,
      "display_name": "Mithril Plate Armor",
      "name": "mithril-plate-armor"
    },
    {
      "id": 932,
      "display_name": "Morningstar",
      "name": "morningstar"
    },
    {
      "id": 891,
      "display_name": "Nature",
      "name": "nature"
    },
    {
      "id": 979,
      "display_name": "Navigator's Tools",
      "name": "navigators-tools"
    },
    {
      "id": 944,
      "display_name": "Net",
      "name": "net"
    },
    {
      "id": 949,
      "display_name": "Padded",
      "name": "padded"
    },
    {
      "id": 970,
      "display_name": "Painter's Supplies",
      "name": "painters-supplies"
    },
    {
      "id": 991,
      "display_name": "Pan Flute",
      "name": "pan-flute"
    },
    {
      "id": 896,
      "display_name": "Perception",
      "name": "perception"
    },
    {
      "id": 900,
      "display_name": "Performance",
      "name": "performance"
    },
    {
      "id": 901,
      "display_name": "Persuasion",
      "name": "persuasion"
    },
    {
      "id": 933,
      "display_name": "Pike",
      "name": "pike"
    },
    {
      "id": 958,
      "display_name": "Plate",
      "name": "plate"
    },
    {
      "id": 983,
      "display_name": "Playing Card Set",
      "name": "playing-card-set"
    },
    {
      "id": 980,
      "display_name": "Poisoner's Kit",
      "name": "poisoners-kit"
    },
    {
      "id": 971,
      "display_name": "Potter's Tools",
      "name": "potters-tools"
    },
    {
      "id": 918,
      "display_name": "Quarterstaff",
      "name": "quarterstaff"
    },
    {
      "id": 903,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 934,
      "display_name": "Rapier",
      "name": "rapier"
    },
    {
      "id": 892,
      "display_name": "Religion",
      "name": "religion"
    },
    {
      "id": 955,
      "display_name": "Ring Mail",
      "name": "ring-mail"
    },
    {
      "id": 870,
      "display_name": "Saving Throws",
      "name": "saving-throws"
    },
    {
      "id": 946,
      "display_name": "Scale Mail",
      "name": "scale-mail"
    },
    {
      "id": 935,
      "display_name": "Scimitar",
      "name": "scimitar"
    },
    {
      "id": 992,
      "display_name": "Shawm",
      "name": "shawm"
    },
    {
      "id": 999,
      "display_name": "Shields",
      "name": "shields"
    },
    {
      "id": 923,
      "display_name": "Shortbow",
      "name": "shortbow"
    },
    {
      "id": 936,
      "display_name": "Shortsword",
      "name": "shortsword"
    },
    {
      "id": 919,
      "display_name": "Sickle",
      "name": "sickle"
    },
    {
      "id": 995,
      "display_name": "Simple Weapons",
      "name": "simple-weapons"
    },
    {
      "id": 886,
      "display_name": "Sleight of Hand",
      "name": "sleight-of-hand"
    },
    {
      "id": 924,
      "display_name": "Sling",
      "name": "sling"
    },
    {
      "id": 972,
      "display_name": "Smith's Tools",
      "name": "smiths-tools"
    },
    {
      "id": 920,
      "display_name": "Spear",
      "name": "spear"
    },
    {
      "id": 904,
      "display_name": "Spell Attacks",
      "name": "spell-attacks"
    },
    {
      "id": 957,
      "display_name": "Splint",
      "name": "splint"
    },
    {
      "id": 887,
      "display_name": "Stealth",
      "name": "stealth"
    },
    {
      "id": 948,
      "display_name": "Steel Shield",
      "name": "steel-shield"
    },
    {
      "id": 878,
      "display_name": "Strength Ability Checks",
      "name": "strength-ability-checks"
    },
    {
      "id": 905,
      "display_name": "Strength Attacks",
      "name": "strength-attacks"
    },
    {
      "id": 871,
      "display_name": "Strength Saving Throws",
      "name": "strength-saving-throws"
    },
    {
      "id": 945,
      "display_name": "Studded Leather",
      "name": "studded-leather"
    },
    {
      "id": 897,
      "display_name": "Survival",
      "name": "survival"
    },
    {
      "id": 981,
      "display_name": "Thieves' Tools",
      "name": "thieves-tools"
    },
    {
      "id": 973,
      "display_name": "Tinker's Tools",
      "name": "tinkers-tools"
    },
    {
      "id": 937,
      "display_name": "Trident",
      "name": "trident"
    },
    {
      "id": 1001,
      "display_name": "Vehicles (Land)",
      "name": "vehicles-land"
    },
    {
      "id": 1002,
      "display_name": "Vehicles (Water)",
      "name": "vehicles-water"
    },
    {
      "id": 993,
      "display_name": "Viol",
      "name": "viol"
    },
    {
      "id": 938,
      "display_name": "War pick",
      "name": "war-pick"
    },
    {
      "id": 939,
      "display_name": "Warhammer",
      "name": "warhammer"
    },
    {
      "id": 974,
      "display_name": "Weaver's Tools",
      "name": "weavers-tools"
    },
    {
      "id": 940,
      "display_name": "Whip",
      "name": "whip"
    },
    {
      "id": 882,
      "display_name": "Wisdom Ability Checks",
      "name": "wisdom-ability-checks"
    },
    {
      "id": 875,
      "display_name": "Wisdom Saving Throws",
      "name": "wisdom-saving-throws"
    },
    {
      "id": 975,
      "display_name": "Woodcarver's Tools",
      "name": "woodcarvers-tools"
    }
  ],
  "30": [
    {
      "id": 1007,
      "display_name": "Aberrations",
      "name": "aberrations"
    },
    {
      "id": 1008,
      "display_name": "Beasts",
      "name": "beasts"
    },
    {
      "id": 1009,
      "display_name": "Celestials",
      "name": "celestials"
    },
    {
      "id": 1010,
      "display_name": "Constructs",
      "name": "constructs"
    },
    {
      "id": 1025,
      "display_name": "Dragonborn",
      "name": "dragonborn"
    },
    {
      "id": 1011,
      "display_name": "Dragons",
      "name": "dragons"
    },
    {
      "id": 1021,
      "display_name": "Dwarves",
      "name": "dwarves"
    },
    {
      "id": 1012,
      "display_name": "Elementals",
      "name": "elementals"
    },
    {
      "id": 1022,
      "display_name": "Elves",
      "name": "elves"
    },
    {
      "id": 1013,
      "display_name": "Fey",
      "name": "fey"
    },
    {
      "id": 1014,
      "display_name": "Fiends",
      "name": "fiends"
    },
    {
      "id": 1015,
      "display_name": "Giants",
      "name": "giants"
    },
    {
      "id": 1030,
      "display_name": "Gnolls",
      "name": "gnolls"
    },
    {
      "id": 1024,
      "display_name": "Gnomes",
      "name": "gnomes"
    },
    {
      "id": 1028,
      "display_name": "Goblinoids",
      "name": "goblinoids"
    },
    {
      "id": 1026,
      "display_name": "Grimlock",
      "name": "grimlock"
    },
    {
      "id": 1023,
      "display_name": "Halflings",
      "name": "halflings"
    },
    {
      "id": 1020,
      "display_name": "Humans",
      "name": "humans"
    },
    {
      "id": 1032,
      "display_name": "Kobolds",
      "name": "kobolds"
    },
    {
      "id": 1031,
      "display_name": "Lizardfolk",
      "name": "lizardfolk"
    },
    {
      "id": 1027,
      "display_name": "Merfolk",
      "name": "merfolk"
    },
    {
      "id": 1016,
      "display_name": "Monstrosities",
      "name": "monstrosities"
    },
    {
      "id": 1017,
      "display_name": "Oozes",
      "name": "oozes"
    },
    {
      "id": 1029,
      "display_name": "Orcs",
      "name": "orcs"
    },
    {
      "id": 1018,
      "display_name": "Plants",
      "name": "plants"
    },
    {
      "id": 1033,
      "display_name": "Sahuagin",
      "name": "sahuagin"
    },
    {
      "id": 1019,
      "display_name": "Undead",
      "name": "undead"
    },
    {
      "id": 1034,
      "display_name": "Werebears",
      "name": "werebears"
    },
    {
      "id": 1035,
      "display_name": "Wereboars",
      "name": "wereboars"
    },
    {
      "id": 1036,
      "display_name": "Wererats",
      "name": "wererats"
    },
    {
      "id": 1037,
      "display_name": "Werewolves",
      "name": "werewolves"
    }
  ],
  "31": [
    {
      "id": 1040,
      "display_name": "Dual Wield Light Restrictions",
      "name": "dual-wield-light-restrictions"
    },
    {
      "id": 1041,
      "display_name": "Dual Wield Modifier Restrictions",
      "name": "dual-wield-modifier-restrictions"
    },
    {
      "id": 1789,
      "display_name": "Heavy Armor Speed Reduction",
      "name": "heavy-armor-speed-reduction"
    },
    {
      "id": 1055,
      "display_name": "Unarmored Dex AC Bonus",
      "name": "unarmored-dex-ac-bonus"
    },
    {
      "id": 1728,
      "display_name": "Unarmored Dex Natural AC Bonus",
      "name": "unarmored-dex-natural-ac-bonus"
    }
  ],
  "32": [
    {
      "id": 1052,
      "display_name": "Bonus Damage",
      "name": "bonus-damage"
    },
    {
      "id": 1053,
      "display_name": "Bonus Range",
      "name": "bonus-range"
    }
  ],
  "33": [
    {
      "id": 1700,
      "display_name": "Acid",
      "name": "acid"
    },
    {
      "id": 1702,
      "display_name": "Bludgeoning",
      "name": "bludgeoning"
    },
    {
      "id": 1701,
      "display_name": "Cold",
      "name": "cold"
    },
    {
      "id": 1703,
      "display_name": "Fire",
      "name": "fire"
    },
    {
      "id": 1704,
      "display_name": "Force",
      "name": "force"
    },
    {
      "id": 1705,
      "display_name": "Lightning",
      "name": "lightning"
    },
    {
      "id": 1706,
      "display_name": "Necrotic",
      "name": "necrotic"
    },
    {
      "id": 1707,
      "display_name": "Piercing",
      "name": "piercing"
    },
    {
      "id": 1708,
      "display_name": "Poison",
      "name": "poison"
    },
    {
      "id": 1709,
      "display_name": "Psychic",
      "name": "psychic"
    },
    {
      "id": 1056,
      "display_name": "Radiant",
      "name": "radiant"
    },
    {
      "id": 1710,
      "display_name": "Slashing",
      "name": "slashing"
    },
    {
      "id": 1711,
      "display_name": "Thunder",
      "name": "thunder"
    }
  ],
  "34": [
    {
      "id": 1398,
      "display_name": "Ability Checks",
      "name": "ability-checks"
    },
    {
      "id": 1406,
      "display_name": "Acrobatics",
      "name": "acrobatics"
    },
    {
      "id": 1480,
      "display_name": "Alchemist's Supplies",
      "name": "alchemists-supplies"
    },
    {
      "id": 1414,
      "display_name": "Animal Handling",
      "name": "animal-handling"
    },
    {
      "id": 1409,
      "display_name": "Arcana",
      "name": "arcana"
    },
    {
      "id": 1405,
      "display_name": "Athletics",
      "name": "athletics"
    },
    {
      "id": 1505,
      "display_name": "Bagpipes",
      "name": "bagpipes"
    },
    {
      "id": 1446,
      "display_name": "Battleaxe",
      "name": "battleaxe"
    },
    {
      "id": 1462,
      "display_name": "Blowgun",
      "name": "blowgun"
    },
    {
      "id": 1474,
      "display_name": "Breastplate",
      "name": "breastplate"
    },
    {
      "id": 1481,
      "display_name": "Brewer's Supplies",
      "name": "brewers-supplies"
    },
    {
      "id": 1482,
      "display_name": "Calligrapher's Supplies",
      "name": "calligraphers-supplies"
    },
    {
      "id": 1483,
      "display_name": "Carpenter's Tools",
      "name": "carpenters-tools"
    },
    {
      "id": 1484,
      "display_name": "Cartographer's Tools",
      "name": "cartographers-tools"
    },
    {
      "id": 1477,
      "display_name": "Chain Mail",
      "name": "chain-mail"
    },
    {
      "id": 1473,
      "display_name": "Chain Shirt",
      "name": "chain-shirt"
    },
    {
      "id": 1404,
      "display_name": "Charisma Ability Checks",
      "name": "charisma-ability-checks"
    },
    {
      "id": 1397,
      "display_name": "Charisma Saving Throws",
      "name": "charisma-saving-throws"
    },
    {
      "id": 1560,
      "display_name": "Choose a Dwarf Artisan's Tool",
      "name": "choose-a-dwarf-artisans-tool"
    },
    {
      "id": 1433,
      "display_name": "Club",
      "name": "club"
    },
    {
      "id": 1485,
      "display_name": "Cobbler's Tools",
      "name": "cobblers-tools"
    },
    {
      "id": 1401,
      "display_name": "Constitution Ability Checks",
      "name": "constitution-ability-checks"
    },
    {
      "id": 1394,
      "display_name": "Constitution Saving Throws",
      "name": "constitution-saving-throws"
    },
    {
      "id": 1486,
      "display_name": "Cook's Utensils",
      "name": "cooks-utensils"
    },
    {
      "id": 1429,
      "display_name": "Crossbow, Hand",
      "name": "crossbow-hand"
    },
    {
      "id": 1463,
      "display_name": "Crossbow, Heavy",
      "name": "crossbow-heavy"
    },
    {
      "id": 1442,
      "display_name": "Crossbow, Light",
      "name": "crossbow-light"
    },
    {
      "id": 1431,
      "display_name": "Dagger",
      "name": "dagger"
    },
    {
      "id": 1443,
      "display_name": "Dart",
      "name": "dart"
    },
    {
      "id": 1419,
      "display_name": "Deception",
      "name": "deception"
    },
    {
      "id": 1400,
      "display_name": "Dexterity Ability Checks",
      "name": "dexterity-ability-checks"
    },
    {
      "id": 1427,
      "display_name": "Dexterity Attacks",
      "name": "dexterity-attacks"
    },
    {
      "id": 1393,
      "display_name": "Dexterity Saving Throws",
      "name": "dexterity-saving-throws"
    },
    {
      "id": 1503,
      "display_name": "Dice Set",
      "name": "dice-set"
    },
    {
      "id": 1497,
      "display_name": "Disguise Kit",
      "name": "disguise-kit"
    },
    {
      "id": 1566,
      "display_name": "Dragonchess Set",
      "name": "dragonchess-set"
    },
    {
      "id": 1506,
      "display_name": "Drum",
      "name": "drum"
    },
    {
      "id": 1507,
      "display_name": "Dulcimer",
      "name": "dulcimer"
    },
    {
      "id": 1447,
      "display_name": "Flail",
      "name": "flail"
    },
    {
      "id": 1508,
      "display_name": "Flute",
      "name": "flute"
    },
    {
      "id": 1498,
      "display_name": "Forgery Kit",
      "name": "forgery-kit"
    },
    {
      "id": 1430,
      "display_name": "Glaive",
      "name": "glaive"
    },
    {
      "id": 1487,
      "display_name": "Glassblower's Tools",
      "name": "glassblowers-tools"
    },
    {
      "id": 1448,
      "display_name": "Greataxe",
      "name": "greataxe"
    },
    {
      "id": 1434,
      "display_name": "Greatclub",
      "name": "greatclub"
    },
    {
      "id": 1449,
      "display_name": "Greatsword",
      "name": "greatsword"
    },
    {
      "id": 1450,
      "display_name": "Halberd",
      "name": "halberd"
    },
    {
      "id": 1475,
      "display_name": "Half Plate",
      "name": "half-plate"
    },
    {
      "id": 1435,
      "display_name": "Handaxe",
      "name": "handaxe"
    },
    {
      "id": 1519,
      "display_name": "Heavy Armor",
      "name": "heavy-armor"
    },
    {
      "id": 1499,
      "display_name": "Herbalism Kit",
      "name": "herbalism-kit"
    },
    {
      "id": 1472,
      "display_name": "Hide",
      "name": "hide"
    },
    {
      "id": 1410,
      "display_name": "History",
      "name": "history"
    },
    {
      "id": 1511,
      "display_name": "Horn",
      "name": "horn"
    },
    {
      "id": 1538,
      "display_name": "Improvised Weapons",
      "name": "improvised-weapons"
    },
    {
      "id": 1428,
      "display_name": "Initiative",
      "name": "initiative"
    },
    {
      "id": 1415,
      "display_name": "Insight",
      "name": "insight"
    },
    {
      "id": 1402,
      "display_name": "Intelligence Ability Checks",
      "name": "intelligence-ability-checks"
    },
    {
      "id": 1395,
      "display_name": "Intelligence Saving Throws",
      "name": "intelligence-saving-throws"
    },
    {
      "id": 1420,
      "display_name": "Intimidation",
      "name": "intimidation"
    },
    {
      "id": 1411,
      "display_name": "Investigation",
      "name": "investigation"
    },
    {
      "id": 1436,
      "display_name": "Javelin",
      "name": "javelin"
    },
    {
      "id": 1488,
      "display_name": "Jeweler's Tools",
      "name": "jewelers-tools"
    },
    {
      "id": 1451,
      "display_name": "Lance",
      "name": "lance"
    },
    {
      "id": 1471,
      "display_name": "Leather",
      "name": "leather"
    },
    {
      "id": 1489,
      "display_name": "Leatherworker's Tools",
      "name": "leatherworkers-tools"
    },
    {
      "id": 1517,
      "display_name": "Light Armor",
      "name": "light-armor"
    },
    {
      "id": 1437,
      "display_name": "Light Hammer",
      "name": "light-hammer"
    },
    {
      "id": 1464,
      "display_name": "Longbow",
      "name": "longbow"
    },
    {
      "id": 1432,
      "display_name": "Longsword",
      "name": "longsword"
    },
    {
      "id": 1509,
      "display_name": "Lute",
      "name": "lute"
    },
    {
      "id": 1510,
      "display_name": "Lyre",
      "name": "lyre"
    },
    {
      "id": 1438,
      "display_name": "Mace",
      "name": "mace"
    },
    {
      "id": 1515,
      "display_name": "Martial Weapons",
      "name": "martial-weapons"
    },
    {
      "id": 1490,
      "display_name": "Mason's Tools",
      "name": "masons-tools"
    },
    {
      "id": 1452,
      "display_name": "Maul",
      "name": "maul"
    },
    {
      "id": 1416,
      "display_name": "Medicine",
      "name": "medicine"
    },
    {
      "id": 1518,
      "display_name": "Medium Armor",
      "name": "medium-armor"
    },
    {
      "id": 1423,
      "display_name": "Melee Attacks",
      "name": "melee-attacks"
    },
    {
      "id": 1468,
      "display_name": "Mithril Plate Armor",
      "name": "mithril-plate-armor"
    },
    {
      "id": 1453,
      "display_name": "Morningstar",
      "name": "morningstar"
    },
    {
      "id": 1412,
      "display_name": "Nature",
      "name": "nature"
    },
    {
      "id": 1500,
      "display_name": "Navigator's Tools",
      "name": "navigators-tools"
    },
    {
      "id": 1465,
      "display_name": "Net",
      "name": "net"
    },
    {
      "id": 1470,
      "display_name": "Padded",
      "name": "padded"
    },
    {
      "id": 1491,
      "display_name": "Painter's Supplies",
      "name": "painters-supplies"
    },
    {
      "id": 1512,
      "display_name": "Pan Flute",
      "name": "pan-flute"
    },
    {
      "id": 1417,
      "display_name": "Perception",
      "name": "perception"
    },
    {
      "id": 1421,
      "display_name": "Performance",
      "name": "performance"
    },
    {
      "id": 1422,
      "display_name": "Persuasion",
      "name": "persuasion"
    },
    {
      "id": 1454,
      "display_name": "Pike",
      "name": "pike"
    },
    {
      "id": 1479,
      "display_name": "Plate",
      "name": "plate"
    },
    {
      "id": 1504,
      "display_name": "Playing Card Set",
      "name": "playing-card-set"
    },
    {
      "id": 1501,
      "display_name": "Poisoner's Kit",
      "name": "poisoners-kit"
    },
    {
      "id": 1492,
      "display_name": "Potter's Tools",
      "name": "potters-tools"
    },
    {
      "id": 1439,
      "display_name": "Quarterstaff",
      "name": "quarterstaff"
    },
    {
      "id": 1424,
      "display_name": "Ranged Attacks",
      "name": "ranged-attacks"
    },
    {
      "id": 1455,
      "display_name": "Rapier",
      "name": "rapier"
    },
    {
      "id": 1413,
      "display_name": "Religion",
      "name": "religion"
    },
    {
      "id": 1476,
      "display_name": "Ring Mail",
      "name": "ring-mail"
    },
    {
      "id": 1391,
      "display_name": "Saving Throws",
      "name": "saving-throws"
    },
    {
      "id": 1467,
      "display_name": "Scale Mail",
      "name": "scale-mail"
    },
    {
      "id": 1456,
      "display_name": "Scimitar",
      "name": "scimitar"
    },
    {
      "id": 1513,
      "display_name": "Shawm",
      "name": "shawm"
    },
    {
      "id": 1532,
      "display_name": "Shields",
      "name": "shields"
    },
    {
      "id": 1444,
      "display_name": "Shortbow",
      "name": "shortbow"
    },
    {
      "id": 1457,
      "display_name": "Shortsword",
      "name": "shortsword"
    },
    {
      "id": 1440,
      "display_name": "Sickle",
      "name": "sickle"
    },
    {
      "id": 1516,
      "display_name": "Simple Weapons",
      "name": "simple-weapons"
    },
    {
      "id": 1407,
      "display_name": "Sleight of Hand",
      "name": "sleight-of-hand"
    },
    {
      "id": 1445,
      "display_name": "Sling",
      "name": "sling"
    },
    {
      "id": 1493,
      "display_name": "Smith's Tools",
      "name": "smiths-tools"
    },
    {
      "id": 1441,
      "display_name": "Spear",
      "name": "spear"
    },
    {
      "id": 1425,
      "display_name": "Spell Attacks",
      "name": "spell-attacks"
    },
    {
      "id": 1478,
      "display_name": "Splint",
      "name": "splint"
    },
    {
      "id": 1408,
      "display_name": "Stealth",
      "name": "stealth"
    },
    {
      "id": 1469,
      "display_name": "Steel Shield",
      "name": "steel-shield"
    },
    {
      "id": 1399,
      "display_name": "Strength Ability Checks",
      "name": "strength-ability-checks"
    },
    {
      "id": 1426,
      "display_name": "Strength Attacks",
      "name": "strength-attacks"
    },
    {
      "id": 1392,
      "display_name": "Strength Saving Throws",
      "name": "strength-saving-throws"
    },
    {
      "id": 1466,
      "display_name": "Studded Leather",
      "name": "studded-leather"
    },
    {
      "id": 1418,
      "display_name": "Survival",
      "name": "survival"
    },
    {
      "id": 1502,
      "display_name": "Thieves' Tools",
      "name": "thieves-tools"
    },
    {
      "id": 1565,
      "display_name": "Three-Dragon Ante Set",
      "name": "threedragon-ante-set"
    },
    {
      "id": 1494,
      "display_name": "Tinker's Tools",
      "name": "tinkers-tools"
    },
    {
      "id": 1458,
      "display_name": "Trident",
      "name": "trident"
    },
    {
      "id": 1555,
      "display_name": "Vehicles (Land)",
      "name": "vehicles-land"
    },
    {
      "id": 1556,
      "display_name": "Vehicles (Water)",
      "name": "vehicles-water"
    },
    {
      "id": 1514,
      "display_name": "Viol",
      "name": "viol"
    },
    {
      "id": 1459,
      "display_name": "War pick",
      "name": "war-pick"
    },
    {
      "id": 1460,
      "display_name": "Warhammer",
      "name": "warhammer"
    },
    {
      "id": 1495,
      "display_name": "Weaver's Tools",
      "name": "weavers-tools"
    },
    {
      "id": 1461,
      "display_name": "Whip",
      "name": "whip"
    },
    {
      "id": 1403,
      "display_name": "Wisdom Ability Checks",
      "name": "wisdom-ability-checks"
    },
    {
      "id": 1396,
      "display_name": "Wisdom Saving Throws",
      "name": "wisdom-saving-throws"
    },
    {
      "id": 1496,
      "display_name": "Woodcarver's Tools",
      "name": "woodcarvers-tools"
    }
  ],
  "37": [
    {
      "id": 1689,
      "display_name": "0 HP",
      "name": "0-hp"
    }
  ],
  "38": [
    {
      "id": 1734,
      "display_name": "Charisma Score",
      "name": "charisma-score"
    },
    {
      "id": 1731,
      "display_name": "Constitution Score",
      "name": "constitution-score"
    },
    {
      "id": 1730,
      "display_name": "Dexterity Score",
      "name": "dexterity-score"
    },
    {
      "id": 1732,
      "display_name": "Intelligence Score",
      "name": "intelligence-score"
    },
    {
      "id": 1729,
      "display_name": "Strength Score",
      "name": "strength-score"
    },
    {
      "id": 1733,
      "display_name": "Wisdom Score",
      "name": "wisdom-score"
    }
  ],
  "39": [
    {
      "id": 1745,
      "display_name": "Blindsight",
      "name": "blindsight"
    },
    {
      "id": 1746,
      "display_name": "Darkvision",
      "name": "darkvision"
    },
    {
      "id": 1747,
      "display_name": "Tremorsense",
      "name": "tremorsense"
    },
    {
      "id": 1748,
      "display_name": "Truesight",
      "name": "truesight"
    }
  ],
  "40": [
    {
      "id": 1754,
      "display_name": "Finesse",
      "name": "finesse"
    },
    {
      "id": 1758,
      "display_name": "Heavy",
      "name": "heavy"
    },
    {
      "id": 1759,
      "display_name": "Light",
      "name": "light"
    },
    {
      "id": 1760,
      "display_name": "Loading",
      "name": "loading"
    },
    {
      "id": 1761,
      "display_name": "Range",
      "name": "range"
    },
    {
      "id": 1762,
      "display_name": "Reach",
      "name": "reach"
    },
    {
      "id": 1755,
      "display_name": "Thrown",
      "name": "thrown"
    },
    {
      "id": 1757,
      "display_name": "Two-Handed",
      "name": "twohanded"
    },
    {
      "id": 1756,
      "display_name": "Versatile",
      "name": "versatile"
    }
  ],
  "41": [
    {
      "id": 1811,
      "display_name": "Gargantuan",
      "name": "gargantuan"
    },
    {
      "id": 1810,
      "display_name": "Huge",
      "name": "huge"
    },
    {
      "id": 1809,
      "display_name": "Large",
      "name": "large"
    },
    {
      "id": 1808,
      "display_name": "Medium",
      "name": "medium"
    },
    {
      "id": 1807,
      "display_name": "Small",
      "name": "small"
    },
    {
      "id": 1806,
      "display_name": "Tiny",
      "name": "tiny"
    }
  ]
}
"""

def get_subtype_for_name_and_type_id(type_id: int, name: str) -> Dict:
    loaded_subtypes = json.loads(subtypes)
    valid_subtypes = loaded_subtypes[str(type_id)]
    for subtype_obj in valid_subtypes:
        if subtype_obj['name'] == name:
            return subtype_obj
    return None