from dnd_controller.models.all import Token, User, GameSession
from functools import wraps
from django.http import HttpResponseRedirect
from rest_framework.exceptions import NotAuthenticated

def dnd_auth_middleware(get_response):

    def middleware(request):
        headers = request.META
        auth_token = headers.get("HTTP_TOKEN")
        try:
            user = Token.validate_token(auth_token)
        except Exception:
            user = None
        request.dnd_user = user

        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware

def is_authenticated(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):

        dnd_user = request.dnd_user
        if dnd_user is not None:
            return function(request, *args, **kwargs)
        else:
            raise NotAuthenticated("Must provide valid auth_token")

    return wrap

def auth_or_admin(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):

        dnd_user = request.dnd_user
        if dnd_user is None:
            request.dnd_user = User.objects.get(pk="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc")


        return function(request, *args, **kwargs)

    return wrap


def dnd_session_middleware(get_response):

    def middleware(request):
        headers = request.META
        session_id = headers.get("HTTP_SESSION_ID")
        try:
            session = GameSession.objects.get(pk=session_id)
        except Exception:
            session = None
        request.dnd_session = session

        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware

def has_session(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):

        dnd_session = request.dnd_session
        if dnd_session is not None:
            return function(request, *args, **kwargs)
        else:
            raise NotAuthenticated("Must provide valid session_id")

    return wrap