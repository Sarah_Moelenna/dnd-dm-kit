from __future__ import annotations

from django.db import models
import uuid
import os
from django.conf import settings
from typing import Dict, List, TYPE_CHECKING

if TYPE_CHECKING:
    from .all import User

class AudioFile(models.Model):
    STATUS_DOWNLOADING = "DOWNLOADING"
    STATUS_ERROR = "ERROR"
    STATUS_READY = "READY"

    AMBIENCE = "AMBIENCE"
    MUSIC = "MUSIC"
    EFFECT = "EFFECT"

    STATUS_CHOICES = [
        (STATUS_DOWNLOADING, "Downloading audio"),
        (STATUS_ERROR, "Error while downloading"),
        (STATUS_READY, "Ready to play"),
    ]
    TYPE_CHOICES = [
        (AMBIENCE, "Ambient Sound"),
        (MUSIC, "Music"),
        (EFFECT, "Sound Effect"),
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    file_name = models.CharField(max_length=1000, db_index=True, null=True)
    display_name = models.CharField(max_length=1000, db_index=True)
    source = models.CharField(max_length=1000, null=True)
    status = models.CharField(max_length=50, db_index=True, choices=STATUS_CHOICES, default = STATUS_DOWNLOADING)
    audio_type = models.CharField(max_length=50, db_index=True, choices=TYPE_CHOICES, default = MUSIC)
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    upload = models.ForeignKey("Upload", on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def create_audio_file(source_url: str, display_name: str, user: User, audio_type: str):
        from dnd_controller.utils.yt_downloader import YTDLSource

        audio_file = AudioFile(
            display_name = display_name,
            source = source_url,
            status = AudioFile.STATUS_DOWNLOADING,
            user=user,
            audio_type=audio_type
        )
        audio_file.save()

        # TODO do as a celery task
        try:
            YTDLSource.from_url(source_url, audio_file)
        except:
            audio_file.status = AudioFile.STATUS_ERROR
            audio_file.save()

        return audio_file

    @staticmethod
    def delete_by_id(audio_id: str, user: User):
        audio = AudioFile.objects.get(pk=audio_id, user=user)
        try:
            os.remove(f'{settings.MEDIA_ROOT}/music/{audio.file_name}')
        except Exception as e:
            print(str(e))
        audio.delete()

    @staticmethod
    def get_by_filename(file_name: str):
        return AudioFile.objects.get(file_name=file_name)
    
    def to_dict(self):
        return {
            "id": self.id,
            "file_name": self.file_name,
            "name": self.display_name,
            "status": self.status,
            "audio_type": self.audio_type,
            "status_display": self.get_status_display(),
            "audio_type_display": self.get_audio_type_display(),
        }
    
    def to_export_dict(self):
        return {
            "id": str(self.id),
            "file_name": self.file_name,
            "display_name": self.display_name,
            "source": self.source,
            "status": self.status,
            "audio_type": self.audio_type,
        }

    @staticmethod
    def import_json(audio_files: List[Dict], user: User):
        from dnd_controller.utils.yt_downloader import YTDLSource

        for audio_file in audio_files:
            audio_file_obj = AudioFile(
                id = audio_file['id'],
                display_name = audio_file['display_name'],
                source = audio_file['source'],
                status = audio_file['status'],
                audio_type = audio_file['audio_type'],
                user=user,
            )
            audio_file_obj.save()

            try:
                YTDLSource.from_url(audio_file['source'], audio_file_obj)
            except:
                audio_file.status = AudioFile.STATUS_ERROR
                audio_file.save()

class Playlist(models.Model):
    AMBIENCE = "AMBIENCE"
    MUSIC = "MUSIC"

    TYPE_CHOICES = [
        (AMBIENCE, "Ambient Sound"),
        (MUSIC, "Music"),
    ]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    name = models.CharField(max_length=1000, null=True)
    audio_type = models.CharField(max_length=50, db_index=True, choices=TYPE_CHOICES, default = MUSIC)

    @property
    def audio_items(self) -> list[AudioFile]:
        return [item.audio_file for item in self.playlistitem_set.all()]

    @staticmethod
    def create(name: str, audio_type: str, user: User) -> Playlist:
        new_item = Playlist(
            user=user,
            name=name,
            audio_type =audio_type
        )
        new_item.save()
        return new_item

    def add_audio_file(self, audio_file: AudioFile):
        PlaylistItem.create(self, audio_file)

    def remove_audio_file(self, audio_file: AudioFile):
        PlaylistItem.delete(self, audio_file)

    def to_simple_dict(self):
        return {
            "audio_item_count": self.playlistitem_set.count(),
            "name": self.name,
            "id": self.id,
            "audio_type": self.audio_type
        }

    def to_dict(self):
        return {
            "audio_items": [item.to_dict() for item in self.audio_items],
            "audio_item_count": self.playlistitem_set.count(),
            "name": self.name,
            "id": self.id,
            "audio_type": self.audio_type
        }

class PlaylistItem(models.Model):
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    audio_file = models.ForeignKey(AudioFile, on_delete=models.CASCADE)

    @staticmethod
    def create(playlist: Playlist, audio_file: AudioFile):
        new_item = PlaylistItem(
            playlist=playlist,
            audio_file=audio_file
        )
        new_item.save()

    @staticmethod
    def delete(playlist: Playlist, audio_file: AudioFile):
        PlaylistItem.objects.filter(playlist=playlist).filter(audio_file=audio_file).delete()

