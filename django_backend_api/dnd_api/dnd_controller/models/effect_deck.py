from __future__ import annotations

from django.db import models
import uuid
import json
from django.conf import settings
from typing import Dict, List, TYPE_CHECKING

if TYPE_CHECKING:
    from .all import User

class EffectDeck(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True)
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    data = models.CharField(max_length=100000)
    created_at = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def create(data: list[dict], name: str, user: User):

        deck = EffectDeck(
            data = json.dumps(data),
            name = name,
            user=user,
        )
        deck.save()
        return deck

    def to_simple_dict(self):
        return {
            "id": self.id,
            "name": self.name,
        }

    def to_dict(self):
        return {
            "id": self.id,
            "data": json.loads(self.data) if self.data else None,
            "name": self.name,
        }