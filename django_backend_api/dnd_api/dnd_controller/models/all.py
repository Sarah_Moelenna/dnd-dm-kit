from __future__ import annotations

from django.db import models
import uuid
import os
import json
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from dnd_controller.utils import character_scraper
from uuid import uuid4
from typing import Optional, Tuple, Dict, List, Any, TYPE_CHECKING
from django.contrib.auth.hashers import make_password, check_password
from django.db.utils import IntegrityError
from datetime import datetime, timedelta
from dnd_controller.utils.session_key_generator import generate_session_key

class User(models.Model):

    TYPE_ADMIN = "TYPE_ADMIN"
    TYPE_USER_PLUS = "TYPE_USER_PLUS"
    TYPE_USER = "TYPE_USER"

    TYPE_CHOICES = [
        (TYPE_ADMIN, "Admin"),
        (TYPE_USER_PLUS, "User Plus"),
        (TYPE_USER, "User"),
    ]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField(max_length=50, unique=True)
    display_name = models.CharField(max_length=150)
    created_at = models.DateField(auto_now_add=True)
    password = models.CharField(max_length=1000)
    user_type = models.CharField(max_length=50, db_index=True, choices=TYPE_CHOICES, default=TYPE_USER)

    def __str__(self):
        return self.username

    def to_dict(self):
        return {
            "username": self.username,
            "display_name": self.display_name,
            "storage_used": self.storage_used,
            "max_storage": self.max_storage,
            "can_upload": self.can_upload,
            "user_type": self.user_type,
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S"),
        }

    @property
    def uploads(self):
        return self.upload_set.all()

    @property
    def storage_used(self):
        storage_used = 0
        uploads = self.uploads
        for upload in uploads:
            storage_used = storage_used + upload.file_size
        return storage_used

    @property
    def max_storage(self):
        return 5368706371

    @property
    def can_upload(self):
        return self.storage_used < self.max_storage

    @staticmethod
    def create_user(username: str, password:str):
        user = User(username=username, password=make_password(password))
        try:
            user.save()
        except IntegrityError as e:
            print (f'Caught exception while saving User: {str(e)}')
            raise ValueError("Duplicate column value.")

        return user

    def update_password(self, password:str):
        self.password = make_password(password)
        self.save()

    def check_password(self, password:str):
        return check_password(password, self.password)

    @staticmethod
    def login(username: str, password:str):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise ValueError("Invalid username.")
        if not check_password(password, user.password):
            raise ValueError("Invalid password.")
        return user

    @staticmethod
    def get_user_by_username(username: str):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise ValueError("Invalid username.")
        return user


class ContentCollection(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=10000, null=True)
    is_shareable = models.BooleanField(default=False)
    is_default = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def monster_count(self):
        return self.contentcollectionmonster_set.count()

    @property
    def item_count(self):
        return self.contentcollectionitem_set.count()

    @property
    def spell_count(self):
        return self.contentcollectionspell_set.count()

    @property
    def spell_list_count(self):
        return self.contentcollectionspelllist_set.count()

    @property
    def race_count(self):
        return self.contentcollectionrace_set.count()

    @property
    def battlemap_count(self):
        return self.contentcollectionbattlemap_set.count()

    @property
    def background_count(self):
        return self.contentcollectionbackground_set.count()

    @property
    def class_count(self):
        return self.contentcollectiondndclass_set.count()

    @property
    def subclass_count(self):
        return self.contentcollectiondndsubclass_set.count()

    @property
    def feat_count(self):
        return self.contentcollectionfeat_set.count()

    def get_user_collection_status(self, user: user):
        return self.usercontentcollection_set.filter(user=user).exists()


    def to_dict(self, user: User = None):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "is_shareable": self.is_shareable,
            "is_default": self.is_default,
            "created_by": self.user.display_name,
            "monster_count": self.monster_count,
            "item_count": self.item_count,
            "spell_count": self.spell_count,
            "spell_list_count": self.spell_list_count,
            "race_count": self.race_count,
            "battlemap_count": self.battlemap_count,
            "background_count": self.background_count,
            "class_count": self.class_count,
            "subclass_count": self.subclass_count,
            "feat_count": self.feat_count,
            "can_edit": self.user == user,
            "added": self.get_user_collection_status(user) if user is not None else False
        }

class UserContentCollection(models.Model):
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Upload(models.Model):
    TYPE_AUDIO = "TYPE_AUDIO"
    TYPE_IMAGE = "TYPE_IMAGE"

    TYPE_CHOICES = [
        (TYPE_AUDIO, "Audio File"),
        (TYPE_IMAGE, "Image File"),
    ]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    file_size = models.IntegerField()
    file_location = models.CharField(max_length=1000, null=True)
    upload_type =  models.CharField(max_length=50, db_index=True, choices=TYPE_CHOICES)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def delete_file(self):
        try:
            os.remove(f'{settings.MEDIA_ROOT}/{self.file_location}')
        except Exception as e:
            print(str(e))

    def to_dict(self):
        return {
            "id": self.id,
            "file_location": self.file_location,
            "created_at": self.created_at.strftime("%Y/%m/%d, %H:%M:%S")
        }

class Campaign(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True)
    active = models.BooleanField(default=False, db_index=True)

    server_id = models.CharField(max_length=1000, db_index=True, null=True)
    voice_channel_id = models.CharField(max_length=1000, db_index=True, null=True)
    text_info_channel_id = models.CharField(max_length=1000, db_index=True, null=True)
    text_rolling_channel_id = models.CharField(max_length=1000, db_index=True, null=True)
    notes = models.CharField(max_length=1000000, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def collections(self):
        return [item.collection for item in self.campaigncollection_set.all().order_by('order')]

    @property
    def shared_collections(self):
        return [item.collection for item in self.campaigncollection_set.filter(sharing=True).order_by('order')]

    @property
    def collections_dict(self):
        items = self.campaigncollection_set.all().order_by('order')
        dicts = []
        for item in items:
            collection_dict = item.collection.to_simple_dict()
            collection_dict['sharing'] = item.sharing
            dicts.append(collection_dict)
        return dicts

    @property
    def session(self):
        sessions = self.gamesession_set.filter(active=True).all()
        if sessions:
            return sessions[0]
        return None

    @property
    def has_running_session(self):
        return self.session != None
    
    @property
    def can_create_session(self):
        return Campaign.get_active(self.user) == None

    @staticmethod
    def create_campaign(name: str, user: User):
        campaign = Campaign(
            name=name,
            user=user
        )
        campaign.save()
        return campaign

    @staticmethod
    def delete_by_id(campaign_id: str, user: User):
        campaign = Campaign.objects.get(pk=campaign_id, user=user)
        campaign.delete()

    def update_active(self, state: bool):
        if state == True:
            Campaign.objects.all().update(active=False)

        self.active = state
        self.save()

    @staticmethod
    def get_characters_to_track(user: User):
        campaign = Campaign.get_active(user)
        if campaign:
            return campaign.character_set.filter(tracking=True).order_by('name').all()
        else:
            return []

    @staticmethod
    def get_allies_to_track(user: User):
        campaign = Campaign.get_active(user)
        if campaign:
            allies = campaign.ally_set.filter(tracking=True).order_by('display_name').all()
            return [ally.to_tracking_dict() for ally in allies]
        else:
            return []

    @staticmethod
    def get_active(user: User):
        return Campaign.objects.filter(user=user,gamesession__active=True).first()

    @staticmethod
    def get_all_active():
        return Campaign.objects.filter(gamesession__active=True).all()

    def to_simple_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "active": self.active,
            "characters": [character.to_dict() for character in self.character_set.all().order_by('name')]
        }

    def to_name_dict(self):
        return {
            "id": self.id,
            "name": self.name,
        }

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "active": self.active,
            "characters": [character.to_dict() for character in self.character_set.all().order_by('name')],
            "allies": [ally.to_dict() for ally in self.ally_set.all().order_by('display_name')],
            "server_id": self.server_id,
            "voice_channel_id": self.voice_channel_id,
            "text_info_channel_id": self.text_info_channel_id,
            "text_rolling_channel_id": self.text_rolling_channel_id,
            "notes": self.notes,
            "collections": self.collections_dict,
            "session": self.session.to_simple_dict() if self.session else None,
            "can_create_session": self.can_create_session
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "active": self.active,
            "server_id": self.server_id,
            "voice_channel_id": self.voice_channel_id,
            "text_info_channel_id": self.text_info_channel_id,
            "text_rolling_channel_id": self.text_rolling_channel_id,
            "notes": self.notes,
            "characters": [character.to_export_dict() for character in self.character_set.all().order_by('name')],
            "allies": [ally.to_export_dict() for ally in self.ally_set.all().order_by('display_name')],
            "campaign_collection": [item.to_export_dict() for item in self.campaigncollection_set.all().order_by('campaign_id')],
        }

    @staticmethod
    def import_json(campaigns: List[Dict], user: User):
        for campaign in campaigns:
            campaign_obj = Campaign(
                id = campaign['id'],
                name = campaign['name'],
                active = campaign['active'],
                server_id = campaign['server_id'],
                voice_channel_id = campaign['voice_channel_id'],
                text_info_channel_id = campaign['text_info_channel_id'],
                text_rolling_channel_id = campaign['text_rolling_channel_id'],
                notes = campaign['notes'],
                user=user,
            )
            campaign_obj.save()
            for character in campaign.get("characters", []):
                character_obj = Character(
                    id = character['id'],
                    url = character['url'],
                    name = character['name'],
                    avatar = character['avatar'],
                    tracking = character['tracking'],
                    campaign = campaign_obj
                )
                character_obj.save()

            for ally in campaign.get("allies", []):
                monster = Monster.objects.get(pk=ally['monster_id'],user=user)
                ally_obj = Ally(
                    id = ally['id'],
                    display_name = ally['display_name'],
                    damage_taken = ally['damage_taken'],
                    max_health = ally['max_health'],
                    tracking = ally['tracking'],
                    campaign = campaign_obj,
                    monster = monster
                )
                ally_obj.save()

            CampaignCollection.import_json(campaign['campaign_collection'], campaign_obj, user)


    def get_character_start_states(self):
        character_states = []
        for character in self.character_set.all().order_by('name'):
            if character.tracking:
                character_states.append(
                    {
                        "type": "CHARACTER",
                        "id": str(uuid4()),
                        "object_id": str(character.id),
                        "name": character.name,
                        "conditions": [],
                        "state": "ALIVE",
                        "image": character.avatar,
                        "initiative": None,
                        "x": None,
                        "y": None
                    }
                )
        for ally in self.ally_set.all().order_by('display_name'):
            if ally.tracking:
                character_states.append(
                    {
                        "type": "ALLY",
                        "id": str(uuid4()),
                        "object_id": str(ally.monster.id),
                        "ally_id": str(ally.id),
                        "name": ally.monster.name,
                        "display_name": ally.display_name,
                        "conditions": [],
                        "state": "ALIVE",
                        "image": ally.monster.image,
                        "initiative": None,
                        "max_health": ally.max_health,
                        "damage_taken": ally.damage_taken,
                        "x": None,
                        "y": None
                    }
                )
        return character_states

class Character(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    url = models.CharField(max_length=1000, db_index=True)
    name = models.CharField(max_length=1000, db_index=True, null=True)
    avatar = models.CharField(max_length=1000, db_index=True, null=True)
    tracking = models.BooleanField(default=False, db_index=True)

    @staticmethod
    def create_character(url: str, campaign: Campaign):
        
        character_data = character_scraper.scrape_character_data([url])[0]

        character = Character(
            campaign=campaign,
            url=url,
            name=character_data['name'],
            avatar=character_data['avatar'],
        )
        character.save()

        return character

    @staticmethod
    def update_tracking_by_id(character_id: str, state: bool, user: User):
        character = Character.objects.get(pk=character_id)
        if character.campaign.user != user:
            raise AttributeError("Invalid user")
        character.tracking = state
        character.save()

    @staticmethod
    def delete_by_id(character_id: str, user: User):
        character = Character.objects.get(pk=character_id)
        if character.campaign.user != user:
            raise AttributeError("Invalid user")
        character.delete()

    def to_dict(self):
        return {
            "id": self.id,
            "url": self.url,
            "name": self.name,
            "avatar": self.avatar,
            "tracking": self.tracking,
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "url": self.url,
            "campaign_id": str(self.campaign.id),
            "name": self.name,
            "avatar": self.avatar,
            "tracking": self.tracking,
        }

class GameSession(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    key = models.CharField(max_length=2000, db_index=True, unique=True)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    active = models.BooleanField(default=True, db_index=True)

    @staticmethod
    def create(campaign: Campaign):

        if campaign.has_running_session == False and campaign.can_create_session == True:
            key = generate_session_key()
            obj = GameSession(
                campaign=campaign,
                key=key
            )
            obj.save()

    @staticmethod
    def delete_by_id(session_id: str, user: User):
        session = GameSession.objects.get(pk=session_id)
        if session.campaign.user != user:
            raise AttributeError("Invalid user")
        session.delete()

    @property
    def collections(self):
        return self.campaign.shared_collections

    @property
    def characters(self):
        characters = self.campaign.character_set.filter(tracking=True).order_by('name').all()
        if characters:
            return characters
        return []

    def to_simple_dict(self):
        return {
            "id": str(self.id),
            "key": self.key,
            "characters": [character.to_dict() for character in self.characters],
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S"),
        }
    
    def to_dict(self):
        return {
            "id": str(self.id),
            "key": self.key,
            "campaign": self.campaign.to_dict(),
            "active": self.active,
            "characters": [character.to_dict() for character in self.characters],
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S"),
        }
    
class Monster(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True, default='')
    image = models.CharField(max_length=1000, null=True)
    actions = models.CharField(max_length=100000, null=True)
    legendary_actions = models.CharField(max_length=10000, null=True)
    reactions = models.CharField(max_length=10000, null=True)
    more_info = models.CharField(max_length=100000, null=True)
    description = models.CharField(max_length=100000, null=True)
    mythic = models.CharField(max_length=100000, null=True)
    bonus = models.CharField(max_length=100000, null=True)
    armour_class = models.CharField(max_length=1000, null=True)
    hit_points = models.CharField(max_length=1000, null=True)
    speed = models.CharField(max_length=1000, null=True)
    saving_throws = models.CharField(max_length=1000, null=True)
    skills = models.CharField(max_length=1000, null=True)
    condition_immunities = models.CharField(max_length=1000, null=True)
    damage_immunities = models.CharField(max_length=1000, null=True)
    damage_resistances = models.CharField(max_length=1000, null=True)
    damage_vulnerabilities = models.CharField(max_length=1000, null=True)
    senses = models.CharField(max_length=1000, null=True)
    languages = models.CharField(max_length=1000, null=True)
    types = models.CharField(max_length=1000, null=True)
    environments = models.CharField(max_length=1000, null=True)
    strength = models.IntegerField(null=True)
    dexterity = models.IntegerField(null=True)
    constitution = models.IntegerField(null=True)
    intelligence = models.IntegerField(null=True)
    wisdom = models.IntegerField(null=True)
    charisma = models.IntegerField(null=True)
    challenge = models.CharField(max_length=1000, null=True)
    challenge_number = models.FloatField(null=True)
    proficiency_bonus = models.CharField(max_length=1000, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_made_content = models.BooleanField(default=False, db_index=True)

    def can_edit(self, user: User = None):
        if not user:
            return False

        if self.user_made_content == True:
            return self.user == user
        
        else:
            return self.user == user and Setting.get_setting("CAN_EDIT_NON_HOMEBREW", user)['value'] == True

    def to_dict(self, user: User = None):
        return {
            "id": self.id,
            "name": self.name,
            "image": self.image,
            "actions": self.actions,
            "legendary_actions": self.legendary_actions,
            "reactions": self.reactions,
            "more_info": self.more_info,
            "description": self.description,
            "mythic": self.mythic,
            "bonus": self.bonus,
            "armour_class": self.armour_class,
            "hit_points": self.hit_points,
            "speed": self.speed,
            "saving_throws": self.saving_throws,
            "skills": self.skills,
            "condition_immunities": self.condition_immunities,
            "damage_immunities": self.damage_immunities,
            "damage_resistances": self.damage_resistances,
            "damage_vulnerabilities": self.damage_vulnerabilities,
            "senses": self.senses,
            "languages": self.languages,
            "types": self.types,
            "environments": self.environments,
            "strength": self.strength,
            "dexterity": self.dexterity,
            "constitution": self.constitution,
            "intelligence": self.intelligence,
            "wisdom": self.wisdom,
            "charisma": self.charisma,
            "challenge": self.challenge,
            "proficiency_bonus": self.proficiency_bonus,
            "xp": self.get_xp(),
            "user_made_content": self.user_made_content,
            "can_edit": self.can_edit(user)
        }

    def to_simple_dict(self, user: User = None):
        return {
            "id": self.id,
            "name": self.name,
            "image": self.image,
            "types": self.types,
            "environments": self.environments,
            "challenge": self.challenge,
            "user_made_content": self.user_made_content,
            "strength": self.strength,
            "dexterity": self.dexterity,
            "constitution": self.constitution,
            "intelligence": self.intelligence,
            "wisdom": self.wisdom,
            "charisma": self.charisma,
            "xp": self.get_xp(),
            "can_edit": self.can_edit(user)
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "image": self.image,
            "actions": self.actions,
            "legendary_actions": self.legendary_actions,
            "reactions": self.reactions,
            "more_info": self.more_info,
            "description": self.description,
            "mythic": self.mythic,
            "bonus": self.bonus,
            "armour_class": self.armour_class,
            "hit_points": self.hit_points,
            "speed": self.speed,
            "saving_throws": self.saving_throws,
            "skills": self.skills,
            "condition_immunities": self.condition_immunities,
            "damage_immunities": self.damage_immunities,
            "damage_resistances": self.damage_resistances,
            "damage_vulnerabilities": self.damage_vulnerabilities,
            "senses": self.senses,
            "languages": self.languages,
            "types": self.types,
            "environments": self.environments,
            "strength": self.strength,
            "dexterity": self.dexterity,
            "constitution": self.constitution,
            "intelligence": self.intelligence,
            "wisdom": self.wisdom,
            "charisma": self.charisma,
            "challenge": self.challenge,
            "challenge_number": self.challenge_number,
            "proficiency_bonus": self.proficiency_bonus,
            "user_made_content": self.user_made_content,
        }

    @staticmethod
    def import_json(monsters: List[Dict], user: User):
        for monster in monsters:
            monster_obj = Monster(
                id = monster['id'],
                name = monster['name'],
                image = monster['image'],
                actions = monster['actions'],
                legendary_actions = monster['legendary_actions'],
                reactions = monster['reactions'],
                more_info = monster['more_info'],
                description = monster['description'],
                mythic = monster['mythic'],
                bonus = monster['bonus'],
                armour_class = monster['armour_class'],
                hit_points = monster['hit_points'],
                speed = monster['speed'],
                saving_throws = monster['saving_throws'],
                skills = monster['skills'],
                condition_immunities = monster['condition_immunities'],
                damage_immunities = monster['damage_immunities'],
                damage_resistances = monster['damage_resistances'],
                damage_vulnerabilities = monster['damage_vulnerabilities'],
                senses = monster['senses'],
                languages = monster['languages'],
                types = monster['types'],
                environments = monster['environments'],
                strength = monster['strength'],
                dexterity = monster['dexterity'],
                constitution = monster['constitution'],
                intelligence = monster['intelligence'],
                wisdom = monster['wisdom'],
                charisma = monster['charisma'],
                challenge = monster['challenge'],
                challenge_number = monster['challenge_number'],
                proficiency_bonus = monster['proficiency_bonus'],
                user_made_content = monster['user_made_content'],
                user=user,
            )
            monster_obj.save()

    @staticmethod
    def create_monster(monster: Dict, user: User):
        monster_obj = Monster(
            name = monster['name'],
            image = monster['image'],
            actions = monster['actions'],
            legendary_actions = monster['legendary_actions'],
            reactions = monster['reactions'],
            more_info = monster['more_info'],
            description = monster['description'],
            mythic = monster['mythic'],
            bonus = monster['bonus'],
            armour_class = monster['armour_class'],
            hit_points = monster['hit_points'],
            speed = monster['speed'],
            saving_throws = monster['saving_throws'],
            skills = monster['skills'],
            condition_immunities = monster['condition_immunities'],
            damage_immunities = monster['damage_immunities'],
            damage_resistances = monster['damage_resistances'],
            damage_vulnerabilities = monster['damage_vulnerabilities'],
            senses = monster['senses'],
            languages = monster['languages'],
            types = monster['types'],
            environments = monster['environments'],
            strength = monster['strength'],
            dexterity = monster['dexterity'],
            constitution = monster['constitution'],
            intelligence = monster['intelligence'],
            wisdom = monster['wisdom'],
            charisma = monster['charisma'],
            challenge = monster['challenge'],
            challenge_number = monster['challenge_number'],
            proficiency_bonus = monster['proficiency_bonus'],
            user_made_content = True,
            user=user,
        )
        monster_obj.save()

    def edit_monster(self, monster: Dict):
        self.name = monster['name']
        self.image = monster['image']
        self.actions = monster['actions']
        self.legendary_actions = monster['legendary_actions']
        self.reactions = monster['reactions']
        self.more_info = monster['more_info']
        self.description = monster['description']
        self.mythic = monster['mythic']
        self.bonus = monster['bonus']
        self.armour_class = monster['armour_class']
        self.hit_points = monster['hit_points']
        self.speed = monster['speed']
        self.saving_throws = monster['saving_throws']
        self.skills = monster['skills']
        self.condition_immunities = monster['condition_immunities']
        self.damage_immunities = monster['damage_immunities']
        self.damage_resistances = monster['damage_resistances']
        self.damage_vulnerabilities = monster['damage_vulnerabilities']
        self.senses = monster['senses']
        self.languages = monster['languages']
        self.types = monster['types']
        self.environments = monster['environments']
        self.strength = monster['strength']
        self.dexterity = monster['dexterity']
        self.constitution = monster['constitution']
        self.intelligence = monster['intelligence']
        self.wisdom = monster['wisdom']
        self.charisma = monster['charisma']
        self.challenge = monster['challenge']
        self.challenge_number = monster['challenge_number']
        self.proficiency_bonus = monster['proficiency_bonus']
        self.user_made_content = True
        self.save()

    def get_xp(self) -> int:
        return int(self.challenge.split(' ')[1].replace(',', '').replace('(', '').replace(')', '').replace('XP', ''))


class ContentCollectionMonster(models.Model):
    monster = models.ForeignKey(Monster, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionMonster.objects.filter(monster=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionMonster(monster=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionMonster.objects.get(monster=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionMonster.objects.filter(monster=resource, content_collection__user=user).all()

class Ally(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=1000, db_index=True, null=True)
    tracking = models.BooleanField(default=False, db_index=True)
    damage_taken = models.IntegerField(default=0)
    max_health = models.IntegerField()
    monster = models.ForeignKey(Monster, on_delete=models.CASCADE)

    @staticmethod
    def create_ally(campaign: Campaign, monster: Monster, display_name: str, max_health: int):
        
        ally = Ally(
            campaign=campaign,
            monster=monster,
            display_name=display_name,
            max_health=max_health,
        )
        ally.save()

        return ally

    @staticmethod
    def update_by_id(ally_id: str, tracking: bool, display_name: str, damage_taken:int, max_health: int, user: User):
        ally = Ally.objects.get(pk=ally_id)
        if ally.campaign.user != user:
            raise AttributeError("Inavlid User")
        if tracking != None:
            ally.tracking = tracking
        if display_name != None:
            ally.display_name = display_name
        if damage_taken != None:
            ally.damage_taken = damage_taken
        if max_health != None:
            ally.max_health = max_health
        ally.save()

    @staticmethod
    def delete_by_id(ally_id: str, user: User):
        ally = Ally.objects.get(pk=ally_id)
        if ally.campaign.user != user:
            raise AttributeError("Inavlid User")
        ally.delete()

    def to_dict(self):
        return {
            "id": str(self.id),
            "display_name": self.display_name,
            "damage_taken": self.damage_taken,
            "max_health": self.max_health,
            "tracking": self.tracking,
            "monster": self.monster.to_dict()
        }

    def to_tracking_dict(self):
    
        return {
            "url": None,
            "name": self.display_name,
            "avatar": self.monster.image,
            "current_xp": 0,
            "max_health": self.max_health,
            "current_health": self.max_health - self.damage_taken,
            "strength": self.monster.strength,
            "dexterity": self.monster.dexterity,
            "constitution": self.monster.constitution,
            "intelligence": self.monster.intelligence,
            "wisdom": self.monster.wisdom,
            "charisma": self.monster.charisma
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "campaign_id": str(self.campaign.id),
            "monster_id": str(self.monster.id),
            "display_name": self.display_name,
            "damage_taken": self.damage_taken,
            "max_health": self.max_health,
            "tracking": self.tracking,
        }


class BattleMap(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True)
    image = models.CharField(max_length=1000, null=True)
    rows = models.IntegerField(default=10)
    columns = models.IntegerField(default=10)
    blocked_cells = models.CharField(max_length=100000, default="")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)


    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "image": self.image,
            "rows": self.rows,
            "columns": self.columns,
            "blocked_cells": self.blocked_cells,
            "can_edit": self.user == user
        }

    def to_export_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "image": self.image,
            "rows": self.rows,
            "columns": self.columns,
            "blocked_cells": self.blocked_cells,
        }

    @staticmethod
    def import_json(battlemaps: List[Dict], user: User):
        for map_item in battlemaps:
            map_obj = BattleMap(
                id = map_item['id'],
                name = map_item['name'],
                image = map_item['image'],
                rows = map_item['rows'],
                columns = map_item['columns'],
                blocked_cells = map_item['blocked_cells'],
                user=user
            )
            map_obj.save()

class ContentCollectionBattleMap(models.Model):
    battlemap = models.ForeignKey(BattleMap, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionBattleMap.objects.filter(battlemap=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionBattleMap(battlemap=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionBattleMap.objects.get(battlemap=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionBattleMap.objects.filter(battlemap=resource, content_collection__user=user).all()

class Setting(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True)
    value = models.CharField(max_length=1000, db_index=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


    GROUPING_BASIC = "Base Settings"
    GROUPING_ROLLS = "Dice Roll Settings"
    GROUPING_ADMIN = "Admin Settings"

    SETTINGS = {
        "DM_NAME": {
            "default_value": "Dungeon Master",
            "display_name": "Dungeon Master Name",
            "type": "STRING",
            "grouping":GROUPING_BASIC
        },
        "POST_MONSTER_ROLL": {
            "default_value": True,
            "display_name": "Post Monster Rolls to Discord Automatically",
            "type": "BOOLEAN",
            "grouping":GROUPING_ROLLS
        },
        "POST_ALLY_ROLL": {
            "default_value": True,
            "display_name": "Post Ally Rolls to Discord Automatically",
            "type": "BOOLEAN",
            "grouping":GROUPING_ROLLS
        },
        "CAN_EDIT_NON_HOMEBREW": {
            "default_value": False,
            "display_name": "Can Edit Non Hombrew Content",
            "type": "BOOLEAN",
            "grouping":GROUPING_ADMIN
        }
    }

    def to_dict(self):
        return {
            "name": self.name,
            "value": json.loads(self.value),
            "grouping": Setting.SETTINGS.get(self.name)["grouping"],
            "type": Setting.SETTINGS.get(self.name)["type"],
            "display_name": Setting.SETTINGS.get(self.name)["display_name"],
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "value": self.value,
        }

    @staticmethod
    def import_json(settings: List[Dict], user: User):
        for setting in settings:
            setting_obj = Setting(
                id = setting['id'],
                name = setting['name'],
                value = setting['value'],
                user = user,
            )
            setting_obj.save()

    @staticmethod
    def get_setting(setting: str, user: User):
        setting_details = Setting.SETTINGS.get(setting)

        try: 
            setting_object: Setting() = Setting.objects.filter(name=setting,user=user).get()
        except ObjectDoesNotExist:
            setting_object = Setting(
                name=setting,
                value=json.dumps(setting_details["default_value"]),
                user=user
            )
            setting_object.save()
        return setting_object.to_dict()

    @staticmethod
    def set_setting(setting: str, value, user: User):
        setting_details = Setting.SETTINGS.get(setting)
        if user.user_type != User.TYPE_ADMIN and setting_details['grouping'] == Setting.GROUPING_ADMIN:
            raise ValueError("Non Admin can't set Admin Settings")

        try: 
            setting_object: Setting() = Setting.objects.filter(name=setting,user=user).get()
            setting_object.value=json.dumps(value)
            setting_object.save()
        except ObjectDoesNotExist:
            setting_object = Setting(
                name=setting,
                value=json.dumps(value),
                user=user
            )
            setting_object.save()
        return setting_object.to_dict()

class Collection(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True)
    heirachy = models.CharField(max_length=10000, db_index=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def pages(self):
        page_list = []
        pages = self.page_set.all()
        page_dict = {str(page.id): page for page in pages}
        if self.heirachy:
            heirachy = json.loads(self.heirachy)
            for key in heirachy.keys():
                page_list.append(page_dict[heirachy[key]["id"]])
        for page in pages:
            if page not in page_list:
                page_list.append(page)
        return page_list

    def tidy_up_heirachy(self, exclude_page_id):
        page_ids = [str(page.id) for page in self.page_set.all()]
        if self.heirachy:
            new_heirachy = {}
            heirachy = json.loads(self.heirachy)
            new_key = 0
            for key, value in heirachy.items():
                if value['id'] in page_ids and value['id'] is not exclude_page_id:
                    new_heirachy[new_key] = value
                    new_key = new_key + 1
            self.heirachy = json.dumps(new_heirachy)


    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "heirachy": json.loads(self.heirachy) if self.heirachy else None,
            "pages": [page.to_dict() for page in self.pages],
        }

    def to_simple_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "heirachy": json.loads(self.heirachy) if self.heirachy else None,
            "pages": [page.to_simple_dict() for page in self.pages],
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "heirachy": self.heirachy,
            "pages": [page.to_export_dict() for page in self.pages],
        }

    @staticmethod
    def import_json(collections: List[Dict], user: User):
        for collection in collections:
            collection_obj = Collection(
                id = collection['id'],
                name = collection['name'],
                heirachy = collection['heirachy'],
                user=user,
            )
            collection_obj.save()
            Page.import_json(collection['pages'], collection_obj)


class Page(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True)
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE)

    @property
    def sections(self):
        return self.page_section_set.all().order_by('order')

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "sections": [section.to_dict() for section in self.sections],
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "collection_id": str(self.collection.id),
            "sections": [section.to_export_dict() for section in self.sections],
        }

    @staticmethod
    def import_json(pages: List[Dict], collection: Collection):
        for page in pages:
            page_obj = Page(
                id = page['id'],
                name = page['name'],
                collection=collection
            )
            page_obj.save()
            Page_Section.import_json(page['sections'], page_obj)

    def to_simple_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
        }

    def add_page_section(self, section_type: str, order: int):

        for section in self.sections:
            if section.order >= order:
                section.order = section.order + 1
                section.save()

        new_section = Page_Section(page=self, section_type=section_type, order=order)
        new_section.save()



class Page_Section(models.Model):
    TYPE_COLUMN_1 = "COLUMN_1"
    TYPE_COLUMN_2 = "COLUMN_2"
    TYPE_COLUMN_3 = "COLUMN_3"
    TYPE_COLUMN_4 = "COLUMN_4"

    TYPE_CHOICES = [
        (TYPE_COLUMN_1, TYPE_COLUMN_1),
        (TYPE_COLUMN_2, TYPE_COLUMN_2),
        (TYPE_COLUMN_3, TYPE_COLUMN_3),
        (TYPE_COLUMN_4, TYPE_COLUMN_4),
    ]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    order = models.IntegerField()
    section_type = models.CharField(max_length=50, db_index=True, choices=TYPE_CHOICES, default = TYPE_COLUMN_1)

    @property
    def components(self):
        return self.page_component_set.all().order_by('order')

    def to_dict(self):
        return {
            "id": str(self.id),
            "order": self.order,
            "section_type": self.section_type,
            "components": [component.to_dict() for component in self.components],
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "order": self.order,
            "section_type": self.section_type,
            "page_id": str(self.page.id),
            "components": [component.to_export_dict() for component in self.components],
        }

    @staticmethod
    def import_json(sections: List[Dict], page: Page):
        for section in sections:
            section_obj = Page_Section(
                id = section['id'],
                order = section['order'],
                section_type = section['section_type'],
                page=page
            )
            section_obj.save()
            Page_Component.import_json(section['components'], section_obj)

    def add_component(self, column: int, order: int, contents: Dict):
        for component in self.components:
            if component.column == column and component.order >= order:
                component.order = component.order + 1
                component.save()

        new_component = Page_Component(page_section=self, column=column, order=order, contents=json.dumps(contents))
        new_component.save()


class Page_Component(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    column = models.IntegerField()
    order = models.IntegerField()
    contents = models.CharField(max_length=100000)
    page_section = models.ForeignKey(Page_Section, on_delete=models.CASCADE)

    def to_dict(self):
        return {
            "id": str(self.id),
            "order": self.order,
            "column": self.column,
            "contents": json.loads(self.contents),
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "order": self.order,
            "column": self.column,
            "contents": self.contents,
            "section_id": str(self.page_section.id)
        }

    @staticmethod
    def import_json(components: List[Dict], section: Page_Section):
        for component in components:
            component_obj = Page_Component(
                id = component['id'],
                order = component['order'],
                column = component['column'],
                contents = component['contents'],
                page_section = section
            )
            component_obj.save()

class CampaignCollection(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE)
    order = models.IntegerField(null=True)
    sharing = models.BooleanField(default=False)

    def to_dict(self):
        return {
            "collection": self.collection.to_simple_dict(),
        }

    def to_export_dict(self):
        return {
            "collection_id": str(self.collection.id),
            "id": str(self.pk),
            "campaign_id": str(self.campaign.id),
            "order": self.order
        }

    @staticmethod
    def import_json(campaign_collections: List[Dict], campaign: Campaign, user: User):
        for campaign_collection in campaign_collections:

            collection = Collection.objects.get(pk=campaign_collection['collection_id'], user=user)

            campaign_collection_obj = CampaignCollection(
                campaign = campaign,
                collection = collection,
                order = campaign_collection.get('order')
            )
            campaign_collection_obj.save()

class Map(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True)
    url = models.CharField(max_length=1000, null=True)
    data = models.CharField(max_length=100000, default="{}")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)


    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "url": self.url,
            "data": json.loads(self.data) if self.data != None else None,
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "url": self.url,
            "data": self.data,
        }

    @staticmethod
    def import_json(maps: List[Dict], user: User):
        for map_item in maps:
            map_obj = Map(
                id = map_item['id'],
                name = map_item['name'],
                url = map_item['url'],
                data = map_item['data'],
                user=user
            )
            map_obj.save()

class Token(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)
    expires_at = models.DateField(null=False)

    @staticmethod
    def create_token(user: User):
        token = Token()
        token.user = user
        token.expires_at = (datetime.now() + timedelta(hours=48))
        token.save()
        return token

    @staticmethod
    def validate_token(token: str) -> User:
        try:
            token = Token.objects.get(id=token)
        except Token.DoesNotExist:
            raise ValueError("Invalid token.")
        if token.expires_at < datetime.now().date():
            raise ValueError("Expired token.")
        return token.user

class Modifier(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    fixed_value = models.IntegerField(null=True)
    type = models.CharField(max_length=1000, null=True)
    sub_type = models.CharField(max_length=1000, null=True)
    dice = models.CharField(max_length=1000, null=True) #json
    restriction = models.CharField(max_length=1000, null=True)
    stat_id = models.CharField(max_length=1000, null=True)
    requires_attunement = models.BooleanField(null=True)
    friendly_type_name = models.CharField(max_length=1000, null=True)
    friendly_sub_type_name = models.CharField(max_length=1000, null=True)
    duration = models.CharField(max_length=1000, null=True) #json
    is_granted = models.BooleanField(null=True)
    bonus_types = models.CharField(max_length=10000, null=True) #json blob for now
    value = models.IntegerField(null=True)
    available_to_multiclass = models.BooleanField(null=True)
    modifier_type_id = models.CharField(max_length=1000, null=True)
    modifier_sub_type_id = models.CharField(max_length=1000, null=True)
    count = models.IntegerField(null=True)
    duration_unit = models.CharField(max_length=1000, null=True)
    use_primary_stat = models.BooleanField(null=True)
    at_higher_levels = models.CharField(max_length=10000, null=True) #json
    limits = models.CharField(max_length=1000, null=True)

    def to_dict(self):
        return {
            "id": str(self.id),
            "fixed_value": self.fixed_value,
            "type": self.type,
            "sub_type": self.sub_type,
            "dice": json.loads(self.dice),
            "restriction": self.restriction,
            "stat_id": self.stat_id,
            "requires_attunement": self.requires_attunement,
            "friendly_type_name": self.friendly_type_name,
            "friendly_sub_type_name": self.friendly_sub_type_name,
            "duration": json.loads(self.duration) if self.duration else None,
            "is_granted": self.is_granted,
            "bonus_types": self.bonus_types,
            "value": self.value,
            "available_to_multiclass": self.available_to_multiclass,
            "modifier_type_id": self.modifier_type_id,
            "modifier_sub_type_id": self.modifier_sub_type_id,
            "count": self.count,
            "duration_unit": self.duration_unit,
            "use_primary_stat": self.use_primary_stat,
            "at_higher_levels": json.loads(self.at_higher_levels) if self.at_higher_levels else [],
            "limits": json.loads(self.limits) if self.limits else None,
        }

    def delete_modifier(self):
        self.delete()

    @staticmethod
    def create_modifier(parent: models.Model, modifier_object: Dict):
        new_modifier = Modifier(
            fixed_value = modifier_object.get("fixed_value", None),
            type = modifier_object.get("type", None),
            sub_type = modifier_object.get("sub_type", None),
            dice = json.dumps(modifier_object.get("dice")) if "dice" in modifier_object.keys() else None,
            restriction = modifier_object.get("restriction", None),
            stat_id = modifier_object.get("stat_id", None),
            requires_attunement = modifier_object.get("requires_attunement", False),
            friendly_type_name = modifier_object.get("friendly_type_name", None),
            friendly_sub_type_name = modifier_object.get("friendly_sub_type_name", None),
            duration = json.dumps(modifier_object.get("duration")) if "duration" in modifier_object.keys() else None,
            is_granted = modifier_object.get("is_granted", False),
            bonus_types = modifier_object.get("bonus_types", None),
            value = modifier_object.get("value", None),
            available_to_multiclass = modifier_object.get("available_to_multiclass", False),
            modifier_type_id = modifier_object.get("modifier_type_id", None),
            modifier_sub_type_id = modifier_object.get("modifier_sub_type_id", None),
            count = modifier_object.get("count", None),
            duration_unit = modifier_object.get("duration_unit", None),
            use_primary_stat = modifier_object.get("use_primary_stat", False),
            at_higher_levels = json.dumps(modifier_object.get("at_higher_levels")) if "at_higher_levels" in modifier_object.keys() else json.dumps([]),
            limits = json.dumps(modifier_object.get("limits")) if "limits" in modifier_object.keys() else None,
        )
        new_modifier.save()

        if isinstance(parent, RacialTrait):
            relation_obj = RacialTraitModifier(
                racial_trait = parent,
                modifier = new_modifier
            )
            relation_obj.save()
        elif isinstance(parent, Option):
            relation_obj = OptionModifier(
                option = parent,
                modifier = new_modifier
            )
            relation_obj.save()
        elif isinstance(parent, Spell):
            relation_obj = SpellModifier(
                spell = parent,
                modifier = new_modifier
            )
            relation_obj.save()
        elif isinstance(parent, Item):
            relation_obj = ItemModifier(
                item = parent,
                modifier = new_modifier
            )
            relation_obj.save()
        elif isinstance(parent, Background):
            relation_obj = BackgroundModifier(
                background = parent,
                modifier = new_modifier
            )
            relation_obj.save()
        elif isinstance(parent, DndClassFeature):
            relation_obj = DndClassFeatureModifier(
                dnd_class_feature = parent,
                modifier = new_modifier
            )
            relation_obj.save()
        elif isinstance(parent, DndClassResource):
            relation_obj = DndClassResourceModifier(
                dnd_class_resource = parent,
                modifier = new_modifier
            )
            relation_obj.save()
        elif isinstance(parent, Feat):
            relation_obj = FeatModifier(
                feat = parent,
                modifier = new_modifier
            )
            relation_obj.save()


    def update_modifier(self, modifier_object: Dict):
        self.fixed_value = modifier_object.get("fixed_value", None)
        self.type = modifier_object.get("type", None)
        self.sub_type = modifier_object.get("sub_type", None)
        self.dice = json.dumps(modifier_object.get("dice")) if "dice" in modifier_object.keys() else None
        self.restriction = modifier_object.get("restriction", None)
        self.stat_id = modifier_object.get("stat_id", None)
        self.requires_attunement = modifier_object.get("requires_attunement", False)
        self.friendly_type_name = modifier_object.get("friendly_type_name", None)
        self.friendly_sub_type_name = modifier_object.get("friendly_sub_type_name", None)
        self.duration = json.dumps(modifier_object.get("duration")) if "duration" in modifier_object.keys() else None
        self.is_granted = modifier_object.get("is_granted", False)
        self.bonus_types = modifier_object.get("bonus_types", None)
        self.value = modifier_object.get("value", None)
        self.available_to_multiclass = modifier_object.get("available_to_multiclass", False)
        self.modifier_type_id = modifier_object.get("modifier_type_id", None)
        self.modifier_sub_type_id = modifier_object.get("modifier_sub_type_id", None)
        self.count = modifier_object.get("count", None)
        self.duration_unit = modifier_object.get("duration_unit", None)
        self.use_primary_stat = modifier_object.get("use_primary_stat", False)
        self.at_higher_levels = json.dumps(modifier_object.get("at_higher_levels")) if "at_higher_levels" in modifier_object.keys() else json.dumps([])
        self.limits = json.dumps(modifier_object.get("limits")) if "limits" in modifier_object.keys() else None
        self.save()

class Spell(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    level = models.IntegerField(null=True)
    school = models.CharField(max_length=1000, null=True)
    duration = models.CharField(max_length=1000, null=True) #json
    activation = models.CharField(max_length=1000, null=True) #json
    range = models.CharField(max_length=1000, null=True) #json
    as_part_of_weapon_attack = models.BooleanField(null=True)
    description = models.CharField(max_length=100000, null=True)
    snippet = models.CharField(max_length=1000, null=True)
    concentration = models.BooleanField(null=True)
    ritual = models.BooleanField(null=True)
    range_area = models.CharField(max_length=1000, null=True) #json
    damage_effect = models.CharField(max_length=1000, null=True) #json
    components = models.CharField(max_length=1000, null=True) #json
    components_description = models.CharField(max_length=1000, null=True)
    save_dc_ability_id = models.IntegerField(null=True)
    healing = models.CharField(max_length=1000, null=True) #json
    healing_dice = models.CharField(max_length=1000, null=True) #json
    temp_hp_dice = models.CharField(max_length=1000, null=True) #json
    attack_type = models.IntegerField(null=True)
    can_cast_at_higher_level = models.BooleanField(null=True)
    is_homebrew = models.BooleanField(null=True)
    requires_saving_throw = models.BooleanField(null=True)
    requires_attack_roll = models.BooleanField(null=True)
    at_higher_levels = models.CharField(max_length=10000, null=True) #json
    conditions = models.CharField(max_length=10000, null=True) #json
    tags = models.CharField(max_length=1000, null=True) #json
    casting_time_description = models.CharField(max_length=1000, null=True)
    scale_type = models.CharField(max_length=1000, null=True)
    spell_groups = models.CharField(max_length=1000, null=True) #json
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    @property
    def modifiers(self):
        return [item.modifier for item in self.spellmodifier_set.all().order_by('modifier__friendly_type_name')]

    def delete_spell(self):
        [modifier.delete_modifier() for modifier in self.modifiers]
        self.delete()

    @staticmethod
    def update(spell, spell_data: Dict, modifiers: List[Dict]):
        for attr, value in spell_data.items():
            setattr(spell, attr, value)
        spell.save()
        
        spell.create_or_update_modifiers(modifiers)

    @staticmethod
    def create(user, spell_data: Dict, modifiers: List[Dict]):
        spell = Spell(user=user)
        for attr, value in spell_data.items():
            setattr(spell, attr, value)
        spell.is_homebrew = True
        spell.save()

        spell.create_or_update_modifiers(modifiers)

    def create_or_update_modifiers(self, modifier_objects: List[Dict]):
        modifiers = {str(modifier.id): modifier for modifier in self.modifiers}
        new_ids = []

        for modifier_object in modifier_objects:
            modifier_id = modifier_object.get("id", None)
            new_ids.append(modifier_id)
            if modifier_id in modifiers.keys():
                modifiers[modifier_id].update_modifier(modifier_object)
            else:
                Modifier.create_modifier(self, modifier_object)

        for modifier_id in modifiers.keys():
            if modifier_id not in new_ids:
                modifiers[modifier_id].delete()

    def can_edit(self, user: User = None):
        if not user:
            return False

        if self.is_homebrew == True:
            return self.user == user
        
        else:
            return self.user == user and Setting.get_setting("CAN_EDIT_NON_HOMEBREW", user)['value'] == True

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "level": self.level,
            "school": self.school,
            "duration": json.loads(self.duration) if self.duration else None,
            "activation": json.loads(self.activation) if self.activation else None,
            "range": json.loads(self.range) if self.range else None,
            "as_part_of_weapon_attack": self.as_part_of_weapon_attack,
            "description": self.description,
            "snippet": self.snippet,
            "concentration": self.concentration,
            "ritual": self.ritual,
            "range_area": json.loads(self.range_area) if self.range_area else None,
            "damage_effect": json.loads(self.damage_effect) if self.damage_effect else None,
            "components": json.loads(self.components) if self.components else None,
            "components_description": self.components_description,
            "save_dc_ability_id": self.save_dc_ability_id,
            "healing": self.healing,
            "healing_dice": json.loads(self.healing_dice) if self.healing_dice else None,
            "temp_hp_dice": json.loads(self.temp_hp_dice) if self.temp_hp_dice else None,
            "attack_type": self.attack_type,
            "can_cast_at_higher_level": self.can_cast_at_higher_level,
            "is_homebrew": self.is_homebrew,
            "requires_saving_throw": self.requires_saving_throw,
            "requires_attack_roll": self.requires_attack_roll,
            "at_higher_levels": json.loads(self.at_higher_levels) if self.at_higher_levels else [],
            "conditions": json.loads(self.conditions) if self.conditions else None,
            "tags": json.loads(self.tags) if self.tags else None,
            "casting_time_description": self.casting_time_description,
            "scale_type": self.scale_type,
            "spell_groups": json.loads(self.spell_groups) if self.spell_groups else None,
            "modifiers": [modifier.to_dict() for modifier in self.modifiers],
            "can_edit": self.can_edit(user)
        }

    def to_simple_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "level": self.level,
            "school": self.school,
            "snippet": self.snippet,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user)
        }

class ContentCollectionSpell(models.Model):
    spell = models.ForeignKey(Spell, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionSpell.objects.filter(spell=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionSpell(spell=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionSpell.objects.get(spell=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionSpell.objects.filter(spell=resource, content_collection__user=user).all()

class SpellModifier(models.Model):
    spell = models.ForeignKey(Spell, on_delete=models.CASCADE)
    modifier = models.ForeignKey(Modifier, on_delete=models.CASCADE)

class Action(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    limited_use = models.CharField(max_length=1000, null=True)
    name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=100000, null=True)
    snippet = models.CharField(max_length=1000, null=True)
    ability_modifier_stat_id = models.IntegerField(null=True)
    on_miss_description = models.CharField(max_length=1000, null=True)
    save_fail_description = models.CharField(max_length=1000, null=True)
    save_success_description = models.CharField(max_length=1000, null=True)
    save_stat_id = models.IntegerField(null=True)
    fixed_save_dc = models.CharField(max_length=1000, null=True)
    attack_type_range = models.IntegerField(null=True)
    action_type = models.IntegerField(null=True)
    attack_sub_type = models.IntegerField(null=True)
    dice = models.CharField(max_length=1000, null=True)
    value = models.IntegerField(null=True)
    damage_type_id = models.IntegerField(null=True)
    is_martial_arts = models.BooleanField(null=True)
    is_proficient = models.BooleanField(null=True)
    spell_range_type = models.CharField(max_length=1000, null=True)
    display_as_attack = models.BooleanField(null=True)
    range = models.CharField(max_length=1000, null=True)
    activation = models.CharField(max_length=1000, null=True)
    number_of_targets = models.IntegerField(null=True)
    fixed_to_hit = models.IntegerField(null=True)
    ammunition = models.CharField(max_length=1000, null=True)

    def to_dict(self):

        return {
            "id": str(self.id),
            "limited_use": json.loads(self.limited_use) if self.limited_use else None,
            "name": self.name,
            "description": self.description,
            "snippet": self.snippet,
            "ability_modifier_stat_id": self.ability_modifier_stat_id,
            "on_miss_description": self.on_miss_description,
            "save_fail_description": self.save_fail_description,
            "save_success_description": self.save_success_description,
            "save_stat_id": self.save_stat_id,
            "fixed_save_dc": self.fixed_save_dc,
            "attack_type_range": self.attack_type_range,
            "action_type": self.action_type,
            "attack_sub_type": self.attack_sub_type,
            "dice": json.loads(self.dice),
            "value": self.value,
            "damage_type_id": self.damage_type_id,
            "is_martial_arts": self.is_martial_arts,
            "is_proficient": self.is_proficient,
            "spell_range_type": self.spell_range_type,
            "display_as_attack": self.display_as_attack,
            "range": json.loads(self.range),
            "activation": json.loads(self.activation),
            "number_of_targets": self.number_of_targets,
            "fixed_to_hit": self.fixed_to_hit,
            "ammunition": self.ammunition,
        }

    def delete_action(self):
        self.delete()

    @staticmethod
    def create_action(parent: models.Model, action_object: Dict):
        new_action = Action(
            limited_use = json.dumps(action_object.get("limited_use")) if "limited_use" in action_object.keys() else None,
            name = action_object.get("name", None),
            description = action_object.get("description", None),
            snippet = action_object.get("snippet", None),
            ability_modifier_stat_id = action_object.get("ability_modifier_stat_id", None),
            on_miss_description = action_object.get("on_miss_description", None),
            save_fail_description = action_object.get("save_fail_description", None),
            save_success_description = action_object.get("save_success_description", None),
            save_stat_id = action_object.get("save_stat_id", None),
            fixed_save_dc = action_object.get("fixed_save_dc", None),
            attack_type_range = action_object.get("attack_type_range", None),
            action_type = action_object.get("action_type", None),
            attack_sub_type = action_object.get("attack_sub_type", None),
            dice = json.dumps(action_object.get("dice")) if "dice" in action_object.keys() else None,
            value = action_object.get("value", None),
            damage_type_id = action_object.get("damage_type_id", None),
            is_martial_arts = action_object.get("is_martial_arts", False),
            is_proficient = action_object.get("is_proficient", False),
            spell_range_type = action_object.get("spell_range_type", None),
            display_as_attack = action_object.get("display_as_attack", False),
            range = json.dumps(action_object.get("range")) if "range" in action_object.keys() else None,
            activation = json.dumps(action_object.get("activation")) if "activation" in action_object.keys() else None,
            number_of_targets = action_object.get("number_of_targets", None),
            fixed_to_hit = action_object.get("fixed_to_hit", None),
            ammunition = action_object.get("ammunition", None),
        )
        new_action.save()

        if isinstance(parent, RacialTrait):
            relation_obj = RacialTraitAction(
                racial_trait = parent,
                action = new_action
            )
            relation_obj.save()
        elif isinstance(parent, Option):
            relation_obj = OptionAction(
                option = parent,
                action = new_action
            )
            relation_obj.save()
        elif isinstance(parent, DndClassFeature):
            relation_obj = DndClassFeatureAction(
                dnd_class_feature = parent,
                action = new_action
            )
            relation_obj.save()
        elif isinstance(parent, Feat):
            relation_obj = FeatAction(
                feat = parent,
                action = new_action
            )
            relation_obj.save()


    def update_action(self, action_object: Dict):
        self.limited_use = json.dumps(action_object.get("limited_use")) if "limited_use" in action_object.keys() else None
        self.name = action_object.get("name", None)
        self.description = action_object.get("description", None)
        self.snippet = action_object.get("snippet", None)
        self.ability_modifier_stat_id = action_object.get("ability_modifier_stat_id", None)
        self.on_miss_description = action_object.get("on_miss_description", None)
        self.save_fail_description = action_object.get("save_fail_description", None)
        self.save_success_description = action_object.get("save_success_description", None)
        self.save_stat_id = action_object.get("save_stat_id", None)
        self.fixed_save_dc = action_object.get("fixed_save_dc", None)
        self.attack_type_range = action_object.get("attack_type_range", None)
        self.action_type = action_object.get("action_type", None)
        self.attack_sub_type = action_object.get("attack_sub_type", None)
        self.dice = json.dumps(action_object.get("dice")) if "dice" in action_object.keys() else None
        self.value = action_object.get("value", None)
        self.damage_type_id = action_object.get("damage_type_id", None)
        self.is_martial_arts = action_object.get("is_martial_arts", False)
        self.is_proficient = action_object.get("is_proficient", False)
        self.spell_range_type = action_object.get("spell_range_type", None)
        self.display_as_attack = action_object.get("display_as_attack", False)
        self.range = json.dumps(action_object.get("range")) if "range" in action_object.keys() else None
        self.activation = json.dumps(action_object.get("activation")) if "activation" in action_object.keys() else None
        self.number_of_targets = action_object.get("number_of_targets", None)
        self.fixed_to_hit = action_object.get("fixed_to_hit", None)
        self.ammunition = action_object.get("ammunition", None)
        self.save()

class Item(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    base_type = models.CharField(max_length=100, null=True)
    dex_bonus = models.CharField(max_length=100, null=True)
    source_id = models.IntegerField(null=True)
    can_equip = models.BooleanField(null=True)
    magic = models.BooleanField(null=True)
    name = models.CharField(max_length=1000, null=True)
    snippet = models.CharField(max_length=1000, null=True)
    weight = models.IntegerField(null=True)
    type = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=100000, null=True)
    can_attune = models.BooleanField(null=True)
    attunement_description = models.CharField(max_length=100000, null=True)
    rarity = models.CharField(max_length=1000, null=True)
    stackable = models.BooleanField(null=True)
    bundle_size = models.IntegerField(null=True)
    avatar_url = models.CharField(max_length=1000, null=True)
    large_avatar_url = models.CharField(max_length=1000, null=True)
    filter_type = models.CharField(max_length=1000, null=True)
    cost = models.IntegerField(null=True)
    tags = models.CharField(max_length=1000, null=True)
    sub_type = models.CharField(max_length=1000, null=True)
    is_consumable = models.BooleanField(null=True)
    base_item_id = models.IntegerField(null=True)
    parent_item: Item = models.ForeignKey('self', on_delete=models.CASCADE, null=True)
    base_armor_name = models.CharField(max_length=1000, null=True)
    strength_requirement = models.IntegerField(null=True)
    armor_class = models.IntegerField(null=True)
    stealth_check = models.IntegerField(null=True)
    damage = models.CharField(max_length=100000, null=True) #json blob for now
    damage_type = models.CharField(max_length=1000, null=True)
    fixed_damage = models.IntegerField(null=True)
    properties = models.CharField(max_length=100000, null=True) #json blob for now
    attack_type = models.IntegerField(null=True)
    category_id = models.IntegerField(null=True)
    range = models.IntegerField(null=True)
    long_range = models.IntegerField(null=True)
    is_monk_weapon = models.BooleanField(null=True)
    level_infusion_granted = models.IntegerField(null=True)
    armor_type_id = models.IntegerField(null=True)
    gear_type_id = models.IntegerField(null=True)
    grouped_id = models.IntegerField(null=True)
    can_be_added_to_inventory = models.BooleanField(null=True)
    user_made_content = models.BooleanField(default=False, db_index=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    has_charges = models.BooleanField(null=True)
    charge_number = models.IntegerField(null=True)
    charge_reset = models.CharField(max_length=100, null=True)
    charge_reset_description = models.CharField(max_length=1000, null=True)
    is_simple = models.BooleanField(default=True)
    bonus_to_hit = models.IntegerField(default=0)
    bonus_to_damage = models.IntegerField(default=0)

    class Meta:
        indexes = [
            models.Index(fields=['type',]),
            models.Index(fields=['sub_type',]),
            models.Index(fields=['rarity',]),
            models.Index(fields=['user_made_content',]),
            models.Index(fields=['magic',]),
        ]

    def can_edit(self, user: User = None):
        if not user:
            return False

        if self.user_made_content == True:
            return self.user == user
        
        else:
            return self.user == user and Setting.get_setting("CAN_EDIT_NON_HOMEBREW", user)['value'] == True

    @property
    def modifiers(self) -> List[Modifier]:
        return [item.modifier for item in self.itemmodifier_set.all()]

    @property
    def children(self):
        return self.item_set.all()

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "base_type": self.base_type,
            "dex_bonus": self.dex_bonus,
            "name": self.name,
            "type": self.type,
            "sub_type": self.sub_type,
            "rarity": self.rarity,
            "large_avatar_url": self.large_avatar_url,
            "tags": self.tags,
            "can_equip": self.can_equip,
            "magic": self.magic,
            "snippet": self.snippet,
            "weight": self.weight,
            "description": self.description,
            "can_attune": self.can_attune,
            "attunement_description": self.attunement_description,
            "stackable": self.stackable,
            "bundle_size": self.bundle_size,
            "avatar_url": self.avatar_url,
            "filter_type": self.filter_type,
            "cost": self.cost,
            "is_consumable": self.is_consumable,
            "base_item_id": self.base_item_id,
            "base_armor_name": self.base_armor_name,
            "strength_requirement": self.strength_requirement,
            "armor_class": self.armor_class,
            "stealth_check": self.stealth_check,
            "damage": json.loads(self.damage) if self.damage else None,
            "damage_type": self.damage_type,
            "fixed_damage": self.fixed_damage,
            "properties": json.loads(self.properties) if self.properties else None,
            "attack_type": self.attack_type,
            "category_id": self.category_id,
            "range": self.range,
            "long_range": self.long_range,
            "is_monk_weapon": self.is_monk_weapon,
            "level_infusion_granted": self.level_infusion_granted,
            "armor_type_id": self.armor_type_id,
            "gear_type_id": self.gear_type_id,
            "grouped_id": self.grouped_id,
            "can_be_added_to_inventory": self.can_be_added_to_inventory,
            "modifiers": [modifier.to_dict() for modifier in self.modifiers],
            "can_edit": self.can_edit(user),
            "parent_id": self.parent_item.id if self.parent_item_id else None,
            "parent": self.parent_item.to_dict() if self.parent_item_id else None,
            "has_charges": self.has_charges,
            "charge_number": self.charge_number,
            "charge_reset": self.charge_reset,
            "charge_reset_description": self.charge_reset_description,
            "is_simple": self.is_simple,
            "bonus_to_hit": self.bonus_to_hit,
            "bonus_to_damage": self.bonus_to_damage
        }

    def to_simple_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "base_type": self.base_type,
            "name": self.name,
            "type": self.type,
            "sub_type": self.sub_type,
            "rarity": self.rarity,
            "large_avatar_url": self.large_avatar_url,
            "tags": self.tags,
            "can_edit": self.can_edit(user)
        }

    def delete_item(self):
        [modifier.delete_modifier() for modifier in self.modifiers]
        self.delete()

    @staticmethod
    def create_item(item_object: Dict, user: User):

        parent_id = item_object.get("parent_id", None)
        parent = None

        if parent_id is not None:
            parent = Item.objects.get(pk=parent_id)

        new_item = Item(
            base_type = item_object.get("base_type", None),
            dex_bonus = item_object.get("dex_bonus", None),
            can_equip = item_object.get("can_equip", None),
            magic = item_object.get("magic", None),
            name = item_object.get("name", None),
            snippet = item_object.get("snippet", None),
            weight = item_object.get("weight", None),
            type = item_object.get("type", None),
            description = item_object.get("description", None),
            can_attune = item_object.get("can_attune", None),
            attunement_description = item_object.get("attunement_description", None),
            rarity = item_object.get("rarity", None),
            stackable = item_object.get("stackable", None),
            bundle_size = item_object.get("bundle_size", None),
            avatar_url = item_object.get("avatar_url", None),
            large_avatar_url = item_object.get("large_avatar_url", None),
            cost = item_object.get("cost", None),
            tags = item_object.get("tags", None),
            sub_type = item_object.get("sub_type", None),
            is_consumable = item_object.get("is_consumable", None),
            base_item_id = item_object.get("base_item_id", None),
            base_armor_name = item_object.get("base_armor_name", None),
            strength_requirement = item_object.get("strength_requirement", None),
            armor_class = item_object.get("armor_class", None),
            stealth_check = item_object.get("stealth_check", None),
            damage = json.dumps(item_object.get("damage")) if 'damage' in item_object.keys() else None,
            damage_type = item_object.get("damage_type", None),
            fixed_damage = item_object.get("fixed_damage", None),
            properties = json.dumps(item_object.get("properties")) if 'properties' in item_object.keys() else None,
            attack_type = item_object.get("attack_type", None),
            range = item_object.get("range", None),
            long_range = item_object.get("long_range", None),
            is_monk_weapon = item_object.get("is_monk_weapon", None),
            armor_type_id = item_object.get("armor_type_id", None),
            can_be_added_to_inventory = item_object.get("can_be_added_to_inventory", None),
            user_made_content = True,
            user = user,
            has_charges = item_object.get("has_charges", None),
            charge_number = item_object.get("charge_number", None),
            charge_reset = item_object.get("charge_reset", None),
            charge_reset_description = item_object.get("charge_reset_description", None),
            is_simple = item_object.get("is_simple", True),
            bonus_to_hit = item_object.get("bonus_to_hit", 0),
            bonus_to_damage = item_object.get("bonus_to_damage", 0),
        )
        new_item.save()
        new_item.parent = parent
        new_item.save()

        new_item.create_or_update_modifiers(item_object.get("modifiers", []))

    def update_item(self, item_object: Dict):
        parent_id = item_object.get("parent_id", None)
        parent = None

        if parent_id is not None:
            parent = Item.objects.get(pk=parent_id)

        self.base_type = item_object.get("base_type", None)
        self.dex_bonus = item_object.get("dex_bonus", None)
        self.can_equip = item_object.get("can_equip", None)
        self.magic = item_object.get("magic", None)
        self.name = item_object.get("name", None)
        self.snippet = item_object.get("snippet", None)
        self.weight = item_object.get("weight", None)
        self.type = item_object.get("type", None)
        self.description = item_object.get("description", None)
        self.can_attune = item_object.get("can_attune", None)
        self.attunement_description = item_object.get("attunement_description", None)
        self.rarity = item_object.get("rarity", None)
        self.stackable = item_object.get("stackable", None)
        self.bundle_size = item_object.get("bundle_size", None)
        self.avatar_url = item_object.get("avatar_url", None)
        self.large_avatar_url = item_object.get("large_avatar_url", None)
        self.cost = item_object.get("cost", None)
        self.tags = item_object.get("tags", None)
        self.sub_type = item_object.get("sub_type", None)
        self.is_consumable = item_object.get("is_consumable", None)
        self.base_item_id = item_object.get("base_item_id", None)
        self.base_armor_name = item_object.get("base_armor_name", None)
        self.strength_requirement = item_object.get("strength_requirement", None)
        self.armor_class = item_object.get("armor_class", None)
        self.stealth_check = item_object.get("stealth_check", None)
        self.damage = json.dumps(item_object.get("damage")) if 'damage' in item_object.keys() else None
        self.damage_type = item_object.get("damage_type", None)
        self.fixed_damage = item_object.get("fixed_damage", None)
        self.properties = json.dumps(item_object.get("properties")) if 'properties' in item_object.keys() else None
        self.attack_type = item_object.get("attack_type", None)
        self.range = item_object.get("range", None)
        self.long_range = item_object.get("long_range", None)
        self.is_monk_weapon = item_object.get("is_monk_weapon", None)
        self.armor_type_id = item_object.get("armor_type_id", None)
        self.can_be_added_to_inventory = item_object.get("can_be_added_to_inventory", None)
        self.has_charges = item_object.get("has_charges", None)
        self.charge_number = item_object.get("charge_number", None)
        self.charge_reset = item_object.get("charge_reset", None)
        self.charge_reset_description = item_object.get("charge_reset_description", None)
        self.parent = parent
        self.is_simple = item_object.get("is_simple", True)
        self.bonus_to_hit = item_object.get("bonus_to_hit", 0)
        self.bonus_to_damage = item_object.get("bonus_to_damage", 0)
        self.save()

        self.create_or_update_modifiers(item_object.get("modifiers", []))

    def create_or_update_modifiers(self, modifier_objects: List[Dict]):
        modifiers = {str(modifier.id): modifier for modifier in self.modifiers}
        new_ids = []

        for modifier_object in modifier_objects:
            modifier_id = modifier_object.get("id", None)
            new_ids.append(modifier_id)
            if modifier_id in modifiers.keys():
                modifiers[modifier_id].update_modifier(modifier_object)
            else:
                Modifier.create_modifier(self, modifier_object)

        for modifier_id in modifiers.keys():
            if modifier_id not in new_ids:
                modifiers[modifier_id].delete()

class ContentCollectionItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionItem.objects.filter(item=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionItem(item=resource, content_collection=collection)
        obj.save()
    
    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionItem.objects.get(item=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionItem.objects.filter(item=resource, content_collection__user=user).all()

class ItemModifier(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    modifier = models.ForeignKey(Modifier, on_delete=models.CASCADE)

######################## RACE #############################

class Race(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    is_sub_race = models.BooleanField(null=True)
    base_race_name = models.CharField(max_length=1000, null=True)
    full_name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=100000, null=True)
    avatar_url = models.CharField(max_length=1000, null=True)
    large_avatar_url = models.CharField(max_length=1000, null=True)
    portrait_avatar_url = models.CharField(max_length=1000, null=True)
    is_homebrew = models.BooleanField(null=True)
    sub_race_short_name = models.CharField(max_length=1000, null=True)
    base_name = models.CharField(max_length=1000, null=True)
    weight_speeds = models.CharField(max_length=1000, null=True)
    size = models.CharField(max_length=1000, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def can_edit(self, user: User = None):
        if not user:
            return False

        if self.is_homebrew == True:
            return self.user == user
        
        else:
            return self.user == user and Setting.get_setting("CAN_EDIT_NON_HOMEBREW", user)['value'] == True

    @property
    def racial_traits(self):
        return self.racialtrait_set.all().order_by('display_order')

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "is_sub_race": self.is_sub_race,
            "base_race_name": self.base_race_name,
            "full_name": self.full_name,
            "description": self.description,
            "avatar_url": self.avatar_url,
            "large_avatar_url": self.large_avatar_url,
            "portrait_avatar_url": self.portrait_avatar_url,
            "is_homebrew": self.is_homebrew,
            "sub_race_short_name": self.sub_race_short_name,
            "base_name": self.base_name,
            "weight_speeds": json.loads(self.weight_speeds),
            "size": self.size,
            "racial_traits": [trait.to_dict() for trait in self.racial_traits],
            "can_edit": self.can_edit(user)
        }

    def to_simple_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "is_sub_race": self.is_sub_race,
            "base_race_name": self.base_race_name,
            "full_name": self.full_name,
            "description": self.description,
            "avatar_url": self.avatar_url,
            "large_avatar_url": self.large_avatar_url,
            "portrait_avatar_url": self.portrait_avatar_url,
            "is_homebrew": self.is_homebrew,
            "sub_race_short_name": self.sub_race_short_name,
            "base_name": self.base_name,
            "weight_speeds": json.loads(self.weight_speeds) if self.weight_speeds else None,
            "size": self.size,
            "racial_traits": [trait.to_simple_dict() for trait in self.racial_traits],
            "can_edit": self.can_edit(user)
        }

    def delete_race(self):
        [racial_trait.delete_racial_trait() for racial_trait in self.racial_traits]
        self.delete()

    @staticmethod
    def create_race(race_object: Dict, user: User):
        new_race = Race(
            is_sub_race = race_object.get("is_sub_race", None),
            base_race_name = race_object.get("base_race_name", None),
            full_name = race_object.get("full_name", None),
            description = race_object.get("description", None),
            avatar_url = race_object.get("avatar_url", None),
            large_avatar_url = race_object.get("large_avatar_url", None),
            portrait_avatar_url = race_object.get("portrait_avatar_url", None),
            is_homebrew = True,
            sub_race_short_name = race_object.get("sub_race_short_name", None),
            base_name = race_object.get("base_name", None),
            weight_speeds = json.dumps(race_object.get("weight_speeds")) if 'weight_speeds' in race_object.keys() else None,
            size = race_object.get("size", None),
            user = user,
        )
        new_race.save()
        new_race.create_or_update_racial_traits(race_object.get("racial_traits", []))

    def update_race(self, race_object: Dict):
            self.is_sub_race = race_object.get("is_sub_race", None)
            self.base_race_name = race_object.get("base_race_name", None)
            self.full_name = race_object.get("full_name", None)
            self.description = race_object.get("description", None)
            self.avatar_url = race_object.get("avatar_url", None)
            self.large_avatar_url = race_object.get("large_avatar_url", None)
            self.portrait_avatar_url = race_object.get("portrait_avatar_url", None)
            self.sub_race_short_name = race_object.get("sub_race_short_name", None)
            self.base_name = race_object.get("base_name", None)
            self.weight_speeds = json.dumps(race_object.get("weight_speeds")) if 'weight_speeds' in race_object.keys() else None
            self.size = race_object.get("size", None)
            self.save()

            self.create_or_update_racial_traits(race_object.get("racial_traits", []))

    def create_or_update_racial_traits(self, racial_trait_objects: List[Dict]):
        racial_traits = {str(trait.id): trait for trait in self.racial_traits}
        new_ids = []

        for racial_trait_object in racial_trait_objects:
            trait_id = racial_trait_object.get("id", None)
            new_ids.append(trait_id)
            if trait_id in racial_traits.keys():
                racial_traits[trait_id].update_racial_trait(racial_trait_object)
            else:
                RacialTrait.create_racial_trait(self, racial_trait_object)

        for racial_trait_id in racial_traits.keys():
            if racial_trait_id not in new_ids:
                racial_traits[racial_trait_id].delete()



class ContentCollectionRace(models.Model):
    race = models.ForeignKey(Race, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionRace.objects.filter(race=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionRace(race=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionRace.objects.get(race=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionRace.objects.filter(race=resource, content_collection__user=user).all()


class RacialTrait(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    display_order = models.IntegerField(null=True)
    name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=100000, null=True)
    snippet = models.CharField(max_length=1000, null=True)
    hide_in_builder = models.BooleanField(null=True)
    hide_in_sheet = models.BooleanField(null=True)
    spell_selection = models.CharField(max_length=10000, null=True)
    race = models.ForeignKey(Race, on_delete=models.CASCADE, null=True)

    @property
    def modifiers(self):
        return [item.modifier for item in self.racialtraitmodifier_set.all().order_by('modifier__friendly_type_name')]

    @property
    def actions(self):
        return [item.action for item in self.racialtraitaction_set.all().order_by('action__name')]

    @property
    def option_sets(self):
        return [item.option_set for item in self.racialtraitoptionset_set.all().order_by('option_set__name')]

    def to_dict(self):
        return {
            "id": str(self.id),
            "display_order": self.display_order,
            "name": self.name,
            "description": self.description,
            "snippet": self.snippet,
            "hide_in_builder": self.hide_in_builder,
            "hide_in_sheet": self.hide_in_sheet,
            "spell_selection": json.loads(self.spell_selection) if self.spell_selection else None,
            "actions": [action.to_dict() for action in self.actions],
            "modifiers": [modifier.to_dict() for modifier in self.modifiers],
            "option_sets": [option.to_dict() for option in self.option_sets],
        }

    def to_simple_dict(self):
        return {
            "id": str(self.id),
            "display_order": self.display_order,
            "name": self.name,
            "description": self.description,
            "snippet": self.snippet,
            "hide_in_builder": self.hide_in_builder,
        }

    def delete_racial_trait(self):
        [modifier.delete_modifier() for modifier in self.modifiers]
        [action.delete_action() for action in self.actions]
        [option_set.delete_option_set() for option_set in self.option_sets]
        self.delete()

    @staticmethod
    def create_racial_trait(race: Race, racial_trait_object: Dict):
        new_racial_trait = RacialTrait(
            display_order = racial_trait_object.get("display_order", None),
            name = racial_trait_object.get("name", None),
            description = racial_trait_object.get("description", None),
            snippet = racial_trait_object.get("snippet", None),
            hide_in_builder = racial_trait_object.get("hide_in_builder", None),
            hide_in_sheet = racial_trait_object.get("hide_in_sheet", None),
            spell_selection = json.dumps(racial_trait_object.get("spell_selection")) if 'spell_selection' in racial_trait_object.keys() else None,
            race=race,
        )
        new_racial_trait.save()

        new_racial_trait.create_or_update_modifiers(racial_trait_object.get("modifiers", []))
        new_racial_trait.create_or_update_actions(racial_trait_object.get("actions", []))
        new_racial_trait.create_or_update_option_sets(racial_trait_object.get("option_sets", []))

    def update_racial_trait(self, racial_trait_object: Dict):
        self.display_order = racial_trait_object.get("display_order", None)
        self.name = racial_trait_object.get("name", None)
        self.description = racial_trait_object.get("description", None)
        self.snippet = racial_trait_object.get("snippet", None)
        self.hide_in_builder = racial_trait_object.get("hide_in_builder", None)
        self.hide_in_sheet = racial_trait_object.get("hide_in_sheet", None)
        self.spell_selection = json.dumps(racial_trait_object.get("spell_selection")) if 'spell_selection' in racial_trait_object.keys() else None
        self.save()

        self.create_or_update_modifiers(racial_trait_object.get("modifiers", []))
        self.create_or_update_actions(racial_trait_object.get("actions", []))
        self.create_or_update_option_sets(racial_trait_object.get("option_sets", []))

    def create_or_update_modifiers(self, modifier_objects: List[Dict]):
        modifiers = {str(modifier.id): modifier for modifier in self.modifiers}
        new_ids = []

        for modifier_object in modifier_objects:
            modifier_id = modifier_object.get("id", None)
            new_ids.append(modifier_id)
            if modifier_id in modifiers.keys():
                modifiers[modifier_id].update_modifier(modifier_object)
            else:
                Modifier.create_modifier(self, modifier_object)

        for modifier_id in modifiers.keys():
            if modifier_id not in new_ids:
                modifiers[modifier_id].delete()

    def create_or_update_actions(self, action_objects: List[Dict]):
        actions = {str(action.id): action for action in self.actions}
        new_ids = []

        for action_object in action_objects:
            action_id = action_object.get("id", None)
            new_ids.append(action_id)
            if action_id in actions.keys():
                actions[action_id].update_action(action_object)
            else:
                Action.create_action(self, action_object)

        for action_id in actions.keys():
            if action_id not in new_ids:
                actions[action_id].delete()

    def create_or_update_option_sets(self, option_set_objects: List[Dict]):
        option_sets = {str(option.id): option for option in self.option_sets}
        new_ids = []

        for option_set_object in option_set_objects:
            option_set_id = option_set_object.get("id", None)
            new_ids.append(option_set_id)
            if option_set_id in option_sets.keys():
                option_sets[option_set_id].update_option_set(option_set_object)
            else:
                OptionSet.create_option_set(self, option_set_object)

        for option_set_id in option_sets.keys():
            if option_set_id not in new_ids:
                option_sets[option_set_id].delete()

class RacialTraitModifier(models.Model):
    racial_trait = models.ForeignKey(RacialTrait, on_delete=models.CASCADE)
    modifier = models.ForeignKey(Modifier, on_delete=models.CASCADE)

class RacialTraitAction(models.Model):
    racial_trait = models.ForeignKey(RacialTrait, on_delete=models.CASCADE)
    action = models.ForeignKey(Action, on_delete=models.CASCADE)

######################## OPTION #############################

class Option(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=10000, null=True)
    spell_selection = models.CharField(max_length=10000, null=True)

    @property
    def modifiers(self):
        return [item.modifier for item in self.optionmodifier_set.all().order_by('modifier__friendly_type_name')]

    @property
    def actions(self):
        return [item.action for item in self.optionaction_set.all().order_by('action__name')]

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "spell_selection": json.loads(self.spell_selection) if self.spell_selection else None,
            "modifiers": [modifier.to_dict() for modifier in self.modifiers],
            "actions": [action.to_dict() for action in self.actions],
        }

    def delete_option(self):
        [modifier.delete_modifier() for modifier in self.modifiers]
        [action.delete_action() for action in self.actions]
        self.delete()

    @staticmethod
    def create_option(parent: models.Model, option_object: Dict):
        new_option = Option(
            name = option_object.get("name", None),
            description = option_object.get("description", None),
            spell_selection = json.dumps(option_object.get("spell_selection")) if 'spell_selection' in option_object.keys() else None,
        )
        new_option.save()

        if isinstance(parent, OptionSet):
            relation_obj = OptionSetOption(
                option_set = parent,
                option = new_option
            )
            relation_obj.save()

        new_option.create_or_update_modifiers(option_object.get("modifiers", []))
        new_option.create_or_update_actions(option_object.get("actions", []))

    def update_option(self, option_object: Dict):
        self.name = option_object.get("name", None)
        self.description = option_object.get("description", None)
        self.spell_selection = json.dumps(option_object.get("spell_selection")) if 'spell_selection' in option_object.keys() else None
        self.save()

        self.create_or_update_modifiers(option_object.get("modifiers", []))
        self.create_or_update_actions(option_object.get("actions", []))
    
    def create_or_update_actions(self, action_objects: List[Dict]):
        actions = {str(action.id): action for action in self.actions}
        new_ids = []

        for action_object in action_objects:
            action_id = action_object.get("id", None)
            new_ids.append(action_id)
            if action_id in actions.keys():
                actions[action_id].update_action(action_object)
            else:
                Action.create_action(self, action_object)

        for action_id in actions.keys():
            if action_id not in new_ids:
                actions[action_id].delete()

    def create_or_update_modifiers(self, modifier_objects: List[Dict]):
        modifiers = {str(modifier.id): modifier for modifier in self.modifiers}
        new_ids = []

        for modifier_object in modifier_objects:
            modifier_id = modifier_object.get("id", None)
            new_ids.append(modifier_id)
            if modifier_id in modifiers.keys():
                modifiers[modifier_id].update_modifier(modifier_object)
            else:
                Modifier.create_modifier(self, modifier_object)

        for modifier_id in modifiers.keys():
            if modifier_id not in new_ids:
                modifiers[modifier_id].delete()

class OptionModifier(models.Model):
    option = models.ForeignKey(Option, on_delete=models.CASCADE)
    modifier = models.ForeignKey(Modifier, on_delete=models.CASCADE)

class OptionAction(models.Model):
    option = models.ForeignKey(Option, on_delete=models.CASCADE)
    action = models.ForeignKey(Action, on_delete=models.CASCADE)

######################## BACKGROUND #############################

class Background(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=10000, null=True)
    short_description = models.CharField(max_length=10000, null=True)
    skill_proficiencies_description = models.CharField(max_length=10000, null=True)
    tool_proficiencies_description = models.CharField(max_length=10000, null=True)
    languages_description = models.CharField(max_length=10000, null=True)
    feature_name = models.CharField(max_length=10000, null=True)
    feature_description = models.CharField(max_length=10000, null=True)
    suggested_characteristics_description = models.CharField(max_length=10000, null=True)
    is_homebrew = models.BooleanField(null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def can_edit(self, user: User = None):
        if not user:
            return False

        if self.is_homebrew == True:
            return self.user == user
        
        else:
            return self.user == user and Setting.get_setting("CAN_EDIT_NON_HOMEBREW", user)['value'] == True

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "short_description": self.short_description,
            "skill_proficiencies_description": self.skill_proficiencies_description,
            "tool_proficiencies_description": self.tool_proficiencies_description,
            "languages_description": self.languages_description,
            "feature_name": self.feature_name,
            "feature_description": self.feature_description,
            "suggested_characteristics_description": self.suggested_characteristics_description,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user),
            "modifiers": [modifier.to_dict() for modifier in self.modifiers],
            "traits":  [trait.to_dict() for trait in self.traits],
            "ideals":  [ideal.to_dict() for ideal in self.ideals],
            "bonds":  [bond.to_dict() for bond in self.bonds],
            "flaws":  [flaw.to_dict() for flaw in self.flaws],
            "equipment_set":  self.equipment_set.to_dict() if self.equipment_set else None,
        }

    def to_simple_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "short_description": self.short_description,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user),
        }

    def delete_background(self):
        [modifier.delete_modifier() for modifier in self.modifiers]
        if self.equipment_set:
            self.equipment_set.delete_equipment_set()
        self.delete()

    @staticmethod
    def create_background(background_object: Dict, user: User):
        new_background = Background(
            name = background_object.get("name", None),
            short_description = background_object.get("short_description", None),
            skill_proficiencies_description = background_object.get("skill_proficiencies_description", None),
            tool_proficiencies_description = background_object.get("tool_proficiencies_description", None),
            languages_description = background_object.get("languages_description", None),
            feature_name = background_object.get("feature_name", None),
            feature_description = background_object.get("feature_description", None),
            suggested_characteristics_description = background_object.get("suggested_characteristics_description", None),
            is_homebrew=True,
            user=user,
        )
        new_background.save()

        new_background.create_or_update_modifiers(background_object.get("modifiers", []))
        new_background.create_or_update_traits(background_object.get("traits", []))
        new_background.create_or_update_ideals(background_object.get("ideals", []))
        new_background.create_or_update_bonds(background_object.get("bonds", []))
        new_background.create_or_update_flaws(background_object.get("flaws", []))
        new_background.create_or_update_equipment_set(background_object.get("equipment_set", []))

    def update_background(self, background_object: Dict):
        self.name = background_object.get("name", None)
        self.short_description = background_object.get("short_description", None)
        self.skill_proficiencies_description = background_object.get("skill_proficiencies_description", None)
        self.tool_proficiencies_description = background_object.get("tool_proficiencies_description", None)
        self.languages_description = background_object.get("languages_description", None)
        self.feature_name = background_object.get("feature_name", None)
        self.feature_description = background_object.get("feature_description", None)
        self.suggested_characteristics_description = background_object.get("suggested_characteristics_description", None)
        self.save()

        self.create_or_update_modifiers(background_object.get("modifiers", []))
        self.create_or_update_traits(background_object.get("traits", []))
        self.create_or_update_ideals(background_object.get("ideals", []))
        self.create_or_update_bonds(background_object.get("bonds", []))
        self.create_or_update_flaws(background_object.get("flaws", []))
        self.create_or_update_equipment_set(background_object.get("equipment_set", []))

    @property
    def modifiers(self):
        return [item.modifier for item in self.backgroundmodifier_set.all().order_by('modifier__friendly_type_name')]

    @property
    def traits(self):
        return self.backgroundtrait_set.all().order_by('description')

    @property
    def ideals(self):
        return self.backgroundideal_set.all().order_by('description')
    
    @property
    def bonds(self):
        return self.backgroundbond_set.all().order_by('description')

    @property
    def flaws(self):
        return self.backgroundflaw_set.all().order_by('description')
    
    @property
    def equipment_set(self):
        background_equipment_set = self.backgroundequipmentset_set.first()
        if background_equipment_set:
            return background_equipment_set.equipment_set
        else:
            return None

    def create_or_update_modifiers(self, modifier_objects: List[Dict]):
        modifiers = {str(modifier.id): modifier for modifier in self.modifiers}
        new_ids = []

        for modifier_object in modifier_objects:
            modifier_id = modifier_object.get("id", None)
            new_ids.append(modifier_id)
            if modifier_id in modifiers.keys():
                modifiers[modifier_id].update_modifier(modifier_object)
            else:
                Modifier.create_modifier(self, modifier_object)

        for modifier_id in modifiers.keys():
            if modifier_id not in new_ids:
                modifiers[modifier_id].delete()

    def create_or_update_traits(self, trait_objects: List[Dict]):
        traits = {str(trait.id): trait for trait in self.traits}
        new_ids = []

        for trait_object in trait_objects:
            trait_id = trait_object.get("id", None)
            new_ids.append(trait_id)
            if trait_id in traits.keys():
                traits[trait_id].update_trait(trait_object)
            else:
                BackgroundTrait.create_trait(self, trait_object)

        for trait_id in traits.keys():
            if trait_id not in new_ids:
                traits[trait_id].delete()

    def create_or_update_ideals(self, ideal_objects: List[Dict]):
        ideals = {str(ideal.id): ideal for ideal in self.ideals}
        new_ids = []

        for ideal_object in ideal_objects:
            ideal_id = ideal_object.get("id", None)
            new_ids.append(ideal_id)
            if ideal_id in ideals.keys():
                ideals[ideal_id].update_ideal(ideal_object)
            else:
                BackgroundIdeal.create_ideal(self, ideal_object)

        for ideal_id in ideals.keys():
            if ideal_id not in new_ids:
                ideals[ideal_id].delete()

    def create_or_update_bonds(self, bond_objects: List[Dict]):
        bonds = {str(bond.id): bond for bond in self.bonds}
        new_ids = []

        for bond_object in bond_objects:
            bond_id = bond_object.get("id", None)
            new_ids.append(bond_id)
            if bond_id in bonds.keys():
                bonds[bond_id].update_bond(bond_object)
            else:
                BackgroundBond.create_bond(self, bond_object)

        for bond_id in bonds.keys():
            if bond_id not in new_ids:
                bonds[bond_id].delete()

    def create_or_update_flaws(self, flaw_objects: List[Dict]):
        flaws = {str(flaw.id): flaw for flaw in self.flaws}
        new_ids = []

        for flaw_object in flaw_objects:
            flaw_id = flaw_object.get("id", None)
            new_ids.append(flaw_id)
            if flaw_id in flaws.keys():
                flaws[flaw_id].update_flaw(flaw_object)
            else:
                BackgroundFlaw.create_flaw(self, flaw_object)

        for flaw_id in flaws.keys():
            if flaw_id not in new_ids:
                flaws[flaw_id].delete()

    def create_or_update_equipment_set(self, equipment_set_object: Dict):

        equipment_set_id = equipment_set_object.get("id", None)
        current_equipment_set = self.equipment_set

        if current_equipment_set is not None and equipment_set_id == str(current_equipment_set.id):
            current_equipment_set.update_equipment_set(equipment_set_object)
        else:
            if current_equipment_set is not None:
                current_equipment_set.delete()
            new_equipment_set = EquipmentSet.create_equipment_set(equipment_set_object)
            new_relationship = BackgroundEquipmentSet(
                background=self,
                equipment_set=new_equipment_set
            )
            new_relationship.save()

class BackgroundTrait(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    background = models.ForeignKey(Background, on_delete=models.CASCADE)
    description = models.CharField(max_length=1000, null=True)

    def to_dict(self):
        return {
            "id": str(self.id),
            "description": self.description
        }

    @staticmethod
    def create_trait(background: Background, trait_object: Dict):
        new_trait = BackgroundTrait(
            description = trait_object.get("description", None),
            background = background
        )
        new_trait.save()

    def update_trait(self, trait_object: Dict):
        self.description = trait_object.get("description", None)
        self.save()

class BackgroundIdeal(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    background = models.ForeignKey(Background, on_delete=models.CASCADE)
    description = models.CharField(max_length=1000, null=True)

    def to_dict(self):
        return {
            "id": str(self.id),
            "description": self.description
        }

    @staticmethod
    def create_ideal(background: Background, ideal_object: Dict):
        new_ideal = BackgroundIdeal(
            description = ideal_object.get("description", None),
            background = background
        )
        new_ideal.save()

    def update_ideal(self, ideal_object: Dict):
        self.description = ideal_object.get("description", None)
        self.save()

class BackgroundBond(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    background = models.ForeignKey(Background, on_delete=models.CASCADE)
    description = models.CharField(max_length=1000, null=True)

    def to_dict(self):
        return {
            "id": str(self.id),
            "description": self.description
        }

    @staticmethod
    def create_bond(background: Background, bond_object: Dict):
        new_bond = BackgroundBond(
            description = bond_object.get("description", None),
            background = background
        )
        new_bond.save()

    def update_bond(self, bond_object: Dict):
        self.description = bond_object.get("description", None)
        self.save()

class BackgroundFlaw(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    background = models.ForeignKey(Background, on_delete=models.CASCADE)
    description = models.CharField(max_length=1000, null=True)

    def to_dict(self):
        return {
            "id": str(self.id),
            "description": self.description
        }

    @staticmethod
    def create_flaw(background: Background, flaw_object: Dict):
        new_flaw = BackgroundFlaw(
            description = flaw_object.get("description", None),
            background = background
        )
        new_flaw.save()

    def update_flaw(self, flaw_object: Dict):
        self.description = flaw_object.get("description", None)
        self.save()

class BackgroundModifier(models.Model):

    background = models.ForeignKey(Background, on_delete=models.CASCADE)
    modifier = models.ForeignKey(Modifier, on_delete=models.CASCADE)

######################## EQUIPMENT SET #############################

class EquipmentSet(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    description = models.CharField(max_length=1000, null=True)

    @property
    def groups(self) -> List[EquipmentSetGroup]:
        return self.equipmentsetgroup_set.all()

    def to_dict(self):
        return {
            "id": str(self.id),
            "description": self.description,
            "groups": [group.to_dict() for group in self.groups]
        }

    def delete_equipment_set(self):
        self.delete()

    @staticmethod
    def create_equipment_set(equipment_set_object: Dict):
        new_equipment_set = EquipmentSet(
            description = equipment_set_object.get("description", None),
        )
        new_equipment_set.save()

        new_equipment_set.create_or_update_groups(equipment_set_object.get("groups", []))

        return new_equipment_set

    def update_equipment_set(self, equipment_set_object: Dict):
        self.description = equipment_set_object.get("description", None)
        self.save()

        self.create_or_update_groups(equipment_set_object.get("groups", []))

    def create_or_update_groups(self, group_objects: List[Dict]):
        groups = {str(group.id): group for group in self.groups}
        new_ids = []

        for group_object in group_objects:
            group_id = group_object.get("id", None)
            new_ids.append(group_id)
            if group_id in groups.keys():
                groups[group_id].update_group(group_object)
            else:
                EquipmentSetGroup.create_group(self, group_object)

        for group_id in groups.keys():
            if group_id not in new_ids:
                groups[group_id].delete()

class EquipmentSetGroup(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    equipment_set = models.ForeignKey(EquipmentSet, on_delete=models.CASCADE)

    @property
    def options(self) -> List[EquipmentSetGroupOption]:
        return self.equipmentsetgroupoption_set.all()

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "options": [option.to_dict() for option in self.options]
        }

    @staticmethod
    def create_group(equipment_set: EquipmentSet, equipment_set_group_object: Dict):
        new_equipment_set_group = EquipmentSetGroup(
            name = equipment_set_group_object.get("name", None),
            equipment_set=equipment_set,
        )
        new_equipment_set_group.save()

        new_equipment_set_group.create_or_update_options(equipment_set_group_object.get("options", []))

    def update_group(self, equipment_set_group_object: Dict):
        self.name = equipment_set_group_object.get("name", None)
        self.save()

        self.create_or_update_options(equipment_set_group_object.get("options", []))

    def create_or_update_options(self, option_objects: List[Dict]):
        options = {str(option.id): option for option in self.options}
        new_ids = []

        for option_object in option_objects:
            option_id = option_object.get("id", None)
            new_ids.append(option_id)
            if option_id in options.keys():
                options[option_id].update_option(option_object)
            else:
                EquipmentSetGroupOption.create_option(self, option_object)

        for option_id in options.keys():
            if option_id not in new_ids:
                options[option_id].delete()

class EquipmentSetGroupOption(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    custom_items = models.CharField(max_length=10000, null=True)
    gold = models.IntegerField(null=True)
    select_type = models.CharField(max_length=100, null=True)
    equipment_set_group = models.ForeignKey(EquipmentSetGroup, on_delete=models.CASCADE)
    item_quantity = models.IntegerField(null=False)

    @property
    def option_items(self):
        return self.equipmentsetgroupoptionitem_set.all()

    @property
    def subsets(self):
        return self.equipmentsetgroupoptionsubset_set.all()

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "custom_items": self.custom_items,
            "gold": self.gold,
            "select_type": self.select_type,
            "item_quantity": self.item_quantity,
            "items": [item.to_dict() for item in self.option_items],
            "subsets": [subset.to_dict() for subset in self.subsets]
        }

    def create_option(equipment_set_group: EquipmentSetGroup, equipment_set_group_option_object: Dict):
        new_equipment_set_group_option = EquipmentSetGroupOption(
            name = equipment_set_group_option_object.get("name", None),
            custom_items = equipment_set_group_option_object.get("custom_items", None),
            gold = equipment_set_group_option_object.get("gold", None),
            select_type = equipment_set_group_option_object.get("select_type", None),
            item_quantity = equipment_set_group_option_object.get("item_quantity", None),
            equipment_set_group=equipment_set_group,
        )
        new_equipment_set_group_option.save()

        new_equipment_set_group_option.create_or_update_items(equipment_set_group_option_object.get("items", []))
        new_equipment_set_group_option.create_or_update_subsets(equipment_set_group_option_object.get("subsets", []))

    def update_option(self, equipment_set_group_option_object: Dict):
        self.name = equipment_set_group_option_object.get("name", None)
        self.custom_items = equipment_set_group_option_object.get("custom_items", None)
        self.gold = equipment_set_group_option_object.get("gold", None)
        self.select_type = equipment_set_group_option_object.get("select_type", None)
        self.item_quantity = equipment_set_group_option_object.get("item_quantity", None)
        self.save()

        self.create_or_update_items(equipment_set_group_option_object.get("items", []))
        self.create_or_update_subsets(equipment_set_group_option_object.get("subsets", []))

    def create_or_update_subsets(self, subset_objects: List[Dict]):
        subsets = {str(subset.id): subset for subset in self.subsets}
        new_ids = []

        for subset_object in subset_objects:
            subset_id = subset_object.get("id", None)
            new_ids.append(subset_id)
            if subset_id in subsets.keys():
                subsets[subset_id].update_subset(subset_object)
            else:
                EquipmentSetGroupOptionSubset.create_subset(self, subset_object)

        for subset_id in subsets.keys():
            if subset_id not in new_ids:
                subsets[subset_id].delete()

    def create_or_update_items(self, item_objects: List[Dict]):
        items = {str(option_item.item.id): option_item.item for option_item in self.option_items}
        new_ids = []

        for item_object in item_objects:
            item_id = item_object.get("id", None)
            new_ids.append(item_id)
            if item_id in items.keys():
                continue
            else:
                item_to_add = Item.objects.get(pk=item_id)
                new_relation = EquipmentSetGroupOptionItem(
                    equipment_set_group_option=self,
                    item=item_to_add
                )
                new_relation.save()

        for option_items in self.option_items:
            if str(option_items.item.id) not in new_ids:
                option_items.delete()

class EquipmentSetGroupOptionItem(models.Model):

    equipment_set_group_option = models.ForeignKey(EquipmentSetGroupOption, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    def to_dict(self):
        return {
            "id": str(self.item.id),
            "name": self.item.name,
        }

class EquipmentSetGroupOptionSubset(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    select_type = models.CharField(max_length=100, null=True)
    equipment_set_group_option = models.ForeignKey(EquipmentSetGroupOption, on_delete=models.CASCADE)

    @property
    def option_subset_items(self):
        return self.equipmentsetgroupoptionsubsetitem_set.all()

    def to_dict(self):
        return {
            "id": str(self.id),
            "select_type": self.select_type,
            "items": [item.to_dict() for item in self.option_subset_items]
        }

    def create_subset(equipment_set_group_option: EquipmentSetGroupOption, equipment_set_group_option_subset_object: Dict):
        new_equipment_set_group_option_subset = EquipmentSetGroupOptionSubset(
            select_type = equipment_set_group_option_subset_object.get("select_type", None),
            equipment_set_group_option=equipment_set_group_option,
        )
        new_equipment_set_group_option_subset.save()

        new_equipment_set_group_option_subset.create_or_update_items(equipment_set_group_option_subset_object.get("items", []))

    def update_subset(self, equipment_set_group_option_subset_object: Dict):
        self.select_type = equipment_set_group_option_subset_object.get("select_type", None)
        self.save()

        self.create_or_update_items(equipment_set_group_option_subset_object.get("items", []))

    def create_or_update_items(self, item_objects: List[Dict]):
        items = {str(option_subset_item.item.id): option_subset_item.item for option_subset_item in self.option_subset_items}
        new_ids = []

        for item_object in item_objects:
            item_id = item_object.get("id", None)
            new_ids.append(item_id)
            if item_id in items.keys():
                continue
            else:
                item_to_add = Item.objects.get(pk=item_id)
                new_relation = EquipmentSetGroupOptionSubsetItem(
                    equipment_set_group_option_subset=self,
                    item=item_to_add
                )
                new_relation.save()

        for option_subset_items in self.option_subset_items:
            if str(option_subset_items.item.id) not in new_ids:
                option_subset_items.delete()

class EquipmentSetGroupOptionSubsetItem(models.Model):

    equipment_set_group_option_subset = models.ForeignKey(EquipmentSetGroupOptionSubset, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    def to_dict(self):
        return {
            "id": str(self.item.id),
            "name": self.item.name,
        }

class BackgroundEquipmentSet(models.Model):

    background = models.ForeignKey(Background, on_delete=models.CASCADE, unique=True)
    equipment_set = models.ForeignKey(EquipmentSet, on_delete=models.CASCADE, unique=True)

class ContentCollectionBackground(models.Model):
    background = models.ForeignKey(Background, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionBackground.objects.filter(background=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionBackground(background=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionBackground.objects.get(background=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionBackground.objects.filter(background=resource, content_collection__user=user).all()

######################## DND CLASS #############################

class DndClass(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=100000, null=True)
    subclass_level = models.IntegerField(null=True)
    subclass_title = models.CharField(max_length=100, null=True)
    subclass_description = models.CharField(max_length=10000, null=True)
    has_spells = models.BooleanField(null=True)
    hit_dice = models.IntegerField(null=True)
    base_hit_points = models.IntegerField(null=True)
    hit_point_attribute_id = models.IntegerField(null=True)
    primary_attribute_ids = models.CharField(max_length=100, null=True)
    spell_casting_attribute_id = models.IntegerField(null=True)
    is_ritual_spell_caster = models.BooleanField(null=True)
    spell_data = models.CharField(max_length=10000, null=True)
    has_static_spell_count = models.BooleanField(null=True)
    custom_spell_count = models.CharField(max_length=1000, null=True)
    prerequisites = models.CharField(max_length=10000, null=True)
    portrait_url = models.CharField(max_length=1000, null=True)
    avatar_url = models.CharField(max_length=1000, null=True)
    knows_all_spells = models.BooleanField(null=True)
    are_spells_prepared = models.BooleanField(null=True)
    spell_container_name = models.CharField(max_length=100, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_homebrew = models.BooleanField(default=False, db_index=True)
    multiclass_value = models.FloatField(null=False, default=1)

    @property
    def class_features(self) -> List[DndClassFeature]:
        return [item.dnd_class_feature for item in self.dndclassdndclassfeature_set.all().order_by('dnd_class_feature__name')]

    @property
    def resources(self) -> List[DndClassResource]:
        return [item.dnd_class_resource for item in self.dndclassdndclassresource_set.all().order_by('dnd_class_resource__name')]

    def subclasses(self, user: User) -> List[DndSubClass]:
        return self.dndsubclass_set.all().order_by('name')

    @property
    def equipment_set(self):
        dnd_class_equipment_set = self.dndclassequipmentset_set.first()
        if dnd_class_equipment_set:
            return dnd_class_equipment_set.equipment_set
        else:
            return None

    def can_edit(self, user: User = None):
        if not user:
            return False

        if self.is_homebrew == True:
            return self.user == user
        
        else:
            return self.user == user and Setting.get_setting("CAN_EDIT_NON_HOMEBREW", user)['value'] == True

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "subclass_title": self.subclass_title,
            "subclass_level": self.subclass_level,
            "subclass_description": self.subclass_description,
            "subclasses": [subclass.to_simple_dict(user) for subclass in self.subclasses(user)],
            "has_spells": self.has_spells,
            "hit_dice": self.hit_dice,
            "base_hit_points": self.base_hit_points,
            "hit_point_attribute_id": self.hit_point_attribute_id,
            "primary_attribute_ids": json.loads(self.primary_attribute_ids) if self.primary_attribute_ids else None,
            "spell_casting_attribute_id": self.spell_casting_attribute_id,
            "is_ritual_spell_caster": self.is_ritual_spell_caster,
            "spell_data": json.loads(self.spell_data) if self.spell_data else None,
            "has_static_spell_count": self.has_static_spell_count,
            "custom_spell_count": self.custom_spell_count,
            "prerequisites": json.loads(self.prerequisites) if self.prerequisites else None,
            "portrait_url": self.portrait_url,
            "avatar_url": self.avatar_url,
            "knows_all_spells": self.knows_all_spells,
            "are_spells_prepared": self.are_spells_prepared,
            "spell_container_name": self.spell_container_name,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user),
            "resources": [resource.to_dict() for resource in self.resources],
            "class_features":  [class_feature.to_dict() for class_feature in self.class_features],
            "equipment_set":  self.equipment_set.to_dict() if self.equipment_set else None,
            "multiclass_value": self.multiclass_value
        }

    def to_simple_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "subclasses": [subclass.to_simple_dict(user) for subclass in self.subclasses(user)],
            "portrait_url": self.portrait_url,
            "avatar_url": self.avatar_url,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user),
        }

    def delete_class(self):
        if self.equipment_set:
            self.equipment_set.delete_equipment_set()
        [resource.delete_resource() for resource in self.resources]
        [class_feature.delete_class_feature() for class_feature in self.class_features]
        self.delete()

    @staticmethod
    def create_dnd_class(dnd_class_object: Dict, user: User):
        new_dnd_class = DndClass(
            name = dnd_class_object.get("name", None),
            description = dnd_class_object.get("description", None),
            subclass_level = dnd_class_object.get("subclass_level", None),
            subclass_title = dnd_class_object.get("subclass_title", None),
            subclass_description = dnd_class_object.get("subclass_description", None),
            has_spells = dnd_class_object.get("has_spells", None),
            hit_dice = dnd_class_object.get("hit_dice", None),
            base_hit_points = dnd_class_object.get("base_hit_points", None),
            hit_point_attribute_id = dnd_class_object.get("hit_point_attribute_id", None),
            primary_attribute_ids = json.dumps(dnd_class_object.get("primary_attribute_ids")) if 'primary_attribute_ids' in dnd_class_object.keys() else None,
            spell_casting_attribute_id = dnd_class_object.get("spell_casting_attribute_id", None),
            is_ritual_spell_caster = dnd_class_object.get("is_ritual_spell_caster", None),
            spell_data = json.dumps(dnd_class_object.get("spell_data")) if 'spell_data' in dnd_class_object.keys() else None,
            prerequisites = json.dumps(dnd_class_object.get("prerequisites")) if 'prerequisites' in dnd_class_object.keys() else None,
            portrait_url = dnd_class_object.get("portrait_url", None),
            avatar_url = dnd_class_object.get("avatar_url", None),
            knows_all_spells = dnd_class_object.get("knows_all_spells", False),
            are_spells_prepared = dnd_class_object.get("are_spells_prepared", False),
            spell_container_name = dnd_class_object.get("spell_container_name", None),
            has_static_spell_count = dnd_class_object.get("has_static_spell_count", True),
            custom_spell_count = dnd_class_object.get("custom_spell_count", None),
            multiclass_value = dnd_class_object.get("multiclass_value", 1),
            is_homebrew=True,
            user=user,
        )
        new_dnd_class.save()

        new_dnd_class.create_or_update_resources(dnd_class_object.get("resources", []))
        new_dnd_class.create_or_update_class_features(dnd_class_object.get("class_features", []))
        new_dnd_class.create_or_update_equipment_set(dnd_class_object.get("equipment_set", []))

    def update_dnd_class(self, dnd_class_object: Dict):
        self.name = dnd_class_object.get("name", None)
        self.description = dnd_class_object.get("description", None)
        self.subclass_level = dnd_class_object.get("subclass_level", None)
        self.subclass_title = dnd_class_object.get("subclass_title", None)
        self.subclass_description = dnd_class_object.get("subclass_description", None)
        self.has_spells = dnd_class_object.get("has_spells", None)
        self.hit_dice = dnd_class_object.get("hit_dice", None)
        self.base_hit_points = dnd_class_object.get("base_hit_points", None)
        self.hit_point_attribute_id = dnd_class_object.get("hit_point_attribute_id", None)
        self.primary_attribute_ids = json.dumps(dnd_class_object.get("primary_attribute_ids")) if 'primary_attribute_ids' in dnd_class_object.keys() else None
        self.spell_casting_attribute_id = dnd_class_object.get("spell_casting_attribute_id", None)
        self.is_ritual_spell_caster = dnd_class_object.get("is_ritual_spell_caster", None)
        self.spell_data = json.dumps(dnd_class_object.get("spell_data")) if 'spell_data' in dnd_class_object.keys() else None
        self.prerequisites = json.dumps(dnd_class_object.get("prerequisites")) if 'prerequisites' in dnd_class_object.keys() else None
        self.portrait_url = dnd_class_object.get("portrait_url", None)
        self.avatar_url = dnd_class_object.get("avatar_url", None)
        self.knows_all_spells = dnd_class_object.get("knows_all_spells", None)
        self.are_spells_prepared = dnd_class_object.get("are_spells_prepared", False)
        self.spell_container_name = dnd_class_object.get("spell_container_name", None)
        self.has_static_spell_count = dnd_class_object.get("has_static_spell_count", True)
        self.custom_spell_count = dnd_class_object.get("custom_spell_count", None)
        self.multiclass_value = dnd_class_object.get("multiclass_value", 1)
        self.save()

        self.create_or_update_resources(dnd_class_object.get("resources", []))
        self.create_or_update_class_features(dnd_class_object.get("class_features", []))
        self.create_or_update_equipment_set(dnd_class_object.get("equipment_set", []))

    def create_or_update_resources(self, resource_objects: List[Dict]):
        resources = {str(resource.id): resource for resource in self.resources}
        new_ids = []

        for resource_object in resource_objects:
            resource_id = resource_object.get("id", None)
            new_ids.append(resource_id)
            if resource_id in resources.keys():
                resources[resource_id].update_resource(resource_object)
            else:
                DndClassResource.create_resource(self, resource_object)

        for resource_id in resources.keys():
            if resource_id not in new_ids:
                resources[resource_id].delete()

    def create_or_update_class_features(self, class_feature_objects: List[Dict]):
        class_features = {str(class_feature.id): class_feature for class_feature in self.class_features}
        new_ids = []

        for class_feature_object in class_feature_objects:
            class_feature_id = class_feature_object.get("id", None)
            new_ids.append(class_feature_id)
            if class_feature_id in class_features.keys():
                class_features[class_feature_id].update_class_feature(class_feature_object)
            else:
                DndClassFeature.create_class_feature(self, class_feature_object)

        for class_feature_id in class_features.keys():
            if class_feature_id not in new_ids:
                class_features[class_feature_id].delete()

    def create_or_update_equipment_set(self, equipment_set_object: Dict):

        equipment_set_id = equipment_set_object.get("id", None)
        current_equipment_set = self.equipment_set

        if current_equipment_set is not None and equipment_set_id == str(current_equipment_set.id):
            current_equipment_set.update_equipment_set(equipment_set_object)
        else:
            if current_equipment_set is not None:
                current_equipment_set.delete()
            new_equipment_set = EquipmentSet.create_equipment_set(equipment_set_object)
            new_relationship = DndClassEquipmentSet(
                dnd_class=self,
                equipment_set=new_equipment_set
            )
            new_relationship.save()

class DndClassEquipmentSet(models.Model):

    dnd_class = models.ForeignKey(DndClass, on_delete=models.CASCADE, unique=True)
    equipment_set = models.ForeignKey(EquipmentSet, on_delete=models.CASCADE, unique=True)

class ContentCollectionDndClass(models.Model):
    dnd_class = models.ForeignKey(DndClass, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionDndClass.objects.filter(dnd_class=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionDndClass(dnd_class=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionDndClass.objects.get(dnd_class=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionDndClass.objects.filter(dnd_class=resource, content_collection__user=user).all()

######################## CLASS Feature #############################

class DndClassFeature(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=100000, null=True)
    level = models.IntegerField(null=True)
    spell_selection = models.CharField(max_length=10000, null=True)
    gives_spells = models.BooleanField(null=True)
    hide_in_sheet = models.BooleanField(default=False)

    @property
    def modifiers(self):
        return [item.modifier for item in self.dndclassfeaturemodifier_set.all().order_by('modifier__friendly_type_name')]

    @property
    def actions(self):
        return [item.action for item in self.dndclassfeatureaction_set.all().order_by('action__name')]

    @property
    def option_sets(self):
        return [item.option_set for item in self.dndclassfeatureoptionset_set.all().order_by('option_set__level')]

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "level": self.level,
            "gives_spells": self.gives_spells,
            "spell_selection": json.loads(self.spell_selection) if self.spell_selection else None,
            "modifiers":  [modifier.to_dict() for modifier in self.modifiers],
            "actions":  [action.to_dict() for action in self.actions],
            "option_sets":  [option_set.to_dict() for option_set in self.option_sets],
            "hide_in_sheet": self.hide_in_sheet
        }

    def delete_class_feature(self):
        [modifier.delete_modifier() for modifier in self.modifiers]
        [action.delete_action() for action in self.actions]
        [option_set.delete_option_set() for option_set in self.option_sets]
        self.delete()

    @staticmethod
    def create_class_feature(parent: models.Model, class_feature_object: Dict):
        new_class_feature = DndClassFeature(
            name = class_feature_object.get("name", None),
            description = class_feature_object.get("description", None),
            level = class_feature_object.get("level", None),
            gives_spells = class_feature_object.get("gives_spells", None),
            hide_in_sheet = class_feature_object.get("hide_in_sheet", False),
            spell_selection = json.dumps(class_feature_object.get("spell_selection")) if 'spell_selection' in class_feature_object.keys() else None,
        )
        new_class_feature.save()

        if isinstance(parent, DndClass):
            relation_obj = DndClassDndClassFeature(
                dnd_class = parent,
                dnd_class_feature = new_class_feature
            )
            relation_obj.save()
        if isinstance(parent, DndSubClass):
            relation_obj = DndSubClassDndClassFeature(
                dnd_sub_class = parent,
                dnd_class_feature = new_class_feature
            )
            relation_obj.save()

        new_class_feature.create_or_update_modifiers(class_feature_object.get("modifiers", []))
        new_class_feature.create_or_update_actions(class_feature_object.get("actions", []))
        new_class_feature.create_or_update_option_sets(class_feature_object.get("option_sets", []))

    def update_class_feature(self, class_feature_object: Dict):
        self.name = class_feature_object.get("name", None)
        self.description = class_feature_object.get("description", None)
        self.level = class_feature_object.get("level", None)
        self.gives_spells = class_feature_object.get("gives_spells", None)
        self.hide_in_sheet = class_feature_object.get("hide_in_sheet", False)
        self.spell_selection = json.dumps(class_feature_object.get("spell_selection")) if 'spell_selection' in class_feature_object.keys() else None
        self.save()

        self.create_or_update_modifiers(class_feature_object.get("modifiers", []))
        self.create_or_update_actions(class_feature_object.get("actions", []))
        self.create_or_update_option_sets(class_feature_object.get("option_sets", []))

    def create_or_update_modifiers(self, modifier_objects: List[Dict]):
        modifiers = {str(modifier.id): modifier for modifier in self.modifiers}
        new_ids = []

        for modifier_object in modifier_objects:
            modifier_id = modifier_object.get("id", None)
            new_ids.append(modifier_id)
            if modifier_id in modifiers.keys():
                modifiers[modifier_id].update_modifier(modifier_object)
            else:
                Modifier.create_modifier(self, modifier_object)

        for modifier_id in modifiers.keys():
            if modifier_id not in new_ids:
                modifiers[modifier_id].delete()

    def create_or_update_actions(self, action_objects: List[Dict]):
        actions = {str(action.id): action for action in self.actions}
        new_ids = []

        for action_object in action_objects:
            action_id = action_object.get("id", None)
            new_ids.append(action_id)
            if action_id in actions.keys():
                actions[action_id].update_action(action_object)
            else:
                Action.create_action(self, action_object)

        for action_id in actions.keys():
            if action_id not in new_ids:
                actions[action_id].delete()

    def create_or_update_option_sets(self, option_set_objects: List[Dict]):
        option_sets = {str(option_set.id): option_set for option_set in self.option_sets}
        new_ids = []

        for option_set_object in option_set_objects:
            option_set_id = option_set_object.get("id", None)
            new_ids.append(option_set_id)
            if option_set_id in option_sets.keys():
                option_sets[option_set_id].update_option_set(option_set_object)
            else:
                OptionSet.create_option_set(self, option_set_object)

        for option_set_id in option_sets.keys():
            if option_set_id not in new_ids:
                option_sets[option_set_id].delete()

######################## Option Set #############################

class OptionSet(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    level = models.IntegerField(null=True)

    @property
    def options(self):
        return [item.option for item in self.optionsetoption_set.all().order_by('option__name')]
        
    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "level": self.level,
            "options": [option.to_dict() for option in self.options]
        }

    def delete_option_set(self):
        [option.delete_option() for option in self.options]
        self.delete()


    @staticmethod
    def create_option_set(parent: models.Model, option_set_object: Dict):
        new_option_set = OptionSet(
            name = option_set_object.get("name", None),
            level = option_set_object.get("level", None),
        )
        new_option_set.save()

        if isinstance(parent, DndClassFeature):
            relation_obj = DndClassFeatureOptionSet(
                dnd_class_feature = parent,
                option_set = new_option_set
            )
            relation_obj.save()
        elif isinstance(parent, RacialTrait):
            relation_obj = RacialTraitOptionSet(
                racial_trait = parent,
                option_set = new_option_set
            )
            relation_obj.save()
        elif isinstance(parent, Feat):
            relation_obj = FeatOptionSet(
                feat = parent,
                option_set = new_option_set
            )
            relation_obj.save()

        new_option_set.create_or_update_options(option_set_object.get("options", []))

    def update_option_set(self, option_set_object: Dict):
        self.name = option_set_object.get("name", None)
        self.level = option_set_object.get("level", None)
        self.save()

        self.create_or_update_options(option_set_object.get("options", []))

    def create_or_update_options(self, option_objects: List[Dict]):
        options = {str(option.id): option for option in self.options}
        new_ids = []

        for option_object in option_objects:
            option_id = option_object.get("id", None)
            new_ids.append(option_id)
            if option_id in options.keys():
                options[option_id].update_option(option_object)
            else:
                Option.create_option(self, option_object)

        for option_id in options.keys():
            if option_id not in new_ids:
                options[option_id].delete()

class DndClassDndClassFeature(models.Model):

    dnd_class_feature = models.ForeignKey(DndClassFeature, on_delete=models.CASCADE)
    dnd_class = models.ForeignKey(DndClass, on_delete=models.CASCADE)

class DndClassFeatureModifier(models.Model):

    dnd_class_feature = models.ForeignKey(DndClassFeature, on_delete=models.CASCADE)
    modifier = models.ForeignKey(Modifier, on_delete=models.CASCADE)

class DndClassFeatureAction(models.Model):

    dnd_class_feature = models.ForeignKey(DndClassFeature, on_delete=models.CASCADE)
    action = models.ForeignKey(Action, on_delete=models.CASCADE)

class DndClassFeatureOptionSet(models.Model):

    dnd_class_feature = models.ForeignKey(DndClassFeature, on_delete=models.CASCADE)
    option_set = models.ForeignKey(OptionSet, on_delete=models.CASCADE)

class OptionSetOption(models.Model):

    option = models.ForeignKey(Option, on_delete=models.CASCADE)
    option_set = models.ForeignKey(OptionSet, on_delete=models.CASCADE)

######################## CLASS RESOURCE #############################

class DndClassResource(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    snippet = models.CharField(max_length=10000, null=True)
    description = models.CharField(max_length=100000, null=True)
    reset_type = models.CharField(max_length=100, null=True)
    activation_type = models.IntegerField(null=True)
    resource_count = models.CharField(max_length=10000, null=True)
    at_higher_levels = models.CharField(max_length=10000, null=True)

    @property
    def modifiers(self):
        return [item.modifier for item in self.dndclassresourcemodifier_set.all().order_by('modifier__friendly_type_name')]

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "snippet": self.snippet,
            "description": self.description,
            "reset_type": self.reset_type,
            "activation_type": self.activation_type,
            "resource_count": json.loads(self.resource_count) if self.resource_count else None,
            "at_higher_levels": json.loads(self.at_higher_levels) if self.at_higher_levels else [],
            "modifiers":  [modifier.to_dict() for modifier in self.modifiers],
        }

    def delete_resource(self):
        [modifier.delete_modifier() for modifier in self.modifiers]
        self.delete()

    @staticmethod
    def create_resource(parent: models.Model, resource_object: Dict):
        new_resource = DndClassResource(
            name = resource_object.get("name", None),
            snippet = resource_object.get("snippet", None),
            description = resource_object.get("description", None),
            reset_type = resource_object.get("reset_type", None),
            activation_type = resource_object.get("activation_type", None),
            resource_count = json.dumps(resource_object.get("resource_count")) if 'resource_count' in resource_object.keys() else None,
            at_higher_levels = json.dumps(resource_object.get("at_higher_levels")) if 'at_higher_levels' in resource_object.keys() else json.dumps([]),
        )
        new_resource.save()

        if isinstance(parent, DndClass):
            relation_obj = DndClassDndClassResource(
                dnd_class = parent,
                dnd_class_resource = new_resource
            )
            relation_obj.save()
        elif isinstance(parent, DndSubClass):
            relation_obj = DndSubClassDndClassResource(
                dnd_sub_class = parent,
                dnd_class_resource = new_resource
            )
            relation_obj.save()
        elif isinstance(parent, Feat):
            relation_obj = FeatDndClassResource(
                feat = parent,
                dnd_class_resource = new_resource
            )
            relation_obj.save()

        new_resource.create_or_update_modifiers(resource_object.get("modifiers", []))

    def update_resource(self, resource_object: Dict):
        self.name = resource_object.get("name", None)
        self.snippet = resource_object.get("snippet", None)
        self.description = resource_object.get("description", None)
        self.reset_type = resource_object.get("reset_type", None)
        self.activation_type = resource_object.get("activation_type", None)
        self.resource_count = json.dumps(resource_object.get("resource_count")) if 'resource_count' in resource_object.keys() else None
        self.at_higher_levels = json.dumps(resource_object.get("at_higher_levels")) if 'at_higher_levels' in resource_object.keys() else json.dumps([])
        self.save()

        self.create_or_update_modifiers(resource_object.get("modifiers", []))

    def create_or_update_modifiers(self, modifier_objects: List[Dict]):
        modifiers = {str(modifier.id): modifier for modifier in self.modifiers}
        new_ids = []

        for modifier_object in modifier_objects:
            modifier_id = modifier_object.get("id", None)
            new_ids.append(modifier_id)
            if modifier_id in modifiers.keys():
                modifiers[modifier_id].update_modifier(modifier_object)
            else:
                Modifier.create_modifier(self, modifier_object)

        for modifier_id in modifiers.keys():
            if modifier_id not in new_ids:
                modifiers[modifier_id].delete()

class DndClassDndClassResource(models.Model):

    dnd_class_resource = models.ForeignKey(DndClassResource, on_delete=models.CASCADE)
    dnd_class = models.ForeignKey(DndClass, on_delete=models.CASCADE)

class DndClassResourceModifier(models.Model):

    dnd_class_resource = models.ForeignKey(DndClassResource, on_delete=models.CASCADE)
    modifier = models.ForeignKey(Modifier, on_delete=models.CASCADE)

class RacialTraitOptionSet(models.Model):
    option_set = models.ForeignKey(OptionSet, on_delete=models.CASCADE)
    racial_trait = models.ForeignKey(RacialTrait, on_delete=models.CASCADE)

##################################### Spell List #################################

class SpellList(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    dnd_class = models.ForeignKey(DndClass, on_delete=models.CASCADE)

    def create(user: User, dnd_class: DndClass):
        new_spell_list = SpellList(
            dnd_class = dnd_class,
            user = user,
        )
        new_spell_list.save()

    @property
    def spells(self) -> List[Spell]:
        return [spell_list_spell.spell for spell_list_spell in self.spelllistspell_set.all()]

    def to_simple_dict(self):
        return {
            "id": self.id,
            "class": self.dnd_class.name,
            "spell_count": len(self.spells)
        }

    def to_dict(self):
        return {
            "id": self.id,
            "class": self.dnd_class.name,
            "spells": [spell.to_dict() for spell in self.spells]
        }

    def add_spell(self, spell: Spell):
        spell_list_spell = SpellListSpell(
            spell_list=self,
            spell=spell
        )
        spell_list_spell.save()

    def remove_spell(self, spell_id: str):
        self.spelllistspell_set.where(spell_id=spell_id).delete()


class SpellListSpell(models.Model):
    spell_list = models.ForeignKey(SpellList, on_delete=models.CASCADE)
    spell = models.ForeignKey(Spell, on_delete=models.CASCADE)

class ContentCollectionSpellList(models.Model):
    spell_list = models.ForeignKey(SpellList, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionSpellList.objects.filter(spell_list=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionSpellList(spell_list=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionSpellList.objects.get(spell_list=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionSpellList.objects.filter(spell_list=resource, content_collection__user=user).all()


######################## SUBCLASS #############################

class DndSubClass(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=100000, null=True)
    has_spells = models.BooleanField(null=True)
    override_spells = models.BooleanField(null=True)
    override_resources = models.BooleanField(null=True)
    spell_casting_attribute_id = models.IntegerField(null=True)
    is_ritual_spell_caster = models.BooleanField(null=True)
    spell_data = models.CharField(max_length=10000, null=True)
    has_static_spell_count = models.BooleanField(null=True)
    custom_spell_count = models.CharField(max_length=1000, null=True)
    knows_all_spells = models.BooleanField(null=True)
    are_spells_prepared = models.BooleanField(null=True)
    spell_container_name = models.CharField(max_length=100, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_homebrew = models.BooleanField(default=False, db_index=True)
    dnd_class = models.ForeignKey(DndClass, on_delete=models.CASCADE)

    @property
    def class_features(self):
        return [item.dnd_class_feature for item in self.dndsubclassdndclassfeature_set.all().order_by('dnd_class_feature__name')]

    @property
    def resources(self):
        return [item.dnd_class_resource for item in self.dndsubclassdndclassresource_set.all().order_by('dnd_class_resource__name')]

    def can_edit(self, user: User = None):
        if not user:
            return False

        if self.is_homebrew == True:
            return self.user == user
        
        else:
            return self.user == user and Setting.get_setting("CAN_EDIT_NON_HOMEBREW", user)['value'] == True

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "spell_casting_attribute_id": self.spell_casting_attribute_id,
            "has_spells": self.has_spells,
            "override_spells": self.override_spells,
            "override_resources": self.override_resources,
            "is_ritual_spell_caster": self.is_ritual_spell_caster,
            "spell_data": json.loads(self.spell_data) if self.spell_data else None,
            "has_static_spell_count": self.has_static_spell_count,
            "custom_spell_count": self.custom_spell_count,
            "knows_all_spells": self.knows_all_spells,
            "are_spells_prepared": self.are_spells_prepared,
            "spell_container_name": self.spell_container_name,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user),
            "resources": [resource.to_dict() for resource in self.resources],
            "class_features":  [class_feature.to_dict() for class_feature in self.class_features],
            "class_name": self.dnd_class.name
        }

    def to_simple_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user),
            "class_name": self.dnd_class.name
        }

    def delete_sub_class(self):
        [resource.delete_resource() for resource in self.resources]
        [class_feature.delete_class_feature() for class_feature in self.class_features]
        self.delete()

    @staticmethod
    def create_dnd_sub_class(dnd_class, dnd_class_object: Dict, user: User):
        new_dnd_class = DndSubClass(
            dnd_class=dnd_class,
            name = dnd_class_object.get("name", None),
            description = dnd_class_object.get("description", None),
            has_spells = dnd_class_object.get("has_spells", None),
            override_spells = dnd_class_object.get("override_spells", None),
            override_resources = dnd_class_object.get("override_resources", None),
            spell_casting_attribute_id = dnd_class_object.get("spell_casting_attribute_id", None),
            is_ritual_spell_caster = dnd_class_object.get("is_ritual_spell_caster", None),
            spell_data = json.dumps(dnd_class_object.get("spell_data")) if 'spell_data' in dnd_class_object.keys() else None,
            knows_all_spells = dnd_class_object.get("knows_all_spells", False),
            are_spells_prepared = dnd_class_object.get("are_spells_prepared", False),
            spell_container_name = dnd_class_object.get("spell_container_name", None),
            has_static_spell_count = dnd_class_object.get("has_static_spell_count", True),
            custom_spell_count = dnd_class_object.get("custom_spell_count", None),
            is_homebrew=True,
            user=user,
        )
        new_dnd_class.save()

        new_dnd_class.create_or_update_resources(dnd_class_object.get("resources", []))
        new_dnd_class.create_or_update_class_features(dnd_class_object.get("class_features", []))

    def update_dnd_sub_class(self, dnd_class_object: Dict):
        self.name = dnd_class_object.get("name", None)
        self.description = dnd_class_object.get("description", None)
        self.has_spells = dnd_class_object.get("has_spells", None)
        self.override_spells = dnd_class_object.get("override_spells", None)
        self.override_resources = dnd_class_object.get("override_resources", None)
        self.spell_casting_attribute_id = dnd_class_object.get("spell_casting_attribute_id", None)
        self.is_ritual_spell_caster = dnd_class_object.get("is_ritual_spell_caster", None)
        self.spell_data = json.dumps(dnd_class_object.get("spell_data")) if 'spell_data' in dnd_class_object.keys() else None
        self.knows_all_spells = dnd_class_object.get("knows_all_spells", None)
        self.are_spells_prepared = dnd_class_object.get("are_spells_prepared", False)
        self.spell_container_name = dnd_class_object.get("spell_container_name", None)
        self.has_static_spell_count = dnd_class_object.get("has_static_spell_count", True)
        self.custom_spell_count = dnd_class_object.get("custom_spell_count", None)
        self.save()

        self.create_or_update_resources(dnd_class_object.get("resources", []))
        self.create_or_update_class_features(dnd_class_object.get("class_features", []))

    def create_or_update_resources(self, resource_objects: List[Dict]):
        resources = {str(resource.id): resource for resource in self.resources}
        new_ids = []

        for resource_object in resource_objects:
            resource_id = resource_object.get("id", None)
            new_ids.append(resource_id)
            if resource_id in resources.keys():
                resources[resource_id].update_resource(resource_object)
            else:
                DndClassResource.create_resource(self, resource_object)

        for resource_id in resources.keys():
            if resource_id not in new_ids:
                resources[resource_id].delete()

    def create_or_update_class_features(self, class_feature_objects: List[Dict]):
        class_features = {str(class_feature.id): class_feature for class_feature in self.class_features}
        new_ids = []

        for class_feature_object in class_feature_objects:
            class_feature_id = class_feature_object.get("id", None)
            new_ids.append(class_feature_id)
            if class_feature_id in class_features.keys():
                class_features[class_feature_id].update_class_feature(class_feature_object)
            else:
                DndClassFeature.create_class_feature(self, class_feature_object)

        for class_feature_id in class_features.keys():
            if class_feature_id not in new_ids:
                class_features[class_feature_id].delete()

class ContentCollectionDndSubClass(models.Model):
    dnd_sub_class = models.ForeignKey(DndSubClass, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionDndSubClass.objects.filter(dnd_sub_class=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionDndSubClass(dnd_sub_class=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionDndSubClass.objects.get(dnd_sub_class=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionDndSubClass.objects.filter(dnd_sub_class=resource, content_collection__user=user).all()

class DndSubClassDndClassResource(models.Model):

    dnd_class_resource = models.ForeignKey(DndClassResource, on_delete=models.CASCADE)
    dnd_sub_class = models.ForeignKey(DndSubClass, on_delete=models.CASCADE)

class DndSubClassDndClassFeature(models.Model):

    dnd_class_feature = models.ForeignKey(DndClassFeature, on_delete=models.CASCADE)
    dnd_sub_class = models.ForeignKey(DndSubClass, on_delete=models.CASCADE)

######################## Feat #############################

class Feat(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=100000, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_homebrew = models.BooleanField(default=False, db_index=True)
    prerequisites = models.CharField(max_length=10000, null=True)
    spell_selection = models.CharField(max_length=10000, null=True)
    gives_spells = models.BooleanField(null=True)
    is_selectable = models.BooleanField(default=True, db_index=True)

    @property
    def modifiers(self) -> List[Modifier]:
        return [item.modifier for item in self.featmodifier_set.all().order_by('modifier__friendly_type_name')]

    @property
    def actions(self) -> List[Action]:
        return [item.action for item in self.feataction_set.all().order_by('action__name')]

    @property
    def option_sets(self) -> List[OptionSet]:
        return [item.option_set for item in self.featoptionset_set.all().order_by('option_set__level')]

    @property
    def resources(self) -> List[DndClassResource]:
        return [item.dnd_class_resource for item in self.featdndclassresource_set.all().order_by('dnd_class_resource__name')]

    def can_edit(self, user: User = None):
        if not user:
            return False

        if self.is_homebrew == True:
            return self.user == user
        
        else:
            return self.user == user and Setting.get_setting("CAN_EDIT_NON_HOMEBREW", user)['value'] == True

    def to_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "prerequisites": json.loads(self.prerequisites) if self.prerequisites else None,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user),
            "resources": [resource.to_dict() for resource in self.resources],
            "modifiers": [modifier.to_dict() for modifier in self.modifiers],
            "actions": [action.to_dict() for action in self.actions],
            "option_sets": [option_set.to_dict() for option_set in self.option_sets],
            "gives_spells": self.gives_spells,
            "spell_selection": json.loads(self.spell_selection) if self.spell_selection else None,
            "is_selectable": self.is_selectable,
        }

    def to_simple_dict(self, user: User = None):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "is_homebrew": self.is_homebrew,
            "can_edit": self.can_edit(user),
            "is_selectable": self.is_selectable,
        }

    def delete_feat(self):
        [resource.delete_resource() for resource in self.resources]
        [modifier.delete_modifier() for modifier in self.modifiers]
        [option_set.delete_option_set() for option_set in self.option_sets]
        [action.delete_action() for action in self.actions]
        self.delete()

    @staticmethod
    def create_feat(feat_object: Dict, user: User):
        new_feat = Feat(
            name = feat_object.get("name", None),
            description = feat_object.get("description", None),
            prerequisites = json.dumps(feat_object.get("prerequisites")) if 'prerequisites' in feat_object.keys() else None,
            is_homebrew=True,
            user=user,
            gives_spells = feat_object.get("gives_spells", None),
            is_selectable = feat_object.get("is_selectable", True),
            spell_selection = json.dumps(feat_object.get("spell_selection")) if 'spell_selection' in feat_object.keys() else None,
        )
        new_feat.save()

        new_feat.create_or_update_resources(feat_object.get("resources", []))
        new_feat.create_or_update_modifiers(feat_object.get("modifiers", []))
        new_feat.create_or_update_actions(feat_object.get("actions", []))
        new_feat.create_or_update_option_sets(feat_object.get("option_sets", []))

    def update_feat(self, feat_object: Dict):
        self.name = feat_object.get("name", None)
        self.description = feat_object.get("description", None)
        self.prerequisites = json.dumps(feat_object.get("prerequisites")) if 'prerequisites' in feat_object.keys() else None
        self.gives_spells = feat_object.get("gives_spells", None)
        self.spell_selection = json.dumps(feat_object.get("spell_selection")) if 'spell_selection' in feat_object.keys() else None
        self.is_selectable = feat_object.get("is_selectable", True)
        self.save()

        self.create_or_update_resources(feat_object.get("resources", []))
        self.create_or_update_modifiers(feat_object.get("modifiers", []))
        self.create_or_update_actions(feat_object.get("actions", []))
        self.create_or_update_option_sets(feat_object.get("option_sets", []))

    def create_or_update_resources(self, resource_objects: List[Dict]):
        resources = {str(resource.id): resource for resource in self.resources}
        new_ids = []

        for resource_object in resource_objects:
            resource_id = resource_object.get("id", None)
            new_ids.append(resource_id)
            if resource_id in resources.keys():
                resources[resource_id].update_resource(resource_object)
            else:
                DndClassResource.create_resource(self, resource_object)

        for resource_id in resources.keys():
            if resource_id not in new_ids:
                resources[resource_id].delete()
    
    def create_or_update_modifiers(self, modifier_objects: List[Dict]):
        modifiers = {str(modifier.id): modifier for modifier in self.modifiers}
        new_ids = []

        for modifier_object in modifier_objects:
            modifier_id = modifier_object.get("id", None)
            new_ids.append(modifier_id)
            if modifier_id in modifiers.keys():
                modifiers[modifier_id].update_modifier(modifier_object)
            else:
                Modifier.create_modifier(self, modifier_object)

        for modifier_id in modifiers.keys():
            if modifier_id not in new_ids:
                modifiers[modifier_id].delete()

    def create_or_update_actions(self, action_objects: List[Dict]):
        actions = {str(action.id): action for action in self.actions}
        new_ids = []

        for action_object in action_objects:
            action_id = action_object.get("id", None)
            new_ids.append(action_id)
            if action_id in actions.keys():
                actions[action_id].update_action(action_object)
            else:
                Action.create_action(self, action_object)

        for action_id in actions.keys():
            if action_id not in new_ids:
                actions[action_id].delete()

    def create_or_update_option_sets(self, option_set_objects: List[Dict]):
        option_sets = {str(option_set.id): option_set for option_set in self.option_sets}
        new_ids = []

        for option_set_object in option_set_objects:
            option_set_id = option_set_object.get("id", None)
            new_ids.append(option_set_id)
            if option_set_id in option_sets.keys():
                option_sets[option_set_id].update_option_set(option_set_object)
            else:
                OptionSet.create_option_set(self, option_set_object)

        for option_set_id in option_sets.keys():
            if option_set_id not in new_ids:
                option_sets[option_set_id].delete()

class FeatModifier(models.Model):

    feat = models.ForeignKey(Feat, on_delete=models.CASCADE)
    modifier = models.ForeignKey(Modifier, on_delete=models.CASCADE)

class FeatAction(models.Model):

    feat = models.ForeignKey(Feat, on_delete=models.CASCADE)
    action = models.ForeignKey(Action, on_delete=models.CASCADE)

class FeatOptionSet(models.Model):

    feat = models.ForeignKey(Feat, on_delete=models.CASCADE)
    option_set = models.ForeignKey(OptionSet, on_delete=models.CASCADE)

class FeatDndClassResource(models.Model):

    dnd_class_resource = models.ForeignKey(DndClassResource, on_delete=models.CASCADE)
    feat = models.ForeignKey(Feat, on_delete=models.CASCADE)

class ContentCollectionFeat(models.Model):
    feat = models.ForeignKey(Feat, on_delete=models.CASCADE)
    content_collection = models.ForeignKey(ContentCollection, on_delete=models.CASCADE)

    @staticmethod
    def exists(resource: models.Model, collection: ContentCollection):
        return ContentCollectionFeat.objects.filter(feat=resource, content_collection=collection).exists()
    
    @staticmethod
    def create(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionFeat(feat=resource, content_collection=collection)
        obj.save()

    @staticmethod
    def delete_resource(resource: models.Model, collection: ContentCollection):
        obj = ContentCollectionFeat.objects.get(feat=resource, content_collection=collection)
        obj.delete()

    @staticmethod
    def list(resource: models.Model, user: User):
        return ContentCollectionFeat.objects.filter(feat=resource, content_collection__user=user).all()

##################################### character #################################

class DndCharacter(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=1000, null=True)
    avatar_src = models.CharField(max_length=1000, null=True)

    race: Optional[Race] = models.ForeignKey(Race, on_delete=models.SET_NULL, null=True)
    background: Optional[Background] = models.ForeignKey(Background, on_delete=models.SET_NULL, null=True)

    race_choices = models.CharField(max_length=10000, null=True)
    class_spell_choices = models.CharField(max_length=10000, null=True)
    background_choices = models.CharField(max_length=10000, null=True)
    equipment_choices = models.CharField(max_length=10000, null=True)
    feat_choices = models.CharField(max_length=10000, null=True)
    action_info = models.CharField(max_length=10000, null=True)
    spell_slot_info = models.CharField(max_length=1000, null=True)

    level = models.IntegerField(null=True)

    strength = models.IntegerField(null=True)
    dexterity = models.IntegerField(null=True)
    constitution = models.IntegerField(null=True)
    intelligence = models.IntegerField(null=True)
    wisdom = models.IntegerField(null=True)
    charisma = models.IntegerField(null=True)

    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    
    copper = models.IntegerField(null=False, default=0)
    silver = models.IntegerField(null=False, default=0)
    electrum = models.IntegerField(null=False, default=0)
    gold = models.IntegerField(null=False, default=0)
    platinum = models.IntegerField(null=False, default=0)
    
    rolled_hp = models.IntegerField(default=0)
    current_hp = models.IntegerField(null=True)

    xp = models.IntegerField(null=True)

    @property
    def classes(self) -> List[DnDCharacterClass]:
        return self.dndcharacterclass_set.all()

    @property
    def inventory_items(self) -> List[DnDCharacterItem]:
        return self.dndcharacteritem_set.all().order_by('item_name')

    def to_simple_dict(self) -> Dict[str, Any]:
        return {
            'id': str(self.id),
            'name': self.name,
            'avatar_src': self.avatar_src,
            'race': self.race.full_name if self.race else None,
            'classes': [character_class.dnd_class.name for character_class in self.classes],
            'campaign': self.campaign.name,
            'hp': self.current_hp,
            'level': self.level
        }

    def to_dict(self) -> Dict[str, Any]:
        return {
            'id': str(self.id),
            'name': self.name,
            'avatar_src': self.avatar_src,
            'classes': {str(character_class.dnd_class_id): character_class.to_dict() for character_class in self.classes},
            'campaign_id': str(self.campaign_id),
            'race_id': str(self.race_id) if self.race_id else None,
            'background_id': str(self.background_id) if self.background_id else None,
            'strength': self.strength,
            'dexterity': self.dexterity,
            'constitution': self.constitution,
            'intelligence': self.intelligence,
            'wisdom': self.wisdom,
            'charisma': self.charisma,
            'level': self.level,
            'race_choices': json.loads(self.race_choices) if self.race_choices else {},
            'class_spell_choices': json.loads(self.class_spell_choices) if self.class_spell_choices else {},
            'background_choices': json.loads(self.background_choices) if self.background_choices else {},
            'equipment_choices': json.loads(self.equipment_choices) if self.equipment_choices else {},
            'feat_choices': json.loads(self.feat_choices) if self.feat_choices else {},
            'action_info': json.loads(self.action_info) if self.action_info else {},
            'spell_slot_info': json.loads(self.spell_slot_info) if self.spell_slot_info else {},
            'copper': self.copper,
            'silver': self.silver,
            'electrum': self.electrum,
            'gold': self.gold,
            'platinum': self.platinum,
            'rolled_hp': self.rolled_hp,
            'current_hp': self.current_hp,
            'experience': self.xp
        }

    def to_data_dict(self) -> Dict[str, Any]:
        class_data = {}
        subclass_data = {}
        for character_class in self.classes:
            class_data[str(character_class.dnd_class_id)] = character_class.dnd_class.to_dict()
            if(character_class.dnd_sub_class_id):
                subclass_data[str(character_class.dnd_sub_class_id)] = character_class.dnd_sub_class.to_dict()

        return {
            'class_data': class_data,
            'subclass_data': subclass_data,
            'race': self.race.to_dict() if self.race else None,
            'background': self.background.to_dict() if self.background else None,
        }

    def to_inventory_dict(self):
        return {
            "inventory": [item.to_dict() for item in self.inventory_items]
        }

    @staticmethod
    def create_character(character_object: Dict, user: User) -> DndCharacter:
        from dnd_controller.utils import user_content

        race = None
        race_id = character_object.get("race_id", None)
        if race_id:
            race = user_content.get_race(user).get(pk=race_id)

        background = None
        background_id = character_object.get("background_id", None)
        if background_id:
            background = user_content.get_background(user).get(pk=background_id)

        campaign = None
        campaign_id = character_object.get("campaign_id", None)
        if campaign_id:
            campaign = Campaign.objects.filter(user=user, id=campaign_id).first()

        new_character = DndCharacter(
            name = character_object.get("name", f"{user.display_name}'s Character"),
            avatar_src = character_object.get("avatar_src", None),
            race = race,
            user = user,
            campaign = campaign,
            background = background,
            strength = character_object.get("strength", None),
            dexterity = character_object.get("dexterity", None),
            constitution = character_object.get("constitution", None),
            intelligence = character_object.get("intelligence", None),
            wisdom = character_object.get("wisdom", None),
            charisma = character_object.get("charisma", None),
            level = character_object.get("level", 1),
            xp = character_object.get("experience", None),
            race_choices = json.dumps(character_object.get("race_choices", {})),
            class_spell_choices = json.dumps(character_object.get("class_spell_choices", {})),
            background_choices = json.dumps(character_object.get("background_choices", {})),
            equipment_choices = json.dumps(character_object.get("equipment_choices", {})),
            feat_choices = json.dumps(character_object.get("feat_choices", {})),
            action_info = json.dumps(character_object.get("action_info", {})),
            spell_slot_info = json.dumps(character_object.get("spell_slot_info", {})),
            copper = character_object.get("copper", 0),
            silver = character_object.get("silver", 0),
            electrum = character_object.get("electrum", 0),
            gold = character_object.get("gold", 0),
            platinum = character_object.get("platinum", 0),
            rolled_hp = character_object.get("rolled_hp", 0),
            current_hp = character_object.get("current_hp", None),
        )
        new_character.save()
        new_character.create_or_update_classes(character_object.get("classes", []), user)

        return new_character

    def update_character(self, character_object: Dict, user: User):
        from dnd_controller.utils import user_content

        race = None
        race_id = character_object.get("race_id", None)
        if race_id:
            race = user_content.get_race(user).get(pk=race_id)

        background = None
        background_id = character_object.get("background_id", None)
        if background_id:
            background = user_content.get_background(user).get(pk=background_id)

        self.name = character_object.get("name", f"{user.display_name}'s Character")
        self.avatar_src = character_object.get("avatar_src", None)
        self.race = race
        self.background = background
        self.strength = character_object.get("strength", None)
        self.dexterity = character_object.get("dexterity", None)
        self.constitution = character_object.get("constitution", None)
        self.intelligence = character_object.get("intelligence", None)
        self.wisdom = character_object.get("wisdom", None)
        self.charisma = character_object.get("charisma", None)
        self.level = character_object.get("level", 1)
        self.xp = character_object.get("experience", None)
        self.race_choices = json.dumps(character_object.get("race_choices", {}))
        self.class_spell_choices = json.dumps(character_object.get("class_spell_choices", {}))
        self.background_choices = json.dumps(character_object.get("background_choices", {}))
        self.equipment_choices = json.dumps(character_object.get("equipment_choices", {}))
        self.feat_choices = json.dumps(character_object.get("feat_choices", {}))
        self.action_info = json.dumps(character_object.get("action_info", {}))
        self.spell_slot_info = json.dumps(character_object.get("spell_slot_info", {}))
        self.copper = character_object.get("copper", 0)
        self.silver = character_object.get("silver", 0)
        self.electrum = character_object.get("electrum", 0)
        self.gold = character_object.get("gold", 0)
        self.platinum = character_object.get("platinum", 0)
        self.rolled_hp = character_object.get("rolled_hp", 0)
        self.current_hp = character_object.get("current_hp", None)
        
        self.save()

        self.create_or_update_classes(character_object.get("classes", []), user)

    def patch_character(self, patch_object: Dict[str, Any]):
        if('action_info' in patch_object.keys()):
            self.action_info = json.dumps(patch_object.get("action_info"))
        if('spell_slot_info' in patch_object.keys()):
            self.spell_slot_info = json.dumps(patch_object.get("spell_slot_info"))
        if('current_hp' in patch_object.keys()):
            self.current_hp = patch_object['current_hp']
        if('level' in patch_object.keys()):
            self.level = patch_object['level']
        if('experience' in patch_object.keys()):
            self.xp = patch_object['experience']
        self.save()

    def create_or_update_classes(self, class_objects: List[Dict], user: User):
        dnd_classes = {str(dnd_class.dnd_class.id): dnd_class for dnd_class in self.classes}
        new_ids = []

        for class_object in class_objects:
            dnd_class_id = class_object.get("id", None)
            new_ids.append(dnd_class_id)
            if dnd_class_id in dnd_classes.keys():
                dnd_classes[dnd_class_id].update_character_class(class_object, user)
            else:
                DnDCharacterClass.create_character_class(self, class_object, user)

        for dnd_class_id in dnd_classes.keys():
            if dnd_class_id not in new_ids:
                dnd_classes[dnd_class_id].delete()

    def add_items(self, item_objects: Dict[str, Any], user: User):
        from dnd_controller.utils import user_content
        existing_items = {str(item.item_id) if item.item_id else item.item_name: item for item in self.inventory_items}

        for item_object in item_objects:

            item_id = item_object.get("item_id", None)
            item_name = item_object.get("item_name", None)
            count =  item_object.get("count", 1)
            description = item_object.get("description", None)
            if item_id and item_name:
                raise ValueError("can't have both id and name")
            create = False

            if item_id:
                if(item_id in existing_items.keys() and existing_items[item_id].item.stackable == True):
                    existing_items[item_id].count += 1
                    existing_items[item_id].save()
                else:
                    item: Item = user_content.get_item(user).get(pk=item_id)
                    new_character_item = DnDCharacterItem(
                        item = item,
                        character = self,
                        count = count,
                        item_name = item.name,
                        description = description
                    )
                    new_character_item.save()
                    existing_items[item_id] = new_character_item
            elif item_name:
                if(item_name in existing_items.keys()):
                    existing_items[item_name].count += 1
                    existing_items[item_name].save()
                else:
                    new_character_item = DnDCharacterItem(
                        item = None,
                        character = self,
                        count = count,
                        item_name = item_name,
                        description = description
                    )
                    new_character_item.save()
                    existing_items[item_name] = new_character_item

class DnDCharacterClass(models.Model):

    character: DndCharacter = models.ForeignKey(DndCharacter, on_delete=models.CASCADE)
    dnd_class: DndClass = models.ForeignKey(DndClass, on_delete=models.CASCADE)
    dnd_sub_class: DndSubClass = models.ForeignKey(DndSubClass, on_delete=models.SET_NULL, null=True)
    level = models.IntegerField(null=True)

    class_choice = models.CharField(max_length=10000, null=True)

    def to_dict(self) ->Dict[str, Any]:
        return {
            "id": str(self.dnd_class_id),
            "class_choice": json.loads(self.class_choice) if self.class_choice else {},
            "subclass_id": str(self.dnd_sub_class_id) if self.dnd_sub_class_id else None,
            "level": self.level
        }

    @staticmethod
    def create_character_class(character: DndCharacter, class_object: Dict, user: User):
        from dnd_controller.utils import user_content

        dnd_class = None
        class_id = class_object.get('id', None)
        if class_id:
            dnd_class = user_content.get_dnd_class(user).get(pk=class_id)

        dnd_sub_class = None
        subclass_id = class_object.get('subclass_id', None)
        if subclass_id:
            dnd_sub_class = user_content.get_dnd_sub_class(user).get(pk=subclass_id)

        new_character_class = DnDCharacterClass(
            character = character,
            dnd_class = dnd_class,
            dnd_sub_class = dnd_sub_class,
            level = class_object.get("level", 1),
            class_choice = json.dumps(class_object.get("class_choice", {})),
        )
        new_character_class.save()


    def update_character_class(self, class_object: Dict, user: User):
        from dnd_controller.utils import user_content

        dnd_sub_class = None
        subclass_id = class_object.get('subclass_id', None)
        if subclass_id:
            dnd_sub_class = user_content.get_dnd_sub_class(user).get(pk=subclass_id)

        self.dnd_sub_class = dnd_sub_class
        self.level = class_object.get("level", 1)
        self.class_choice = json.dumps(class_object.get("class_choice", {}))
        self.save()

class DnDCharacterItem(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    character: DndCharacter = models.ForeignKey(DndCharacter, on_delete=models.CASCADE)
    item: Item = models.ForeignKey(Item, on_delete=models.CASCADE, null=True)
    item_name = models.CharField(max_length=500, null=True)
    description = models.CharField(max_length=500, null=True)
    count = models.IntegerField(null=False, default=1)
    is_equipped = models.BooleanField(default=False)

    def to_dict(self):
        return {
            "id": str(self.id),
            "item": self.item.to_dict() if self.item else None,
            "item_name": self.item_name,
            "description": self.description,
            "count": self.count,
            "is_equipped": self.is_equipped,
        }

    def update_character_item(self, item_object):
        if 'item_name' in item_object.keys() and self.item_id is None:
            self.item_name = item_object.get('item_name', None)
        if 'description' in item_object.keys() and self.item_id is None:
            self.description = item_object.get('description', None)
        if 'count' in item_object.keys():
            self.count = item_object.get('count', None)
        if 'is_equipped' in item_object.keys():
            self.is_equipped = item_object.get('is_equipped', False)
        self.save()
