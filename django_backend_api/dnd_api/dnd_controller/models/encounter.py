from __future__ import annotations

from django.db import models
import uuid
import json
from django.core.exceptions import ObjectDoesNotExist
from uuid import uuid4
from typing import Tuple, Dict, List
from dnd_controller.models.audio_file import AudioFile, Playlist
from dnd_controller.models.all import BattleMap, Monster, Campaign, User

class Encounter(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True, default='')
    song = models.ForeignKey("AudioFile", on_delete=models.SET_NULL, null=True)
    playlist = models.ForeignKey("Playlist", on_delete=models.SET_NULL, null=True)
    battlemap = models.ForeignKey("BattleMap", on_delete=models.SET_NULL, null=True)
    custom = models.CharField(max_length=100000, default="[]")
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    campaign = models.ForeignKey("Campaign", on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def get_monster_summary(self) -> str:
        summary_string = ''
        for encounter_monster in self.encountermonster_set.all():
            summary_string = summary_string + f'{encounter_monster.get_summary()}, '
    
        return summary_string[:-2]

    def to_dict(self):
        total_xp, modified_xp, modifier, monster_count = self.get_xp_details() 
        return {
            "id": self.id,
            "name": self.name,
            "encounter_monsters": [encounter_monster.to_dict() for encounter_monster in self.encountermonster_set.order_by('monster__name').all()],
            "summary": self.get_monster_summary(),
            "song": self.song.to_dict() if self.song else None,
            "playlist": self.playlist.to_simple_dict() if self.playlist else None,
            "battlemap": self.battlemap.to_dict() if self.battlemap else None,
            "total_xp": total_xp,
            "modified_xp": modified_xp,
            "modifier": modifier,
            "monster_count": monster_count,
            "custom": json.loads(self.custom),
            "campaign": self.campaign.to_simple_dict() if self.campaign else None,
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "song_id": str(self.song.id) if self.song else None,
            "playlist_id": str(self.playlist.id) if self.playlist else None,
            "battlemap_id": str(self.battlemap.id) if self.battlemap else None,
            "encounter_monsters": [encounter_monster.to_export_dict() for encounter_monster in self.encountermonster_set.order_by('monster__name').all()],
            "custom": self.custom
        }

    @staticmethod
    def import_json(encounters: List[Dict], user: User):
        for encounter in encounters:
            try:
                song = AudioFile.objects.get(pk=encounter['song_id'],user=user)
            except:
                song = None

            try:
                playlist = Playlist.objects.get(pk=encounter['playlist_id'],user=user)
            except:
                playlist = None

            try:
                battlemap = BattleMap.objects.get(pk=encounter['battlemap_id'],user=user)
            except:
                battlemap = None

            encounter_obj = Encounter(
                id = encounter['id'],
                name = encounter['name'],
                song = song,
                playlist = playlist,
                battlemap = battlemap,
                user=user
            )
            encounter_obj.save()
            EncounterMonster.import_json(encounter['encounter_monsters'], encounter_obj, user)

    @staticmethod
    def create_encounter(name: str, user: User):
        encounter = Encounter(name=name, user=user)
        encounter.save()

    @staticmethod
    def delete_by_id(encounter_id: str, user: User):
        encounter = Encounter.objects.get(pk=encounter_id,user=user)
        encounter.delete()

    def get_monster_start_states(self):
        monster_states = []
        for custom_participant in json.loads(self.custom):
            monster_states.append(custom_participant)
        for encounter_monster in self.encountermonster_set.all():
            monster_states.extend(encounter_monster.get_monster_start_states())
        return monster_states

    def get_xp_details(self) -> Tuple[int, int, int]:
        total_xp = 0
        count = 0
        modifier = 1
        for encounter_monster in self.encountermonster_set.all():
            total_xp = total_xp + encounter_monster.get_xp()
            count = count + encounter_monster.count
        
        # https://www.dndbeyond.com/sources/basic-rules/building-combat-encounters
        if count <= 1:
            modifier = 1
        elif count == 2:
            modifier = 1.5
        elif count > 2 and count <= 6:
            modifier = 2
        elif count > 6 and count <= 10:
            modifier = 2.5
        elif count > 10 and count <= 14:
            modifier = 3
        elif count > 14:
            modifier = 4

        return total_xp, total_xp * modifier, modifier, count
        

class EncounterMonster(models.Model):
    TYPE_SINGLE = "SINGLE"
    TYPE_MULTIPLE = "MULTIPLE"
    TYPE_SWARM = "SWARM"

    TYPE_CHOICES = [
        (TYPE_SINGLE, "Downloading audio"),
        (TYPE_MULTIPLE, "Error while downloading"),
        (TYPE_MULTIPLE, "Ready to play"),
    ]

    monster = models.ForeignKey(Monster, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=1000, null=True)
    encounter = models.ForeignKey(Encounter, on_delete=models.CASCADE)
    grouping_type = models.CharField(max_length=50, db_index=True, choices=TYPE_CHOICES, default = TYPE_SINGLE)
    count = models.IntegerField(null=True)
    delayed_count = models.IntegerField(default=0)

    def to_dict(self):
        return {
            "monster": self.monster.to_dict(),
            "display_name": self.display_name,
            "grouping_type": self.grouping_type,
            "count": self.count,
            "delayed_count": self.delayed_count,
            "id": self.pk,
            "xp": self.get_xp()
        }

    def to_export_dict(self):
        return {
            "monster_id": str(self.monster.id),
            "display_name": self.display_name,
            "grouping_type": self.grouping_type,
            "count": self.count,
            "delayed_count": self.delayed_count,
            "id": str(self.pk),
            "encounter_id": str(self.encounter.id)
        }

    @staticmethod
    def import_json(encounter_monsters: List[Dict], encounter: Encounter, user: User):
        for encounter_monster in encounter_monsters:
            try:
                monster = Monster.objects.get(pk=encounter_monster['monster_id'], user=user)
            except:
                raise ValueError(f"No Monster with ID: {encounter_monster['monster_id']}")

            encounter_monster_obj = EncounterMonster(
                display_name = encounter_monster['display_name'],
                grouping_type = encounter_monster['grouping_type'],
                count = encounter_monster['count'],
                encounter = encounter,
                monster= monster,
            )
            encounter_monster_obj.save()

    def get_xp(self) -> int:
        if self.grouping_type == EncounterMonster.TYPE_SINGLE:
            return self.monster.get_xp()
        elif self.grouping_type == EncounterMonster.TYPE_MULTIPLE:
            return self.monster.get_xp() * self.count
        elif self.grouping_type == EncounterMonster.TYPE_SWARM:
            return self.monster.get_xp() * self.count


    def get_summary(self) -> str:
        if self.grouping_type == EncounterMonster.TYPE_SINGLE:
            return self.monster.name
        elif self.grouping_type == EncounterMonster.TYPE_MULTIPLE:
            return f'{self.count} * {self.monster.name}'
        elif self.grouping_type == EncounterMonster.TYPE_SWARM:
            return f'{self.monster.name} Swarm'

    def get_monster_start_states(self):
        alphanumerics = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        monster_states = []
        if self.grouping_type == EncounterMonster.TYPE_SINGLE:
            monster_states.append({
                "type": "MONSTER",
                "id": str(uuid4()),
                "object_id": str(self.monster.id),
                "name": self.monster.name,
                "display_name": self.display_name,
                "damage_taken": 0,
                "conditions": [],
                "grouping_type": self.grouping_type,
                "state": "ALIVE",
                "image": self.monster.image,
                "initiative": None,
                "delayed": 1 if self.delayed_count == 1 else 0,
                "x": None,
                "y": None
            })
        elif self.grouping_type == EncounterMonster.TYPE_MULTIPLE:
            for i in range(0, self.count):
                monster_states.append({
                    "type": "MONSTER",
                    "id": str(uuid4()),
                    "object_id": str(self.monster.id),
                    "name": f'{self.monster.name} - {alphanumerics[i]}',
                    "display_name": f'{self.display_name} - {alphanumerics[i]}',
                    "damage_taken": 0,
                    "conditions": [],
                    "grouping_type": self.grouping_type,
                    "state": "ALIVE",
                    "image": self.monster.image,
                    "initiative": None,
                    "delayed": 1 if self.count - i <= self.delayed_count else 0,
                    "x": None,
                    "y": None
                })
        elif self.grouping_type == EncounterMonster.TYPE_SWARM:
            monster_states.append({
                "type": "MONSTER",
                "id": str(uuid4()),
                "object_id": str(self.monster.id),
                "name": f'{self.monster.name} - Swarm',
                "display_name": f'{self.display_name} - Swarm',
                "damage_taken": 0,
                "conditions": [],
                "grouping_type": self.grouping_type,
                "count": self.count,
                "state": "ALIVE",
                "image": self.monster.image,
                "initiative": None,
                "delayed": 1 if self.delayed_count == 1 else 0,
                "x": None,
                "y": None
            })
        return monster_states

    @staticmethod
    def delete_by_id(encounter_monster_id: str, user: User):
        encounter_monster = EncounterMonster.objects.get(pk=encounter_monster_id)
        if encounter_monster.encounter.user != user:
            raise AttributeError("Invalid User")
        encounter_monster.delete()

    @staticmethod
    def create_or_edit(encounter: Encounter, monster: Monster, grouping_type: str, count: int, display_name: str, delayed_count: int):
        try:
            encounter_monster: EncounterMonster() = EncounterMonster.objects.filter(monster=monster, encounter=encounter).get()
            encounter_monster.grouping_type = grouping_type
            encounter_monster.count = count
            encounter_monster.display_name = display_name
            encounter_monster.delayed_count = delayed_count
            encounter_monster.save()
        except ObjectDoesNotExist:
            encounter_monster = EncounterMonster(
                grouping_type=grouping_type,
                count=count,
                delayed_count=delayed_count
            )
            encounter_monster.encounter = encounter
            encounter_monster.monster = monster
            encounter_monster.display_name = display_name if display_name else monster.name
            encounter_monster.save()


class Combat(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, db_index=True, default='')
    encounter = models.ForeignKey(Encounter, on_delete=models.CASCADE)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    state = models.CharField(max_length=10000, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    @staticmethod
    def create_from_encounter_for_campaign(encounter: Encounter, campaign: Campaign, user: User):
        member_states = campaign.get_character_start_states()
        member_states.extend(
            encounter.get_monster_start_states()
        )

        total_xp, modified_xp, modifier, monster_count = encounter.get_xp_details() 

        starting_state = {
            "members": member_states,
            "combat_state": "ROLLING_INITIATIVE",
            "song_id": str(encounter.song.id) if encounter.song else None,
            "playlist_id": str(encounter.playlist.id) if encounter.playlist else None,
            "current_turn": None,
            "xp": {
                "total_xp": total_xp,
                "modified_xp": modified_xp,
                "modifier": modifier,
                "monster_count": monster_count,
            },
            "battlemap": encounter.battlemap.to_dict() if encounter.battlemap else None
        }
        combat = Combat(
            name = f'{campaign.name} - {encounter.name}',
            encounter=encounter,
            campaign=campaign,
            state=json.dumps(starting_state),
            user=user
        )
        combat.save()
        return combat

    @staticmethod
    def delete_by_id(combat_id, user: User):
        combat = Combat.objects.get(pk=combat_id, user=user)
        combat.delete()

    @staticmethod
    def update_state_by_id(combat_id, state, user: User):
        combat = Combat.objects.get(pk=combat_id, user=user)
        combat.state = json.dumps(state)
        combat.save()

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "state": json.loads(self.state),
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S"),
            "updated_at": self.updated_at.strftime("%Y-%m-%d %H:%M:%S"),
        }

    def to_export_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "state": self.state,
            "encounter_id": str(self.encounter.id),
            "campaign_id": str(self.campaign.id),
            "created_at": str(self.created_at),
            "updated_at": str(self.updated_at),
        }

    @staticmethod
    def import_json(combats: List[Dict], user: User):
        for combat in combats:
            encounter = Encounter.objects.get(pk=combat['encounter_id'], user=user)
            campaign = Campaign.objects.get(pk=combat['campaign_id'], user=user)

            combat_obj = Combat(
                id = combat['id'],
                name = combat['name'],
                state = combat['state'],
                encounter = encounter,
                campaign = campaign,
                user=user,
            )
            combat_obj.save()

    def monsters_dict(self):
        monster_dict = {}
        state = json.loads(self.state)
        for member in state['members']:
            if member['type'] == 'MONSTER' or member['type'] == 'ALLY':
                monster = Monster.objects.get(pk=member['object_id'])
                monster_dict[str(monster.id)] = monster.to_dict()

        return monster_dict