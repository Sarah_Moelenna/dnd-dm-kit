from django.core.management.base import BaseCommand, CommandError
from dnd_controller.models.all import Modifier, Spell, DndClassResource
import json

class Command(BaseCommand):
    help = 'flattens at_higher_levels'

    def handle(self, *args, **options):
        modifiers = Modifier.objects.all()
        for modifier in modifiers:
            at_higher_levels_object = None
            if modifier.at_higher_levels is not None and modifier.at_higher_levels is not 'null':
                at_higher_levels_object = json.loads(modifier.at_higher_levels)
                if(isinstance(at_higher_levels_object, dict) and 'higherLevelDefinitions' in at_higher_levels_object.keys()):
                    at_higher_levels_object = at_higher_levels_object['higherLevelDefinitions']
                elif(isinstance(at_higher_levels_object, list)):
                    pass
                else:
                    at_higher_levels_object = []
            if at_higher_levels_object is None:
                at_higher_levels_object = []

            modifier.at_higher_levels = json.dumps(at_higher_levels_object)
            modifier.save()

        spells = Spell.objects.all()
        for spell in spells:
            at_higher_levels_object = None
            if spell.at_higher_levels is not None and spell.at_higher_levels is not 'null':
                at_higher_levels_object = json.loads(spell.at_higher_levels)
                if(isinstance(at_higher_levels_object, dict) and 'higherLevelDefinitions' in at_higher_levels_object.keys()):
                    at_higher_levels_object = at_higher_levels_object['higherLevelDefinitions']
                elif(isinstance(at_higher_levels_object, list)):
                    pass
                else:
                    at_higher_levels_object = []
            if at_higher_levels_object is None:
                at_higher_levels_object = []

            spell.at_higher_levels = json.dumps(at_higher_levels_object)
            spell.save()

        resources = DndClassResource.objects.all()
        for resource in resources:
            at_higher_levels_object = None
            if resource.at_higher_levels is not None and resource.at_higher_levels is not 'null':
                at_higher_levels_object = json.loads(resource.at_higher_levels)
                if(isinstance(at_higher_levels_object, dict) and 'higherLevelDefinitions' in at_higher_levels_object.keys()):
                    at_higher_levels_object = at_higher_levels_object['higherLevelDefinitions']
                elif(isinstance(at_higher_levels_object, list)):
                    pass
                else:
                    at_higher_levels_object = []
            if at_higher_levels_object is None:
                at_higher_levels_object = []

            resource.at_higher_levels = json.dumps(at_higher_levels_object)
            resource.save()

