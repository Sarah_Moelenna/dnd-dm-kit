from django.core.management.base import BaseCommand, CommandError
from dnd_controller.models.all import Monster
from dnd_controller.utils.monster_scraper import string_challenge_number_to_number

class Command(BaseCommand):
    help = 'updates challenge number on monsters'

    def handle(self, *args, **options):
        monsters = Monster.objects.all()
        for monster in monsters:
            monster.challenge_number = string_challenge_number_to_number(monster.challenge.split(" ")[0])
            monster.save()
            