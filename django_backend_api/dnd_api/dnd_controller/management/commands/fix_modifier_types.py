from django.core.management.base import BaseCommand, CommandError
from dnd_controller.models.all import Modifier
from dnd_controller.utils.type_data import get_type_for_name, get_subtype_for_name_and_type_id

class Command(BaseCommand):
    help = 'fixes modifiers with types set to 0'

    def handle(self, *args, **options):
        modifiers = Modifier.objects.filter(modifier_type_id=0).all()
        for modifier in modifiers:
            type_obj = get_type_for_name(modifier.type)
            if type_obj is not None:
                modifier.modifier_type_id = type_obj['id']

                subtype_obj = get_subtype_for_name_and_type_id(modifier.modifier_type_id, modifier.sub_type)
                if subtype_obj is not None:
                    modifier.modifier_sub_type_id = subtype_obj['id']
                modifier.save()
            