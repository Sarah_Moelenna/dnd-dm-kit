from django.core.management.base import BaseCommand, CommandError
from dnd_controller.models.all import Campaign, Character, Monster , Setting, Collection, Map, BattleMap
from dnd_controller.models import AudioFile, Encounter, Combat
import json
from datetime import datetime
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage


class Command(BaseCommand):
    help = 'Export all data'

    def handle(self, *args, **options):

        folder = f'exports/{str(datetime.now())}/'

        # ------- Monsters ---------- #
        monster_dicts = []
        monsters = Monster.objects.all()
        for monster in monsters:
            model_dict = monster.to_export_dict()
            monster_dicts.append(model_dict)
        
        file_name = f"{folder}/monsters.json"
        file_obj = ContentFile(json.dumps(monster_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        # ------- Audio Files ---------- #
        audio_dicts = []
        audio_files = AudioFile.objects.all()
        for audio_file in audio_files:
            model_dict = audio_file.to_export_dict()
            audio_dicts.append(model_dict)
        
        file_name = f"{folder}/audio_files.json"
        file_obj = ContentFile(json.dumps(audio_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        # ------- Campaigns ---------- #
        campaign_dicts = []
        campaigns = Campaign.objects.all()
        for campaign in campaigns:
            model_dict = campaign.to_export_dict()
            campaign_dicts.append(model_dict)
        
        file_name = f"{folder}/campaigns.json"
        file_obj = ContentFile(json.dumps(campaign_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        # ------- Encounter ---------- #
        encounter_dicts = []
        encounters = Encounter.objects.all()
        for encounter in encounters:
            model_dict = encounter.to_export_dict()
            encounter_dicts.append(model_dict)
        
        file_name = f"{folder}/encounters.json"
        file_obj = ContentFile(json.dumps(encounter_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        # ------- Combat ---------- #
        combat_dicts = []
        combats = Combat.objects.all()
        for combat in combats:
            model_dict = combat.to_export_dict()
            combat_dicts.append(model_dict)
        
        file_name = f"{folder}/combats.json"
        file_obj = ContentFile(json.dumps(combat_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        # ------- Settings ---------- #
        setting_dicts = []
        settings = Setting.objects.all()
        for setting in settings:
            model_dict = setting.to_export_dict()
            setting_dicts.append(model_dict)
        
        file_name = f"{folder}/settings.json"
        file_obj = ContentFile(json.dumps(setting_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        # ------- Collection ---------- #
        collection_dicts = []
        collections = Collection.objects.all()
        for collection in collections:
            model_dict = collection.to_export_dict()
            collection_dicts.append(model_dict)
        
        file_name = f"{folder}/collections.json"
        file_obj = ContentFile(json.dumps(collection_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        # ------- Map ---------- #
        map_dicts = []
        maps = Map.objects.all()
        for map_item in maps:
            model_dict = map_item.to_export_dict()
            map_dicts.append(model_dict)
        
        file_name = f"{folder}/maps.json"
        file_obj = ContentFile(json.dumps(map_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        # ------- BattleMap ---------- #
        map_dicts = []
        maps = BattleMap.objects.all()
        for map_item in maps:
            model_dict = map_item.to_export_dict()
            map_dicts.append(model_dict)
        
        file_name = f"{folder}/battlemaps.json"
        file_obj = ContentFile(json.dumps(map_dicts))
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)