from django.core.management.base import BaseCommand, CommandError
from dnd_controller.models.all import Modifier
import json

class Command(BaseCommand):
    help = 'fixes modifiers to ensure fixed values are set correctly'

    def handle(self, *args, **options):
        modifiers = Modifier.objects.all()
        for modifier in modifiers:
            #print(modifier.id)
            dice_object = None
            if modifier.dice is not None and modifier.dice is not 'null':
                dice_object = json.loads(modifier.dice)
            if dice_object is None:
                dice_object = {"diceCount": None, "diceValue": None, "diceMultiplier": None, "fixedValue": None, "diceString": None}

            if "diceCount" not in dice_object.keys():
                dice_object['diceCount'] = None
            if "diceValue" not in dice_object.keys():
                dice_object['diceValue'] = None
            if "diceMultiplier" not in dice_object.keys():
                dice_object['diceMultiplier'] = None
            if "fixedValue" not in dice_object.keys():
                dice_object['fixedValue'] = None
            if "diceString" not in dice_object.keys():
                dice_object['diceString'] = None

            if modifier.fixed_value is not None and dice_object['fixedValue'] is None:
                dice_object['fixedValue'] = modifier.fixed_value
            elif modifier.fixed_value is None and dice_object['fixedValue'] is not None:
                modifier.fixed_value = dice_object['fixedValue']
            modifier.dice = json.dumps(dice_object)
            modifier.save()