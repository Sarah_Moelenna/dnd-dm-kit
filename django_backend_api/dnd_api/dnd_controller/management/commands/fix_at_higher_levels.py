from django.core.management.base import BaseCommand, CommandError
from dnd_controller.models.all import Modifier, Spell, DndClassResource
import json
from typing import List, Dict, Any

class Command(BaseCommand):
    help = 'fixs at_higher_levels'

    def correct_at_higher_levels_object(self, at_higher_levels_objects: List[Dict[str, Any]]):
        new_objects = []

        for at_higher_levels_object in at_higher_levels_objects:
            if at_higher_levels_object['dice'] is None:
                at_higher_levels_object['dice'] = {"diceCount": None, "diceValue": None, "diceMultiplier": None, "fixedValue": None, "diceString": None}
            if at_higher_levels_object['value'] is not None and at_higher_levels_object['value'] != 0:
                if at_higher_levels_object['dice']['fixedValue'] is None or at_higher_levels_object['dice']['fixedValue'] == 0:
                    at_higher_levels_object['dice']['fixedValue'] = at_higher_levels_object['value']
                    at_higher_levels_object.pop('value')
                    new_objects.append(at_higher_levels_object)
                else:
                    raise ValueError(json.dumps(at_higher_levels_object))
            else:
                at_higher_levels_object.pop('value')
                new_objects.append(at_higher_levels_object)

        return new_objects

    def handle(self, *args, **options):
        modifiers = Modifier.objects.all()
        for modifier in modifiers:
            at_higher_levels_objects = json.loads(modifier.at_higher_levels)
            if(len(at_higher_levels_objects) > 0):
                at_higher_levels_objects = self.correct_at_higher_levels_object(at_higher_levels_objects)

                modifier.at_higher_levels = json.dumps(at_higher_levels_objects)
                modifier.save()

        spells = Spell.objects.all()
        for spell in spells:
            at_higher_levels_objects = json.loads(spell.at_higher_levels)
            if(len(at_higher_levels_objects) > 0):
                at_higher_levels_objects = self.correct_at_higher_levels_object(at_higher_levels_objects)

                spell.at_higher_levels = json.dumps(at_higher_levels_objects)
                spell.save()

        resources = DndClassResource.objects.all()
        for resource in resources:
            at_higher_levels_objects = json.loads(resource.at_higher_levels)
            if(len(at_higher_levels_objects) > 0):
                at_higher_levels_objects = self.correct_at_higher_levels_object(at_higher_levels_objects)

                resource.at_higher_levels = json.dumps(at_higher_levels_objects)
                resource.save()


    