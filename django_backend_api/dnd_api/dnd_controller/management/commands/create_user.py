from django.core.management.base import BaseCommand
from dnd_controller.models.all import User

class Command(BaseCommand):
    help = 'create a user'

    def handle(self, *args, **options):
        username = input('Enter username: ')
        password = input('Enter password: ')
        
        user = User.create_user(username, password)
        user.save()
            