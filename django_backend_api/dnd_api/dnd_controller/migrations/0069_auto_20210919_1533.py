# Generated by Django 3.2 on 2021-09-19 14:33

from django.db.models import Q
from django.db import migrations

def create_starter_content_collections(apps, schema_editor):

    Monster = apps.get_model('dnd_controller', 'Monster')
    BattleMap = apps.get_model('dnd_controller', 'BattleMap')
    Spell = apps.get_model('dnd_controller', 'Spell')
    Item = apps.get_model('dnd_controller', 'Item')
    Race = apps.get_model('dnd_controller', 'Race')

    ContentCollectionMonster = apps.get_model('dnd_controller', 'ContentCollectionMonster')
    ContentCollectionBattleMap = apps.get_model('dnd_controller', 'ContentCollectionBattleMap')
    ContentCollectionSpell = apps.get_model('dnd_controller', 'ContentCollectionSpell')
    ContentCollectionItem = apps.get_model('dnd_controller', 'ContentCollectionItem')
    ContentCollectionRace = apps.get_model('dnd_controller', 'ContentCollectionRace')

    ContentCollection = apps.get_model('dnd_controller', 'ContentCollection')

    User = apps.get_model('dnd_controller', 'User')

    UserContentCollection = apps.get_model('dnd_controller', 'UserContentCollection')

    admin_user = User.objects.get(id="4e5757f2-26bd-4ab0-9c02-3c3c5f4ac3bc")

    official_content = ContentCollection(
        name="Official DnD Content",
        is_shareable=True,
        is_default=True,
        user= admin_user
    )
    official_content.save()

    dm_kit_battlemaps = ContentCollection(
        name="Dm's Kit Free Battle Maps",
        is_shareable=True,
        is_default=True,
        user= admin_user
    )
    dm_kit_battlemaps.save()
    
    # add non homebrew to official content
    for monster in Monster.objects.all().filter(user=admin_user).filter(user_made_content=False):
        content_collection_monster = ContentCollectionMonster(
            monster=monster,
            content_collection=official_content,
        )
        content_collection_monster.save()

    for spell in Spell.objects.all().filter(user=admin_user).filter(is_homebrew=False):
        content_collection_spell = ContentCollectionSpell(
            spell=spell,
            content_collection=official_content,
        )
        content_collection_spell.save()

    for item in Item.objects.all().filter(user=admin_user).filter(user_made_content=False):
        content_collection_item = ContentCollectionItem(
            item=item,
            content_collection=official_content,
        )
        content_collection_item.save()

    for race in Race.objects.all().filter(user=admin_user).filter(is_homebrew=False):
        content_collection_race = ContentCollectionRace(
            race=race,
            content_collection=official_content,
        )
        content_collection_race.save()

    # add battlemaps to free battlemaps
    for battlemap in BattleMap.objects.all().filter(user=admin_user):
        content_collection_battlemap = ContentCollectionBattleMap(
            battlemap=battlemap,
            content_collection=dm_kit_battlemaps,
        )
        content_collection_battlemap.save()

    # add content collections to users
    for user in User.objects.all().filter(~Q(id=admin_user.id)):

        user_official_content = UserContentCollection(
            user=user,
            content_collection=official_content
        )
        user_official_content.save()

        user_battlemaps = UserContentCollection(
            user=user,
            content_collection=dm_kit_battlemaps
        )
        user_battlemaps.save()
            

class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0068_usercontentcollection'),
    ]

    operations = [
        migrations.RunPython(create_starter_content_collections),
    ]
