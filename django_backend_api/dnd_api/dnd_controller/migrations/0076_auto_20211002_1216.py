# Generated by Django 3.2 on 2021-10-02 11:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0075_racialtraitoption'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='armorClass',
            new_name='armor_class',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='armorTypeId',
            new_name='armor_type_td',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='attackType',
            new_name='attack_type',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='attunementDescription',
            new_name='attunement_description',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='avatarUrl',
            new_name='avatar_url',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='baseArmorName',
            new_name='base_armor_name',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='baseItemId',
            new_name='base_item_id',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='bundleSize',
            new_name='bundle_size',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='canAttune',
            new_name='can_attune',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='canBeAddedToInventory',
            new_name='can_be_added_to_inventory',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='canEquip',
            new_name='can_equip',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='categoryId',
            new_name='category_id',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='damageType',
            new_name='damage_type',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='filterType',
            new_name='filter_type',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='fixedDamage',
            new_name='fixed_damage',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='gearTypeId',
            new_name='gear_type_id',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='groupedId',
            new_name='grouped_id',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='isConsumable',
            new_name='is_consumable',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='isMonkWeapon',
            new_name='is_monk_weapon',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='isPack',
            new_name='is_pack',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='largeAvatarUrl',
            new_name='large_avatar_url',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='levelInfusionGranted',
            new_name='level_infusion_granted',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='stealthCheck',
            new_name='stealth_check',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='strengthRequirement',
            new_name='strength_requirement',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='subType',
            new_name='sub_type',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='weaponBehaviors',
            new_name='weapon_wehaviors',
        ),
        migrations.RemoveField(
            model_name='item',
            name='is_html',
        ),
    ]
