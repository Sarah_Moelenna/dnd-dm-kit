# Generated by Django 3.2 on 2022-06-06 13:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0119_contentcollectionsubdndclass'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dndsubclass',
            name='avatar_url',
        ),
        migrations.RemoveField(
            model_name='dndsubclass',
            name='portrait_url',
        ),
    ]
