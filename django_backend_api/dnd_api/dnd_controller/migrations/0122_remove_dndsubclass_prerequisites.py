# Generated by Django 3.2 on 2022-06-06 14:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0121_auto_20220606_1510'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dndsubclass',
            name='prerequisites',
        ),
    ]
