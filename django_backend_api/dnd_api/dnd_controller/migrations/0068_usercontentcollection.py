# Generated by Django 3.2 on 2021-09-19 14:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0067_contentcollection_contentcollectionbattlemap_contentcollectionitem_contentcollectionmonster_contentc'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserContentCollection',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content_collection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dnd_controller.contentcollection')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dnd_controller.user')),
            ],
        ),
    ]
