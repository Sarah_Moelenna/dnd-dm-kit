# Generated by Django 3.2 on 2021-06-28 09:50

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0029_alter_map_data'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ally',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('display_name', models.CharField(db_index=True, max_length=1000, null=True)),
                ('tracking', models.BooleanField(db_index=True, default=False)),
                ('damage_taken', models.IntegerField(default=0)),
                ('max_health', models.IntegerField()),
                ('campaign', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dnd_controller.campaign')),
                ('monster', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dnd_controller.monster')),
            ],
        ),
    ]
