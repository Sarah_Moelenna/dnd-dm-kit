# Generated by Django 3.2 on 2021-04-16 21:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0010_alter_monster_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monster',
            name='challenge',
            field=models.CharField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='monster',
            name='proficiency_bonus',
            field=models.CharField(max_length=1000, null=True),
        ),
    ]
