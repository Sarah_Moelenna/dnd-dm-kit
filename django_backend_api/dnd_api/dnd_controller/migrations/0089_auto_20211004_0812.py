# Generated by Django 3.2 on 2021-10-04 07:12

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0088_background_backgroundequipmentset_backgroundmodifier_equipmentset_equipmentsetgroup_equipmentsetgrou'),
    ]

    operations = [
        migrations.RenameField(
            model_name='background',
            old_name='short_escription',
            new_name='short_description',
        ),
        migrations.CreateModel(
            name='BackgroundTrait',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=1000, null=True)),
                ('background', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dnd_controller.background')),
            ],
        ),
        migrations.CreateModel(
            name='BackgroundIdeal',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=1000, null=True)),
                ('background', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dnd_controller.background')),
            ],
        ),
        migrations.CreateModel(
            name='BackgroundFlaw',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=1000, null=True)),
                ('background', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dnd_controller.background')),
            ],
        ),
        migrations.CreateModel(
            name='BackgroundBond',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=1000, null=True)),
                ('background', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dnd_controller.background')),
            ],
        ),
    ]
