# Generated by Django 3.2 on 2021-07-18 21:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0044_encounter_battlemap'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaigncollection',
            name='sharing',
            field=models.BooleanField(default=False),
        ),
    ]
