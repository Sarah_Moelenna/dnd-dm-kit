# Generated by Django 3.2 on 2022-06-13 12:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0142_dndclassfeature_hide_in_sheet'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='bonus_to_damage',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='item',
            name='bonus_to_hit',
            field=models.IntegerField(default=0),
        ),
    ]
