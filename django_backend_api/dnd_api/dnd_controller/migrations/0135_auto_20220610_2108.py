# Generated by Django 3.2 on 2022-06-10 20:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0134_auto_20220610_2045'),
    ]

    operations = [
        migrations.AddField(
            model_name='dndcharacter',
            name='copper',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='dndcharacter',
            name='electrum',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='dndcharacter',
            name='gold',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='dndcharacter',
            name='platinum',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='dndcharacter',
            name='silver',
            field=models.IntegerField(default=0),
        ),
    ]
