# Generated by Django 3.2 on 2021-10-02 11:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dnd_controller', '0076_auto_20211002_1216'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='weapon_wehaviors',
            new_name='weapon_behaviors',
        ),
    ]
