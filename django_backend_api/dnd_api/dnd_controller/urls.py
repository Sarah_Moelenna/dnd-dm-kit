from django.urls import path

from .views import all as views
from .views import character
from .views import spell_list
from .views import dnd_sub_class
from .views import feat
from .views import audio
from .views import encounter
from .views import effect_deck

urlpatterns = [
    path('', views.index, name='index'),
    
    path('login', views.login, name='login'),
    path('validate', views.validate_auth, name='validate_auth'),
    path('account', views.account, name='account'),

    path('audio/file/<str:filename>', audio.audio_file, name='audio'),
    path('audio/upload', audio.FileUploadView.as_view(), name='audio_upload'),
    path('audio/<str:audio_id>', audio.audio_by_id, name="audio_by_id"),
    path('audio', audio.audio, name='audio'),
    path('playlist/<str:playlist_id>/audio/<str:audio_id>', audio.playlist_by_id_item, name="playlist_by_id_item"),
    path('playlist/<str:playlist_id>', audio.playlist_by_id, name="playlist_by_id"),
    path('playlist', audio.playlist, name='playlist'),
    path('user/music', audio.user_music, name='user'),

    path('effect_deck/<str:deck_id>', effect_deck.effect_deck_by_id, name="effect_deck_by_id"),
    path('effect_deck', effect_deck.effect_deck, name='effect_deck'),

    path('commands', views.commands, name='commands'),
    path('character-tracker', views.character_tracker, name='character_tracker'),
    path('bot-state', views.bot_state, name='bot_state'),
    path('user', views.user, name='user'),
    path('settings', views.settings, name='settings'),
    path('user-settings', views.user_settings, name='user_settings'),

    path('race-scrape', views.race_scrape, name='race_scrape'),
    path('spell-scrape', views.spell_scrape, name='spell_scrape'),
    path('item-scrape', views.item_scrape, name='item_scrape'),
    path('background-scrape', views.background_scrape, name='background_scrape'),
    path('class-scrape', views.class_scrape, name='class_scrape'),

    path('item/filters', views.item_filters, name='item_filters'),
    path('item/options', views.item_options, name='item_options'),
    path('item/<str:item_id>', views.item_by_id, name='item_by_id'),
    path('item', views.item, name='item'),

    path('monster-scrape', views.monster_scrape, name='monster_scrape'),
    path('monster/<str:monster_id>', views.monster_by_id, name='monster_by_id'),
    path('monsters', views.monsters, name='monsters'),

    path('campaign/<str:campaign_id>/session', views.campaign_by_id_session, name='campaign_by_id_session'),
    path('campaign/<str:campaign_id>/collection', views.campaign_by_id_collection, name='campaign_by_id_collection'),
    path('campaign/<str:campaign_id>', views.campaign_by_id, name='campaign_by_id'),
    path('campaign', views.campaign, name='campaign'),

    
    path('session/page/<str:page_id>', views.session_page_by_id, name='session_page_by_id'),
    path('session/collection', views.session_collection, name='session_collection'),
    path('session/login', views.session_login, name='session_login'),
    path('session/<str:session_id>', views.session_by_id, name='session_by_id'),

    path('dnd-character/<str:character_id>/inventory/<str:item_id>', character.character_items_by_id, name='character_items_by_id'),
    path('dnd-character/<str:character_id>/inventory', character.character_items, name='character_items'),
    path('dnd-character/<str:character_id>/data', character.character_data_by_id, name='dnd_character_data_by_id'),
    path('dnd-character/<str:character_id>', character.character_by_id, name='dnd_character_by_id'),
    path('dnd-character', character.character, name='dnd_character'),

    path('character/<str:character_id>/tracking', views.track_character_by_id, name='track_character_by_id'),
    path('character/<str:character_id>', views.character_by_id, name='character_by_id'),
    path('character', views.character, name='character'),
    path('ally/<str:ally_id>', views.ally_by_id, name='ally_by_id'),
    path('ally', views.ally, name='ally'),

    path('encounter/<str:encounter_id>/encounter_monster', encounter.encounter_encounter_monster, name='encounter_encounter_monster'),
    path('encounter/<str:encounter_id>', encounter.encounter_by_id, name='encounter_by_id'),
    path('encounter', encounter.encounters, name='encounter'),
    path('encounter_monster/<str:encounter_monster_id>', encounter.encounter_monster_by_id, name='encounter_monster_by_id'),
    path('combat/<str:combat_id>/monsters', encounter.combat_by_id_monsters, name='combat_by_id_monsters'),
    path('combat/<str:combat_id>', encounter.combat_by_id, name='combat_by_id'),
    path('combat/', encounter.combat, name='combat'),

    path('background/<str:background_id>', views.background_by_id, name='background_by_id'),
    path('background', views.background, name='background'),

    path('race/<str:race_id>', views.race_by_id, name='race_by_id'),
    path('race', views.race, name='race'),

    path('spell/lookup', views.spell_lookup, name='spell_lookup'),
    path('spell/<str:spell_id>', views.spell_by_id, name='spell_by_id'),
    path('spell', views.spell, name='spell'),

    path('spell-list/class/<str:class_id>', spell_list.spell_list_by_class, name='spell_list_by_class'),
    path('spell-list/<str:spell_list_id>/spell/<str:spell_id>', spell_list.spell_list_by_id_spell, name='spell_list_by_id_spell'),
    path('spell-list/<str:spell_list_id>', spell_list.spell_list_by_id, name='spell_by_id_spell'),
    path('spell-list', spell_list.spell_list, name='spell_list'),

    path('sub-class/<str:sub_class_id>', dnd_sub_class.dnd_sub_class_by_id, name='dnd_sub_class_by_id'),
    path('class/<str:class_id>/sub-class', dnd_sub_class.dnd_class_sub_class, name='dnd_class_sub_class'),
    path('class/<str:class_id>', views.dnd_class_by_id, name='dnd_class_by_id'),
    path('class', views.dnd_class, name='dnd_class'),

    path('feat/lookup', feat.feat_lookup, name='feat_lookup'),
    path('feat/<str:feat_id>', feat.feat_by_id, name='feat_by_id'),
    path('feat', feat.feat, name='feat'),

    path('dice-roll/', views.dice_roll, name='dice_roll'),

    path('user-content-collection/<str:collection_id>', views.user_content_collection_by_id, name='user_content_collection_by_id'),
    path('content-collection/<str:resource>/<str:resource_id>', views.content_collection_resource, name='content_collection_resource'),
    path('content-collection/<str:collection_id>', views.content_collection_by_id, name='content_collection_by_id'),
    path('content-collection', views.content_collection, name='content_collection'),

    path('component/<str:component_id>', views.component_by_id, name='component_by_id'),
    path('section/<str:section_id>/component', views.section_component, name='section_component'),
    path('section/<str:section_id>', views.section_by_id, name='section_by_id'),
    path('page/<str:page_id>/section', views.page_section, name='page_section'),
    path('page/<str:page_id>', views.page_by_id, name='page_by_id'),
    path('collection/<str:collection_id>/page', views.collection_page, name='collection_page'),
    path('collection/<str:collection_id>', views.collection_by_id, name='collection_by_id'),
    path('collection', views.collection, name='collection'),

    path('user-images/<str:upload_id>', views.user_images_by_id, name='user_images_by_id'),
    path('user-images', views.user_images, name='user_images'),

    path('images/<str:filename>', views.images, name='images'),
    path('static/<str:filename>', views.static, name='static'),
    path('file-upload', views.FileUploadView.as_view(), name='file_upload'),
    path('import', views.ImportUploadView.as_view(), name='import_upload'),

    path('battlemap/<str:map_id>', views.battlemap_by_id, name='battlemap_by_id'),
    path('battlemap', views.battlemap, name='battlemap'),

    path('map/<str:map_id>', views.map_by_id, name='map_by_id'),
    path('map', views.map, name='map'),
]