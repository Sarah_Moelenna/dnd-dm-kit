from django.apps import AppConfig


class DndControllerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dnd_controller'
