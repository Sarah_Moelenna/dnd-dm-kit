from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from dnd_controller.utils import user_content

from dnd_controller.models.all import (
    Feat,
)
from dnd_controller.middleware.auth import is_authenticated

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def feat(request):

    if request.method == 'GET':

        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        name = request.GET.get("name", None)
        homebrew = request.GET.get("homebrew", None)
        is_selectable = request.GET.get("is_selectable", None)

        feats = user_content.get_feat(request.dnd_user)

        if name:
            feats = feats.filter(name__icontains=name)
        if is_selectable:
            feats = feats.filter(is_selectable=(is_selectable == 'true'))
        if homebrew:
            if homebrew == "true":
                feats = feats.filter(is_homebrew=True)
            elif homebrew == "false":
                feats = feats.filter(is_homebrew=False)

        feats = feats.order_by('name')

        p = Paginator(feats, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [item.to_simple_dict(request.dnd_user) for item in p.page(page)]
        }
        return JsonResponse(data, safe=False)
    
    elif request.method == 'POST':
        feat_data = request.data.get("data")
        Feat.create_feat(feat_data, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def feat_lookup(request):

    if request.method == 'POST':

        feat_ids = request.data.get("feat_ids")

        feats = user_content.get_feat(request.dnd_user).filter(pk__in=feat_ids).all()

        data = [feat.to_dict() for feat in feats]
        return JsonResponse(data, safe=False)

@api_view(["GET", "PUT", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def feat_by_id(request, feat_id):

    if request.method == 'GET':
        feat = user_content.get_feat(request.dnd_user).get(pk=feat_id)

        return JsonResponse(feat.to_dict(request.dnd_user), safe=False)

    elif request.method == 'PUT':
        feat_data = request.data.get("data")
        feat = Feat.objects.get(pk=feat_id, user=request.dnd_user)
        feat.update_feat(feat_data)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        feat = Feat.objects.get(pk=feat_id, user=request.dnd_user)
        feat.delete_feat()
        return Response(status=status.HTTP_200_OK)