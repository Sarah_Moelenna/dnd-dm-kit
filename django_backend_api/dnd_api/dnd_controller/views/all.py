from django.http import HttpResponse, FileResponse, JsonResponse
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from django.core.files.storage import FileSystemStorage
from rest_framework.views import APIView
from rest_framework.exceptions import NotAuthenticated, NotFound, ParseError
from django.conf import settings as django_settings
import urllib
import json
from uuid import uuid4
from django.db.models import Q
from dnd_controller.utils import user_content
from dnd_controller.models import (
    AudioFile,
    Encounter,
    Combat,
)
from dnd_controller.models.all import (
    Campaign,
    Character,
    Monster,
    Setting,
    Collection,
    Page,
    Page_Section,
    Page_Component,
    CampaignCollection,
    Map,
    Ally,
    User,
    Token,
    GameSession, 
    BattleMap,
    Item,
    Upload,
    Race,
    Spell,
    ContentCollection,
    UserContentCollection,
    ContentCollectionRace,
    ContentCollectionMonster,
    ContentCollectionItem,
    ContentCollectionSpell,
    ContentCollectionBattleMap,
    Background,
    ContentCollectionBackground,
    DndClass,
    ContentCollectionDndClass,
    ContentCollectionSpellList,
    SpellList,
    DndSubClass,
    ContentCollectionDndSubClass,
    Feat,
    ContentCollectionFeat
)
from dnd_controller.utils.bot_commands import process_bot_command
from dnd_controller.utils import character_scraper
from dnd_controller.utils import bot_state_reader
from dnd_controller.utils import monster_scraper
from dnd_controller.utils import dice_roller
from dnd_controller.utils import item_scraper
from dnd_controller.utils import race_scraper
from dnd_controller.utils import spell_scraper
from dnd_controller.utils import background_scraper
from dnd_controller.utils import class_scraper
from dnd_controller.middleware.auth import is_authenticated, auth_or_admin, has_session

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def login(request):
    if request.method == 'GET':
        username = request.query_params.get("username")
        password = request.query_params.get("password")
        if username is None or password is None:
            raise ParseError(detail="Username and Password must be provided.")
        try:
            user = User.login(username, password)
        except ValueError:
            raise ParseError(detail="Invalid login.")
        token = Token.create_token(user)
        data = {"token" : token.id}

        return JsonResponse(data, safe=False)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def validate_auth(request):
    if request.method == 'GET':
        auth_token = request.query_params.get("auth_token")
        if auth_token is None:
            raise ParseError(detail="auth_token must be provided.")
        try:
            token = Token.objects.get(pk=auth_token)
        except ValueError:
            raise ParseError(detail="Invalid login.")

        data = {"token" : token.id}

        return JsonResponse(data, safe=False)

@api_view(["PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def account(request):
    if request.method == 'PUT':
        
        old_password = request.data.get("old_password")
        password = request.data.get("password")
        
        result = request.dnd_user.check_password(old_password)
        if result == True:
            request.dnd_user.update_password(password)

        return Response(status=status.HTTP_200_OK)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def spell(request):

    if request.method == 'GET':
        name = request.GET.get("name", None)
        school = request.GET.get("school", None)
        level = request.GET.get("level", None)
        sort = request.GET.get("sort", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)


        spells = user_content.get_spell(request.dnd_user)
        if name:
            spells = spells.filter(name__icontains=name)
        if school:
            spells = spells.filter(school=school)
        if level:
            spells = spells.filter(level=level)

        if sort is not None:
            spells = spells.order_by(sort)
        else:
            spells = spells.order_by("name")

        p = Paginator(spells, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [spell.to_simple_dict(request.dnd_user) for spell in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        spell_data = request.data.get("data")
        modifiers = spell_data.pop('modifiers', [])
        Spell.create(request.dnd_user, spell_data, modifiers)
        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def spell_lookup(request):

    if request.method == 'POST':

        spell_ids = request.data.get("spell_ids")

        spells = user_content.get_spell(request.dnd_user).filter(pk__in=spell_ids).all()

        data = [spell.to_dict() for spell in spells]
        return JsonResponse(data, safe=False)

@api_view(["GET", "PUT", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def spell_by_id(request, spell_id: str):

    if request.method == 'GET':
        spell = user_content.get_spell(request.dnd_user).get(pk=spell_id)
        return JsonResponse(spell.to_dict(request.dnd_user), safe=False)

    elif request.method == 'PUT':
        spell = Spell.objects.get(pk=spell_id, user=request.dnd_user)

        spell_data = request.data.get("data")

        modifiers = spell_data.pop('modifiers', [])
        Spell.update(spell, spell_data, modifiers)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        spell = Spell.objects.get(pk=spell_id, user=request.dnd_user)
        spell.delete_spell()
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def dnd_class(request):

    if request.method == 'GET':

        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 999)

        name = request.GET.get("name", None)
        homebrew = request.GET.get("homebrew", None)

        dnd_classs = user_content.get_dnd_class(request.dnd_user)

        if name:
            dnd_classs = dnd_classs.filter(name__icontains=name)
        if homebrew:
            if homebrew == "true":
                dnd_classs = dnd_classs.filter(is_homebrew=True)
            elif homebrew == "false":
                dnd_classs = dnd_classs.filter(is_homebrew=False)

        dnd_classs = dnd_classs.order_by('name')

        p = Paginator(dnd_classs, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [item.to_simple_dict(request.dnd_user) for item in p.page(page)]
        }
        return JsonResponse(data, safe=False)
    
    elif request.method == 'POST':
        dnd_class_data = request.data.get("data")
        DndClass.create_dnd_class(dnd_class_data, request.dnd_user)
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "PUT", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def dnd_class_by_id(request, class_id):

    if request.method == 'GET':
        dnd_class = user_content.get_dnd_class(request.dnd_user).get(pk=class_id)

        return JsonResponse(dnd_class.to_dict(request.dnd_user), safe=False)

    elif request.method == 'PUT':
        dnd_class_data = request.data.get("data")
        dnd_class = DndClass.objects.get(pk=class_id, user=request.dnd_user)
        dnd_class.update_dnd_class(dnd_class_data)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        dnd_class = DndClass.objects.get(pk=class_id, user=request.dnd_user)
        dnd_class.delete_class()
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def race(request):

    if request.method == 'GET':
        name = request.GET.get("name", None)

        races = user_content.get_race(request.dnd_user)
        if name:
            races = races.filter(full_name__icontains=name)

        races = races.order_by("base_name", "full_name")

        race_data = [f.to_simple_dict(request.dnd_user) for f in races]
        return JsonResponse(race_data, safe=False)

    elif request.method == 'POST':
        race_data = request.data.get("data")
        Race.create_race(race_data, request.dnd_user)
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "PUT", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def race_by_id(request, race_id: str):

    if request.method == 'GET':
        race = user_content.get_race(request.dnd_user).get(pk=race_id)
        return JsonResponse(race.to_dict(request.dnd_user), safe=False)

    elif request.method == 'PUT':
        race_data = request.data.get("data")
        race = Race.objects.get(pk=race_id, user=request.dnd_user)
        race.update_race(race_data)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        race = Race.objects.get(pk=race_id, user=request.dnd_user)
        race.delete_race()
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def campaign(request):

    if request.method == 'GET':
        name = request.GET.get("name", None)
        page = request.GET.get("page", 1)
        simple = request.GET.get("simple", False)
        results_per_page = request.GET.get("results_per_page", 20)

        campaigns = Campaign.objects.all()
        campaigns = campaigns.filter(user=request.dnd_user)
        if name:
            campaigns = campaigns.filter(name__icontains=name)

        p = Paginator(campaigns, results_per_page)

        if not simple:
            data = {
                "current_page": page,
                "total_pages": p.num_pages,
                "total_results": p.count,
                "results": [campaign.to_dict() for campaign in p.page(page)]
            }
        else:
            data = {
                "current_page": page,
                "total_pages": p.num_pages,
                "total_results": p.count,
                "results": [campaign.to_name_dict() for campaign in p.page(page)]
            }
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        name = request.data.get("name")
        Campaign.create_campaign(name, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "GET", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def campaign_by_id(request, campaign_id: str):

    if request.method == 'GET':
        campaign = Campaign.objects.get(pk=campaign_id, user=request.dnd_user)
        return JsonResponse(campaign.to_dict(), safe=False)

    elif request.method == 'DELETE':
        Campaign.delete_by_id(campaign_id, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        
        campaign = Campaign.objects.get(pk=campaign_id,user=request.dnd_user)

        if "active" in request.data:
            campaign.update_active(request.data.get("active"))
        if "server_id" in request.data:
            campaign.server_id = request.data.get("server_id")
        if "voice_channel_id" in request.data:
            campaign.voice_channel_id = request.data.get("voice_channel_id")
        if "text_info_channel_id" in request.data:
            campaign.text_info_channel_id = request.data.get("text_info_channel_id")
        if "text_rolling_channel_id" in request.data:
            campaign.text_rolling_channel_id = request.data.get("text_rolling_channel_id")
        if "notes" in request.data:
            campaign.notes = request.data.get("notes")
        campaign.save()
        
        return Response(status=status.HTTP_200_OK)

@api_view(["POST", "DELETE", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def campaign_by_id_collection(request, campaign_id: str):

    if request.method == 'DELETE':
        campaign = Campaign.objects.get(pk=campaign_id, user=request.dnd_user)

        collection_id = request.data.get("collection_id")
        collection = Collection.objects.get(pk=collection_id, user=request.dnd_user)

        obj = CampaignCollection.objects.filter(collection=collection, campaign=campaign).all()[0]
        obj.delete()

        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        campaign = Campaign.objects.get(pk=campaign_id, user=request.dnd_user)

        order = request.data.get("order", None)
        share = request.data.get("share", None)
        collection_id = request.data.get("collection_id", None)

        if order is not None :
            for key, value in order.items():
                collection = Collection.objects.get(pk=value)
                obj = CampaignCollection.objects.filter(collection=collection, campaign=campaign).all()[0]
                obj.order = key
                obj.save()

        if share is not None:
            collection = Collection.objects.get(pk=collection_id, user=request.dnd_user)
            obj = CampaignCollection.objects.filter(collection=collection, campaign=campaign).all()[0]
            obj.sharing = share
            obj.save()
        
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'POST':
        
        campaign = Campaign.objects.get(pk=campaign_id, user=request.dnd_user)

        collection_id = request.data.get("collection_id")
        collection = Collection.objects.get(pk=collection_id, user=request.dnd_user)

        obj = CampaignCollection(
            campaign=campaign,
            collection=collection
        )
        obj.save()
        
        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def campaign_by_id_session(request, campaign_id: str):

    if request.method == 'POST':
        
        campaign = Campaign.objects.get(pk=campaign_id, user=request.dnd_user)
        GameSession.create(campaign)
        
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def session_by_id(request, session_id: str):

    if request.method == 'DELETE':
        
        GameSession.delete_by_id(session_id, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def session_login(request):

    if request.method == 'GET':
        
        session_key = request.GET.get("session_key", None)
        session_id = request.GET.get("session_id", None)

        if session_key is not None:
            session = GameSession.objects.filter(key=session_key).first()
        if session_id is not None:
            session = GameSession.objects.filter(pk=session_id).first()
        return JsonResponse(session.to_simple_dict(), safe=False)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@has_session
def session_collection(request):

    if request.method == 'GET':
        
        collections = request.dnd_session.collections
        return JsonResponse([f.to_simple_dict() for f in collections], safe=False)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@has_session
def session_page_by_id(request, page_id):

    if request.method == 'GET':
        
        allowed_collections = request.dnd_session.collections
        page = Page.objects.get(pk=page_id)
        if page.collection in allowed_collections:
            return JsonResponse(page.to_dict(), safe=False)
        raise NotFound("Page not found")

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def character(request):

    if request.method == 'POST':
        url = request.data.get("url")
        campaign_id = request.data.get("campaign_id")
        campaign = Campaign.objects.get(pk=campaign_id,user=request.dnd_user)
        Character.create_character(url, campaign)
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def character_by_id(request, character_id: str):

    if request.method == 'DELETE':
        Character.delete_by_id(character_id, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def track_character_by_id(request, character_id: str):

    if request.method == 'PUT':
        tracking = request.data.get("tracking")
        Character.update_tracking_by_id(character_id, tracking, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def ally(request):

    if request.method == 'POST':
        monster_id = request.data.get("monster_id")
        campaign_id = request.data.get("campaign_id")
        campaign = Campaign.objects.get(pk=campaign_id,user=request.dnd_user)
        monster = user_content.get_monster(request.dnd_user).get(pk=monster_id)
        display_name = request.data.get("display_name")
        max_health = request.data.get("max_health")
        Ally.create_ally(campaign, monster, display_name, max_health)
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def ally_by_id(request, ally_id: str):

    if request.method == 'PUT':
        display_name = request.data.get("display_name", None)
        max_health = request.data.get("max_health", None)
        tracking = request.data.get("tracking", None)
        damage_taken = request.data.get("damage_taken", None)
        Ally.update_by_id(ally_id, tracking, display_name, damage_taken, max_health, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        Ally.delete_by_id(ally_id, request.dnd_user)
        return Response(status=status.HTTP_200_OK)


@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def commands(request):

    if request.method == 'POST':
        command_code = request.data.get("command_code")
        data = request.data.get("data")
        campaign = Campaign.get_active(request.dnd_user)
        if campaign is not None:
            process_bot_command(command_code, data, request.dnd_user, str(campaign.id))
        else:
            process_bot_command(command_code, data, request.dnd_user, '')
        return Response(status=status.HTTP_200_OK)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def character_tracker(request):

    if request.method == 'GET':
        characters = Campaign.get_characters_to_track(request.dnd_user)
        allies = Campaign.get_allies_to_track(request.dnd_user)

        if len(characters) > 0:
            urls = [character.url for character in characters]

            data = character_scraper.scrape_character_data(urls)
            data.extend(allies)

            return JsonResponse(data, safe=False)
        return JsonResponse([], safe=False)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def bot_state(request):
    if request.method == 'GET':
        campaign = Campaign.get_active(request.dnd_user)
        if campaign is not None:
            return JsonResponse(bot_state_reader.get_bot_state(str(campaign.id)), safe=False)
        else:
            return JsonResponse(bot_state_reader.get_bot_state(None), safe=False)


@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def user(request):

    if request.method == 'GET':
        campaign = Campaign.get_active(request.dnd_user)
        dm_name = Setting.get_setting("DM_NAME", request.dnd_user)['value']

        data = {
            "user": request.dnd_user.to_dict(),
            "active_campaign": campaign.to_dict() if campaign else None,
            "campaigns": [campaign.to_dict() for campaign in Campaign.objects.filter(user=request.dnd_user).all()],
            "dm_name": dm_name,
        }

        return JsonResponse(data, safe=False)


@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@auth_or_admin
def settings(request):

    if request.method == 'GET':
        campaigns = Campaign.get_all_active()

        data = {
            "active_campaigns": [campaign.to_dict() for campaign in campaigns]
        }

        return JsonResponse(data, safe=False)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def background_scrape(request):

    if request.method == 'POST':
        data = request.data.get("background")
        background_scraper.scrape_background(data)

        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def class_scrape(request):

    if request.method == 'POST':
        data = request.data.get("class")
        class_scraper.scrape_class(data)

        return Response(status=status.HTTP_200_OK)
        
@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def item_scrape(request):

    if request.method == 'POST':
        data = request.data.get("items")
        item_scraper.scrape_items(data)

        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@auth_or_admin
def race_scrape(request):

    if request.method == 'POST':
        race = request.data.get("race")
        racial_traits = request.data.get("racial_traits")
        try:
            race = json.loads(race)
            racial_traits = json.loads(racial_traits)
            race_scraper.scrape_race(race, racial_traits, request.dnd_user)
        except Exception as e:
            raise e
            pass
        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@auth_or_admin
def spell_scrape(request):

    if request.method == 'POST':
        spells = request.data.get("spells")
        try:
            spells = json.loads(spells)
            spell_scraper.scrape_spells(spells)
        except Exception as e:
            raise e
            pass
        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def monster_scrape(request):

    if request.method == 'POST':
        html = request.data.get("html")
        monster_scraper.scrape_monster(html)
        return Response(status=status.HTTP_200_OK)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def monsters(request):

    if request.method == 'GET':

        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        name = request.GET.get("name", None)
        challenge_min = monster_scraper.string_challenge_number_to_number(request.GET.get("challenge_min", None))
        challenge_max = monster_scraper.string_challenge_number_to_number(request.GET.get("challenge_max", None))
        size = request.GET.get("size", None)
        monster_type = request.GET.get("type", None)
        environment = request.GET.get("environment", None)
        homebrew = request.GET.get("homebrew", None)

        sort = request.GET.get("sort", None)

        monsters = user_content.get_monster(user=request.dnd_user)

        if name:
            monsters = monsters.filter(name__icontains=name)
        if challenge_min:
            monsters = monsters.filter(challenge_number__gte=challenge_min)
        if challenge_max:
            monsters = monsters.filter(challenge_number__lte=challenge_max)
        if size:
            monsters = monsters.filter(types__icontains=size)
        if monster_type:
            monsters = monsters.filter(types__icontains=monster_type)
        if environment:
            monsters = monsters.filter(environments__icontains=environment)
        if homebrew:
            if homebrew == "true":
                monsters = monsters.filter(user_made_content=True)
            elif homebrew == "false":
                monsters = monsters.filter(user_made_content=False)

        if sort is not None:
            monsters = monsters.order_by(sort)

        p = Paginator(monsters, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [monster.to_simple_dict(request.dnd_user) for monster in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    if request.method == 'POST':
        data = request.data.get("data")
        Monster.create_monster(data, request.dnd_user)
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "PUT", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def monster_by_id(request, monster_id):

    if request.method == 'GET':
        monster = user_content.get_monster(request.dnd_user).get(pk=monster_id)

        return JsonResponse(monster.to_dict(request.dnd_user), safe=False)

    elif request.method == 'PUT':
        monster = Monster.objects.get(pk=monster_id, user=request.dnd_user, user_made_content=True)
        data = request.data.get("data")
        monster.edit_monster(data)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        monster = Monster.objects.get(pk=monster_id, user=request.dnd_user, user_made_content=True)
        monster.delete()
        return Response(status=status.HTTP_200_OK)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def background(request):

    if request.method == 'GET':

        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 10)

        name = request.GET.get("name", None)
        homebrew = request.GET.get("homebrew", None)

        backgrounds = user_content.get_background(request.dnd_user)

        if name:
            backgrounds = backgrounds.filter(name__icontains=name)
        if homebrew:
            if homebrew == "true":
                backgrounds = backgrounds.filter(is_homebrew=True)
            elif homebrew == "false":
                backgrounds = backgrounds.filter(is_homebrew=False)

        backgrounds = backgrounds.order_by('name')

        p = Paginator(backgrounds, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [item.to_simple_dict(request.dnd_user) for item in p.page(page)]
        }
        return JsonResponse(data, safe=False)
    
    elif request.method == 'POST':
        background_data = request.data.get("data")
        Background.create_background(background_data, request.dnd_user)
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "PUT", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def background_by_id(request, background_id):

    if request.method == 'GET':
        background = user_content.get_background(request.dnd_user).get(pk=background_id)

        return JsonResponse(background.to_dict(request.dnd_user), safe=False)

    elif request.method == 'PUT':
        background_data = request.data.get("data")
        background = Background.objects.get(pk=background_id, user=request.dnd_user)
        background.update_background(background_data)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        background = Background.objects.get(pk=background_id, user=request.dnd_user)
        background.delete_background()
        return Response(status=status.HTTP_200_OK)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def item(request):

    if request.method == 'GET':

        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        name = request.GET.get("name", None)
        type = request.GET.get("type", None)
        sub_type = request.GET.get("sub_type", None)
        rarity = request.GET.get("rarity", None)
        tags = request.GET.get("tags", None)
        homebrew = request.GET.get("homebrew", None)
        magic = request.GET.get("magic", None)

        items = user_content.get_item(request.dnd_user)

        if name:
            items = items.filter(name__icontains=name)
        if type:
            items = items.filter(type=type)
        if sub_type:
            items = items.filter(sub_type=sub_type)
        if rarity:
            items = items.filter(rarity=rarity)
        if tags:
            items = items.filter(tags__icontains=tags)
        if homebrew:
            if homebrew == "true":
                items = items.filter(user_made_content=True)
            elif homebrew == "false":
                items = items.filter(user_made_content=False)
        if magic:
            if magic == "true":
                items = items.filter(magic=True)
            elif magic == "false":
                items = items.filter(magic=False)

        items = items.order_by('name')

        p = Paginator(items, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [item.to_simple_dict(request.dnd_user) for item in p.page(page)]
        }
        return JsonResponse(data, safe=False)
    
    elif request.method == 'POST':
        item_data = request.data.get("data")
        Item.create_item(item_data, request.dnd_user)
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "PUT", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def item_by_id(request, item_id):

    if request.method == 'GET':
        item = user_content.get_item(request.dnd_user).get(pk=item_id)

        return JsonResponse(item.to_dict(request.dnd_user), safe=False)

    elif request.method == 'PUT':
        item_data = request.data.get("data")
        item = Item.objects.get(pk=item_id, user=request.dnd_user)
        item.update_item(item_data)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        item = Item.objects.get(pk=item_id, user=request.dnd_user)
        item.delete_item()
        return Response(status=status.HTTP_200_OK)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def item_options(request):
    if request.method == 'GET':
        weapons = user_content.get_item(request.dnd_user).filter(magic=False, base_type='Weapon').order_by('name').all()
        armours = user_content.get_item(request.dnd_user).filter(magic=False, base_type='Armour').order_by('name').all()

        data = {
            "weapons": [{"id":str(weapon.id), "name": weapon.name} for weapon in weapons],
            "armours": [{"id":str(armour.id), "name": armour.name} for armour in armours]
        }

        return JsonResponse(data, safe=False)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def item_filters(request):
    if request.method == 'GET':
        types = user_content.get_item(request.dnd_user).exclude(type__isnull=True).exclude(type="").order_by().values_list('type', flat=True).distinct().all()
        sub_types = user_content.get_item(request.dnd_user).exclude(sub_type__isnull=True).order_by().values_list('sub_type', flat=True).distinct().all()
        raritys = user_content.get_item(request.dnd_user).exclude(rarity__isnull=True).order_by().values_list('rarity', flat=True).distinct().all()

        item_type = [item for item in types]
        item_sub_type = [item for item in sub_types]
        item_rarity = [item for item in raritys]

        item_type.sort()
        item_sub_type.sort()
        item_rarity.sort()

        data = {
            "type": item_type,
            "sub_type": item_sub_type,
            "rarity": item_rarity
        }

        return JsonResponse(data, safe=False)


@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def dice_roll(request):

    if request.method == 'GET':
        roll = urllib.parse.unquote(request.GET.get("roll"))
        roll_type = request.GET.get("roll_type")
        roll_modifier = request.GET.get("roll_modifier")
        identity = request.GET.get("roll_identity")

        if roll_type == "DUNGEON_MASTER":
            identity = Setting.get_setting("DM_NAME", request.dnd_user)['value']

        roll_result = {
            "selected_roll": None,
            "unselected_roll": None,
            "roll": roll,
            "roll_modifier": roll_modifier,
            "identity": identity
        }

        if roll_modifier == "DEFAULT":
            roll_result["selected_roll"] = dice_roller.get_roll_result(roll)
        elif roll_modifier == "ADVANTAGE":
            result_one = dice_roller.get_roll_result(roll)
            result_two = dice_roller.get_roll_result(roll)
            if result_one['total_result'] > result_two['total_result']:
                roll_result["selected_roll"] = result_one
                roll_result["unselected_roll"] = result_two
            else:
                roll_result["selected_roll"] = result_two
                roll_result["unselected_roll"] = result_one
        elif roll_modifier == "DISADVANTAGE":
            result_one = dice_roller.get_roll_result(roll)
            result_two = dice_roller.get_roll_result(roll)
            if result_one['total_result'] < result_two['total_result']:
                roll_result["selected_roll"] = result_one
                roll_result["unselected_roll"] = result_two
            else:
                roll_result["selected_roll"] = result_two
                roll_result["unselected_roll"] = result_one

        if roll_type == "MONSTER_ROLL" and Setting.get_setting("POST_MONSTER_ROLL", request.dnd_user)['value'] == True:
            dice_roller.post_roll_result(roll_result, request.dnd_user)
        if roll_type == "ALLY_ROLL" and Setting.get_setting("POST_ALLY_ROLL", request.dnd_user)['value'] == True:
            dice_roller.post_roll_result(roll_result, request.dnd_user)
        if roll_type == "DUNGEON_MASTER":
            dice_roller.post_roll_result(roll_result, request.dnd_user)

        return JsonResponse(roll_result, safe=False)


@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def user_settings(request):

    if request.method == 'GET':
        settings = {}

        for key in Setting.SETTINGS.keys():
            setting_object = (Setting.get_setting(key, request.dnd_user))

            if request.dnd_user.user_type != User.TYPE_ADMIN and setting_object['grouping'] == Setting.GROUPING_ADMIN:
                continue

            if setting_object["grouping"] not in settings:
                settings[setting_object["grouping"]] = []
            settings[setting_object["grouping"]].append(setting_object)

        return JsonResponse(settings, safe=False)

    elif request.method == 'POST':
        setting_name = request.data.get("setting_name")
        setting_value = request.data.get("setting_value")

        Setting.set_setting(setting_name, setting_value, request.dnd_user)

        return Response(status=status.HTTP_200_OK)


@api_view(["POST", "GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def content_collection(request):

    if request.method == 'POST':

        name = request.data.get("name")
        is_shareable = request.data.get("is_shareable")
        is_default = request.data.get("is_default") if request.dnd_user.user_type is User.TYPE_ADMIN else False
        collection = ContentCollection(
            name=name,
            is_shareable=is_shareable,
            is_default=is_default,
            user=request.dnd_user
        )
        collection.save()
        return Response(status=status.HTTP_200_OK)
    
    elif request.method == 'GET':

        owned = request.GET.get("owned", False)
        name = request.GET.get("name", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        if owned:
            collections = user_content.get_content_collection(user=request.dnd_user)
        else:
            collections = ContentCollection.objects.all().filter(is_shareable=True).filter(~Q(user=request.dnd_user))

        if name:
            collections = collections.filter(name__icontains=name)
        

        p = Paginator(collections, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [f.to_dict(user = request.dnd_user) for f in p.page(page)]
        }

        return JsonResponse(data, safe=False)


@api_view(["DELETE", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def content_collection_by_id(request, collection_id: str):

    if request.method == 'DELETE':
        collection = ContentCollection.objects.get(pk=collection_id,user=request.dnd_user,is_shareable=False)
        collection.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':

        name = request.data.get("name", None)
        description = request.data.get("description", None)
        is_shareable = request.data.get("is_shareable", None)
        is_default = request.data.get("is_default", None) if request.dnd_user.user_type == User.TYPE_ADMIN else None

        collection = ContentCollection.objects.get(pk=collection_id,user=request.dnd_user)
        
        if name is not None and is_shareable is not None and is_default is not None:
            collection.filter(is_shareable=False)

        if name is not None:
            collection.name = name
        if description is not None:
            collection.description = description
        if is_shareable is not None:
            collection.is_shareable = is_shareable
        if is_default is not None:
            collection.is_default = is_default
        collection.save()

        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def content_collection_resource(request, resource: str, resource_id: str):

    resources = {
        "race": {"model": Race, "pivot": ContentCollectionRace},
        "monster": {"model": Monster, "pivot": ContentCollectionMonster},
        "spell": {"model": Spell, "pivot": ContentCollectionSpell},
        "item": {"model": Item, "pivot": ContentCollectionItem},
        "battlemap": {"model": BattleMap, "pivot": ContentCollectionBattleMap},
        "background": {"model": Background, "pivot": ContentCollectionBackground},
        "class": {"model": DndClass, "pivot": ContentCollectionDndClass},
        "spelllist": {"model": SpellList, "pivot": ContentCollectionSpellList},
        "subclass": {"model": DndSubClass, "pivot": ContentCollectionDndSubClass},
        "feat": {"model": Feat, "pivot": ContentCollectionFeat},
    }

    if request.method == 'GET':

        resource_obj = resources[resource]['model'].objects.get(pk=resource_id,user=request.dnd_user)
        resource_content_collections = resources[resource]['pivot'].list(resource_obj, request.dnd_user)
        collections = [obj.content_collection.to_dict() for obj in resource_content_collections]
        collection_ids = [obj.content_collection.id for obj in resource_content_collections]

        data = {
            "existing": collections,
            "avaliable": [obj.to_dict() for obj in ContentCollection.objects.filter(user=request.dnd_user).exclude(id__in=collection_ids)]
        }
        
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':

        resource_obj = resources[resource]['model'].objects.get(pk=resource_id,user=request.dnd_user)
        content_collection_id = request.data.get("content_collection_id")
        content_collection = ContentCollection.objects.get(pk=content_collection_id,user=request.dnd_user)
        if not resources[resource]['pivot'].exists(resource_obj, content_collection):
            resources[resource]['pivot'].create(resource_obj, content_collection)
        
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        resource_obj = resources[resource]['model'].objects.get(pk=resource_id,user=request.dnd_user)
        content_collection_id = request.data.get("content_collection_id")
        content_collection = ContentCollection.objects.get(pk=content_collection_id,user=request.dnd_user)
        if resources[resource]['pivot'].exists(resource_obj, content_collection):
            resources[resource]['pivot'].delete_resource(resource_obj, content_collection)
        return Response(status=status.HTTP_200_OK)


@api_view(["DELETE", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def user_content_collection_by_id(request, collection_id: str):

    if request.method == 'DELETE':
        collection = ContentCollection.objects.get(pk=collection_id,is_shareable=True)
        user_collection = UserContentCollection.objects.filter(user=request.dnd_user, content_collection=collection).all()[0]
        user_collection.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'POST':
        collection = ContentCollection.objects.get(pk=collection_id,is_shareable=True)
        share_status = UserContentCollection.objects.filter(user=request.dnd_user, content_collection=collection).exists()
        if share_status == True:
            raise ValueError("Content Colelction already added to user")
        user_collection = UserContentCollection(
            user=request.dnd_user,
            content_collection=collection
        )
        user_collection.save()
        return Response(status=status.HTTP_200_OK)



@api_view(["POST", "GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def collection(request):

    if request.method == 'POST':

        name = request.data.get("name")
        collection = Collection(name=name, user=request.dnd_user)
        collection.save()
        return Response(status=status.HTTP_200_OK)
    
    elif request.method == 'GET':

        name = request.GET.get("name", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        collections = Collection.objects.all().order_by("name")
        collections = collections.filter(user=request.dnd_user)
        if name:
            collections = collections.filter(name__icontains=name)
        

        p = Paginator(collections, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [f.to_simple_dict() for f in p.page(page)]
        }

        return JsonResponse(data, safe=False)
            

@api_view(["DELETE", "GET", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def collection_by_id(request, collection_id: str):

    if request.method == 'GET':
        collection = Collection.objects.get(pk=collection_id,user=request.dnd_user)
        return JsonResponse(collection.to_simple_dict(), safe=False)

    elif request.method == 'DELETE':
        collection = Collection.objects.get(pk=collection_id,user=request.dnd_user)
        collection.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':

        name = request.data.get("name")
        heirachy = request.data.get("heirachy")

        collection = Collection.objects.get(pk=collection_id,user=request.dnd_user)
        if name:
            collection.name = name
        if heirachy:
            collection.heirachy = json.dumps(heirachy)
        collection.save()

        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def collection_page(request, collection_id: str):

    if request.method == 'POST':

        collection = Collection.objects.get(pk=collection_id,user=request.dnd_user)
        name = request.data.get("name")
        page = Page(name=name, collection=collection)
        page.save()
        
        return Response(status=status.HTTP_200_OK)


@api_view(["DELETE", "GET", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def page_by_id(request, page_id: str):

    if request.method == 'DELETE':
        page = Page.objects.get(pk=page_id)
        if page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")

        collection = page.collection
        page.delete()
        collection.tidy_up_heirachy(page_id)
        collection.save()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        page = Page.objects.get(pk=page_id)
        if page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        name = request.data.get("name")
        if name:
            page.name = name
        page.save()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'GET':
        page = Page.objects.get(pk=page_id)
        if page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        return JsonResponse(page.to_dict(), safe=False)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def page_section(request, page_id: str):

    if request.method == 'POST':
        page = Page.objects.get(pk=page_id)
        if page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        section_type = request.data.get("type")
        order = request.data.get("order")

        page.add_page_section(section_type, order)
        return Response(status=status.HTTP_200_OK)


@api_view(["DELETE", "GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def section_by_id(request, section_id: str):

    if request.method == 'DELETE':
        page_section = Page_Section.objects.get(pk=section_id)
        if page_section.page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        page_section.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'GET':
        page_section = Page_Section.objects.get(pk=section_id)
        if page_section.page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        return JsonResponse(page_section.to_dict(), safe=False)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def section_component(request, section_id: str):

    if request.method == 'POST':
        page_section = Page_Section.objects.get(pk=section_id)
        if page_section.page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        column = request.data.get("column")
        order = request.data.get("order")
        contents = request.data.get("contents")

        page_section.add_component(column, order, contents)
        return Response(status=status.HTTP_200_OK)


@api_view(["DELETE", "GET", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def component_by_id(request, component_id: str):

    if request.method == 'DELETE':
        page_component = Page_Component.objects.get(pk=component_id)
        if page_component.page_section.page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        page_component.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        
        page_component = Page_Component.objects.get(pk=component_id)
        if page_component.page_section.page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        contents = request.data.get("contents")
        page_component.contents = json.dumps(contents)
        page_component.save()

        return Response(status=status.HTTP_200_OK)

    elif request.method == 'GET':
        page_component = Page_Component.objects.get(pk=component_id)
        if page_component.page_section.page.collection.user != request.dnd_user:
            raise AttributeError("Invalid User")
        
        return JsonResponse(page_component.to_dict(), safe=False)


@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def user_images(request):

    if request.method == 'GET':

        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        uploads = Upload.objects.filter(upload_type=Upload.TYPE_IMAGE, user=request.dnd_user).order_by("-created_at").all()

        p = Paginator(uploads, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [f.to_dict() for f in p.page(page)]
        }

        return JsonResponse(data, safe=False)
            

@api_view(["DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def user_images_by_id(request, upload_id: str):

    if request.method == 'DELETE':
        upload = Upload.objects.get(pk=upload_id,user=request.dnd_user,upload_type=Upload.TYPE_IMAGE)
        upload.delete_file()
        upload.delete()
        return Response(status=status.HTTP_200_OK)

class FileUploadView(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        if request.dnd_user is None:
            raise NotAuthenticated(detail="Token is invalid.")

        if request.dnd_user.can_upload == False:
            raise ValueError("User has reached max upload limit")

        file_obj = request.FILES['file']
        file_size = request.FILES['file'].size

        file_name = f"images/{str(uuid4())}_{file_obj.name}"
        fs = FileSystemStorage() #defaults to   MEDIA_ROOT  
        file_name = fs.save(file_name, file_obj)

        upload_obj = Upload(user=request.dnd_user, file_location=file_name, file_size=file_size, upload_type=Upload.TYPE_IMAGE)
        upload_obj.save()

        response = {
            "file": file_name
        }
        return JsonResponse(response, safe=False)

class ImportUploadView(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):

        if request.dnd_user is None:
                raise NotAuthenticated(detail="Token is invalid.")

        file_obj = request.FILES['file']
        file_name = file_obj.name
        contents = file_obj.read()

        if file_name == "audio_files.json":
            AudioFile.import_json(json.loads(contents), request.dnd_user)
        elif file_name == "campaigns.json":
            Campaign.import_json(json.loads(contents), request.dnd_user)
        elif file_name == "collections.json":
            Collection.import_json(json.loads(contents), request.dnd_user)
        elif file_name == "combats.json":
            Combat.import_json(json.loads(contents), request.dnd_user)
        elif file_name == "encounters.json":
            Encounter.import_json(json.loads(contents), request.dnd_user)
        elif file_name == "monsters.json":
            Monster.import_json(json.loads(contents), request.dnd_user)
        elif file_name == "settings.json":
            Setting.import_json(json.loads(contents), request.dnd_user)
        elif file_name == "maps.json":
            Map.import_json(json.loads(contents), request.dnd_user)
        elif file_name == "battlemaps.json":
            BattleMap.import_json(json.loads(contents), request.dnd_user)

        return Response(status=status.HTTP_200_OK)


@api_view(["GET"])
@csrf_exempt
def images(request, filename: str):

    if request.method == 'GET':
        img = open(f'{django_settings.MEDIA_ROOT}/images/{filename}', 'rb')

        return FileResponse(img)

@api_view(["GET"])
@csrf_exempt
def static(request, filename: str):

    if request.method == 'GET':
        file = open(f'{django_settings.STATIC_ROOT}/{filename}', 'rb')

        return FileResponse(file)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def map(request):


    if request.method == 'GET':
        name = request.GET.get("name", None)
        results_per_page = request.GET.get("results_per_page", 20)
        page = request.GET.get("page", 1)

        maps = Map.objects.all()
        maps = maps.filter(user=request.dnd_user)
        if name:
            maps = maps.filter(name__icontains=name)
        maps = maps.order_by("-created_at")

        p = Paginator(maps, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [f.to_dict() for f in p.page(page)]
        }

        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        name = request.data.get("name")
        map_item = Map(name=name,user=request.dnd_user)
        map_item.save()
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "GET", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def map_by_id(request, map_id: str):

    if request.method == 'GET':
        map_item = Map.objects.get(pk=map_id,user=request.dnd_user)
        return JsonResponse(map_item.to_dict(), safe=False)

    elif request.method == 'DELETE':
        map_item = Map.objects.get(pk=map_id,user=request.dnd_user)
        map_item.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        
        map_item = Map.objects.get(pk=map_id,user=request.dnd_user)

        url = request.data.get("url", None)
        data = request.data.get("data", None)
        if url:
            map_item.url = url
        if data is not None:
            map_item.data = json.dumps(data)
        map_item.save()
        
        return Response(status=status.HTTP_200_OK)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def battlemap(request):


    if request.method == 'GET':
        name = request.GET.get("name", None)
        results_per_page = request.GET.get("results_per_page", 20)
        page = request.GET.get("page", 1)

        maps = user_content.get_battlemap(request.dnd_user)
        if name:
            maps = maps.filter(name__icontains=name)
        maps = maps.order_by("-created_at")

        p = Paginator(maps, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [f.to_dict(request.dnd_user) for f in p.page(page)]
        }

        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        name = request.data.get("name")
        map_item = BattleMap(name=name,user=request.dnd_user)
        map_item.save()
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "GET", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def battlemap_by_id(request, map_id: str):

    if request.method == 'GET':
        map_item = user_content.get_battlemap(request.dnd_user).get(pk=map_id)
        return JsonResponse(map_item.to_dict(request.dnd_user), safe=False)

    elif request.method == 'DELETE':
        map_item = BattleMap.objects.get(pk=map_id,user=request.dnd_user)
        map_item.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        
        map_item = BattleMap.objects.get(pk=map_id,user=request.dnd_user)

        image = request.data.get("image", None)
        rows = request.data.get("rows", None)
        columns = request.data.get("columns", None)
        blocked_cells = request.data.get("blocked_cells", None)
        if image:
            map_item.image = image
        if rows is not None:
            map_item.rows = rows
        if columns is not None:
            map_item.columns = columns
        if blocked_cells is not None:
            map_item.blocked_cells = blocked_cells
        map_item.save()
        
        return Response(status=status.HTTP_200_OK)