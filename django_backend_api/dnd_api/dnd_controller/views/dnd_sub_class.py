from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from dnd_controller.utils import user_content

from dnd_controller.models.all import (
    DndSubClass,
)
from dnd_controller.middleware.auth import is_authenticated

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def dnd_class_sub_class(request, class_id):

    if request.method == 'POST':
        dnd_class_data = request.data.get("data")
        dnd_class = user_content.get_dnd_class(request.dnd_user).get(pk=class_id)
        DndSubClass.create_dnd_sub_class(dnd_class, dnd_class_data, request.dnd_user)
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "PUT", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def dnd_sub_class_by_id(request, sub_class_id):

    if request.method == 'GET':
        dnd_class = user_content.get_dnd_sub_class(request.dnd_user).get(pk=sub_class_id)

        return JsonResponse(dnd_class.to_dict(request.dnd_user), safe=False)

    elif request.method == 'PUT':
        dnd_class_data = request.data.get("data")
        dnd_class = DndSubClass.objects.get(pk=sub_class_id, user=request.dnd_user)
        dnd_class.update_dnd_sub_class(dnd_class_data)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        dnd_class = DndSubClass.objects.get(pk=sub_class_id, user=request.dnd_user)
        dnd_class.delete_sub_class()
        return Response(status=status.HTTP_200_OK)