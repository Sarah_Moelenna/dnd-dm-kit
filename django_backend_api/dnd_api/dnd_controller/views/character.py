from multiprocessing.sharedctypes import Value
from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator

from dnd_controller.models.all import (
    DndCharacter,
    DnDCharacterItem
)
from dnd_controller.middleware.auth import is_authenticated

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def character(request):
    
    if request.method == 'GET':
        name = request.GET.get("name", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)


        characters = DndCharacter.objects.filter(user=request.dnd_user)
        if name:
            characters = characters.filter(name__icontains=name)

        characters = characters.order_by("name")

        p = Paginator(characters, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [character.to_simple_dict() for character in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        character_data = request.data.get("data")

        character = DndCharacter.create_character(character_data, request.dnd_user)
        return JsonResponse(character.to_dict(), safe=False)

@api_view(["GET", "PUT", "DELETE", "PATCH"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def character_by_id(request, character_id):

    if request.method == 'GET':
        character = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)

        return JsonResponse(character.to_dict(), safe=False)

    elif request.method == 'PUT':
        character_data = request.data.get("data")

        character = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)
        character.update_character(character_data, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        character = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)
        character.delete()
        return Response(status=status.HTTP_200_OK)
    
    elif request.method == 'PATCH':

        character: DndCharacter = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)
        patch_data = request.data.get("data")
        character.patch_character(patch_data)

        return Response(status=status.HTTP_200_OK)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def character_data_by_id(request, character_id):

    if request.method == 'GET':

        character: DndCharacter = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)
        data = character.to_data_dict()

        return JsonResponse(data, safe=False)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def character_items(request, character_id):

    if request.method == 'GET':

        character: DndCharacter = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)
        data = character.to_inventory_dict()

        return JsonResponse(data, safe=False)

    elif request.method == 'POST':

        character: DndCharacter = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)
        item_data = request.data.get("data")
        character.add_items(item_data, request.dnd_user)
        data = character.to_inventory_dict()

        return JsonResponse(data, safe=False)

@api_view(["PATCH", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def character_items_by_id(request, character_id, item_id):

    if request.method == 'PATCH':

        character: DndCharacter = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)
        item: DnDCharacterItem = character.dndcharacteritem_set.get(pk=item_id)
        item_data = request.data.get("data")
        item.update_character_item(item_data)
        data = character.to_inventory_dict()

        return JsonResponse(data, safe=False)

    elif request.method == 'DELETE':

        character: DndCharacter = DndCharacter.objects.filter(user=request.dnd_user).get(pk=character_id)
        item: DnDCharacterItem = character.dndcharacteritem_set.get(pk=item_id)
        item.delete()
        data = character.to_inventory_dict()

        return JsonResponse(data, safe=False)