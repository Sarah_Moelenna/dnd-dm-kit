from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
import json
from dnd_controller.utils import user_content
from dnd_controller.models import (
    AudioFile,
    Encounter,
    EncounterMonster,
    Combat,
    Playlist,
)
from dnd_controller.models.all import (
    Campaign,
)
from dnd_controller.middleware.auth import is_authenticated

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def encounters(request):

    if request.method == 'GET':
        name = request.GET.get("name", None)
        campaign_id = request.GET.get("campaign_id", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        encounters = Encounter.objects.all()
        if name:
            encounters = encounters.filter(name__icontains=name)
        if campaign_id:
            campaign = Campaign.objects.get(pk=campaign_id,user=request.dnd_user)
            encounters = encounters.filter(campaign=campaign)
        encounters = encounters.filter(user=request.dnd_user)

        encounters = encounters.order_by("-created_at")

        p = Paginator(encounters, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [encounter.to_dict() for encounter in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        name = request.data.get("name")
        Encounter.create_encounter(name, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "GET", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def encounter_by_id(request, encounter_id: str):

    if request.method == 'GET':
        encounter = Encounter.objects.get(pk=encounter_id,user=request.dnd_user)
        return JsonResponse(encounter.to_dict(), safe=False)

    elif request.method == 'DELETE':
        Encounter.delete_by_id(encounter_id, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        audio_id = request.data.get("audio_id", None)
        playlist_id = request.data.get("playlist_id", None)
        battlemap_id = request.data.get("battlemap_id", None)
        campaign_id = request.data.get("campaign_id", None)
        custom = request.data.get("custom", None)
        encounter = Encounter.objects.get(pk=encounter_id, user=request.dnd_user)

        if audio_id:
            audio = AudioFile.objects.get(pk=audio_id,user=request.dnd_user)
            encounter.song = audio
            encounter.playlist = None
        else:
            encounter.song = None

        if playlist_id:
            playlist = Playlist.objects.get(pk=playlist_id,user=request.dnd_user)
            encounter.playlist = playlist
            encounter.song = None
        else:
            encounter.playlist = None

        if battlemap_id:
            battlemap = user_content.get_battlemap(request.dnd_user).get(pk=battlemap_id)
            encounter.battlemap = battlemap
        else:
            encounter.battlemap = None

        if campaign_id:
            campaign = Campaign.objects.get(pk=campaign_id,user=request.dnd_user)
            encounter.campaign = campaign
        else:
            encounter.campaign = None

        if custom is not None:
            encounter.custom = json.dumps(custom)
        encounter.save()
        return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def encounter_encounter_monster(request, encounter_id: str):

    if request.method == 'POST':

        monster_id = request.data.get("monster_id")
        count = request.data.get("count")
        grouping_type = request.data.get("grouping_type")
        display_name = request.data.get("display_name", None)
        delayed_count = request.data.get("delayed_count", 0)

        encounter = Encounter.objects.get(pk=encounter_id, user=request.dnd_user)
        monster = user_content.get_monster(request.dnd_user).get(pk=monster_id)

        EncounterMonster.create_or_edit(encounter, monster, grouping_type, count, display_name, delayed_count)
        
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def encounter_monster_by_id(request, encounter_monster_id: str):

    if request.method == 'DELETE':
        EncounterMonster.delete_by_id(encounter_monster_id, request.dnd_user)
        return Response(status=status.HTTP_200_OK)


@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def combat(request):

    if request.method == 'GET':
        combats = Combat.objects.all()
        combats = combats.filter(user=request.dnd_user).order_by("-created_at")

        combat_data = [f.to_dict() for f in combats]
        return JsonResponse(combat_data, safe=False)

    elif request.method == 'POST':
        encounter_id = request.data.get("encounter_id")
        encounter = Encounter.objects.get(pk=encounter_id, user=request.dnd_user)
        campaign = Campaign.get_active(request.dnd_user)

        combat = Combat.create_from_encounter_for_campaign(encounter, campaign, request.dnd_user)
        return JsonResponse(combat.to_dict(), safe=False)

@api_view(["DELETE", "GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def combat_by_id(request, combat_id: str):

    if request.method == 'GET':
        combat = Combat.objects.get(pk=combat_id,user=request.dnd_user)
        return JsonResponse(combat.to_dict(), safe=False)

    if request.method == 'POST':
        
        new_state = request.data.get("state")
        combat = Combat.update_state_by_id(combat_id, new_state, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        Combat.delete_by_id(combat_id, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def combat_by_id_monsters(request, combat_id: str):

    if request.method == 'GET':

        combat = Combat.objects.get(pk=combat_id, user=request.dnd_user)
        
        return JsonResponse(combat.monsters_dict(), safe=False)
