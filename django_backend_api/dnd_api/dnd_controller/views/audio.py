from django.http import JsonResponse, FileResponse
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from dnd_controller.models import AudioFile, Playlist
from dnd_controller.models.all import Upload
from dnd_controller.middleware.auth import is_authenticated
from rest_framework.exceptions import NotAuthenticated, ValidationError
from django.conf import settings as django_settings
from dnd_controller.utils.redis import get_data
from django.core.files.storage import FileSystemStorage
from uuid import uuid4
from rest_framework.views import APIView

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def audio(request):

    if request.method == 'GET':

        name = request.GET.get("name", None)
        audio_type = request.GET.get("audio_type", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        audio_files = AudioFile.objects.all()
        if name:
            audio_files = audio_files.filter(display_name__icontains=name)
        if audio_type:
            audio_files = audio_files.filter(audio_type=audio_type)
        audio_files = audio_files.filter(user=request.dnd_user)

        audio_files = audio_files.order_by("display_name")

        p = Paginator(audio_files, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [audio_file.to_dict() for audio_file in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        if request.dnd_user.can_upload == False:
            raise ValueError("User has reached max upload limit")

        url = request.data.get("url")
        display_name = request.data.get("display_name")
        audio_type = request.data.get("audio_type")
        AudioFile.create_audio_file(url, display_name, request.dnd_user, audio_type)
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "PUT"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def audio_by_id(request, audio_id: str):

    if request.method == 'DELETE':
        AudioFile.delete_by_id(audio_id, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        display_name = request.data.get("display_name")
        audio_type = request.data.get("audio_type")
        audio_file: AudioFile = AudioFile.objects.get(pk=audio_id, user=request.dnd_user)
        audio_file.display_name = display_name
        audio_file.audio_type = audio_type
        audio_file.save()
        return Response(status=status.HTTP_200_OK)

@api_view(["GET"])
@csrf_exempt
def audio_file(request, filename: str):

    if request.method == 'GET':
        img = open(f'{django_settings.MEDIA_ROOT}/music/{filename}', 'rb')

        return FileResponse(img)

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def playlist(request):

    if request.method == 'GET':
        name = request.GET.get("name", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        playlists = Playlist.objects.all()
        if name:
            playlists = playlists.filter(name__icontains=name)
        playlists = playlists.filter(user=request.dnd_user)

        playlists = playlists.order_by("name")

        p = Paginator(playlists, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [playlist.to_simple_dict() for playlist in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        name = request.data.get("name")
        audio_type = request.data.get("audio_type")
        Playlist.create(name, audio_type, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "PUT", "GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def playlist_by_id(request, playlist_id: str):

    if request.method == 'DELETE':
        playlist: Playlist = Playlist.objects.get(pk=playlist_id, user=request.dnd_user)
        playlist.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        name = request.data.get("name")
        playlist: Playlist = Playlist.objects.get(pk=playlist_id, user=request.dnd_user)
        playlist.name = name
        playlist.save()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'GET':
        playlist: Playlist = Playlist.objects.get(pk=playlist_id, user=request.dnd_user)
        return JsonResponse(playlist.to_dict(), safe=False)

@api_view(["DELETE", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def playlist_by_id_item(request, playlist_id: str, audio_id: str):

    if request.method == 'DELETE':
        playlist: Playlist = Playlist.objects.get(pk=playlist_id, user=request.dnd_user)
        audio_file: AudioFile = AudioFile.objects.get(pk=audio_id, user=request.dnd_user)
        playlist.remove_audio_file(audio_file)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'POST':
        playlist: Playlist = Playlist.objects.get(pk=playlist_id, user=request.dnd_user)
        audio_file: AudioFile = AudioFile.objects.get(pk=audio_id, user=request.dnd_user)
        playlist.add_audio_file(audio_file)
        return Response(status=status.HTTP_200_OK)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def user_music(request):

    if request.method == 'GET':
        music_data = get_data(f"{request.dnd_user.id}-music")
        ambience_data = get_data(f"{request.dnd_user.id}-ambience")
        if not music_data:
            music_data = {
                "tracks": [],
                "playlist": None
            }
        if not ambience_data:
            ambience_data = {
                "tracks": [],
                "playlist": None
            }
        data = {
            "music": music_data,
            "ambience": ambience_data
        }
        return JsonResponse(data, safe=False)

class FileUploadView(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        if request.dnd_user is None:
            raise NotAuthenticated(detail="Token is invalid.")

        if request.dnd_user.can_upload == False:
            raise ValueError("User has reached max upload limit")

        file_obj = request.FILES['file']
        file_size = request.FILES['file'].size
        display_name = request.data['display_name']
        audio_type = request.data['audio_type']

        if display_name is None:
            raise ValidationError("display_name is required")
        if audio_type is None:
            raise ValidationError("audio_type is required")

        file_name = f"{str(uuid4())}.{file_obj.name.split('.')[-1]}"
        full_file_name = f"music/{file_name}"
        fs = FileSystemStorage() #defaults to MEDIA_ROOT  
        full_file_name = fs.save(full_file_name, file_obj)

        upload_obj = Upload(user=request.dnd_user, file_location=full_file_name, file_size=file_size, upload_type=Upload.TYPE_AUDIO)
        upload_obj.save()

        audio_file = AudioFile(
            display_name = display_name,
            file_name = file_name,
            source = None,
            status = AudioFile.STATUS_READY,
            user=request.dnd_user,
            audio_type=audio_type
        )
        audio_file.save()

        return Response(status=status.HTTP_200_OK)