from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from dnd_controller.utils import user_content

from dnd_controller.models.all import (
    SpellList,
)
from dnd_controller.middleware.auth import is_authenticated

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def spell_list(request):
    
    if request.method == 'GET':
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)


        spell_lists = SpellList.objects.filter(user=request.dnd_user).order_by("dnd_class__name")

        p = Paginator(spell_lists, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [spell_list.to_simple_dict() for spell_list in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        class_id = request.data.get("class_id")
        dnd_class = user_content.get_dnd_class(request.dnd_user).get(pk=class_id)
        SpellList.create(request.dnd_user, dnd_class)
        return Response(status=status.HTTP_200_OK)

@api_view(["GET", 'DELETE'])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def spell_list_by_id(request, spell_list_id):
    
    if request.method == 'GET':
        spell_list = SpellList.objects.filter(user=request.dnd_user).get(pk=spell_list_id)
        return JsonResponse(spell_list.to_dict(), safe=False)

    elif request.method == 'DELETE':
        spell_list = SpellList.objects.filter(user=request.dnd_user).get(pk=spell_list_id)
        spell_list.delete()
        return Response(status=status.HTTP_200_OK)

@api_view(["POST", 'DELETE'])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def spell_list_by_id_spell(request, spell_list_id, spell_id):
    
    if request.method == 'POST':
        spell_list = SpellList.objects.filter(user=request.dnd_user).get(pk=spell_list_id)
        spell = user_content.get_spell(request.dnd_user).get(pk=spell_id)
        spell_list.add_spell(spell)
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        spell_list = SpellList.objects.filter(user=request.dnd_user).get(pk=spell_list_id)
        spell_list.remove_spell(spell)
        return Response(status=status.HTTP_200_OK)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def spell_list_by_class(request, class_id):

    if request.method == 'GET':
        spell_lists = user_content.get_spell_list(request.dnd_user).filter(dnd_class_id=class_id).all()
        data = {
            "results": [spell_list.to_dict() for spell_list in spell_lists]
        }
        return JsonResponse(data, safe=False)