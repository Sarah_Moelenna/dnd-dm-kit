from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from dnd_controller.models import EffectDeck
from dnd_controller.middleware.auth import is_authenticated
import json

@api_view(["GET", "POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def effect_deck(request):

    if request.method == 'GET':
        name = request.GET.get("name", None)
        page = request.GET.get("page", 1)
        results_per_page = request.GET.get("results_per_page", 20)

        decks = EffectDeck.objects.all()
        if name:
            decks = decks.filter(name__icontains=name)
        decks = decks.filter(user=request.dnd_user)

        decks = decks.order_by("name")

        p = Paginator(decks, results_per_page)

        data = {
            "current_page": page,
            "total_pages": p.num_pages,
            "total_results": p.count,
            "results": [deck.to_simple_dict() for deck in p.page(page)]
        }
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        name = request.data.get("name")
        data = request.data.get("data")
        EffectDeck.create(data, name, request.dnd_user)
        return Response(status=status.HTTP_200_OK)

@api_view(["DELETE", "PUT", "GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
@is_authenticated
def effect_deck_by_id(request, deck_id: str):

    if request.method == 'DELETE':
        deck: EffectDeck = EffectDeck.objects.get(pk=deck_id, user=request.dnd_user)
        deck.delete()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        name = request.data.get("name")
        data = request.data.get("data")
        deck: EffectDeck = EffectDeck.objects.get(pk=deck_id, user=request.dnd_user)
        if name is not None:
            deck.name = name
        if data is not None:
            deck.data = json.dumps(data)
        deck.save()
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'GET':
        deck: EffectDeck = EffectDeck.objects.get(pk=deck_id, user=request.dnd_user)
        return JsonResponse(deck.to_dict(), safe=False)