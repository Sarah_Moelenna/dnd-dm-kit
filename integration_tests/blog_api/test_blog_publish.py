import requests
import json
from utils.settings import BLOG_API_URL
from utils.user_helper import UserHelper
from utils.blog_helper import BlogHelper

def test_edit_blog_as_admin():
    blog_data = BlogHelper.create_draft_blog()
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.post(
        url=f'{BLOG_API_URL}/blogs/{blog_data["id"]}/publish',
        headers=headers
    )

    assert response.status_code == 200

    response_data = response.json()

    assert response_data['status'] == 'PUBLISHED'

def test_edit_blog_as_admin_cant_publish_twice():
    blog_data = BlogHelper.create_draft_blog()
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.post(
        url=f'{BLOG_API_URL}/blogs/{blog_data["id"]}/publish',
        headers=headers
    )

    assert response.status_code == 200

    response = requests.post(
        url=f'{BLOG_API_URL}/blogs/{blog_data["id"]}/publish',
        headers=headers
    )

    assert response.status_code == 404

def test_edit_blog_as_admin_not_found():
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.post(
        url=f'{BLOG_API_URL}/blogs/bbvfcgfdgfg/publish',
        headers=headers
    )

    assert response.status_code == 404