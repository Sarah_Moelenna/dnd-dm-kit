import requests
import json
from utils.settings import BLOG_API_URL
from utils.user_helper import UserHelper
from utils.blog_helper import BlogHelper

def test_create_blog_success():
    token, user = UserHelper.login_as_superadmin()

    data = {
        "title": "My cool blog",
    }

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.post(
        url=f'{BLOG_API_URL}/blogs',
        data=json.dumps(data),
        headers=headers
    )

    response_data = response.json()

    assert response.status_code == 200

    assert 'id' in response_data.keys()
    assert 'title' in response_data.keys()
    assert 'author' in response_data.keys()
    assert 'excerpt' in response_data.keys()
    assert 'thumbnail_src' in response_data.keys()
    assert 'published_at' in response_data.keys()
    assert 'meta_title' in response_data.keys()
    assert 'meta_description' in response_data.keys()
    assert 'meta_image' in response_data.keys()
    assert 'status' in response_data.keys()

def test_create_blog_errors_on_duplicate_title():
    token, user = UserHelper.login_as_superadmin()

    data = {
        "title": "My duplicate test blog",
    }
    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    first_response = requests.post(
        url=f'{BLOG_API_URL}/blogs',
        data=json.dumps(data),
        headers=headers
    )
    second_response = requests.post(
        url=f'{BLOG_API_URL}/blogs',
        data=json.dumps(data),
        headers=headers
    )

    assert first_response.status_code == 200
    assert second_response.status_code == 500

def test_get_blogs():
    _ = BlogHelper.create_draft_blog()

    response = requests.get(
        url=f'{BLOG_API_URL}/blogs',
    )

    response_data = response.json()

    assert 'results' in response_data.keys()
    assert 'total_pages' in response_data.keys()
    assert 'current_page' in response_data.keys()
    assert 'total_results' in response_data.keys()

    for blog in response_data['results']:
        assert blog['status'] == 'PUBLISHED'


def test_get_blogs_as_admin():
    _ = BlogHelper.create_draft_blog()
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.get(
        url=f'{BLOG_API_URL}/blogs',
        headers=headers,
    )

    response_data = response.json()

    assert 'results' in response_data.keys()
    assert 'total_pages' in response_data.keys()
    assert 'current_page' in response_data.keys()
    assert 'total_results' in response_data.keys()
    assert len(response_data['results']) > 0

    assert 'id' in response_data['results'][0].keys()
    assert 'title' in response_data['results'][0].keys()
    assert 'author' in response_data['results'][0].keys()
    assert 'excerpt' in response_data['results'][0].keys()
    assert 'thumbnail_src' in response_data['results'][0].keys()
    assert 'published_at' in response_data['results'][0].keys()
    assert 'meta_title' in response_data['results'][0].keys()
    assert 'meta_description' in response_data['results'][0].keys()
    assert 'meta_image' in response_data['results'][0].keys()
    assert 'status' in response_data['results'][0].keys()