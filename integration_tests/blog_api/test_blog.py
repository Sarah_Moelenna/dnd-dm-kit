import requests
import json
from utils.settings import BLOG_API_URL
from utils.user_helper import UserHelper
from utils.blog_helper import BlogHelper

def test_get_blog_as_admin():
    blog_data = BlogHelper.create_draft_blog()
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.get(
        url=f'{BLOG_API_URL}/blogs/{blog_data["id"]}',
        headers=headers,
    )

    assert response.status_code == 200

    response_data = response.json()

    assert 'id' in response_data.keys()
    assert 'title' in response_data.keys()
    assert 'author' in response_data.keys()
    assert 'excerpt' in response_data.keys()
    assert 'thumbnail_src' in response_data.keys()
    assert 'published_at' in response_data.keys()
    assert 'meta_title' in response_data.keys()
    assert 'meta_description' in response_data.keys()
    assert 'meta_image' in response_data.keys()
    assert 'status' in response_data.keys()

def test_get_blog_as_no_one_draft_blog():
    blog_data = BlogHelper.create_draft_blog()

    response = requests.get(
        url=f'{BLOG_API_URL}/blogs/{blog_data["id"]}',
    )

    assert response.status_code == 404

def test_get_blog_as_admin_not_found():
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.get(
        url=f'{BLOG_API_URL}/blogs/bfghbfdghgfnfgjng',
        headers=headers,
    )

    assert response.status_code == 404

def test_edit_blog_as_admin():
    blog_data = BlogHelper.create_draft_blog()
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    data = {
        "title": "my cool title test",
        "author": "bruce",
        "excerpt": "vfdzvfdvdf",
        "thumbnail_src": "gdfagdf",
        "meta_title": "gzfdgdfg",
        "meta_description": "zgdfgzdfg",
        "meta_image": "zdgdfzgd",
        "slug": "gfdgd",
        "tag": "gdgrdfv ",
        "banner_src": "gfdgdgd",
    }

    response = requests.patch(
        url=f'{BLOG_API_URL}/blogs/{blog_data["id"]}',
        headers=headers,
        data=json.dumps(data)
    )

    assert response.status_code == 200

    response_data = response.json()

    assert response_data['title'] == data['title']
    assert response_data['author'] == data['author']
    assert response_data['excerpt'] == data['excerpt']
    assert response_data['thumbnail_src'] == data['thumbnail_src']
    assert response_data['meta_title'] == data['meta_title']
    assert response_data['meta_description'] == data['meta_description']
    assert response_data['meta_image'] == data['meta_image']
    assert response_data['slug'] == data['slug']
    assert response_data['tag'] == data['tag']
    assert response_data['banner_src'] == data['banner_src']

def test_edit_blog_as_admin_cant_edit_status():
    blog_data = BlogHelper.create_draft_blog()
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    data = {
        "status": "PUBLISHED",
    }

    response = requests.patch(
        url=f'{BLOG_API_URL}/blogs/{blog_data["id"]}',
        headers=headers,
        data=json.dumps(data)
    )

    assert response.status_code == 400

def test_edit_blog_as_admin_must_send_one_field():
    blog_data = BlogHelper.create_draft_blog()
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    data = {}

    response = requests.patch(
        url=f'{BLOG_API_URL}/blogs/{blog_data["id"]}',
        headers=headers,
        data=json.dumps(data)
    )

    assert response.status_code == 400
