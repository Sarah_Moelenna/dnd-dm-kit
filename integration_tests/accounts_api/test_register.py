import requests
from utils.settings import ACCOUNTS_API_URL
import json

def test_register_success():
    data = {
        "forename": "sarah",
        "surname": "anderson",
        "email": "sarah@campaignerstoolkit.com",
        "display_name": "SarahMoelenna",
        "password": "password123$"
    }

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.post(
        url=f'{ACCOUNTS_API_URL}/register',
        data=json.dumps(data),
        headers=headers
    )

    response_data = response.json()

    assert 'token' in response_data.keys()
    assert 'expires_at' in response_data.keys()

def test_register_reject_duplicate_email():
    data = {
        "forename": "sarah",
        "surname": "anderson",
        "email": "sarah@campaignerstoolkit.com",
        "display_name": "SarahMoelenna",
        "password": "password123$"
    }

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.post(
        url=f'{ACCOUNTS_API_URL}/register',
        data=json.dumps(data),
        headers=headers
    )

    status_code = response.status_code
    assert status_code is not 200