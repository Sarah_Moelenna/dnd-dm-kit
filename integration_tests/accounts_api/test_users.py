import requests
from utils.settings import ACCOUNTS_API_URL
import json
from utils.user_helper import UserHelper

def test_users_all_filters_success():
    token, _ = UserHelper.login_as_superadmin()
    _, _, user_data = UserHelper.register_new_user()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/users?display_name={user_data["display_name"]}&email={user_data["email"]}&user_type={user_data["user_type"]}',
        headers=headers
    )

    response_data = response.json()

    assert response_data['results'][0]['display_name'] == user_data['display_name']
    assert response_data['results'][0]['email'] == user_data['email']
    assert response_data['results'][0]['user_type'] == user_data['user_type']

def test_users_no_filters_success():
    token, _ = UserHelper.login_as_superadmin()
    _, _, _ = UserHelper.register_new_user()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/users',
        headers=headers
    )

    response_data = response.json()

    assert 'display_name' in response_data['results'][0]
    assert 'email' in response_data['results'][0]
    assert 'user_type' in response_data['results'][0]

def test_users_insufficient_authentication():
    token, _, _ = UserHelper.register_new_user()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/users',
        headers=headers
    )

    assert response.status_code == 403

def test_users_post_successful():
    token, _ = UserHelper.login_as_superadmin()

    data = {
        "forename": "arandom",
        "surname": "testuser",
        "email": "testuser@campaignerstoolkit.com",
        "display_name": "testuserrandom1",
        "password": "password123$",
        "user_type": "USER"
    }

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.post(
        url=f'{ACCOUNTS_API_URL}/users',
        data=json.dumps(data),
        headers=headers
    )

    response_data = response.json()

    assert response.status_code == 200
    assert response_data['forename'] == "arandom"
    assert response_data['surname'] == "testuser"
    assert response_data['display_name'] == "testuserrandom1"
    assert response_data['user_type'] == "USER"

def test_users_post_wrong_user_type():
    token, _ = UserHelper.login_as_superadmin()

    data = {
        "forename": "arandom28",
        "surname": "testuser28",
        "email": "testuser28@campaignerstoolkit.com",
        "display_name": "testuserrandom128",
        "password": "password123$",
        "user_type": "FUNUSER"
    }

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.post(
        url=f'{ACCOUNTS_API_URL}/users',
        data=json.dumps(data),
        headers=headers
    )

    assert response.status_code == 400

def test_users_post_superadmin_creation_not_allowed():
    token, _ = UserHelper.login_as_superadmin()

    data = {
        "forename": "arandom23",
        "surname": "testuser23",
        "email": "testuser23@campaignerstoolkit.com",
        "display_name": "testuserrandom123",
        "password": "password123$",
        "user_type": "SUPERADMIN"
    }

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.post(
        url=f'{ACCOUNTS_API_URL}/users',
        data=json.dumps(data),
        headers=headers
    )

    assert response.status_code == 403

def test_users_post_insufficient_permissions():

    data = {
        "forename": "arandom235",
        "surname": "testuser235",
        "email": "testuser235@campaignerstoolkit.com",
        "display_name": "testuserrandom1235",
        "password": "password123$",
        "user_type": "USER"
    }

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.post(
        url=f'{ACCOUNTS_API_URL}/users',
        data=json.dumps(data),
        headers=headers
    )

    assert response.status_code == 403

def test_users_post_missing_parameter():
    token, _ = UserHelper.login_as_superadmin()

    data = {
        "surname": "testuser",
        "email": "testuser@campaignerstoolkit.com",
        "display_name": "testuserrandom1",
        "password": "password123$",
        "user_type": "USER"
    }

    headers = {
        "Content-Type": "application/json",
        "token": token
    }

    response = requests.post(
        url=f'{ACCOUNTS_API_URL}/users',
        data=json.dumps(data),
        headers=headers
    )

    assert response.status_code == 400

def test_users_by_id_successful():
    token, _ = UserHelper.login_as_superadmin()
    _, _, user_data = UserHelper.register_new_user()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/users/{user_data["id"]}',
        headers=headers
    )

    response_data = response.json()

    assert response_data['id'] == user_data['id']

def test_users_by_id_wrong_id():
    token, _ = UserHelper.login_as_superadmin()
    _, _, user_data = UserHelper.register_new_user()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/users/d5ee739b-e8fc-4934-ab7f-ad6ed885fe59',
        headers=headers
    )

    assert response.status_code == 404

def test_users_by_id_delete_successful():
    token, _ = UserHelper.login_as_superadmin()
    _, _, user_data = UserHelper.register_new_user()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    delete_response = requests.delete(
        url=f'{ACCOUNTS_API_URL}/users/{user_data["id"]}',
        headers=headers
    )

    assert delete_response.status_code == 204

def test_users_by_id_put_successful():
    token, _ = UserHelper.login_as_superadmin()
    _, _, user_data = UserHelper.register_new_user()

    data = {
        "forename": "putdataforename1",
        "surname": "putdatasurname1",
        "email": "putdataemail1@campaignerstoolkit.com",
        "display_name": "putdatadisplayname1",
        "password": "password1234$",
        "user_type": "ADMIN"
    }

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.put(
        url=f'{ACCOUNTS_API_URL}/users/{user_data["id"]}',
        headers=headers,
        data=json.dumps(data)
    )

    response_data = response.json()

    assert response.status_code == 200
    assert response_data['forename'] == data['forename']
    assert response_data['surname'] == data['surname']
    assert response_data['email'] == data['email']
    assert response_data['display_name'] == data['display_name']
    assert response_data['user_type'] == data['user_type']

def test_users_by_id_put_create_new_superadmin():
    token, _ = UserHelper.login_as_superadmin()
    _, _, user_data = UserHelper.register_new_user()

    data = {
        "forename": "putdataforename2",
        "surname": "putdatasurname2",
        "email": "putdataemail2@campaignerstoolkit.com",
        "display_name": "putdatadisplayname2",
        "password": "password1234$",
        "user_type": "SUPERADMIN"
    }

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.put(
        url=f'{ACCOUNTS_API_URL}/users/{user_data["id"]}',
        headers=headers,
        data=json.dumps(data)
    )

    assert response.status_code == 403

def test_users_by_id_put_modify_superadmin():
    token, admin_data = UserHelper.login_as_superadmin()

    data = {
        "forename": "putdataforename3",
        "surname": "putdatasurname3",
        "email": "putdataemail3@campaignerstoolkit.com",
        "display_name": "putdatadisplayname3",
        "password": "password1234$",
        "user_type": "USER"
    }

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.put(
        url=f'{ACCOUNTS_API_URL}/users/{admin_data["id"]}',
        headers=headers,
        data=json.dumps(data)
    )

    assert response.status_code == 404

def test_users_by_id_put_user_deleted():
    token, _ = UserHelper.login_as_superadmin()
    _, _, user_data = UserHelper.register_new_user()

    data = {
        "forename": "putdataforename4",
        "surname": "putdatasurname4",
        "email": "putdataemail4@campaignerstoolkit.com",
        "display_name": "putdatadisplayname4",
        "password": "password1234$",
        "user_type": "USER"
    }

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    delete_response = requests.delete(
        url=f'{ACCOUNTS_API_URL}/users/{user_data["id"]}',
        headers=headers
    )

    response = requests.put(
        url=f'{ACCOUNTS_API_URL}/users/{user_data["id"]}',
        headers=headers,
        data=json.dumps(data)
    )

    assert response.status_code == 404

def test_users_by_id_put_wrong_user_type():
    token, _ = UserHelper.login_as_superadmin()
    _, _, user_data = UserHelper.register_new_user()

    data = {
        "forename": "putdataforename5",
        "surname": "putdatasurname5",
        "email": "putdataemail5@campaignerstoolkit.com",
        "display_name": "putdatadisplayname5",
        "password": "password1234$",
        "user_type": "FUNUSER"
    }

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.put(
        url=f'{ACCOUNTS_API_URL}/users/{user_data["id"]}',
        headers=headers,
        data=json.dumps(data)
    )

    assert response.status_code == 404