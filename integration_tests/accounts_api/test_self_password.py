import requests
from utils.settings import ACCOUNTS_API_URL
import json
from utils.user_helper import UserHelper

def test_self_password_edit_success():

    token, password, user_data = UserHelper.register_new_user()
    
    put_headers = {
        "Content-Type": "application/json",
        "token" : token
    }
    put_data = {
        "current_password": password,
        "new_password": "newpassword456!",
    }
    put_response = requests.put(
        url=f'{ACCOUNTS_API_URL}/self/password',
        data=json.dumps(put_data),
        headers=put_headers
    )
    assert put_response.status_code == 201
    
    login_data = {
        "email": user_data['email'],
        "password": "newpassword456!",
    }

    login_headers = {
        "Content-Type": "application/json"
    }

    login_response = requests.post(
        url=f'{ACCOUNTS_API_URL}/login',
        data=json.dumps(login_data),
        headers=login_headers
    )

    assert login_response.status_code == 200

def test_self_password_edit_wrong_password():
    token, password, user_data = UserHelper.register_new_user()

    put_headers = {
        "Content-Type": "application/json",
        "token" : token
    }
    put_data = {
        "current_password": "wrongpassword",
        "new_password": "newpassword456!",
    }
    put_response = requests.put(
        url=f'{ACCOUNTS_API_URL}/self/password',
        data=json.dumps(put_data),
        headers=put_headers
    )
    assert put_response.status_code != 201

def test_self_password_edit_missing_password():
    token, password, user_data = UserHelper.register_new_user()
    
    put_headers = {
        "Content-Type": "application/json",
        "token" : token
    }
    put_data = {
        "current_password": "wrongpassword"
    }
    put_response = requests.put(
        url=f'{ACCOUNTS_API_URL}/self/password',
        data=json.dumps(put_data),
        headers=put_headers
    )
    assert put_response.status_code != 201