import requests
from utils.settings import ACCOUNTS_API_URL
import json

def test_login_success():
    data = {
        "email": "admin@campaignerstoolkit.com",
        "password": "adminpassword123",
    }

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.post(
        url=f'{ACCOUNTS_API_URL}/login',
        data=json.dumps(data),
        headers=headers
    )

    response_data = response.json()

    assert 'token' in response_data.keys()
    assert 'expires_at' in response_data.keys()