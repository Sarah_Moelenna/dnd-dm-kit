import requests
from utils.settings import ACCOUNTS_API_URL
import json
from utils.user_helper import UserHelper

def test_permissions_success():
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/permissions',
        headers=headers
    )

    response_data = response.json()

    assert "ACCOUNTS.USER.MANAGEMENT" in response_data

def test_permissions_insufficient_privilage():

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/permissions',
        headers=headers
    )

    assert response.status_code == 403