import requests
from utils.settings import ACCOUNTS_API_URL
import json
from utils.user_helper import UserHelper

def test_users_types_success():
    token, _ = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/user-types',
        headers=headers
    )

    response_data = response.json()

    assert "ADMIN" in response_data
    assert "USER" in response_data
    assert "SUPERADMIN" in response_data

def test_users_types_insufficient_permissions():

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/user-types',
        headers=headers
    )

    assert response.status_code == 403