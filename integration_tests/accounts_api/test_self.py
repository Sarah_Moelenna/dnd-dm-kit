import requests
from utils.settings import ACCOUNTS_API_URL
import json
from utils.user_helper import UserHelper

def test_self_edit_success():
    token, password, user_data = UserHelper.register_new_user()

    put_headers = {
        "Content-Type": "application/json",
        "token" : token
    }
    put_data = {
        "forename": "new",
        "surname": "name",
        "display_name": "NewData"
    }
    put_response = requests.put(
        url=f'{ACCOUNTS_API_URL}/self',
        data=json.dumps(put_data),
        headers=put_headers
    )
    put_response_data = put_response.json()

    assert put_response_data['forename'] == "new"
    assert put_response_data['surname'] == "name"
    assert put_response_data['display_name'] == "NewData"

def test_self_missing_user():
    headers = {
        "Content-Type": "application/json"
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/self',
        headers=headers
    )
    status_code = response.status_code

    assert status_code != 200

def test_self_user_success():
    token, user_data = UserHelper.login_as_superadmin()

    headers = {
        "Content-Type": "application/json",
        "token" : token
    }

    response = requests.get(
        url=f'{ACCOUNTS_API_URL}/self',
        headers=headers
    )

    response_data = response.json()

    assert "permissions" in response_data
    assert len(response_data['permissions']) >=0