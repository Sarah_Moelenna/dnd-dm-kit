import requests
from utils.settings import ACCOUNTS_API_URL
import json
from typing import Tuple, Dict
from utils.string_generator import generate_string

class UserHelper():

    existing_emails = []
    existing_display_names = []

    @classmethod
    def login_as_superadmin(cls) -> Tuple[str, Dict[str, str]]:
        """
        get token and user_data blob for SUPERADMIN account
        """
        data = {
            "email": "admin@campaignerstoolkit.com",
            "password": "adminpassword123",
        }

        headers = {
            "Content-Type": "application/json"
        }

        response = requests.post(
            url=f'{ACCOUNTS_API_URL}/login',
            data=json.dumps(data),
            headers=headers
        )

        token = response.json()['token']

        headers = {
            "Content-Type": "application/json",
            "token": token
        }

        response = requests.get(
            url=f'{ACCOUNTS_API_URL}/self',
            headers=headers
        )

        user_data = response.json()

        return token, user_data

    @classmethod
    def register_new_user(cls) -> Tuple[str, str, Dict[str, str]]:
        """
        returns the token, password and use_data dict from a randomly generated new user
        """
        password = generate_string(16)

        # register a new user and get token
        data = {
            "forename": "Test",
            "surname": "User0001",
            "email": cls.generate_email(),
            "display_name": cls.generate_display_name(),
            "password": password
        }
        headers = {
            "Content-Type": "application/json"
        }
        response = requests.post(
            url=f'{ACCOUNTS_API_URL}/register',
            data=json.dumps(data),
            headers=headers
        )
        response_data = response.json()
        token = response_data["token"]

        # get user data blob for that user
        headers = {
            "Content-Type": "application/json",
            "token": token
        }
        response = requests.get(
            url=f'{ACCOUNTS_API_URL}/self',
            headers=headers
        )
        user_data = response.json()

        return token, password, user_data

    @classmethod
    def generate_email(cls) -> str:
        """
        generates a new email that hasn't been used before
        """
        unique_email_found = False
        while not unique_email_found:
            email = f'{generate_string(16)}@{generate_string(10)}@.com'
            if email not in cls.existing_emails:
                unique_email_found = True
        return email 

    @classmethod
    def generate_display_name(cls) -> str:
        """
        generates a new display_name that hasn't been used before
        """
        unique_display_name_found = False
        while not unique_display_name_found:
            display_name = generate_string(20)
            if display_name not in cls.existing_display_names:
                unique_display_name_found = True
        return display_name 
