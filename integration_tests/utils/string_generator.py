import random

def generate_string(length: int) -> str:
    """
    generate a random string of given length
    """
    abc = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    chars=[]
    for i in range(length):
        chars.append(random.choice(abc))

    return "".join(chars)
