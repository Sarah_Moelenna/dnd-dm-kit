import requests
from utils.settings import BLOG_API_URL
import json
from typing import Dict
from utils.user_helper import UserHelper
from utils.string_generator import generate_string

class BlogHelper():

    existing_titles = []

    @classmethod
    def create_draft_blog(cls) -> Dict[str, str]:
        token, _ = UserHelper.login_as_superadmin()

        data = {
            "title": cls.generate_title(),
        }

        headers = {
            "Content-Type": "application/json",
            "token": token
        }

        response = requests.post(
            url=f'{BLOG_API_URL}/blogs',
            data=json.dumps(data),
            headers=headers
        )

        return response.json()

    @classmethod
    def generate_title(cls) -> str:
        """
        generates a new title that hasn't been used before
        """
        unique_title = False
        while not unique_title:
            title = f'{generate_string(16)}'
            if title not in cls.existing_titles:
                unique_title = True
        return title 
