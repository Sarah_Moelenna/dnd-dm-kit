<?php

    error_reporting(E_ERROR | E_PARSE);

    include 'database_connection.php';
    include 'responses.php';
    include 'utils.php';
    include 'models/user.php';
    include 'models/blog.php';
    include 'models/blog_component.php';

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: *');
    header("Access-Control-Allow-Headers: X-Requested-With, token, content-type");

    $request_url = explode('?', $_SERVER['REQUEST_URI'])[0];
    $request_method = $_SERVER['REQUEST_METHOD'];

    if(isset($_SERVER['HTTP_TOKEN'])){
        $token = $_SERVER['HTTP_TOKEN'];
        $GLOBALS['USER'] = User::getUserForToken($token);
        $GLOBALS['USER']->checkPermissions();
    }

    function blogs_view($request_url, $request_method){
        if($request_method == 'GET'){
            $page = intval($_GET["page"] ?: 1) - 1;
            if(isset($GLOBALS['USER']) == false){
                Blog::getBlogs($page, "PUBLISHED", "published_at", "DESC");
            } else {
                $status = $_GET["status"] ?: NULL;
                Blog::getBlogs($page, $status, "published_at", "DESC");
            }
        
        } elseif($request_method == 'POST'){
        
            if(isset($GLOBALS['USER']) == false){
                AuthErrorResponse();
            }
            $json_params = json_decode(file_get_contents("php://input"));
            Blog::createBlog($json_params);
        
        } else {
            MethodNotAllowedResponse();
        }
    }

    function blog_view($request_url, $request_method){
        $id = str_replace("/blogs/", "", $request_url);
        if($request_method == 'GET'){
            if(isset($GLOBALS['USER']) == false){
                Blog::getBlogForID($id, true);
            } else {
                Blog::getBlogForID($id, false);
            }

        } elseif($request_method == 'PATCH'){
        
            if(isset($GLOBALS['USER']) == false){
                AuthErrorResponse();
            }
            $json_params = json_decode(file_get_contents("php://input"), true);
            Blog::editBlog($id, $json_params);
        
        } else {
            MethodNotAllowedResponse();
        }
    }

    function blog_publish_view($request_url, $request_method){
        $id = str_replace("/publish", "", str_replace("/blogs/", "", $request_url));
        if($request_method == 'POST'){
            if(isset($GLOBALS['USER']) == false){
                AuthErrorResponse();
            }
            Blog::publishBlogForID($id);

        } else {
            MethodNotAllowedResponse();
        }
    }

    function image_upload_view($request_url, $request_method){
        if($request_method == 'POST'){
            if(isset($GLOBALS['USER']) == false){
                AuthErrorResponse();
            }

            $check = getimagesize($_FILES["file"]["tmp_name"]);
            if($check !== false) {
                //file is an image
                $target_file = "images/" . generate_id() . "." . strtolower(end(explode(".", $_FILES["file"]["name"])));
                if ( ! is_dir("images")) {
                    mkdir("images");
                }
                $result = move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
                $response = array(
                    "file_location" => $target_file,
                    "success" => $result
                );
                SuccessResponse($response);
            } else {
                BadRequestResponse("Can only submit an image.");
            }

        } else {
            MethodNotAllowedResponse();
        }
    }

    function images_views($request_url, $request_method){
        $file_name = str_replace("/images/", "", $request_url);
        $file_type = end(explode(".", $file_name));
        if($request_method == 'GET'){
            // open the file in a binary mode
            $fp = fopen($file_name, 'rb');

            // send the right headers
            header("Content-Type: image/" . $file_type);
            header("Content-Length: " . filesize("./images/" . $file_name));

            // dump the picture and stop the script
            fpassthru($fp);
            exit;
        } else {
            MethodNotAllowedResponse();
        }
    }

    function handle_options($request_method){
        if($request_method == 'OPTIONS'){
            NoContentResponse();
        }
    }

    if($request_url == '/blogs'){
        header('Content-Type:application/json');
        handle_options($request_method);
        blogs_view($request_url, $request_method);
    } elseif($request_url == '/image-upload'){
        header('Content-Type:application/json');
        handle_options($request_method);
        image_upload_view($request_url, $request_method);
    } elseif(preg_match("#\/images\/[a-zA-Z0-9-.]*$#", $request_url)){
        header('Content-Type:application/json');
        handle_options($request_method);
        images_view($request_url, $request_method);
    } elseif(preg_match("#\/blogs\/[a-zA-Z0-9-]*$#", $request_url)){
        header('Content-Type:application/json');
        handle_options($request_method);
        blog_view($request_url, $request_method);
    } elseif(preg_match("#\/blogs\/[a-zA-Z0-9-]*\/publish$#", $request_url)){
        header('Content-Type:application/json');
        handle_options($request_method);
        blog_publish_view($request_url, $request_method);
    } else {
        ResourceNotFoundResponse();
    }

?>