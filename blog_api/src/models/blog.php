<?php

    class Blog{

        function __construct(
            $id,
            $title,
            $meta_title,
            $meta_description,
            $meta_image,
            $author,
            $tag,
            $excerpt,
            $slug,
            $thumbnail_src,
            $banner_src,
            $status,
            $created_by,
            $created_at,
            $updated_at,
            $deleted_at,
            $published_at,
            $content,
            $previous_blog
        ) {
            $this->id = $id;
            $this->title = $title;
            $this->meta_title = $meta_title;
            $this->meta_description = $meta_description;
            $this->meta_image = $meta_image;
            $this->author = $author;
            $this->tag = $tag;
            $this->excerpt = $excerpt;
            $this->slug = $slug;
            $this->thumbnail_src = $thumbnail_src;
            $this->banner_src = $banner_src;
            $this->status = $status;
            $this->created_by = $created_by;
            $this->created_at = $created_at;
            $this->updated_at = $updated_at;
            $this->deleted_at = $deleted_at;
            $this->published_at = $published_at;
            $this->content = $content;
            $this->previous_blog = $previous_blog;
        }

        public function to_array(){
            $content = $this->content;
            if($content != null){
                $content = json_decode($content);
            }

            $author = $this->author;
            if($author != null){
                $author = json_decode($author);
            }

            return array(
                "id" => $this->id,
                "title" => $this->title,
                "meta_title" => $this->meta_title,
                "meta_description" => $this->meta_description,
                "meta_image" => $this->meta_image,
                "author" => $author,
                "tag" => $this->tag,
                "excerpt" => $this->excerpt,
                "slug" => $this->slug,
                "thumbnail_src" => $this->thumbnail_src,
                "banner_src" => $this->banner_src,
                "status" => $this->status,
                "created_by" => $this->created_by,
                "created_at" => $this->created_at,
                "updated_at" => $this->updated_at,
                "deleted_at" => $this->deleted_at,
                "published_at" => $this->published_at,
                "content" => $content,
                "previous_blog" => $this->previous_blog,
                "next_blog_info" => $this->get_next_blog(),
                "previous_blog_info" => $this->get_previous_blog()
            );
        }

        public function get_next_blog(){
            intiateDatabaseConnection();
            $sql = "SELECT title, slug, tag FROM `blog` where previous_blog = '" . $this->id . "' and status = 'PUBLISHED' ORDER BY published_at DESC LIMIT 1";
            $result = $GLOBALS['db_handle']->query($sql);
            if($result == false || $result->num_rows == 0){
                closeDatabaseConnection();
                return null;
            }
            $row = $result->fetch_array();
            closeDatabaseConnection();
            return array(
                "title" => $row["title"],
                "path" => '/' . $row["tag"] . '/' . $row['slug']
            );
        }   

        public function get_previous_blog(){
            if($this->previous_blog == null){
                return null;
            }

            intiateDatabaseConnection();
            $sql = "SELECT title, slug, tag FROM `blog` where id = '" . $this->previous_blog . "' and status = 'PUBLISHED' LIMIT 1";
            $result = $GLOBALS['db_handle']->query($sql);
            if($result == false || $result->num_rows == 0){
                closeDatabaseConnection();
                return null;
            }
            $row = $result->fetch_array();
            closeDatabaseConnection();
            return array(
                "title" => $row["title"],
                "path" => '/' . $row["tag"] . '/' . $row['slug']
            );
        }   

        public function to_simple_array(){
            $author = $this->author;
            if($author != null){
                $author = json_decode($author);
            }

            return array(
                "id" => $this->id,
                "title" => $this->title,
                "author" => $author,
                "excerpt" => $this->excerpt,
                "thumbnail_src" => $this->thumbnail_src,
                "published_at" => $this->published_at,
                "meta_title" => $this->meta_title,
                "meta_description" => $this->meta_description,
                "meta_image" => $this->meta_image,
                "status" => $this->status
            );
        }

        public static function initBlogFromRow($row){
            return new Blog(
                $row["id"],
                $row["title"],
                $row["meta_title"],
                $row["meta_description"],
                $row["meta_image"],
                $row["author"],
                $row["tag"],
                $row["excerpt"],
                $row["slug"],
                $row["thumbnail_src"],
                $row["banner_src"],
                $row["status"],
                $row["created_by"],
                $row["created_at"],
                $row["updated_at"],
                $row["deleted_at"],
                $row["published_at"],
                $row["content"],
                $row["previous_blog"]
            );
        }

        public static function getTotalBlogCount($status_filter){
            if(is_null($status_filter)){
                $SQL = "SELECT COUNT(*) as blog_count FROM blog";
            } else {
                $SQL = "SELECT COUNT(*) as blog_count FROM blog where status = '" . $status_filter . "'";
            }
            $result = $GLOBALS['db_handle']->query($SQL);
            $row = $result->fetch_assoc();
            return $row['blog_count'];
        }

        public static function getBlogForID($id, $published_only=true){
            // validate $id is a string
            if(is_string($id) == false){
                ResourceNotFoundResponse();
            }
            intiateDatabaseConnection();

            if($published_only == true){
                $SQL = "SELECT * FROM `blog`  WHERE id='" . filter_var($id , FILTER_SANITIZE_STRING) . "' and status = 'PUBLISHED' LIMIT 1";
            } else {
                $SQL = "SELECT * FROM `blog`  WHERE id='" . filter_var($id , FILTER_SANITIZE_STRING) . "' LIMIT 1";
            }

            $result = $GLOBALS['db_handle']->query($SQL);
            if($result == false || $result->num_rows == 0){
                ResourceNotFoundResponse();
            }
            $row = $result->fetch_array();
            closeDatabaseConnection();
            $blog = Blog::initBlogFromRow($row);
            SuccessResponse($blog->to_array());
        }

        public static function editBlog($id, $params){
            // get blog
            intiateDatabaseConnection();
            $SQL = "SELECT * FROM `blog`  WHERE id='" . $id . "' and status = 'DRAFT' LIMIT 1";
            $result = $GLOBALS['db_handle']->query($SQL);
            if($result == false || $result->num_rows == 0){
                ResourceNotFoundResponse();
            }

            //check theres at least one field to update
            if(count(array_keys($params)) == 0){
                BadRequestResponse('must submit at least one field');
            }

            //start building sql and check submitted fields are valid
            $editable_fields = array("title", "author", "excerpt", "thumbnail_src", "meta_title", "meta_description", "meta_image", "slug", "tag", "banner_src", "content", "previous_blog");
            $keys = array_keys($params);

            $sql = "UPDATE blog SET ";
            foreach($keys as $key){
                if(!in_array($key, $editable_fields)){
                    BadRequestResponse($key . ' is not a valid field to edit');
                }
                $value = $params[$key];
                if($key == "content" || $key == "author"){
                    $value = json_encode($value);
                    $sql = $sql . filter_var($key , FILTER_SANITIZE_STRING) . " = '" . $value . "', ";
                } else{
                    $sql = $sql . filter_var($key , FILTER_SANITIZE_STRING) . " = '" . filter_var($value , FILTER_SANITIZE_STRING) . "', ";
                }
                
            }
            $sql = substr($sql, 0, -2);
            $sql = $sql . " WHERE id='" . filter_var($id , FILTER_SANITIZE_STRING) . "' and status = 'DRAFT'";

            // run sql
            $result = $GLOBALS['db_handle']->query($sql);
            if($GLOBALS['db_handle']->error != false){
                var_dump($sql);
                var_dump($GLOBALS['db_handle']->error);
                ServerErrorResponse();
            }

            closeDatabaseConnection();
            Blog::getBlogForID($id, false);
        }

        public static function publishBlogForID($id){
            intiateDatabaseConnection();
            $SQL = "SELECT * FROM `blog`  WHERE id='" . $id . "' and status = 'DRAFT' LIMIT 1";
            $result = $GLOBALS['db_handle']->query($SQL);
            if($result == false || $result->num_rows == 0){
                ResourceNotFoundResponse();
            }

            $sql = "UPDATE blog SET status = 'PUBLISHED' where id = '" . filter_var($id , FILTER_SANITIZE_STRING) . "' and status = 'DRAFT'";
            // run sql
            $result = $GLOBALS['db_handle']->query($sql);
            if($GLOBALS['db_handle']->error != false){
                ServerErrorResponse();
            }

            closeDatabaseConnection();
            Blog::getBlogForID($id, false);

        }

        public static function getBlogs($page, $status_filter, $order_key, $order_direction){
            // validate $page is an int
            if(is_int($page) == false){
                ResourceNotFoundResponse();
            }

            $blogs = array();
            intiateDatabaseConnection();

            if(is_null($status_filter)){
                $SQL = "SELECT * FROM blog ORDER BY " . $order_key . " " . $order_direction . " LIMIT " . ($page * 20) . "," . (($page * 20) + 20);
            } else {
                $SQL = "SELECT * FROM blog WHERE status='" . $status_filter . "' ORDER BY " . $order_key . " " . $order_direction . " LIMIT " . ($page * 20) . "," . (($page * 20) + 20);
            }
            $result = $GLOBALS['db_handle']->query($SQL);
            if($result != false){
                while($row = $result->fetch_assoc()) {
                    array_push($blogs, Blog::initBlogFromRow($row)->to_simple_array());
                }
            }

            $blog_count = Blog::getTotalBlogCount($status_filter);
            $data = array(
                "results" => $blogs,
                "current_page" => $page + 1,
                "total_pages" => ceil($blog_count/20),
                "total_results" => $blog_count
            );

            closeDatabaseConnection();
            SuccessResponse($data);
        }

        public static function createBlog($params){
            if(isset($params->title) == false){
                BadRequestResponse("title must be provided");
            }

            $title = filter_var($params->title , FILTER_SANITIZE_STRING);
            intiateDatabaseConnection();
            $id = generate_id();

            $SQL = "INSERT INTO blog
            (id, title, status, created_by)
            VALUES('" . $id . "', '" . $title . "', 'DRAFT', '" . $GLOBALS['USER']->id . "')";
            $result = $GLOBALS['db_handle']->query($SQL);
            if($GLOBALS['db_handle']->error != false){
                ServerErrorResponse();
            }

            closeDatabaseConnection();
            Blog::getBlogForID($id, false);
        }

    }


?>