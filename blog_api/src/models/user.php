<?php

    class User{

        function __construct(
            $id,
            $forename,
            $surname,
            $display_name,
            $email,
            $user_type,
            $permissions
        ) {
            $this->id = $id;
            $this->forename = $forename;
            $this->surname = $surname;
            $this->display_name = $display_name;
            $this->email = $email;
            $this->user_type = $user_type;
            $this->permissions = $permissions;
        }

        public function to_array(){
            return array(
                "id" => $this->id,
                "forename" => $this->forename,
                "surname" => $this->surname,
                "display_name" => $this->display_name,
                "email" => $this->email,
                "user_type" => $this->user_type,
                "permissions" => $this->permissions
            );
        }

        public function to_simple_array(){
            return array(
                "forename" => $this->forename,
                "surname" => $this->surname
            );
        }

        public function checkPermissions(){
            if($this->user_type != "ADMIN" && $this->user_type != "SUPERADMIN"){
                AuthErrorResponse();
            }
            if (!in_array("BLOGS.MANAGEMENT", $this->permissions)) {
                AuthErrorResponse();
            }
        }

        public static function getUserForToken($token){
            $url = $_ENV['ACCOUNTS_API_HOST'] . "/self";
            $ch = curl_init($url);
    
            // Set the content type to application/json and add auth token
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'token:'.$token));
            // Return response instead of outputting
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // set as GET
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
    
            $result = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
            curl_close($ch);

            if($status != "200"){
                AuthErrorResponse();
            }

            $user_data = json_decode($result);

            return new User(
                $user_data->id,
                $user_data->forename,
                $user_data->surname,
                $user_data->display_name,
                $user_data->email,
                $user_data->user_type,
                $user_data->permissions
            );
        }
    }


?>