<?php

    function AuthErrorResponse(){
        $response = [
            "error" => "Unauthorized",
            "status" => "401",
        ];
        http_response_code(401);
        echo json_encode($response);
        exit();
    }

    function MethodNotAllowedResponse(){
        $response = [
            "error" => "Method Not Allowed",
            "status" => "405",
        ];
        http_response_code(405);
        echo json_encode($response);
        exit();
    }

    function ResourceNotFoundResponse(){
        $response = [
            "error" => "Resource Not Found",
            "status" => "404",
        ];
        http_response_code(404);
        echo json_encode($response);
        exit();
    }

    function BadRequestResponse($details){
        $response = [
            "error" => "Bad Request - " . $details,
            "status" => "400",
        ];
        http_response_code(400);
        echo json_encode($response);
        exit();
    }

    function ServerErrorResponse(){
        $response = [
            "error" => "Server Error",
            "status" => "500",
        ];
        http_response_code(500);
        echo json_encode($response);
        exit();
    }

    function SuccessResponse($response){
        http_response_code(200);
        echo json_encode($response);
        exit();
    }

    function CreatedResponse(){
        http_response_code(201);
        exit();
    }

    function NoContentResponse(){
        http_response_code(204);
        exit();
    }

?>