from flask import Flask, request
from flask_socketio import SocketIO, emit, join_room, rooms, leave_room
from redis import StrictRedis
import json
import os
import requests

app = Flask(__name__)
app.config['ENV'] = 'development'
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = "secret"
socketio = SocketIO(app, cors_allowed_origins='*', engineio_logger=True, logger=True, path='/socket')

REDIS_URL = "redis://redis/"
redis = StrictRedis.from_url(REDIS_URL)

def get_map_from_api(map_id, auth_token):
    response = requests.get(f"http://{os.getenv('API_HOST')}/api/map/{map_id}", headers={"token": auth_token})
    response.raise_for_status()
    result = response.json()
    return result

def get_combat_from_api(combat_id, auth_token):
    response = requests.get(f"http://{os.getenv('API_HOST')}/api/combat/{combat_id}", headers={"token": auth_token})
    response.raise_for_status()
    result = response.json()
    return result

def get_message_data(room):
    messages = []
    for i in range(0, redis.llen(room + "-messages")):
        messages.append(json.loads(redis.lindex(room + "-messages", i)))
    emit("messages", messages, include_self=True, to=room, namespace='/socket')

def get_map_data(room):
    cache = redis.get(room + "-map")
    if cache:
        data = json.loads(cache)
        emit("map", data, include_self=True, to=room, namespace='/socket')

def get_combat_data(room):
    cache = redis.get(room + "-combat")
    if cache:
        data = json.loads(cache)
        emit("combat", data, include_self=True, to=room, namespace='/socket')

def get_audio_data(room):
    cache = redis.get(room + "-music_name")
    if cache:
        data = json.loads(cache)
        emit("music_name", data, include_self=True, to=room, namespace='/socket')
    cache = redis.get(room + "-ambience_name")
    if cache:
        data = json.loads(cache)
        emit("ambience_name", data, include_self=True, to=room, namespace='/socket')

    cache = redis.get(room + "-music_volume")
    if cache:
        data = json.loads(cache)
        emit("music_volume", data, include_self=True, to=room, namespace='/socket')
    cache = redis.get(room + "-ambience_volume")
    if cache:
        data = json.loads(cache)
        emit("ambience_volume", data, include_self=True, to=room, namespace='/socket')

def clear_map_data(room):
    cache = redis.delete(room + "-map")
    emit("map", None, include_self=True, to=room, namespace='/socket')

def clear_combat_data(room):
    cache = redis.delete(room + "-combat")
    emit("combat", None, include_self=True, to=room, namespace='/socket')

def clear_messages(room):
    cache = redis.delete(room + "-messages")
    emit("messages", [], include_self=True, to=room, namespace='/socket')

def clear_session(room):
    clear_map_data(room)
    clear_messages(room)
    clear_combat_data(room)
    emit("session_end", include_self=True, to=room, namespace='/socket')

def get_map_id(room):
    cache = redis.get(room + "-map")
    if cache:
        data = json.loads(cache)
        emit("map_id", data['id'], include_self=True, to=room, namespace='/socket')

def get_combat_id(room):
    cache = redis.get(room + "-combat")
    if cache:
        data = json.loads(cache)
        emit("combat_id", data['id'], include_self=True, to=room, namespace='/socket')

def get_session_data(room):
    # map
    get_map_data(room)
    get_message_data(room)
    get_combat_data(room)
    get_audio_data(room)


def get_session_room():
    for room in rooms():
        if room.startswith('session-'):
            return room
    raise ValueError("Not Connected to Any Session Room")

def leave_all_session_room():
    for room in rooms():
        if room.startswith('session-'):
            leave_room(room)

#def ack():
#    print('message was received!')
#
#@socketio.on('connect')
#def test_connect(auth):
#    raise ValueError(auth)
#    send(message)
#    #emit('my response', {'data': 'Connected'})
#
#@socketio.on('disconnect')
#def test_disconnect():
#    print('Client disconnected')

## ------------------------ SESSION ----------------------------##

@socketio.on('session_end', namespace='/socket')
def handle_session_end():
    room = get_session_room()
    clear_session(room)


## ------------------------ COMBAT ----------------------------##

@socketio.on('get_current_combat_id', namespace='/socket')
def handle_map_id():
    room = get_session_room()
    get_combat_id(room)

@socketio.on('combat_update', namespace='/socket')
def handle_combat(combat_id, auth):
    room = get_session_room()
    data = get_combat_from_api(combat_id, auth)
    redis.set(room + "-combat", json.dumps(data))
    emit("combat", data, broadcast=True, to=room, namespace='/socket')

@socketio.on('combat_clear', namespace='/socket')
def handle_combat_clear():
    room = get_session_room()
    clear_combat_data(room)

@socketio.on('combat_move', namespace='/socket')
def handle_combat_move(id, data):
    room = get_session_room()
    emit("combat_move", {"id": id, "values": data}, include_self=True, broadcast=True, to=room, namespace='/socket')

## ------------------------ MAPS ----------------------------##
@socketio.on('get_current_map_id', namespace='/socket')
def handle_map_id():
    room = get_session_room()
    get_map_id(room)

@socketio.on('map_ping', namespace='/socket')
def handle_map_ping(coordinates):
    coordinates["id"] = request.sid
    room = get_session_room()
    emit("map_ping", coordinates, include_self=True, broadcast=True, to=room, namespace='/socket')

@socketio.on('map_clear', namespace='/socket')
def handle_map_clear():
    room = get_session_room()
    clear_map_data(room)

@socketio.on('map_update', namespace='/socket')
def handle_map(map_id, auth):
    room = get_session_room()
    data = get_map_from_api(map_id, auth)
    redis.set(room + "-map", json.dumps(data))
    emit("map", data, broadcast=True, to=room, namespace='/socket')

## ------------------------ AUDIO ----------------------------##

@socketio.on('song_change_music', namespace='/socket')
def handle_song_change(name):
    room = get_session_room()
    redis.set(room + "-music_name", json.dumps(name))
    emit("music_name", name, broadcast=True, to=room, namespace='/socket')

@socketio.on('song_change_ambience', namespace='/socket')
def handle_ambience_change(name):
    room = get_session_room()
    redis.set(room + "-ambience_name", json.dumps(name))
    emit("ambience_name", name, broadcast=True, to=room, namespace='/socket')

@socketio.on('music_volume', namespace='/socket')
def handle_music_volume_change(value):
    room = get_session_room()
    redis.set(room + "-music_volume", json.dumps(value))
    emit("music_volume", value, include_self=True, broadcast=True, to=room, namespace='/socket')

@socketio.on('ambience_volume', namespace='/socket')
def handle_ambience_volume_change(value):
    room = get_session_room()
    redis.set(room + "-ambience_volume", json.dumps(value))
    emit("ambience_volume", value, include_self=True, broadcast=True, to=room, namespace='/socket')

@socketio.on('sound_effect_play', namespace='/socket')
def handle_sound_effect(file):
    room = get_session_room()
    emit("sound_effect_play", file, include_self=True, broadcast=True, to=room, namespace='/socket')

## ------------------------ JOIN ----------------------------##

@socketio.on('join', namespace='/socket')
def handle_join(room_id):
    room = "session-" + room_id
    leave_all_session_room()
    join_room(room)
    get_session_data(room)

## ------------------------ MESSAGES ----------------------------##

@socketio.on('send_message', namespace='/socket')
def handle_message(data):
    room = get_session_room()
    redis.lpush(room + "-messages", json.dumps(data))
    get_message_data(room)

if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port="5000")