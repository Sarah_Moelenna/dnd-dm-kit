run:
	- docker-compose up

run_prod:
	- docker-compose -f docker-compose-prod.yaml up -d

stop_prod:
	- docker-compose -f docker-compose-prod.yaml down

build_api:
	- docker-compose build api
	- docker-compose build migration_runner
	- docker-compose build auto_backup_tool

build_api_accounts:
	- docker-compose build accounts_api
	- docker-compose build accounts_migration_runner

build_blog_api:
	- docker-compose build blog_api

build_worker:
	- docker-compose build discord_bot_worker
	- docker-compose build dnd_beyond_scrape_worker

build_websocket:
	- docker-compose build dnd_websockets

build_react:
	- docker-compose build front_end

build_react_prod:
	- docker-compose -f docker-compose-prod.yaml build front_end_prod

build_promotional:
	- docker-compose build promotional

build_promotional_prod:
	- docker-compose -f docker-compose-prod.yaml build promotional_prod

build_nginx:
	- docker-compose build nginx

build_all:
	build_api
	build_api_accounts
	build_nginx
	build_react
	build_worker

create_migrations:
	- cd django_backend_api/dnd_api && python manage.py makemigrations dnd_controller

create_empty_migration:
	- cd django_backend_api/dnd_api && python manage.py makemigrations dnd_controller --empty

create_migrations_accounts:
	- cd accounts_api && python manage.py makemigrations accounts

create_empty_migration_accounts:
	- cd accounts_api && python manage.py makemigrations accounts --empty

run_integration_tests:
	cd integration_tests && make test