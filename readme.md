# About
An all in one solution for the DM with too many chrome tabs.

## Features:
- Campaign Management: create and manage your ongoing campaigns.
- Music Bot: download and loop music in discord from youtube.
- DNDBeyond Integration: build encounters from owned monsters, and run them from the encounter control panel.
- Battlemap: create battlemaps connected to encounters that your players can interact with.
- Campaign Organisation: keep all your notes in one place with links to maps, items, monsters and music.

# Installation

- Install Docker
```
https://www.docker.com/products/docker-desktop
```

- Install the chrome extension in the "chrome_scraper" directory
```
https://webkul.com/blog/how-to-install-the-unpacked-extension-in-chrome/
```

- Create a discord bot, invite to a server and get the token
```
https://discordpy.readthedocs.io/en/stable/discord.html
```

- Copy the env sample
```
cp .env.sample .env
```

- Add your discord bot token to the .env file

- Run the following commands
```
mkdir media
mkdir media/music
make build_all
```

# Running

To run, use the following command:
```
make run
```

The front end is then accessible at:
```
http://localhost:3000/
```

# Testing

To run integration tests, use the following commands:
```
make run_integration_tests
```