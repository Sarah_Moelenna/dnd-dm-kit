import requests
import math
from typing import Dict

def scrape_character_sheet(url: str) -> Dict:
    character_dict = {}

    character_id = url.split('/')[-1]
    response = requests.get(f"https://character-service.dndbeyond.com/character/v4/character/{character_id}")
    character_data = response.json()

    character_dict['url'] = url
    character_dict['name'] = character_data['data']['name']
    character_dict['avatar'] = character_data['data']['avatarUrl']
    character_dict['current_xp'] = int(character_data['data']['currentXp']) if character_data['data']['currentXp'] else 0


    base_hit_points = int(character_data['data']['baseHitPoints']) if character_data['data']['baseHitPoints'] else 0
    bonus_hit_points = int(character_data['data']['bonusHitPoints']) if character_data['data']['bonusHitPoints'] else 0
    removed_hit_points = int(character_data['data']['removedHitPoints']) if character_data['data']['removedHitPoints'] else 0

    base_strength = int(character_data['data']['stats'][0]['value']) if character_data['data']['stats'][0]['value'] else 0
    base_dexterity = int(character_data['data']['stats'][1]['value']) if character_data['data']['stats'][1]['value'] else 0
    base_constitution = int(character_data['data']['stats'][2]['value']) if character_data['data']['stats'][2]['value'] else 0
    base_intelligence = int(character_data['data']['stats'][3]['value']) if character_data['data']['stats'][3]['value'] else 0
    base_wisdom = int(character_data['data']['stats'][4]['value']) if character_data['data']['stats'][4]['value'] else 0
    base_charisma = int(character_data['data']['stats'][5]['value']) if character_data['data']['stats'][5]['value'] else 0

    bonus_strength = int(character_data['data']['bonusStats'][0]['value']) if character_data['data']['bonusStats'][0]['value'] else 0
    bonus_dexterity = int(character_data['data']['bonusStats'][1]['value']) if character_data['data']['bonusStats'][1]['value'] else 0
    bonus_constitution = int(character_data['data']['bonusStats'][2]['value']) if character_data['data']['bonusStats'][2]['value'] else 0
    bonus_intelligence = int(character_data['data']['bonusStats'][3]['value']) if character_data['data']['bonusStats'][3]['value'] else 0
    bonus_wisdom = int(character_data['data']['bonusStats'][4]['value']) if character_data['data']['bonusStats'][4]['value'] else 0
    bonus_charisma = int(character_data['data']['bonusStats'][5]['value']) if character_data['data']['bonusStats'][5]['value'] else 0

    override_strength = int(character_data['data']['overrideStats'][0]['value']) if character_data['data']['overrideStats'][0]['value'] else 0
    override_dexterity = int(character_data['data']['overrideStats'][1]['value']) if character_data['data']['overrideStats'][1]['value'] else 0
    override_constitution = int(character_data['data']['overrideStats'][2]['value']) if character_data['data']['overrideStats'][2]['value'] else 0
    override_intelligence = int(character_data['data']['overrideStats'][3]['value']) if character_data['data']['overrideStats'][3]['value'] else 0
    override_wisdom = int(character_data['data']['overrideStats'][4]['value']) if character_data['data']['overrideStats'][4]['value'] else 0
    override_charisma = int(character_data['data']['overrideStats'][5]['value']) if character_data['data']['overrideStats'][5]['value'] else 0

    bonus_armour_class = 0

    # modifiers
    modifiers = []
    for key in character_data['data']['modifiers'].keys():
        modifiers.extend(character_data['data']['modifiers'][key])

    for modifier in modifiers:
        if modifier['type'] == 'bonus':
            if modifier['subType'] == 'strength-score':
                bonus_strength = bonus_strength + modifier['value']
            elif modifier['subType'] == 'dexterity-score':
                bonus_dexterity = bonus_dexterity + modifier['value']
            elif modifier['subType'] == 'constitution-score':
                bonus_constitution = bonus_constitution + modifier['value']
            elif modifier['subType'] == 'intelligence-score':
                bonus_intelligence = bonus_intelligence + modifier['value']
            elif modifier['subType'] == 'wisdom-score':
                bonus_wisdom = bonus_wisdom + modifier['value']
            elif modifier['subType'] == 'charisma-score':
                bonus_charisma = bonus_charisma + modifier['value']
            elif modifier['subType'] == 'armor-class':
                bonus_armour_class = bonus_armour_class + modifier['value']

    level = 0
    for dnd_class in character_data['data']['classes']:
        level = level + dnd_class['level']

    strength = (base_strength + bonus_strength) if not override_strength else override_strength
    dexterity = (base_dexterity + bonus_dexterity) if not override_dexterity else override_dexterity
    constitution = (base_constitution + bonus_constitution) if not override_constitution else override_constitution
    intelligence = (base_intelligence + bonus_intelligence) if not override_intelligence else override_intelligence
    wisdom = (base_wisdom + bonus_wisdom) if not override_wisdom else override_wisdom
    charisma = (base_charisma + bonus_charisma) if not override_charisma else override_charisma

    constitution_health = math.floor(max(0, (constitution) - 10) / 2) * level
    character_dict['max_health'] = base_hit_points + bonus_hit_points + constitution_health
    character_dict['current_health'] = character_dict['max_health'] - removed_hit_points

    character_dict['strength'] = strength
    character_dict['dexterity'] = dexterity
    character_dict['constitution'] = constitution
    character_dict['intelligence'] = intelligence
    character_dict['wisdom'] = wisdom
    character_dict['charisma'] = charisma

    return character_dict
