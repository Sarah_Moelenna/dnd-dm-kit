import asyncio
from redis import StrictRedis
import logging
import json
from typing import List
from scraper import scrape_character_sheet

logging.basicConfig(level=logging.DEBUG)

#redis stuff
REDIS_URL = "redis://redis/"
redis = StrictRedis.from_url(REDIS_URL)

def get_work_keys() ->List[str]:
    keys_to_work = []
    for key in redis.scan_iter("CHARACTER_TRACKNG_SETTINGS-*"):
        keys_to_work.append(key.decode("utf-8"))
    return keys_to_work


def process_work_key(key: str):
    settings_key = key
    data_key = key.replace('SETTINGS', 'DATA')

    settings_cache = redis.get(settings_key)
    #logging.debug(settings_cache)
    if settings_cache is not None:
        redis.delete(settings_key)
        new_character_data = []
        character_settings = json.loads(settings_cache)
        #logging.debug(character_settings)
        if character_settings:
            for character_setting in character_settings:
                try:
                    character_data = scrape_character_sheet(character_setting['url'])
                    new_character_data.append(character_data)
                except Exception as e:
                    logging.debug(str(e))
            redis.set(data_key, json.dumps(new_character_data), ex=30)
            logging.debug(json.dumps(new_character_data))


async def worker_loop():
    while True:
        keys_to_work = get_work_keys()
        for key in keys_to_work:
            process_work_key(key)
        
        #logging.debug("SLEEPING")
        await asyncio.sleep(0.5)

loop = asyncio.new_event_loop()
loop.create_task(worker_loop())
loop.run_forever()
