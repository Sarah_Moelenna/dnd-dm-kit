import requests
from state import CampaignState, BotState
from typing import Dict
import os

def update_state_with_api_state():
    response = requests.get(f"http://{os.getenv('API_HOST')}/api/settings")
    settings_data: Dict = response.json()

    active_campaign_ids = []

    if settings_data.get('active_campaigns', None) is not None:
        for campaign in settings_data.get('active_campaigns', []):

            active_campaign = campaign
            active_server_id = int(campaign['server_id']) if campaign['server_id'] else None
            active_voice_channel = int(campaign['voice_channel_id']) if campaign['voice_channel_id'] else None
            active_rolling_channel = int(campaign['text_rolling_channel_id']) if campaign['text_rolling_channel_id'] else None
            active_info_channel = int(campaign['text_info_channel_id']) if campaign['text_info_channel_id'] else None

            active_campaign_ids.append(campaign['id'])
            BotState.update_campaign_state(campaign['id'], active_campaign, active_server_id, active_voice_channel, active_rolling_channel, active_info_channel)

    BotState.update_inactive_campaigns(active_campaign_ids)