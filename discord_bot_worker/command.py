from typing import Dict
from state import BotState

def process_command(data: Dict):
    campaign_state = BotState.get_campaign(data['campaign_id'])

    if data['command_code'] == 'CMD_PLAY':

        if data['music_play_type'] == 'LOOP':
            campaign_state.should_music_loop = True
        elif data['music_play_type'] == 'ONCE':
            campaign_state.should_music_loop = False

        if f"media/music/{data['file_name']}" != campaign_state.song_to_play:
            campaign_state.song_change = True
        campaign_state.song_to_play = f"media/music/{data['file_name']}"
        campaign_state.should_music_play = True

    elif data['command_code'] == 'CMD_STOP':
        campaign_state.should_music_loop = False
        campaign_state.should_music_play = False
        campaign_state.song_change = True

    elif data['command_code'] == 'CMD_SHARE_IMG':
        if data['location'] == "ROLLING":
            campaign_state.rolling_post_queue.append(data['url'])
        elif data['location'] == "INFO":
            campaign_state.info_post_queue.append(data['url'])

    elif data['command_code'] == 'CMD_SHARE_TXT':
        if data['location'] == "ROLLING":
            campaign_state.rolling_post_queue.append(data['text'])
        elif data['location'] == "INFO":
            campaign_state.info_post_queue.append(data['text'])

    elif data['command_code'] == 'CMD_UPLOAD_IMG':
        if data['location'] == "ROLLING":
            campaign_state.rolling_upload_queue.append(
                {"file": data['filepath'], "delete": data['delete']}
            )
        elif data['location'] == "INFO":
            campaign_state.info_upload_queue.append(
                {"file": data['filepath'], "delete": data['delete']}
            )