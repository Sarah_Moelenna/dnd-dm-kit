from discord import Guild as DiscordGuild
from typing import List
from redis import StrictRedis
import json

class Guild():

    def __init__(self, guild: DiscordGuild):
        self.id = guild.id
        self.name = guild.name
        self.text_channels = [{"id": str(channel.id), "name": channel.name} for channel in guild.text_channels]
        self.voice_channels = [{"id": str(channel.id), "name": channel.name} for channel in guild.voice_channels]

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "text_channels": self.text_channels,
            "voice_channels": self.voice_channels,
        }


class CampaignState():

    should_music_play = False
    should_music_loop = False
    song_to_play = None
    song_change = False
    id = None
    active_campaign = None
    should_change_voice_channel = True
    active_server_id = None
    active_voice_channel = None
    active_rolling_channel = None
    active_info_channel = None
    rolling_post_queue = []
    info_post_queue = []
    rolling_upload_queue = []
    info_upload_queue = []
    rolling_messages = []

    #discord objects
    voice_channel = None
    voice_client = None

    def set_voice_channel(self, voice_channel):
        self.voice_channel = voice_channel

    def set_voice_client(self, voice_client):
        self.voice_client = voice_client

    def get_state_dict(self):
        return {
            "should_music_play": self.should_music_play,
            "should_music_loop": self.should_music_loop,
            "song_to_play": self.song_to_play,
            "song_change": self.song_change,
            "id": self.id,
            "active_campaign": self.active_campaign,
            "active_server_id": str(self.active_server_id),
            "active_voice_channel": str(self.active_voice_channel),
            "active_rolling_channel": str(self.active_rolling_channel),
            "active_info_channel": str(self.active_info_channel),
            "rolling_post_queue": str(self.rolling_post_queue),
            "info_post_queue": str(self.info_post_queue),
            "rolling_upload_queue": str(self.rolling_upload_queue),
            "info_upload_queue": str(self.info_upload_queue),
            "rolling_messages": self.rolling_messages,
        }

    def update_redis_with_bot_state(self, redis: StrictRedis):
        if self.active_campaign is not None:
            redis.set(f"{self.active_campaign['id']}:BOT_STATE", json.dumps(self.get_state_dict()))

    def clear_state(self):
        self.should_music_play = None
        self.should_music_loop = False
        self.song_to_play = None
        self.song_change = False
        self.guilds = {}
        self.active_campaign = None
        self.should_change_voice_channel = True
        self.active_server_id = None
        self.active_voice_channel = None
        self.active_rolling_channel = None
        self.active_info_channel = None
        self.rolling_post_queue = []
        self.info_post_queue = []
        self.rolling_upload_queue = []
        self.info_upload_queue = []
        self.rolling_messages = []


class BotState():

    campaign_states = {}
    guilds = {}

    @classmethod
    def get_state_dict(cls):
        return {
            "guilds": [guild.to_dict() for guild in cls.guilds],
        }

    @classmethod
    def update_campaign_state(cls, campaign_id: str, active_campaign, active_server_id, active_voice_channel, active_rolling_channel, active_info_channel):
        current_state = cls.campaign_states.get(campaign_id, None)
        if current_state is None:
            current_state = CampaignState()
            current_state.active_campaign = active_campaign
            current_state.active_server_id = active_server_id
            current_state.active_voice_channel = active_voice_channel
            current_state.active_rolling_channel = active_rolling_channel
            current_state.active_info_channel = active_info_channel
            current_state.should_change_voice_channel = True
            current_state.id = campaign_id
        else:
            current_state.should_change_voice_channel = current_state.active_voice_channel != active_voice_channel
            current_state.active_campaign = active_campaign
            current_state.active_server_id = active_server_id
            current_state.active_voice_channel = active_voice_channel
            current_state.active_rolling_channel = active_rolling_channel
            current_state.active_info_channel = active_info_channel
            current_state.id = campaign_id

        cls.campaign_states[campaign_id] = current_state

    @classmethod
    def update_guild_state(cls, guilds: List[DiscordGuild]):
        new_guilds = [Guild(guild) for guild in guilds]
        cls.guilds = new_guilds

    @classmethod
    def update_redis_with_bot_state(cls, redis: StrictRedis):
        redis.set("BOT_STATE", json.dumps(cls.get_state_dict()))

        for campaign_state in cls.campaign_states.values():
            campaign_state.update_redis_with_bot_state(redis)

    @classmethod
    def update_inactive_campaigns(cls, active_ids: List[str]):
        for campaign_id in cls.campaign_states.keys():
            if campaign_id not in active_ids:
                current_state = cls.campaign_states.get(campaign_id, None)
                if current_state is not None:
                    current_state.clear_state()


    @classmethod
    def campaigns(cls) -> List[CampaignState]:
        return cls.campaign_states.values()

    @classmethod
    def get_campaign(cls, id: str) -> CampaignState:
        return cls.campaign_states.get(id, None)