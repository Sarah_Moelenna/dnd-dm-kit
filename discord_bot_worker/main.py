from time import sleep

import discord
from discord.ext import commands,tasks
import os
import asyncio
import json
from typing import List
from redis import StrictRedis
from state import BotState
from command import process_command
from api_integration import update_state_with_api_state
from datetime import datetime

# Get the API token from the .env file.
DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")
REDIS_URL = "redis://redis/"

intents = discord.Intents().all()
client = discord.Client(intents=intents)
bot = commands.Bot(command_prefix='!',intents=intents)

redis = StrictRedis.from_url(REDIS_URL)


async def process_redis():
    await client.wait_until_ready()
    while True:
        try:
            BotState.update_guild_state(client.guilds)
            BotState.update_redis_with_bot_state(redis)

            command_cache = redis.get("COMMANDS")
            redis.set("COMMANDS", json.dumps([]))
            
            if command_cache:
                commands = json.loads(command_cache)
                if commands:
                    for command in commands:
                        process_command(command)
        except Exception as e:
            print(str(e))
            raise e

        await asyncio.sleep(1)

async def process_backend_settings():
    while True:
        try:
            update_state_with_api_state()
        except Exception as e:
            print(str(e))

        await asyncio.sleep(1)

async def process_audio():
    await client.wait_until_ready()

    while True:
        for campaign_state in BotState.campaigns():
            try:
                if campaign_state.should_change_voice_channel:

                    if campaign_state.active_voice_channel is not None:
                        campaign_state.should_change_voice_channel = False

                        if campaign_state.voice_client is not None:
                            await campaign_state.voice_client.disconnect()
                            campaign_state.set_voice_client(
                                None
                            )

                        campaign_state.set_voice_channel(
                            client.get_channel(campaign_state.active_voice_channel)
                        )

                        if campaign_state.voice_channel is not None:
                            if campaign_state.voice_client is not None:
                                await campaign_state.voice_client.disconnect()
                            voice_client = await campaign_state.voice_channel.connect()
                            campaign_state.set_voice_client(
                                voice_client
                            )
                    else:
                        if campaign_state.voice_client is not None:
                            await campaign_state.voice_client.disconnect()
                            campaign_state.set_voice_client(
                                None
                            )

                # only do these if we're connected to a voice client
                if campaign_state.voice_client is not None:
                    if campaign_state.song_change:
                        campaign_state.voice_client.stop()
                        campaign_state.song_change = False
                    if campaign_state.should_music_play and not campaign_state.voice_client.is_playing():
                        campaign_state.voice_client.play(discord.FFmpegPCMAudio(source=campaign_state.song_to_play))
                        if not campaign_state.should_music_loop:
                            campaign_state.should_music_play = False
            
            except Exception as e:
                print(str(e))
                #raise e

        await asyncio.sleep(1)

async def process_posting():
    await client.wait_until_ready()

    while True:
        for campaign_state in BotState.campaigns():
            try:

                if campaign_state.active_rolling_channel is not None:
                    rolling_channel = client.get_channel(campaign_state.active_rolling_channel)
                    if rolling_channel is not None:
                        for message in campaign_state.rolling_post_queue:
                            await rolling_channel.send(message)
                        campaign_state.rolling_post_queue = []

                        for file in campaign_state.rolling_upload_queue:
                            await rolling_channel.send(file=discord.File(f"media/{file['file']}"))
                            if file["delete"] == True:
                                os.remove(f"media/{file['file']}")
                        campaign_state.rolling_upload_queue = []

                if campaign_state.active_info_channel is not None:
                    info_channel = client.get_channel(campaign_state.active_info_channel)
                    if info_channel is not None:
                        for message in campaign_state.info_post_queue:
                            await info_channel.send(message)
                        campaign_state.info_post_queue = []

                        for file in campaign_state.info_upload_queue:
                            await info_channel.send(file=discord.File(f"media/{file['file']}"))
                            if file["delete"] == True:
                                os.remove(f"media/{file['file']}")
                        campaign_state.info_upload_queue = []

                if campaign_state.active_rolling_channel is not None:
                    rolling_channel = client.get_channel(campaign_state.active_rolling_channel)
                    if rolling_channel is not None:
                        messages: List = await rolling_channel.history(limit=20).flatten()
                        message_dicts = []
                        for message in messages:
                            message_dict = {
                                "id": str(message.id),
                                "author": message.author.display_name,
                                "content": message.content,
                                "mentioned_users": [],
                                "embeds": [embed.to_dict() for embed in message.embeds],
                                "minutes_since_created": int((datetime.now() - message.created_at).total_seconds()/60),
                            }
                            ids_found = []
                            for mention in message.mentions:
                                ids_found.append(str(mention.id))
                                message_dict['mentioned_users'].append({
                                    'id': str(mention.id),
                                    'name': mention.display_name,
                                })

                            if len(message.raw_mentions) > 0:
                                guild = client.get_guild(campaign_state.active_server_id)
                                for member_id in message.raw_mentions:
                                    if str(member_id) not in ids_found:
                                        member = None
                                        if guild:
                                            member = guild.get_member(member_id)
                                        if not member:
                                            member = client.get_user(member_id)
                                        if member:
                                            message_dict['mentioned_users'].append({
                                                'id': str(member.id),
                                                'name': member.display_name,
                                            })
                            message_dicts.append(message_dict)
                        campaign_state.rolling_messages = message_dicts


            
            except Exception as e:
                print(str(e))
                raise e

        await asyncio.sleep(1)


client.loop.create_task(process_audio())
client.loop.create_task(process_redis())
client.loop.create_task(process_backend_settings())
client.loop.create_task(process_posting())
client.run(DISCORD_TOKEN)