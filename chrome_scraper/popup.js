// Regex-pattern to check URLs against. 
// It matches URLs like: http[s]://[...]stackoverflow.com[...]
var monsterRegex = new RegExp("https:\\/\\/www\\.dndbeyond\\.com\\/monsters\\/([\\w\\d\\/]*)");
var genericRegex = new RegExp("https:\\/\\/www\\.dndbeyond\\.com\\/([\\w\\d\\/]*)");

function send_monster_html (results){
    var data = {
        "html": results[0]
    }

    $.ajax
    (
        {
            type: "POST",
            url: "http://localhost:8000/api/monster-scrape",
            dataType:"json",
            headers: {
                "Content-Type":"application/json"
            },
            data: JSON.stringify(data),
            success: function(msg)
            {
                console.log("URL Tracked");
            }
        }
    );

}

function import_page_of_monsters(){
    return `
    function import_monster_from_url(url){
        response = $.ajax
        (
            {
                type: "GET",
                url: url,
                dataType : "html",
                async: false
            }
        );

        var doc = document.createElement('div');
        doc.innerHTML = response.responseText

        monster_info_element = doc.getElementsByClassName("details-more-info")

        if(monster_info_element.length == 0){
            return
        }
        
        monster_html = monster_info_element[0].outerHTML

        var data = {
            "html": monster_html
        }
    
        $.ajax
        (
            {
                type: "POST",
                url: "http://localhost:8000/api/monster-scrape",
                dataType:"json",
                headers: {
                    "Content-Type":"application/json"
                },
                data: JSON.stringify(data),
                success: function(msg)
                {
                    console.log("Monster Scraped");
                }//end function
            }
        );
    }
    
    
    function get_extract_search_info_js_str(){element = document.querySelector(".RPGMonster-listing");

        monster_buttom_elements = element.getElementsByClassName("info")
        
        for( var i = 0; i < monster_buttom_elements.length; i++){
            url = "https://www.dndbeyond.com/monsters/" + monster_buttom_elements[i].getAttribute("data-slug")
            import_monster_from_url(url)
        }

    }
    get_extract_search_info_js_str();`
}

function import_all_pages_of_monsters(){
    return `
    function import_monster_from_url(url){
        response = $.ajax
        (
            {
                type: "GET",
                url: url,
                dataType : "html",
                async: false
            }
        );

        var doc = document.createElement('div');
        doc.innerHTML = response.responseText
        monster_info_element = doc.getElementsByClassName("details-more-info")

        if(monster_info_element.length == 0){
            return
        }
        
        monster_html = monster_info_element[0].outerHTML

        var data = {
            "html": monster_html
        }
    
        $.ajax
        (
            {
                type: "POST",
                url: "http://localhost:8000/api/monster-scrape",
                dataType:"json",
                headers: {
                    "Content-Type":"application/json"
                },
                data: JSON.stringify(data),
                success: function(msg)
                {
                    console.log("Monster Scraped");
                }//end function
            }
        );
    }
    
    function get_monster_urls_from_page(page_number){
        response = $.ajax
        (
            {
                type: "GET",
                url: "https://www.dndbeyond.com/monsters?page=" + page_number,
                dataType : "html",
                async: false
            }
        );

        var doc = document.createElement('div');
        doc.innerHTML = response.responseText

        monster_buttom_elements = doc.getElementsByClassName("info")
        monster_urls = []
        for( var i = 0; i < monster_buttom_elements.length; i++){
            url = "https://www.dndbeyond.com/monsters/" + monster_buttom_elements[i].getAttribute("data-slug")
            monster_urls.push(url)
        }
        return monster_urls
    }

    function get_max_page_of_monsters(){
        response = $.ajax
        (
            {
                type: "GET",
                url: "https://www.dndbeyond.com/monsters",
                dataType : "html",
                async: false
            }
        );

        var doc = document.createElement('div');
        doc.innerHTML = response.responseText

        page_number_elements = doc.getElementsByClassName("b-pagination-item")
        max_page_number = 0
        for( var i = 0; i < page_number_elements.length; i++){
            number = parseInt(page_number_elements[i].innerHTML)
            if(number > max_page_number){
                max_page_number = number
            }
        }
        return max_page_number
    }
    
    function get_extract_search_info_js_str(){element = document.querySelector(".RPGMonster-listing");
        var max_page_number = get_max_page_of_monsters()
        
        for(var j = 0; j < max_page_number; j++){
            monster_urls = get_monster_urls_from_page(j)
            for( var i = 0; i < monster_urls.length; i++){
                import_monster_from_url(monster_urls[i])
            }
        }
        
    }
    get_extract_search_info_js_str();`
}

function import_monster() {

    chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
        var tab = tabs[0];

        if(monsterRegex.test(tab.url)){
            chrome.tabs.executeScript(tab.id, {
                code: 'document.querySelector(".more-info.details-more-info").outerHTML'
            }, send_monster_html);
        }
      });

}

document.addEventListener('DOMContentLoaded', function() {
    var checkPageButton = document.getElementById('clickIt');
    checkPageButton.addEventListener('click', function() {  
  
        import_monster()

    }, false);
  }, false);

document.addEventListener('DOMContentLoaded', function() {
    var checkPageButton = document.getElementById('bulkImportPage');
    checkPageButton.addEventListener('click', function() {
  
        chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
            var tab = tabs[0];

            if(genericRegex.test(tab.url)){
                chrome.tabs.executeScript(tab.id, {
                    code: import_page_of_monsters()
                });
            }
          });

    }, false);
  }, false);


document.addEventListener('DOMContentLoaded', function() {
    var checkPageButton = document.getElementById('bulkImportItems');
    checkPageButton.addEventListener('click', function() {
  
        chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
            var tab = tabs[0];

            if(genericRegex.test(tab.url)){
                chrome.tabs.executeScript(tab.id, {
                    code: import_items()
                });
            }
          });

    }, false);
}, false);


document.addEventListener('DOMContentLoaded', function() {
    var checkPageButton = document.getElementById('bulkImportAll');
    checkPageButton.addEventListener('click', function() {
  
        chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
            var tab = tabs[0];

            if(genericRegex.test(tab.url)){
                chrome.tabs.executeScript(tab.id, {
                    code: import_all_pages_of_monsters()
                });
            }
          });

    }, false);
  }, false);
