function interceptData() {
    var xhrOverrideScript = document.createElement('script');
    xhrOverrideScript.type = 'text/javascript';
    xhrOverrideScript.innerHTML = `
    (function() {
      var XHR = XMLHttpRequest.prototype;
      var send = XHR.send;
      var open = XHR.open;
      XHR.open = function(method, url) {
          this.url = url; // the request url
          return open.apply(this, arguments);
      }
      XHR.send = function() {
          this.addEventListener('load', function() {
              console.log(this.url)
              if (this.url.includes('character-service.dndbeyond.com/character/v4/game-data/items')) {
                  var dataDOMElement = document.createElement('div');
                  console.log("Items Found")
                  $.ajax(
                    {
                        type: "POST",
                        url: "http://localhost:8000/api/item-scrape",
                        dataType:"json",
                        headers: {
                            "Content-Type":"application/json"
                        },
                        data: JSON.stringify({items: this.response}),
                        success: function(msg)
                        {
                            console.log("item Scraped");
                        }
                    }
                );
              }               
          });
          return send.apply(this, arguments);
      };
    })();
    `
    document.head.prepend(xhrOverrideScript);
  }
  function checkForDOM() {
    if (document.body && document.head) {
      interceptData();
    } else {
      requestIdleCallback(checkForDOM);
    }
  }
  requestIdleCallback(checkForDOM);