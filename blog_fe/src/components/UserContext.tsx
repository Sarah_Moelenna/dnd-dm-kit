import React from 'react';

export interface IUser {
  id: string,
  email: string,
  display_name: string,
  created_at: string,
  user_type: string,
  forename: string,
  surname: string,
  permissions: Array<string>
}

export interface IUserContext {
  user?: IUser;
  update_user?: Function;
}

const defaultState = {
  user: undefined,
};

export const UserContext = React.createContext<IUserContext>(defaultState);
