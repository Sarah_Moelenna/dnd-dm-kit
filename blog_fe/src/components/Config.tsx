const { REACT_APP_ACCOUNTS_API_URL, REACT_APP_BLOG_API_URL, REACT_APP_ACCOUNTS_FE_URL, REACT_APP_BLOG_FE_URL } = process.env;

declare global {
    interface Window { BLOG_API_URL: any; ACCOUNTS_API_URL: any; ACCOUNTS_FE_URL: any; BLOG_FE_URL: any; }
}

export const get_accounts_api_url = () => {
    if(process.env.NODE_ENV == 'development'){
        return REACT_APP_ACCOUNTS_API_URL;
    } else {
        return window.ACCOUNTS_API_URL;
    }
}

export const get_accounts_fe_url = () => {
    if(process.env.NODE_ENV == 'development'){
        return REACT_APP_ACCOUNTS_FE_URL;
    } else {
        return window.ACCOUNTS_FE_URL;
    }
}

export const get_blog_api_url = () => {
    if(process.env.NODE_ENV == 'development'){
        return REACT_APP_BLOG_API_URL;
    } else {
        return window.BLOG_API_URL;
    }
}

export const get_blog_fe_url = () => {
    if(process.env.NODE_ENV == 'development'){
        return REACT_APP_BLOG_FE_URL;
    } else {
        return window.BLOG_FE_URL;
    }
}