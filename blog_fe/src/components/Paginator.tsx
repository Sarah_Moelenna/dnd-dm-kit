import React from 'react';

interface IPaginator {
    current_page: number;
    total_pages: number;
    page_change_function: Function
}

const Paginator = ({ current_page, total_pages, page_change_function}: IPaginator) => {
    var pages_to_display = []
    for (var i = (current_page - 6); i < (current_page + 6); i++){
        if (i > 0 && i <= total_pages){
            pages_to_display.push(i)
        }
    }

    if(total_pages == 1){
        return null
    }

    return (
        <div className="paginator">
            {current_page != 1 && <div className="item item-first" onClick={() => page_change_function(1)}><p>First</p></div>}
            {pages_to_display.map((page) => (
                <div key={page} className={page != current_page ? 'item' : 'item item-inactive'} onClick={() => page_change_function(page)}>
                    <p>{page}</p>
                </div>
            ))}
            {current_page != total_pages && <div className="item item-last" onClick={() => page_change_function(total_pages)}><p>Last</p></div>}
        </div>
    );
}

export default Paginator;