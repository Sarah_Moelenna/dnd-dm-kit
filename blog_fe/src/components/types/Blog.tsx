export interface IBlogSimple {
    id: string
    title: string
    author?: IAuthor | null
    excerpt: string
    thumbnail_src: string
    published_at: string
    meta_title: string
    meta_description: string
    meta_image: string
    status: string
}

interface IBlogNavigation{
    title: string
    path: string
}

export interface IBlog {
    id: string
    title: string
    author: IAuthor | null
    excerpt: string
    thumbnail_src: string
    published_at: string
    meta_title: string
    meta_description: string
    meta_image: string
    status: string
    slug: string
    tag: string
    banner_src: string
    created_by: string
    created_at: string
    updated_at: string
    deleted_at: string
    content?: Array<IContentBlock> | null
    previous_blog: string
    previous_blog_info: IBlogNavigation
    next_blog_info: IBlogNavigation
}

export interface IBlogUpdate {
    title: string
    author: IAuthor | null
    excerpt: string
    thumbnail_src: string
    published_at: string
    meta_title: string
    meta_description: string
    meta_image: string
    slug: string
    tag: string
    banner_src: string
    previous_blog: string
}
  
export interface IBlogs {
    results: Array<IBlogSimple>
    current_page: number
    total_pages: number
    total_results: number
}

export interface IContentImage {
    id: string
    url: string
    alt?: string
    text?: string
}

export interface IContentBlock {
    id: string
    type?: string
    data?: string | Array<IContentImage>
}

export interface IUploadResponse {
    file_location: string,
    success: boolean
}

export interface IAuthor {
    name: string
    image_src: string
}