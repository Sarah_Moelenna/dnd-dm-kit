import React, {Component} from 'react';
import { BLOG_GET, BLOG_FORM, BLOG_PATCH } from '../BlogRequests';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { IBlog, IContentBlock, IUploadResponse } from '../types/Blog';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { get_blog_api_url } from '../Config';
import { v4 as uuidv4 } from 'uuid';
import { ContentBlock } from './ContentBlock'
import { Authors } from '../data/Authors';
import { BlogSelect } from './BlogSelect'

interface IProps {
    blog_id: string,
    close_function: Function
}

interface IAddComponentProps {
    id?: string,
    add_content_function: Function
}

interface IState {
    blog?: IBlog;
    previous_blog: IBlog | null
}

class AddComponent extends Component<IAddComponentProps> {
    constructor(props: IAddComponentProps) {
        super(props);
    }

    render () {
       
        return (
            <div className='add-content-block'>
                {this.props.id != undefined ?
                    <div className='icon' onClick={() => {this.props.add_content_function(this.props.id)}}>
                        <i className="fas fa-plus"></i>
                    </div>
                :
                    <div className='icon' onClick={() => {this.props.add_content_function()}}>
                        <i className="fas fa-plus"></i>
                    </div>
                }
            </div>
        )
    }
}

export class Blog extends Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
    
        this.state = {
            blog: undefined,
            previous_blog: null
        };
    }

    componentWillMount() {
        BLOG_GET(
            "/blogs/" + this.props.blog_id,
            (jsondata: IBlog) => {
                this.setState({blog: jsondata})
                if(jsondata.previous_blog){
                    this.get_previous_blog(jsondata.previous_blog)
                }
            },
            (e: string) => { this.props.close_function() }
        )
    }

    select_previous_blog = (id: string) => {
        if(this.state.blog == undefined){
            return;
        }
        let new_blog = this.state.blog;
        new_blog.previous_blog = id
        this.get_previous_blog(id)
        this.setState({ blog: new_blog });
    }

    get_previous_blog(id: string) {
        BLOG_GET(
            "/blogs/" + id,
            (jsondata: IBlog) => {
                this.setState({previous_blog: jsondata})
            },
            (e: string) => { this.props.close_function() }
        )
    }

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(this.state.blog == undefined){
            return;
        }
        let new_blog = this.state.blog;

        switch(e.target.name){
            case "title":
                new_blog.title = e.target.value;
                break;
            case "author":
                let new_author = null
                for(var i = 0; i < Authors.length; i++){
                    if(Authors[i].name == e.target.value){
                        new_author = Authors[i]
                    }
                }
                new_blog.author = new_author;
                break;
            case "excerpt":
                new_blog.excerpt = e.target.value;
                break;
            case "meta_title":
                new_blog.meta_title = e.target.value;
                break;
            case "meta_description":
                new_blog.meta_description = e.target.value;
                break;
            case "slug":
                new_blog.slug = e.target.value;
                break;
            case "tag":
                new_blog.tag = e.target.value;
                break;
            default:
                break;
        }

        this.setState({ blog: new_blog });
    };

    save_blog = () => {
        if(this.state.blog == undefined){
            return
        }

        let data: object = {
            "title": this.state.blog.title,
            "author": this.state.blog.author,
            "excerpt": this.state.blog.excerpt,
            "thumbnail_src": this.state.blog.thumbnail_src,
            "meta_title": this.state.blog.meta_title,
            "meta_description": this.state.blog.meta_description,
            "meta_image": this.state.blog.meta_image,
            "slug": this.state.blog.slug,
            "tag": this.state.blog.tag,
            "banner_src": this.state.blog.banner_src,
            "content": this.state.blog.content,
            "previous_blog": this.state.blog.previous_blog
        }

        BLOG_PATCH(
            '/blogs/' + this.state.blog.id,
            data,
            (jsondata: IBlog) => {
                this.setState({blog: jsondata})
            },
            (e: string) => {console.log(e);}
        )
    }

    upload_image = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(e.target.files == null || e.target.files.length == 0){
            return
        }

        const formData = new FormData()
        formData.append('file', e.target.files[0], e.target.files[0].name)

        BLOG_FORM(
            '/image-upload',
            formData,
            (jsondata: IUploadResponse) => {
                if(jsondata.success == true){
                    if(this.state.blog != undefined){
                        let new_blog = this.state.blog;

                        switch(e.target.name){
                            case "thumbnail_src":
                                new_blog.thumbnail_src = get_blog_api_url() + "/" + jsondata.file_location;
                                break;
                            case "banner_src":
                                new_blog.banner_src = get_blog_api_url() + "/" + jsondata.file_location;
                                break;
                            case "meta_image":
                                new_blog.meta_image = get_blog_api_url() + "/" + jsondata.file_location;
                                break;
                            default:
                                break;
                        }

                        this.setState({ blog: new_blog });
                    }
                }
            },
            (e: string) => {console.log(e);}
        )
    }

    add_content_block = (id?: string) => {
        if(this.state.blog == undefined){
            return;
        }

        if(id != undefined){
            if(this.state.blog.content == undefined){
                return;
            }
            let new_content_block: IContentBlock = {
                "id": uuidv4()
            };

            let new_content: Array<IContentBlock> = []
            for(var i = 0; i < this.state.blog.content.length; i++){
                new_content.push(this.state.blog.content[i]);
                if(this.state.blog.content[i].id == id){
                    new_content.push(new_content_block)
                }
            }

            let new_blog = this.state.blog;
            new_blog.content = new_content;
            this.setState({ blog: new_blog });

        } else {
            let new_blog = this.state.blog;
            let new_content_block: IContentBlock = {
                "id": uuidv4()
            };
            if(new_blog.content == undefined){
                new_blog.content = [];
            }
            new_blog.content.unshift(new_content_block)
            this.setState({ blog: new_blog });
        }
    }

    remove_content_block = (id: string) => {
        if(this.state.blog == undefined){
            return;
        }
        if(this.state.blog.content == undefined){
            return;
        }

        let new_content: Array<IContentBlock> = []
        for(var i = 0; i < this.state.blog.content.length; i++){
            if(this.state.blog.content[i].id != id){
                new_content.push(this.state.blog.content[i])
            }
        }

        let new_blog = this.state.blog;
        new_blog.content = new_content;
        this.setState({ blog: new_blog });

    }

    update_content_block = (new_content_block: IContentBlock) => {
        if(this.state.blog == undefined){
            return;
        }
        if(this.state.blog.content == undefined){
            return;
        }

        let new_content: Array<IContentBlock> = []
        for(var i = 0; i < this.state.blog.content.length; i++){
            if(this.state.blog.content[i].id != new_content_block.id){
                new_content.push(this.state.blog.content[i])
            } else {
                new_content.push(new_content_block)
            }
        }

        let new_blog = this.state.blog;
        new_blog.content = new_content;
        this.setState({ blog: new_blog });

    }

    render () {
       
        return (
            <div>
                {this.state.blog &&
                    <Row>
                        <Col xs={12} md={12}>
                            <h1><i className="fa fa-chevron-left clickable-icon" onClick={() => this.props.close_function()}></i> {this.state.blog.title}</h1>
                        </Col>
                        <Col xs={3} md={3} className="blog-info">
                            <h2>Basic Information</h2>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Title</Form.Label>
                                <Form.Control required value={this.state.blog.title} name="title" type="text" placeholder="Blog Title" onChange={this.handleChange} disabled={this.state.blog.status == 'PUBLISHED'}/>
                            </Form.Group>

                            <Form.Group controlId="formBasicSize">
                                <Form.Label>Author</Form.Label>
                                <Form.Control name="author" as="select" onChange={this.handleChange} disabled={this.state.blog.status == 'PUBLISHED'}>
                                        <option selected={true} value="">-</option>
                                        {Authors.map((author) => (
                                            <option
                                                value={author.name}
                                                selected={(this.state.blog != undefined && this.state.blog.author != undefined) ? this.state.blog.author.name == author.name: false}
                                                key={author.name}
                                            >
                                                {author.name}
                                            </option>
                                        ))}
                                </Form.Control>
                            </Form.Group>
                            {this.state.blog.author &&
                                <div className='author'>
                                    <img src={this.state.blog.author.image_src}/>
                                    <p>{this.state.blog.author.name}</p>
                                </div>
                            }

                            <Form.Group controlId="formBasicName">
                                <Form.Label>Excerpt</Form.Label>
                                <Form.Control as="textarea" rows={3} required value={this.state.blog.excerpt} name="excerpt" type="text" placeholder="" onChange={this.handleChange} disabled={this.state.blog.status == 'PUBLISHED'}/>
                            </Form.Group>

                            {this.state.blog.status != 'PUBLISHED' ?
                                <Form.Group controlId="formFileLg" className="mb-3">
                                    <Form.Label>Banner Image</Form.Label>
                                    <Form.Control type="file" name="banner_src" size="lg" onChange={this.upload_image}/>
                                </Form.Group>
                                :
                                <Form.Label>Banner Image</Form.Label>
                            }

                            { this.state.blog.banner_src &&
                                <div className='image-container'>
                                    <img src={this.state.blog.banner_src} />
                                </div>
                            }

                            {this.state.blog.status != 'PUBLISHED' ?
                                <Form.Group controlId="formFileLg" className="mb-3">
                                    <Form.Label>Thumbnail Image</Form.Label>
                                    <Form.Control type="file" name="thumbnail_src" size="lg" onChange={this.upload_image}/>
                                </Form.Group>
                                :
                                <Form.Label>Thumbnail Image</Form.Label>
                            }

                            { this.state.blog.thumbnail_src &&
                                <div className='image-container'>
                                    <img src={this.state.blog.thumbnail_src} />
                                </div>
                            }

                            <Form.Group controlId="formFileLg" className="mb-3">
                                <Form.Label>Previous Blog</Form.Label>
                                <Form.Control required value={this.state.previous_blog != null ? this.state.previous_blog.title: ''} type="text"disabled={true}/>
                                {this.state.blog.status != 'PUBLISHED' &&
                                    <BlogSelect select_blog_function={this.select_previous_blog} />
                                }
                            </Form.Group>

                            {this.state.blog.status == 'PUBLISHED' &&
                                <Form.Group controlId="formFileLg" className="mb-3">
                                    <Form.Label>Next Blog</Form.Label>
                                    <Form.Control required value={this.state.blog.next_blog_info != null ? this.state.blog.next_blog_info.title: ''} type="text"disabled={true}/>
                                </Form.Group>
                            }

                            <hr/>
                            <h2>URL</h2>
                            <Form.Group controlId="formBasicName">
                                <Form.Control required value={"/" + this.state.blog.tag + "/" + this.state.blog.slug} name="slug" type="text" disabled={true}/>
                            </Form.Group>


                            <Form.Group controlId="formBasicName">
                                <Form.Label>Slug</Form.Label>
                                <Form.Control required value={this.state.blog.slug} name="slug" type="text" placeholder="" onChange={this.handleChange} disabled={this.state.blog.status == 'PUBLISHED'}/>
                            </Form.Group>

                            <Form.Group controlId="formBasicName">
                                <Form.Label>Tag</Form.Label>
                                <Form.Control required value={this.state.blog.tag} name="tag" type="text" placeholder="" onChange={this.handleChange} disabled={this.state.blog.status == 'PUBLISHED'}/>
                            </Form.Group>

                            <hr/>
                            <h2>Meta Data</h2>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Meta Title</Form.Label>
                                <Form.Control required value={this.state.blog.meta_title} name="meta_title" type="text" placeholder="" onChange={this.handleChange} disabled={this.state.blog.status == 'PUBLISHED'}/>
                            </Form.Group>

                            <Form.Group controlId="formBasicName">
                                <Form.Label>Meta Description</Form.Label>
                                <Form.Control required value={this.state.blog.meta_description} name="meta_description" type="text" placeholder="" onChange={this.handleChange} disabled={this.state.blog.status == 'PUBLISHED'}/>
                            </Form.Group>

                            {this.state.blog.status != 'PUBLISHED' ?
                                <Form.Group controlId="formFileLg" className="mb-3">
                                    <Form.Label>Meta Image</Form.Label>
                                    <Form.Control type="file" name="meta_image" size="lg" onChange={this.upload_image}/>
                                </Form.Group>
                                :
                                <Form.Label>Meta Image</Form.Label>
                            }

                            { this.state.blog.meta_image &&
                                <div className='image-container'>
                                    <img src={this.state.blog.meta_image} />
                                </div>
                            }

                            <hr/>
                            {this.state.blog.status != 'PUBLISHED' &&
                                <Button onClick={this.save_blog}>Save</Button>
                            }
                        </Col>
                        <Col xs={9} md={9}>
                            <h2>Content</h2>
                            {this.state.blog.content != undefined ?
                                <div className='content-block-list'>
                                    {this.state.blog.status != 'PUBLISHED' &&
                                        <AddComponent add_content_function={this.add_content_block}/>
                                    }
                                    {this.state.blog.content.map((content_block) => (
                                        <div key={content_block.id} className="list-item">
                                            <ContentBlock content_block={content_block} update_content_block={this.update_content_block} remove_content_block={this.remove_content_block} disabled={this.state.blog !=undefined && this.state.blog.status == 'PUBLISHED'}/>
                                            {this.state.blog && this.state.blog.status != 'PUBLISHED' &&
                                                <AddComponent id={content_block.id} add_content_function={this.add_content_block}/>
                                            }
                                        </div>
                                    ))}
                                </div>
                                :
                                <div className='content-block-list'>
                                    {this.state.blog.status != 'PUBLISHED' &&
                                        <AddComponent add_content_function={this.add_content_block}/>
                                    }
                                </div>
                            }
                        </Col>
                    </Row>
                }
            </div>
        )

    }
}