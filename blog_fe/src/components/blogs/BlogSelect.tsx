import React, {Component} from 'react';
import { BLOG_GET } from '../BlogRequests';
import Paginator from '../Paginator';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { stringify } from 'query-string';
import { IBlogs, IBlogSimple } from '../types/Blog';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

interface IProps {
    select_blog_function: Function
}


interface IState {
    blogs: Array<IBlogSimple>;
    total_pages: number;
    current_page: number;
    toggle: boolean;
}

interface IBlogQueryParams {
    page?: number;
    status?: string;
}
  

export class BlogSelect extends Component<IProps, IState> {

    componentWillMount() {
        this.refresh_blogs(this.state.current_page)
    }

    constructor(props: IProps) {
        super(props);
    
        this.state = {
            blogs: [],
            total_pages: 0,
            current_page: 1,
            toggle: false
        };
    }

    refresh_blogs = (page: number) => {
        var params: IBlogQueryParams = {
            page: page,
            status: 'PUBLISHED'
        }

        BLOG_GET(
            "/blogs?" + stringify(params),
            (jsondata: IBlogs) => {
                this.setState({blogs: jsondata.results, current_page: jsondata.current_page, total_pages: jsondata.total_pages})
            },
            (e: string) => { this.setState({blogs: [], current_page: 1, total_pages: 0}) }
        )

    };

    set_page = (page: number) => {
        this.refresh_blogs(page)
    };

    open = () => {
        this.refresh_blogs(this.state.current_page)
        this.setState({toggle: true})
    }

    select_function = (id: string) => {

        this.props.select_blog_function(id)
        this.setState({toggle: false})
    }

    render () {
        return (
            <div className="modal-selector-container">
                {this.state.toggle == false &&
                    <Button onClick={this.open}>Choose Previous Blog</Button>
                }
                <Modal show={this.state.toggle} className="item-select-modal select-modal">
                    <Modal.Header>Blogs</Modal.Header>
                        <Modal.Body>
                            <div className='list-items'>
                                {this.state.blogs.map((blog) => (
                                    <div key={blog.id} className="list-item">
                                        <Row>
                                            <Col xs={3} md={3}>
                                                <p className="card-text">{blog.title}</p>
                                            </Col>
                                            <Col xs={3} md={3}>
                                                <p className="card-text">{blog.author != undefined && blog.author != null ? blog.author.name : ''}</p>
                                            </Col>
                                            <Col xs={3} md={3}>
                                                <p className="card-text">{blog.status}</p>
                                            </Col>
                                            <Col xs={3} md={3}>
                                                <Button onClick={()=>this.select_function(blog.id)}>Select</Button>
                                            </Col>
                                        </Row>
                                    </div>
                                ))}
                            </div>
                            <Paginator current_page={this.state.current_page} total_pages={this.state.total_pages} page_change_function={this.set_page} />
                        </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={()=>{this.setState({toggle: !this.state.toggle})}}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}