import React, {Component} from 'react';
import { BLOG_GET } from '../BlogRequests';
import Paginator from '../Paginator';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { stringify } from 'query-string';
import { IBlogs, IBlogSimple } from '../types/Blog';
import Button from 'react-bootstrap/Button';
import { Blog } from './Blog';

interface IProps {
}


interface IState {
    blogs: Array<IBlogSimple>;
    total_pages: number;
    current_page: number;
    filters: object;
    selected_blog?: string;
}

interface IBlogQueryParams {
    page?: number;
    status?: string;
}
  

export class Blogs extends Component<IProps, IState> {

    componentWillMount() {
        this.refresh_blogs(this.state.current_page, this.state.filters)
    }

    constructor(props: IProps) {
        super(props);
    
        this.state = {
            blogs: [],
            total_pages: 0,
            current_page: 1,
            filters: {}
        };
    }

    refresh_blogs = (page: number, filters: object) => {
        var params: IBlogQueryParams = filters
        params['page'] = page

        BLOG_GET(
            "/blogs?" + stringify(params),
            (jsondata: IBlogs) => {
                this.setState({blogs: jsondata.results, current_page: jsondata.current_page, total_pages: jsondata.total_pages})
            },
            (e: string) => { this.setState({blogs: [], current_page: 1, total_pages: 0}) }
        )

    };

    set_page = (page: number) => {
        this.refresh_blogs(page, this.state.filters)
    };

    open_blog = (id: string) => {
        this.setState({selected_blog: id})
    }

    close_blog = () => {
        this.setState({selected_blog: undefined})
    }

    render () {
        if(this.state.selected_blog === undefined){
            return (
                <div className="blog-list">
                    <h1>Blogs</h1>
                    <div className='list-items'>
                        {this.state.blogs.map((blog) => (
                            <div key={blog.id} className="list-item">
                                <Row>
                                    <Col xs={3} md={3}>
                                        <p className="card-text">{blog.title}</p>
                                    </Col>
                                    <Col xs={3} md={3}>
                                        <p className="card-text">{blog.author != undefined && blog.author != null ? blog.author.name : ''}</p>
                                    </Col>
                                    <Col xs={3} md={3}>
                                        <p className="card-text">{blog.status}</p>
                                    </Col>
                                    <Col xs={3} md={3}>
                                        <Button onClick={()=>this.open_blog(blog.id)}>Open</Button>
                                    </Col>
                                </Row>
                                <hr/>
                            </div>
                        ))}
                    </div>
                    <Paginator current_page={this.state.current_page} total_pages={this.state.total_pages} page_change_function={this.set_page} />
                </div>
            )
        } else {
            return (
                <div>
                    <Blog blog_id={this.state.selected_blog} close_function={this.close_blog} />
                </div>
            )
        }
    }
}