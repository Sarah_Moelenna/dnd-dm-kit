import React, {Component} from 'react';
import { IContentBlock, IContentImage, IUploadResponse } from '../types/Blog';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MDEditor from '@uiw/react-md-editor';
import { BLOG_FORM } from '../BlogRequests';
import { get_blog_api_url } from '../Config';
import { v4 as uuidv4 } from 'uuid';
import Card from 'react-bootstrap/Card';

interface IProps {
    content_block: IContentBlock,
    update_content_block: Function,
    remove_content_block: Function,
    disabled: boolean
}
 
interface IContentBlockDataProps {
    content_block: IContentBlock,
    update_content_block: Function,
    disabled: boolean
}

class ContentBlockSubtitle extends Component<IContentBlockDataProps> {
    constructor(props: IContentBlockDataProps) {
        super(props);
    }

    update_data = (e: React.ChangeEvent<HTMLInputElement>) => {
        let new_content_block = this.props.content_block;
        new_content_block.data = e.target.value;
        this.props.update_content_block(new_content_block)
    };

    render () {
        return(
            <div>
                <Form.Group controlId="formBasicName">
                    <Form.Control required value={typeof this.props.content_block.data === 'string' ? this.props.content_block.data : ''} name="title" type="text" onChange={this.update_data} disabled={this.props.disabled}/>
                </Form.Group>
            </div>
        )
    }
}

class ContentBlockParagraph extends Component<IContentBlockDataProps> {
    constructor(props: IContentBlockDataProps) {
        super(props);
    }

    update_data = (e: React.ChangeEvent<HTMLInputElement>) => {
        let new_content_block = this.props.content_block;
        new_content_block.data = e.target.value;
        this.props.update_content_block(new_content_block)
    };

    render () {
        return(
            <div>
                <Form.Group controlId="formBasicName">
                    <Form.Control required as='textarea' rows={5} value={typeof this.props.content_block.data === 'string' ? this.props.content_block.data : ''} name="title" type="text" onChange={this.update_data} disabled={this.props.disabled}/>
                </Form.Group>
            </div>
        )
    }
}

class ContentBlockMarkdown extends Component<IContentBlockDataProps> {
    constructor(props: IContentBlockDataProps) {
        super(props);
    }

    update_data = (text?: string | undefined) => {
        let new_content_block = this.props.content_block;
        new_content_block.data = text;
        this.props.update_content_block(new_content_block)
    };

    render () {
        return(
            <div>
                <MDEditor
                    value={typeof this.props.content_block.data === 'string' ? this.props.content_block.data : ''}
                    preview="edit"
                    onChange={this.update_data}
                />
            </div>
        )
    }
}

class ContentBlockImages extends Component<IContentBlockDataProps> {
    constructor(props: IContentBlockDataProps) {
        super(props);
    }

    update_data = (new_data: Array<IContentImage>) => {
        let new_content_block = this.props.content_block;
        new_content_block.data = new_data;
        this.props.update_content_block(new_content_block)
    };

    upload_image = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(e.target.files == null || e.target.files.length == 0){
            return
        }

        const formData = new FormData()
        formData.append('file', e.target.files[0], e.target.files[0].name)

        BLOG_FORM(
            '/image-upload',
            formData,
            (jsondata: IUploadResponse) => {
                if(jsondata.success == true){
                    let new_data = this.props.content_block.data
                    if(!Array.isArray(new_data)){
                        new_data = [];
                    }
                    let new_image: IContentImage = {
                        url: get_blog_api_url() + "/" + jsondata.file_location,
                        id: uuidv4(),
                    }
                    new_data.push(new_image)
                    this.update_data(new_data)

                }
            },
            (e: string) => {console.log(e);}
        )
    }

    remove_image = (id: string) => {
        let new_data = []
        if(!Array.isArray(this.props.content_block.data)){
            return
        }

        for(var i = 0; i < this.props.content_block.data.length; i++){
            if(this.props.content_block.data[i].id != id){
                new_data.push(this.props.content_block.data[i])
            }
        }
        this.update_data(new_data)
    }

    edit_image = (id: string, key: string, value: string) => {
        if(!Array.isArray(this.props.content_block.data)){
            return
        }
        let new_data = this.props.content_block.data

        for(var i = 0; i < new_data.length; i++){
            if(new_data[i].id == id){
                new_data[i][key as keyof IContentImage] = value;
            }
        }
        this.update_data(new_data)
    }

    render () {
        return(
            <div>
                { Array.isArray(this.props.content_block.data) ?
                    <div className='blog-images'>
                        {this.props.content_block.data.map((image) => (
                            <Card key={image.id}>
                                <Card.Img variant="top" src={image.url} />
                                <Card.Body>
                                    <Form.Group>
                                        <Form.Label>Image Text</Form.Label>
                                        <Form.Control 
                                            required
                                            value={typeof image.text === 'string' ? image.text : ''}
                                            type="text"
                                            onChange={(e) => (this.edit_image(image.id, "text", e.target.value))}
                                            disabled={this.props.disabled}
                                        />
                                    </Form.Group>

                                    <Form.Group>
                                        <Form.Label>Image Alt Text</Form.Label>
                                        <Form.Control 
                                            required
                                            value={typeof image.alt === 'string' ? image.alt : ''}
                                            type="text"
                                            onChange={(e) => (this.edit_image(image.id, "alt", e.target.value))}
                                            disabled={this.props.disabled}
                                        />
                                    </Form.Group>

                                    <div className='delete'>
                                        <i className="fas fa-trash-alt clickable-icon" onClick={() => {this.remove_image(image.id)}}></i>
                                    </div>
                                </Card.Body>
                            </Card>
                        ))}
                        {this.props.disabled == false &&
                            <Card>
                                <Card.Body>
                                    <Form.Group controlId="formFileLg" className="mb-3">
                                        <Form.Label>New Image</Form.Label>
                                        <Form.Control
                                            type="file"
                                            onChange={this.upload_image}
                                            accept=".webp"
                                        />
                                    </Form.Group>
                                </Card.Body>
                            </Card>
                        }
                    </div>
                :
                    <div className='blog-images'>
                        {this.props.disabled == false &&
                            <Card>
                                <Card.Body>
                                    <Form.Group controlId="formFileLg" className="mb-3">
                                        <Form.Label>New Image</Form.Label>
                                        <Form.Control type="file" onChange={this.upload_image}/>
                                    </Form.Group>
                                </Card.Body>
                            </Card>
                        }
                    </div>
                }
            </div>
        )
    }
}

export class ContentBlock extends Component<IProps> {

    constructor(props: IProps) {
        super(props);
    }

    update_type = (e: React.ChangeEvent<HTMLInputElement>) => {
        let new_content_block = this.props.content_block;
        new_content_block.type = e.target.value;
        new_content_block.data = undefined
        this.props.update_content_block(new_content_block)
    };

    render () {
       
        return (
            <div className='content-block'>
                <Row>
                    <Col xs={1} md={1}>
                        <Form.Label>Type</Form.Label>
                    </Col>
                    <Col xs={2} md={2}>
                        <Form.Group controlId="formBasicSize">
                            
                            <Form.Control name="type" as="select" onChange={this.update_type} disabled={this.props.disabled}>
                                    <option selected={true} value="">-</option>
                                    <option value="LINE" selected={this.props.content_block.type == "LINE"}>Line</option>
                                    <option value="MARKDOWN" selected={this.props.content_block.type == "MARKDOWN"}>Markdown</option>
                                    <option value="PARAGRAPH" selected={this.props.content_block.type == "PARAGRAPH"}>Paragraph</option>
                                    <option value="SUBTITLE" selected={this.props.content_block.type == "SUBTITLE"}>Subtitle</option>
                                    <option value="IMAGES" selected={this.props.content_block.type == "IMAGES"}>Images</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col xs={8} md={8}></Col>
                    <Col xs={1} md={1}>
                        {this.props.disabled != true &&
                            <div className='delete'>
                                <i className="fas fa-trash-alt clickable-icon" onClick={() => {this.props.remove_content_block(this.props.content_block.id)}}></i>
                            </div>
                        }
                    </Col>
                    <Col xs={12} md={12}>
                        <div className='content-block-data'>
                            {this.props.content_block.type == 'SUBTITLE' &&
                                <ContentBlockSubtitle content_block={this.props.content_block} update_content_block={this.props.update_content_block} disabled={this.props.disabled}/>
                            }
                            {this.props.content_block.type == 'PARAGRAPH' &&
                                <ContentBlockParagraph content_block={this.props.content_block} update_content_block={this.props.update_content_block} disabled={this.props.disabled}/>
                            }
                            {this.props.content_block.type == 'MARKDOWN' &&
                                <ContentBlockMarkdown content_block={this.props.content_block} update_content_block={this.props.update_content_block} disabled={this.props.disabled}/>
                            }
                            {this.props.content_block.type == 'IMAGES' &&
                                <ContentBlockImages content_block={this.props.content_block} update_content_block={this.props.update_content_block} disabled={this.props.disabled}/>
                            }
                        </div>
                    </Col>
                </Row>
            </div>
        )

    }
}