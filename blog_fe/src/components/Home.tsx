import React, {Component} from 'react';
import Cookies from 'universal-cookie';
import { ACCOUNTS_GET } from './AccountsRequests';
import { get_accounts_fe_url } from './Config';
import { IUserContext, IUser } from './UserContext';
import { Blogs } from './blogs/Blogs';

const cookies = new Cookies();

export class Home extends Component {

    componentWillMount() {
        const user_context: IUserContext = this.context

        ACCOUNTS_GET(
            '/self',
            (jsondata: IUser) => {
                if(!jsondata.permissions.includes("BLOGS.MANAGEMENT")){
                    window.location.href = get_accounts_fe_url();
                }

                if(user_context.update_user != undefined){
                    user_context.update_user(jsondata)
                }
            },
            (e: string) => {
                cookies.remove('auth_token', { path: '/' });
                window.location.href = get_accounts_fe_url();
            }
        )
    }

  render () {
    return (
        <div className="home-container">
            <div className="home-content">
                <Blogs/>
            </div>
        </div>
    )
  }
}