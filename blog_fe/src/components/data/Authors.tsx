import { get_blog_fe_url } from '../Config';
import { IAuthor } from '../types/Blog';

export const Authors: Array<IAuthor> = [
    {
        name: "Sarah Anderson",
        image_src: get_blog_fe_url() + "/sarah.webp"
    },
    {
        name: "Ilja Belankins",
        image_src: get_blog_fe_url() + "/ilja.webp"
    },
    {
        name: "Gerik Peterson",
        image_src: get_blog_fe_url() + "/gerik.webp"
    }
]