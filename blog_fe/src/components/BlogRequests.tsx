import Cookies from 'universal-cookie';
import { get_blog_api_url } from './Config';

const cookies = new Cookies();
const REACT_APP_BLOG_API_URL = get_blog_api_url()

export const BLOG_FORM = (path: string, data: BodyInit, callback: Function, error_callback: Function) => {

    var headers = {
        "token": cookies.get('auth_token')
    }
    
    fetch(REACT_APP_BLOG_API_URL + path, {
        method: "POST",
        headers: headers,
        body: data
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const BLOG_POST = (path: string, data: object, callback: Function, error_callback: Function) => {

    var body = JSON.stringify(data)

    var headers = {
        'Content-Type': 'application/json',
        "token": cookies.get('auth_token')
    }
    
    fetch(REACT_APP_BLOG_API_URL + path, {
        method: "POST",
        headers: headers,
        body: body
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const BLOG_PUT = (path: string, data: object, callback: Function, error_callback: Function) => {

    var body = JSON.stringify(data)

    var headers = {
        'Content-Type': 'application/json',
        "token": cookies.get('auth_token')
    }
    
    fetch(REACT_APP_BLOG_API_URL + path, {
        method: "PUT",
        headers: headers,
        body: body
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const BLOG_PATCH = (path: string, data: object, callback: Function, error_callback: Function) => {

    var body = JSON.stringify(data)

    var headers = {
        'Content-Type': 'application/json',
        "token": cookies.get('auth_token')
    }
    
    fetch(REACT_APP_BLOG_API_URL + path, {
        method: "PATCH",
        headers: headers,
        body: body
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const BLOG_DELETE = (path: string, data: object, callback: Function, error_callback: Function) => {

    var body = JSON.stringify(data)

    var headers = {
        'Content-Type': 'application/json',
        "token": cookies.get('auth_token')
    }
    
    fetch(REACT_APP_BLOG_API_URL + path, {
        method: "DELETE",
        headers: headers,
        body: body
        }).then( response => {
            if (response.status >= 400) { throw response }
            if (callback != undefined){
                response.json().then(function(result) {
                    callback(result)
                }).catch((error) => {
                    callback(response)
                })
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}

export const BLOG_GET = (path: string, callback: Function, error_callback: Function) => {

    var headers = {
        "token": cookies.get('auth_token')
    }

    fetch(REACT_APP_BLOG_API_URL + path, {
        method: "GET",
        headers: headers,
        }).then( response => {
            if (response.status >= 400) { throw response }
            return response
        }).then(res => res.json())
        .then((response) => {
            if (callback != undefined){
                callback(response)
            }
        })
        .catch((error) => {
            if (error_callback != undefined){
                error_callback(error)
            }
        });
}
