import React, {Component} from 'react';
import { BrowserRouter, Switch , Route  } from "react-router-dom";
import { UserContext } from './components/UserContext';
import { Home } from './components/Home';

class App extends Component {

  state = {
    user: undefined,
  }

  update_user: Function = (user: object) => {
    this.setState({user: user})
  }

  get_user_context = () => {
    return {
      user: this.state.user,
      update_user: this.update_user
    }
  }

  render () {
    return (
      <div className="App">
        <UserContext.Provider value={this.get_user_context()}>
            <BrowserRouter>
              <Switch>
                <Route path={["/"]} component={Home} />
              </Switch>
            </BrowserRouter>
          </UserContext.Provider>
      </div>
    );
  }
}

export default App;
